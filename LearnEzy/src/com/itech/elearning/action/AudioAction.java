package com.itech.elearning.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.AudioForm;
import com.itech.elearning.forms.ContantMasterForm;
import com.itech.elearning.handler.AudioHandler;
import com.itech.elearning.handler.ContantMasterHandler;
import com.itech.elearning.utils.FileUploadDownloadUtil;
import com.itech.elearning.utils.StringUtil;
public class AudioAction extends BaseAction
{
	AudioHandler handler = new AudioHandler();
	ContantMasterHandler handlerCourse = new ContantMasterHandler();
   public ActionForward list(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
   {
	   try{
		   HttpSession session = request.getSession();
		   String compid = null;
		   int roleid = 0;
		   compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		   long userid = ((LoginForm)session.getAttribute("userDetail")).getUserId();
		   roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
		   List<ContantMasterForm> courseList = handlerCourse.listAllCourse(roleid,compid,userid,getDataSource(request));
			request.setAttribute("courseList", courseList);

			  List<AudioForm> audioList = handler.list(roleid,compid,getDataSource(request));
				request.setAttribute("audioList",audioList);				
			

		}catch (Exception e) {
			e.printStackTrace();
		}

	   return mapping.findForward("success");
   }
   
   public ActionForward add(ActionMapping mapping,ActionForm form,HttpServletRequest request, HttpServletResponse response ) throws Exception
   {
	   try{
			
		   HttpSession session = request.getSession();
		   String compid = null;
		   compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
				AudioForm paymentForm = (AudioForm)form;
				String fileName="";
				String audioUploadPath= getServlet().getServletContext().getRealPath("/")+ "audioFiles";
				 
				 
				FormFile myFile= paymentForm.getAudioFile();
				if(myFile.getFileName()!="" && myFile.getFileSize()>2){
						fileName=StringUtil.repStrUnScr(paymentForm.getCourseId()+myFile.getFileName());
						FileUploadDownloadUtil.fileUploader(myFile, audioUploadPath, fileName);
						paymentForm.setFileName(fileName);
				}
			    request.setAttribute("status",handler.add(compid,paymentForm,getDataSource(request)));
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	   return list(mapping, form, request, response);
   }
 
   
public ActionForward changestatus(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
   {
	   try{
			
		   HttpSession session = request.getSession();
		   String compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			boolean active=Boolean.valueOf(request.getParameter("active"));
			int audioId=Integer.valueOf(request.getParameter("audioId"));
				request.setAttribute("status", handler.changestatus(getDataSource(request),active,audioId,compid));
				
		 

		}catch (Exception e) {
			e.printStackTrace();
		}
	   return list(mapping, form, request, response);
   }
   
   
public ActionForward update(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
   {
	   try{ 	
		   HttpSession session = request.getSession();
		   String compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		   int userid=1;
				AudioForm paymentForm = (AudioForm)form;
				String fileName="";
				String audioUploadPath= getServlet().getServletContext().getRealPath("/")+ "audioFiles";
				 
				 
				FormFile myFile= paymentForm.getAudioFile();
				if(myFile.getFileName()!="" && myFile.getFileSize()>2){
						fileName=StringUtil.repStrUnScr(paymentForm.getCourseId()+myFile.getFileName());
						FileUploadDownloadUtil.fileUploader(myFile, audioUploadPath, fileName);
						paymentForm.setFileName(fileName);
				}
				request.setAttribute("status",handler.update(compid,paymentForm,getDataSource(request),userid));
			 
		}catch (Exception e) {
			e.printStackTrace();
		}
	   return list(mapping, form, request, response);
   }
   
public ActionForward excelReport(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{

	try{
		  HttpSession session = request.getSession();
		   String compid = null;
		   int roleid = 0;
		   compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		   long userid = ((LoginForm)session.getAttribute("userDetail")).getUserId();
		   roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
		   List<ContantMasterForm> courseList = handlerCourse.listAllCourse(roleid,compid,userid,getDataSource(request));
			request.setAttribute("courseList", courseList);

			  List<AudioForm> audioList = handler.list(roleid,compid,getDataSource(request));
				request.setAttribute("audioList",audioList);				
			
		
	}catch (Exception e) {
		e.printStackTrace();
	}
return mapping.findForward("excel");

}


}
