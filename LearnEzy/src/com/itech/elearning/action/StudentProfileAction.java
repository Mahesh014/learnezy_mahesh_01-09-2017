package com.itech.elearning.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import com.itech.elearning.forms.ContryForm;
import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.StudentProfileForm;
import com.itech.elearning.forms.StudentRegistrationForm;
import com.itech.elearning.handler.CountryHandler;
import com.itech.elearning.handler.LoginHandler;
import com.itech.elearning.handler.StudentProfileHandler;
import com.itech.elearning.handler.MYStudentRegistrationHandler;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.FileUploadDownloadUtil;

public class StudentProfileAction extends BaseAction {

	StudentProfileHandler handler=new StudentProfileHandler();
	MYStudentRegistrationHandler handler1=new MYStudentRegistrationHandler();
	LoginHandler handler2=new LoginHandler();
	CountryHandler countryHandler = new CountryHandler();
	public ActionForward toHome(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)
	throws IOException {

		HttpSession session = request.getSession(true);
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			return mapping.findForward("student");
		} else
			return mapping.findForward("session");

	}	


	public ActionForward getProfileData(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws IOException {

		HttpSession session = request.getSession(true);
		Long userid = null;
		String compid = null;
		int roleid = 0;
		try {
			//String CourseIdsList= ((LoginForm) session.getAttribute("userDetail")).getCourseids();
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
			String CourseIdsList= handler.getCourseList(getDataSource(request),userid);
		
			List<StudentRegistrationForm> courselist = handler1.courselist(compid,getDataSource(request));
			request.setAttribute("courselist", courselist);

			StudentProfileForm stdentinfoList=handler.getStudentData(userid,CourseIdsList,getDataSource(request));
			request.setAttribute("stdentinfoList", stdentinfoList);
			
			List<ContryForm> countryList = countryHandler.activeList(roleid,compid,getDataSource(request));
			request.setAttribute("countryList", countryList);
			

		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {

			return mapping.findForward("studentprofileinfo");
		} else
			return mapping.findForward("session");



	}	
	
public ActionForward updateStudentData(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		StudentProfileForm studentForm = (StudentProfileForm) form;
		HttpSession session = request.getSession(true);
		try {
			LoginForm userDetail = ((LoginForm) session.getAttribute("userDetail"));
			
			String profilePhotoName="";
			String fileUploadPath= getServlet().getServletContext().getRealPath("/")+ "imageUpload";
			boolean uploadStatus=true;
			boolean ssFlag=false;
			FormFile pFile= studentForm.getImageFile();
		
			if(pFile.getFileName()!=null && pFile.getFileSize()>2){
				profilePhotoName=studentForm.getUname()+pFile.getFileName();
				uploadStatus = FileUploadDownloadUtil.fileUploader(pFile, fileUploadPath, profilePhotoName);
				ssFlag=true;
				userDetail.setProfilePhoto(profilePhotoName);
				session.setAttribute("userDetail", userDetail);
			}else{
				profilePhotoName=studentForm.getOldPhoto();
			}
			if(uploadStatus){
				String theCourseList[] =request.getParameterValues("courseidslist");
				String status = handler.updateUser(studentForm,theCourseList, getDataSource(request),profilePhotoName);
				request.setAttribute("status", status);
			}
			if(ssFlag){
			request.setAttribute("status", Common.REC_UPDATED);
			}
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		return getProfileData(mapping, form, request, response);

	}




}
