package com.itech.elearning.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONArray;

import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.PaymentMasterForm;
import com.itech.elearning.handler.PaymentMasterHandler;
public class PaymentMasterAction extends BaseAction
{
	PaymentMasterHandler handler = new PaymentMasterHandler();
   public ActionForward list(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
   {
	   try{
		   HttpSession session = request.getSession();
		   String compid = null;
		   int roleid = 0;
		   compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		   roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
			  List<PaymentMasterForm> paymentList = handler.list(roleid,compid,getDataSource(request));
				request.setAttribute("paymentList",paymentList);				
			

		}catch (Exception e) {
			e.printStackTrace();
		}

	   return mapping.findForward("paymentMaster");
   }
   
   public ActionForward add(ActionMapping mapping,ActionForm form,HttpServletRequest request, HttpServletResponse response ) throws Exception
   {
	   try{
			
		   HttpSession session = request.getSession();
		   String compid = null;
		   compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
				PaymentMasterForm paymentForm = (PaymentMasterForm)form;
			    request.setAttribute("status",handler.add(compid,paymentForm,getDataSource(request)));
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	   return list(mapping, form, request, response);
   }
 
   
public ActionForward changestatus(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
   {
	   try{
			
		   HttpSession session = request.getSession();
		   String compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			boolean active=Boolean.valueOf(request.getParameter("active"));
			int paymentId=Integer.valueOf(request.getParameter("paymentId"));
				request.setAttribute("status", handler.changestatus(getDataSource(request),active,paymentId,compid));
				
		 

		}catch (Exception e) {
			e.printStackTrace();
		}
	   return list(mapping, form, request, response);
   }
   
   
public ActionForward update(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
   {
	   try{ 	
		   HttpSession session = request.getSession();
		   String compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		   int userid=1;
				PaymentMasterForm paymentForm = (PaymentMasterForm)form;
				request.setAttribute("status",handler.update(compid,paymentForm,getDataSource(request),userid));
			 
		}catch (Exception e) {
			e.printStackTrace();
		}
	   return list(mapping, form, request, response);
   }
   
public ActionForward excelReport(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{

	try{
		
		HttpSession session = request.getSession();
        String compid = null;
        
		Long userid = null;
		int roleid= 0;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
			List<PaymentMasterForm> taxMasterList = handler.list(roleid,compid,getDataSource(request));
			request.setAttribute("taxMasterList",taxMasterList);
			
			String fileName="Parameters_master";
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition","inline;filename="+""+fileName+".xls");
		} else{
			return mapping.findForward("session");
	}
	}catch (Exception e) {
		e.printStackTrace();
	}
return mapping.findForward("excel");

}
public ActionForward courseFeesAcademy(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response)
		throws Exception {
	 
	try {
		 
			JSONArray jArray = new JSONArray();
			String courseId=request.getParameter("courseId");
			System.out.println("vani Testing "+courseId);
			jArray = handler.courseFeesAcademy(getDataSource(request),courseId);
			response.getOutputStream().write(jArray.toJSONString().getBytes());
		 
	} catch (NullPointerException e) {
		e.printStackTrace();
		return mapping.findForward("session");
	}
	return null;
}

}
