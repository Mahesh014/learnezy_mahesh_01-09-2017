package com.itech.elearning.action;

import java.util.List;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import ch.qos.logback.core.net.LoginAuthenticator;

import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.TestMasterForm;
import com.itech.elearning.handler.TestMasterHandler;
import com.itech.elearning.utils.FileUpload;

public class TestMasterAction extends BaseAction {
	TestMasterHandler handler = new TestMasterHandler();

	public ActionForward list(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		TestMasterForm testForm = (TestMasterForm) form;
		HttpSession session = request.getSession();

		Long userid = null;
		String compid = null; 
		int roleid = 0;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			compid = ((LoginForm) session.getAttribute("userDetail")).getCompanyId();
			roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {

			List<TestMasterForm> courseList = handler.listAllCourse(roleid,compid,userid,getDataSource(request));
			request.setAttribute("courseList", courseList);
			
			List<TestMasterForm> testTypeList = handler.listAllTestType(roleid,compid,userid,getDataSource(request));
			request.setAttribute("testTypeList", testTypeList);
			
			List<TestMasterForm> testList = handler.listAllTest(roleid,compid,userid,getDataSource(request), testForm.getTopicId());
			request.setAttribute("testList", testList);

			/*List<TestMasterForm> topicList = handler.listAll(userid, getDataSource(request));
			request.setAttribute("topicList", topicList);*/

			request.setAttribute("topicForm", testForm);
			return mapping.findForward("TestMaster");
		}

		return list(mapping, testForm, request, response);

	}

	public ActionForward add(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		
		TestMasterForm testForm = new TestMasterForm();
		HttpSession session = request.getSession();
		String compid = null; 
		compid = ((LoginForm) session.getAttribute("userDetail")).getCompanyId();
		String courseId=request.getParameter("courseId");
		Long testTypeId=Long.parseLong(request.getParameter("testTypeId"));
		String tname=request.getParameter("tname");
		String tdate=request.getParameter("tdate");
		String qIds=request.getParameter("qIds");
		/*System.out.println(request.getParameterValues("qIds"));
		String qIds=StringUtil.sapratedByComa(request.getParameterValues("qIds"));
		System.out.println(qIds);*/
		String testTime=request.getParameter("testTime");
		String minScore=request.getParameter("minScore");
		String achivementScore=request.getParameter("achivementScore");
		
		request.setAttribute("status", handler.addTest(compid,testForm,getDataSource(request),courseId,testTypeId,tname,tdate,qIds,testTime,minScore,achivementScore));
		return list(mapping, form, request, response);
	}

	public ActionForward changestatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			TestMasterForm testForm = (TestMasterForm) form;
			testForm.setTestId(Long.parseLong((request.getParameter("testID"))));
			testForm.setActive(Boolean.parseBoolean(request.getParameter("active")));
			
			request.setAttribute("status", handler.changeTestType(testForm,getDataSource(request)));
			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");
	}

	public ActionForward update(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			TestMasterForm testForm = new TestMasterForm();
			String courseId=request.getParameter("courseId");
			Long testTypeId=Long.parseLong(request.getParameter("testTypeId"));
			String tname=request.getParameter("tname");
			String testId=request.getParameter("testId");
			String tdate=request.getParameter("tdate");
			//String qIds=StringUtil.sapratedByComa(request.getParameterValues("qIds"));
			String qIds=request.getParameter("qIds");
			String testTime=request.getParameter("testTime");
			String minScore=request.getParameter("minScore");
			String achivementScore=request.getParameter("achivementScore");
			System.out.println("Test Date: "+testId);
			request.setAttribute("status", handler.updateTest(testForm,getDataSource(request),courseId,testTypeId,tname,tdate,qIds,testTime,minScore,achivementScore,testId));
			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");
	}
	public ActionForward excelReport(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{

		try{
			TestMasterForm testForm = new TestMasterForm();
			HttpSession session = request.getSession();

			Long userid = null;
			String compid = null;
			int roleid = 0;
			
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
				compid = ((LoginForm) session.getAttribute("userDetail")).getCompanyId();
				roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {
				List<TestMasterForm> testList = handler.listAllTest(roleid,compid,userid,getDataSource(request),testForm.getTopicId());
				request.setAttribute("testList", testList);
				String fileName="Test_master";
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition","inline;filename="+""+fileName+".xls");
			} else{
				return mapping.findForward("session");
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
	return mapping.findForward("excel");

	}
	
	public ActionForward testExcelUpload(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{
		TestMasterForm testForm = (TestMasterForm) form;
		
		
		HttpSession session = request.getSession();

		Long userid = null;
		String compid = null;
		int roleid = 0;
		
		
		String  fileName=null;
		boolean uploadstatus=false;
		String filePath=null;
		  try {
			  

			  if(!testForm.getFilename1().equals("")) { 
					compid = ((LoginForm) session.getAttribute("userDetail")).getCompanyId();


					 FormFile myfile = testForm.getFilename1();
			 
					  
		 
					 filePath = getServlet().getServletContext().getRealPath("/") +"upload";
					 System.out.println("File Path="+filePath);
					 
					 fileName =myfile.getFileName();
					 String fileName1 = myfile.getFileName();
					 System.out.println("Filename="+fileName);
					 
					 
					 //Excel or csv file upload
					 try {
						uploadstatus=FileUpload.fileUploader(myfile, filePath, fileName);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
						 filePath = getServlet().getServletContext().getRealPath("/")+ "upload\\";
						 						
						String success = handler.testExcelUpload(fileName, filePath,compid);
						System.out.println("prashanth 111111111111111");
						
						request.setAttribute("status", success);
				 
			  }
				
		  }
		 catch (Exception e) {
					e.printStackTrace();
				}
		 return list(mapping, form, request, response);
	
	}
}
