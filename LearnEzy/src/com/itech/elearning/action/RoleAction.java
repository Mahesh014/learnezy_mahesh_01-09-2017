package com.itech.elearning.action;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONArray;

import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.RoleForm;
import com.itech.elearning.handler.RoleHandler;

public class RoleAction extends BaseAction {

	RoleHandler handler = new RoleHandler();

	public ActionForward list(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		Long userid = null;
		String compid = null;
		int roleid = 0;
		compid = ((LoginForm) session.getAttribute("userDetail")).getCompanyId();
		roleid = ((LoginForm) session.getAttribute("userDetail")).getRole();
		System.out.println("compid "+compid);
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
			roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			List<RoleForm> roleList = handler.list(roleid,compid,getDataSource(request));
			request.setAttribute("roleList", roleList);
			return mapping.findForward("role");
		} else
			return mapping.findForward("session");

	}

	public ActionForward add(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		Long userid = null;
		String compid = null;
		
		
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			RoleForm roleForm = (RoleForm) form;
			
			request.setAttribute("status", handler.addRole(roleForm,compid,getDataSource(request)));
			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");

	}

	public ActionForward changestatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			RoleForm roleForm = (RoleForm) form;
			roleForm
					.setRoleid(Long.parseLong((request.getParameter("roleid"))));
			roleForm.setActive(Boolean.parseBoolean(request
					.getParameter("active")));
			request.setAttribute("status", handler.changeRole(roleForm,
					getDataSource(request)));
			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");
	}

	public ActionForward update(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			RoleForm roleForm = (RoleForm) form;
			request.setAttribute("status", handler.updateRole(roleForm,
					getDataSource(request)));

			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");
	}
	public ActionForward excelReport(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{

		try{
			
			HttpSession session = request.getSession();
			
			Long userid = null;
			String compid = null;
			int roleid=0;
			try {
				userid = ((LoginForm)session.getAttribute("userDetail")).getUserId();
				compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
				roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {

				List<RoleForm> roleList = handler.list(roleid,compid,getDataSource(request));
				request.setAttribute("roleList", roleList);
				String fileName="Role_master";
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition","inline;filename="+""+fileName+".xls");
			} else{
				return mapping.findForward("session");
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
	return mapping.findForward("excel");

	}
	
public ActionForward getlicenseid(ActionMapping mapping, ActionForm form, HttpServletRequest request,HttpServletResponse response)throws Exception {
		
		try {
			
			

			JSONArray obj = handler.getlicenseid(getDataSource(request),request,response);
				response.getOutputStream().write(obj.toString().getBytes());

				
				} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
public ActionForward licenseadd(ActionMapping mapping, ActionForm form, HttpServletRequest request,HttpServletResponse response)throws Exception {
	
	try {
		String licenseid=request.getParameter("licensid");
		String licensename=request.getParameter("licensename");
		String space=request.getParameter("space");
		String clientper=request.getParameter("clientper");


		System.out.println("licenseid"+licenseid);
		

		JSONArray obj = handler.licenseadd(getDataSource(request),request,response,licensename,space,clientper,licenseid);
			response.getOutputStream().write(obj.toString().getBytes());

			
			} catch (Exception e) {
		e.printStackTrace();
	}
	return null;
}
public ActionForward licenseupdate(ActionMapping mapping, ActionForm form, HttpServletRequest request,HttpServletResponse response)throws Exception {
	
	try {
		String licenseid=request.getParameter("licensid");
		String licensename=request.getParameter("licensename");
		String space=request.getParameter("space");
		String clientper=request.getParameter("clientper");


		System.out.println("licenseid"+licenseid);
		

		JSONArray obj = handler.licenseupdate(getDataSource(request),request,response,licensename,space,clientper,licenseid);
			response.getOutputStream().write(obj.toString().getBytes());

			
			} catch (Exception e) {
		e.printStackTrace();
	}
	return null;
}


















public ActionForward chanstatus(ActionMapping mapping, ActionForm form, HttpServletRequest request,HttpServletResponse response)throws Exception {
	
	try {
		String licenseid=request.getParameter("id");
		String status=request.getParameter("status");
		


		System.out.println("licenseid"+licenseid);
		

		JSONArray obj = handler.chanstatus(getDataSource(request),request,licenseid,status);
			response.getOutputStream().write(obj.toString().getBytes());

			
			} catch (Exception e) {
		e.printStackTrace();
	}
	return null;
}






public ActionForward licenselist(ActionMapping mapping, ActionForm form, HttpServletRequest request,HttpServletResponse response)throws Exception {
	
	try {
		
		

		JSONArray obj = handler.licenselist(getDataSource(request),request,response);
			response.getOutputStream().write(obj.toString().getBytes());

			
			} catch (Exception e) {
		e.printStackTrace();
	}
	return null;
}


	
	
	
	
	
}
