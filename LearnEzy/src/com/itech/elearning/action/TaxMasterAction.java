package com.itech.elearning.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.TaxMasterForm;
import com.itech.elearning.handler.TaxMasterHandler;
public class TaxMasterAction extends BaseAction
{
   TaxMasterHandler taxHandler = new TaxMasterHandler();
   public ActionForward list(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
   {
	   try{
		   HttpSession session = request.getSession();
		   String compid = null;
		   int roleid = 0;
		   compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		   roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
			  List<TaxMasterForm> taxMasterList = taxHandler.list(roleid,compid,getDataSource(request));
				request.setAttribute("taxMasterList",taxMasterList);				
			

		}catch (Exception e) {
			e.printStackTrace();
		}

	   return mapping.findForward("taxmaster");
   }
   
   public ActionForward add(ActionMapping mapping,ActionForm form,HttpServletRequest request, HttpServletResponse response ) throws Exception
   {
	   try{
			
		   HttpSession session = request.getSession();
		   String compid = null;
		   compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
				TaxMasterForm taxMasterform = (TaxMasterForm)form;
			    request.setAttribute("status",taxHandler.add(compid,taxMasterform,getDataSource(request)));
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	   return list(mapping, form, request, response);
   }
 
   
public ActionForward changestatus(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
   {
	   try{
			
		   HttpSession session = request.getSession();
		   String compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			   boolean active=Boolean.valueOf(request.getParameter("active"));
				int gid=Integer.valueOf(request.getParameter("taxId"));
				request.setAttribute("status", taxHandler.changestatus(getDataSource(request),active,gid,compid));
				
		 

		}catch (Exception e) {
			e.printStackTrace();
		}
	   return list(mapping, form, request, response);
   }
   
   
public ActionForward update(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
   {
	   try{ 	
		   HttpSession session = request.getSession();
		   String compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		   int userid=1;
				TaxMasterForm taxMasterform = (TaxMasterForm)form;
				request.setAttribute("status",taxHandler.update(compid,taxMasterform,getDataSource(request),userid));
			 
		}catch (Exception e) {
			e.printStackTrace();
		}
	   return list(mapping, form, request, response);
   }
   
public ActionForward excelReport(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{

	try{
		
		HttpSession session = request.getSession();
        String compid = null;
        
		Long userid = null;
		int roleid = 0;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
			List<TaxMasterForm> taxMasterList = taxHandler.list(roleid,compid,getDataSource(request));
			request.setAttribute("taxMasterList",taxMasterList);
			
			String fileName="Parameters_master";
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition","inline;filename="+""+fileName+".xls");
		} else{
			return mapping.findForward("session");
	}
	}catch (Exception e) {
		e.printStackTrace();
	}
return mapping.findForward("excel");

}


}
