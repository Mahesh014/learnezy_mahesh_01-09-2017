package com.itech.elearning.action;

import java.util.List;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONArray;

import com.itech.elearning.forms.AppointmentForm;
import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.handler.AppointmentHandler;

public class AppointmentAction extends BaseAction{
	AppointmentHandler handler = new AppointmentHandler();
	public ActionForward list(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {

		System.out.println("APPP");
		HttpSession session = request.getSession();

		Long userid = null;
		int roleid = 0;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			  String compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			  List<AppointmentForm> appointmentList = handler.list(roleid,getDataSource(request),compid);
			  request.setAttribute("appointmentList",appointmentList);
		}

		return mapping.findForward("appointment");

	}
	

public ActionForward add(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
  {
	 
	  try{
			HttpSession session=request.getSession();
			Long userid = null;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {	 
				  String compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
				  System.out.println("Inside the add method");
				  String status=null;
				  AppointmentForm apptForm = (AppointmentForm)form;
				 
				        status = handler.add(apptForm,getDataSource(request),compid);
				        System.out.println(status);
				        request.setAttribute("status", status);
					
				  System.out.println("result "+status);


			}else{
				return mapping.findForward("sessionfailure");
			}
			
			}catch (Exception e) {
				e.printStackTrace();
			}
		
	  

	  return list(mapping, form, request, response);
  }
  
  
public ActionForward changestatus(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
  {
	 
	  
	  try{
			HttpSession session=request.getSession();
			Long userid = null;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {	 
				  AppointmentForm apptForm = (AppointmentForm)form;
				  apptForm.setAppointmentId(Integer.parseInt(request.getParameter("appointmentId").trim()));
				  apptForm.setActive(Boolean.parseBoolean(request.getParameter("active")));
				
				  System.out.println("haiiii  hhh");
				  request.setAttribute("status",handler.changeStatus(apptForm,getDataSource(request)));


			}else{
				return mapping.findForward("sessionfailure");
			}
			
			}catch (Exception e) {
				e.printStackTrace();
			}
		
	  
	  

	
	  return list(mapping, form, request, response);
  }
  
  
public ActionForward update(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
  {
	 
	  try{
			HttpSession session=request.getSession();
			Long userid = null;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {	 
				  AppointmentForm apptForm = (AppointmentForm)form;
					 
				  
			      request.setAttribute("status", handler.updateCountry(apptForm,getDataSource(request),userid));



			}else{
				return mapping.findForward("sessionfailure");
			}
			
			}catch (Exception e) {
				e.printStackTrace();
			}
		
	  
	  
	

				return list(mapping, form, request, response);
  }
  
  
 
public ActionForward ajaxActiveList(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception {
  	HttpSession session=request.getSession();
  	Long userid = null;
	try {
		userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
	} catch (NullPointerException e) {
		return mapping.findForward("session");
	}
	if (userid != null) {	 
		String countryid=request.getParameter("countryid");
		JSONArray jArray =handler.ajaxActiveList(getDataSource(request),countryid);
		 
		response.getOutputStream().write(jArray.toJSONString().getBytes());
	}else{
		return mapping.findForward("sessionfailure");
	}
	return null;
}

public ActionForward excelReport(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{

	try{
		 
		HttpSession session = request.getSession();

		Long userid = null;
		int roleid  = 0;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			  String compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			  List<AppointmentForm> appointmentList = handler.list(roleid,getDataSource(request),compid);
			  request.setAttribute("appointmentList",appointmentList);
			  String fileName="Appointment_master";
			  response.setContentType("application/vnd.ms-excel");
			  response.setHeader("Content-Disposition","inline;filename="+""+fileName+".xls");
		} else{
			return mapping.findForward("session");
	}
	}catch (Exception e) {
		e.printStackTrace();
	}
return mapping.findForward("excel");

}
}
