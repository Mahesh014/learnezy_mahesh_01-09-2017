package com.itech.elearning.action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import javax.mail.Session;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.elearning.forms.CourseMasterForm;
import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.handler.CourseMasterHandler;
import com.itech.elearning.handler.TestQuestionMasterHandler;
import com.itech.elearning.utils.ConnectionUtil;

public class TestQuestionMasterAction extends BaseAction{

	CourseMasterHandler handler = new CourseMasterHandler();
	TestQuestionMasterHandler Handler1=new TestQuestionMasterHandler();
	
	public ActionForward list(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
	{
		//ContantMasterForm subtopicForm = (ContantMasterForm) form;
		HttpSession session = request.getSession();
		Long userid = null;
		String compid = null;
		int roleid = 0;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			List<CourseMasterForm> userList = handler.listAll(roleid,compid,userid,
					getDataSource(request));
			request.setAttribute("usersList", userList);
		}	
		
		return mapping.findForward("TestQuestionMasterAction");
		}
	
	@SuppressWarnings("unchecked")
	public ActionForward userNames(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
	{
		
		System.out.println("Ajax has been called");
		HttpSession session = request.getSession();
		String compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
    JSONArray jsonArray=new JSONArray();

	Connection con=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	String sql=null;
	try{
		con=ConnectionUtil.getMySqlConnection();
		sql="select UserId,concat(FirstName,' ',lastname)as Name from usermaster where CompanyId = '"+compid+"'";
		pstmt=con.prepareStatement(sql);
		rs=pstmt.executeQuery();
		if(rs.next())
		{
			JSONObject obj=new JSONObject();
			obj.put("UserId", rs.getInt("UserId"));
			obj.put("Name", rs.getString("Name"));
			jsonArray.add(obj);
		}
		
		response.getOutputStream().write(jsonArray.toJSONString().getBytes());
		
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	finally
	{
		try{
			if(con!=null)con.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
		return null;
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	public ActionForward topicsList(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
	{
		
		System.out.println("Ajax has been called");
		HttpSession session = request.getSession();
		
		String compid = null;
		compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
    JSONArray jsonArray=new JSONArray();
    
	int courseID=Integer.parseInt(request.getParameter("courseID"));
	
	Connection con=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	String sql=null;
	try{
		con=ConnectionUtil.getMySqlConnection();
		sql="select TopicName, TopicID from topicmaster where CourseId='"+courseID+"' and CompanyId = '"+compid+"'";
		pstmt=con.prepareStatement(sql);
		rs=pstmt.executeQuery();
		if(rs.next())
		{
			JSONObject obj=new JSONObject();
			obj.put("TopicID", rs.getInt("TopicID"));
			obj.put("TopicName", rs.getString("TopicName"));
			jsonArray.add(obj);
		}
		
		response.getOutputStream().write(jsonArray.toJSONString().getBytes());
		
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	finally
	{
		try{
			if(con!=null)con.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
		return null;
	}
	
	
	@SuppressWarnings("unchecked")
		public ActionForward subTopicList(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
	{
		HttpSession session = request.getSession();
		String compid = null;
		compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		System.out.println("subTopicList");
		String topicID=request.getParameter("TopicID");
		JSONArray array=new JSONArray();
		
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		
		try
		{
			con=ConnectionUtil.getMySqlConnection();
			String sql="select SubTopicID,SubTopicName from subtopicmaster where TopicID='"+topicID+"' and CompanyId = '"+compid+"'";
			pstmt=con.prepareStatement(sql);
			rs=pstmt.executeQuery();
			while(rs.next())
			{
				JSONObject obj=new JSONObject();
				obj.put("SubTopicID", rs.getInt("SubTopicID"));
				obj.put("SubTopicName", rs.getString("SubTopicName"));
				array.add(obj);
				System.out.println("into Action");
			}
			response.getOutputStream().write(array.toJSONString().getBytes());

		}catch(Exception e)
		{
			e.printStackTrace();
		}
		System.out.println("into Action Done");
		return null;
		
	}
	
	@SuppressWarnings("unchecked")
	public ActionForward questionsGrid(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
{
	HttpSession session = request.getSession();
	String compid = null;
	JSONArray array=new JSONArray();
	
	Connection con=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	
	try
	{
		compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		con=ConnectionUtil.getMySqlConnection();
		String sql="select t.QuestionId,t.Question,c.CourseName,tm.TopicName from testmasteroverall as t join "
                   +" coursemater as c on t.CourseId = c.CourseId join topicmaster tm on t.TopicID = tm.TopicID and CompanyId= '"+compid+"'";
		pstmt=con.prepareStatement(sql);
		rs=pstmt.executeQuery();
		while(rs.next())
		{
			JSONObject obj=new JSONObject();
			obj.put("QuestionId", rs.getInt("QuestionId"));
			obj.put("Question", rs.getString("Question"));
			obj.put("TopicName", rs.getString("TopicName"));
			obj.put("CourseName", rs.getString("CourseName"));
			array.add(obj);
		}
		response.getOutputStream().write(array.toJSONString().getBytes());

	}catch(Exception e)
	{
		e.printStackTrace();
	}
	System.out.println("into Action Done");
	return null;
	
}
	
	@SuppressWarnings("unchecked")
	public ActionForward Filteredquestions(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
{
	
	JSONArray array=new JSONArray();
	HttpSession session = request.getSession();
	String selectedIDs=request.getParameter("selected");
	Connection con=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	String compid = null;
	try
	{
		compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		con=ConnectionUtil.getMySqlConnection();
		String sql="select * from testmasteroverall where QuestionId in ("+selectedIDs+") and CompanyId = '"+compid+"'";
		pstmt=con.prepareStatement(sql);
		System.out.println("iooooo "+sql);
		rs=pstmt.executeQuery();
		while(rs.next())
		{
			JSONObject obj=new JSONObject();
			obj.put("QuestionId", rs.getInt("QuestionId"));
			obj.put("Question", rs.getString("Question"));

			array.add(obj);
		}
		response.getOutputStream().write(array.toJSONString().getBytes());

	}catch(Exception e)
	{
		e.printStackTrace();
	}
	System.out.println("into Action Done");
	return null;
	
}
	

	public ActionForward addQuestions(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
	{
		HttpSession session = request.getSession();
		Long userid = null;
		String compid = null;
		int roleid = 0;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			List<CourseMasterForm> userList = handler.listAll(roleid,compid,userid,
					getDataSource(request));
			request.setAttribute("usersList", userList);
		}		

		String courseId=request.getParameter("courseId");
		String topicId=request.getParameter("topicId");
		String subTopicId=request.getParameter("subTopicId");
		String testName=request.getParameter("testName");
		String question[]=request.getParameterValues("question");
		String ansA[]=request.getParameterValues("ansA");
		String ansB[]=request.getParameterValues("ansB");
		String ansC[]=request.getParameterValues("ansC");
		String ansD[]=request.getParameterValues("ansD");
		String crctAns[]=request.getParameterValues("crctAns");
		
		if(courseId!=null)
		{
		String addQuery=Handler1.addQuestions(compid,courseId,topicId,subTopicId,testName,question,ansA,ansB,ansC,ansD,crctAns,getDataSource(request));
		request.setAttribute("addQuery", addQuery);
		}
		
		return mapping.findForward("TestQuestionMasterAction");
	
	}
	
	public ActionForward UpdateQuestions(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
	{

		String selectedIDs=request.getParameter("QuestionsID");
		String userID=request.getParameter("userID");
		
		Connection con=null;
		PreparedStatement pstmt=null;
		
		try
		{
			con=ConnectionUtil.getMySqlConnection();
			String sql="insert into allocatedquestions(questionid,userid) values (?,?)";
			pstmt=con.prepareStatement(sql);
			pstmt.setString(1, selectedIDs);
			pstmt.setString(2, userID);
			System.out.println(sql);
			pstmt.executeUpdate();
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	
	}
}
	

