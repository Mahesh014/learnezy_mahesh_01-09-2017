package com.itech.elearning.action;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONArray;

import com.itech.elearning.forms.ContryForm;
import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.handler.CountryHandler;



public class CountryMasterAction extends BaseAction 
{
	CountryHandler handler = new CountryHandler();
  
public ActionForward list(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
  {
	  try{
			HttpSession session=request.getSession();
			Long userid = null;
			String compid = null;
		    int roleid = 0;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
				compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
				roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {	 
				  List<ContryForm> countryList = handler.list(roleid,compid,getDataSource(request));
				  request.setAttribute("countryList",countryList);
			}else{
				return mapping.findForward("sessionfailure");
			}
			
			}catch (Exception e) {
				e.printStackTrace();
			}
		
	  return mapping.findForward("country");
  }
  
  
public ActionForward add(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
  {
	 
	  try{
			HttpSession session=request.getSession();
			Long userid = null;
			String compid = null;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {	 
               compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
				  System.out.println("Inside the add method");
				  String status=null;
				  ContryForm countryForm = (ContryForm)form;
				 
				        status = handler.add(countryForm,compid,getDataSource(request));
				        System.out.println(status);
				        request.setAttribute("status", status);
					
				  System.out.println("result "+status);


			}else{
				return mapping.findForward("sessionfailure");
			}
			
			}catch (Exception e) {
				e.printStackTrace();
			}
		
	  

	  return list(mapping, form, request, response);
  }
  
  
public ActionForward changestatus(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
  {
	 
	  
	  try{
			HttpSession session=request.getSession();
			Long userid = null;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {	 
				  ContryForm countryForm = (ContryForm)form;
				  countryForm.setCountryid(Integer.parseInt(request.getParameter("countryid").trim()));
				  countryForm.setActive(Boolean.parseBoolean(request.getParameter("active")));
				
				  System.out.println("haiiii  hhh");
				  request.setAttribute("status",handler.changeStatus(countryForm,getDataSource(request)));


			}else{
				return mapping.findForward("sessionfailure");
			}
			
			}catch (Exception e) {
				e.printStackTrace();
			}
		
	  
	  

	
	  return list(mapping, form, request, response);
  }
  
  
public ActionForward update(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
  {
	 
	  try{
			HttpSession session=request.getSession();
			Long userid = null;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {	 
				  ContryForm countryForm = (ContryForm)form;
					 
				  
			      request.setAttribute("status", handler.updateCountry(countryForm,getDataSource(request),userid));



			}else{
				return mapping.findForward("sessionfailure");
			}
			
			}catch (Exception e) {
				e.printStackTrace();
			}
		
	  
	  
	

				return list(mapping, form, request, response);
  }
  
  
 
public ActionForward ajaxActiveList(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception {
  	HttpSession session=request.getSession();
  	Long userid = null;
	try {
		userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
	} catch (NullPointerException e) {
		return mapping.findForward("session");
	}
	if (userid != null) {	 
		String countryid=request.getParameter("countryid");
		JSONArray jArray =handler.ajaxActiveList(getDataSource(request),countryid);
		 
		response.getOutputStream().write(jArray.toJSONString().getBytes());
	}else{
		return mapping.findForward("sessionfailure");
	}
	return null;
}

public ActionForward excelReport(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{

	try{
		 
		HttpSession session = request.getSession();

		Long userid = null;
		String compid = null;
		int roleid = 0;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			  List<ContryForm> countryList = handler.list(roleid,compid,getDataSource(request));
			  request.setAttribute("countryList",countryList);
			  String fileName="country_master";
			  response.setContentType("application/vnd.ms-excel");
			  response.setHeader("Content-Disposition","inline;filename="+""+fileName+".xls");
		} else{
			return mapping.findForward("session");
	}
	}catch (Exception e) {
		e.printStackTrace();
	}
return mapping.findForward("excel");

}
}
