package com.itech.elearning.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import com.itech.elearning.forms.CompanyMasterForm;
import com.itech.elearning.forms.ContryForm;
import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.ParentCompanyForm;
import com.itech.elearning.forms.RoleForm;
import com.itech.elearning.handler.CompanyHandler;
import com.itech.elearning.handler.RoleHandler;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.FileUploadDownloadUtil;

public class CompanyAction extends  BaseAction
{
	CompanyHandler handler = new CompanyHandler();
    
	public ActionForward list(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		Long userid = null;
		String compid = null;
		int roleid = 0;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
			compid = ((LoginForm) session.getAttribute("userDetail")).getCompanyId();
			roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			
			if(roleid == 1 || roleid == 9 )
			{
			List<CompanyMasterForm> CompanyList = handler.list(getDataSource(request));
			request.setAttribute("CompanyList", CompanyList);
			
			List<CompanyMasterForm> countryList = handler.ActiveCountrylist(getDataSource(request));
			request.setAttribute("countryList",countryList);
			
			List<CompanyMasterForm> ParentCompanyList = handler.ActiveParentlist(getDataSource(request));
			request.setAttribute("ParentCompanyList",ParentCompanyList);
			
			System.out.println("Country List "+countryList);
			}
			else
			{
				List<CompanyMasterForm> countryList = handler.ActiveCountrylistadd(getDataSource(request),compid);
				request.setAttribute("countryList",countryList);
				
				List<CompanyMasterForm> ParentCompanyList = handler.ActiveParentlist(getDataSource(request));
				request.setAttribute("ParentCompanyList",ParentCompanyList);
				
				
				List<CompanyMasterForm> CompanyList = handler.list1(compid,getDataSource(request));
				request.setAttribute("CompanyList", CompanyList);
				
			}
			return mapping.findForward("company");
		} else
			return mapping.findForward("session");
	}

	public ActionForward add(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		Long userid = null;
		String imagefileName="";
		boolean uploadStatus=true;
		String imageUploadPath= getServlet().getServletContext().getRealPath("/")+ "complogos";
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			CompanyMasterForm companyForm = (CompanyMasterForm) form;
			FormFile imageFile= companyForm.getImageFile();
			if(imageFile.getFileName()!="" && imageFile.getFileSize()>2){
					imagefileName=imageFile.getFileName();
					System.out.println(imagefileName);
					uploadStatus = FileUploadDownloadUtil.fileUploader(imageFile, imageUploadPath, imagefileName);
			}	
			request.setAttribute("status", handler.add(companyForm,imagefileName,getDataSource(request)));
			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");
	}

	public ActionForward changestatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			CompanyMasterForm companyForm = (CompanyMasterForm) form;
			companyForm.setCompanyID(Integer.parseInt((request.getParameter("companyID"))));
			companyForm.setActive(Boolean.parseBoolean(request.getParameter("active")));
			request.setAttribute("status", handler.changestatus(companyForm,getDataSource(request)));
			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");
	}

	public ActionForward update(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		String imagefileName="";
		boolean uploadStatus=true;
		boolean uploadPhoto=false;
		HttpSession session = request.getSession();
		String imageUploadPath= getServlet().getServletContext().getRealPath("/")+ "complogos";
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			CompanyMasterForm companyForm = (CompanyMasterForm) form;
			imagefileName=companyForm.getComplogo();
			FormFile imageFile= companyForm.getImageFile();
			if(imageFile.getFileName()!="" && imageFile.getFileSize()>2){
					imagefileName=imageFile.getFileName();
					System.out.println(imagefileName);
					uploadStatus = FileUploadDownloadUtil.fileUploader(imageFile, imageUploadPath, imagefileName);
					uploadPhoto=true;
			}	
		
			String status = handler.update(companyForm,getDataSource(request),imagefileName);
			
			if (uploadPhoto==true && status.equals(Common.No_Change)) {
				request.setAttribute("status", Common.REC_UPDATED);
			}else{
				request.setAttribute("status", status);
			}
			 
			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");
	}
	public ActionForward excelReport(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{

		try{
			
			HttpSession session = request.getSession();
			
			Long userid = null;
			String compid = null;
			try {
				userid = ((LoginForm)session.getAttribute("userDetail")).getUserId();
				compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {

				List<CompanyMasterForm> CompanyList = handler.list(getDataSource(request));
				request.setAttribute("CompanyList", CompanyList);
				String fileName="Company_master";
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition","inline;filename="+""+fileName+".xls");
			} else{
				return mapping.findForward("session");
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
	return mapping.findForward("excel");

	}
}
