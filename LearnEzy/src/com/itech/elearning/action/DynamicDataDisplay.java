package com.itech.elearning.action;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


import com.cete.dynamicpdf.text.m;
import com.google.gson.Gson;
import com.itech.elearning.forms.CityForm;
import com.itech.elearning.forms.ContactForm;
import com.itech.elearning.forms.DynamicDataForm;
import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.RoleForm;

import com.itech.elearning.handler.DynamicDataHandler;
import com.itech.elearning.handler.TaxMasterHandler;
import com.itech.elearning.utils.ConnectionUtil;
import com.itech.elearning.utils.Learnezysms;
import com.itech.elearning.utils.MailSendingUtil;
import com.redfin.sitemapgenerator.ChangeFreq;
import com.redfin.sitemapgenerator.WebSitemapGenerator;
import com.redfin.sitemapgenerator.WebSitemapUrl;
import java.io.FileWriter;
import java.io.IOException;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
public class DynamicDataDisplay extends BaseAction
{
	String eleaning="";
	String classroom="";
	String classrooms="";
    String elearnings="";
    String institutename="";
    String localities="";
    String citiys="";
    String coursesnameses="";
    String coursesnames="";
   int catid=0;
   int catid1=0;

   
   int cid=0;
   String coursename="";
   String country="";
   String coursenames="";
   String locality="";
   String citys="";
   String paids="";
   String frees="";
   String beginners="";
   String intermediates="";
   String advanceds="";
   String lessfives="";
   String greaterfives="";
   String greatertwos="";
   String greaterfivethousands="";
   
   String coursecategorys ="";	
	DynamicDataHandler handler = new DynamicDataHandler();
	TaxMasterHandler taxHandler = new TaxMasterHandler();

	
	
  public ActionForward list(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws IOException
  {
	  
	  System.out.println("Inside Action*******List");
	  String id = "1";
	  String cartid = null;
	List<DynamicDataForm> companyList = handler.list(getDataSource(request));
	request.setAttribute("companyList", companyList);
	
	List<DynamicDataForm> LatestCourseList = handler.LatestCourselist(getDataSource(request));
	request.setAttribute("LatestCourseList", LatestCourseList);
	
	List<DynamicDataForm> LatestCourseCategory = handler.LatestCourseCategory(getDataSource(request));
	request.setAttribute("LatestCourseCategory", LatestCourseCategory);
	
	System.out.println("Latest Course Details "+LatestCourseList);
	List<DynamicDataForm> cityList = handler.listcity(getDataSource(request));
	request.setAttribute("cityList",cityList);
	
	List<DynamicDataForm> courselist = handler.courselist(id,getDataSource(request));
	request.setAttribute("courselist",courselist);
	System.out.println("now "+courselist);
	System.out.println("citylist "+cityList);
	
	List<DynamicDataForm> dynamicnumbers = handler.dynamicnumbers(getDataSource(request));
	request.setAttribute("dynamicnumbers",dynamicnumbers);
	
	List<DynamicDataForm> reviewList = handler.reviewdetails(getDataSource(request));
	request.setAttribute("reviewList", reviewList);
	
	List<DynamicDataForm> popularlist = handler.populardetails(getDataSource(request));
	request.setAttribute("popularlist", popularlist);
	
	List<DynamicDataForm> recentlist = handler.recentdetails(getDataSource(request));
	request.setAttribute("recentlist",recentlist);
	
	List<DynamicDataForm> sitemaplist = handler.sitemaplist(getDataSource(request));
	request.setAttribute("sitemaplist",sitemaplist);
	
	List<DynamicDataForm> cartlist = handler.cartlist(cartid,getDataSource(request));
	request.setAttribute("cartlist",cartlist);
	
	System.out.println("review "+reviewList);
	
	return mapping.findForward("welcome");
  }
  
  
//Ajax action for Add to cart
  public ActionForward addtocart(ActionMapping mapping, ActionForm form, HttpServletRequest request,HttpServletResponse response)throws Exception {
  	
  	try {
    	DynamicDataForm roleForm = (DynamicDataForm) form;

  		System.out.println("Inside Subscribe Action");
  		String cartid=request.getParameter("courseId");
  			System.out.println("fromdate is "+cartid);
  		   
  	     //    fromDate=request.getParameter("fromdate");
  	        
  		JSONArray obj = handler.addtocart(getDataSource(request),response,cartid,request);
  			response.getOutputStream().write(obj.toString().getBytes());
  			System.out.println("inside Suhbscibr mail****************");
  		
  	}catch (Exception e) {
  		e.printStackTrace();
  	}
  	return null;
  }
  
  
  
  public ActionForward search(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
  {
	    System.out.println("search Module coding");
	    String couursename=request.getParameter("courseName");
	    System.out.println("coursename"+couursename);

	    String cityname=request.getParameter("city");
	    
	    System.out.println("cityname********"+cityname);
	    

	    String coursemode=request.getParameter("coursemode");	    
	    System.out.println("coursemode********"+coursemode);

	    String headingcityname=request.getParameter("city");
	    String cityid;
      DynamicDataForm dynaform = (DynamicDataForm) form;
	    coursename=dynaform.getCourseName();
	    cityid = dynaform.getCityid();
	    System.out.println("coursename********"+coursename);
	    
	    System.out.println("cityid********"+cityid);

	    country=dynaform.getCityid();
	  System.out.println("Testing *********"+dynaform.getCoursenamelatest());
	  System.out.println("cityname **********"+dynaform.getCityid());
	  
		List<DynamicDataForm> SearchList = handler.searchlist(couursename,cityname,coursemode,cityid,getDataSource(request),request);
	       request.setAttribute("coursenames",coursenames);
	       List<DynamicDataForm> institutelist = handler.institutelist(dynaform,getDataSource(request));
	       request.setAttribute("institutelist", institutelist);
	       List<DynamicDataForm> localitylist = handler.localitylist(dynaform,getDataSource(request));
	       request.setAttribute("localitylist", localitylist);
	       List<DynamicDataForm> citylist = handler.citylist(dynaform,getDataSource(request));
	       request.setAttribute("citylist", citylist);
	       List<DynamicDataForm> courselist = handler.courselists(dynaform,getDataSource(request));
	       request.setAttribute("courselist", courselist);
	      request.setAttribute("SearchList",SearchList);
	      request.setAttribute("coursenames",couursename);
	      
	      if((cityname!=null)&&(cityname!="")){
			request.setAttribute("city", cityname);
			 request.setAttribute("Headingcity",cityname.replace("-"," "));
	      }
	      else if((coursemode!=null)&&(coursemode!="")){
				request.setAttribute("online", coursemode);
				 request.setAttribute("Headingonline",coursemode.replace("-"," "));
		      }
	      
	  return mapping.findForward("search");
  }
  
  public ActionForward dynamiccourse(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
  {
	  String id = request.getParameter("NAME");
	  System.out.println(id);
	  
	  List<DynamicDataForm> companyList = handler.list(getDataSource(request));
		request.setAttribute("companyList", companyList);
		
		List<DynamicDataForm> LatestCourseList = handler.LatestCourselist(getDataSource(request));
		request.setAttribute("LatestCourseList", LatestCourseList);
		
		List<DynamicDataForm> LatestCourseCategory = handler.LatestCourseCategory(getDataSource(request));
		request.setAttribute("LatestCourseCategory", LatestCourseCategory);
		
		System.out.println("Latest Course Details "+LatestCourseList);
		List<DynamicDataForm> cityList = handler.listcity(getDataSource(request));
		request.setAttribute("cityList",cityList);
		
		List<DynamicDataForm> courselist = handler.courselist(id,getDataSource(request));
		request.setAttribute("courselist",courselist);
		System.out.println("now "+courselist);
		System.out.println("citylist "+cityList);
		
		List<DynamicDataForm> dynamicnumbers = handler.dynamicnumbers(getDataSource(request));
		request.setAttribute("dynamicnumbers",dynamicnumbers);
		
		
		List<DynamicDataForm> reviewList = handler.reviewdetails(getDataSource(request));
		request.setAttribute("reviewList", reviewList);
		
		List<DynamicDataForm> popularlist = handler.populardetails(getDataSource(request));
		request.setAttribute("popularlist", popularlist);
		
		List<DynamicDataForm> recentlist = handler.recentdetails(getDataSource(request));
		request.setAttribute("recentlist",recentlist);
		
		
		System.out.println("review "+reviewList);
	  
	  return mapping.findForward("welcome");
  }
  
  public ActionForward catlist(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException{

		JSONArray returnVal = new JSONArray();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
		con = ConnectionUtil.getMySqlConnection();
		String sql = "SELECT coursecatId,coursecatname FROM coursecategorymaster where active=1 ";	
		System.out.println("sql vai queary is-->"+sql);
		pstmt = con.prepareStatement(sql);
		rs = pstmt.executeQuery();
		while (rs.next()) {
			
			JSONObject obj=new JSONObject();
			obj.put("coursecatId", rs.getString("coursecatId"));
			obj.put("coursecatname", rs.getString("coursecatname"));
			returnVal.add(obj);
		}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		response.getOutputStream().write(returnVal.toJSONString().getBytes());
		return null;
	}
  
  
    public ActionForward subscribe(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) 
    {
    	   	
    	DynamicDataForm roleForm = (DynamicDataForm) form;
    	String id = "1";
    	List<DynamicDataForm> companyList = handler.list(getDataSource(request));
    	request.setAttribute("companyList", companyList);
    	
    	List<DynamicDataForm> LatestCourseList = handler.LatestCourselist(getDataSource(request));
    	request.setAttribute("LatestCourseList", LatestCourseList);
    	
    	List<DynamicDataForm> LatestCourseCategory = handler.LatestCourseCategory(getDataSource(request));
    	request.setAttribute("LatestCourseCategory", LatestCourseCategory);
    	
    	System.out.println("Latest Course Details "+LatestCourseList);
    	List<DynamicDataForm> cityList = handler.listcity(getDataSource(request));
    	request.setAttribute("cityList",cityList);
    	
    	List<DynamicDataForm> courselist = handler.courselist(id,getDataSource(request));
    	request.setAttribute("courselist",courselist);
    	System.out.println("now "+courselist);
    	System.out.println("citylist "+cityList);
    	
    	List<DynamicDataForm> dynamicnumbers = handler.dynamicnumbers(getDataSource(request));
    	request.setAttribute("dynamicnumbers",dynamicnumbers);
    	
    	List<DynamicDataForm> reviewList = handler.reviewdetails(getDataSource(request));
    	request.setAttribute("reviewList", reviewList);
    	
    	System.out.println("review "+reviewList);
    	List<DynamicDataForm> popularlist = handler.populardetails(getDataSource(request));
    	request.setAttribute("popularlist", popularlist);
    	
    	List<DynamicDataForm> recentlist = handler.recentdetails(getDataSource(request));
    	request.setAttribute("recentlist",recentlist);
    	
	    request.setAttribute("status", handler.addsubscribe(roleForm,getDataSource(request)));
    	return mapping.findForward("welcome");
    }
    
    
    public ActionForward latestcourses(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
    {
    	System.out.println("vani ");
    	List<DynamicDataForm> latestcourses = handler.latestcourses(getDataSource(request));
    	request.setAttribute("latestcourses", latestcourses);
    	System.out.println("latest courses "+latestcourses);
      	List<DynamicDataForm> dynamicnumberslatest = handler.dynamicnumberslatest(getDataSource(request));
    	request.setAttribute("dynamicnumberslatest",dynamicnumberslatest);
    	return mapping.findForward("latest");
    }
    
    public ActionForward serachLatest(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)
    {
    	DynamicDataForm dynamicdataform = (DynamicDataForm)form;


    	List<DynamicDataForm> latestcourses = handler.latestcoursessearch(dynamicdataform,getDataSource(request));
    	request.setAttribute("latestcourses", latestcourses);
    	
    	List<DynamicDataForm> dynamicnumberslatest = handler.dynamicnumberslatest(getDataSource(request));
    	request.setAttribute("dynamicnumberslatest",dynamicnumberslatest);


    	return mapping.findForward("latest");
    }
    
    public ActionForward searchelearning(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
    {
    	DynamicDataForm dynamicdataform = (DynamicDataForm)form;
    	System.out.println("search learning ");
    	List<DynamicDataForm> latestcourses = handler.searchelearningcourse(getDataSource(request));
    	request.setAttribute("latestcourses", latestcourses);
    	List<DynamicDataForm> dynamicnumberslatest = handler.dynamicnumberslatest(getDataSource(request));
    	request.setAttribute("dynamicnumberslatest",dynamicnumberslatest);
    	return mapping.findForward("latest");
    }
    public ActionForward selectingcourses(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws MalformedURLException
    {
    	
    	
        URL url = new URL(request.getRequestURL().toString());
        System.out.println("urllllllllllllllll"+url);

    	
    	
    	System.out.println("inside the selecting courses action");
    	String category="";
    	String cities="";
    	String institutenames="";
    	String Categorynames="";
    	String localitynames="";
    	String ugname="";
    	String ugnames="";
    	String urlinstitue=null;
    	String urlcity=null;
    	String urlloaction=null;
    	String urlcategory=null;
    	String urlcourse=null;
    	String urlelearning=null;
    	String urlclassroom=null;
    	String coursename="";
    	 
    	String[] coursecategory = new String[10];
    	String[] locality = new String[10];
    	String[] city = new String[10];
String testingstring="";
urlinstitue=request.getParameter("institutename");
 urlcity=request.getParameter("city");
 urlloaction=request.getParameter("locality");
 urlcategory=request.getParameter("coursecategory");
 urlcourse=request.getParameter("course");
 urlelearning=request.getParameter("online");
 urlclassroom=request.getParameter("classroomms");
System.out.println("urlelearning"+urlelearning);
System.out.println("classrooms"+urlclassroom);
System.out.println("urlcategory"+urlcategory);

System.out.println("urlcity"+urlcity);
System.out.println("urlcoursesssssssssssssssssssssssssssssssssssssssssssssss"+urlcourse);
System.out.println("urlinsssssssssssssssssssssssssssssssssssssssssssssssss"+urlinstitue);

System.out.println("urllocation"+urlloaction);
institutenames=request.getParameter("appendinstitutevalue");
    	institutename=request.getParameter("appendinstitutevalue");
localitynames=request.getParameter("appendlocalityname");
localities=request.getParameter("appendlocalityname");
cities=request.getParameter("appendcityname");
citiys=request.getParameter("appendcityname");
coursename=request.getParameter("hiddenappendcoursesname");
coursesnames=request.getParameter("hiddenappendcoursesname");
System.out.println("courseeeeeeeeeeeeeeeeeeeeeeee"+coursename);
System.out.println("institutename"+institutenames);
System.out.println("localitynames"+localitynames);
System.out.println("cities"+cities);
System.out.println("coursename"+coursename);



    	System.out.println("parthi ");
    	 eleaning=request.getParameter("elearning");
    	 System.out.println("elearningexamples"+eleaning);
    	 elearnings=request.getParameter("elearning");
    	 String classroom=request.getParameter("classroom");
    	 classrooms=request.getParameter("classroom");
    	 String appendcourse=request.getParameter("appendclassroomname");
    	 System.out.println("classroomsnameeeeeeeeeeeeeeee"+classroom);
    	 String paid=request.getParameter("paid");
    	 paids=request.getParameter("paid");
    	 System.out.println("paid"+paid);
    	 String free=request.getParameter("free");
    	 frees=request.getParameter("free");

    	 String beginner=request.getParameter("beginner");
    	 beginners=request.getParameter("beginner");
    	 String intermediate=request.getParameter("intermediate");
    	 intermediates=request.getParameter("intermediate");
    	 String advanced=request.getParameter("advanced");
    	 advanceds=request.getParameter("advanced");
    	 String lessfive=request.getParameter("lessthanfive");
    	 lessfives=request.getParameter("lessthanfive");
    	 String graterfive=request.getParameter("greaterfive");
    	 greaterfives=request.getParameter("greaterfive");
    	 String gratertwo=request.getParameter("greatertwot");
    	 greatertwos=request.getParameter("greatertwot");
    	 String greaterfivethousand=request.getParameter("greaterthanfiveth");
    	 greaterfivethousands=request.getParameter("greaterthanfiveth");

			 System.out.println("coursecategorys"+coursecategorys);
		    	
		    	Categorynames=request.getParameter("appendcoursecatnames");
		    	
		    	coursecategorys=request.getParameter("appendcoursecatnames");
    	 
    	 System.out.println("free"+free);
    	System.out.println("eleanrn"+eleaning);
    	
    
	    DynamicDataForm dynaform = (DynamicDataForm) form;

    	 List<DynamicDataForm> institutelist = handler.institutelist(dynaform,getDataSource(request));
	       request.setAttribute("institutelist", institutelist);
	       List<DynamicDataForm> localitylist = handler.localitylist(dynaform,getDataSource(request));
	       request.setAttribute("localitylist", localitylist);
	       List<DynamicDataForm> citylist = handler.citylist(dynaform,getDataSource(request));
	       request.setAttribute("citylist", citylist);
	       List<DynamicDataForm> courselist = handler.courselists(dynaform,getDataSource(request));
	       request.setAttribute("courselist", courselist);
	       
	      
	   	List<DynamicDataForm> latestcourses = handler.selectingcourses(getDataSource(request),eleaning,classroom,paid,free,beginner,intermediate,advanced,lessfive,graterfive,gratertwo,greaterfivethousand,Categorynames,institutenames,localitynames,cities,coursename);
    	request.setAttribute("latestcourses", latestcourses);
    	
	    	
    	List<DynamicDataForm> dynamicnumberslatest = handler.dynamicnumberslatest(getDataSource(request));
    	request.setAttribute("dynamicnumberslatest",dynamicnumberslatest);
    	
    	if(urlinstitue!=null && urlcity!=null && urlloaction!=null && urlcategory==null && urlcourse==null &&!eleaning.equals("1") &&!appendcourse.equals("1"))
        {
        	System.out.println("condition for ins loc and city");
        	request.setAttribute("institutenames", urlinstitue);
        	 
        	
        	request.setAttribute("citys", urlcity);
        	request.setAttribute("location", urlloaction);
        	

System.out.println("insnamessssss"+urlinstitue);
        	
        }
    	else if(urlinstitue!=null && urlcity==null && urlloaction==null && urlcategory!=null && urlcourse==null &&!eleaning.equals("1") &&!appendcourse.equals("1"))
        {
        	System.out.println("category and  institute");
    		request.setAttribute("categoryname", urlcategory);
    		request.setAttribute("institutenamess", urlinstitue);

        	
        }
    	else if(urlinstitue!=null && urlcity!=null && urlloaction==null && urlcategory!=null && urlcourse==null &&!eleaning.equals("1") &&!appendcourse.equals("1"))
        {
        	System.out.println("category and  institute and city");
    		request.setAttribute("categoryname", urlcategory);
    		System.out.println("categoryname"+urlcategory);
    		request.setAttribute("institutenamesss", urlinstitue);
    		System.out.println("urlinstitue"+urlinstitue);

    		request.setAttribute("citiess", urlcity.replace(">", ""));
    		System.out.println("urlcity"+urlcity);


        	
        }
    	
    	else if(urlinstitue!=null && urlcity==null && urlloaction==null && urlcategory==null && urlcourse!=null &&!eleaning.equals("1") &&!appendcourse.equals("1"))
        {
    		request.setAttribute("institutenamesssss", urlinstitue);
    		request.setAttribute("coursenames", urlcourse);



        	
        }
    	else if(urlinstitue==null && urlcity!=null && urlloaction==null && urlcategory==null && urlcourse!=null &&!eleaning.equals("1") &&!appendcourse.equals("1"))
        {
    		request.setAttribute("coursescityname", urlcity);
    		request.setAttribute("coursenames", urlcourse);


        	
        }
    	else if(urlinstitue==null && urlcity!=null && urlloaction!=null && urlcategory==null && urlcourse!=null &&!eleaning.equals("1") &&!appendcourse.equals("1"))
        {
    		
    		request.setAttribute("citynamecoursearea", urlcity);
    		request.setAttribute("coursenames", urlcourse);
    		request.setAttribute("areaname", urlloaction);
    		// If you need gzipped output
    	    

  

        	
        }
    	else if(urlinstitue==null && urlcity!=null && urlloaction==null && urlcategory==null && urlcourse!=null &&eleaning.equals("1") &&!appendcourse.equals("1"))
        {
    		System.out.println("inside city elearning and course");
    		request.setAttribute("elearning", "online");
    		
    		request.setAttribute("elearningcity", urlcity);
    		request.setAttribute("coursenames", urlcourse);
    	}
    	else if(urlinstitue==null && urlcity!=null && urlloaction==null && urlcategory==null && urlcourse!=null &&!eleaning.equals("1") &&appendcourse.equals("1"))
        {
    		
    		request.setAttribute("classroom", "classroom");    		
    		request.setAttribute("elearningcity", urlcity);
    		request.setAttribute("coursenames", urlcourse);
    	}

    	else if(urlinstitue==null && urlcity!=null && urlloaction!=null && urlcategory==null && urlcourse!=null &&eleaning.equals("1") &&!appendcourse.equals("1"))
        {
    		String spitstring[]=urlcity.split("-");

    		request.setAttribute("elearninglocation", "online");
    		System.out.println("eeeeeurlloaction"+urlloaction);
    		request.setAttribute("elearningcitylocation", spitstring[1]);
    		request.setAttribute("coursenames", urlcourse);
    		request.setAttribute("areaname",spitstring[0]);

    	}
    	else if(urlinstitue==null && urlcity!=null && urlloaction!=null && urlcategory==null && urlcourse!=null &&!eleaning.equals("1") &&appendcourse.equals("1"))
        {
    		
    		request.setAttribute("classroomlocation", "classroom");    
    		String spitstring[]=urlcity.split("-");

    		request.setAttribute("elearningcitylocation", spitstring[1]);
    		
    		
    		System.out.println("urlcity"+spitstring[1]);
    		System.out.println("urlcourse"+urlcourse);

    		request.setAttribute("coursenames", urlcourse);
    		request.setAttribute("areaname", spitstring[0]);
    		System.out.println("urlloaction"+urlloaction);

    	}

    	else         {
    		if(urlcity!=null)
    			
    		{
    		String spitstring[]=urlcity.split("_");

    		System.out.println("category and institute locality and city");
    		request.setAttribute("categoryname", urlcategory);
    		request.setAttribute("institutenamessss", urlinstitue);
    		request.setAttribute("citiess", spitstring[1].replace(">", ""));
    		request.setAttribute("areaname", spitstring[0].replace(">", ""));
    		}

        	
        }








    	
    	
    	return mapping.findForward("selectlatestcourses");
    }
    public ActionForward selectcoursecategory(ActionMapping mapping, ActionForm form, HttpServletRequest request,HttpServletResponse response)throws Exception {
		
		try {

		       System.out.println("In userIddddddddddddddddddd");
				JSONArray obj = handler.selectcoursecategory(getDataSource(request),request,response);
				response.getOutputStream().write(obj.toString().getBytes());

				
				} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

  
	 }
    public ActionForward coursesearch(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
    {
  	    System.out.println("search Module coding");
  	    
  	    DynamicDataForm dynaform = (DynamicDataForm) form;
  	    //System.out.println(request.getParameter("course"));
  	  System.out.println("Testing "+dynaform.getCourseName());
  	  System.out.println("cityname "+dynaform.getCityname());
  	coursenames=dynaform.getCourseName();
  	locality=dynaform.getLocality();
  	citys=dynaform.getCity();
  	request.setAttribute("coursenames", coursenames);
  	request.setAttribute("locality", locality);
  	request.setAttribute("city", citys);


  	
  		List<DynamicDataForm> SearchList = handler.coursesearch(dynaform,getDataSource(request),request);
  	    request.setAttribute("latestcourses",SearchList);
  	    System.out.println("Search List "+SearchList);
  	  return mapping.findForward("searchcourse");
    }
    
    public ActionForward coursesdescription(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
    {
  	    System.out.println("search Module coding");
  	    String courseename=request.getParameter("coursename");
  	    String institute=request.getParameter("institute");
	    
  	    int courseid=Integer.parseInt(request.getParameter("id"));
  	    int coursecatid=Integer.parseInt(request.getParameter("catid"));
  	    
  	    System.out.println("****************courseid"+courseid);
  	  System.out.println("******************coursecatid"+coursecatid);
  	  
  	  catid=courseid;
  	cid=coursecatid;
  	    DynamicDataForm dynaform = (DynamicDataForm) form;
  	  List<DynamicDataForm> coursedesc = handler.coursesdescription(dynaform,getDataSource(request),request,courseid);
  	    request.setAttribute("coursedesc",coursedesc);
  	  
  	  List<DynamicDataForm> releatedcourse = handler.releatedcourse(dynaform,getDataSource(request),request,courseid,coursecatid);
	    request.setAttribute("releatedcourse",releatedcourse);
	    List<DynamicDataForm> coursecategory = handler.coursecategory(dynaform,getDataSource(request),request,courseid,coursecatid);
	    request.setAttribute("coursecategory",coursecategory);
	    request.setAttribute("coursename",courseename);
	    request.setAttribute("institute",institute);

  	    System.out.println("Search List "+coursedesc);
  	  return mapping.findForward("coursesdescription");
    }
    public ActionForward loginfailurecoursesdescription(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
    {

  	    System.out.println("search Module coding");
  	    int courseid=catid;
  	    System.out.println("courseid"+courseid);
  	    int coursecatid=cid;
  	    System.out.println("coursecatid"+coursecatid);

  	     DynamicDataForm dynaform = (DynamicDataForm) form;
  	  List<DynamicDataForm> coursedesc = handler.coursesdescription(dynaform,getDataSource(request),request,courseid);
  	    request.setAttribute("coursedesc",coursedesc);
  	  
  	  List<DynamicDataForm> releatedcourse = handler.releatedcourse(dynaform,getDataSource(request),request,courseid,coursecatid);
	    request.setAttribute("releatedcourse",releatedcourse);
	    List<DynamicDataForm> coursecategory = handler.coursecategory(dynaform,getDataSource(request),request,courseid,coursecatid);
	    request.setAttribute("coursecategory",coursecategory);
  	    System.out.println("Search List "+coursedesc);
		String  status = "Username or password is Invalid";
		 request.setAttribute("status", status);
  	  return mapping.findForward("coursesdescription");
    }    
    
    public ActionForward latestcoursesdescription(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
    {
    int catid=Integer.parseInt(request.getParameter("catid"));
    catid1=Integer.parseInt(request.getParameter("catid"));
    	List<DynamicDataForm> latestcourses = handler.latestcoursesdescription(getDataSource(request),catid);
    	request.setAttribute("latestcourses", latestcourses);
    	System.out.println("latest courses "+latestcourses);
      	List<DynamicDataForm> dynamicnumberslatest = handler.dynamicnumberslatest(getDataSource(request));
    	request.setAttribute("dynamicnumberslatest",dynamicnumberslatest);
    	return mapping.findForward("latestcoursesdescription");
    }
    public ActionForward latestcourseratewise(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
    {
    	int catid=catid1;
    	List<DynamicDataForm> latestcourses = handler.latestcourseratewise(getDataSource(request),catid);
    	request.setAttribute("latestcourses", latestcourses);
    	System.out.println("latest courses "+latestcourses);
      	List<DynamicDataForm> dynamicnumberslatest = handler.dynamicnumberslatest(getDataSource(request));
    	request.setAttribute("dynamicnumberslatest",dynamicnumberslatest);
    	return mapping.findForward("latestcoursesdescription");
    }
    public ActionForward latestcourserpricelowtohigh(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
    {
    int catid=catid1;
    	List<DynamicDataForm> latestcourses = handler.latestcourserpricelowtohigh(getDataSource(request),catid);
    	request.setAttribute("latestcourses", latestcourses);
    	System.out.println("latest courses "+latestcourses);
      	List<DynamicDataForm> dynamicnumberslatest = handler.dynamicnumberslatest(getDataSource(request));
    	request.setAttribute("dynamicnumberslatest",dynamicnumberslatest);
    	return mapping.findForward("latestcoursesdescription");
    }
    public ActionForward latestcourserpricehightolow(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
    {
    int catid=catid1;
    
    
    	List<DynamicDataForm> latestcourses = handler.latestcourserpricehightolow(getDataSource(request),catid);
    	request.setAttribute("latestcourses", latestcourses);
    	System.out.println("latest courses "+latestcourses);
      	List<DynamicDataForm> dynamicnumberslatest = handler.dynamicnumberslatest(getDataSource(request));
    	request.setAttribute("dynamicnumberslatest",dynamicnumberslatest);
    	return mapping.findForward("latestcoursesdescription");
    }
 
   public ActionForward add(ActionMapping mapping,ActionForm form,HttpServletRequest request, HttpServletResponse response ) throws Exception  {
			  String course=request.getParameter("courses");
			  String price=request.getParameter("courseprice");
			  String email=request.getParameter("emailid");
			  String couseid=request.getParameter("corseidss");
			  System.out.println("course"+course);
			  System.out.println("price"+price);


    	
    	DynamicDataForm dynaform = (DynamicDataForm) form;
			   int courseid=catid;
		  	    int coursecatid=cid;
		  	    String status=null;
		  	  List<DynamicDataForm> coursedesc = handler.coursesdescription(dynaform,getDataSource(request),request,courseid);
		  	    request.setAttribute("coursedesc",coursedesc);
		  	  List<DynamicDataForm> releatedcourse = handler.releatedcourse(dynaform,getDataSource(request),request,courseid,coursecatid);
			    request.setAttribute("releatedcourse",releatedcourse);
			    List<DynamicDataForm> coursecategory = handler.coursecategory(dynaform,getDataSource(request),request,courseid,coursecatid);
			    request.setAttribute("coursecategory",coursecategory);
			    
			    HashMap<String, String> globasetting = taxHandler.globasetting(getDataSource(request));
				request.setAttribute("globasetting", globasetting);
				String serviceTax=globasetting.get("service tax").toString();
				request.setAttribute("serviceTax", serviceTax);
		  	    System.out.println("Search List "+coursedesc);
		  	  status=handler.add(dynaform,getDataSource(request));
			   request.setAttribute("status",status);
			   request.setAttribute("course",course);
			   request.setAttribute("price",price);
				  request.setAttribute("emailid",email);
				  request.setAttribute("couseid",couseid);


			  

		  	  if(status.equalsIgnoreCase("Added successfully"))
		  	  {
		  		 return mapping.findForward("sucesshomepageregister");
		  	  }
		  	  else
		  	  {
		   return mapping.findForward("homepageregister");
		  	  }
	   }
    
public ActionForward elearningcountss(ActionMapping mapping, ActionForm form, HttpServletRequest request,HttpServletResponse response)throws Exception {
		
		try {

		       System.out.println("In userIddddddddddddddddddd");
				JSONArray obj = handler.elearningcount(getDataSource(request),request,response);
				response.getOutputStream().write(obj.toString().getBytes());

				
				} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

  }
public ActionForward getcity(ActionMapping mapping, ActionForm form, HttpServletRequest request,HttpServletResponse response)throws Exception {
	
	try {

	       System.out.println("In userIddddddddddddddddddd");
			JSONArray obj = handler.city(getDataSource(request),request,response);
			response.getOutputStream().write(obj.toString().getBytes());

			
			} catch (Exception e) {
		e.printStackTrace();
	}
	return null;

}






public ActionForward classroomcounts(ActionMapping mapping, ActionForm form, HttpServletRequest request,HttpServletResponse response)throws Exception {
	
	try {

	       System.out.println("In userIddddddddddddddddddd");
			JSONArray obj = handler.classroomcounts(getDataSource(request),request,response);
			response.getOutputStream().write(obj.toString().getBytes());

			
			} catch (Exception e) {
		e.printStackTrace();
	}
	return null;

}
public ActionForward freecounts(ActionMapping mapping, ActionForm form, HttpServletRequest request,HttpServletResponse response)throws Exception {
	
	try {

	       System.out.println("In userIddddddddddddddddddd");
			JSONArray obj = handler.freecounts(getDataSource(request),request,response);
			response.getOutputStream().write(obj.toString().getBytes());

			
			} catch (Exception e) {
		e.printStackTrace();
	}
	return null;

}
public ActionForward beginnercounts(ActionMapping mapping, ActionForm form, HttpServletRequest request,HttpServletResponse response)throws Exception {
	
	try {

	       System.out.println("In userIddddddddddddddddddd");
			JSONArray obj = handler.beginnercounts(getDataSource(request),request,response);
			response.getOutputStream().write(obj.toString().getBytes());

			
			} catch (Exception e) {
		e.printStackTrace();
	}
	return null;

}
public ActionForward advancedcounts(ActionMapping mapping, ActionForm form, HttpServletRequest request,HttpServletResponse response)throws Exception {
	
	try {

	       System.out.println("In userIddddddddddddddddddd");
			JSONArray obj = handler.advancedcounts(getDataSource(request),request,response);
			response.getOutputStream().write(obj.toString().getBytes());

			
			} catch (Exception e) {
		e.printStackTrace();
	}
	return null;

}



public ActionForward intermediatecounts(ActionMapping mapping, ActionForm form, HttpServletRequest request,HttpServletResponse response)throws Exception {
	
	try {

	       System.out.println("In userIddddddddddddddddddd");
			JSONArray obj = handler.intermediatecounts(getDataSource(request),request,response);
			response.getOutputStream().write(obj.toString().getBytes());

			
			} catch (Exception e) {
		e.printStackTrace();
	}
	return null;

}
public ActionForward paidcounts(ActionMapping mapping, ActionForm form, HttpServletRequest request,HttpServletResponse response)throws Exception {
	
	try {

	       System.out.println("In userIddddddddddddddddddd");
			JSONArray obj = handler.paidcounts(getDataSource(request),request,response);
			response.getOutputStream().write(obj.toString().getBytes());

			
			} catch (Exception e) {
		e.printStackTrace();
	}
	return null;

}



public ActionForward pricelowtohigh(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
{
	    System.out.println("price low to high");
	    
	    DynamicDataForm dynaform = (DynamicDataForm) form;
	    
	    List<DynamicDataForm> institutelist = handler.institutelist(dynaform,getDataSource(request));
	       request.setAttribute("institutelist", institutelist);
	       List<DynamicDataForm> localitylist = handler.localitylist(dynaform,getDataSource(request));
	       request.setAttribute("localitylist", localitylist);
	       List<DynamicDataForm> citylist = handler.citylist(dynaform,getDataSource(request));
	       request.setAttribute("citylist", citylist);
	       List<DynamicDataForm> courselist = handler.courselists(dynaform,getDataSource(request));
	       request.setAttribute("courselist", courselist);
	    
	    
	    
	    
	    //System.out.println(request.getParameter("course"));
	 String coursenames=coursename;
	 System.out.println("coursename"+coursenames);
	 String countrys=country;
	 System.out.println("countrys"+countrys);

		List<DynamicDataForm> SearchList = handler.pricelowtohigh(dynaform,getDataSource(request),coursenames,countrys);
		request.setAttribute("SearchList",SearchList);
	    System.out.println("Search List "+SearchList);
	  return mapping.findForward("search");
}

public ActionForward pricehightolow(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
{
	    System.out.println("price low to high");
	    
	    DynamicDataForm dynaform = (DynamicDataForm) form;
	    
	    
	    List<DynamicDataForm> institutelist = handler.institutelist(dynaform,getDataSource(request));
	       request.setAttribute("institutelist", institutelist);
	       List<DynamicDataForm> localitylist = handler.localitylist(dynaform,getDataSource(request));
	       request.setAttribute("localitylist", localitylist);
	       List<DynamicDataForm> citylist = handler.citylist(dynaform,getDataSource(request));
	       request.setAttribute("citylist", citylist);
	       List<DynamicDataForm> courselist = handler.courselists(dynaform,getDataSource(request));
	       request.setAttribute("courselist", courselist);
	    
	    
	    //System.out.println(request.getParameter("course"));
	 String coursenames=coursename;
	 System.out.println("coursename"+coursenames);
	 String countrys=country;
	 System.out.println("countrys"+countrys);

		List<DynamicDataForm> SearchList = handler.pricehightolow(dynaform,getDataSource(request),coursenames,countrys);
		request.setAttribute("SearchList",SearchList);
	    System.out.println("Search List "+SearchList);
	  return mapping.findForward("search");
}

public ActionForward rating(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
{
	    System.out.println("price low to high");
	    
	    DynamicDataForm dynaform = (DynamicDataForm) form;
	    //System.out.println(request.getParameter("course"));
	 String coursenames=coursename;
	 System.out.println("coursename"+coursenames);
	 String countrys=country;
	 System.out.println("countrys"+countrys);

		List<DynamicDataForm> SearchList = handler.rating(dynaform,getDataSource(request),coursenames,countrys);
		request.setAttribute("SearchList",SearchList);
	    System.out.println("Search List "+SearchList);
	  return mapping.findForward("search");
}
public ActionForward coursesearchresult(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
{
	    System.out.println("search Module coding");
	    
	    DynamicDataForm dynaform = (DynamicDataForm) form;
	    //System.out.println(request.getParameter("course"));
	  System.out.println("Testing "+dynaform.getCourseName());
	  System.out.println("cityname "+dynaform.getCityname());
		List<DynamicDataForm> SearchList = handler.coursesearch(dynaform,getDataSource(request),request);
	    request.setAttribute("SearchList",SearchList);
	    System.out.println("Search List "+SearchList);
	  return mapping.findForward("search");
}
public ActionForward pricehightolowsearchagain(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
{
	    System.out.println("search Module coding");
	    String coursename=coursenames;
	    String localitys=locality;
	    String city=citys;
	    DynamicDataForm dynaform = (DynamicDataForm) form;
	   List<DynamicDataForm> SearchList = handler.pricehightolowsearchagain(dynaform,getDataSource(request),request,coursename,localitys,city);
	    request.setAttribute("latestcourses",SearchList);
	    System.out.println("Search List "+SearchList);
	  return mapping.findForward("searchcourse");
}
public ActionForward pricelowtohighsearchagain(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
{
	    System.out.println("search Module coding");
	    String coursename=coursenames;
	    String localitys=locality;
	    String city=citys;
	    DynamicDataForm dynaform = (DynamicDataForm) form;
	   List<DynamicDataForm> SearchList = handler.pricelowtohighsearchagain(dynaform,getDataSource(request),request,coursename,localitys,city);
	    request.setAttribute("latestcourses",SearchList);
	    System.out.println("Search List "+SearchList);
	  return mapping.findForward("searchcourse");
}

public ActionForward ratewisesearchagain(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
{
	    System.out.println("search Module coding");
	    String coursename=coursenames;
	    String localitys=locality;
	    String city=citys;
	    DynamicDataForm dynaform = (DynamicDataForm) form;
	   List<DynamicDataForm> SearchList = handler.ratewisesearchagain(dynaform,getDataSource(request),request,coursename,localitys,city);
	    request.setAttribute("latestcourses",SearchList);
	    System.out.println("Search List "+SearchList);
	  return mapping.findForward("searchcourse");
}
public ActionForward pricehightolowlatestcourses(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
{
	System.out.println("vani ");
	List<DynamicDataForm> latestcourses = handler.pricehightolowlatestcourses(getDataSource(request));
	request.setAttribute("latestcourses", latestcourses);
	System.out.println("latest courses "+latestcourses);
  	List<DynamicDataForm> dynamicnumberslatest = handler.dynamicnumberslatest(getDataSource(request));
	request.setAttribute("dynamicnumberslatest",dynamicnumberslatest);
	return mapping.findForward("latest");
}

public ActionForward pricelowtohighlatestcourses(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
{
	System.out.println("vani ");
	List<DynamicDataForm> latestcourses = handler.pricelowtohighlatestcourses(getDataSource(request));
	request.setAttribute("latestcourses", latestcourses);
	System.out.println("latest courses "+latestcourses);
  	List<DynamicDataForm> dynamicnumberslatest = handler.dynamicnumberslatest(getDataSource(request));
	request.setAttribute("dynamicnumberslatest",dynamicnumberslatest);
	return mapping.findForward("latest");
}
public ActionForward ratewiselatestcourses(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
{
	System.out.println("vani ");
	List<DynamicDataForm> latestcourses = handler.ratewiselatestcourses(getDataSource(request));
	request.setAttribute("latestcourses", latestcourses);
	System.out.println("latest courses "+latestcourses);
  	List<DynamicDataForm> dynamicnumberslatest = handler.dynamicnumberslatest(getDataSource(request));
	request.setAttribute("dynamicnumberslatest",dynamicnumberslatest);
	return mapping.findForward("latest");
}
public ActionForward pricelowtohighselectingcourses(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
{
	String category="";
	String institutename="";
	String Categorynames="";
	String ugname="";
	String ugnames="";
	String[] coursecategory = new String[10];
	System.out.println("parthi ");
	 eleaning=elearnings;
	 String classroom=classrooms;
	 String paid=paids;
	 System.out.println("paid"+paid);
	 String free=frees;
	 String beginner=beginners;
	 String intermediate=intermediates;
	 String advanced=advanceds;
	 String lessfive=lessfives;
	 String graterfive=greaterfives;
	 String gratertwo=greatertwos;
	 String greaterfivethousand=greaterfivethousands;

 	Categorynames=coursecategorys;

	 
	 System.out.println("free"+free);
	System.out.println("eleanrn"+eleaning);
	List<DynamicDataForm> latestcourses = handler.pricelowtohighselectingcourses(getDataSource(request),eleaning,classroom,paid,free,beginner,intermediate,advanced,lessfive,graterfive,gratertwo,greaterfivethousand,Categorynames);
	request.setAttribute("latestcourses", latestcourses);
	List<DynamicDataForm> dynamicnumberslatest = handler.dynamicnumberslatest(getDataSource(request));
	request.setAttribute("dynamicnumberslatest",dynamicnumberslatest);
	
	
	return mapping.findForward("selectlatestcourses");
}
public ActionForward pricehightolowselectingcourses(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
{
	String category="";
	String Categorynames="";
	String ugname="";
	String ugnames="";
	String coursecategory = "";
	System.out.println("parthi ");
	 eleaning=elearnings;
	 String classroom=classrooms;
	 String paid=paids;
	 System.out.println("paid"+paid);
	 String free=frees;
	 String beginner=beginners;
	 String intermediate=intermediates;
	 String advanced=advanceds;
	 String lessfive=lessfives;
	 String graterfive=greaterfives;
	 String gratertwo=greatertwos;
	 String greaterfivethousand=greaterfivethousands;

		 coursecategory=coursecategorys;
		 	
	    	Categorynames=coursecategorys;
	    	

	 
	 System.out.println("free"+free);
	System.out.println("eleanrn"+eleaning);
	List<DynamicDataForm> latestcourses = handler.pricehightolowselectingcourses(getDataSource(request),eleaning,classroom,paid,free,beginner,intermediate,advanced,lessfive,graterfive,gratertwo,greaterfivethousand,Categorynames);
	request.setAttribute("latestcourses", latestcourses);
	List<DynamicDataForm> dynamicnumberslatest = handler.dynamicnumberslatest(getDataSource(request));
	request.setAttribute("dynamicnumberslatest",dynamicnumberslatest);
	
	
	return mapping.findForward("selectlatestcourses");
}

public ActionForward ratewiseselectingcourses(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
{
	String category="";
	String Categorynames="";
	String ugname="";
	String ugnames="";
	String[] coursecategory = new String[10];
	System.out.println("parthi ");
	 eleaning=elearnings;
	 String classroom=classrooms;
	 String paid=paids;
	 System.out.println("paid"+paid);
	 String free=frees;
	 String beginner=beginners;
	 String intermediate=intermediates;
	 String advanced=advanceds;
	 String lessfive=lessfives;
	 String graterfive=greaterfives;
	 String gratertwo=greatertwos;
	 String greaterfivethousand=greaterfivethousands;

 	Categorynames=coursecategorys;


	 
	 System.out.println("free"+free);
	System.out.println("eleanrn"+eleaning);
	List<DynamicDataForm> latestcourses = handler.priceratewiseselectingcourses(getDataSource(request),eleaning,classroom,paid,free,beginner,intermediate,advanced,lessfive,graterfive,gratertwo,greaterfivethousand,Categorynames);
	request.setAttribute("latestcourses", latestcourses);
	List<DynamicDataForm> dynamicnumberslatest = handler.dynamicnumberslatest(getDataSource(request));
	request.setAttribute("dynamicnumberslatest",dynamicnumberslatest);
	
	
	return mapping.findForward("selectlatestcourses");
}
//public ActionForward enquiry1(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws IOException
//{
//	System.out.println("hiiiiiiiiiiiiiiii");
//	String txtname=request.getParameter("txtname");
//	String txtemail=request.getParameter("txtemail");
//	String contactnumber=request.getParameter("contactnumber");
//	String message=request.getParameter("textarea-message");
//	Learnezysms.quickenquiry(txtname,txtemail,message,contactnumber);
//	response.sendRedirect("sendmailcommonfooter.jsp?txtname="+txtname+"&txtemail="+txtemail+"&contactnumber="+contactnumber+"&message="+message+"");
//	return null;
//	}

public ActionForward enquiry1(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws IOException
{
	DynamicDataForm roleForm = (DynamicDataForm) form;

	System.out.println("hiiiiiiiiiiiiiiii");
	String txtname=request.getParameter("txtname");
	String txtemail=request.getParameter("txtemail");
	System.out.println("email****************"+txtemail);
	String contactnumber=request.getParameter("contactnumber");
	String message=request.getParameter("textarea-message");
	System.out.println("************Mymsg"+message);	
    request.setAttribute("status", handler.addsubscribe2(txtname,txtemail,contactnumber,message,getDataSource(request)));
	Learnezysms.quickenquiry(txtname,txtemail,message,contactnumber);
	response.sendRedirect("sendmailcommonfooter.jsp?txtname="+txtname+"&txtemail="+txtemail+"&contactnumber="+contactnumber+"&message="+message+"");
	MailSendingUtil.footerenquiryMail(txtemail);
	return null;
}







public ActionForward enquiry(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws IOException
{
	String course=request.getParameter("course");
	String firstname=request.getParameter("firstname");
	String lastname=request.getParameter("lastname");
	String email=request.getParameter("email");
	String mobilenumber=request.getParameter("mobilenumber");
	String city=request.getParameter("city");
	String gender=request.getParameter("gender");
	String level=request.getParameter("level");
	String age=request.getParameter("age");
	String message=request.getParameter("message");
	Learnezysms.sendenquirysms(course,firstname,email,mobilenumber);
	MailSendingUtil.enquiryMail(email);
	
	response.sendRedirect("mail.jsp?course="+course+"&firstname="+firstname+"&lastname="+lastname+"&email="+email+"&mobilenumber="+mobilenumber+"&city="+city+"&gender="+gender+"&level="+level+"&age="+age+"&message="+message+"");
	return null;
	}



//public ActionForward enquiry(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws IOException
//{
//	String course=request.getParameter("course");
//	String firstname=request.getParameter("firstname");
//	String lastname=request.getParameter("lastname");
//	String email=request.getParameter("email");
//	String mobilenumber=request.getParameter("mobilenumber");
//	String city=request.getParameter("city");
//	String gender=request.getParameter("gender");
//	String level=request.getParameter("level");
//	String age=request.getParameter("age");
//	String message=request.getParameter("message");
//	Learnezysms.sendenquirysms(course,firstname,email,mobilenumber);
//	MailSendingUtil.enquiryMail(email);
//	
//	
//	
//	response.sendRedirect("mail.jsp?course="+course+"&firstname="+firstname+"&lastname="+lastname+"&email="+email+"&mobilenumber="+mobilenumber+"&city="+city+"&gender="+gender+"&level="+level+"&age="+age+"&message="+message+"");
//	return null;
//	}

//public ActionForward contact(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws IOException
//{
//	
//	
//	String name=request.getParameter("name");
//
//	String email1=request.getParameter("Email");
//
//	String contact=request.getParameter("contct");
//
//	String subject=request.getParameter("subject");
//	String message2=request.getParameter("textareamessage");
//	
//	MailSendingUtil.contactMail(email1);
//	
//	Learnezysms.sendcontactsms(name,email1,contact,subject,message2);
//
//	response.sendRedirect("Contactmail.jsp?name="+name+"&Email="+email1+"&Contact="+contact+"&subject="+subject+"&message="+message2+"");
//	return null;
//	}
public ActionForward contact(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws IOException
{
	
	
	String name=request.getParameter("name");

	String email1=request.getParameter("Email");

	String contact=request.getParameter("contct");

	String subject=request.getParameter("subject");
	String message2=request.getParameter("textareamessage");
	
	MailSendingUtil.contactMail(email1);
	
	Learnezysms.sendcontactsms(name,email1,contact,subject,message2);

	response.sendRedirect("Contactmail.jsp?name="+name+"&Email="+email1+"&Contact="+contact+"&subject="+subject+"&message="+message2+"");
	return null;
	}


public ActionForward autoCompleteindexes(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) {
	  
	   try {
						   
		   System.out.println("autocompleteeeeeeeeeeeeeeeeeeeeeeeeeeee");
		   System.out.println("autocompleteeeeeeeeeeeeeeeeeeeeeeeeeeee");

			String term = request.getParameter("term");
			System.out.println("Data from ajax call " + term);

			
			ArrayList<String> list = handler.getProductListArray(getDataSource(request),term);
			
			
			System.out.println("List in autocomplete="+list);

			String searchList = new Gson().toJson(list);
			response.getWriter().write(searchList);
		}catch (Exception e) {
		e.printStackTrace();
	}		return null;
	
} 
public ActionForward locality(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) {
	  
	   try {
						   
		  System.out.println("inside locality action");
			String term = request.getParameter("term");
			System.out.println("Data from ajax call " + term);

			
			ArrayList<String> list = handler.getlocality(getDataSource(request),term);
			
			
			System.out.println("List in autocomplete="+list);

			String searchList = new Gson().toJson(list);
			response.getWriter().write(searchList);
		}catch (Exception e) {
		e.printStackTrace();
	}		return null;
	
} 
public ActionForward city(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) {
	  
	   try {
						   
		   System.out.println("autocompleteeeeeeeeeeeeeeeeeeeeeeeeeeee");
		   System.out.println("autocompleteeeeeeeeeeeeeeeeeeeeeeeeeeee");

			String term = request.getParameter("term");
			System.out.println("Data from ajax call " + term);

			
			ArrayList<String> list = handler.getcity(getDataSource(request),term);
			
			
			System.out.println("List in autocomplete="+list);

			String searchList = new Gson().toJson(list);
			response.getWriter().write(searchList);
		}catch (Exception e) {
		e.printStackTrace();
	}		return null;
	
} 
public ActionForward getcityss(ActionMapping mapping, ActionForm form, HttpServletRequest request,HttpServletResponse response)throws Exception {
	
	try {
		int city=Integer.parseInt(request.getParameter("city"));
		System.out.println("In userIddddddddddddddddddd"+catid);
		JSONArray obj = handler.getcityindex(getDataSource(request),request,response,city);
			response.getOutputStream().write(obj.toString().getBytes());
			} catch (Exception e) {
		e.printStackTrace();
	}
	return null;
}
public ActionForward getinstitute(ActionMapping mapping, ActionForm form, HttpServletRequest request,HttpServletResponse response)throws Exception {
	
	try {
		String institutename=request.getParameter("institute");
		System.out.println("institutenamessssssssssssssss"+institutename);
		

		JSONArray obj = handler.getinstitute(getDataSource(request),request,response,institutename);
			response.getOutputStream().write(obj.toString().getBytes());

			
			} catch (Exception e) {
		e.printStackTrace();
	}
	return null;
}
public ActionForward getcityurl(ActionMapping mapping, ActionForm form, HttpServletRequest request,HttpServletResponse response)throws Exception {
	
	try {
		String cityid=request.getParameter("cityid");
		System.out.println("institutenamessssssssssssssss"+cityid);
		

		JSONArray obj = handler.getcityurl(getDataSource(request),request,response,cityid);
			response.getOutputStream().write(obj.toString().getBytes());

			
			} catch (Exception e) {
		e.printStackTrace();
	}
	return null;
}

public ActionForward getlocalityurl(ActionMapping mapping, ActionForm form, HttpServletRequest request,HttpServletResponse response)throws Exception {
	
	try {
		String locality=request.getParameter("locality");
		System.out.println("institutenamessssssssssssssss"+locality);
		

		JSONArray obj = handler.getlocalityurl(getDataSource(request),request,response,locality);
			response.getOutputStream().write(obj.toString().getBytes());

			
			} catch (Exception e) {
		e.printStackTrace();
	}
	return null;
}

public ActionForward getcoursecategory(ActionMapping mapping, ActionForm form, HttpServletRequest request,HttpServletResponse response)throws Exception {
	
	try {
		String catname=request.getParameter("catname");
		System.out.println("catname"+catname);
		

		JSONArray obj = handler.getcategoryurl(getDataSource(request),request,response,catname);
			response.getOutputStream().write(obj.toString().getBytes());

			
			} catch (Exception e) {
		e.printStackTrace();
	}
	return null;
}

public ActionForward getcourse(ActionMapping mapping, ActionForm form, HttpServletRequest request,HttpServletResponse response)throws Exception {
	
	try {
		String coursename=request.getParameter("coursename");
		System.out.println("catname"+coursename);
		

		JSONArray obj = handler.getcourse(getDataSource(request),request,response,coursename);
			response.getOutputStream().write(obj.toString().getBytes());

			
			} catch (Exception e) {
		e.printStackTrace();
	}
	return null;
}

public ActionForward subscribeMail(ActionMapping mapping, ActionForm form, HttpServletRequest request,HttpServletResponse response)throws Exception {
	
	try {
    	DynamicDataForm roleForm = (DynamicDataForm) form;

		System.out.println("Inside Subscribe Action");
		String fromdate=request.getParameter("subscribeemail");
			System.out.println("fromdate is "+fromdate);
		   
	     //    fromDate=request.getParameter("fromdate");
	        
		JSONArray obj = handler.subscribeMail(getDataSource(request),response,fromdate,request);
			response.getOutputStream().write(obj.toString().getBytes());
			System.out.println("inside Suhbscibr mail****************");
			
	}catch (Exception e) {
		e.printStackTrace();
	}
	return null;
}
  
}
