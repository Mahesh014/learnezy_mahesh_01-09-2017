package com.itech.elearning.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

import javax.mail.Session;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


import com.google.gson.Gson;
import com.itech.elearning.forms.ContryForm;
import com.itech.elearning.forms.CourseMasterForm;
import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.StudentRegistrationForm;
import com.itech.elearning.handler.CityHandler;
import com.itech.elearning.handler.CountryHandler;
import com.itech.elearning.handler.CourseMasterHandler;
import com.itech.elearning.handler.LoginHandler;
import com.itech.elearning.handler.StateHandler;
import com.itech.elearning.handler.MYStudentRegistrationHandler;
import com.itech.elearning.handler.TaxMasterHandler;
import com.itech.elearning.handler.TransactionHandler;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.FileUploadDownloadUtil;
import com.itech.elearning.utils.MailSendingUtil;
import com.itech.elearning.utils.NumberToWord;
import com.itech.elearning.utils.SmsSender;
import com.itech.elearning.utils.StringUtil;

public class StudentRegistrationAction extends BaseAction{

	MYStudentRegistrationHandler handler=new MYStudentRegistrationHandler();
	LoginHandler handler1=new LoginHandler();
	InvoiceModuleAction pdf= new InvoiceModuleAction();
	CourseMasterHandler courseHandler= new CourseMasterHandler();
	TransactionHandler transactionHandler= new TransactionHandler();
	 
	CountryHandler countryHandler = new CountryHandler();
	StateHandler stateHandler = new StateHandler();
	CityHandler cityHandler = new CityHandler();
	TaxMasterHandler taxHandler = new TaxMasterHandler();
	public ActionForward list(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession session=request.getSession();
	

		String compid = null;
	    int roleid = 0;
		compid=request.getParameter("compid");
		if(compid==null)
		{
			compid="1";
		}		
		roleid = Integer.parseInt(request.getParameter("roleid"));	
		System.out.println("vani testing compid "+compid);
		List<StudentRegistrationForm> courselist = handler.courselist1(compid,getDataSource(request));
		request.setAttribute("courselist", courselist);
		List<ContryForm> countryList = countryHandler.activeList(roleid,compid,getDataSource(request));
		request.setAttribute("countryList", countryList);
		
		HashMap<String, String> globasetting = taxHandler.globasetting(getDataSource(request));
		request.setAttribute("globasetting", globasetting);
		
		String serviceTax=globasetting.get("service tax").toString();
		System.out.println("vani "+serviceTax);
		request.setAttribute("serviceTax", serviceTax);
		return mapping.findForward("studentRegistration");
	}

	public ActionForward addUser(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception {
		String status= Common.FAILURE_MESSAGE;
		StudentRegistrationForm studentForm = (StudentRegistrationForm) form;
		HttpSession session = request.getSession();
		String compid = request.getParameter("compid"); //((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		String profilePhotoName="";
		String fileUploadPath= getServlet().getServletContext().getRealPath("/")+ "imageUpload";
		boolean uploadStatus=true;
		boolean trail=false;
		FormFile pFile= studentForm.getImageFile();
		if(pFile.getFileName()!="" && pFile.getFileSize()>2){
				profilePhotoName=studentForm.getUname()+pFile.getFileName();
				uploadStatus = FileUploadDownloadUtil.fileUploader(pFile, fileUploadPath, profilePhotoName);
		}
		if(uploadStatus){
			String courseList =StringUtil.sapratedByComa(request.getParameterValues("courseid"));
			int serviceTax=Integer.parseInt(request.getParameter("serviceTax"));
			int tax = studentForm.getTax();
			int totalAmount=studentForm.getTotalAmount();
			int subAmount=studentForm.getSubAmount();
			int discountAmount=studentForm.getDiscountAmount();
			int paymentStatus=0; 
			int userId = handler.addUser(compid,studentForm,courseList, getDataSource(request),profilePhotoName,paymentStatus);
			if (userId!=0 && totalAmount!=0 && trail==false ) {
				
				int transactionID= 0;//transactionHandler.addTransaction(getDataSource(request), courseList, userId, tax, paymentStatus, totalAmount);
				
				/*request.setAttribute("courseList", courseList);
				request.setAttribute("userId", userId);
				request.setAttribute("totalAmount", totalAmount);
				request.setAttribute("tax", tax);
				request.setAttribute("transactionID", transactionID);*/
				
				session.setAttribute("courseList", courseList);
				session.setAttribute("userId", userId);
				session.setAttribute("totalAmount", totalAmount);
				session.setAttribute("tax", tax);
				session.setAttribute("transactionID", transactionID);
				session.setAttribute("subAmount", subAmount);
				session.setAttribute("discountAmount", discountAmount);
				session.setAttribute("serviceTax", serviceTax);
				return mapping.findForward("paymentGateway");
				
				
				/*studentForm.setTransactionID(transactionID);
				List<CourseMasterForm> selectedCourseList= courseHandler.courseListByCourseIds(courseList, getDataSource(request));
				String fileName=StringUtil.repStrUnScr(studentForm.getFirstName()+"_"+studentForm.getLastName()+"_"+userId+"_Invoice.pdf");
				String filePath = getServlet().getServletContext().getRealPath("/")+"invoice"+"/"+fileName;
				//return an PDF Converter
				int flag=InvoiceModuleAction.getPdfInoive(getDataSource(request),studentForm,filePath,selectedCourseList,userId,totalAmount);
				if(flag==1){
					String[] studentEmail={studentForm.getEmailId()};
					MailSendingUtil.postAttachedMail(studentEmail, filePath);
					
				}*/
	 	 	}
			request.setAttribute("status",status);
			
		}
		return list(mapping, studentForm, request, response);
	}

	public ActionForward PGI(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception {
				HttpSession session = request.getSession();
		try {

			
			String courseList = session.getAttribute("courseList").toString();
			String userId = session.getAttribute("userId").toString();
			String totalAmount = session.getAttribute("totalAmount").toString();
			String tax = session.getAttribute("tax").toString();
			String subAmount =session.getAttribute("subAmount").toString();
			String transactionID = session.getAttribute("transactionID").toString();
			String discountAmount = session.getAttribute("discountAmount").toString();
			String serviceTax = session.getAttribute("serviceTax").toString();
			
			String PGIID = "1234";
			String CardNumber ="121244444";
			int transactionStatus=1;
			int paymentStatus=1;
			
			handler.updatePaymentStatus(getDataSource(request), userId, paymentStatus);
			transactionHandler.updateTransactionStatus(getDataSource(request),transactionID, transactionStatus, CardNumber, PGIID  );
		
			String itechLogofilePath = getServlet().getServletContext().getRealPath("/")+"/images/Itech Solutions logo.png";
			String fontPath = getServlet().getServletContext().getRealPath("/")+"/images/OpenSans-Regular.ttf";
			
			List<CourseMasterForm> selectedCourseList= courseHandler.courseListByCourseIds(courseList, getDataSource(request));
			StudentRegistrationForm studentData= handler.getStudentData(userId, courseList, getDataSource(request));
			
			String inWord=new NumberToWord().convertNumberToWords(Integer.parseInt(totalAmount));
			String fileName=StringUtil.repStrUnScr(studentData.getFirstName()+"_"+studentData.getLastName()+"_"+userId+"_Invoice.pdf");
			String filePath = getServlet().getServletContext().getRealPath("/")+"invoice"+"/"+fileName;
			
			int flag=InvoiceModuleAction.getPdfInoive(getDataSource(request),studentData,filePath,selectedCourseList,userId,totalAmount,tax,fontPath,itechLogofilePath,inWord,subAmount,transactionID,discountAmount,serviceTax);
			
			if(flag==1){
				String[] studentEmail={studentData.getEmailId()};
				String subject="Inovice :: Student Training Booking";
				String messageBody="Welcome to Itech Training Academy. Please Find the Attached Invoice";
				MailSendingUtil.postAttachedMail(studentEmail, filePath, subject, messageBody,fileName);
				request.setAttribute("status","Thank you for the Payment!");
			}else{
				request.setAttribute("status",Common.FAILURE_MESSAGE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.invalidate(); 
		return mapping.findForward("PGISuccess");
	}
	public ActionForward toHome(ActionMapping mapping, ActionForm form,	HttpServletRequest request, HttpServletResponse response)throws IOException {

		HttpSession session = request.getSession(true);
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			return mapping.findForward("studentDashboard");
		} else
			return mapping.findForward("session");
	}	

	@SuppressWarnings("unchecked")
	public ActionForward checkUserName(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
				JSONObject jObj = new JSONObject();
				HttpSession session = request.getSession();
				//String compid = null;
				//compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
				String userName=request.getParameter("userName").trim();
				System.out.println(userName);
				if (userName!=null) {
					int flag=handler.checkUserName(getDataSource(request),userName);
					System.out.println("Vani flag val  "+flag);
					jObj.put("flag",flag);
					response.getOutputStream().write(jObj.toJSONString().getBytes());
				}
				return null;
	}
	
	public ActionForward userRegistration(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception {
		String status= Common.FAILURE_MESSAGE;
		StudentRegistrationForm studentForm = (StudentRegistrationForm) form;
		HttpSession session = request.getSession();
		String compid = request.getParameter("compid");
		if(compid==null)
		{
			compid="1";
		}
		System.out.println("vani testing compid "+compid);
		//compid= ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		String profilePhotoName="";
		String fileUploadPath= getServlet().getServletContext().getRealPath("/")+ "imageUpload";
		boolean uploadStatus=true;
		FormFile pFile= studentForm.getImageFile();
		if(pFile.getFileName()!="" && pFile.getFileSize()>2){
				profilePhotoName=studentForm.getUname()+pFile.getFileName();
				uploadStatus = FileUploadDownloadUtil.fileUploader(pFile, fileUploadPath, profilePhotoName);
		}
		if(uploadStatus){
			int userId = handler.userRegistration(studentForm,compid,getDataSource(request),profilePhotoName);
			if (userId!=0) {
				status="Thank you for registering with LearnEZY, please select the course";
				List<StudentRegistrationForm> courselist = handler.courselist1(compid,getDataSource(request));
				request.setAttribute("courselist", courselist);
				
				HashMap<String, String> globasetting = taxHandler.globasetting(getDataSource(request));
				request.setAttribute("globasetting", globasetting);
				String serviceTax=globasetting.get("service tax").toString();
				request.setAttribute("serviceTax", serviceTax);
				
				request.setAttribute("status",status);
				request.setAttribute("userId",userId);
				
				
				session.setAttribute("userName",studentForm.getEmailId());
				
			}else{
				return mapping.findForward("studentRegistration");
			}
		}
		return mapping.findForward("studentRegistration1");
	}

	public ActionForward addCourse(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception {
		String status= Common.FAILURE_MESSAGE;
		StudentRegistrationForm studentForm = (StudentRegistrationForm) form;
		
		
			String courseList =StringUtil.sapratedByComa(request.getParameterValues("courseid"));
			int serviceTax=Integer.parseInt(request.getParameter("serviceTax"));
			int tax = studentForm.getTax();
			int totalAmount=studentForm.getTotalAmount();
			long userId=studentForm.getUserid();
			int subAmount=studentForm.getSubAmount();
			int discountAmount=studentForm.getDiscountAmount();
			int paymentStatus=0;
			int addCourseFlag=handler.addCourse(getDataSource(request), courseList, userId);
			if ( addCourseFlag==1 && userId!=0 && totalAmount!=0) {
				HttpSession session = request.getSession();
				String compid = "1";
				//compid = ((LoginForm) session.getAttribute("userDetail")).getCompanyId();
				int transactionID = transactionHandler.addTransaction(getDataSource(request), courseList, userId, tax, paymentStatus, totalAmount,compid);
			
				session.setAttribute("userName",session.getAttribute("userName").toString());
				request.setAttribute("course", courseList);
				request.setAttribute("tamt",totalAmount);
				session.setAttribute("courseList", courseList);
				session.setAttribute("userId", userId);
				session.setAttribute("totalAmount", totalAmount);
				session.setAttribute("tax", tax);

				session.setAttribute("transactionID", transactionID);
				session.setAttribute("subAmount", subAmount);
				session.setAttribute("discountAmount", discountAmount);
				session.setAttribute("serviceTax", serviceTax);
				request.setAttribute("status","Thank you for selecting a course, please make the payment");
				List<StudentRegistrationForm> selectedcourselist = handler.selectedcourselist(compid,getDataSource(request),courseList);
				request.setAttribute("selectedcourselist", selectedcourselist);
			
			 
				StringBuffer courseNames=new StringBuffer();
				StringBuffer courseNameCourseId=new StringBuffer();
				StringBuffer discountPercentage=new StringBuffer();
				int listSize=selectedcourselist.size();
				int count=0;
				ListIterator<StudentRegistrationForm> itr= selectedcourselist.listIterator();
				while (itr.hasNext()) {
					StudentRegistrationForm studentRegistrationForm = (StudentRegistrationForm) itr.next();
					String courseName=studentRegistrationForm.getCoursename();
					System.out.println("serviceTaxserviceTaxserviceTaxserviceTax"+courseNames);

					int courseId=studentRegistrationForm.getCourseid();
					String discount=studentRegistrationForm.getDiscountPercentage();
					if (courseName.length()<4) {
						courseNameCourseId.append(courseName+courseId);
					}else{
						courseNameCourseId.append(courseName.substring(0, 4)+courseId);
					}
					
					courseNames.append(courseName);
					discountPercentage.append(discount);
					
					
					if (count<listSize-1) {
						courseNames.append(",");
						discountPercentage.append(",");
						courseNameCourseId.append(",");
					}
					count=count+1;
					
				}
				session.setAttribute("courseNames", courseNameCourseId.toString());
				
				
				//session.setAttribute("courseNameCourseId", courseNameCourseId.toString());
				session.setAttribute("discountPercentage", discountPercentage.toString());
				
				System.out.println(courseNames.toString()+ "  "+discountPercentage.toString());
				return mapping.findForward("paymentGateway");
		 	}
			request.setAttribute("status",status);
		request.setAttribute("userId",userId);
		return userRegistration(mapping, studentForm, request, response);
	}
	
public ActionForward listaca(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession session = request.getSession();
		//String compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
	String compid = null;
    int rolid = 0;
	compid=request.getParameter("compid");
	
	System.out.println("Vani testing academy registration");
	if(compid==null)
	{
		compid="1";
	}
	
	    request.setAttribute("compid",compid);
	    rolid =Integer.parseInt(request.getParameter("roleid"));

		List<StudentRegistrationForm> courselist = handler.courselist(compid,getDataSource(request));
		request.setAttribute("courselist", courselist);
		
		List<ContryForm> countryList = countryHandler.activeList(rolid,compid,getDataSource(request));
		request.setAttribute("countryList", countryList);
		
		HashMap<String, String> globasetting = taxHandler.globasetting(getDataSource(request));
		request.setAttribute("globasetting", globasetting);
		
		String serviceTax=globasetting.get("service tax").toString();
		System.out.println("vani "+serviceTax);
		
		request.setAttribute("serviceTax", serviceTax);
		return mapping.findForward("academyregistration");
	}

public ActionForward listfree(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
	
	HttpSession session = request.getSession();
	//String compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
	String compid = null;
    int roleid = 0;
	compid=request.getParameter("compid");
	if(compid==null)
	{
		compid="1";
	}
	roleid = Integer.parseInt(request.getParameter("roleid"));
	List<StudentRegistrationForm> courselist = handler.courselist(compid,getDataSource(request));
	request.setAttribute("courselist", courselist);
	
	List<ContryForm> countryList = countryHandler.activeList(roleid,compid,getDataSource(request));
	request.setAttribute("countryList", countryList);
	
	HashMap<String, String> globasetting = taxHandler.globasetting(getDataSource(request));
	request.setAttribute("globasetting", globasetting);
	
	String serviceTax=globasetting.get("service tax").toString();
	System.out.println("vani "+serviceTax);
	
	request.setAttribute("serviceTax", serviceTax);
	return mapping.findForward("freeregis");
}
	
public ActionForward AcademyRegistration(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
	String status= Common.FAILURE_MESSAGE;
	StudentRegistrationForm studentForm = (StudentRegistrationForm) form;
	HttpSession session = request.getSession();
	String compid = "1";
	//compid= ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
	String profilePhotoName="";
	String fileUploadPath= getServlet().getServletContext().getRealPath("/")+ "imageUpload";
	boolean uploadStatus=true;
	FormFile pFile= studentForm.getImageFile();
	if(pFile.getFileName()!="" && pFile.getFileSize()>2){
			profilePhotoName=studentForm.getUname()+pFile.getFileName();
			uploadStatus = FileUploadDownloadUtil.fileUploader(pFile, fileUploadPath, profilePhotoName);
	}
	if(uploadStatus){
		compid = String.valueOf(handler.addCompany(studentForm,compid,getDataSource(request)));
		System.out.println("compid "+compid);
		
		int userId = handler.AcademyRegistration(studentForm,compid,getDataSource(request),profilePhotoName);
		if (userId!=0) {
			
			status="Thank you for registering with LearnEZY, we will get back to you soon!!!";
			List<StudentRegistrationForm> courselist = handler.courselist(compid,getDataSource(request));
			request.setAttribute("courselist", courselist);
			
			HashMap<String, String> globasetting = taxHandler.globasetting(getDataSource(request));
			request.setAttribute("globasetting", globasetting);
			
			String serviceTax=globasetting.get("service tax").toString();
			request.setAttribute("serviceTax", serviceTax);
			
			request.setAttribute("status",status);
			request.setAttribute("userId",userId);
			
			
			session.setAttribute("userName",studentForm.getEmailId());
			
		}else{
			return listaca(mapping, studentForm, request, response);
		}
	}
	return mapping.findForward("academymakepayment");
}

public ActionForward FreelancerRegistration(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
	String status= Common.FAILURE_MESSAGE;
	StudentRegistrationForm studentForm = (StudentRegistrationForm) form;
   System.out.println("area of expertise "+studentForm.getAreofexper());
	HttpSession session = request.getSession();
	String compid = "1";
	compid = String.valueOf(handler.addCompany(studentForm,compid,getDataSource(request)));
	System.out.println("compid "+compid);
	String profilePhotoName="";
	String fileUploadPath= getServlet().getServletContext().getRealPath("/")+ "imageUpload";
	boolean uploadStatus=true;
	FormFile pFile= studentForm.getImageFile();
	if(pFile.getFileName()!="" && pFile.getFileSize()>2){
			profilePhotoName=studentForm.getUname()+pFile.getFileName();
			uploadStatus = FileUploadDownloadUtil.fileUploader(pFile, fileUploadPath, profilePhotoName);
	}
	
	if(uploadStatus){
		int userId = handler.FreeLanceRegistration(studentForm,compid,getDataSource(request),profilePhotoName);
		if (userId!=0) {
			status="Thank you for registering with LearnEZY, we will get back to you soon!!!";
			List<StudentRegistrationForm> courselist = handler.courselist(compid,getDataSource(request));
			request.setAttribute("courselist", courselist);
			
			HashMap<String, String> globasetting = taxHandler.globasetting(getDataSource(request));
			request.setAttribute("globasetting", globasetting);
			
			String serviceTax=globasetting.get("service tax").toString();
			request.setAttribute("serviceTax", serviceTax);
			
			request.setAttribute("status",status);
			request.setAttribute("userId",userId);
			
			
			session.setAttribute("userName",studentForm.getEmailId());
			
		}else{
			return listfree(mapping, studentForm, request, response);
		}
	}
	return mapping.findForward("academymakepayment");
}

public ActionForward addacadet(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
{
	String status= Common.FAILURE_MESSAGE;
	StudentRegistrationForm studentForm = (StudentRegistrationForm) form;
	
	
		String courseList =StringUtil.sapratedByComa(request.getParameterValues("courseid"));
		System.out.println("Course List "+courseList);
		int serviceTax=Integer.parseInt(request.getParameter("serviceTax"));
		int tax = studentForm.getTax();
		int totalAmount=studentForm.getTotalAmount();
		long userId=studentForm.getUserid();
		int subAmount=studentForm.getSubAmount();
		//int discountAmount=studentForm.getDiscountAmount();
		int paymentStatus=0;
		int addCourseFlag=handler.addCourse(getDataSource(request), courseList, userId);
		if ( addCourseFlag==1 && userId!=0 && totalAmount!=0) {
			HttpSession session = request.getSession();
			String compid = "1";
			//compid = ((LoginForm) session.getAttribute("userDetail")).getCompanyId();
			int transactionID = transactionHandler.addTransaction(getDataSource(request), courseList, userId, tax, paymentStatus, totalAmount,compid);
		
			//session.setAttribute("userName",session.getAttribute("userName").toString().substring(0, 4)+userId);
			request.setAttribute("course", courseList);
			request.setAttribute("tamt",totalAmount);
			session.setAttribute("courseList", courseList);
			session.setAttribute("userId", userId);
			session.setAttribute("totalAmount", totalAmount);
			session.setAttribute("tax", tax);
			session.setAttribute("transactionID", transactionID);
			session.setAttribute("subAmount", subAmount);
			//session.setAttribute("discountAmount", discountAmount);
			session.setAttribute("serviceTax", serviceTax);
			request.setAttribute("status","Thank you for selecting a course, please make the payment");
			List<StudentRegistrationForm> selectedcourselist = handler.selectedcourselist(compid,getDataSource(request),courseList);
			request.setAttribute("selectedcourselist", selectedcourselist);
		
		 
			StringBuffer courseNames=new StringBuffer();
			StringBuffer courseNameCourseId=new StringBuffer();
			StringBuffer discountPercentage=new StringBuffer();
			int listSize=selectedcourselist.size();
			int count=0;
			ListIterator<StudentRegistrationForm> itr= selectedcourselist.listIterator();
			while (itr.hasNext()) {
				StudentRegistrationForm studentRegistrationForm = (StudentRegistrationForm) itr.next();
				String courseName=studentRegistrationForm.getCoursename();
				int courseId=studentRegistrationForm.getCourseid();
				String discount=studentRegistrationForm.getDiscountPercentage();
				if (courseName.length()<4) {
					courseNameCourseId.append(courseName+courseId);
				}else{
					courseNameCourseId.append(courseName.substring(0, 4)+courseId);
				}
				
				courseNames.append(courseName);
				discountPercentage.append(discount);
				
				
				if (count<listSize-1) {
					courseNames.append(",");
					discountPercentage.append(",");
					courseNameCourseId.append(",");
				}
				count=count+1;
				
			}
			session.setAttribute("courseNames", courseNameCourseId.toString());
			System.out.println("qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"+courseNames);

			System.out.println("qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"+courseNameCourseId.toString());
			//session.setAttribute("courseNameCourseId", courseNameCourseId.toString());
		//	session.setAttribute("discountPercentage", discountPercentage.toString());
			
			System.out.println(courseNames.toString()+ "  "+discountPercentage.toString());
			return mapping.findForward("paymentGatewayaca");
	 	}
		request.setAttribute("status",status);
	request.setAttribute("userId",userId);
	return listaca(mapping, studentForm, request, response);
	
}
public ActionForward verifyemailid(ActionMapping mapping, ActionForm form, HttpServletRequest request,HttpServletResponse response)throws Exception {
	
	try {
		String email=request.getParameter("emailid");
		JSONArray obj = handler.verifyemailid(getDataSource(request),request,response,email);
			response.getOutputStream().write(obj.toString().getBytes());

			
			} catch (Exception e) {
		e.printStackTrace();
	}
	return null;
}


public ActionForward addcourses(ActionMapping mapping, ActionForm form, HttpServletRequest request,HttpServletResponse response)throws Exception {
	
	try {
		String email=request.getParameter("emailid");
		String course=request.getParameter("courseid");
		JSONArray obj = handler.addcourses(getDataSource(request),request,response,email,course);
			response.getOutputStream().write(obj.toString().getBytes());

			
			} catch (Exception e) {
		e.printStackTrace();
	}
	return null;
}

public ActionForward addtrasnsactiondetails(ActionMapping mapping, ActionForm form, HttpServletRequest request,HttpServletResponse response)throws Exception {
	
	try {
		String userid=request.getParameter("userid");
		String courseid=request.getParameter("courseid");
		String totalidamount=request.getParameter("totalidamount");
		String tax=request.getParameter("tax");


		JSONArray obj = handler.addtrasnsactiondetails(getDataSource(request),request,response,userid,courseid,totalidamount,tax);
			response.getOutputStream().write(obj.toString().getBytes());

			
			} catch (Exception e) {
		e.printStackTrace();
	}
	return null;
}




public ActionForward clear(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) throws IOException{
	System.out.println("systemmmmmmmmmmmmmmmmmmmmmmmmmmmmmm");
	@SuppressWarnings("unused")



		 JSONObject obj=new JSONObject();
		 obj.put("clr", 0);
		 
		 response.getOutputStream().write(obj.toJSONString().getBytes());
		return null;
	
}	


public ActionForward downloadexcel(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception
{

try{
	String file1 = request.getParameter("filename");
	
	System.out.println(file1+" ----------------");
	
    response.setContentType("application/octet-stream");
   
    response.setHeader("Content-Disposition","attachment;fileName=" +file1);
   
    	
    	String filename1 = getServlet().getServletContext().getRealPath("\\")+"Excelfiledownload"+"\\"+file1;
    	File fileToDownload1 = new File(filename1);
    	FileInputStream in = new FileInputStream(fileToDownload1);


    	ServletOutputStream out = response.getOutputStream();
    	 
    	byte[] outputByte = new byte[4096];
    
    	while(in.read(outputByte, 0, 4096) != -1)
    	{
    		out.write(outputByte, 0, 4096);
    	}
    	
    	in.close();
    	
    	out.flush();
    	out.close();
    	


}catch (Exception e) {
e.printStackTrace();
}
return list(mapping, form, request, response);
} 
public ActionForward autoCompleteindexesare(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) {
	  
	   try {
						   
		   System.out.println("autocompleteeeeeeeeeeeeeeeeeeeeeeeeeeee");
		   System.out.println("autocompleteeeeeeeeeeeeeeeeeeeeeeeeeeee");

			String term = request.getParameter("term");
			System.out.println("Data from ajax call******************************** " + term);

			
			ArrayList<String> list = handler.getProductListArray1(getDataSource(request),term);
			
			
			System.out.println("List in autocomplete="+list);

			String searchList = new Gson().toJson(list);
			response.getWriter().write(searchList);
		}catch (Exception e) {
		e.printStackTrace();
	}			return null;
	
} 


public ActionForward viewdetails(ActionMapping mapping, ActionForm form, HttpServletRequest request,HttpServletResponse response)throws Exception {
	
	try {
		String userid=request.getParameter("java");
	
System.out.println("qqqqqqq"+userid);

		JSONArray obj = handler.viewdetails(getDataSource(request),request,response,userid);
			response.getOutputStream().write(obj.toString().getBytes());

			
			} catch (Exception e) {
		e.printStackTrace();
	}
	return null;
}


	public ActionForward viewdetails1(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) throws Exception
		
		{
			System.out.println("inside ChessCoachingTopics list ");
			
			
				
			try
			{
				
					String coachingtopicsid=request.getParameter("java");
					request.setAttribute("playerfull",coachingtopicsid);
					System.out.println("coachingtopicsiddddddddddddddddddddddddddddddddddddd"+coachingtopicsid);
				List<StudentRegistrationForm> topicsList=handler.viewdetails1(getDataSource(request),coachingtopicsid);
				request.setAttribute("topicsList",topicsList);
				System.out.println("topicsList is:"+topicsList);
		
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			
			return mapping.findForward("search12");
			
			
		}
	public ActionForward viewdetailscourse(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception
			
			{
				System.out.println("inside ChessCoachingTopics list ");
				
				
					
				try
				{
					
						String coachingtopicsid=request.getParameter("java");
						request.setAttribute("playerfull",coachingtopicsid);
						System.out.println("coachingtopicsiddddddddddddddddddddddddddddddddddddd"+coachingtopicsid);
					List<StudentRegistrationForm> topicsListsss=handler.viewdetailssss(getDataSource(request),coachingtopicsid);
					request.setAttribute("topicsListss",topicsListsss);
					System.out.println("topicsList is:"+topicsListsss);
			
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				
				
				return mapping.findForward("search123");
				
				
			}
	}




