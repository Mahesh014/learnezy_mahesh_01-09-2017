package com.itech.elearning.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.TopicTextForm;
import com.itech.elearning.handler.TopicTextHandler;

public class TopicTextAction extends BaseAction {
	TopicTextHandler handler = new TopicTextHandler();

	public ActionForward list(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
         
		TopicTextForm topicForm = (TopicTextForm) form;
		String testID=request.getParameter("testID");	
		request.setAttribute("testID", testID);
		HttpSession session = request.getSession();
		Long userid = null;
		String compid = null;
		Long courseID=null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			courseID=((LoginForm)session.getAttribute("userDetail")).getCourseID();
			compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			List<TopicTextForm> topicList = handler.listAllTopic(compid,courseID,userid,getDataSource(request));
			request.setAttribute("topicList", topicList);
			request.setAttribute("topictxt", handler.listAll(compid,testID,userid,
					getDataSource(request), topicForm.getTopicID()));
		}
		return mapping.findForward("topic");
	}
	public String MyString(HttpServletRequest request, HttpServletResponse response) {
		return "Itech my string form struct";
	}
}