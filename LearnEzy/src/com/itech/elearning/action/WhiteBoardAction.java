package com.itech.elearning.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.WhiteBoardForm;
import com.itech.elearning.handler.WhiteBoardHandler;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.StringUtil;
import com.itech.twiddla.TwiddlaHelper;

public class WhiteBoardAction extends BaseAction {
	WhiteBoardHandler handler = new WhiteBoardHandler();

	public ActionForward list(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		HttpSession session = request.getSession();
		Long userid = null;
		String compid = null;
		int roleid = 0;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {

			List<WhiteBoardForm> appointmentScheduleList = handler.appointmentUserlist(roleid,compid,getDataSource(request));
			request.setAttribute("appointmentScheduleList", appointmentScheduleList);
		}

		return mapping.findForward("appointmentScheduleList");

	}

	public ActionForward accessSession(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		WhiteBoardForm whiteBoardForm = (WhiteBoardForm) form;
		HttpSession session = request.getSession();

		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			String appointmentScheduleId = request.getParameter("appointmentScheduleId").trim();
			String option = request.getParameter("option");
				
			/*
			 * whiteBoardForm.setAppointmentScheduleId(1);
			 * whiteBoardForm.setSessionStatus(option);
			 * whiteBoardForm.setAccessPassword(meetingpassword);
			 * whiteBoardForm.setWhiteBoardSessionTitle(meetingtitle);
			 */

			WhiteBoardForm whiteBoardForm1 = handler.getWhiteBoardSessionID(getDataSource(request),	appointmentScheduleId);
			if (whiteBoardForm1 != null && !option.equals("Close")) {
				option = whiteBoardForm1.getSessionStatus();
				option = "Join";
			}else{
				whiteBoardForm1=handler.getAppointmentDetails(getDataSource(request),appointmentScheduleId);
			}
			System.out.println("WhiteBoard Session ID: " + whiteBoardForm1.getWhiteBoardSessionID() +" :::"+ request.getParameter("appointmentScheduleId"));
				if (option.equals("Open")) {
					String sessionTitle = whiteBoardForm1.getWhiteBoardSessionTitle();
					String sessionPassword =whiteBoardForm1.getAccessPassword();
					String url = "";
					int whiteBoardSessionID = TwiddlaHelper.CreateMeeting(sessionTitle, sessionPassword, url);
					;
					// whiteBoardForm.setWhiteBoardSessionID(String.valueOf(whiteBoardSessionID));
					int flag = handler.addSession(whiteBoardForm, getDataSource(request), appointmentScheduleId,
							sessionTitle, sessionPassword, option, whiteBoardSessionID);
					if (flag == 1) {
						String frameURL = "http://www.twiddla.com/api/start.aspx?sessionid=" + whiteBoardSessionID
								+ "&hide=chat,invite,profile,welcome,url,documents,email,widgets,math,room,settings";
						request.setAttribute("frameURL", frameURL);
						request.setAttribute("sessionPassword", sessionPassword);
						request.setAttribute("sessionTitle", sessionTitle);
						return mapping.findForward("whiteBoard");
					}
				}
				if (option.equals("Join")) {

					String frameURL = "http://www.twiddla.com/api/start.aspx?sessionid="
							+ whiteBoardForm1.getWhiteBoardSessionID()
							+ "&hide=chat,invite,profile,welcome,url,documents,email,widgets,math,room,settings";
					request.setAttribute("frameURL", frameURL);
					request.setAttribute("sessionPassword", whiteBoardForm1.getAccessPassword());
					request.setAttribute("sessionTitle", whiteBoardForm1.getWhiteBoardSessionTitle());
					return mapping.findForward("whiteBoard");

				}

				if (option.equals("Close")) {
					
					//handler.updateSession(getDataSource(request), appointmentScheduleId, option, whiteBoardForm1.getWhiteBoardSessionID());
					handler.updateVideoSession(getDataSource(request), appointmentScheduleId, option);
					String errorStatus ="1"; //TwiddlaHelper.deleteMeeting(Integer.parseInt(whiteBoardForm1.getWhiteBoardSessionID()));
					if (errorStatus.equals("1")) {

						return list(mapping, whiteBoardForm1, request, response);
					}
					
				}
			 
			}
			return null;

	}


	public ActionForward accessVideoSession(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();

		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			String appointmentScheduleId = request.getParameter("appointmentScheduleId").trim();
			String option = request.getParameter("option");
			
			WhiteBoardForm whiteBoardForm1 = handler.getWhiteBoardSessionID(getDataSource(request),appointmentScheduleId);
			if (whiteBoardForm1 != null && !option.equals("Close")) {
				option = whiteBoardForm1.getSessionStatus();
				option = "Join";
			}else{
				whiteBoardForm1=handler.getAppointmentDetails(getDataSource(request),appointmentScheduleId);
			}
			if (option.equals("Open")) {
				String meetingtitle = whiteBoardForm1.getWhiteBoardSessionTitle();
				String meetingpassword =whiteBoardForm1.getAccessPassword();
				String meetingDateTime =whiteBoardForm1.getAppointmentDateTime();
				String meetingURLLink = "https://appear.in/"+StringUtil.repStrUnScr(meetingtitle+" "+meetingDateTime);
				int flag = handler.addVideoSession(whiteBoardForm1, getDataSource(request), appointmentScheduleId, meetingtitle, meetingpassword, option, meetingDateTime, meetingURLLink);
				if (flag == 1) {
					request.setAttribute("frameURL", meetingURLLink);
					request.setAttribute("sessionPassword", meetingpassword);
					request.setAttribute("sessionTitle",meetingtitle);
					return mapping.findForward("videoChat");
				}
			}
			if (option.equals("Join")) {
				String meetingtitle = whiteBoardForm1.getWhiteBoardSessionTitle();
				String meetingpassword =whiteBoardForm1.getAccessPassword();
				String meetingDateTime =whiteBoardForm1.getAppointmentDateTime();
				String meetingURLLink = "https://appear.in/"+StringUtil.repStrUnScr(meetingtitle+" "+meetingDateTime);
				request.setAttribute("frameURL", meetingURLLink);
				request.setAttribute("sessionPassword", meetingpassword);
				request.setAttribute("sessionTitle",meetingtitle);
				return mapping.findForward("videoChat");

			}

			if (option.equals("Close")) {
				handler.updateVideoSession(getDataSource(request), appointmentScheduleId, option);
				return list(mapping, whiteBoardForm1, request, response);
				
			}
		}
		return null;

	}

 
	
	/*public ActionForward accessVideoSession(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		WhiteBoardForm whiteBoardForm = (WhiteBoardForm) form;
		HttpSession session = request.getSession();

		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			String appointmentScheduleId = request.getParameter("appointmentScheduleId").trim();
			String option = request.getParameter("option");
			
			WhiteBoardForm whiteBoardForm1 = handler.getWhiteBoardSessionID(getDataSource(request),appointmentScheduleId);
			if (whiteBoardForm1 != null && !option.equals("Close")) {
				option = whiteBoardForm1.getSessionStatus();
				option = "Join";
			}else{
				whiteBoardForm1=handler.getAppointmentDetails(getDataSource(request),appointmentScheduleId);
			}
				if (option.equals("Open")) {
					System.out.println(appointmentScheduleId);
					request.setAttribute("appointmentScheduleId",appointmentScheduleId);
					return mapping.findForward("Video");
				}
				
				if (option.equals("createVideoMeeting")) {
					String meetingName = request.getParameter("meetingName");
					String meetingURLLink = request.getParameter("meetingURLLink").trim();
					String sessionTitle = "Java Session";
					String sessionPassword = "itech123";
					option="Open";
					int flag = handler.addVideoSession(whiteBoardForm, getDataSource(request), appointmentScheduleId,
							sessionTitle, sessionPassword, option, meetingName, meetingURLLink);
					if(flag==1){
						request.setAttribute("status", Common.REC_ADDED);
					}else{
						
						request.setAttribute("status", Common.FAILURE_MESSAGE);
					}
					request.setAttribute("appointmentScheduleId", appointmentScheduleId);
					return mapping.findForward("Video");
				}
				
				if (option.equals("Close")) {					
					int flag=handler.updateVideoSession(getDataSource(request), appointmentScheduleId, option);
					return list(mapping, whiteBoardForm1, request, response);
					 
				}
		}
		return null;

	}*/

}
