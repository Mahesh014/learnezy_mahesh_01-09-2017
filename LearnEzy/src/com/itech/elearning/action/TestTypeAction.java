package com.itech.elearning.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.TestTypeForm;
import com.itech.elearning.handler.TestTypeHandler;
import com.itech.elearning.utils.FileUpload;

public class TestTypeAction extends BaseAction {
	TestTypeHandler handler = new TestTypeHandler();

	public ActionForward list(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		Long userid = null;
		String compid = null;
		int roleid = 0;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
			roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			List<TestTypeForm> testTypeList = handler.list(roleid,compid,getDataSource(request));
			request.setAttribute("testTypeList", testTypeList);
			return mapping.findForward("testType");
		} else
			return mapping.findForward("session");
	}

	public ActionForward add(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		Long userid = null;
		String compid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			TestTypeForm testTypeForm = (TestTypeForm) form;
			request.setAttribute("status", handler.addTestType(compid,testTypeForm,
					getDataSource(request)));
			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");

	}

	public ActionForward changestatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			TestTypeForm testTypeForm = (TestTypeForm) form;
			testTypeForm.setTestTypeId(Long.parseLong((request
					.getParameter("testTypeID"))));
			testTypeForm.setActive(Boolean.parseBoolean(request
					.getParameter("active")));
			request.setAttribute("status", handler.changeTestType(testTypeForm,
					getDataSource(request)));
			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");
	}

	public ActionForward update(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		Long userid = null;
		String compid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			TestTypeForm testTypeForm = (TestTypeForm) form;
			request.setAttribute("status", handler.updateTestType(testTypeForm,
					getDataSource(request)));

			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");
	}
	
	public ActionForward excelReport(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{

		try{
			
			HttpSession session = request.getSession();

			Long userid = null;
			String compid = null;
			int roleid = 0;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {
				compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
				roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
				List<TestTypeForm> testTypeList = handler.list(roleid,compid,getDataSource(request));
				request.setAttribute("testTypeList", testTypeList);
				String fileName="testTypeList_master";
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition","inline;filename="+""+fileName+".xls");
			} else{
				return mapping.findForward("session");
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
	return mapping.findForward("excel");

	}
	
	public ActionForward testTypeExcelUpload(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{

		TestTypeForm testTypeForm = (TestTypeForm) form;

		String  fileName=null;
		boolean uploadstatus=false;
		String filePath=null;
		String compid = null;
		HttpSession session = request.getSession();

		int roleid = 0;
		 try {
			  
			 
			  if(!testTypeForm.getFileName().equals("")) { 
					compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();


					 FormFile myfile = testTypeForm.getFileName();
		 
					 filePath = getServlet().getServletContext().getRealPath("/") +"upload";
					 System.out.println("File Path="+filePath);
					 
					 fileName =myfile.getFileName();
					 String fileName1 = myfile.getFileName();
					 System.out.println("Filename="+fileName);
					 
					 
					 //Excel or csv file upload
					 uploadstatus=FileUpload.fileUploader(myfile, filePath, fileName);
					
						 filePath = getServlet().getServletContext().getRealPath("/")+ "upload\\";
						 						
						String success = handler.testTypeExcelUpload(fileName, filePath,compid);
						System.out.println("prashanth 111111111111111");
						
						request.setAttribute("status", success);
				 }
			  
				  
				
		  }
		 catch (Exception e) {
					e.printStackTrace();
				}
		 return list(mapping, form, request, response);
	}
}
