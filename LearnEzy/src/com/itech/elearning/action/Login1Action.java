package com.itech.elearning.action;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.itech.elearning.constants.learnezyconstants;
import com.itech.elearning.forms.Login1Form;
import com.itech.elearning.handler.Login1Handler;
import com.itech.elearning.handler.TaxMasterHandler;





public class Login1Action  extends BaseAction{
	Login1Handler handler = new Login1Handler();
	TaxMasterHandler taxHandler = new TaxMasterHandler();
	
	public ActionForward login1(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		  String status=learnezyconstants.FAILURE_MESSAGE;
		  Login1Form loginform=(Login1Form)form;
		  
		 String userName=loginform.getEmailid();
		 System.out.println("emailid in action"+userName);
		 String password=loginform.getPassword();
		  String course=request.getParameter("course");
		 String price=request.getParameter("price");
		 String courseid=request.getParameter("corseiddd");
		  System.out.println("action****"+course);
		  System.out.println("action****"+price);
		  List<Login1Form> loginCredential=handler.loginValidation(userName,password,getDataSource(request));
		  
		  if(!loginCredential.isEmpty()){
			  HttpSession session=request.getSession();
			 
			  session.setAttribute("loginCredential", loginCredential);
			  session.setAttribute("emailId", loginCredential.iterator().next().getEmailid());
			  request.setAttribute("course", course);
			  request.setAttribute("price", price);
			  request.setAttribute("emailid",loginCredential.iterator().next().getEmailid());
			  request.setAttribute("couseid", courseid);
			    HashMap<String, String> globasetting = taxHandler.globasetting(getDataSource(request));
				request.setAttribute("globasetting", globasetting);
				String serviceTax=globasetting.get("service tax").toString();
				request.setAttribute("serviceTax", serviceTax);
			  
			  
			  
			  
            return mapping.findForward("sucesshomepageregister");
		  }
				 else
				 { 
					 status = "Username or password is Invalid";
				 request.setAttribute("status", status);
				 request.setAttribute("course1", course);
				  request.setAttribute("price1", price);
				return mapping.findForward("loginfail");
					
					  
				 }
		 
		  
				
				  
			  }
	
}

	