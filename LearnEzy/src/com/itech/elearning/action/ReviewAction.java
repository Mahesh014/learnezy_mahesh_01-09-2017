package com.itech.elearning.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.cete.dynamicpdf.imaging.tiff.f;
import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.RatingForm;
import com.itech.elearning.forms.ReviewForm;
import com.itech.elearning.handler.ReviewHandler;

public class ReviewAction extends BaseAction
{
	ReviewHandler handler = new ReviewHandler();
  public ActionForward list(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
  {
	  HttpSession session = request.getSession();
		Long userid = null;
		

		
	
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			List<ReviewForm> courseList = handler.list(userid,getDataSource(request));
			request.setAttribute("courseList", courseList);
			System.out.println("kkkk "+courseList);
			return mapping.findForward("reviews");
		} else
			return mapping.findForward("session");

  }
  
  public ActionForward add(ActionMapping mapping, ActionForm form,HttpServletRequest request,HttpServletResponse response)
  {
	  HttpSession session = request.getSession();
		Long userid = null;
		String emailid=null;
		

		ReviewForm review = (ReviewForm)form;
	
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
			emailid=((LoginForm) session.getAttribute("userDetail"))
			.getEmailid();
			System.out.println("emailidrrrrrrrrrrreeeeeeeeeeee"+emailid);
		
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			List<ReviewForm> courseList = handler.list(userid,getDataSource(request));
			request.setAttribute("courseList", courseList);
			System.out.println("kkkk "+courseList);
			
			
			request.setAttribute("status",handler.addrating(userid,review,getDataSource(request),emailid));
			
			
			return mapping.findForward("reviews");
		} else
			return mapping.findForward("session");
  }
  
}


