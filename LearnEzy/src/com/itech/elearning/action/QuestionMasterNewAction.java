package com.itech.elearning.action;

import java.io.IOException;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import org.json.simple.JSONArray;

import com.itech.elearning.forms.AssignmentMasterForm;
import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.QuestionMasterNewForm;

import com.itech.elearning.handler.QuestionMasterNewHandler;
import com.itech.elearning.utils.FileUploadDownloadUtil;

public class QuestionMasterNewAction  extends BaseAction{


	QuestionMasterNewHandler handler = new 	QuestionMasterNewHandler();

	public ActionForward list(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		QuestionMasterNewForm testForm = (QuestionMasterNewForm) form;
		HttpSession session = request.getSession();
        String compid = null;
		Long userid = null;
		int roleid = 0;
		
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			compid = ((LoginForm) session.getAttribute("userDetail")).getCompanyId();
			roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {

			List<QuestionMasterNewForm> courseList = handler.listAllCourse(roleid,userid,compid,getDataSource(request));
			request.setAttribute("courseList", courseList);

			List<QuestionMasterNewForm> topicList = handler.listAllTopic(roleid,userid,compid,testForm.getCourseId(), getDataSource(request));
			request.setAttribute("topicList", topicList);

			List<QuestionMasterNewForm> questionList = handler.listAllQuestion(roleid,compid,userid, getDataSource(request), testForm.getCourseId(),testForm.getTopicId());
			request.setAttribute("questionList", questionList);
	
			request.setAttribute("testForm", testForm);
			return mapping.findForward("QuestionMaster");
		}

		return list(mapping, testForm, request, response);

	}
	
	
	
	public ActionForward excelUpload(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		HttpSession session = request.getSession();
        String compid = null;
		Long userid = null;
		QuestionMasterNewForm testForm = (QuestionMasterNewForm) form;
		
		 if(!testForm.getExcelFile().equals("")){
				compid = ((LoginForm) session.getAttribute("userDetail")).getCompanyId();
			 FormFile myfile = testForm.getExcelFile();
			 String filePath = getServlet().getServletContext().getRealPath("/") +"upload";
		     String	 fileName =myfile.getFileName();
			 String fileName1 = myfile.getFileName();
		     boolean	 uploadStatus=FileUploadDownloadUtil.fileUploader(myfile, filePath, fileName);
		     filePath = getServlet().getServletContext().getRealPath("/")+ "upload\\";
			 String success = handler.testTypeExcelUpload(fileName, filePath,compid);
   			request.setAttribute("status", success);
		 }
		return list(mapping, form, request, response);
	}
	

	public ActionForward add(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		QuestionMasterNewForm testForm = new QuestionMasterNewForm();
		HttpSession session = request.getSession();
		String compid = null;
		compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		System.out.println("Topic Id ------->   "	+ request.getParameter("topicId"));
		testForm.setQuestion(request.getParameter("question"));
		testForm.setAnsA(request.getParameter("ansA"));
		testForm.setAnsB(request.getParameter("ansB"));
		testForm.setAnsC(request.getParameter("ansC"));
		testForm.setAnsD(request.getParameter("ansD"));
		testForm.setCourseId(Long.parseLong(request.getParameter("courseId")));
		testForm.setCrctAns(request.getParameter("crctAns"));
		testForm.setDescriptiveAns(request.getParameter("descriptiveAns"));
		request.setAttribute("status", handler.addQuestion(compid,testForm,getDataSource(request)));
		return list(mapping, form, request, response);
	}

	public ActionForward changestatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		System.out.println("hiiiiiiii");
		QuestionMasterNewForm courseForm = (QuestionMasterNewForm) form;
		String check = handler.changeStatus(courseForm, getDataSource(request),
				Long.parseLong(request.getParameter("questionId")));
		request.setAttribute("status", check);
		return list(mapping, form, request, response);
	}

	public ActionForward update(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
			.getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			QuestionMasterNewForm testForm = (QuestionMasterNewForm) form;

			request.setAttribute("status", handler.updateTest(testForm,
					getDataSource(request)));
			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");
	}

	
	
	
	public ActionForward delete(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		System.out.println("hiiiiiiii");
		QuestionMasterNewForm testForm = (QuestionMasterNewForm) form;
		String check = handler.delete(testForm, getDataSource(request),
				Integer.parseInt(request.getParameter("questionId")));
		request.setAttribute("status", check);
		return list(mapping, form, request, response);
	}

	public ActionForward questionByCourseid(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
	 
		HttpSession session = request.getSession();

		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
				String courseId=request.getParameter("courseId");
				System.out.println("courseId "+courseId);
				JSONArray jarry = new JSONArray();
				jarry= handler.questionByCourseid(getDataSource(request),courseId);
				 
				response.getOutputStream().write(jarry.toJSONString().getBytes());
				
		}
		return null;
	}
	public ActionForward excelReport(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{

		try{
			QuestionMasterNewForm testForm = (QuestionMasterNewForm) form;
			HttpSession session = request.getSession();

			Long userid = null;
			String compid = null;
			int roleid = 0;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
				compid = ((LoginForm) session.getAttribute("userDetail")).getCompanyId();
				roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {
				List<QuestionMasterNewForm> questionList = handler.listAllQuestion(roleid,compid,userid, getDataSource(request), testForm.getCourseId(),testForm.getTopicId());
				request.setAttribute("questionList", questionList);
				String fileName="Question_master";
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition","inline;filename="+""+fileName+".xls");
			} else{
				return mapping.findForward("session");
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
	return mapping.findForward("excel");

	}
	
	public ActionForward reset(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		QuestionMasterNewForm testForm = new QuestionMasterNewForm();
		 
		testForm.setQuestion("");
		testForm.setAnsA("");
		testForm.setAnsB("");
		testForm.setAnsC("");
		testForm.setAnsD("");
		testForm.setCourseId(Long.parseLong("-1"));
		testForm.setCrctAns("");
		testForm.setDescriptiveAns("");
		 return list(mapping, form, request, response);
	}
}
