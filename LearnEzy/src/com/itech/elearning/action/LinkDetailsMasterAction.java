package com.itech.elearning.action;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.itech.elearning.forms.LinkDetailsMasterForm;
import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.handler.LinkDetailsMasterHandler;


public class LinkDetailsMasterAction extends BaseAction {
	LinkDetailsMasterHandler handler = new LinkDetailsMasterHandler();

	public ActionForward list(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();

		Long userid = null;
		String compid = null;
		int roleid = 0;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			compid = ((LoginForm) session.getAttribute("userDetail")).getCompanyId();
			roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {

			List<LinkDetailsMasterForm> linkList = handler.list(roleid,compid,userid,getDataSource(request));
			request.setAttribute("linkList", linkList);
			System.out.println(linkList);

		}

		return mapping.findForward("linkList");

	}

	public ActionForward add(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		String compid = null;
		compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		LinkDetailsMasterForm lForm = (LinkDetailsMasterForm) form;
		request.setAttribute("status", handler.add(compid,lForm,getDataSource(request)));
		return list(mapping, form, request, response);
	}

	public ActionForward changestatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			LinkDetailsMasterForm lForm = (LinkDetailsMasterForm) form;
			Long linkId=Long.parseLong((request.getParameter("linkID")));
			boolean active=Boolean.parseBoolean(request.getParameter("active"));
			request.setAttribute("status", handler.changeStatus(lForm,getDataSource(request),linkId,active));
			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");
	}

	public ActionForward update(ActionMapping mapping, ActionForm form,	HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			LinkDetailsMasterForm lForm = (LinkDetailsMasterForm) form;
			request.setAttribute("status", handler.update(lForm,getDataSource(request)));
			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");
	}
	 
	
	public ActionForward addVa(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		LinkDetailsMasterForm lForm = (LinkDetailsMasterForm) form;
		HttpSession session = request.getSession();
		String compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		request.setAttribute("status", handler.add(compid,lForm,getDataSource(request)));
		return list(mapping, form, request, response);
	}
	public ActionForward excelReport(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{

		try{
			 
			HttpSession session = request.getSession();

			Long userid = null;
			String compid = null;
			int roleid = 0;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
				compid = ((LoginForm) session.getAttribute("userDetail")).getCompanyId();
				roleid = ((LoginForm) session.getAttribute("userDetail")).getRole();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {
				List<LinkDetailsMasterForm> linkList = handler.list(roleid,compid,userid,getDataSource(request));
				request.setAttribute("linkList", linkList);
				  String fileName="link_master";
				  response.setContentType("application/vnd.ms-excel");
				  response.setHeader("Content-Disposition","inline;filename="+""+fileName+".xls");
			} else{
				return mapping.findForward("session");
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
	return mapping.findForward("excel");

	}
}
