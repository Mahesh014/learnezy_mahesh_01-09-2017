package com.itech.elearning.action;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONObject;

import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.TestMasterForm;
import com.itech.elearning.handler.TakeTestHandler;

public class TestAction extends BaseAction{
	TakeTestHandler handler = new TakeTestHandler();
	public ActionForward  testList(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception {
		System.out.println("TEst page");
		HttpSession session = request.getSession();
		String compid = null;
		try {
			compid =((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			Long userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			if (userid != null) {
				String courseId=request.getParameter("courseId");
				List<TestMasterForm> testList = handler.listTestByCourseId(compid,userid,getDataSource(request),courseId);
				request.setAttribute("testList", testList);
		 	}

	} catch (NullPointerException e) {
		e.printStackTrace();
		return mapping.findForward("session");
	}
		return mapping.findForward("testPage");
	}
	
	public ActionForward  takeTest(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception {
		System.out.println("TEst page");
		HttpSession session = request.getSession();
		String compid = null;
		try {
			Long userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			if (userid != null) {
				String courseId=request.getParameter("courseId");
				String testId=request.getParameter("testId");
				int hour=0;
				 compid =((LoginForm)session.getAttribute("userDetail")).getCompanyId();
				
				
				List<TestMasterForm> testQuestionList = handler.takeTest(compid,userid,getDataSource(request),courseId,testId);
				
				int testTime=testQuestionList.iterator().next().getTestTime();
				hour=testTime*60;
				
				
				
				request.setAttribute("hour", hour);
				 
				
				
				
				
				System.out.println(testQuestionList);
				String[] questionIds=testQuestionList.iterator().next().getQuestionIds().split(",");
				
				TreeSet<Integer> treeSetQuestion=new TreeSet<Integer>(); 
				HashMap<Integer,String> answerQuestion=new HashMap<Integer,String>(); 
				for (int i = 0; i < questionIds.length; i++) {
					treeSetQuestion.add(Integer.parseInt(questionIds[i]));
				}
					
				session.setAttribute("testQuestionList", testQuestionList);
				session.setAttribute("treeSetQuestion", treeSetQuestion);
				session.setAttribute("answerQuestion", answerQuestion);
				
				//request.setAttribute("answerQuestion", answerQuestion);
				  
			}
		
	} catch (NullPointerException e) {
		e.printStackTrace();
		return mapping.findForward("session");
	}
		return mapping.findForward("testPage1");
	}
	
	@SuppressWarnings("unchecked")
	public ActionForward  testStarted(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		System.out.println("testStarted");
		try {
			Long userId = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			String compid = null;
			if (userId != null) {
			try {
				compid =((LoginForm)session.getAttribute("userDetail")).getCompanyId();
				TreeSet<Integer> treeSetQuestion=(TreeSet<Integer>) session.getAttribute("treeSetQuestion"); 
				JSONObject jObj=new JSONObject();
			 	int questionId=0;
				Iterator<Integer> itr=treeSetQuestion.iterator();  
				  if(itr.hasNext()){  
					  questionId=itr.next();
					  System.out.println("questionId "+ questionId);
				  }else{
					  questionId=-1;
				  }
				  
				  if (questionId!=-1 &&  questionId!=0) {
					  jObj=handler.getQuestion(getDataSource(request),questionId);
				  }else  
				  if (questionId==-1) {
					    Long courseId=((List<TestMasterForm>) session.getAttribute("testQuestionList")).iterator().next().getCourseId();
						Long testId=((List<TestMasterForm>) session.getAttribute("testQuestionList")).iterator().next().getTestId();;
						HashMap<Integer,String> answerQuestion=(HashMap<Integer,String>) session.getAttribute("answerQuestion"); 
						jObj=handler.insertTest(compid,getDataSource(request),answerQuestion,courseId,testId,userId);
				  }
				  response.getOutputStream().write(jObj.toJSONString().getBytes());
				
				  
			} catch (Exception e) {
				e.printStackTrace();
			}	
				
			
			}
		
	} catch (NullPointerException e) {
		e.printStackTrace();
		return mapping.findForward("session");
	}
		return null;
	}
	
	
	@SuppressWarnings("unchecked")
	public ActionForward  nextQuestion(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		System.out.println("nextQuestion");
		try {
			Long userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			if (userid != null) {
			try {
				
				HashMap<Integer,String> answerQuestion=(HashMap<Integer,String>) session.getAttribute("answerQuestion"); 
				TreeSet<Integer> treeSetQuestion=(TreeSet<Integer>) session.getAttribute("treeSetQuestion"); 
				
				int questionId=Integer.parseInt(request.getParameter("questionId"));
				String ans=request.getParameter("ans");
				
				//System.out.println(questionId+" "+ans);
				answerQuestion.put(questionId, ans);
				treeSetQuestion.remove(questionId);
				
				session.setAttribute("treeSetQuestion", treeSetQuestion);
				session.setAttribute("answerQuestion", answerQuestion);
				
				return testStarted(mapping, form, request, response);
					
			} catch (Exception e) {
				e.printStackTrace();
			}	
				
			
			}
		
	} catch (NullPointerException e) {
		e.printStackTrace();
		return mapping.findForward("session");
	}
		return null;
	}
	
	
	@SuppressWarnings("unchecked")
	public ActionForward  testTimeComplete(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		System.out.println("testStarted");
		try {
			Long userId = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			String compid = null;
			if (userId != null) {
			try {
				JSONObject obj=new JSONObject();
				compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			 	Long courseId=((List<TestMasterForm>) session.getAttribute("testQuestionList")).iterator().next().getCourseId();
				
			 	Long testId=((List<TestMasterForm>) session.getAttribute("testQuestionList")).iterator().next().getTestId();;
				
				HashMap<Integer,String> answerQuestion=(HashMap<Integer,String>) session.getAttribute("answerQuestion"); 
				
				obj=handler.insertTest(compid,getDataSource(request),answerQuestion,courseId,testId,userId);
				
				response.getOutputStream().write(obj.toJSONString().getBytes());
			} catch (Exception e) {
				e.printStackTrace();
			}	
				
			
			}
		
	} catch (NullPointerException e) {
		e.printStackTrace();
		return mapping.findForward("session");
	}
		return null;
	}
}
