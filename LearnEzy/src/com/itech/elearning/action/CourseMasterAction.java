package com.itech.elearning.action;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFPictureData;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.PictureData;
import org.apache.poi.ss.usermodel.Row;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import org.json.simple.JSONArray;

import com.itech.elearning.forms.ContantMasterForm;
import com.itech.elearning.forms.CourseMasterForm;
import com.itech.elearning.forms.CourseOfferedForm;
import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.handler.CourseMasterHandler;
import com.itech.elearning.utils.FileUploadDownloadUtil;

public class CourseMasterAction extends BaseAction {

	CourseMasterHandler handler = new CourseMasterHandler();

	public ActionForward list(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		CourseMasterForm courseForm = (CourseMasterForm) form;
		HttpSession session = request.getSession();
		Long userid = null;
		String compid = null;
		int roleid = 0;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
            compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
            roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
            
        	List<CourseMasterForm> coursecatList = handler.listAllcategory(roleid,compid,userid,getDataSource(request));
			request.setAttribute("coursecatList", coursecatList);
			
			List<CourseMasterForm> courseofferedList = handler.listCourseOffered(courseForm,roleid,compid,userid,getDataSource(request)); 
            request.setAttribute("courseofferedList",courseofferedList);
            System.out.println("course offered  "+courseofferedList);
            
			List<CourseMasterForm> courseList = handler.listAll(roleid,compid,userid,getDataSource(request));
			request.setAttribute("courseList", courseList);

			return mapping.findForward("user");
		}
		return list(mapping, courseForm, request, response);

	}
	
	
	public ActionForward excelUpload(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		try
		{
			
		CourseMasterForm courseForm = (CourseMasterForm) form;
		HttpSession session = request.getSession();
		Long userid = null;
		String compid = null;
		int n=0;
		int countofvalues=0;
		int totalcount=1;
		 int as=0;
		int previousId=0; 
		 int userID=0;
		 
		 int startingNo=1;
		 int endingNo=5;
	
		int rowNo=0;	

		String newPhoto="";
		String 	Filename="";
		boolean uploadstatus=false;
		String coursecatid="";
		String coursename="";
		String maxstudent="";
		String  coursedu="";
		String percentage="";
		String discont="";
		String validupto="";
		String free="";
		String amount="";
		String elearing="";
		String classroom="";
		String level="";
		String instrucorname="";
		String availseats="";
		String coursestartdate="";
		
		
		
		  List<CourseMasterForm> ld = null;


		 if(!courseForm.getExcelFile().equals("")){
			  compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			 FormFile myfile = courseForm.getExcelFile();
			 String filePath = getServlet().getServletContext().getRealPath("/") +"upload";
		     String	 fileName =myfile.getFileName();
			 myfile.getFileName();
		     boolean	 uploadStatus=FileUploadDownloadUtil.fileUploader(myfile, filePath, fileName);
		     filePath = getServlet().getServletContext().getRealPath("/")+ "upload\\";
		 


			
   			compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			 
			 uploadstatus=FileUploadDownloadUtil.fileUploader(myfile, filePath, fileName);
		     if (uploadstatus == true) {
				filePath = getServlet().getServletContext().getRealPath("/")+"upload\\";
				InputStream input = new BufferedInputStream(new FileInputStream(filePath+"\\"+fileName));
				POIFSFileSystem fs = new POIFSFileSystem(input);
				HSSFWorkbook wb = new HSSFWorkbook(fs);
				HSSFSheet sheet = wb.getSheetAt(0);
				Iterator<Row> rows = sheet.rowIterator();
				DataFormatter data=null;
				HSSFRow row=null;
				List<HSSFPictureData> lst = null;//wb.getAllPictures();
				ListIterator<HSSFPictureData> it = null;//lst.listIterator();
				int i=0;

				
				while (rows.hasNext()) {
					 row =  (HSSFRow) rows.next();

					 if(row.getRowNum()==0){
						   continue; //just skip the rows if row number is 0 or 1
						  }
					data = new DataFormatter();
					coursecatid = data.formatCellValue(row.getCell(0));
					coursename = data.formatCellValue(row.getCell(1));
					maxstudent = data.formatCellValue(row.getCell(2));
					coursedu= data.formatCellValue(row.getCell(3));
					percentage= data.formatCellValue(row.getCell(4));
					discont= data.formatCellValue(row.getCell(5));
					validupto= data.formatCellValue(row.getCell(6));
					free= data.formatCellValue(row.getCell(7));
					amount= data.formatCellValue(row.getCell(8));
					elearing= data.formatCellValue(row.getCell(9));
					classroom= data.formatCellValue(row.getCell(10));
					level= data.formatCellValue(row.getCell(11));
					instrucorname= data.formatCellValue(row.getCell(12));
					availseats= data.formatCellValue(row.getCell(13));
					coursestartdate= data.formatCellValue(row.getCell(14));










					//emailId = data.formatCellValue(row.getCell(3));
					
					i=row.getRowNum();
					rowNo=row.getRowNum();
					@SuppressWarnings("unused")
			List lst1 = (List) wb.getAllPictures();
					
					boolean tot=false;
						
								newPhoto="";
								
							    PictureData pict = (PictureData)lst1.get(i-1);
							    String ext = pict.suggestFileExtension();
							    byte[] image = pict.getData();
							 
							      	newPhoto = image+".jpg";
							      	System.out.println("///"+newPhoto);
									String filePathphotouplod = getServlet().getServletContext().getRealPath("/")+"courselogos\\"
											+ newPhoto;
									if(Filename==""){
										Filename=""+newPhoto;
									}else{
									Filename=Filename+","+newPhoto;
									System.out.println("Filenameeeeeeeeeeeeeeeeeeeeeeeee"+Filename);
									}
									
									
									FileOutputStream out = new FileOutputStream(filePathphotouplod);
							      out.write(image);
							      out.close();
							      	
						      
							//}
					//}
					   ld = new ArrayList<CourseMasterForm>();
					   courseForm.setCoursecategoryId(Integer.parseInt(coursecatid));
					   courseForm.setCourseName(coursename);
					   courseForm.setMaxStd(Integer.parseInt(maxstudent));
					   courseForm.setDuration(Integer.parseInt(coursedu));
					   courseForm.setPasspercent(percentage);
					   courseForm.setDispercentage(discont);
					   courseForm.setDiscountValidUpTo(validupto);
					   courseForm.setFree(free);
					   courseForm.setFees(Long.parseLong(amount));
					   courseForm.setElearning(elearing);
					   courseForm.setClassroom(classroom);
					   courseForm.setLevel(level);
					   courseForm.setInstructorname(instrucorname);
					   courseForm.setAvailableseats(availseats);
					   courseForm.setCoursestartdate(coursestartdate);
					   
                         ld.add(courseForm);
					      String success = handler.excelUpload(getDataSource(request), ld,Filename,n,rowNo,countofvalues,totalcount,compid);
			      request.setAttribute("status",success);
			      n++;
					
					//}
							startingNo+=5;
							endingNo+=5;	
					}
					
					//i++;
				}}
		}
		
	catch (Exception e) {
			e.printStackTrace();
		}
 			
   			
		 
		return list(mapping, form, request, response);
	}

	public ActionForward addCourse(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		System.out.println("hiiiiiiii");
		HttpSession session = request.getSession();
		String compid = null;
		compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		String imageUploadPath= getServlet().getServletContext().getRealPath("/")+ "courselogos";
		String imageUploadPath1= getServlet().getServletContext().getRealPath("/")+ "courselogos";
		String imagefileName="";
		String imagefileName1="";

		CourseMasterForm courseForm = (CourseMasterForm) form;
		boolean uploadStatus=true;
		FormFile imageFile= courseForm.getImageFile();
		if(imageFile.getFileName()!="" && imageFile.getFileSize()>2){
				imagefileName=imageFile.getFileName();
				System.out.println(imagefileName);
				uploadStatus = FileUploadDownloadUtil.fileUploader(imageFile, imageUploadPath, imagefileName);
		}	
		
		FormFile imageFile1= courseForm.getInstructorimage();
		if(imageFile1.getFileName()!="" && imageFile.getFileSize()>2){
				imagefileName1=imageFile1.getFileName();
				System.out.println(imagefileName1);
				uploadStatus = FileUploadDownloadUtil.fileUploader(imageFile1, imageUploadPath1, imagefileName1);
		}	
		
		
		System.out.println("jdfjhdjf "+courseForm.getCourseofferedname());
		String check = handler.addCourse(courseForm,compid ,imagefileName,imagefileName1,getDataSource(request));
		request.setAttribute("status", check);
		return list(mapping, form, request, response);
	}

	public ActionForward changestatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		System.out.println("hiiiiiiii");
		HttpSession session = request.getSession();
		String compid = null;
		//compid = ((LoginForm)session.getAttribute("userDetails")).getCompanyId();
		CourseMasterForm courseForm = (CourseMasterForm) form;
		long courseId=Long.parseLong(request.getParameter("courseid"));
		String check = handler.changeStatus(compid,courseForm, getDataSource(request),courseId);
		request.setAttribute("status", check);
		return list(mapping, form, request, response);
	}

	public ActionForward edit(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		CourseMasterForm courseForm = (CourseMasterForm) form;
		courseForm = handler.get(Long.parseLong(request.getParameter("id")),
				getDataSource(request));
		request.setAttribute("courseForm", courseForm);
		return list(mapping, courseForm, request, response);
	}

	public ActionForward updateCourse(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		String imageUploadPath= getServlet().getServletContext().getRealPath("/")+ "courselogos";
		String imageUploadPath1= getServlet().getServletContext().getRealPath("/")+ "courselogos";
		String imagefileName="";
		String imagefileName1="";
		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			CourseMasterForm courseForm = (CourseMasterForm) form;
			boolean uploadStatus=true;

			FormFile imageFile= courseForm.getImageFile();
			if(imageFile.getFileName()!="" && imageFile.getFileSize()>2){
					imagefileName=imageFile.getFileName();
					System.out.println(imagefileName);
					uploadStatus = FileUploadDownloadUtil.fileUploader(imageFile, imageUploadPath, imagefileName);
					
			}	
			else if(request.getParameter("hiddencoursename")!=null ||!request.getParameter("hiddencoursename").equals(""))
			{
				imagefileName=request.getParameter("hiddencoursename");
				
			}
			
			FormFile imageFile1= courseForm.getInstructorimage();
			if(imageFile1.getFileName()!="" && imageFile.getFileSize()>2){
					imagefileName1=imageFile1.getFileName();
					System.out.println(imagefileName1);
					
					System.out.println("inssssssssssssssssssssssssssssssssssssssimmmmmmmmmmmmmmm"+imagefileName1);
					uploadStatus = FileUploadDownloadUtil.fileUploader(imageFile1, imageUploadPath1, imagefileName1);
			}	
			else if(request.getParameter("hiddeninstructorname")!=null ||!request.getParameter("hiddeninstructorname").equals(""))
			{
				imagefileName1=request.getParameter("hiddeninstructorname");
				
			}
			
			request.setAttribute("status", handler.updateCourse(courseForm,
					getDataSource(request),imagefileName,imagefileName1));
			return list(mapping, courseForm, request, response);
		} else
			return mapping.findForward("session");

	}
	
	public ActionForward courseFees(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		 
		try {
			 
				JSONArray jArray = new JSONArray();
				String courseId=request.getParameter("courseId");
				
				jArray = handler.courseFees(getDataSource(request),courseId);
				response.getOutputStream().write(jArray.toJSONString().getBytes());
			 
		} catch (NullPointerException e) {
			e.printStackTrace();
			return mapping.findForward("session");
		}
		return null;
	}
	
	
	public ActionForward getcourse(ActionMapping mapping, ActionForm form, HttpServletRequest request,HttpServletResponse response)throws Exception {
		
		try {
			String catid=request.getParameter("catId");
			System.out.println("catid"+catid);
			

			JSONArray obj = handler.getcourse(getDataSource(request),request,response,catid);
				response.getOutputStream().write(obj.toString().getBytes());

				
				} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	
	public ActionForward excelReport(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{

		try{
			 
			HttpSession session = request.getSession();
            String compid = null;
			Long userid = null;
			int roleid = 0;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {
				compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
				roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
				List<CourseMasterForm> courseList = handler.listAll(roleid,compid,userid,	getDataSource(request));
				request.setAttribute("courseList", courseList);
				String fileName="Course_master";
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition","inline;filename="+""+fileName+".xls");
			} else{
				return mapping.findForward("session");
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
	return mapping.findForward("excel");

	}
}
