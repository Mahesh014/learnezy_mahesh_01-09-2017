package com.itech.elearning.action;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.elearning.forms.CityForm;
import com.itech.elearning.forms.ContryForm;
import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.StateForm;
import com.itech.elearning.forms.ReviewMasterForm;
import com.itech.elearning.handler.ReviewMasterHandler;
import com.itech.elearning.utils.ConnectionUtil;

public class ReviewMasterAction extends BaseAction {

	ReviewMasterHandler reviewHandler=new ReviewMasterHandler();
	public ActionForward list(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{

		try{
			
			HttpSession session=request.getSession();
			Long userid = null;
			String compid = null;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {
	           compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
	           ReviewMasterForm reviewForm = (ReviewMasterForm) form;
				
				List<ReviewMasterForm> reviewList = reviewHandler.list(compid,getDataSource(request));
				request.setAttribute("reviewList", reviewList);
				
			
			}else{
				return mapping.findForward("sessionfailure");
			}

		}catch (Exception e) {
			e.printStackTrace();
		}

	return mapping.findForward("review1");

	}

	
	public ActionForward add(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{
		try{
			HttpSession session=request.getSession();
			Long userid = null;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {
				String compid = null;
				compid = ((LoginForm) session.getAttribute("userDetail")).getCompanyId();
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
				
				System.out.println("compid="+compid);
				System.out.println("userid="+userid);
				
				
				
				ReviewMasterForm reviewForm = (ReviewMasterForm) form;
				
				request.setAttribute("status",reviewHandler.add(reviewForm,compid,userid,getDataSource(request)));
			}else{
				return mapping.findForward("sessionfailure");
			}

		}catch (Exception e) {
			e.printStackTrace();
		}
		return list(mapping, form, request, response);
	}

	
	public ActionForward changestatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception
			{

		try{
			HttpSession session=request.getSession();
			Long userid = null;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {
				ReviewMasterForm reviewForm = (ReviewMasterForm) form;
				reviewForm.setReviewId(Integer.parseInt(request.getParameter("reviewId")));
				reviewForm.setActive(Boolean.parseBoolean(request.getParameter("active")));
				request.setAttribute("status",reviewHandler.changeStatus(reviewForm,getDataSource(request)));
			}else{
				return mapping.findForward("sessionfailure");
			}

		}catch (Exception e) {
			e.printStackTrace();
		}
		return list(mapping, form, request, response);
			}

	
	public ActionForward activeCourseList(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws IOException
	{
		JSONArray returnVal = new JSONArray();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			con = ConnectionUtil.getMySqlConnection();
			String sql = "select * from coursemater where Active=1 ";
			System.out.println("sql  queary is-->"+sql);
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				
				JSONObject obj=new JSONObject();
				obj.put("courseId", rs.getInt("CourseId"));
				obj.put("courseName", rs.getString("CourseName"));
				
				returnVal.add(obj);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		response.getOutputStream().write(returnVal.toJSONString().getBytes());
		return null;
		
	}
	public ActionForward reviewlist(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{

		try{
			
			String letter=null;
			
			
				letter= request.getParameter("letter");
	           
	           ReviewMasterForm reviewForm = (ReviewMasterForm) form;
				
				List<ReviewMasterForm> reviewList = reviewHandler.reviewlist(getDataSource(request),letter);
				request.setAttribute("reviewList", reviewList);
				
			
		}catch (Exception e2) {
			e2.printStackTrace();
		}
	return mapping.findForward("reviewrating");

	}
public ActionForward reviewlistviewmore(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{

	try{
		
			//letter= request.getParameter("letter");
           
           ReviewMasterForm reviewForm = (ReviewMasterForm) form;
			
			List<ReviewMasterForm> reviewListmore = reviewHandler.reviewlistviewmore(getDataSource(request));
			request.setAttribute("reviewListmore", reviewListmore);
			
		
	}catch (Exception e2) {
		e2.printStackTrace();
	}
return mapping.findForward("reviewrating1");

}}
