package com.itech.elearning.action;

import java.util.List;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.StudentSessionClassForm;
import com.itech.elearning.forms.WhiteBoardForm;
import com.itech.elearning.handler.StudentSessionClassHandler;
import com.itech.elearning.handler.WhiteBoardHandler;
import com.itech.elearning.utils.StringUtil;

public class StudentSessionClassAction extends BaseAction{
	StudentSessionClassHandler handler = new StudentSessionClassHandler();
	WhiteBoardHandler handlerWhiteBoardHandle = new WhiteBoardHandler();
	public ActionForward list(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {

		 
		HttpSession session = request.getSession();

		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {

			 List<StudentSessionClassForm> appointmentScheduleList = handler.appointmentUserlist(getDataSource(request),userid);
			 request.setAttribute("appointmentScheduleList",appointmentScheduleList);
		}

		return mapping.findForward("appointmentScheduleList");

	}
	
	public ActionForward accessSession(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		 
		HttpSession session = request.getSession();

		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			String appointmentScheduleId = request.getParameter("appointmentScheduleId").trim();
			
			StudentSessionClassForm studentSessionClassForm = handler.getWhiteBoardSessionID(getDataSource(request),appointmentScheduleId);
			
			if (studentSessionClassForm != null && studentSessionClassForm.getSessionStatus().equals("Open") ) {
				String frameURL="http://www.twiddla.com/api/start.aspx?sessionid="+studentSessionClassForm.getWhiteBoardSessionID()+"&hide=chat,invite,profile,welcome,url,documents,email,widgets,math,room,settings";
				request.setAttribute("frameURL",frameURL);
				request.setAttribute("sessionPassword",studentSessionClassForm.getAccessPassword());
				request.setAttribute("sessionTitle",studentSessionClassForm.getWhiteBoardSessionTitle());
				return mapping.findForward("whiteBoard");
			}
		
		}
		return null;
	}
	
	public ActionForward getStudentSessionList(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();

		try {
			Long userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			if (userid != null) {
				JSONArray jArray = new JSONArray();
				jArray = handler.getStudentSessionList(getDataSource(request),userid);
				response.getOutputStream().write(jArray.toJSONString().getBytes());
			}

		} catch (NullPointerException e) {
			e.printStackTrace();
			return mapping.findForward("session");
		}
		return null;
	}
	
	
	public ActionForward accessVideoSession(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		 
		HttpSession session = request.getSession();

		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			String appointmentScheduleId = request.getParameter("appointmentScheduleId").trim();
			WhiteBoardForm 	whiteBoardForm1=handlerWhiteBoardHandle.getAppointmentDetails(getDataSource(request),appointmentScheduleId);
			
				String meetingtitle = whiteBoardForm1.getWhiteBoardSessionTitle();
				String meetingpassword =whiteBoardForm1.getAccessPassword();
				String meetingDateTime =whiteBoardForm1.getAppointmentDateTime();
				String meetingURLLink = "https://appear.in/"+StringUtil.repStrUnScr(meetingtitle+" "+meetingDateTime);
				request.setAttribute("frameURL", meetingURLLink);
				request.setAttribute("sessionPassword", meetingpassword);
				request.setAttribute("sessionTitle",meetingtitle);
				return mapping.findForward("videoChat");

			
		}
		return null;

	}

	 
}
