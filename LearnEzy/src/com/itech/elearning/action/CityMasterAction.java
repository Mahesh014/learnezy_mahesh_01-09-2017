
package com.itech.elearning.action;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONArray;

import com.itech.elearning.forms.CityForm;
import com.itech.elearning.forms.ContryForm;
import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.StateForm;
import com.itech.elearning.handler.CityHandler;
import com.itech.elearning.handler.CountryHandler;
import com.itech.elearning.handler.StateHandler;


public class CityMasterAction extends BaseAction 
{
	CountryHandler countryHandler = new CountryHandler();
	StateHandler stateHandler = new StateHandler();
	CityHandler cityHandler = new CityHandler();
	
	public ActionForward list(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{

		try{
			
			HttpSession session=request.getSession();
			Long userid = null;
			String compid = null;
			int roleid = 0;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
				
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {
	           compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
	           roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
				CityForm cityForm = (CityForm) form;
				
				List<ContryForm> countryList = countryHandler.activeList(roleid,compid,getDataSource(request));
				request.setAttribute("countryList", countryList);
				if(cityForm.getStateid()==0){
					List<StateForm> stateList = stateHandler.activeList(roleid,compid,getDataSource(request));
					request.setAttribute("stateList", stateList);
				}else{
					List<StateForm> stateList = stateHandler.activeListByCountry(roleid,compid,cityForm.getCountryid(),getDataSource(request));
					request.setAttribute("stateList", stateList);
					
				}
				List<CityForm> cityList = cityHandler.list(roleid,compid,cityForm.getStateid(),cityForm.getCountryid(),getDataSource(request));
				request.setAttribute("cityList", cityList);
			
			}else{
				return mapping.findForward("sessionfailure");
			}

		}catch (Exception e) {
			e.printStackTrace();
		}

	return mapping.findForward("city");

	}

	
	public ActionForward add(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{
		try{
			HttpSession session=request.getSession();
			Long userid = null;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {
				String compid = null;
				compid = ((LoginForm) session.getAttribute("userDetail")).getCompanyId();
				CityForm cityForm = (CityForm) form;
				request.setAttribute("status",cityHandler.add(cityForm,compid,getDataSource(request)));
			}else{
				return mapping.findForward("sessionfailure");
			}

		}catch (Exception e) {
			e.printStackTrace();
		}
		return list(mapping, form, request, response);
	}

	
	public ActionForward changestatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception
			{

		try{
			HttpSession session=request.getSession();
			Long userid = null;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {
				CityForm cityForm = (CityForm) form;
				cityForm.setCountryid(Long.valueOf(request.getParameter("countryid")));
				cityForm.setStateid(Long.valueOf(request.getParameter("stateid")));
				cityForm.setCityid(Long.valueOf(request.getParameter("cityid")));
				cityForm.setActive(Boolean.parseBoolean(request.getParameter("Active")));
				request.setAttribute("status",cityHandler.changeStatus(cityForm,getDataSource(request)));
			}else{
				return mapping.findForward("sessionfailure");
			}

		}catch (Exception e) {
			e.printStackTrace();
		}
		return list(mapping, form, request, response);
			}

	public ActionForward update(ActionMapping mapping, ActionForm form,	HttpServletRequest request, HttpServletResponse response) throws Exception	{
		try{
			HttpSession session=request.getSession();
			Long userid = null;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {
				
				CityForm cityForm = (CityForm) form;
				request.setAttribute("status", cityHandler.update(cityForm, getDataSource(request),userid));
			}else{
				return mapping.findForward("sessionfailure");
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return list(mapping, form, request, response);
	}

	
	public ActionForward reset(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception{
		try{
			HttpSession session=request.getSession();
			Long userid = null;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {
				CityForm cityForm = (CityForm) form;
				List<CityForm> cityList = new ArrayList<CityForm>();
				cityForm.setCityid(0);
				cityForm.setCityname("");
				cityForm.setStateid(0);
				cityList.add(cityForm);
				request.setAttribute("cityList", cityList);
			}else{
				return mapping.findForward("sessionfailure");
			}

		}catch (Exception e) {
			e.printStackTrace();
		}

		return list(mapping, form, request, response);
	}

	
	public ActionForward resetButton(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{
		try{
			HttpSession session=request.getSession();
			Long userid = null;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {
				CityForm cityForm = (CityForm) form;
				cityForm.setCityid(0);
				cityForm.setStateid(0);
				cityForm.setCountryid(0);
			}else{
				return mapping.findForward("sessionfailure");
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	return list(mapping, form, request, response);
	}
	
	
	
	public ActionForward excelReport(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{

		try{
			
			HttpSession session=request.getSession();
			Long userid = null;
			String compid = null;
			int roleid = 0;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
				compid = ((LoginForm) session.getAttribute("userDetail")).getCompanyId();
				roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {	
				CityForm cityForm = (CityForm) form;
				
				List<CityForm> cityList = cityHandler.list(roleid,compid,cityForm.getStateid(),cityForm.getCountryid(),getDataSource(request));
				request.setAttribute("cityList", cityList);
			
				String fileName="City_master";
			    response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition","inline;filename="+""+fileName+".xls");
			}else{
				return mapping.findForward("sessionfailure");
			}

		}catch (Exception e) {
			e.printStackTrace();
		}

	return mapping.findForward("excel");

	}
	
	
	public ActionForward ajaxActiveList(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception {
			String stateId=request.getParameter("stateId");
			JSONArray jArray =cityHandler.ajaxActiveList(getDataSource(request),stateId);
			response.getOutputStream().write(jArray.toJSONString().getBytes());
			return null;
	}

}
