package com.itech.elearning.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.RoleForm;
import com.itech.elearning.forms.StudentForm;
import com.itech.elearning.forms.SubTopicMasterForm;
import com.itech.elearning.forms.TestMasterForm;
import com.itech.elearning.handler.CourseMasterHandler;
import com.itech.elearning.handler.StudentHandler;
import com.itech.elearning.handler.SubTopicMasterHandler;
import com.itech.elearning.handler.TestMasterHandler;

public class StudentAction extends BaseAction {
	StudentHandler handler = new StudentHandler();
	CourseMasterHandler coursehandler = new CourseMasterHandler();
	TestMasterHandler topicHandler = new TestMasterHandler();
	SubTopicMasterHandler subTopicHandler = new SubTopicMasterHandler();

	public ActionForward courseList(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession();
		try {
			Long userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();

			if (userid != null) {
				JSONArray jArray = new JSONArray();
				jArray = handler.courseList(getDataSource(request), userid);
				response.getOutputStream().write(
						jArray.toJSONString().getBytes());
			}

		} catch (NullPointerException e) {
			e.printStackTrace();
			return mapping.findForward("session");
		}
		return null;
	}

	public ActionForward courseDetials(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();

		try {
			Long userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			String compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			int roleid = ((LoginForm) session.getAttribute("userDetail")).getRole();
			Long courseId = Long.valueOf(request.getParameter("courseId"));
			if (userid != null) {
				String courseName = coursehandler.getCourseNameByID(courseId,getDataSource(request));
				request.setAttribute("courseName", courseName);

				List<TestMasterForm> topicList = topicHandler.listAllTopic(	userid, courseId, getDataSource(request));
				request.setAttribute("topicList", topicList);

				List<SubTopicMasterForm> subTopicList = subTopicHandler.listSubTopic(roleid,compid,userid, 0, courseId,getDataSource(request));
				request.setAttribute("subTopicList", subTopicList);

				request.setAttribute("courseId", courseId);
			}

		} catch (NullPointerException e) {
			e.printStackTrace();
			return mapping.findForward("session");
		}
		return mapping.findForward("courseDetials");
	}

	public ActionForward contantList(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession();
		try {
			Long userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
			Long subTopicId = Long.valueOf(request.getParameter("subTopicId"));
			Long courseId = Long.valueOf(request.getParameter("courseId"));
			if (userid != null) {
				JSONObject jObj = new JSONObject();
				jObj = handler.contantList(getDataSource(request), subTopicId, courseId, userid);
				response.getOutputStream().write(jObj.toJSONString().getBytes());
				return null;
			}
		} catch (NullPointerException e) {
			e.printStackTrace();
			return mapping.findForward("session");
		}
		return mapping.findForward("courseDetials");
	}

	public ActionForward lastView(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession();
		try {
			Long userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();

			if (userid != null) {
				Long courseId = Long.valueOf(request.getParameter("courseId"));
				long subTopicId = handler.lastView(getDataSource(request),courseId, userid);				
				JSONObject jObj = new JSONObject();
				jObj = handler.contantList(getDataSource(request), subTopicId,courseId, userid);
				response.getOutputStream().write(jObj.toJSONString().getBytes());
			}

		} catch (NullPointerException e) {
			e.printStackTrace();
			return mapping.findForward("session");
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public ActionForward addLastView(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();

		try {
			Long userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
			Long courseId = Long.valueOf(request.getParameter("courseId"));
			Long topicId = Long.valueOf(request.getParameter("topicId"));
			Long subTopicId = Long.valueOf(request.getParameter("subTopicId"));
			if (userid != null) {
				JSONObject jObj = new JSONObject();
				jObj.put("subTopicId", handler.addLastView(
						getDataSource(request), courseId, topicId, subTopicId,
						userid));
				response.getOutputStream()
						.write(jObj.toJSONString().getBytes());

			}

		} catch (NullPointerException e) {
			e.printStackTrace();
			return mapping.findForward("session");
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public ActionForward addStudentAssignment(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();

		try {
			Long userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
			Long assignmentId = Long.valueOf(request
					.getParameter("assignmentId"));
			if (userid != null) {
				JSONObject jObj = new JSONObject();
				jObj.put("flag", handler.addStudentAssignment(
						getDataSource(request), assignmentId, userid));
				response.getOutputStream()
						.write(jObj.toJSONString().getBytes());

			}

		} catch (NullPointerException e) {
			e.printStackTrace();
			return mapping.findForward("session");
		}
		return null;
	}

	
	@SuppressWarnings("unchecked")
	public ActionForward getCourseCompleteCount(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();

		try {
			Long userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			if (userid != null) {
				JSONObject jObj = new JSONObject();
				jObj.put("courseCount", handler.getCourseCompleteCount(getDataSource(request), userid));
				response.getOutputStream().write(jObj.toJSONString().getBytes());
			}

		} catch (NullPointerException e) {
			e.printStackTrace();
			return mapping.findForward("session");
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public ActionForward getAchievementCompleteCount(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();

		try {
			Long userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			if (userid != null) {
				JSONObject jObj = new JSONObject();
				jObj.put("achievementCount", handler.getAchievementCompleteCount(getDataSource(request), userid));
				response.getOutputStream().write(jObj.toJSONString().getBytes());
			}

		} catch (NullPointerException e) {
			e.printStackTrace();
			return mapping.findForward("session");
		}
		return null;
	}

	
	@SuppressWarnings("unchecked")
	public ActionForward getTestCompleteCount(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();

		try {
			Long userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			if (userid != null) {
				JSONObject jObj = new JSONObject();
				jObj.put("testCount", handler.getTestCompleteCount(getDataSource(request), userid));
				response.getOutputStream().write(jObj.toJSONString().getBytes());
			}

		} catch (NullPointerException e) {
			e.printStackTrace();
			return mapping.findForward("session");
		}
		return null;
	}

	
	@SuppressWarnings("unchecked")
	public ActionForward getAssignmentCount(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();

		try {
			Long userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
			if (userid != null) {
				JSONObject jObj = new JSONObject();
				jObj.put("assignmentCount", handler.getAssignmentCount(
						getDataSource(request), userid));
				response.getOutputStream()
						.write(jObj.toJSONString().getBytes());

			}

		} catch (NullPointerException e) {
			e.printStackTrace();
			return mapping.findForward("session");
		}
		return null;
	}

	public ActionForward getAssignmentTopicId(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		try {
			Long userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
			Long courseId = Long.valueOf(request.getParameter("courseId"));
			if (userid != null) {
				JSONArray jArray = new JSONArray();
				jArray = handler.getAssignmentTopicId(getDataSource(request),
						userid, courseId);
				response.getOutputStream().write(
						jArray.toJSONString().getBytes());
			}

		} catch (NullPointerException e) {
			e.printStackTrace();
			return mapping.findForward("session");
		}
		return null;
	}

	public ActionForward getAssignmentQuestionbyTopicID(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		try {
			Long userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
			Long topicId = Long.valueOf(request.getParameter("topicId"));
			if (userid != null) {
				JSONArray jArray = new JSONArray();
				jArray = handler.getAssignmentByTopicId(getDataSource(request),
						userid, null, topicId);
				response.getOutputStream().write(
						jArray.toJSONString().getBytes());
			}

		} catch (NullPointerException e) {
			e.printStackTrace();
			return mapping.findForward("session");
		}
		return null;
	}
	
	public ActionForward getAssignmentQuestionbySubTopicID(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		try {
			Long userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			Long subTopicId = Long.valueOf(request.getParameter("subTopicId"));
			Long courseId = Long.valueOf(request.getParameter("courseId"));
			if (userid != null) {
				JSONArray jArray = new JSONArray();
				jArray = handler.getAssignmentQuestionbySubTopicID(getDataSource(request),	userid, courseId, subTopicId);
				response.getOutputStream().write(
						jArray.toJSONString().getBytes());
			}

		} catch (NullPointerException e) {
			e.printStackTrace();
			return mapping.findForward("session");
		}
		return null;
	}


	public ActionForward getKnowledgeBank(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();

		try {
			Long userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
			if (userid != null) {
				Long courseId = Long.valueOf(request.getParameter("courseId"));
				JSONArray jArray = new JSONArray();
				System.out.println(courseId + "::::::::::");
				jArray = handler.getKnowledgeBank(getDataSource(request),
						userid, courseId);
				response.getOutputStream().write(
						jArray.toJSONString().getBytes());

			}

		} catch (NullPointerException e) {
			e.printStackTrace();
			return mapping.findForward("session");
		}
		return null;
	}

	public ActionForward getResources(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();

		try {
			Long userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
			if (userid != null) {
				Long courseId = Long.valueOf(request.getParameter("courseId"));
				JSONArray jArray = new JSONArray();
				System.out.println(courseId + "::::::::::");
				jArray = handler.getResources(getDataSource(request), userid,
						courseId);
				response.getOutputStream().write(
						jArray.toJSONString().getBytes());

			}

		} catch (NullPointerException e) {
			e.printStackTrace();
			return mapping.findForward("session");
		}
		return null;
	}

	public ActionForward getLinks(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();

		try {
			Long userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			if (userid != null) {
				JSONArray jArray = new JSONArray();
				jArray = handler.getLinks(getDataSource(request), userid);
				response.getOutputStream().write(jArray.toJSONString().getBytes());
			}

		} catch (NullPointerException e) {
			e.printStackTrace();
			return mapping.findForward("session");
		}
		return null;
	}
	public ActionForward getNotification(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();

		try {
			Long userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			if (userid != null) {
				JSONArray jArray = new JSONArray();
				jArray = handler.getNotification(getDataSource(request), userid);
				response.getOutputStream().write(jArray.toJSONString().getBytes());
			}
		} catch (NullPointerException e) {
			e.printStackTrace();
			return mapping.findForward("session");
		}
		return null;
	}
	public ActionForward getAudioFiles(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();

		try {
			Long userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			if (userid != null) {
				JSONArray jArray = new JSONArray();
				String audioUploadPath= getServlet().getServletContext().getRealPath("/")+ "audioFiles";
				
				jArray = handler.getAudioFiles(getDataSource(request), userid,audioUploadPath);
				response.getOutputStream().write(jArray.toJSONString().getBytes());
			}

		} catch (NullPointerException e) {
			e.printStackTrace();
			return mapping.findForward("session");
		}
		return null;
	}
	public ActionForward addRating(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession();
		try {
			Long userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			String courseIdRating=request.getParameter("courseIdRating");
			String rating=request.getParameter("rating");
			
			String companyId1 = ((LoginForm) session.getAttribute("userDetail")).getCompanyId();
			
			
			int companyId=Integer.parseInt(companyId1);
			//String compid = null;
			//compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			System.out.println("userid="+userid);
			System.out.println("courseId="+courseIdRating);
			System.out.println("rating="+rating);
			//System.out.println("companyId1="+compid);
			
			
			//int companyId=0;
			
			if (userid != null) {
				System.out.println("userid11111111111="+userid);
				
				JSONArray jArray = new JSONArray();
				jArray = handler.addRating(getDataSource(request), userid,courseIdRating,rating,companyId);
				response.getOutputStream().write(jArray.toJSONString().getBytes());
			}

		} catch (NullPointerException e) {
			e.printStackTrace();
			return mapping.findForward("session");
		}
		return null;
	}
	public ActionForward mynotes(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
System.out.println("mynotessssssssssssssssss");
		HttpSession session = request.getSession();
		Long userid = null;
		String compid = null;
		int courseid=0;
		
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
			courseid=Integer.parseInt(request.getParameter("courseId"));
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			List<StudentForm> mynotes = handler.list(getDataSource(request),userid,courseid);
			request.setAttribute("mynotes", mynotes);
			request.setAttribute("courseid", courseid);
			return mapping.findForward("mynotes");
		} else
			return mapping.findForward("session");

	}
	public ActionForward mynotesadd(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		Long userid = null;
		String compid = null;
		int courseid=0;
		
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
			courseid=Integer.parseInt(request.getParameter("courseId"));
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
						
			StudentForm studentform = (StudentForm) form;
			System.out.println("studentforms"+studentform.getNotes());
			request.setAttribute("status", handler.addmynotes(getDataSource(request),userid,courseid,studentform));
			return mynotes(mapping, form, request, response);
			
			
			
			
		} else
			return mapping.findForward("session");

	}
	public ActionForward clear(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws IOException{
		System.out.println("systemmmmmmmmmmmmmmmmmmmmmmmmmmmmmm");
		@SuppressWarnings("unused")

	

			 JSONObject obj=new JSONObject();
			 obj.put("clr", 0);
			 
			 response.getOutputStream().write(obj.toJSONString().getBytes());
			return null;
		
	}	

	
}
