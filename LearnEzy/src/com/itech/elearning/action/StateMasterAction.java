package com.itech.elearning.action;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONArray;

import com.itech.elearning.forms.ContryForm;
import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.StateForm;
import com.itech.elearning.handler.CountryHandler;
import com.itech.elearning.handler.StateHandler;

public class StateMasterAction extends BaseAction {  

	CountryHandler countryhandler = new CountryHandler();
	StateHandler handler = new StateHandler();

	
	public ActionForward list(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{
		StateForm stateForm = (StateForm)form;
		try{
			HttpSession session=request.getSession();
			Long userid = null;
			String compid = null;
			int roleid = 0;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {	 
				compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
				roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
				List<ContryForm> countryList = countryhandler.activeList(roleid,compid,getDataSource(request));
				request.setAttribute("countryList", countryList);
				
				List<StateForm> stateList = handler.list(roleid,stateForm.getCountryid(),compid,getDataSource(request));
				request.setAttribute("stateList", stateList);
			}else{
				return mapping.findForward("sessionfailure");
			}

		}catch (Exception e) {
			e.printStackTrace();
		}

		return mapping.findForward("state");

	}

	
	public ActionForward add(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{
		try{
			HttpSession session=request.getSession();
			Long userid = null;
			String compid = null;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {	 
				compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
				StateForm stateForm = (StateForm)form;
				request.setAttribute("status",handler.addState(compid,stateForm,getDataSource(request)));
			}else{
				return mapping.findForward("sessionfailure");
			}
		}catch (Exception e) {
			e.printStackTrace();
		}

		return list(mapping, form, request, response);
	}

	
	public ActionForward changestatus(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{

		try{
			HttpSession session=request.getSession();
			Long userid = null;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {	 
				StateForm stateForm = (StateForm)form;
				stateForm.setStateid(Long.valueOf(request.getParameter("stateid")));
				stateForm.setCountryid(Long.valueOf(request.getParameter("countryid")));
				stateForm.setActive(Boolean.parseBoolean(request.getParameter("active")));
				request.setAttribute("status", handler.changeState(stateForm, getDataSource(request)));
			}else{
				return mapping.findForward("sessionfailure");
			}

		}catch (Exception e) {
			e.printStackTrace();
		}
		return list(mapping, form, request, response);
	}

	
	public ActionForward update(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{
		try{
			HttpSession session=request.getSession();
			Long userid = null;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {	 
				 
				StateForm stateForm = (StateForm)form;
				request.setAttribute("status",handler.updateState(stateForm, getDataSource(request),userid));
			}else{
				return mapping.findForward("sessionfailure");
			}

		}catch (Exception e) {
			e.printStackTrace();
		}

		return list(mapping, form, request, response);
	}
	
	
	public ActionForward resetButton(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{
		try{
			HttpSession session=request.getSession();
			Long userid = null;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {	 
				StateForm stateForm = (StateForm)form;
				stateForm.setCountryid(-1);
			}else{
				return mapping.findForward("sessionfailure");
			}

		}catch (Exception e) {
			e.printStackTrace();
		}

		return list(mapping, form, request, response);
		
	}

	
	public ActionForward excelReport(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{
		StateForm stateForm = (StateForm)form;
		try{
			HttpSession session=request.getSession();
			Long userid = null;
			String compid = null;
			int roleid = 0;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {	 
			    compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			    roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
				List<StateForm> stateList = handler.list(roleid,stateForm.getCountryid(),compid,getDataSource(request));
				request.setAttribute("stateList", stateList);
				String fileName="State_master";
			    response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition","inline;filename="+""+fileName+".xls");
			}else{
				return mapping.findForward("sessionfailure");
			}

		}catch (Exception e) {
			e.printStackTrace();
		}

		return mapping.findForward("excel");
	}
	
	
	public ActionForward ajaxActiveList(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception {
	  		String countryId=request.getParameter("countryId");
	  		HttpSession session = request.getSession();
	  		//String compid = null;
	  		//compid =((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			JSONArray jArray =handler.ajaxActiveList(getDataSource(request),countryId);
			response.getOutputStream().write(jArray.toJSONString().getBytes());
		    return null;
	}
}
