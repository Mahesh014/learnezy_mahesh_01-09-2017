package com.itech.elearning.action;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import org.json.simple.JSONObject;
import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.SampleProgramMasterForm;
import com.itech.elearning.handler.SampleProgramMasterHandler;
import com.itech.elearning.utils.FileUpload;

public class SampleProgramMasterAction extends BaseAction {


	SampleProgramMasterHandler handler = new SampleProgramMasterHandler();



	public ActionForward list(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		long userid = 0;
		String compid = null;
		int roleid = 0;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != 0) {
			compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			List<SampleProgramMasterForm> courseList = handler.listAllCourse(roleid,userid,compid,getDataSource(request));
			request.setAttribute("courseList", courseList);
			
			List<SampleProgramMasterForm> sampleProgramList = handler.listSampleProgram(roleid,userid,compid,getDataSource(request));
			request.setAttribute("sampleProgramList", sampleProgramList);
			
		}else{
			return mapping.findForward("session");
		}

		 
		return mapping.findForward("sampleProgram");
	}
	
	

	public ActionForward add(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		SampleProgramMasterForm subtopicForm = (SampleProgramMasterForm) form;
		HttpSession session = request.getSession();
		String compid = null;
		compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		String check = handler.add(subtopicForm,compid,getDataSource(request));
		request.setAttribute("status", check);
		return list(mapping, form, request, response);
	}

	public ActionForward changestatus(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {

		SampleProgramMasterForm subtopicForm = (SampleProgramMasterForm) form;
		String check = handler.changeStatus(subtopicForm, getDataSource(request),Long.parseLong(request.getParameter("sampleProgramID")));
		request.setAttribute("status", check);
		return list(mapping, form, request, response);
	}
	
	
	public ActionForward update(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		long userid = 0;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != 0) {
			System.out.println("Update");
			SampleProgramMasterForm subtopicForm = (SampleProgramMasterForm) form;
			request.setAttribute("status", handler.update(subtopicForm,getDataSource(request)));
			return list(mapping, subtopicForm, request, response);
		} else
			return mapping.findForward("session");
	}

	
	public ActionForward edit(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		try {
			long sampleProgramID=Long.parseLong(request.getParameter("sampleProgramID"));
			System.out.println("sampleProgramID ID: "+sampleProgramID);
			JSONObject obj=handler.getEdit(getDataSource(request),sampleProgramID);
			response.getOutputStream().write(obj.toString().getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public ActionForward excelReport(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{

		try{
			
			HttpSession session = request.getSession();
      
			Long userid = null;
			String compid = null;
			int roleid = 0;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
				roleid = ((LoginForm) session.getAttribute("userDetail")).getRole();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {
				compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
				List<SampleProgramMasterForm> sampleProgramList = handler.listSampleProgram(roleid,userid,compid,getDataSource(request));
				request.setAttribute("sampleProgramList", sampleProgramList);
				String fileName="sampleProgram_master";
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition","inline;filename="+""+fileName+".xls");
			} else{
				return mapping.findForward("session");
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
	return mapping.findForward("excel");

	}
	
public ActionForward sampleExcelUpload(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{
		
		SampleProgramMasterForm subtopicForm = (SampleProgramMasterForm) form;
	

		Long userid = null;
		String compid = null;
		String  fileName=null;
		boolean uploadstatus=false;
		HttpSession session = request.getSession();

		String filePath=null;
		  try {
			  

			  if(!subtopicForm.getFileName().equals("")) { 
					compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();


					 FormFile myfile = subtopicForm.getFileName();
		 
					 filePath = getServlet().getServletContext().getRealPath("/") +"upload";
					 System.out.println("File Path="+filePath);
					 
					 fileName =myfile.getFileName();
					 String fileName1 = myfile.getFileName();
					 System.out.println("Filename="+fileName);
					 
					 
					 //Excel or csv file upload
					 uploadstatus=FileUpload.fileUploader(myfile, filePath, fileName);
					
						 filePath = getServlet().getServletContext().getRealPath("/")+ "upload\\";
						 						
						String success = handler.sampleProgramExcelUpload(fileName, filePath,compid);
						System.out.println("prashanth 111111111111111");
						
						request.setAttribute("status", success);
				 }
				  
				
		  }
		 catch (Exception e) {
					e.printStackTrace();
				}
		 return list(mapping, form, request, response);
	}
}
