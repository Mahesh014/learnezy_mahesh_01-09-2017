package com.itech.elearning.action;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONObject;

import com.itech.elearning.forms.CompanyMasterForm;
import com.itech.elearning.forms.ContryForm;
import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.UserRegisterForm;
import com.itech.elearning.handler.CityHandler;
import com.itech.elearning.handler.CompanyHandler;
import com.itech.elearning.handler.CountryHandler;
import com.itech.elearning.handler.StateHandler;
import com.itech.elearning.handler.UserRegisterHandler;

public class UserRegisterAction extends BaseAction {

	UserRegisterHandler handler = new UserRegisterHandler();
	CountryHandler countryHandler = new CountryHandler();
	StateHandler stateHandler = new StateHandler();
	CityHandler cityHandler = new CityHandler();
	
	public ActionForward list(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		UserRegisterForm userForm = (UserRegisterForm) form;
		HttpSession session = request.getSession();
		Long userid = null;
		String compid = null;
		int roleid = 0;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			compid =((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
			List<UserRegisterForm> roleList = handler.roleList(roleid,compid,getDataSource(request));
			request.setAttribute("roleList", roleList);

			List<UserRegisterForm> userList = handler.listAll(roleid,compid,userid,getDataSource(request));
			request.setAttribute("usersList", userList);
			
			List<UserRegisterForm> courseList = handler.listAllCourse(roleid,compid,userid,getDataSource(request));
			request.setAttribute("courseList", courseList);
			
			List<ContryForm> countryList = countryHandler.activeList(roleid,compid,getDataSource(request));
			request.setAttribute("countryList", countryList);
			if(roleid == 1 || roleid == 9 )
			{
			List<UserRegisterForm> companyList = handler.list(getDataSource(request));
			request.setAttribute("companyList",companyList);
	
			System.out.println("If");
			}
			else
			{
				List<UserRegisterForm> companyList = handler.list(compid,getDataSource(request));
				request.setAttribute("companyList",companyList);
				System.out.println("else");
			}
			userForm.setCountryid(-1);
			userForm.setRoleId((long) -1);
			return mapping.findForward("user");
			
			
		}
		return list(mapping, userForm, request, response);

	}

	public ActionForward addUser(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {

		UserRegisterForm userForm = (UserRegisterForm) form;
		String check = handler.addUser(userForm, getDataSource(request));
		request.setAttribute("status", check);
		return list(mapping, form, request, response);
	}

	public ActionForward changestatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		System.out.println("hiiiiiiii");
		UserRegisterForm userForm = (UserRegisterForm) form;
		String check = handler.changeStatus(userForm, getDataSource(request));
		request.setAttribute("status", check);
		return list(mapping, form, request, response);
	}

	public ActionForward edit(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		UserRegisterForm userForm = (UserRegisterForm) form;
		userForm = handler.get(Long.parseLong(request.getParameter("id")),getDataSource(request));
		request.setAttribute("userForm", userForm);
		return list(mapping, userForm, request, response);
	}

	public ActionForward updateUser(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			UserRegisterForm userForm = (UserRegisterForm) form;
			request.setAttribute("status", handler.updateUser(userForm,
					getDataSource(request)));
			return list(mapping, userForm, request, response);
		} else
			return mapping.findForward("session");

	}
	
	@SuppressWarnings("unchecked")
	public ActionForward checkUserName(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
				JSONObject jObj = new JSONObject();
				String userName=request.getParameter("userName").trim();
				System.out.println(userName);
				if (userName!=null) {
					int flag=handler.checkUserName(getDataSource(request),userName);
					jObj.put("flag",flag);
					response.getOutputStream().write(jObj.toJSONString().getBytes());
				}
				return null;
	}
	
	public ActionForward excelReport(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{
	
		try{
			HttpSession session=request.getSession();
			Long userid = null;
			String compid = null;
			int roleid= 0;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {	 
			    compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			    roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
				List<UserRegisterForm> userList = handler.listAll(roleid,compid,userid,getDataSource(request));
				request.setAttribute("usersList", userList);
				
				
				String fileName="users_master";
			    response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition","inline;filename="+""+fileName+".xls");
			}else{
				return mapping.findForward("sessionfailure");
			}

		}catch (Exception e) {
			e.printStackTrace();
		}

		return mapping.findForward("excel");
	}

}
