package com.itech.elearning.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class LogoutAction extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {
			HttpSession session = request.getSession();
			Object o = session.getAttribute("username").toString();
			System.gc();
			if (o != null) {

				session.removeAttribute("username");
				session.invalidate();
				request.setAttribute("status", "Logout Successfully...!!!");
				return mapping.findForward("signup");
			} else {
				return mapping.findForward("signup");
			}
		} catch (NullPointerException e) {
			return mapping.findForward("signup");
		}

	}
}
