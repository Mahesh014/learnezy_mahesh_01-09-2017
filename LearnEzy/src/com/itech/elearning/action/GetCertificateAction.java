package com.itech.elearning.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONArray;

import com.itech.elearning.forms.GetCertificateForm;
import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.handler.GetCertificateHandler;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class GetCertificateAction extends BaseAction{
	GetCertificateHandler handler= new 	GetCertificateHandler();
	public ActionForward courseList(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession();
		try {
			Long userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
            String compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			if (userid != null) {
				JSONArray jArray = new JSONArray();
				jArray = handler.courseList(compid,getDataSource(request), userid);
				response.getOutputStream().write(jArray.toJSONString().getBytes());
			}

		} catch (NullPointerException e) {
			e.printStackTrace();
			return mapping.findForward("session");
		}
		return null;
	}
	public ActionForward getCertificateDetials(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		Long userid = null;
		String compid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			String courseId=request.getParameter("courseId");
			int courseCompFlag=handler.courseCompletedCheck(compid,getDataSource(request),userid,Integer.parseInt(courseId)) ;
			if (courseCompFlag==1) {
				List< GetCertificateForm> getCertificateDetials = handler.getCertificateDetials(compid,userid,getDataSource(request),courseId);
				request.setAttribute("getCertificateDetials", getCertificateDetials);
			}
			
		}
		return mapping.findForward("ecertificate");
	}
	
	public ActionForward getCertificatePDF(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			String courseName=request.getParameter("courseName");
			String studentName=request.getParameter("studentName");
			String passpercent=request.getParameter("passpercent");
			String courseComplDate=request.getParameter("courseComplDate");
			
			String itechLogofilePath = getServlet().getServletContext().getRealPath("/")+"/images/Itech Solutions logo.png";
			String fontPath = getServlet().getServletContext().getRealPath("/")+"/images/OpenSans-Regular.ttf";
			String filePath = getServlet().getServletContext().getRealPath("/")+"e-certificate"+"/"+"e-certificate.pdf";
			
			System.out.println("filePath is=====>"+filePath);
			try {
					new GetCertificateAction().pdfConverter(courseName,studentName,passpercent,courseComplDate,filePath,itechLogofilePath,fontPath);
				}catch (Exception e) {
					e.printStackTrace();
				}
				System.out.println("file download inside the leavemoduleaction ");
				String file1 = null;
				file1 = "e-certificate.pdf";
				//return an application file instead of html page
		      response.setContentType("application/octet-stream");
		     
		      response.setHeader("Content-Disposition","attachment;filename=" +file1);
		    
		   try {
		      	String filename1 = getServlet().getServletContext().getRealPath("\\")+"e-certificate\\"+file1;
		      	File fileToDownload1 = new File(filename1);
		      	FileInputStream in = new FileInputStream(fileToDownload1);
		      	
		      	ServletOutputStream out = response.getOutputStream();
		      	 
		      	byte[] outputByte = new byte[4096];
		      	while(in.read(outputByte, 0, 4096) != -1)
		      	{
		      		out.write(outputByte, 0, 4096);
		      	}
		      	in.close();
		      	out.flush();
		      	out.close();
		      	return null;         
		  	}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return mapping.findForward("ecertificate");
	}
	
	
	public void pdfConverter(String courseName, String studentName, String passpercent, String courseComplDate , String filePath, String itechLogofilePath, String fontPath) {
		try {
			/*----------------------------------------------------------------PAGE SETUP AND CREATING A PDF -----------------------------------------------*/    
		    
			
			 BaseColor color1 = WebColors.getRGBColor("#E58B30");
			 BaseColor color2 = WebColors.getRGBColor("#069");
			 BaseColor color3 = WebColors.getRGBColor("#F28858");	
			 BaseColor color4 = WebColors.getRGBColor("#787878");	
			 
			 BaseFont base = BaseFont.createFont(fontPath, BaseFont.WINANSI, true);
			 
			 Font font1 = new Font(base,30, Font.BOLDITALIC);
			 Font font2 = new Font(base,24,Font.ITALIC, color2 );
			 Font font3 = new Font(base,26,Font.ITALIC,color3);
			 Font font4 = new Font(base,12,Font.ITALIC,color3);
			 Font font5 = new Font(base,16,Font.ITALIC,color3);		
			
			
			
			
			 Document document = new Document();
		     OutputStream file = new FileOutputStream(new File(filePath));
		     document.setPageSize(PageSize.A4.rotate());
		     //document.setMargins(108,25,15,15);
		     document.setMarginMirroring(false);
		     PdfWriter.getInstance(document, file);
		     document.open();
		     
		     
		     PdfPTable table1 = new PdfPTable(1); 
		     table1.setWidthPercentage(100);
		        PdfPCell	cell1 = new PdfPCell();
				cell1.setBorderWidthLeft  (6f);
				cell1.setBorderWidthRight (6f);
				cell1.setBorderWidthTop   (6f);
				cell1.setBorderWidthBottom(6f);
				cell1.setBorderColor(color4); 
				
				PdfPTable table2 = new PdfPTable(1); 
   		     	table2.setWidthPercentage(98);
   		     	table2.setSpacingBefore(10f);
   		     	table2.setSpacingAfter(10f);
				PdfPCell	cell2 = new PdfPCell();
  				cell2.setBorderWidthLeft  (3f);
  				cell2.setBorderWidthRight (3f);
  				cell2.setBorderWidthTop   (3f);
  				cell2.setBorderWidthBottom(3f);
  				cell2.setBorderColor(color4); ;
  				
  				
  				
  				PdfPTable table3 = new PdfPTable(2); 
   		     	table3.setWidthPercentage(85);
   		     	float[] columnWidths_table3 = new float[] {20f,80f};
   		     	table3.setWidths(columnWidths_table3);	
   		     	
   		     	
   		     	
   		     	Image image = Image.getInstance(itechLogofilePath);
	 			image.scaleAbsolute(100f,50f);
	 			
	 			PdfPCell cell3 = new PdfPCell(image, false);
	 			cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
	 			cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
	 			cell3.setBorder(Rectangle.NO_BORDER);
	 			cell3.setFixedHeight(75f);
	 			cell3.setBackgroundColor(color1);
  				table3.addCell(cell3);
  				
  				
  				cell3 = new PdfPCell(new PdfPCell(new Phrase("CERTIFICATE OF TRIANING",font1)));
  				cell3.setBorder(Rectangle.NO_BORDER);
  				cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
  				cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
  				cell3.setBackgroundColor(color1);
  				cell3.setFixedHeight(75f);
  				table3.addCell(cell3);
  				
  				 
  				
  				
  				cell3 = new PdfPCell(new Phrase("This is to certify that",font2));
  				cell3.setBorder(Rectangle.NO_BORDER);
  				cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
  				cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
  				cell3.setColspan(2);
  				cell3.setFixedHeight(50f);
  				table3.addCell(cell3);
  				
  				
  				cell3 = new PdfPCell(new Phrase(studentName,font3));
  				cell3.setBorder(Rectangle.NO_BORDER);
  				cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
  				cell3.setColspan(2);
  				cell3.setFixedHeight(40f);
  				table3.addCell(cell3);
  				
  				
  				cell3 = new PdfPCell(new Phrase("has completed the course ",font2));
  				cell3.setBorder(Rectangle.NO_BORDER);
  				cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
  				cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
  				cell3.setColspan(2);
  				cell3.setFixedHeight(50f);
  				table3.addCell(cell3);
  				
  				cell3 = new PdfPCell(new Phrase(courseName,font3));
  				cell3.setBorder(Rectangle.NO_BORDER);
  				cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
  				cell3.setColspan(2);
  				cell3.setFixedHeight(40f);
  				table3.addCell(cell3);
  				
  				
  				cell3 = new PdfPCell(new Phrase("with score of",font2));
  				cell3.setBorder(Rectangle.NO_BORDER);
  				cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
  				cell3.setColspan(2);
  				cell3.setFixedHeight(50f);
  				table3.addCell(cell3);
  				
  				cell3 = new PdfPCell(new Phrase(passpercent+" %",font3));
  				cell3.setBorder(Rectangle.NO_BORDER);
  				cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
  				cell3.setColspan(2);
  				cell3.setFixedHeight(40f);
  				table3.addCell(cell3);
  				
  				cell3 = new PdfPCell(new Phrase("dated",font2));
  				cell3.setBorder(Rectangle.NO_BORDER);
  				cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
  				cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
  				cell3.setColspan(2);
  				cell3.setFixedHeight(50f);
  				table3.addCell(cell3);
  				
  				
  				cell3 = new PdfPCell(new Phrase(courseComplDate,font5));
  				cell3.setBorder(Rectangle.NO_BORDER);
  				cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
  				cell3.setColspan(2);
  				cell3.setFixedHeight(40f);
  				table3.addCell(cell3);
  				
  				cell3 = new PdfPCell(new Phrase("COMPUTER GENERATED, SIGNATURE NOT REQUIRED",font4));
  				cell3.setBorder(Rectangle.NO_BORDER);
  				cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
  				cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
  				cell3.setFixedHeight(30f);
  				cell3.setColspan(2);
  				table3.addCell(cell3);
  				
  				
  				cell2.addElement(table3);
  				
  				table2.addCell(cell2);
  				
  				
  				cell1.addElement(table2);
		     
  				table1.addCell(cell1);
		     
		     
			             	
			 document.add(table1);             		
		     document.close();
		     file.close();
		      
		System.out.println("Pdf generated successfully");
		
		
		} catch (Exception e) {
			e.printStackTrace();
		}  	
		 
	}	
	 
	
}
