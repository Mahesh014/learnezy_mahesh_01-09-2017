package com.itech.elearning.action;

import java.util.List;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.PassPercentForm;
import com.itech.elearning.handler.PassPercentHandler;


public class PassPercentAction extends BaseAction {

	PassPercentHandler handler=new PassPercentHandler();	

	public ActionForward list(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		PassPercentForm passpercentForm = (PassPercentForm) form;
		HttpSession session = request.getSession();
		Long userid = null;
		String compid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			List<PassPercentForm> passpercentList = handler.passpercentList(getDataSource(request),passpercentForm.getCourseId(),compid);
			request.setAttribute("passpercentList", passpercentList);

			List<PassPercentForm> courseList = handler.courcelist(compid,getDataSource(request));
			request.setAttribute("courseList", courseList);

			return mapping.findForward("passpercent");
		} else
			return mapping.findForward("session");

	}
	
	public ActionForward changestatus(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		PassPercentForm passpercentForm = (PassPercentForm) form;
		HttpSession session = request.getSession();
		String compid = null;
		compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		String check = handler.changeStatus(compid,passpercentForm, getDataSource(request),Long.parseLong(request.getParameter("passpercentid")));
		request.setAttribute("status", check);
		return list(mapping, form, request, response);
	}
	
	
	public ActionForward add(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		PassPercentForm passpercentForm = (PassPercentForm) form;
		HttpSession session = request.getSession();
		String compid = null;
		compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		request.setAttribute("status", handler.add(compid,passpercentForm,	getDataSource(request)));
		return list(mapping, form, request, response);
	}
	
	public ActionForward update(ActionMapping mapping, ActionForm form,	HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			PassPercentForm passpercentForm = (PassPercentForm) form;
			request.setAttribute("status", handler.update(passpercentForm,getDataSource(request)));
			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");
	}
}
