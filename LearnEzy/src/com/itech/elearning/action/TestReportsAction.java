package com.itech.elearning.action;

import java.util.List;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.TestReportsForm;
import com.itech.elearning.handler.TestReportsHandler;
import com.itech.elearning.utils.DateTimeUtil;
 
 
public class TestReportsAction extends BaseAction{
	TestReportsHandler handler = new TestReportsHandler();
	public ActionForward list(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();

		Long userid = null;
		String compid = null;
		int roleid = 0;
		
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		
		if (userid != null) {
			
			List<TestReportsForm> studentList = handler.studentList(roleid,compid,userid,getDataSource(request));
			request.setAttribute("studentList", studentList);
			
			List<TestReportsForm> courseList = handler.listAllCourse(roleid,compid,userid,getDataSource(request));
			request.setAttribute("courseList", courseList);
			
			String studentId="-1";
			String courseId="";
			String fromDate="";
			String todate="";
			
			List< TestReportsForm> testList = handler.testList(roleid,compid,userid,getDataSource(request),studentId,courseId,fromDate,todate);
			request.setAttribute("testList",testList);
			 
		}
		return mapping.findForward("testReports");

		

	}
	
	public ActionForward getList(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();

		Long userid = null;
		String compid = null;
		int roleid = 0;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		
		if (userid != null) {
			compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
			List<TestReportsForm> studentList = handler.studentList(roleid,compid,userid,getDataSource(request));
			request.setAttribute("studentList", studentList);
			
			List<TestReportsForm> courseList = handler.listAllCourse(roleid,compid,userid,getDataSource(request));
			
			request.setAttribute("courseList", courseList);
			String studentId=request.getParameter("userid");
			String courseId=request.getParameter("courseId");
			String fromDate=DateTimeUtil.formatSqlDate(request.getParameter("fromDate"));
			String todate=DateTimeUtil.formatSqlDate(request.getParameter("toDate"));
			System.out.println(studentId);
			List< TestReportsForm> testList = handler.testList(roleid,compid,userid,getDataSource(request), studentId, courseId,fromDate,todate);
			request.setAttribute("testList", testList);
			
			 
		}
		return mapping.findForward("testReports");
	}

}
