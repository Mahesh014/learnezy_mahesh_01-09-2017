package com.itech.elearning.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.itech.elearning.forms.CourseCategoryMasterForm;
import com.itech.elearning.forms.LoginForm;

import com.itech.elearning.handler.CourseCategoryMasterHandler;


public class CourseCategoryMasterAction extends BaseAction
{
	CourseCategoryMasterHandler handler = new CourseCategoryMasterHandler();

	public ActionForward list(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		Long userid = null;
		String compid = null;
		int roleid = 0;
		compid = ((LoginForm) session.getAttribute("userDetail")).getCompanyId();
		roleid = ((LoginForm) session.getAttribute("userDetail")).getRole();
	
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
			roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			
			
			List<CourseCategoryMasterForm> roleList = handler.list(roleid,compid,getDataSource(request));
			request.setAttribute("roleList", roleList);
			return mapping.findForward("coursecat");
		} else
			return mapping.findForward("session");

	}

	public ActionForward add(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		Long userid = null;
		String compid = null;
		
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			CourseCategoryMasterForm roleForm = (CourseCategoryMasterForm) form;
			
			request.setAttribute("status", handler.addRole(roleForm,compid,getDataSource(request)));
			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");

	}

	public ActionForward changestatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			CourseCategoryMasterForm roleForm = (CourseCategoryMasterForm) form;
			roleForm
					.setCoursecategoryId(Integer.parseInt((request.getParameter("coursecategoryId"))));
			roleForm.setActive(Boolean.parseBoolean(request
					.getParameter("active")));
			request.setAttribute("status", handler.changeRole(roleForm,
					getDataSource(request)));
			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");
	}

	public ActionForward update(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			CourseCategoryMasterForm roleForm = (CourseCategoryMasterForm) form;
			request.setAttribute("status", handler.updateRole(roleForm,
					getDataSource(request)));

			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");
	}
	public ActionForward excelReport(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{

		try{
			
			HttpSession session = request.getSession();
			
			Long userid = null;
			String compid = null;
			int roleid=0;
			try {
				userid = ((LoginForm)session.getAttribute("userDetail")).getUserId();
				compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
				roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {

				List<CourseCategoryMasterForm> coursecategory = handler.list(roleid,compid,getDataSource(request));
				request.setAttribute("coursecategory", coursecategory);
				String fileName="Course category_master";
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition","inline;filename="+""+fileName+".xls");
			} else{
				return mapping.findForward("session");
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
	return mapping.findForward("excel");

	}
}
