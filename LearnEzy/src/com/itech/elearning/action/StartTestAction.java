package com.itech.elearning.action;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.StartTestForm;
import com.itech.elearning.handler.StartTestHandler;

public class StartTestAction extends BaseAction {
	StartTestHandler handler = new StartTestHandler();
	public ActionForward list(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		StartTestForm testForm = (StartTestForm) form;
	String testid=request.getParameter("testID");
		HttpSession session = request.getSession();
		Long userid = null;
		String compid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			List<StartTestForm> questionList = handler.listAll(testid,userid,compid,testForm,getDataSource(request));
			session.setAttribute("questionList", questionList);
			return mapping.findForward("startTest1");
		}
		return mapping.findForward("startTest1");
	}
	public ActionForward Results(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws IOException 
	{
		Long userid = null;
		Long CourseID=null;
		String compid = null;
		HttpSession session=request.getSession();
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
			CourseID=((LoginForm)session.getAttribute("userDetail")).getCourseID();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		int countAns=0;
		int countNotAns=0;
		String IDs[]=request.getParameterValues("questionId");
		List<String> lst=new ArrayList<String>();
		for(int i=0;i<IDs.length;i++)
		{
			String gg[]=request.getParameterValues(IDs[i]);
			if(gg!=null){
                  lst.add(gg[0]);
                  countAns=countAns+1;
		    }else{
				  lst.add("NotAnswered");
				  countNotAns=countNotAns+1;
				 }
		}
		String topicID=request.getParameter("topicID");
		request.setAttribute("countAns", countAns);
		request.setAttribute("countNotAns", countNotAns);
		String [] Values=lst.toArray(new String[lst.size()]);
		request.setAttribute("status",handler.checkstatus(topicID,CourseID,userid,request,Values,IDs,compid,getDataSource(request)));
		return mapping.findForward("Results");
	}	
}