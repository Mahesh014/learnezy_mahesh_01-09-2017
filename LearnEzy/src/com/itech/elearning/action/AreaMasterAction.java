package com.itech.elearning.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONArray;

import com.itech.elearning.forms.AreaMasterForm;
import com.itech.elearning.forms.CityForm;
import com.itech.elearning.forms.ContryForm;
import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.StateForm;
import com.itech.elearning.handler.AreaMasterHandler;
import com.itech.elearning.handler.CityHandler;
import com.itech.elearning.handler.CountryHandler;
import com.itech.elearning.handler.StateHandler;

public class AreaMasterAction extends BaseAction
{

	AreaMasterHandler handler = new AreaMasterHandler();
	CountryHandler countryHandler = new CountryHandler();
	StateHandler stateHandler = new StateHandler();
	CityHandler cityHandler = new CityHandler();
	
	public ActionForward list(ActionMapping mapping,ActionForm form,HttpServletRequest request,
			HttpServletResponse response) throws Exception
	{
try{
			
			HttpSession session=request.getSession();
			Long userid = null;
			String compid = null;
			int roleid = 0;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
				
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {
	           compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
	           roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
	           AreaMasterForm areaformForm = (AreaMasterForm) form;
				
				List<ContryForm> countryList = countryHandler.activeList(roleid,compid,getDataSource(request));
				request.setAttribute("countryList", countryList);
				
				List<AreaMasterForm> arealist = handler.list(roleid,compid,areaformForm.getStateid(),areaformForm.getCountryid(),areaformForm.getCityid(),getDataSource(request));
				request.setAttribute("arealist",arealist);
				System.out.println(arealist);
			}else{
				return mapping.findForward("sessionfailure");
			}

		}catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("area");
	}
	
	public ActionForward add(ActionMapping mapping,ActionForm form,HttpServletRequest request,
			HttpServletResponse response) throws Exception
	{
		try{
			HttpSession session=request.getSession();
			Long userid = null;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {
				String compid = null;
				compid = ((LoginForm) session.getAttribute("userDetail")).getCompanyId();
				AreaMasterForm cityForm = (AreaMasterForm) form;
				request.setAttribute("status",handler.add(cityForm,compid,getDataSource(request)));
			}else{
				return mapping.findForward("sessionfailure");
			}

		}catch (Exception e) {
			e.printStackTrace();
		}
		return list(mapping, form, request, response);
	   	
	}
	
	public ActionForward changestatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception
			{
		try{
			HttpSession session=request.getSession();
			Long userid = null;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {
				AreaMasterForm cityForm = (AreaMasterForm) form;
				//cityForm.setAreaid(Long.valueOf(request.getParameter("areaid")));
				//System.out.println(cityForm.getAreaid());
				String areaid = request.getParameter("areaid");
				System.out.println("areaid Testing "+areaid);
				cityForm.setActive(Boolean.parseBoolean(request.getParameter("Active")));
				request.setAttribute("status",handler.changeStatus(areaid,cityForm,getDataSource(request)));
			}else{
				return mapping.findForward("sessionfailure");
			}

		}catch (Exception e) {
			e.printStackTrace();
		}
		return list(mapping, form, request, response);
			}
	
	public ActionForward update(ActionMapping mapping,ActionForm form,HttpServletRequest request,
			HttpServletResponse response) throws Exception
	{
		try{
			HttpSession session=request.getSession();
			Long userid = null;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {
				
				AreaMasterForm cityForm = (AreaMasterForm) form;
				request.setAttribute("status", handler.update(cityForm, getDataSource(request),userid));
			}else{
				return mapping.findForward("sessionfailure");
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return list(mapping, form, request, response);
	}
	
	public ActionForward ajaxActiveList(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception {
		String cityid=request.getParameter("cityId");
		JSONArray jArray =handler.ajaxActiveList(getDataSource(request),cityid);
		response.getOutputStream().write(jArray.toJSONString().getBytes());
		return null;
}
	
	
	public ActionForward exportexcel(ActionMapping mapping,ActionForm form,HttpServletRequest request,
			HttpServletResponse response) throws Exception
	{
try{
			
			HttpSession session=request.getSession();
			Long userid = null;
			String compid = null;
			int roleid = 0;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
				
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {
	           compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
	           roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
	           AreaMasterForm areaformForm = (AreaMasterForm) form;
				
				List<ContryForm> countryList = countryHandler.activeList(roleid,compid,getDataSource(request));
				request.setAttribute("countryList", countryList);
				
				List<AreaMasterForm> arealist = handler.list(roleid,compid,areaformForm.getStateid(),areaformForm.getCountryid(),areaformForm.getCityid(),getDataSource(request));
				request.setAttribute("arealist",arealist);
				System.out.println(arealist);
			}else{
				return mapping.findForward("excel");
			}

		}catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("excel");
	}
	
	
	
	
	
	
	
}
