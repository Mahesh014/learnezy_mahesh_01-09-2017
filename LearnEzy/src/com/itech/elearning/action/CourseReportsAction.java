package com.itech.elearning.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;



import com.itech.elearning.forms.CourseReportsForm;
import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.handler.CourseReportsHandler;
import com.itech.elearning.utils.DateTimeUtil;


public class CourseReportsAction extends BaseAction{
	CourseReportsHandler handler = new CourseReportsHandler();
	public ActionForward list(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();

		Long userid = null;
		String compid = null;
		int roleid = 0;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
		if (userid != null) {
			List<CourseReportsForm> courseList = handler.listAllCourse(roleid,compid,userid,getDataSource(request));
			request.setAttribute("courseList", courseList);
			
			String courseId="";
			String fromDate="";
			String todate="";
			
			List< CourseReportsForm> courseDetialsList = handler.courseDetialsList(roleid,compid,userid,getDataSource(request),courseId,fromDate,todate);
			request.setAttribute("courseDetialsList", courseDetialsList);
		}
		return mapping.findForward("courseReports");
	}
	
	public ActionForward getList(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();

		Long userid = null;
		String compid = null;
		int roleid = 0;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
		if (userid != null) {
			List<CourseReportsForm> courseList = handler.listAllCourse(roleid,compid,userid,getDataSource(request));
			request.setAttribute("courseList", courseList);
			 
			String courseId=request.getParameter("courseId");
			String fromDate=DateTimeUtil.formatSqlDate(request.getParameter("fromDate"));
			String todate=DateTimeUtil.formatSqlDate(request.getParameter("toDate"));
			
			List< CourseReportsForm> courseDetialsList = handler.courseDetialsList(roleid,compid,userid,getDataSource(request),courseId,fromDate,todate);
			request.setAttribute("courseDetialsList", courseDetialsList);
			
			 
		}
		return mapping.findForward("courseReports");
	}

}
