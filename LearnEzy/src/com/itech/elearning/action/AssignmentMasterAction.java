package com.itech.elearning.action;

import java.util.List;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import org.json.simple.JSONArray;

import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.AssignmentMasterForm;
import com.itech.elearning.forms.SubTopicMasterForm;
import com.itech.elearning.handler.AssignmentMasterHandler;
import com.itech.elearning.utils.FileUploadDownloadUtil;

public class AssignmentMasterAction extends BaseAction {
	AssignmentMasterHandler handler = new AssignmentMasterHandler();

	public ActionForward list(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		AssignmentMasterForm asingForm = (AssignmentMasterForm) form;
		HttpSession session = request.getSession();

		Long userid = null;
		String compid = null;
		int roleid = 0;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			compid = ((LoginForm) session.getAttribute("userDetail")).getCompanyId();
			roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		
		if (userid != null) {

			List<AssignmentMasterForm> courseList = handler.listAllCourse(roleid,compid,userid,getDataSource(request));
			request.setAttribute("courseList", courseList);
						
			List<AssignmentMasterForm> topicList = handler.listAllTopic(roleid,compid,userid,asingForm.getCourseId(),getDataSource(request));
			request.setAttribute("topicList", topicList);
			
			List<AssignmentMasterForm> subTopicList = handler.listAllSubTopic(roleid,compid,userid,asingForm.getCourseId(),asingForm.getTopicId(),getDataSource(request));
			
			request.setAttribute("subTopicList", subTopicList);
			
					
			List<AssignmentMasterForm> assignmentList = handler.assignmentList(roleid,compid,userid,asingForm.getCourseId(), asingForm.getTopicId(), getDataSource(request));
			request.setAttribute("assignmentList", assignmentList);
			//System.out.println("AM"+ assignmentList);
		}

		return mapping.findForward("assignmentMaster");

	}
	
	
	public ActionForward excelUpload(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		AssignmentMasterForm asingForm = (AssignmentMasterForm) form;
		
		 if(!asingForm.getExcelFile().equals("")){
			 FormFile myfile = asingForm.getExcelFile();
			 String filePath = getServlet().getServletContext().getRealPath("/") +"upload";
		     String	 fileName =myfile.getFileName();
			 String fileName1 = myfile.getFileName();
		     boolean	 uploadStatus=FileUploadDownloadUtil.fileUploader(myfile, filePath, fileName);
		     filePath = getServlet().getServletContext().getRealPath("/")+ "upload\\";
			 String success = handler.testTypeExcelUpload(fileName, filePath);
   			request.setAttribute("status", success);
		 }
		return list(mapping, form, request, response);
	}
	

	public ActionForward add(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		AssignmentMasterForm asingForm = (AssignmentMasterForm) form;
		HttpSession session = request.getSession();
		String compid = null;
		compid = ((LoginForm) session.getAttribute("userDetail")).getCompanyId();
		request.setAttribute("status", handler.add(asingForm,compid,getDataSource(request)));
		return list(mapping, form, request, response);
	}

	public ActionForward changestatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			AssignmentMasterForm asingForm = (AssignmentMasterForm) form;
			Long assignmentId=Long.parseLong((request.getParameter("assignmentId")));
			boolean active=Boolean.parseBoolean(request.getParameter("active"));
			request.setAttribute("status", handler.changeStatus(asingForm,getDataSource(request),assignmentId,active));
			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");
	}

	public ActionForward update(ActionMapping mapping, ActionForm form,	HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			AssignmentMasterForm asingForm = (AssignmentMasterForm) form;

			request.setAttribute("status", handler.update(asingForm,getDataSource(request)));
			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");
	}
	public ActionForward listAssignmentById(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		AssignmentMasterForm asingForm = (AssignmentMasterForm) form;
		HttpSession session = request.getSession();

		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {

			List<AssignmentMasterForm> assignmentList = handler.listAssignmentById(userid,asingForm.getCourseId(),getDataSource(request));
			request.setAttribute("assignmentList", assignmentList);
			System.out.println("AM"+ assignmentList);
		}

		return mapping.findForward("listAssignmentById");

	}
	
	public ActionForward excelReport(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{

		try{
			AssignmentMasterForm asingForm = (AssignmentMasterForm) form;
			HttpSession session = request.getSession();

			Long userid = null;
			String compid = null;
			int roleid = 0;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
				compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
				roleid = ((LoginForm) session.getAttribute("userDetail")).getRole();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			
			if (userid != null) {
				List<AssignmentMasterForm> assignmentList = handler.assignmentList(roleid,compid,userid,asingForm.getCourseId(), asingForm.getTopicId(), getDataSource(request));
				request.setAttribute("assignmentList", assignmentList);
				String fileName="Assignment_master";
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition","inline;filename="+""+fileName+".xls");
			} else{
				return mapping.findForward("session");
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
	return mapping.findForward("excel");

	}
	public ActionForward reset(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			AssignmentMasterForm asingForm = (AssignmentMasterForm) form;
			asingForm.setCourseId(Long.parseLong("-1"));
			asingForm.setTopicId(Long.parseLong("-1"));
			asingForm.setSubTopicId(Long.parseLong("-1"));
			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");
	 
	}
	
	public ActionForward getSubTpics(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			
			HttpSession session = request.getSession();

			Long userid = null;
			String compid = null;
			int roleid = 0;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
				compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
				roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid == null) {
				return mapping.findForward("session");
			}
			else{
				
				//Long userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
				Long courseId=Long.valueOf(request.getParameter("courseId"));
				Long topicId=Long.valueOf(request.getParameter("topicId"));
				List<AssignmentMasterForm> subTopicList = handler.listAllSubTopic(roleid,compid,userid,courseId,topicId,getDataSource(request));
				/*Gson gson = new GsonBuilder().create();
				
				JSONArray arry = new JSONArray();
				
				JsonArray obj = gson.toJsonTree(subTopicList).getAsJsonArray();
				System.out.println("jsonArray"+obj.toString());*/
				JSONArray arry = new JSONArray();
				JSONArray obj = (JSONArray) handler.listAllSubTopic1(compid,userid,courseId,topicId,getDataSource(request));
				
				response.getOutputStream().write(obj.toString().getBytes());
					
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public ActionForward getTopic(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		try {
			long courseId=Long.parseLong(request.getParameter("courseId"));
			System.out.println("CONTENT ID: "+courseId);
			JSONArray obj=handler.getTopic(getDataSource(request),courseId);
			response.getOutputStream().write(obj.toString().getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	
	
	
	
}
