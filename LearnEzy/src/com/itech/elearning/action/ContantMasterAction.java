package com.itech.elearning.action;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.poi.hssf.usermodel.HSSFPictureData;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.PictureData;
import org.apache.poi.ss.usermodel.Row;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.ContantMasterForm;
import com.itech.elearning.handler.ContantMasterHandler;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.FileUploadDownloadUtil;


public class ContantMasterAction extends BaseAction {


	ContantMasterHandler handler = new ContantMasterHandler();

	public ActionForward list(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {

		ContantMasterForm contentForm = (ContantMasterForm) form;
		HttpSession session = request.getSession();
		Long userid = null;
		String compid = null;
		int roleid = 0;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
			
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
            compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			List<ContantMasterForm> courseList = handler.listAllCourse(roleid,compid,userid,getDataSource(request));
			request.setAttribute("courseList", courseList);

			List<ContantMasterForm> topicList = handler.listAllTopic(roleid,compid,userid,contentForm.getCourseId(),getDataSource(request));
			request.setAttribute("topicList", topicList);
			
			List<ContantMasterForm> subTopicList = handler.listSubTopic(roleid,compid,userid,contentForm.getTopicId(),contentForm.getCourseId(),getDataSource(request));
			request.setAttribute("subTopicList", subTopicList);
						
			List<ContantMasterForm> contentList = handler.listContent(roleid,compid,userid,contentForm.getSubTopicId(),contentForm.getTopicId(),contentForm.getCourseId(),getDataSource(request));
			request.setAttribute("contentList", contentList);
			
			contentForm.setContentId(-1);
		}
		return mapping.findForward("ContantMaster");
	}
	public ActionForward addCourse(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception {
		ContantMasterForm contentForm = (ContantMasterForm) form;
		String status = Common.FAILURE_MESSAGE;
		String videofileName="";
		String imagefileName="";
		String imageUploadPath= getServlet().getServletContext().getRealPath("/")+ "imageUpload";
		String videoUploadPath= getServlet().getServletContext().getRealPath("/")+ "videoUpload";
		boolean uploadStatus=true;
		try {
			
		FormFile imageFile= contentForm.getImageFile();
		if(imageFile.getFileName()!="" && imageFile.getFileSize()>2){
				imagefileName=imageFile.getFileName();
				System.out.println(imagefileName);
				uploadStatus = FileUploadDownloadUtil.fileUploader(imageFile, imageUploadPath, imagefileName);
		}		
		if (uploadStatus==true) {
			FormFile videoFile= contentForm.getVideoFile();
			if(videoFile.getFileName()!="" && videoFile.getFileSize()>2){
				videofileName=videoFile.getFileName();
				System.out.println(videofileName);
				uploadStatus = FileUploadDownloadUtil.fileUploader(videoFile, videoUploadPath, videofileName);
			}
		}	
		if (uploadStatus==true) {
			HttpSession session = request.getSession();
			String compid = null;
				compid = ((LoginForm) session.getAttribute("userDetail")).getCompanyId();
				status = handler.addcontant(contentForm, getDataSource(request),imagefileName,videofileName,compid);
				request.setAttribute("status", status);
				
		}

		} catch (Exception e) {
		e.printStackTrace();
		}
		return list(mapping, form, request, response);
	}
	
	
	@SuppressWarnings("unchecked")
	public ActionForward excelUpload(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		try
		{
		ContantMasterForm contentForm = (ContantMasterForm) form;
		HttpSession session = request.getSession();
		Long userid = null;
		String compid = null;
		String courseid="";
		String topicid="";
		String subtopicid="";
		String videofilename="";
		String contenttext="";
		String youtubelink="";
		int n=0;
		int countofvalues=0;
		int totalcount=1;
		 int as=0;
		int previousId=0; 
		 int userID=0;
		 
		 int startingNo=1;
		 int endingNo=5;
	
		int rowNo=0;	

		String newPhoto="";
		String 	Filename="";
			
		
		
		  List<ContantMasterForm> ld = null;


		
		boolean uploadstatus = false;
		 if(!contentForm.getExcelFile().equals("")){
			 compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			 FormFile myfile = contentForm.getExcelFile();
			 String filePath = getServlet().getServletContext().getRealPath("/") +"upload";
		     String	 fileName =myfile.getFileName();
			 String fileName1 = myfile.getFileName();
			 uploadstatus=FileUploadDownloadUtil.fileUploader(myfile, filePath, fileName);
		     if (uploadstatus == true) {
				filePath = getServlet().getServletContext().getRealPath("/")+"upload\\";
				InputStream input = new BufferedInputStream(new FileInputStream(filePath+"\\"+fileName));
				POIFSFileSystem fs = new POIFSFileSystem(input);
				HSSFWorkbook wb = new HSSFWorkbook(fs);
				HSSFSheet sheet = wb.getSheetAt(0);
				Iterator<Row> rows = sheet.rowIterator();
				DataFormatter data=null;
				HSSFRow row=null;
				List<HSSFPictureData> lst = null;//wb.getAllPictures();
				ListIterator<HSSFPictureData> it = null;//lst.listIterator();
				int i=0;

				
				while (rows.hasNext()) {
					 row =  (HSSFRow) rows.next();

					 if(row.getRowNum()==0){
						   continue; //just skip the rows if row number is 0 or 1
						  }
					data = new DataFormatter();
					courseid = data.formatCellValue(row.getCell(0));
					System.out.println("courseidddddddddddddd"+courseid);
					
					topicid = data.formatCellValue(row.getCell(1));
					subtopicid = data.formatCellValue(row.getCell(2));
					videofilename= data.formatCellValue(row.getCell(3));
					contenttext= data.formatCellValue(row.getCell(4));
					youtubelink= data.formatCellValue(row.getCell(5));


					//emailId = data.formatCellValue(row.getCell(3));
					
					i=row.getRowNum();
					rowNo=row.getRowNum();
					@SuppressWarnings("unused")
			List lst1 = (List) wb.getAllPictures();
					
					boolean tot=false;
						
								newPhoto="";
								
							    PictureData pict = (PictureData)lst1.get(i-1);
							    String ext = pict.suggestFileExtension();
							    byte[] image = pict.getData();
							 
							      	newPhoto = image+".jpg";
							      	System.out.println("///"+newPhoto);
									String filePathphotouplod = getServlet().getServletContext().getRealPath("/")+"imageUpload\\"
											+ newPhoto;
									if(Filename==""){
										Filename=""+newPhoto;
									}else{
									Filename=Filename+","+newPhoto;
									}
									
									
									FileOutputStream out = new FileOutputStream(filePathphotouplod);
							      out.write(image);
							      out.close();
							      	
						      
							//}
					//}
					   ld = new ArrayList<ContantMasterForm>();
					   contentForm.setCourseId(Integer.parseInt(courseid));
					   contentForm.setTopicId(Integer.parseInt(topicid));
					   contentForm.setSubTopicId(Integer.parseInt(subtopicid));
					   contentForm.setVideofilename(videofilename);
					   contentForm.setContentText(contenttext);
					   contentForm.setYouTubeLink(youtubelink);


					      //categoryForm.setEmailId(emailId);
					    
					      ld.add(contentForm);
					      String success = handler.excelUpload(getDataSource(request), ld,Filename,n,rowNo,countofvalues,totalcount,compid);
			      request.setAttribute("status",success);
			      n++;
					
					//}
							startingNo+=5;
							endingNo+=5;	
					}
					
					//i++;
				}}
		}catch (Exception e) {
			e.printStackTrace();
		}

			
		
		return list(mapping, form, request, response);
	}
	
	public ActionForward changestatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		ContantMasterForm contentForm = (ContantMasterForm) form;
		long contentId=Long.parseLong(request.getParameter("contentId"));
		boolean active=Boolean.parseBoolean(request.getParameter("active"));
		String check = handler.changeStatus(contentForm, getDataSource(request),contentId,active);
		request.setAttribute("status", check);
		return list(mapping, form, request, response);
	}
	
	
	public ActionForward edit(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		try {
			long contentID=Long.parseLong(request.getParameter("contentID"));
			System.out.println("CONTENT ID: "+contentID);
			JSONObject obj=handler.getEdit(getDataSource(request),contentID);
			response.getOutputStream().write(obj.toString().getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public ActionForward getTopic(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		try {
			long courseId=Long.parseLong(request.getParameter("courseId"));
			System.out.println("CONTENT ID: "+courseId);
			JSONArray obj=handler.getTopic(getDataSource(request),courseId);
			response.getOutputStream().write(obj.toString().getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public ActionForward getSubTopic(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		try {
			long topicId=Long.parseLong(request.getParameter("topicId"));
			System.out.println("CONTENT ID: "+topicId);
			JSONArray obj=handler.getSubTopic(getDataSource(request),topicId);
			response.getOutputStream().write(obj.toString().getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public ActionForward updateContent(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ContantMasterForm contentForm = (ContantMasterForm) form;
		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			String videofileName="";
			String imagefileName="";
			String imageUploadPath= getServlet().getServletContext().getRealPath("/")+ "imageUpload";
			String videoUploadPath= getServlet().getServletContext().getRealPath("/")+ "videoUpload";
			boolean uploadStatus=true;
			
			
			imagefileName=request.getParameter("hiddenimageFile");
			System.out.println("imagefilename"+imagefileName);
			
				
			FormFile imageFile= contentForm.getImageFile();
			if(imageFile.getFileName()!="" && imageFile.getFileSize()>2){
					imagefileName=imageFile.getFileName();
					System.out.println(imagefileName);
					uploadStatus = FileUploadDownloadUtil.fileUploader(imageFile, imageUploadPath, imagefileName);
			}	
			
			
			if(request.getParameter("hiddenimageFile")!=null||!request.getParameter("hiddenimageFile").equals(""))
			{
				
				imagefileName=request.getParameter("hiddenimageFile");
				System.out.println("imagefilename"+imagefileName);
				
				
				
				
			}
			
			if (uploadStatus==true) {
				FormFile videoFile= contentForm.getVideoFile();
				if(videoFile.getFileName()!="" && videoFile.getFileSize()>2){
					videofileName=videoFile.getFileName();
					System.out.println(videofileName);
					uploadStatus = FileUploadDownloadUtil.fileUploader(videoFile, videoUploadPath, videofileName);
				}
				
				if(request.getParameter("hiddenvideoFile")!=null||!request.getParameter("hiddenvideoFile").equals(""))
				{
					
					videofileName=request.getParameter("hiddenvideoFile");
				}
				
				
			}	
			if (uploadStatus==true) {
				request.setAttribute("status", handler.updateContent(contentForm,getDataSource(request),imagefileName,videofileName));
			}
				return list(mapping, contentForm, request, response);
		} else
			return mapping.findForward("session");
	}
	
	public ActionForward excelReport(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{
		try{
			HttpSession session = request.getSession();
			Long userid = null;
			String compid = null;
			int roleid = 0;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
				compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {
				List<ContantMasterForm> contentList = handler.listContent(roleid,compid,userid,Long.parseLong("0"),Long.parseLong("0"),Long.parseLong("0"),getDataSource(request));
				request.setAttribute("contentList", contentList);
				String fileName="content_master";
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition","inline;filename="+""+fileName+".xls");
			} else{
				return mapping.findForward("session");
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
	return mapping.findForward("excel");

	}
}
