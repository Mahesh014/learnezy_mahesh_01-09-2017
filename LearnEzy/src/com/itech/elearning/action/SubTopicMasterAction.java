package com.itech.elearning.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import com.itech.elearning.forms.CourseMasterForm;
import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.SubTopicMasterForm;
import com.itech.elearning.handler.SubTopicMasterHandler;
import com.itech.elearning.utils.FileUploadDownloadUtil;


public class SubTopicMasterAction extends BaseAction {


	SubTopicMasterHandler handler = new SubTopicMasterHandler();



	public ActionForward list(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		SubTopicMasterForm subtopicForm = (SubTopicMasterForm) form;
		HttpSession session = request.getSession();
		long userid = 0;
		String compid = null;
		int roleid = 0;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			compid = ((LoginForm) session.getAttribute("userDetail")).getCompanyId();
			roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != 0) {
			List<SubTopicMasterForm> courseList = handler.listAllCourse(roleid,compid,userid,getDataSource(request));
			request.setAttribute("courseList",courseList);
			
			 
			List<SubTopicMasterForm> topicList = handler.listAllTopic(roleid,compid,userid,subtopicForm.getCourseId(),subtopicForm.getTopicId(),getDataSource(request));
			request.setAttribute("topicList", topicList);
			 
						
			List<SubTopicMasterForm> subTopicList = handler.listSubTopic(roleid,compid,userid,subtopicForm.getTopicId(),subtopicForm.getCourseId(),getDataSource(request));
			request.setAttribute("subTopicList", subTopicList);
			
			return mapping.findForward("SubTopicMaster");
		}

		return list(mapping, subtopicForm, request, response);

	}
	
	
	public ActionForward excelUpload(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		SubTopicMasterForm subtopicForm = (SubTopicMasterForm) form;
		HttpSession session = request.getSession();
		Long userid = null;
		String compid = null;
		
		 if(!subtopicForm.getExcelFile().equals("")){
			 compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			 FormFile myfile = subtopicForm.getExcelFile();
			 String filePath = getServlet().getServletContext().getRealPath("/") +"upload";
		     String	 fileName =myfile.getFileName();
			 String fileName1 = myfile.getFileName();
		     boolean	 uploadStatus=FileUploadDownloadUtil.fileUploader(myfile, filePath, fileName);
		     filePath = getServlet().getServletContext().getRealPath("/")+ "upload\\";
			 String success = handler.testTypeExcelUpload(fileName, filePath,compid);
   			request.setAttribute("status", success);
		 }
		return list(mapping, form, request, response);
	}
	

	public ActionForward addCourse(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		SubTopicMasterForm subtopicForm = (SubTopicMasterForm) form;
		HttpSession session = request.getSession();
		String compid = null;
		compid = ((LoginForm) session.getAttribute("userDetail")).getCompanyId();
		subtopicForm.setCompid(compid);
		String check = handler.addsubTopic(subtopicForm, getDataSource(request));
		request.setAttribute("status", check);
		return list(mapping, form, request, response);
	}

	public ActionForward changestatus(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {

		SubTopicMasterForm subtopicForm = (SubTopicMasterForm) form;
		String check = handler.changeStatus(subtopicForm, getDataSource(request),Long.parseLong(request.getParameter("subtopicid")));
		request.setAttribute("status", check);
		return list(mapping, form, request, response);
	}
	
	
	public ActionForward updateCourse(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		SubTopicMasterForm subtopicForm = (SubTopicMasterForm) form;
		HttpSession session = request.getSession();
		long userid = 0;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != 0) {
			System.out.println("Update");
			
			request.setAttribute("status", handler.updateTopic(subtopicForm,getDataSource(request)));
			return list(mapping, subtopicForm, request, response);
		} else
			return mapping.findForward("session");
	}
	public ActionForward excelReport(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{
		SubTopicMasterForm subtopicForm = (SubTopicMasterForm) form;
		try{
			HttpSession session=request.getSession();
			Long userid = null;
			String compid = null;
			int roleid = 0;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
				compid = ((LoginForm) session.getAttribute("userDetail")).getCompanyId();
				roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
				
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {	 
							
				List<SubTopicMasterForm> subTopicList = handler.listSubTopic(roleid,compid,userid,subtopicForm.getTopicId(),subtopicForm.getCourseId(),getDataSource(request));
				request.setAttribute("subTopicList", subTopicList);
				
				String fileName="subTopic_master";
			    response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition","inline;filename="+""+fileName+".xls");
			}else{
				return mapping.findForward("sessionfailure");
			}

		}catch (Exception e) {
			e.printStackTrace();
		}

		return mapping.findForward("excel");
	}

}
