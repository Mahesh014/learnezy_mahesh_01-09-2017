package com.itech.elearning.action;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.itech.elearning.forms.ForgotPasswordForm;
import com.itech.elearning.handler.ForgotPasswordHandler;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.FuctionMailClass;

public class ForgotPasswordAction extends BaseAction {
	ForgotPasswordHandler handler = new ForgotPasswordHandler();

	public ActionForward sendmail(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
        request.getSession();
		String status=Common.FAILURE_MESSAGE;
		ForgotPasswordForm forgotPasswordForm = (ForgotPasswordForm) form;
		String email = forgotPasswordForm.getEmail();
		String toAddress = "yaseen@itechsolutions.in";
		int emailCount =1;
		try {
			String result = handler.forgotPassword(forgotPasswordForm,	getDataSource(request));
				Cookie[] cookies = request.getCookies();
			   	boolean addCookie=true;
			   	if (cookies != null) {
			   		for (int i = 0; i < cookies.length; i++) {
			   		  Cookie cookie=cookies[i];
			   	      String cookieName = cookie.getName();
			   	      if (cookieName.equals("emailCount")) {
			   	    	String cookieValue = cookie.getValue();
			   	    	emailCount=Integer.parseInt(cookieValue);
				   	    System.out.println(cookieName+" :::::::::: "+emailCount);
				   	      if(emailCount>=2){
				   	    	status="Please write an Email to 'support@learnezy.com' and we will get back to you";
				   	    	addCookie=false;
				   	      }else{
				   	    	emailCount=emailCount+1;
				   	    	if (result != null) {
				   	    			String subject = "Itech Training";
				   					String message = "Hi, This mail ur getting from Itech Training Your password is: " + result;
				   					try {
				   						FuctionMailClass mailClient = new FuctionMailClass();
				   						mailClient.postMail(new String[] { email }, subject, message, toAddress);
				   						System.out.println("Mail sent succesfully");
				   						status="Please check your registered Email ID";
				   					} catch (Exception e) {
				   						e.printStackTrace();
				   					}
				   	    		request.setAttribute("status",  status);
								return mapping.findForward("forgotpasswordPass");
							}
				   	      }
				   	      
			   	      }
			   	    	
			   	   }
			   	     
			   	}
			   		if (addCookie) {
			   			Cookie cookie = new Cookie("emailCount",String.valueOf(emailCount));
				   		cookie.setMaxAge(60*60); //1 hour
				   		response.addCookie(cookie);
				   		status="Sorry this is not the registered Email ID, please try again!";
					}
		} catch (Exception e) {
			e.printStackTrace();
		}
		request.setAttribute("status",  status);
		return mapping.findForward("forgotpassword");
	}
}