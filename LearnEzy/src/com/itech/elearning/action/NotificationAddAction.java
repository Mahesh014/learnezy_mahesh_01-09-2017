package com.itech.elearning.action;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONArray;

import com.itech.elearning.forms.ContantMasterForm;
import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.NotificationMasterForm;
import com.itech.elearning.handler.ContantMasterHandler;
import com.itech.elearning.handler.NotificationAddHandler;

public class NotificationAddAction extends BaseAction{
	NotificationAddHandler handler = new NotificationAddHandler();
	ContantMasterHandler contantHandler = new ContantMasterHandler();
	public ActionForward list(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		 
		HttpSession session = request.getSession();

		Long userid = null;
		String compid = null;
		int roleid = 0;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			List<NotificationMasterForm> studentList = handler.studentList(roleid,compid,userid,getDataSource(request));
			request.setAttribute("studentList", studentList);
					
			List<NotificationMasterForm> notificationList = handler.list(roleid,compid,userid,getDataSource(request));
			request.setAttribute("notificationList", notificationList);
			System.out.println(notificationList);
			
			List<ContantMasterForm> courseList = contantHandler.listAllCourse(roleid,compid,userid,getDataSource(request));
			request.setAttribute("courseList", courseList);
		}

		return mapping.findForward("notificationStudent");

	}

	public ActionForward add(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession session = request.getSession();
		String compid = null;
	
		compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();

		String[] studentId=request.getParameterValues("studentId");
		
		NotificationMasterForm notiForm = (NotificationMasterForm) form;
		for (int i = 0; i < studentId.length; i++) {
			request.setAttribute("status", handler.add(compid,notiForm,getDataSource(request),studentId[i]));
		}
		
		return list(mapping, form, request, response);
	}

	public ActionForward changestatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			NotificationMasterForm notiForm = (NotificationMasterForm) form;
			Long notificationID=Long.parseLong((request.getParameter("notificationID")));
			boolean active= true; /*Boolean.parseBoolean(request.getParameter("active"));*/
			request.setAttribute("status", handler.changeStatus(notiForm,getDataSource(request),notificationID,active));
			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");
	}

	public ActionForward update(ActionMapping mapping, ActionForm form,	HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			NotificationMasterForm notiForm = (NotificationMasterForm) form;

			request.setAttribute("status", handler.update(notiForm,getDataSource(request)));
			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");
	}
	 
	public ActionForward studentListByCourse(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		 HttpSession session = request.getSession();
		 String compid = null;
		 compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		try {
			String courseId=request.getParameter("courseId1");
			//System.out.println("CCCCCCCCCCCC: "+courseId);
				JSONArray jArray = new JSONArray();
				jArray = handler.studentListByCourse(compid,getDataSource(request),courseId);
				response.getOutputStream().write(jArray.toJSONString().getBytes());
			 
		} catch (NullPointerException e) {
			e.printStackTrace();
			return mapping.findForward("session");
		}
		return null;
	}
	public ActionForward excelReport(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{

		try{
			 
			HttpSession session = request.getSession();

			Long userid = null;
			String compid = null;
			int roleid=0;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
				compid = ((LoginForm) session.getAttribute("userDetail")).getCompanyId();
				roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {
				List<NotificationMasterForm> notificationList = handler.list(roleid,compid,userid,getDataSource(request));
				request.setAttribute("notificationList", notificationList);
				System.out.println(notificationList);
				String fileName="student_notification";
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition","inline;filename="+""+fileName+".xls");
			} else{
				return mapping.findForward("session");
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
	return mapping.findForward("excel");

	}
	
}
