package com.itech.elearning.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.itech.elearning.forms.ContactForm;
import com.itech.elearning.handler.ContactHandler;
public class ContactAction extends BaseAction {
	ContactHandler handler= new ContactHandler();
	public ActionForward add(ActionMapping mapping,ActionForm form,HttpServletRequest request, HttpServletResponse response ) throws Exception  {
		   try{				
			   ContactForm contactForm = (ContactForm)form;
			   request.setAttribute("status",handler.add(contactForm,getDataSource(request)));
				
			}catch (Exception e) {
				e.printStackTrace();
			}
		   return mapping.findForward("contact");
	   }
}
