package com.itech.elearning.action;

import java.util.List;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.NotificationMasterForm;
import com.itech.elearning.handler.NotificationMasterHandler;


public class NotificationMasterAction extends BaseAction {
	NotificationMasterHandler handler = new NotificationMasterHandler();

	public ActionForward list(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		NotificationMasterForm notiForm = (NotificationMasterForm) form;
		HttpSession session = request.getSession();
        String compid = null;
        
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
            compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			List<NotificationMasterForm> courseList = handler.listAllCourse(compid,userid,getDataSource(request));
			request.setAttribute("courseList", courseList);
						
			List<NotificationMasterForm> notificationList = handler.list(compid,userid,notiForm.getCourseId(),getDataSource(request));
			request.setAttribute("notificationList", notificationList);
			System.out.println(notificationList);

		}

		return mapping.findForward("notificationMaster");

	}

	public ActionForward add(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		String compid = null;
		compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		NotificationMasterForm notiForm = (NotificationMasterForm) form;
		request.setAttribute("status", handler.add(compid,notiForm,getDataSource(request)));
		return list(mapping, form, request, response);
	}

	public ActionForward changestatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			NotificationMasterForm notiForm = (NotificationMasterForm) form;
			Long notificationID=Long.parseLong((request.getParameter("notificationID")));
			boolean active=Boolean.parseBoolean(request.getParameter("active"));
			request.setAttribute("status", handler.changeStatus(notiForm,getDataSource(request),notificationID,active));
			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");
	}

	public ActionForward update(ActionMapping mapping, ActionForm form,	HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			NotificationMasterForm notiForm = (NotificationMasterForm) form;

			request.setAttribute("status", handler.update(notiForm,getDataSource(request)));
			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");
	}
	 
	
	
}
