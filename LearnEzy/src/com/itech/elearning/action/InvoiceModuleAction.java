package com.itech.elearning.action;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.ListIterator;

import javax.sql.DataSource;

import com.itech.elearning.forms.CourseMasterForm;
import com.itech.elearning.forms.StudentRegistrationForm;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class InvoiceModuleAction extends BaseAction {
	
	/*public static final Font font1 = new Font(Font.FontFamily.COURIER ,12, Font.BOLD);
	public static final Font font2 = new Font(Font.FontFamily.COURIER ,8);
	public static final Font font4 = new Font(Font.FontFamily.COURIER ,10);
	public static final Font font3 = new Font(Font.FontFamily.HELVETICA ,8, Font.BOLD);*/

	
	public static int getPdfInoive(DataSource dataSource, StudentRegistrationForm studentData, String filePath, List<CourseMasterForm> selectedCourseList, String userId, String totalAmount, String tax, String fontPath, String itechLogofilePath, String inWord, String subAmount, String transactionID, String discountAmount, String serviceTax) {
		int flag=0;
		int pageCounter=28;
		try{
			
			 BaseColor color1 = WebColors.getRGBColor("#E58B30");
//			 BaseColor color2 = WebColors.getRGBColor("#069");
//			 BaseColor color3 = WebColors.getRGBColor("#F28858");	
//			 BaseColor color4 = WebColors.getRGBColor("#787878");
//			
			 BaseFont base = BaseFont.createFont(fontPath, BaseFont.WINANSI, true);
			 
			 Font font1 = new Font(base,14, Font.BOLD,color1);
			 Font font2 = new Font(base,10,Font.ITALIC);
			 Font font3 = new Font(base,10);
			 Font font4 = new Font(base,10,Font.BOLD);
			 Font font5 = new Font(base,8,Font.ITALIC);
			 Font font6 = new Font(base,8);
			
			/*----------------------------------------------------------------PAGE SETUP AND CREATING A PDF -----------------------------------------------*/    
		     Document document = new Document();
		     OutputStream file = new FileOutputStream(new File(filePath));
		     document.setPageSize(PageSize.A4);
		     //document.setMargins(108,25,15,15);
		     document.setMarginMirroring(false);
		     PdfWriter.getInstance(document, file);
		     document.open();
		     
		     PdfPTable table1 = new PdfPTable(1); 
		     table1.setWidthPercentage(100);
		     table1.setSpacingAfter(4f);
		     				Image image = Image.getInstance(itechLogofilePath);
		     				image.scaleAbsolute(125f,40f);
		     
	 		 PdfPCell       cell1 = new PdfPCell(image, false);
	 		 				cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
	 		 				cell1.setBorder(Rectangle.NO_BORDER);
	 		 				cell1.setFixedHeight(40f);
	 		 				table1.addCell(cell1);;
		             		 
		             		cell1 = new PdfPCell(new Phrase("#216,'Jaya', Opp to Muneshwara Temple, No 16/1, 2nd Floor",font2));
		             		cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		             		cell1.setBorder(Rectangle.NO_BORDER);
		             		table1.addCell(cell1);
		             		
		             		cell1 = new PdfPCell(new Phrase("Subramanya Nagar,",font2));
		             		cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		             		cell1.setBorder(Rectangle.NO_BORDER);
		             		table1.addCell(cell1);

		             		cell1 = new PdfPCell(new Phrase("Bangalore-560021.",font2));
		             		cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		             		cell1.setBorder(Rectangle.NO_BORDER);
		             		table1.addCell(cell1);
		             		
		             		cell1 = new PdfPCell(new Phrase("Phone: +91-9611400344, +91-9611421111",font2));
		             		cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		             		cell1.setBorder(Rectangle.NO_BORDER);
		             		table1.addCell(cell1);
		             		
		             		cell1 = new PdfPCell(new Phrase("Email: info@learnezy.com , sales@learnezy.com",font2));
		             		cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		             		cell1.setBorder(Rectangle.NO_BORDER);
		             		table1.addCell(cell1);
		              		
		     PdfPTable tableCust = new PdfPTable(3); 
		     tableCust.setWidthPercentage(100);
		     tableCust.setSpacingBefore(4f);
		     tableCust.setSpacingAfter(4f);
		     PdfPCell       Custcell = new PdfPCell(new Phrase("Student Name  :  ",font4));
		             		Custcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		             		Custcell.setBorder(Rectangle.NO_BORDER);
		             		tableCust.addCell(Custcell);
		             		
		             		Custcell = new PdfPCell(new Phrase(studentData.getFirstName()+" "+studentData.getLastName(),font3));
			             	Custcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			             	Custcell.setBorder(Rectangle.NO_BORDER);
			             	tableCust.addCell(Custcell);
		             		
			             	Custcell = new PdfPCell(new Phrase("Invoice No  :  ITA"+transactionID,font5));
							Custcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							Custcell.setBorder(Rectangle.NO_BORDER);
							tableCust.addCell(Custcell);
				
							 
		             		Custcell = new PdfPCell(new Phrase("Phone No  :  ",font4));
		             		Custcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		             		Custcell.setBorder(Rectangle.NO_BORDER);
		             		tableCust.addCell(Custcell);
		             		
		             		Custcell = new PdfPCell(new Phrase(studentData.getContactno(),font3));
		             		Custcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		             		Custcell.setBorder(Rectangle.NO_BORDER);
		             		tableCust.addCell(Custcell);
		             		
		             		Custcell = new PdfPCell(new Phrase("Invoice Date  :  12-12-2015",font5));
							Custcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							Custcell.setBorder(Rectangle.NO_BORDER);
							tableCust.addCell(Custcell);
         		
							Custcell = new PdfPCell(new Phrase("Email ID :  ",font4));
		             		Custcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		             		Custcell.setBorder(Rectangle.NO_BORDER);
		             		tableCust.addCell(Custcell);
		             		
		             		Custcell = new PdfPCell(new Phrase(studentData.getEmailId(),font3));
		             		Custcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		             		Custcell.setBorder(Rectangle.NO_BORDER);
		             		tableCust.addCell(Custcell);
		             		
		             		Custcell = new PdfPCell();
		             		Custcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		             		Custcell.setBorder(Rectangle.NO_BORDER);
		             		tableCust.addCell(Custcell);
		             		
		             		Custcell = new PdfPCell();
		             		Custcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		             		Custcell.setBorder(Rectangle.NO_BORDER);
		             		tableCust.addCell(Custcell);
		             		
		             		float[] columnWidths = {20f, 50f, 50f};
		             		tableCust.setWidths(columnWidths);
		             		

		     PdfPTable table2 = new PdfPTable(1); 
		     PdfPCell   	cell2 = new PdfPCell(new Phrase("VAT/TIN No: BPG6661234",font6));
		             		cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
		             		cell2.setBorder(Rectangle.NO_BORDER);
		             		table2.addCell(cell2);
		             		
		      	
		     PdfPTable table3 = new PdfPTable(3); 
		     table3.setSpacingAfter(10f);
		     table3.setWidthPercentage(100);
		     PdfPCell       cell3 = new PdfPCell(new Phrase("Sl No",font4));
		             		cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
		             		cell3.setBorder(Rectangle.BOTTOM);
		             		table3.addCell(cell3);
		             		
		             		cell3 = new PdfPCell(new Phrase("Course",font4));
		             		cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
		             		cell3.setBorder(Rectangle.BOTTOM);
		             		table3.addCell(cell3);
		             		
		             		cell3 = new PdfPCell(new Phrase("Total",font4));
		             		cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
		             		cell3.setBorder(Rectangle.BOTTOM);
		             		table3.addCell(cell3);
		             		
		             		ListIterator<CourseMasterForm> itr= selectedCourseList.listIterator();
		             		int count=1;
		             		while (itr.hasNext()) {
		             			CourseMasterForm nextObj= itr.next();
		             			cell3 = new PdfPCell(new Phrase(String.valueOf(count),font3));
		             			cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
		             			cell3.setBorder(Rectangle.NO_BORDER);
		             			table3.addCell(cell3);
		             		
		             			cell3 = new PdfPCell(new Phrase(nextObj.getCourseName(),font3));
		             			cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
		             			cell3.setBorder(Rectangle.NO_BORDER);
		             			table3.addCell(cell3);
		             		
		             			long fees=nextObj.getFees();
		             			cell3 = new PdfPCell(new Phrase("Rs. "+String.valueOf(fees)+".00",font3));
		             			cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
		             			cell3.setBorder(Rectangle.NO_BORDER);
		             			table3.addCell(cell3);
		             			count++;
		             		}
		             		if(count<pageCounter){
		             		pageCounter=pageCounter-count;
		             		for(int i=1;i<=pageCounter;i++){
		             			cell3 = new PdfPCell(new Phrase(""));
		             			cell3.setColspan(3);
		             			cell3.setBorder(Rectangle.NO_BORDER);
		             			table3.addCell(cell3);
		             		}
		             		}
		             		float[] table3columnWidths = {12f, 75f, 25f};
		             		table3.setWidths(table3columnWidths);
		             		
		         PdfPTable tableTotalDesc = new PdfPTable(2); 
		         tableTotalDesc.setSpacingAfter(10f);
		      	 tableTotalDesc.setWidthPercentage(100);
		         PdfPCell       totalDesccell  = new PdfPCell(new Phrase("Sub Total : ",font4));
 		             						totalDesccell.setHorizontalAlignment(Element.ALIGN_RIGHT);
 		             						totalDesccell.setBorder(Rectangle.NO_BORDER);
 		             						tableTotalDesc.addCell(totalDesccell);
 		             		
 		             						totalDesccell  = new PdfPCell(new Phrase("Rs. "+String.valueOf(subAmount)+".00",font3));
 		             						totalDesccell.setHorizontalAlignment(Element.ALIGN_RIGHT);
 		             						totalDesccell.setBorder(Rectangle.NO_BORDER);
 		             						tableTotalDesc.addCell(totalDesccell);
		        		     
 		             						totalDesccell  = new PdfPCell(new Phrase("Service Tax @ "+serviceTax+"% ",font4));
		        		             		totalDesccell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		        		             		totalDesccell.setBorder(Rectangle.NO_BORDER);
		        		             		tableTotalDesc.addCell(totalDesccell);
		        		             		
		        		             		totalDesccell  = new PdfPCell(new Phrase("+ Rs. "+tax+".00",font3));
 		             						totalDesccell.setHorizontalAlignment(Element.ALIGN_RIGHT);
 		             						totalDesccell.setBorder(Rectangle.NO_BORDER);
 		             						tableTotalDesc.addCell(totalDesccell);
 		             						
 		             						totalDesccell  = new PdfPCell(new Phrase("Total Amount Discount@: ",font4));
		        		             		totalDesccell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		        		             		totalDesccell.setBorder(Rectangle.NO_BORDER);
		        		             		tableTotalDesc.addCell(totalDesccell);
		        		             		
		        		             		totalDesccell  = new PdfPCell(new Phrase("- Rs. "+discountAmount+".00",font3));
 		             						totalDesccell.setHorizontalAlignment(Element.ALIGN_RIGHT);
 		             						totalDesccell.setBorder(Rectangle.NO_BORDER);
 		             						tableTotalDesc.addCell(totalDesccell);
		        		             		
 		             						totalDesccell = new PdfPCell(new Phrase("Grand Total : ",font4));
		        		             		totalDesccell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		        		             		totalDesccell.setBorder(Rectangle.NO_BORDER);
		        		             		tableTotalDesc.addCell(totalDesccell);
		        		             		
		        		             		totalDesccell = new PdfPCell(new Phrase("Rs. "+totalAmount+".00",font3));
		        		             		totalDesccell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		        		             		totalDesccell.setBorder(Rectangle.NO_BORDER);
		        		             		tableTotalDesc.addCell(totalDesccell);
		        		             		
		        		             		float[] tableTotalDescWidths = {50f, 50f};
		        		             		tableTotalDesc.setWidths(tableTotalDescWidths);
		        		             		
		        		             		      		
		     PdfPTable tableinWord = new PdfPTable(2); 
		     tableinWord.setSpacingAfter(10f);
		     tableinWord.setWidthPercentage(100);
		     PdfPCell       tableinWordcell = new PdfPCell(new Phrase("Inword  :  ",font4));
		       				tableinWordcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		       				tableinWordcell.setBorder(Rectangle.NO_BORDER);
		      				tableinWord.addCell(tableinWordcell);
		        		      		
		       				tableinWordcell = new PdfPCell(new Phrase(inWord+" Rupees Only",font3));
		       	      		tableinWordcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		       	      		tableinWordcell.setBorder(Rectangle.NO_BORDER);
		       	      		tableinWord.addCell(tableinWordcell);
		       	      	
		       	      		float[] tableinWordWidths = {25f, 75f};
		       	      		tableinWord.setWidths(tableinWordWidths);
		       	      		
		       	      	 PdfPTable tableinWordDesc = new PdfPTable(2); 
		       	      	 tableinWordDesc.setSpacingAfter(10f);
		       	         tableinWordDesc.setWidthPercentage(100);
		    		     PdfPCell       tableinWordDesccell = new PdfPCell();
		    		     				tableinWordDesccell.setBorder(Rectangle.NO_BORDER);
		    		     				tableinWordDesccell.addElement(tableinWord);
		    		       				tableinWordDesc.addCell(tableinWordDesccell);
		    		        		      		
		    		       				tableinWordDesccell = new PdfPCell();
		    		       				tableinWordDesccell.setBorder(Rectangle.NO_BORDER);
		    		       				tableinWordDesccell.addElement(tableTotalDesc);
		    		       	      		tableinWordDesc.addCell(tableinWordDesccell);  
		    		      float[] tableinWordDesccolumnWidths = {50f,50f};
		    		      tableinWordDesc.setWidths(tableinWordDesccolumnWidths);
		    		       	      	 
		       
		       PdfPTable mainTable = new PdfPTable(2); 
		       mainTable.setWidthPercentage(100);		
		       PdfPCell    mainTableCell = new PdfPCell(new Phrase("INVOICE",font1));
		       			   mainTableCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		       			   mainTableCell.setBorder(Rectangle.NO_BORDER);
    		   			   mainTableCell.setColspan(2);
    		   			   mainTable.addCell(mainTableCell);
    		   
    		   			   mainTableCell = new PdfPCell();
		       			   mainTableCell.setBorder(Rectangle.BOTTOM);
		        		   mainTableCell.addElement(table1);
		        		   mainTable.addCell(mainTableCell);
		        			
		        		   mainTableCell = new PdfPCell();
		        		   mainTableCell.setBorder(Rectangle.BOTTOM);
		        		   mainTableCell.addElement(table2);
		        		   mainTable.addCell(mainTableCell);
		        		   
		        		   mainTableCell = new PdfPCell();
		        		   mainTableCell.setBorder(Rectangle.BOTTOM);
		        		   mainTableCell.setColspan(2);
		        		   mainTableCell.addElement(tableCust);
		        		   mainTable.addCell(mainTableCell);
		        		   
		        		   mainTableCell = new PdfPCell();
		        		   mainTableCell.setColspan(2);
		        		   mainTableCell.setBorder(Rectangle.BOTTOM);
		        		   mainTableCell.addElement(table3);
		        		   mainTable.addCell(mainTableCell);
		        		   
		        		   /*mainTableCell = new PdfPCell();
		        		   mainTableCell.setBorder(Rectangle.BOTTOM);
		        		   mainTableCell.addElement(tableinWord);
		        		   mainTable.addCell(mainTableCell);*/
		        		   
		        		   mainTableCell = new PdfPCell();
		        		   mainTableCell.setColspan(2);
		        		   mainTableCell.setBorder(Rectangle.BOTTOM);
		        		   mainTableCell.addElement(tableinWordDesc);
		        		   mainTable.addCell(mainTableCell);
		        		   
		        		   mainTableCell = new PdfPCell((new Phrase("This is a Computer Generated Invoice and does not require any Signature",font5)));
		        		   mainTableCell.setColspan(2);
		        		   mainTableCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		        		   mainTableCell.setBorder(Rectangle.NO_BORDER);
		        		   mainTable.addCell(mainTableCell);
		      
		document.add(mainTable);
		document.close();
		file.close();
		flag=1;
		System.out.println("Pdf generated successfully");
		
		} catch (Exception e) {
			e.printStackTrace();
		}  	
		return flag;
	}	
}
