package com.itech.elearning.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import com.itech.elearning.forms.EBookDetailsMasterForm;
import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.handler.EBookDetailsMasterHandler;
import com.itech.elearning.utils.FileUploadDownloadUtil;
import com.itech.elearning.utils.StringUtil;


public class EBookDetailsMasterAction extends BaseAction {
	EBookDetailsMasterHandler handler = new EBookDetailsMasterHandler();

	public ActionForward list(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		EBookDetailsMasterForm ebForm = (EBookDetailsMasterForm) form;
		HttpSession session = request.getSession();
       String compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		Long userid = null;
		int roleid = 0;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {

			List<EBookDetailsMasterForm> courseList = handler.listAllCourse(roleid,compid,userid,getDataSource(request));
			request.setAttribute("courseList", courseList);
						
			List<EBookDetailsMasterForm> eBookList = handler.list(roleid,compid,userid,ebForm.getCourseId(),getDataSource(request));
			request.setAttribute("eBookList", eBookList);
			System.out.println(eBookList);
			
			/*String eBookFileUploadPath= getServlet().getServletContext().getRealPath("/")+ "ebookPDF";
			request.setAttribute("eBookFileUploadPath", eBookFileUploadPath);*/
		}

		return mapping.findForward("eBookDetailsMaster");

	}

	public ActionForward add(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			HttpSession session = request.getSession();
			String compid = null;
			compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			EBookDetailsMasterForm ebForm = (EBookDetailsMasterForm) form;
			String eBookFileName="";
			String eBookFileUploadPath= getServlet().getServletContext().getRealPath("/")+ "ebookPDF";
			boolean uploadStatus=true;
			
			FormFile eFile= ebForm.getImageFile();
			if(eFile.getFileName()!="" && eFile.getFileSize()>2){
					eBookFileName=StringUtil.repStrUnScr(ebForm.geteBookName()+" "+eFile.getFileName());
					uploadStatus = FileUploadDownloadUtil.fileUploader(eFile, eBookFileUploadPath, eBookFileName);
			}
			if(uploadStatus){
				ebForm.setFileName(eBookFileName);
				request.setAttribute("status", handler.add(compid,ebForm,getDataSource(request)));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list(mapping, form, request, response);
	}

	public ActionForward changestatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			EBookDetailsMasterForm ebForm = (EBookDetailsMasterForm) form;
			Long eBookID=Long.parseLong((request.getParameter("eBookID")));
			boolean active=Boolean.parseBoolean(request.getParameter("active"));
			request.setAttribute("status", handler.changeStatus(ebForm,getDataSource(request),eBookID,active));
			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");
	}

	public ActionForward update(ActionMapping mapping, ActionForm form,	HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			EBookDetailsMasterForm ebForm = (EBookDetailsMasterForm) form;
			String eBookFileName="";
			String eBookFileUploadPath= getServlet().getServletContext().getRealPath("/")+ "ebookPDF";
			boolean uploadStatus=true;
			
			FormFile eFile= ebForm.getImageFile();
			if(eFile.getFileName()!="" && eFile.getFileSize()>2){
				eBookFileName=StringUtil.repStrUnScr(ebForm.geteBookName()+eFile.getFileName());
				uploadStatus = FileUploadDownloadUtil.fileUploader(eFile, eBookFileUploadPath, eBookFileName);
				ebForm.setFileName(eBookFileName);
			}
			
			else if(request.getParameter("hiddenpdffile")!=null||!request.getParameter("hiddenpdffile").equals(null))
			{
				System.out.println("inside actionnnnnnnn");
				eBookFileName=request.getParameter("hiddenpdffile");
				System.out.println("hideeeeeeeeeeeeeeeeee"+eBookFileName);
				ebForm.setFileName(eBookFileName);

				
			}
			request.setAttribute("status", handler.update(ebForm,getDataSource(request)));

			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");
	}
	 
	public ActionForward excelReport(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{

		try{
			EBookDetailsMasterForm ebForm = (EBookDetailsMasterForm) form;
			HttpSession session = request.getSession();

			Long userid = null;
			String compid = null;
			int roleid = 0;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {
				compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
				roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
				List<EBookDetailsMasterForm> eBookList = handler.list(roleid,compid,userid,ebForm.getCourseId(),getDataSource(request));
				request.setAttribute("eBookList", eBookList);
				String fileName="eBookList_master";
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition","inline;filename="+""+fileName+".xls");
			} else{
				return mapping.findForward("session");
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
	return mapping.findForward("excel");

	}
	
}
