package com.itech.elearning.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.StudentReportsForm;
import com.itech.elearning.handler.StudentReportsHandler;
import com.itech.elearning.utils.DateTimeUtil;
 
 
public class StudentReportsAction extends BaseAction{
	StudentReportsHandler handler = new StudentReportsHandler();
	public ActionForward list(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();

		Long userid = null;
		String compid = null;
		int roleid = 0;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		
		if (userid != null) {
			compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
			List< StudentReportsForm> studentList = handler.studentList(roleid,compid,userid,getDataSource(request));
			request.setAttribute("studentList", studentList);
			
			String studentId="-1";
			String fromDate="";
			String todate="";
			List<StudentReportsForm> studentDetailsList = handler.studentDetailsList(roleid,compid,userid,getDataSource(request),studentId,fromDate,todate);
			request.setAttribute("studentDetailsList", studentDetailsList);
			 
		}
		return mapping.findForward("studentReports");

		

	}
	
	public ActionForward getList(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();

		Long userid = null;
		String compid = null;
		int roleid = 0;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		
		if (userid != null) {
			compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
			List< StudentReportsForm> studentList = handler.studentList(roleid,compid,userid,getDataSource(request));
			request.setAttribute("studentList", studentList);
			
			String studentId=request.getParameter("userid");
			String fromDate=DateTimeUtil.formatSqlDate(request.getParameter("fromDate"));
			String todate=DateTimeUtil.formatSqlDate(request.getParameter("toDate"));
			
			List< StudentReportsForm> studentDetailsList = handler.studentDetailsList(roleid,compid,userid,getDataSource(request),studentId,fromDate,todate);
			request.setAttribute("studentDetailsList", studentDetailsList);
			
			 
		}
		return mapping.findForward("studentReports");
	}

}
