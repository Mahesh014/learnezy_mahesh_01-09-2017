package com.itech.elearning.action;

import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.itech.elearning.forms.CourseMasterForm;
import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.StudentRegistrationForm;
import com.itech.elearning.handler.CourseMasterHandler;
import com.itech.elearning.handler.LoginHandler;
import com.itech.elearning.handler.MYStudentRegistrationHandler;
import com.itech.elearning.handler.TaxMasterHandler;
import com.itech.elearning.handler.TransactionHandler;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.MailSendingUtil;
import com.itech.elearning.utils.NumberToWord;
import com.itech.elearning.utils.StringUtil;

public class AddNewCourseAction extends BaseAction{
	MYStudentRegistrationHandler handler=new MYStudentRegistrationHandler();
	LoginHandler handler1=new LoginHandler();
	InvoiceModuleAction pdf= new InvoiceModuleAction();
	CourseMasterHandler courseHandler= new CourseMasterHandler();
	TransactionHandler transactionHandler= new TransactionHandler();
	TaxMasterHandler taxHandler = new TaxMasterHandler();
	
	
	public ActionForward list(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		 
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
		String compid=((LoginForm)session.getAttribute("userDetail")).getCompanyId();
				List<StudentRegistrationForm> courselist = handler.courseNotTakenlist(getDataSource(request),userid,compid);
				request.setAttribute("courselist", courselist);
				
				HashMap<String, String> globasetting = taxHandler.globasetting(getDataSource(request));
				request.setAttribute("globasetting", globasetting);
				
				String serviceTax=globasetting.get("service tax").toString();
				request.setAttribute("serviceTax", serviceTax);
				
			 
				
				return mapping.findForward("addNewCourse");
		} else{	
			return mapping.findForward("session");
		}
	}
	
	public ActionForward addNewCourse(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception {
		String status= Common.FAILURE_MESSAGE;
		StudentRegistrationForm studentForm = (StudentRegistrationForm) form;
		HttpSession session = request.getSession();
		Long userid = null;
		
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			String courseList =StringUtil.sapratedByComa(request.getParameterValues("courseid"));
			int serviceTax=Integer.parseInt(request.getParameter("serviceTax"));
			int tax = studentForm.getTax();
			String compid = null;
			compid = ((LoginForm) session.getAttribute("userDetail")).getCompanyId();
			int totalAmount=studentForm.getTotalAmount();
			long userId=userid;
			int subAmount=studentForm.getSubAmount();
			int discountAmount=studentForm.getDiscountAmount();
			int paymentStatus=0;
			int addCourseFlag=handler.addNewCourse(getDataSource(request), courseList, userId);
			if ( addCourseFlag==1 && userId!=0 && totalAmount!=0) {
				int transactionID = transactionHandler.addTransaction(getDataSource(request), courseList, userId, tax, paymentStatus, totalAmount,compid);
				request.setAttribute("course", courseList);
				request.setAttribute("tamt",totalAmount);
				session.setAttribute("userName",session.getAttribute("userName").toString().substring(0, 4)+userId);
				session.setAttribute("courseList", courseList);
				session.setAttribute("userId",userId);
				session.setAttribute("totalAmount", totalAmount);
				session.setAttribute("tax", tax);
				session.setAttribute("transactionID", transactionID);
				session.setAttribute("subAmount", subAmount);
				session.setAttribute("discountAmount", discountAmount);
				session.setAttribute("serviceTax", serviceTax);
				request.setAttribute("status","Thank you for selecting a course, please make the payment");
				
				List<StudentRegistrationForm> selectedcourselist = handler.selectedcourselist(compid,getDataSource(request),courseList);
				request.setAttribute("selectedcourselist", selectedcourselist);
			
			 
				StringBuffer courseNames=new StringBuffer();
				StringBuffer courseNameCourseId=new StringBuffer();
				StringBuffer discountPercentage=new StringBuffer();
				int listSize=selectedcourselist.size();
				int count=0;
				ListIterator<StudentRegistrationForm> itr= selectedcourselist.listIterator();
				while (itr.hasNext()) {
					StudentRegistrationForm studentRegistrationForm = (StudentRegistrationForm) itr.next();
					String courseName=studentRegistrationForm.getCoursename();
					int courseId=studentRegistrationForm.getCourseid();
					String discount=studentRegistrationForm.getDiscountPercentage();
					if (courseName.length()<4) {
						courseNameCourseId.append(courseName+courseId);
					}else{
						courseNameCourseId.append(courseName.substring(0, 4)+courseId);
					}
					
					courseNames.append(courseName);
					discountPercentage.append(discount);
					
					
					if (count<listSize-1) {
						courseNames.append(",");
						discountPercentage.append(",");
						courseNameCourseId.append(",");
					}
					count=count+1;
					
				}
				session.setAttribute("courseNames", courseNameCourseId.toString());
				//session.setAttribute("courseNameCourseId", courseNameCourseId.toString());
				session.setAttribute("discountPercentage", discountPercentage.toString());
				
				System.out.println(courseNames.toString()+ "  "+discountPercentage.toString());
				return mapping.findForward("paymentGateway");
		 	}
			request.setAttribute("status",status);
			request.setAttribute("userId",userId);
		} else{	
			return mapping.findForward("session");
		}
		return list(mapping, studentForm, request, response);
		}
	
	public ActionForward PGI(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
try {

	 
	String courseList = session.getAttribute("courseList").toString();
	String userId = session.getAttribute("userId").toString();
	String totalAmount = session.getAttribute("totalAmount").toString();
	String tax = session.getAttribute("tax").toString();
	String subAmount =session.getAttribute("subAmount").toString();
	String transactionID = session.getAttribute("transactionID").toString();
	String discountAmount = session.getAttribute("discountAmount").toString();
	String serviceTax = session.getAttribute("serviceTax").toString();
	
	String PGIID = "1234";
	String CardNumber ="121244444";
	int transactionStatus=1;
	int paymentStatus=1;
	
	handler.updatePaymentStatus(getDataSource(request), userId, paymentStatus);
	transactionHandler.updateTransactionStatus(getDataSource(request),transactionID, transactionStatus, CardNumber, PGIID  );

	String itechLogofilePath = getServlet().getServletContext().getRealPath("/")+"/images/Itech Solutions logo.png";
	String fontPath = getServlet().getServletContext().getRealPath("/")+"/images/OpenSans-Regular.ttf";
	
	List<CourseMasterForm> selectedCourseList= courseHandler.courseListByCourseIds(courseList, getDataSource(request));
	StudentRegistrationForm studentData= handler.getStudentData(userId, courseList, getDataSource(request));
	
	String inWord=new NumberToWord().convertNumberToWords(Integer.parseInt(totalAmount));
	String fileName=StringUtil.repStrUnScr(studentData.getFirstName()+"_"+studentData.getLastName()+"_"+userId+"_Invoice.pdf");
	String filePath = getServlet().getServletContext().getRealPath("/")+"invoice"+"/"+fileName;
	
	int flag=InvoiceModuleAction.getPdfInoive(getDataSource(request),studentData,filePath,selectedCourseList,userId,totalAmount,tax,fontPath,itechLogofilePath,inWord,subAmount,transactionID,discountAmount,serviceTax);
	
	if(flag==1){
		String[] studentEmail={studentData.getEmailId()};
		String subject="Inovice :: Student Training Booking";
		String messageBody="Welcome to Itech Training Academy. Please Find the Attached Invoice";
		MailSendingUtil.postAttachedMail(studentEmail, filePath, subject, messageBody,fileName);
		request.setAttribute("status","Thank you for the Payment!");
	}else{
		request.setAttribute("status",Common.FAILURE_MESSAGE);
	}
} catch (Exception e) {
	e.printStackTrace();
}
return mapping.findForward("student");
}
}
