package com.itech.elearning.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.itech.elearning.forms.LanguageMasterForm;
import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.handler.LanguageMasterHandler;

public class LanguageMasterAction extends BaseAction {
	LanguageMasterHandler handler = new LanguageMasterHandler();

	public ActionForward list(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		Long userid = null;
		String compid = null;
		int roleid = 0;
		compid = ((LoginForm) session.getAttribute("userDetail")).getCompanyId();
		roleid = ((LoginForm) session.getAttribute("userDetail")).getRole();
		System.out.println("compid "+compid);
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
			roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			List<LanguageMasterForm> languagelist = handler.list(roleid,compid,getDataSource(request));
			request.setAttribute("languagelist", languagelist);
			return mapping.findForward("language");
		} else
			return mapping.findForward("session");

	}

	public ActionForward add(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		Long userid = null;
		String compid = null;
		
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			LanguageMasterForm languageForm = (LanguageMasterForm) form;
			
			request.setAttribute("status", handler.add(languageForm,compid,getDataSource(request)));
			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");

	}

	public ActionForward changestatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			LanguageMasterForm languageForm = (LanguageMasterForm) form;
			languageForm.setLanguageid(Integer.parseInt((request.getParameter("languageid"))));
			languageForm.setActive(Boolean.parseBoolean(request
					.getParameter("active")));
			request.setAttribute("status", handler.changestatus(languageForm,
					getDataSource(request)));
			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");
	}

	public ActionForward update(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			LanguageMasterForm languageForm = (LanguageMasterForm) form;
			request.setAttribute("status", handler.update(languageForm,
					getDataSource(request)));

			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");
	}
	public ActionForward excelReport(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{

		try{
			
			HttpSession session = request.getSession();
			
			Long userid = null;
			String compid = null;
			int roleid=0;
			try {
				userid = ((LoginForm)session.getAttribute("userDetail")).getUserId();
				compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
				roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {

				List<LanguageMasterForm> languagelist = handler.list(roleid,compid,getDataSource(request));
				System.out.println("anguagelist"+languagelist.iterator().next().getLanguagename());
				request.setAttribute("languagelist",languagelist);
				String fileName="Language_master";
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition","inline;filename="+""+fileName+".xls");
			} else{
				return mapping.findForward("session");
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
	return mapping.findForward("excel");

	}

	
	
	
	
	

}
