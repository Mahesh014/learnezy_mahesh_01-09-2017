package com.itech.elearning.action;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.itech.elearning.forms.ChangePasswordForm;
import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.handler.ChangePasswordHandler;
import com.itech.elearning.utils.Common;

public class ChangePasswordAction extends BaseAction {

	ChangePasswordHandler handler = new ChangePasswordHandler();

	public ActionForward list(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		return mapping.findForward("changepwd");
	}

	public ActionForward update(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		ChangePasswordForm changepwdform = (ChangePasswordForm) form;
		HttpSession session = request.getSession();
		try {
			String status=Common.FAILURE_MESSAGE;
			Long userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			if (userid != null) {
				if (changepwdform.getNewpass().equals(changepwdform.getConfpass())) { //To check NewPassword and Confirm Password
					if (!changepwdform.getNewpass().equals(changepwdform.getCurrpass())) {//To check NewPassword and current Password
						int result = handler.changePassword(changepwdform,getDataSource(request),userid);
						if (result == 1) {	status=Common.PASSWORD_CHAGED;}
						if (result == 2) {	status=Common.CURRENT_PASS_NOTMATCH;}
						
					} else {
					status = Common.SAME_NAMES_IN_ALL_FIELDS;
					}
				}else{
					status = Common.NEW_CONF_PASS_NOTMATCH;
				}
				
				request.setAttribute("status", status);
				return mapping.findForward("changepwd");
			} else{
				return mapping.findForward("session");
			}
		} catch (Exception e) {
			return mapping.findForward("session");
		}
	}
}
