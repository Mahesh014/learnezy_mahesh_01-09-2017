package com.itech.elearning.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.RatingForm;
import com.itech.elearning.forms.RoleForm;
import com.itech.elearning.handler.RatingHandler;

public class RatingAction extends BaseAction
{
	RatingHandler handler = new RatingHandler();
  public ActionForward list(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
  {
	  
		HttpSession session = request.getSession();
		Long userid = null;
		

		
	
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			List<RatingForm> courseList = handler.list(userid,getDataSource(request));
			request.setAttribute("courseList", courseList);
			System.out.println("kkkk "+courseList);
			return mapping.findForward("rating");
		} else
			return mapping.findForward("session");
	
  }
  
  
  public ActionForward add(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
  {
	  HttpSession session = request.getSession();
	  Long userid = null;
	  String emailid=null;
	  RatingForm RateForm = (RatingForm) form;
		

		
	
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
			emailid = ((LoginForm) session.getAttribute("userDetail"))
			.getEmailid();
		
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			List<RatingForm> courseList = handler.list(userid,getDataSource(request));
			request.setAttribute("courseList", courseList);
			System.out.println("kkkk "+courseList);
			
			request.setAttribute("status",handler.addrating(userid,emailid,RateForm,getDataSource(request)));
			

			return mapping.findForward("rating");
		} else
			return mapping.findForward("session");
  }
  
}
