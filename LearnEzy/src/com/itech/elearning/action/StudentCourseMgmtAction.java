package com.itech.elearning.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.StudentCourseMgmtForm;
import com.itech.elearning.handler.StudentCourseMgmtHandler;

public class StudentCourseMgmtAction extends BaseAction {

	StudentCourseMgmtHandler handler = new StudentCourseMgmtHandler();

	public ActionForward list(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		StudentCourseMgmtForm stdForm = (StudentCourseMgmtForm) form;
		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {

			handler.getStdInfo(getDataSource(request), Long.parseLong(request
					.getParameter("stdId")), request);
			List<StudentCourseMgmtForm> courseList = handler.listAll(userid,
					getDataSource(request));
			request.setAttribute("courseList", courseList);

			List<StudentCourseMgmtForm> studentList = handler.listStdList(
					userid, getDataSource(request), stdForm);
			request.setAttribute("studentList", studentList);
			request.setAttribute("stdForm", stdForm);

			return mapping.findForward("stdMgmt");
		}
		return list(mapping, stdForm, request, response);
	}

	public ActionForward add(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		System.out.println("hiiiiiiii");
		StudentCourseMgmtForm courseForm = (StudentCourseMgmtForm) form;		
		request.setAttribute("status", handler.addDetails(courseForm,
				getDataSource(request), Long.parseLong(request
						.getParameter("stdId"))));
		return list(mapping, form, request, response);
	}

}
