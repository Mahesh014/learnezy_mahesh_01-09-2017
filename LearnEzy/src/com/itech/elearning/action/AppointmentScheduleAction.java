package com.itech.elearning.action;

import java.util.List;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONArray;

import com.itech.elearning.forms.AppointmentForm;
import com.itech.elearning.forms.AppointmentScheduleForm;
import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.handler.AppointmentHandler;
import com.itech.elearning.handler.AppointmentScheduleHandler;
import com.itech.elearning.utils.StringUtil;

public class AppointmentScheduleAction extends BaseAction{
	AppointmentHandler handlerAppointment = new AppointmentHandler();
	AppointmentScheduleHandler handler = new AppointmentScheduleHandler();
	public ActionForward list(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {

		System.out.println("APPP");
		HttpSession session = request.getSession();
        int roleid = 0;
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			 AppointmentScheduleForm apptForm = (AppointmentScheduleForm)form;
			 
			  String compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			  List<AppointmentScheduleForm> appointmentScheduleList = handler.list(roleid,getDataSource(request),compid);
			  request.setAttribute("appointmentScheduleList",appointmentScheduleList);
			  
			  List<AppointmentScheduleForm> userList = handler.userList(roleid,compid,getDataSource(request));
			  request.setAttribute("userList",userList);
			  
			  List<AppointmentScheduleForm> courseList = handler.courseList(roleid,compid,getDataSource(request));
			  request.setAttribute("courseList", courseList);
			  
			  List<AppointmentForm> appointmentList = handlerAppointment.activeList(roleid,compid,getDataSource(request));
			  request.setAttribute("appointmentList",appointmentList);
			  
			 apptForm.setAppointmentId(-1);
			 apptForm.setCourseid(-1);
		}

		return mapping.findForward("appointmentSchedule");
	}
	

public ActionForward add(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
  {
	 
	  try{
			HttpSession session=request.getSession();
			Long userid = null;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {	 
				 String compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
				  System.out.println("Inside the add method");
				  String status=null;
				  AppointmentScheduleForm apptForm = (AppointmentScheduleForm)form;
				  String[] studentIdArry= request.getParameterValues("appointmentCreatedID");
				  apptForm.setStudentId(StringUtil.sapratedByComa(studentIdArry));
				 
				        status = handler.add(apptForm,getDataSource(request),userid,compid);
				        System.out.println(status);
				        request.setAttribute("status", status);
					
				  System.out.println("result "+status);


			}else{
				return mapping.findForward("sessionfailure");
			}
			
			}catch (Exception e) {
				e.printStackTrace();
			}
		
	  

	  return list(mapping, form, request, response);
  }
  
  
public ActionForward changestatus(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
  {
	 
	  
	  try{
			HttpSession session=request.getSession();
			Long userid = null;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {	 
				  AppointmentScheduleForm apptForm = (AppointmentScheduleForm)form;
				  apptForm.setAppointmentScheduleId(Integer.parseInt(request.getParameter("appointmentScheduleId").trim()));
				  apptForm.setActive(Boolean.parseBoolean(request.getParameter("active")));
				
				  System.out.println("haiiii  hhh");
				  request.setAttribute("status",handler.changeStatus(apptForm,getDataSource(request)));


			}else{
				return mapping.findForward("sessionfailure");
			}
			
			}catch (Exception e) {
				e.printStackTrace();
			}
		
	  
	  

	
	  return list(mapping, form, request, response);
  }
  
  
public ActionForward update(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
  {
	 
	  try{
			HttpSession session=request.getSession();
			Long userid = null;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {	 
				  AppointmentScheduleForm apptForm = (AppointmentScheduleForm)form;
				  String[] studentIdArry= request.getParameterValues("appointmentCreatedID");
				  apptForm.setStudentId(StringUtil.sapratedByComa(studentIdArry));
				  request.setAttribute("status", handler.update(apptForm,getDataSource(request),userid));



			}else{
				return mapping.findForward("sessionfailure");
			}
			
			}catch (Exception e) {
				e.printStackTrace();
			}
		
	  
	  
	

				return list(mapping, form, request, response);
  }
  
  
 
 
public ActionForward excelReport(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{

	try{
		 
		HttpSession session = request.getSession();

		Long userid = null;
		int roleid = 0;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			  String compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			  List<AppointmentScheduleForm> appointmentScheduleList = handler.list(roleid,getDataSource(request),compid);
			  request.setAttribute("appointmentScheduleList",appointmentScheduleList);
			  String fileName="Appointment_Schedule_master";
			  response.setContentType("application/vnd.ms-excel");
			  response.setHeader("Content-Disposition","inline;filename="+""+fileName+".xls");
		} else{
			return mapping.findForward("session");
	}
	}catch (Exception e) {
		e.printStackTrace();
	}
return mapping.findForward("excel");

}

public ActionForward getStudentName(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception {
	String studentIds= request.getParameter("studentIds");
	
	JSONArray jArray =handler.getStudentName(getDataSource(request),studentIds);
	response.getOutputStream().write(jArray.toJSONString().getBytes());
    return null;
}
}
