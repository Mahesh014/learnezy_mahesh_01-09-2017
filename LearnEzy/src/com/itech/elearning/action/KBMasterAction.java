package com.itech.elearning.action;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.itech.elearning.forms.KBMasterForm;
import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.handler.KBMasterHandler;

public class KBMasterAction  extends BaseAction{


	KBMasterHandler handler = new 	KBMasterHandler();
 
	public ActionForward list(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		KBMasterForm kbForm = (KBMasterForm) form;
		HttpSession session = request.getSession();
        int roleid = 0;
		Long userid = null;
		String compid = null;
		
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			try {
				compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
				roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
				List<KBMasterForm> courseList = handler.listAllCourse(roleid,compid,userid,getDataSource(request));
				request.setAttribute("courseList", courseList);
				
				List<KBMasterForm> KBlist = handler.list(roleid,compid,userid, getDataSource(request), kbForm.getCourseId());
				request.setAttribute("KBlist", KBlist);
				System.out.println("KB "+KBlist);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
	}

		return mapping.findForward("kbMaster");

	}
	
	

	public ActionForward add(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		String compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		KBMasterForm kbForm = (KBMasterForm) form;
		request.setAttribute("status", handler.add(compid,kbForm,getDataSource(request)));
		return list(mapping, form, request, response);
	}

	public ActionForward changestatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		
		Long kbId=Long.parseLong(request.getParameter("kbId"));
		boolean active= Boolean.parseBoolean(request.getParameter("active"));
		String check = handler.changeStatus(getDataSource(request),kbId,active);
		request.setAttribute("status", check);
		return list(mapping, form, request, response);
	}

	public ActionForward update(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			KBMasterForm kbForm = (KBMasterForm) form;
			request.setAttribute("status", handler.update(kbForm,getDataSource(request)));
			return list(mapping, form, request, response);
		} else
			return mapping.findForward("session");
	}
	public ActionForward excelReport(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{

		try{
			
			HttpSession session = request.getSession();
			KBMasterForm kbForm = (KBMasterForm) form;
			Long userid = null;
			int roleid = 0;
			String compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
				roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {

				List<KBMasterForm> KBlist = handler.list(roleid,compid,userid, getDataSource(request), kbForm.getCourseId());
				request.setAttribute("KBlist", KBlist);
				String fileName="KB_master";
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition","inline;filename="+""+fileName+".xls");
			} else{
				return mapping.findForward("session");
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
	return mapping.findForward("excel");

	}
	


}
