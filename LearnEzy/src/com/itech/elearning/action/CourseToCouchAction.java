package com.itech.elearning.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.itech.elearning.forms.CourseToCouchForm;
import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.handler.CourseToCouchHandler;

public class CourseToCouchAction extends BaseAction 
{
	CourseToCouchHandler handler = new CourseToCouchHandler();
	public ActionForward list(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
System.out.println("Course Actidfgfdgfgdfg ");
		CourseToCouchForm couchForm = (CourseToCouchForm) form;
		HttpSession session = request.getSession();
		Long userid = null;
		String compid = null;
		compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if(request.getParameter("stdId") != null)
		{
		request.setAttribute("stdId", request
					.getParameter("stdId"));
		}
		
		if (userid != null) {

			handler.getCouchInfo(getDataSource(request), Long.parseLong(request
					.getParameter("stdId")), request);
			List<CourseToCouchForm> courseList = handler.listAllCourse(compid,userid,
					getDataSource(request));
			request.setAttribute("courseList", courseList);
		//	List<CourseToCouchForm> topicList = handler.listAllTopoic(userid,
			//		getDataSource(request), couchForm.getCourseId());
	//		request.setAttribute("topicList", topicList);

			List<CourseToCouchForm> couchList = handler.listCouchList(compid,
					userid, getDataSource(request), couchForm,Long.parseLong(request
							.getParameter("stdId")) );
			request.setAttribute("couchList", couchList);
			
	
			

			
		}
		request.setAttribute("couchForm", couchForm);
		return mapping.findForward("courseMgmt");
	}
	
	
	public ActionForward add(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		String compid = null;
		compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		System.out.println("hiiiiiiii");
		
		CourseToCouchForm courseForm = (CourseToCouchForm) form;
		
		request.setAttribute("status", handler.addDetails(compid,courseForm,
				getDataSource(request), Long.parseLong(request
						.getParameter("stdId"))));
		return list(mapping, form, request, response);
	}

}
