package com.itech.elearning.action;

import com.memetix.mst.language.Language;
import com.memetix.mst.translate.Translate;

public class MicrosoftTranslator {
public static void main(String[] args) throws Exception {
        
        Translate.setClientId("secret client id");
        Translate.setClientSecret("secret key");


        String translatedText = Translate.execute("hello", Language.ENGLISH, Language.FRENCH);
        System.out.println(translatedText);
    }

}
