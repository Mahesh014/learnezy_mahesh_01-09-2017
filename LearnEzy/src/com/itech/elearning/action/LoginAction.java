package com.itech.elearning.action;

import java.io.IOException;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.StudentRegistrationForm;
import com.itech.elearning.handler.LoginHandler;
import com.itech.elearning.handler.MYStudentRegistrationHandler;
import com.itech.elearning.handler.TaxMasterHandler;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.FileUploadDownloadUtil;

public class LoginAction extends BaseAction {
	MYStudentRegistrationHandler handler11=new MYStudentRegistrationHandler();
	TaxMasterHandler taxHandler = new TaxMasterHandler();
	LoginHandler handler = new LoginHandler();
	
	public ActionForward login(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		LoginForm loginform = (LoginForm) form;
		String userName = loginform.getUserName();
		String password = loginform.getPassword();
		LoginForm userDetail = null;
		String compid = "";
		if (userName != null) {
			userDetail = handler.loginValidation(userName, password,getDataSource(request));
		} 
		HttpSession session = request.getSession();		
		
		if (userDetail != null) {
			compid =  userDetail.getCompanyId();
			System.out.println("compid **********************"+compid);
			session.setAttribute("userDetail", userDetail);
			session.setAttribute("username", userDetail.getUserName());
			session.setAttribute("userfirstname", userDetail.getFirstname());
			System.out.println("username in action###############################################"+userDetail.getUserName());
			System.out.println("username in action###############################################"+userDetail.getFirstname());

			session.setAttribute("compid", userDetail.getCompanyId());
			
			// for the Admin Login
			if (userDetail.getRole() == 1) {
				return mapping.findForward("adminhome");
			}
			// for the Couch Login
			else if (userDetail.getRole() == 2) {
				return mapping.findForward("couch");
			}
			// for the Student Login
			else if (userDetail.getRole() == 3) {
				long userId=userDetail.getUserId();
				session.setAttribute("userName",userName);
				int flag = handler.checkForCourseAndPayment(userId,getDataSource(request));
				flag=1;
				System.out.println("flag value "+flag);
				if (flag==0) {
					return selectCourse(mapping, loginform, request, response,userId);
				}else{
					int paymentFlag = handler.checkForPayment(userId,getDataSource(request));
					if(paymentFlag==0){
						return selectCourse(mapping, loginform, request, response,userId);
					}
				}
				return mapping.findForward("student");
			}else if (userDetail.getRole() == 5) {
				long userId=userDetail.getUserId();
				session.setAttribute("userName",userName);
				int paymentFlag = handler.checkForPayment(userId,getDataSource(request));
				if(paymentFlag==0){
					return AcademyRegistration(mapping, loginform, request, response,userId);
				}
				if(paymentFlag==1){
					return mapping.findForward("academyHome");
				 }				 
			} 
			else if (userDetail.getRole() == 6) {
				long userId=userDetail.getUserId();
				session.setAttribute("userName",userName);
				int paymentFlag = handler.checkForPayment(userId,getDataSource(request));
				if(paymentFlag==0){
					return AcademyRegistration(mapping, loginform, request, response,userId);
				}
				if(paymentFlag==1){
					return mapping.findForward("academyHome");
				 }				 
			}
			
			else if (userDetail.getRole() == 9) {//For Super Admin
				long userId=userDetail.getUserId();
				session.setAttribute("userName",userName);
				int paymentFlag = handler.checkForPayment(userId,getDataSource(request));
				if(paymentFlag==0){
					return AcademyRegistration(mapping, loginform, request, response,userId);
				}
				if(paymentFlag==1){
					return mapping.findForward("Superadmin");
				 }				 
			}
			else {
				String failure = "User doesn't exits";
				request.setAttribute("status", failure);
				return mapping.findForward("failure");
			}
		}
		else {
			String failure = "Username or password is Invalid";
			request.setAttribute("status", failure);
			return mapping.findForward("failure");
		}
		return mapping.findForward("failure");
	}


	public ActionForward admhome(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			return mapping.findForward("masterdataadmin");
		} else
			return mapping.findForward("session");
	}


	public ActionForward toHome(ActionMapping mapping, ActionForm form,	HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession session = request.getSession(true);
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();	
		} catch (NullPointerException e) {
			return mapping.findForward("session");	
		}
		if (userid != null) {
			return mapping.findForward("student");
		} else	
			return mapping.findForward("session");
	}
	
	public ActionForward selectCourse(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response, long userId) throws Exception {
				String status="Thank you for registering with LearnEZY, please select the course";
				HttpSession session = request.getSession();
				String compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
				List<StudentRegistrationForm> courselist = handler11.courselist1(compid,getDataSource(request));
				request.setAttribute("courselist", courselist);
				
				HashMap<String, String> globasetting = taxHandler.globasetting(getDataSource(request));
				request.setAttribute("globasetting", globasetting);
				
				String serviceTax=globasetting.get("service tax").toString();
				request.setAttribute("serviceTax", serviceTax);
				
				request.setAttribute("status",status);
				request.setAttribute("userId",userId);
				return mapping.findForward("studentRegistration1");
	}
	
	public ActionForward AcademyRegistration(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response,long userId) throws Exception {
		
   
	String status="Thank you for registering with LearnEZY, we will get back to you soon!!!";

				HttpSession session = request.getSession();
				String compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
				List<StudentRegistrationForm> courselist = handler11.courselist(compid,getDataSource(request));
				request.setAttribute("courselist", courselist);
				
				HashMap<String, String> globasetting = taxHandler.globasetting(getDataSource(request));
				request.setAttribute("globasetting", globasetting);
				
				String serviceTax=globasetting.get("service tax").toString();
				request.setAttribute("serviceTax", serviceTax);
				
				request.setAttribute("status",status);
				request.setAttribute("userId",userId);
		return mapping.findForward("academymakepayment");
	}
}
