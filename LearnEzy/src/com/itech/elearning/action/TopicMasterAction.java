package com.itech.elearning.action;

import java.util.List;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.TopicMasterForm;
import com.itech.elearning.handler.TopicMasterHandler;
import com.itech.elearning.utils.FileUpload;

public class TopicMasterAction extends BaseAction {

	TopicMasterHandler handler = new TopicMasterHandler();

	public ActionForward list(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		TopicMasterForm topicForm = (TopicMasterForm) form;
		HttpSession session = request.getSession();
		
		Long userid = null;
		String compid = null;
		int roleid = 0;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
            compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
            roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
			List<TopicMasterForm> courseList = handler.listAllCourse(roleid,compid,userid,getDataSource(request));
			
			
		TopicMasterForm temp_courseId=courseList.get(1);
		System.out.println("****************************"+temp_courseId);
		
			request.setAttribute("courseList", courseList);
                  System.out.println("sdfasdfasdfasdfasdfasdfasdfasdf"+topicForm.getCourseId());
			List<TopicMasterForm> topicList = handler.listAll(roleid,compid,userid,topicForm.getCourseId(),getDataSource(request));
			request.setAttribute("topicList", topicList);

		//	request.setAttribute("topicForm", topicForm);
			
			return mapping.findForward("TopicMaster");
		}

		return list(mapping, topicForm, request, response);

	}
	
	
	public ActionForward addCourse(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		String compid = null;
		compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
		System.out.println("hiiiiiiii");
		TopicMasterForm topicForm = (TopicMasterForm) form;
	
		String check = handler.addTopic(compid,topicForm, getDataSource(request));
		request.setAttribute("status", check);
		return list(mapping, form, request, response);
	}

	public ActionForward changestatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		TopicMasterForm topicForm = (TopicMasterForm) form;
		String check = handler.changeStatus(topicForm, getDataSource(request),
				Long.parseLong(request.getParameter("topicid")));
		request.setAttribute("status", check);
		return list(mapping, form, request, response);
	}

	/*public ActionForward edit(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		TopicMasterForm topicForm = (TopicMasterForm) form;
		topicForm = handler.get(Long.parseLong(request.getParameter("id")),
				getDataSource(request));
		request.setAttribute("topicForm", topicForm);
		return list(mapping, topicForm, request, response);
	}*/

	public ActionForward updateCourse(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		Long userid = null;
		try {
			userid = ((LoginForm) session.getAttribute("userDetail"))
					.getUserId();
		} catch (NullPointerException e) {
			return mapping.findForward("session");
		}
		if (userid != null) {
			TopicMasterForm topicForm = (TopicMasterForm) form;
			request.setAttribute("status", handler.updateTopic(topicForm,
					getDataSource(request)));
			return list(mapping, topicForm, request, response);
		} else
			return mapping.findForward("session");
	}
	
	public ActionForward excelReport(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{
		TopicMasterForm topicForm = (TopicMasterForm) form;
		try{
			HttpSession session=request.getSession();
			Long userid = null;
			String compid = null;
			int roleid = 0;
			try {
				userid = ((LoginForm) session.getAttribute("userDetail")).getUserId();
			} catch (NullPointerException e) {
				return mapping.findForward("session");
			}
			if (userid != null) {	 
			    compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();
			    roleid = ((LoginForm)session.getAttribute("userDetail")).getRole();
				List<TopicMasterForm> topicList = handler.listAll(roleid,compid,userid,topicForm.getCourseId(),getDataSource(request));
				request.setAttribute("topicList", topicList);

				
				String fileName="topic_master";
			    response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition","inline;filename="+""+fileName+".xls");
			}else{
				return mapping.findForward("sessionfailure");
			}

		}catch (Exception e) {
			e.printStackTrace();
		}

		return mapping.findForward("excel");
	}
	
public ActionForward topicExcelUpload(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{
		
		TopicMasterForm topicForm = (TopicMasterForm) form;
		HttpSession session = request.getSession();

		Long userid = null;
		String compid = null;
		
		String  fileName=null;
		boolean uploadstatus=false;
		String filePath=null;
		
		
		  try {
			  

			  if(!topicForm.getFilename().equals("")) { 
				  compid = ((LoginForm)session.getAttribute("userDetail")).getCompanyId();

					 FormFile myfile = topicForm.getFilename();
		 
					 filePath = getServlet().getServletContext().getRealPath("/") +"upload";
					 System.out.println("File Path="+filePath);
					 
					 fileName =myfile.getFileName();
					 String fileName1 = myfile.getFileName();
					 System.out.println("Filename="+fileName);
					 
					 
					 //Excel or csv file upload
					 uploadstatus=FileUpload.fileUploader(myfile, filePath, fileName);
					
						 filePath = getServlet().getServletContext().getRealPath("/")+ "upload\\";
						 						
						String success = handler.topicExcelUpload(fileName, filePath,compid);
						System.out.println("prashanth 111111111111111");
						
						request.setAttribute("status", success);
						
				 }
				  
				
		  }
		 catch (Exception e) {
					e.printStackTrace();
				}
		

			return list(mapping, topicForm, request, response);
		
	}
	


}
