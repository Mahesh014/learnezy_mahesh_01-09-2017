package com.itech.elearning.forms;

import org.apache.struts.validator.ValidatorForm;

@SuppressWarnings("serial")
public class TopicTextForm extends ValidatorForm {
	private Long topicID;
	private String topicName;
	private String topicText;
	private int topicDuration;

	public int getTopicDuration() {
		return topicDuration;
	}

	public void setTopicDuration(int topicDuration) {
		this.topicDuration = topicDuration;
	}

	public Long getTopicID() {
		return topicID;
	}

	public void setTopicID(Long topicID) {
		this.topicID = topicID;
	}

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public String getTopicText() {
		return topicText;
	}

	public void setTopicText(String topicText) {
		this.topicText = topicText;
	}

}
