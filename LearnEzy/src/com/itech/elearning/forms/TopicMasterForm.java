package com.itech.elearning.forms;

import org.apache.struts.upload.FormFile;
import org.apache.struts.validator.ValidatorForm;

/**
 * @author Itech
 * 
 */
public class TopicMasterForm extends ValidatorForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long topicId;
	private String topicName;
	private long courseId;
	private String courseName;

	private boolean active;
	
	private FormFile filename;

	public long getTopicId() {
		return topicId;
	}

	public void setTopicId(long topicId) {
		this.topicId = topicId;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}




	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public long getCourseId() {
		return courseId;
	}

	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public void setFilename(FormFile filename) {
		this.filename = filename;
	}

	public FormFile getFilename() {
		return filename;
	}

	

}
