package com.itech.elearning.forms;

import org.apache.struts.validator.ValidatorForm;

@SuppressWarnings("serial")
public class CourseScheduleForm extends ValidatorForm {

	private Long courseId;
	private Long topicId;
	private String topicName;
	private String courseName;
	private String tdate;
	private String ttime;
	private int duration;
	private boolean active;
	private Long schuduleId; 

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public Long getTopicId() {
		return topicId;
	}

	public void setTopicId(Long topicId) {
		this.topicId = topicId;
	}

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getTdate() {
		return tdate;
	}

	public void setTdate(String tdate) {
		this.tdate = tdate;
	}

	public String getTtime() {
		return ttime;
	}

	public void setTtime(String ttime) {
		this.ttime = ttime;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Long getSchuduleId() {
		return schuduleId;
	}

	public void setSchuduleId(Long schuduleId) {
		this.schuduleId = schuduleId;
	}

	
	
	

}
