package com.itech.elearning.forms;

import org.apache.struts.action.ActionForm;

public class PaymentMasterForm extends ActionForm{
   /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int paymentId;
	private int noOfCourses;
	private String paymentName;
	private String amount;
	private boolean active;
	private String editJson;
	public int getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(int paymentId) {
		this.paymentId = paymentId;
	}
	public int getNoOfCourses() {
		return noOfCourses;
	}
	public void setNoOfCourses(int noOfCourses) {
		this.noOfCourses = noOfCourses;
	}
	public String getPaymentName() {
		return paymentName;
	}
	public void setPaymentName(String paymentName) {
		this.paymentName = paymentName;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public String getEditJson() {
		return editJson;
	}
	public void setEditJson(String editJson) {
		this.editJson = editJson;
	}

}
