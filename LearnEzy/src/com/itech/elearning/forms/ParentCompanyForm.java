package com.itech.elearning.forms;

import org.apache.struts.action.ActionForm;

public class ParentCompanyForm extends ActionForm
{
  private int pcompid;
  private String pcompanyname;
  private boolean active;
public int getPcompid() {
	return pcompid;
}
public void setPcompid(int pcompid) {
	this.pcompid = pcompid;
}
public String getPcompanyname() {
	return pcompanyname;
}
public void setPcompanyname(String pcompanyname) {
	this.pcompanyname = pcompanyname;
}
public boolean isActive() {
	return active;
}
public void setActive(boolean active) {
	this.active = active;
}
}
