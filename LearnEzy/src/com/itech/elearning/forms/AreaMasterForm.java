package com.itech.elearning.forms;

import org.apache.struts.action.ActionForm;

public class AreaMasterForm extends ActionForm
{
	private long stateid;
	private boolean active;
	private long countryid;
	private long cityid;
	private long areaid;
	
	
	private String areaname;
	public long getStateid() {
		return stateid;
	}
	public void setStateid(long stateid) {
		this.stateid = stateid;
	}
	public long getAreaid() {
		return areaid;
	}
	public void setAreaid(long areaid) {
		this.areaid = areaid;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public long getCountryid() {
		return countryid;
	}
	public void setCountryid(long countryid) {
		this.countryid = countryid;
	}
	public long getCityid() {
		return cityid;
	}
	public void setCityid(long cityid) {
		this.cityid = cityid;
	}
	public String getAreaname() {
		return areaname;
	}
	public void setAreaname(String areaname) {
		this.areaname = areaname;
	}
	
	
	
}
