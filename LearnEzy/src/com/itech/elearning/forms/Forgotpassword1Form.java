package com.itech.elearning.forms;

import org.apache.struts.validator.ValidatorForm;

public class Forgotpassword1Form  extends ValidatorForm {

	private static final long serialVersionUID = 1L;
	private String emailid;
	private String password;
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}
	public String getEmailid() {
		return emailid;
	}
	
 

}
