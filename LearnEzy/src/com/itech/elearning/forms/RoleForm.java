package com.itech.elearning.forms;

import org.apache.struts.action.ActionForm;

public class RoleForm extends ActionForm {

	private static final long serialVersionUID = 1L;
	private Long roleid;
	private String rolename;
	private boolean active;

	public Long getRoleid() {
		return roleid;
	}

	public void setRoleid(Long roleid) {
		this.roleid = roleid;
	}

	public String getRolename() {
		return rolename;
	}

	public void setRolename(String rolename) {
		this.rolename = rolename;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isActive() {
		return active;
	}

}
