package com.itech.elearning.forms;

import org.apache.struts.action.ActionForm;

public class TaxMasterForm extends ActionForm{
   /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int taxId;
	private String taxName;
	private String taxValue;
	private boolean active;
	private String taxJson;
	
	public int getTaxId() {
		return taxId;
	}
	public void setTaxId(int taxId) {
		this.taxId = taxId;
	}
	public String getTaxName() {
		return taxName;
	}
	public void setTaxName(String taxName) {
		this.taxName = taxName;
	}
	public String getTaxValue() {
		return taxValue;
	}
	public void setTaxValue(String taxValue) {
		this.taxValue = taxValue;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public void setTaxJson(String taxJson) {
		this.taxJson = taxJson;
	}
	public String getTaxJson() {
		return taxJson;
	}
	
}
