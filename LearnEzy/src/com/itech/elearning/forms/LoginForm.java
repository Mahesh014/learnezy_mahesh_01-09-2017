package com.itech.elearning.forms;

import org.apache.struts.validator.ValidatorForm;

@SuppressWarnings("serial")
public class LoginForm extends ValidatorForm {
	
	private String userName;
	private String password;
	private Long userId;
	private String emailid;
	private int role;
	private String firstname;
	private String lastname;
	private Long testId;
	private String courseName;
	private String topicName;
	private String testDate;
	private String testType;
	private int testTypeId;
	private Long TopicID;
	private Long courseID;
	private String profilePhoto;
	private String companyId;
	private String companyname;
	private String courseids;
	private int courseid;
	private String coursename;
	private String theCourceIdForSending;
	private String fullName;	
	private String totalmemberjoined;
	private String userNameOrPasswordChnd;
	private String notificationfinder;
	private int totalNotification;
	private String notifications;
	private String courseNameForProgress;
	private int CourseProgressPercent;
	
	
	
	
	
	
	
	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getCompanyname() {
		return companyname;
	}

	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}

	public String getUserNameOrPasswordChnd() {
		return userNameOrPasswordChnd;
	}

	public void setUserNameOrPasswordChnd(String userNameOrPasswordChnd) {
		this.userNameOrPasswordChnd = userNameOrPasswordChnd;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Long getTestId() {
		return testId;
	}

	public void setTestId(Long testId) {
		this.testId = testId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public String getTestDate() {
		return testDate;
	}

	public void setTestDate(String testDate) {
		this.testDate = testDate;
	}

	public String getTestType() {
		return testType;
	}

	public void setTestType(String testType) {
		this.testType = testType;
	}

	public int getTestTypeId() {
		return testTypeId;
	}

	public void setTestTypeId(int testTypeId) {
		this.testTypeId = testTypeId;
	}

	public void setTopicID(Long topicID) {
		TopicID = topicID;
	}

	public Long getTopicID() {
		return TopicID;
	}

	public void setCourseID(Long courseID) {
		this.courseID = courseID;
	}

	public Long getCourseID() {
		return courseID;
	}

	public void setCourseid(int courseid) {
		this.courseid = courseid;
	}

	public int getCourseid() {
		return courseid;
	}

	public void setCoursename(String coursename) {
		this.coursename = coursename;
	}

	public String getCoursename() {
		return coursename;
	}

	public void setCourseids(String courseids) {
		this.courseids = courseids;
	}

	public String getCourseids() {
		return courseids;
	}

	public void setTheCourceIdForSending(String theCourceIdForSending) {
		this.theCourceIdForSending = theCourceIdForSending;
	}

	public String getTheCourceIdForSending() {
		return theCourceIdForSending;
	}

	public void setTotalmemberjoined(String totalmemberjoined) {
		this.totalmemberjoined = totalmemberjoined;
	}

	public String getTotalmemberjoined() {
		return totalmemberjoined;
	}

	public void setNotificationfinder(String notificationfinder) {
		this.notificationfinder = notificationfinder;
	}

	public String getNotificationfinder() {
		return notificationfinder;
	}

	public void setTotalNotification(int totalNotification) {
		this.totalNotification = totalNotification;
	}

	public int getTotalNotification() {
		return totalNotification;
	}

	public void setNotifications(String notifications) {
		this.notifications = notifications;
	}

	public String getNotifications() {
		return notifications;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setCourseNameForProgress(String courseNameForProgress) {
		this.courseNameForProgress = courseNameForProgress;
	}

	public String getCourseNameForProgress() {
		return courseNameForProgress;
	}

	public void setCourseProgressPercent(int courseProgressPercent) {
		CourseProgressPercent = courseProgressPercent;
	}

	public int getCourseProgressPercent() {
		return CourseProgressPercent;
	}

	public void setProfilePhoto(String profilePhoto) {
		this.profilePhoto = profilePhoto;
	}

	public String getProfilePhoto() {
		return profilePhoto;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public String getEmailid() {
		return emailid;
	}

	
	
	
	
	

}
