package com.itech.elearning.forms;

import org.apache.struts.upload.FormFile;
import org.apache.struts.validator.ValidatorForm;

public class StudentRegistrationForm extends ValidatorForm{
	private static final long serialVersionUID = 1L;

	private String firstName;
	private String middleName;
	private String lastName;
	private String dob;
	private String gender;
	private String qualification;
	private String contactno;
	private String address;
	private String phone;
	private String uname;
	private String password;
	private int totalAmount;
	private int subAmount;
	private Long roleId;
	private String rolename;
	private String emailId;
	private boolean active;
	private Long userid;
	private int studenid;
	private FormFile imageFile;
	private int courseid;
	private String coursename;
	private int discountAmount;
	private String areofexper;
	private String coursesoffered;
	private String areaid;
	
	
	
	
	public String getAreaid() {
		return areaid;
	}
	public void setAreaid(String areaid) {
		this.areaid = areaid;
	}
	public String getCoursesoffered() {
		return coursesoffered;
	}
	public void setCoursesoffered(String coursesoffered) {
		this.coursesoffered = coursesoffered;
	}
	public String getAreofexper() {
		return areofexper;
	}
	public void setAreofexper(String areofexper) {
		this.areofexper = areofexper;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	private String discountPercentage;
	private int courseAmount;
	
	private int transactionID;
	private int coursesId;
	private int  tax;
	private String  PGIID;
	private String  cardNumber;
	private String  transactionStatus;
	private String  datetime;

	private int stateid;
	private String  statename;
	private int countryid;
	private String  countryname;
	private int cityid;
	private String cityname;
	private int pincode;
	
	private String  otherstatename;
	private String othercityname;
	private String  othercountryname;
	
	
	public String getOtherstatename() {
		return otherstatename;
	}
	public void setOtherstatename(String otherstatename) {
		this.otherstatename = otherstatename;
	}
	public String getOthercityname() {
		return othercityname;
	}
	public void setOthercityname(String othercityname) {
		this.othercityname = othercityname;
	}
	public String getOthercountryname() {
		return othercountryname;
	}
	public void setOthercountryname(String othercountryname) {
		this.othercountryname = othercountryname;
	}
	public int getStateid() {
		return stateid;
	}
	public void setStateid(int stateid) {
		this.stateid = stateid;
	}
	public String getStatename() {
		return statename;
	}
	public void setStatename(String statename) {
		this.statename = statename;
	}
	public int getCountryid() {
		return countryid;
	}
	public void setCountryid(int countryid) {
		this.countryid = countryid;
	}
	public String getCountryname() {
		return countryname;
	}
	public void setCountryname(String countryname) {
		this.countryname = countryname;
	}
	public int getCityid() {
		return cityid;
	}
	public void setCityid(int cityid) {
		this.cityid = cityid;
	}
	public String getCityname() {
		return cityname;
	}
	public void setCityname(String cityname) {
		this.cityname = cityname;
	}
	public int getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(int transactionID) {
		this.transactionID = transactionID;
	}
	public int getCoursesId() {
		return coursesId;
	}
	public void setCoursesId(int coursesId) {
		this.coursesId = coursesId;
	}
	public int getTax() {
		return tax;
	}
	public void setTax(int tax) {
		this.tax = tax;
	}
	public String getPGIID() {
		return PGIID;
	}
	public void setPGIID(String pGIID) {
		PGIID = pGIID;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getTransactionStatus() {
		return transactionStatus;
	}
	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}
	public String getDatetime() {
		return datetime;
	}
	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	public String getContactno() {
		return contactno;
	}
	public void setContactno(String contactno) {
		this.contactno = contactno;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public String getRolename() {
		return rolename;
	}
	public void setRolename(String rolename) {
		this.rolename = rolename;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public Long getUserid() {
		return userid;
	}
	public void setUserid(Long userid) {
		this.userid = userid;
	}
	public void setStudenid(int studenid) {
		this.studenid = studenid;
	}
	public int getStudenid() {
		return studenid;
	}
	public void setCoursename(String coursename) {
		this.coursename = coursename;
	}
	public String getCoursename() {
		return coursename;
	}
	public void setCourseid(int courseid) {
		this.courseid = courseid;
	}
	public int getCourseid() {
		return courseid;
	}
	public void setImageFile(FormFile imageFile) {
		this.imageFile = imageFile;
	}
	public FormFile getImageFile() {
		return imageFile;
	}
	public int getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(int totalAmount) {
		this.totalAmount = totalAmount;
	}
	public int getSubAmount() {
		return subAmount;
	}
	public void setSubAmount(int subAmount) {
		this.subAmount = subAmount;
	}
	public int getPincode() {
		return pincode;
	}
	public void setPincode(int pincode) {
		this.pincode = pincode;
	}
	public int getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(int discountAmount) {
		this.discountAmount = discountAmount;
	}
	public void setDiscountPercentage(String discountPercentage) {
		this.discountPercentage = discountPercentage;
	}
	public String getDiscountPercentage() {
		return discountPercentage;
	}
	public void setCourseAmount(int courseAmount) {
		this.courseAmount = courseAmount;
	}
	public int getCourseAmount() {
		return courseAmount;
	}
	
	
	
}
