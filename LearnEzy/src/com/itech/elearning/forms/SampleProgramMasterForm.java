package com.itech.elearning.forms;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class SampleProgramMasterForm extends ActionForm {
	/**, name, subTopicId, sampleProgramContent, 
	active
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long topicId;
	private String topicName;
	private long courseId;
	private String courseName;
	private long subTopicId;
	private String subTopicName;
		
	
	private long sampleProgramID;
	private String name;
	private String sampleProgramContent;
	private String jsonEdit;
	private boolean active;
	
	private FormFile fileName;
	
	
	
	public long getSampleProgramID() {
		return sampleProgramID;
	}

	public void setSampleProgramID(long sampleProgramID) {
		this.sampleProgramID = sampleProgramID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSampleProgramContent() {
		return sampleProgramContent;
	}

	public void setSampleProgramContent(String sampleProgramContent) {
		this.sampleProgramContent = sampleProgramContent;
	}

	

	public long getTopicId() {
		return topicId;
	}

	public void setTopicId(long topicId) {
		this.topicId = topicId;
	}

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public long getCourseId() {
		return courseId;
	}

	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public long getSubTopicId() {
		return subTopicId;
	}

	public void setSubTopicId(long subTopicId) {
		this.subTopicId = subTopicId;
	}

	public String getSubTopicName() {
		return subTopicName;
	}

	public void setSubTopicName(String subTopicName) {
		this.subTopicName = subTopicName;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getJsonEdit() {
		return jsonEdit;
	}

	public void setJsonEdit(String jsonEdit) {
		this.jsonEdit = jsonEdit;
	}

	public void setFileName(FormFile fileName) {
		this.fileName = fileName;
	}

	public FormFile getFileName() {
		return fileName;
	}

	
}
