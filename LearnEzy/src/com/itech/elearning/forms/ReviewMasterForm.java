package com.itech.elearning.forms;

import org.apache.struts.action.ActionForm;

public class ReviewMasterForm extends ActionForm{

	
	
	private static final long serialVersionUID = 1L;
	private int reviewId;
	private int userId;
	
	private String reviewDescription,emailid;
	
	private int courseId;
	private String courseName,rating;
	
	private int companyId;
	private String companyName;
	
	private String createDateTime;
	private boolean approvalStatus;
	private boolean active;
	
	private int courseList;
	
	
	
	
	
	public int getReviewId() {
		return reviewId;
	}
	public void setReviewId(int reviewId) {
		this.reviewId = reviewId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getReviewDescription() {
		return reviewDescription;
	}
	public void setReviewDescription(String reviewDescription) {
		this.reviewDescription = reviewDescription;
	}
	public int getCourseId() {
		return courseId;
	}
	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}
	public int getCompanyId() {
		return companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	public String getCreateDateTime() {
		return createDateTime;
	}
	public void setCreateDateTime(String createDateTime) {
		this.createDateTime = createDateTime;
	}
	public void setApprovalStatus(boolean approvalStatus) {
		this.approvalStatus = approvalStatus;
	}
	public boolean isApprovalStatus() {
		return approvalStatus;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public boolean isActive() {
		return active;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCourseList(int courseList) {
		this.courseList = courseList;
	}
	public int getCourseList() {
		return courseList;
	}
	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}
	public String getEmailid() {
		return emailid;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public String getRating() {
		return rating;
	}
	
	
	
	
	
}
