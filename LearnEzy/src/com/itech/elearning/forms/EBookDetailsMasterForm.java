package com.itech.elearning.forms;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;


public class EBookDetailsMasterForm extends ActionForm {
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private Long eBookID;
	private Long courseId;
	private String courseName;
	private String fileName;
	private String eBookName;
	private String jEdit;
	private boolean active;
	private FormFile imageFile;
	
	public Long geteBookID() {
		return eBookID;
	}
	public void seteBookID(Long eBookID) {
		this.eBookID = eBookID;
	}
	public Long getCourseId() {
		return courseId;
	}
	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String geteBookName() {
		return eBookName;
	}
	public void seteBookName(String eBookName) {
		this.eBookName = eBookName;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	 
	public void setImageFile(FormFile imageFile) {
		this.imageFile = imageFile;
	}
	public FormFile getImageFile() {
		return imageFile;
	}
	public String getjEdit() {
		return jEdit;
	}
	public void setjEdit(String jEdit) {
		this.jEdit = jEdit;
	}
	 
	 
	 
	 
}
