package com.itech.elearning.forms;

import org.apache.struts.upload.FormFile;
import org.apache.struts.validator.ValidatorForm;

@SuppressWarnings("serial")
public class CourseMasterForm extends ValidatorForm {
	private Long courseId;
	private String courseName;
	
	private String courseimage;
	private String instructorimages;
private String dispercentage;
	
	private Long fees;
	private int maxStd;
	private int duration;
	private int totalDiscountAmount;
	private boolean active;
	private String passpercent;
	private long topicId;
	private String discountValidUpTo;
	private String discountValidUpToDisplay;
	private FormFile excelFile; 
	  private int coursecategoryId;
	  private String coursecategoryname;
	  private String elearning;
	  private String classroom;
	  private String free;
	  private String level;
	  private FormFile instructorimage;
	  private String instructorname;
	  private String availableseats;
	  private String coursestartdate;
	 public FormFile getInstructorimage() {
		return instructorimage;
	}

	public void setInstructorimage(FormFile instructorimage) {
		this.instructorimage = instructorimage;
	}

	public String getInstructorname() {
		return instructorname;
	}

	public void setInstructorname(String instructorname) {
		this.instructorname = instructorname;
	}

	public String getAvailableseats() {
		return availableseats;
	}

	public void setAvailableseats(String availableseats) {
		this.availableseats = availableseats;
	}

	public String getCoursestartdate() {
		return coursestartdate;
	}

	public void setCoursestartdate(String coursestartdate) {
		this.coursestartdate = coursestartdate;
	}

	public String getFree() {
		return free;
	}

	public void setFree(String free) {
		this.free = free;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getElearning() {
		return elearning;
	}

	public void setElearning(String elearning) {
		this.elearning = elearning;
	}

	public String getClassroom() {
		return classroom;
	}

	public void setClassroom(String classroom) {
		this.classroom = classroom;
	}

	private FormFile imageFile;
	  private String logo;
	   
	   
	   private String courseofferedid;
	   
	   
	   
	   public String getCourseofferedid() {
		return courseofferedid;
	}

	public void setCourseofferedid(String courseofferedid) {
		this.courseofferedid = courseofferedid;
	}

	public String getCourseofferedname() {
		return courseofferedname;
	}

	public void setCourseofferedname(String courseofferedname) {
		this.courseofferedname = courseofferedname;
	}

	private String courseofferedname;
	   
	   
	   
	
	public FormFile getImageFile() {
		return imageFile;
	}

	public void setImageFile(FormFile imageFile) {
		this.imageFile = imageFile;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public int getCoursecategoryId() {
		return coursecategoryId;
	}

	public void setCoursecategoryId(int coursecategoryId) {
		this.coursecategoryId = coursecategoryId;
	}

	public String getCoursecategoryname() {
		return coursecategoryname;
	}

	public void setCoursecategoryname(String coursecategoryname) {
		this.coursecategoryname = coursecategoryname;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public Long getFees() {
		return fees;
	}

	public void setFees(Long fees) {
		this.fees = fees;
	}

	public int getMaxStd() {
		return maxStd;
	}

	public void setMaxStd(int maxStd) {
		this.maxStd = maxStd;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public void setPasspercent(String passpercent) {
		this.passpercent = passpercent;
	}

	public String getPasspercent() {
		return passpercent;
	}

	public void setTopicId(long topicId) {
		this.topicId = topicId;
	}

	public long getTopicId() {
		return topicId;
	}

	public int getTotalDiscountAmount() {
		return totalDiscountAmount;
	}

	public void setTotalDiscountAmount(int totalDiscountAmount) {
		this.totalDiscountAmount = totalDiscountAmount;
	}

	public String getDiscountValidUpTo() {
		return discountValidUpTo;
	}

	public void setDiscountValidUpTo(String discountValidUpTo) {
		this.discountValidUpTo = discountValidUpTo;
	}

	public String getDiscountValidUpToDisplay() {
		return discountValidUpToDisplay;
	}

	public void setDiscountValidUpToDisplay(String discountValidUpToDisplay) {
		this.discountValidUpToDisplay = discountValidUpToDisplay;
	}

	public void setExcelFile(FormFile excelFile) {
		this.excelFile = excelFile;
	}

	public FormFile getExcelFile() {
		return excelFile;
	}

	public void setInstructorimages(String instructorimages) {
		this.instructorimages = instructorimages;
	}

	public String getInstructorimages() {
		return instructorimages;
	}

	public void setCourseimage(String courseimage) {
		this.courseimage = courseimage;
	}

	public String getCourseimage() {
		return courseimage;
	}

	public void setDispercentage(String dispercentage) {
		this.dispercentage = dispercentage;
	}

	public String getDispercentage() {
		return dispercentage;
	}

	 
}
