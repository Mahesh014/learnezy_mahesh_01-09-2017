package com.itech.elearning.forms;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;


public class AssignmentMasterForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8610834010183143417L;
	private Long assignmentId;
	private Long courseId;
	private Long topicId;
	private String courseName;
	private String topicName;
	private String assignmentQuestion;
	private String assignmentAnswer;
	private String jEdit;
	private boolean active;
	private FormFile excelFile; 
	private Long subTopicId;
	private String subTopicName;
	public Long getCourseId() {
		return courseId;
	}
	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}
	public Long getTopicId() {
		return topicId;
	}
	public void setTopicId(Long topicId) {
		this.topicId = topicId;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getTopicName() {
		return topicName;
	}
	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}
	public String getAssignmentQuestion() {
		return assignmentQuestion;
	}
	public void setAssignmentQuestion(String assignmentQuestion) {
		this.assignmentQuestion = assignmentQuestion;
	}
	public String getAssignmentAnswer() {
		return assignmentAnswer;
	}
	public void setAssignmentAnswer(String assignmentAnswer) {
		this.assignmentAnswer = assignmentAnswer;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public void setAssignmentId(Long assignmentId) {
		this.assignmentId = assignmentId;
	}
	public Long getAssignmentId() {
		return assignmentId;
	}
	public String getSubTopicName() {
		return subTopicName;
	}
	public void setSubTopicName(String subTopicName) {
		this.subTopicName = subTopicName;
	}
	public Long getSubTopicId() {
		return subTopicId;
	}
	public void setSubTopicId(Long subTopicId) {
		this.subTopicId = subTopicId;
	}
	public String getjEdit() {
		return jEdit;
	}
	public void setjEdit(String jEdit) {
		this.jEdit = jEdit;
	}
	public void setExcelFile(FormFile excelFile) {
		this.excelFile = excelFile;
	}
	public FormFile getExcelFile() {
		return excelFile;
	}
	
	 
}
