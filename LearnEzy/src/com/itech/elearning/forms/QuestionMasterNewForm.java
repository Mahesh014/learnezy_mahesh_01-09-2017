package com.itech.elearning.forms;

import org.apache.struts.upload.FormFile;
import org.apache.struts.validator.ValidatorForm;

public class QuestionMasterNewForm extends ValidatorForm {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long courseId;
	private long topicId;
	private long testId;
	private long questionId;
	private String courseName;
	private String topicName;
	private String testName;
	private boolean active;
	private long testType;
	private String question;
	private String descriptiveAns;
	private String ansA;
	private String ansB;
	private String ansC;
	private String ansD;
	private String anscrctAns;
	private String crctAns;
	private String testTypeName;
	private String jEdit;
	private FormFile excelFile; 
	
	public long getCourseId() {
		return courseId;
	}
	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}
	public long getTopicId() {
		return topicId;
	}
	public void setTopicId(long topicId) {
		this.topicId = topicId;
	}
	public long getTestId() {
		return testId;
	}
	public void setTestId(long testId) {
		this.testId = testId;
	}
	public long getQuestionId() {
		return questionId;
	}
	public void setQuestionId(long questionId) {
		this.questionId = questionId;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getTopicName() {
		return topicName;
	}
	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}
	public String getTestName() {
		return testName;
	}
	public void setTestName(String testName) {
		this.testName = testName;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public long getTestType() {
		return testType;
	}
	public void setTestType(long testType) {
		this.testType = testType;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getDescriptiveAns() {
		return descriptiveAns;
	}
	public void setDescriptiveAns(String descriptiveAns) {
		this.descriptiveAns = descriptiveAns;
	}
	public String getAnsA() {
		return ansA;
	}
	public void setAnsA(String ansA) {
		this.ansA = ansA;
	}
	public String getAnsB() {
		return ansB;
	}
	public void setAnsB(String ansB) {
		this.ansB = ansB;
	}
	public String getAnsC() {
		return ansC;
	}
	public void setAnsC(String ansC) {
		this.ansC = ansC;
	}
	public String getAnsD() {
		return ansD;
	}
	public void setAnsD(String ansD) {
		this.ansD = ansD;
	}
	public String getAnscrctAns() {
		return anscrctAns;
	}
	public void setAnscrctAns(String anscrctAns) {
		this.anscrctAns = anscrctAns;
	}
	public String getCrctAns() {
		return crctAns;
	}
	public void setCrctAns(String crctAns) {
		this.crctAns = crctAns;
	}
	public String getTestTypeName() {
		return testTypeName;
	}
	public void setTestTypeName(String testTypeName) {
		this.testTypeName = testTypeName;
	}
	public String getjEdit() {
		return jEdit;
	}
	public void setjEdit(String jEdit) {
		this.jEdit = jEdit;
	}
	public void setExcelFile(FormFile excelFile) {
		this.excelFile = excelFile;
	}
	public FormFile getExcelFile() {
		return excelFile;
	}

	
	
	
	
}
