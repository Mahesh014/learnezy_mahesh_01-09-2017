package com.itech.elearning.forms;

import org.apache.struts.validator.ValidatorForm;

public class TestQuestionMasterForm extends ValidatorForm{
	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1L;
	
	private long topicId;
	private String topicName;
	
	private long courseId;
	private String courseName;
	
	private long subTopicId;
	public String getTopicName() {
		return topicName;
	}
	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}
	public long getCourseId() {
		return courseId;
	}
	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public long getSubTopicId() {
		return subTopicId;
	}
	public void setSubTopicId(long subTopicId) {
		this.subTopicId = subTopicId;
	}
	public String getSubTopicName() {
		return subTopicName;
	}
	public void setSubTopicName(String subTopicName) {
		this.subTopicName = subTopicName;
	}
	public String getSubTopicText() {
		return subTopicText;
	}
	public void setSubTopicText(String subTopicText) {
		this.subTopicText = subTopicText;
	}
	public long getSsubTopicId() {
		return ssubTopicId;
	}
	public void setSsubTopicId(long ssubTopicId) {
		this.ssubTopicId = ssubTopicId;
	}
	public String getSsubTopicName() {
		return ssubTopicName;
	}
	public void setSsubTopicName(String ssubTopicName) {
		this.ssubTopicName = ssubTopicName;
	}

	private String subTopicName;
	
	private String subTopicText;
	
	private long ssubTopicId;
	private String ssubTopicName;
	public void setTopicId(long topicId) {
		this.topicId = topicId;
	}
	public long getTopicId() {
		return topicId;
	}
}
