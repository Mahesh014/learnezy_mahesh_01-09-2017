package com.itech.elearning.forms;

import org.apache.struts.action.ActionForm;

public class ChangePasswordForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8282534113708251768L;
	private String currpass;
	private String newpass;
	private String confpass;

	public String getCurrpass() {
		return currpass;
	}

	public void setCurrpass(String currpass) {
		this.currpass = currpass;
	}

	public String getNewpass() {
		return newpass;
	}

	public void setNewpass(String newpass) {
		this.newpass = newpass;
	}

	public String getConfpass() {
		return confpass;
	}

	public void setConfpass(String confpass) {
		this.confpass = confpass;
	}

}
