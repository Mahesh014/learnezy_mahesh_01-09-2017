package com.itech.elearning.forms;

import org.apache.struts.action.ActionForm;

 
public class StudentForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long courseId;
	private String courseName;
	private long subTopicId;
	private String subTopicName;
	private long topicId;
	private String topicName;
	private String notes ;
	private int notesid;
	public long getTopicId() {
		return topicId;
	}
	public void setTopicId(long topicId) {
		this.topicId = topicId;
	}
	public String getTopicName() {
		return topicName;
	}
	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}
	public long getCourseId() {
		return courseId;
	}
	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public long getSubTopicId() {
		return subTopicId;
	}
	public void setSubTopicId(long subTopicId) {
		this.subTopicId = subTopicId;
	}
	public String getSubTopicName() {
		return subTopicName;
	}
	public void setSubTopicName(String subTopicName) {
		this.subTopicName = subTopicName;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotesid(int notesid) {
		this.notesid = notesid;
	}
	public int getNotesid() {
		return notesid;
	}
	
	 
}
