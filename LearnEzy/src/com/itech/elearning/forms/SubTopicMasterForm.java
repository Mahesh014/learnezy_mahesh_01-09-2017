package com.itech.elearning.forms;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class SubTopicMasterForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long topicId;
	private String topicName;
	private long courseId;
	private String courseName;
	private long subTopicId;
	private String subTopicName;
    private String compid;
    
    
	public String getCompid() {
		return compid;
	}

	public void setCompid(String compid) {
		this.compid = compid;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	private FormFile excelFile; 

	private boolean active;

	public long getTopicId() {
		return topicId;
	}

	public void setTopicId(long topicId) {
		this.topicId = topicId;
	}

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public long getCourseId() {
		return courseId;
	}

	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public long getSubTopicId() {
		return subTopicId;
	}

	public void setSubTopicId(long subTopicId) {
		this.subTopicId = subTopicId;
	}

	public String getSubTopicName() {
		return subTopicName;
	}

	public void setSubTopicName(String subTopicName) {
		this.subTopicName = subTopicName;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public void setExcelFile(FormFile excelFile) {
		this.excelFile = excelFile;
	}

	public FormFile getExcelFile() {
		return excelFile;
	}

	
}
