package com.itech.elearning.forms;

import org.apache.struts.action.ActionForm;

public class StudentSessionClassForm  extends ActionForm{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int appointmentScheduleId;
	private int appointmentCreatedID;
	private int courseid;
	private String appointmentType;
	private String status;
	private String sessionStatus;
	private String courseName;
	private String appointmentDateTime;
	private boolean active;
	private String meetingName;
	private String meetingURLLink;
	private String whiteBoardSessionID;
	private String accessPassword;
	private String whiteBoardUserName;
	private String whiteBoardSessionTitle;
	
	public String getWhiteBoardSessionID() {
		return whiteBoardSessionID;
	}
	public void setWhiteBoardSessionID(String whiteBoardSessionID) {
		this.whiteBoardSessionID = whiteBoardSessionID;
	}
	public String getAccessPassword() {
		return accessPassword;
	}
	public void setAccessPassword(String accessPassword) {
		this.accessPassword = accessPassword;
	}
	public String getWhiteBoardUserName() {
		return whiteBoardUserName;
	}
	public void setWhiteBoardUserName(String whiteBoardUserName) {
		this.whiteBoardUserName = whiteBoardUserName;
	}
	public String getWhiteBoardSessionTitle() {
		return whiteBoardSessionTitle;
	}
	public void setWhiteBoardSessionTitle(String whiteBoardSessionTitle) {
		this.whiteBoardSessionTitle = whiteBoardSessionTitle;
	}
	public int getAppointmentScheduleId() {
		return appointmentScheduleId;
	}
	public void setAppointmentScheduleId(int appointmentScheduleId) {
		this.appointmentScheduleId = appointmentScheduleId;
	}
	public int getAppointmentCreatedID() {
		return appointmentCreatedID;
	}
	public void setAppointmentCreatedID(int appointmentCreatedID) {
		this.appointmentCreatedID = appointmentCreatedID;
	}
	public int getCourseid() {
		return courseid;
	}
	public void setCourseid(int courseid) {
		this.courseid = courseid;
	}
	public String getAppointmentType() {
		return appointmentType;
	}
	public void setAppointmentType(String appointmentType) {
		this.appointmentType = appointmentType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getAppointmentDateTime() {
		return appointmentDateTime;
	}
	public void setAppointmentDateTime(String appointmentDateTime) {
		this.appointmentDateTime = appointmentDateTime;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public String getSessionStatus() {
		return sessionStatus;
	}
	public void setSessionStatus(String sessionStatus) {
		this.sessionStatus = sessionStatus;
	}
	public String getMeetingURLLink() {
		return meetingURLLink;
	}
	public void setMeetingURLLink(String meetingURLLink) {
		this.meetingURLLink = meetingURLLink;
	}
	public String getMeetingName() {
		return meetingName;
	}
	public void setMeetingName(String meetingName) {
		this.meetingName = meetingName;
	}
	 
}
