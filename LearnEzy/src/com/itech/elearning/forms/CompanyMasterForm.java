package com.itech.elearning.forms;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class CompanyMasterForm extends ActionForm
{
   public int companyID;
   public String companyName;
   private String complogo;
   private String compsubdomain;
   private FormFile imageFile;
	private int countryid;
	private String countryname;

	  private int pcompid;
	  private String pcompanyname;
	  

private int stateid;
private int cityid;


	  
   
   public int getStateid() {
	return stateid;
}
public void setStateid(int stateid) {
	this.stateid = stateid;
}
public int getCityid() {
	return cityid;
}
public void setCityid(int cityid) {
	this.cityid = cityid;
}
public int getPcompid() {
		return pcompid;
	}
	public void setPcompid(int pcompid) {
		this.pcompid = pcompid;
	}
	public String getPcompanyname() {
		return pcompanyname;
	}
	public void setPcompanyname(String pcompanyname) {
		this.pcompanyname = pcompanyname;
	}
public int getCountryid() {
		return countryid;
	}
	public void setCountryid(int countryid) {
		this.countryid = countryid;
	}
	public String getCountryname() {
		return countryname;
	}
	public void setCountryname(String countryname) {
		this.countryname = countryname;
	}
public FormFile getImageFile() {
	return imageFile;
}
public void setImageFile(FormFile imageFile) {
	this.imageFile = imageFile;
}
public String getComplogo() {
	return complogo;
}
public void setComplogo(String complogo) {
	this.complogo = complogo;
}
public String getCompsubdomain() {
	return compsubdomain;
}
public void setCompsubdomain(String compsubdomain) {
	this.compsubdomain = compsubdomain;
}
public boolean active;
public int getCompanyID() {
	return companyID;
}
public void setCompanyID(int companyID) {
	this.companyID = companyID;
}
public String getCompanyName() {
	return companyName;
}
public void setCompanyName(String companyName) {
	this.companyName = companyName;
}
public boolean isActive() {
	return active;
}
public void setActive(boolean active) {
	this.active = active;
}
}
