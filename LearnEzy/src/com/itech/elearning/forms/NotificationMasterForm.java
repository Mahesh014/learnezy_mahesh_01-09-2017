package com.itech.elearning.forms;

import org.apache.struts.action.ActionForm;


public class NotificationMasterForm extends ActionForm {
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long notificationID;
	private Long courseId;
	private String courseName;
	private String notification;
	private String createdDate;
	private String expiryDate;
	private String firstName;
	private String jEdit;
	private Long userid;
	private boolean active;
	public Long getNotificationID() {
		return notificationID;
	}
	public void setNotificationID(Long notificationID) {
		this.notificationID = notificationID;
	}
	public Long getCourseId() {
		return courseId;
	}
	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}
	public String getNotification() {
		return notification;
	}
	public void setNotification(String notification) {
		this.notification = notification;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getCourseName() {
		return courseName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public Long getUserid() {
		return userid;
	}
	public void setUserid(Long userid) {
		this.userid = userid;
	}
	public String getjEdit() {
		return jEdit;
	}
	public void setjEdit(String jEdit) {
		this.jEdit = jEdit;
	}

	
 
	 
}
