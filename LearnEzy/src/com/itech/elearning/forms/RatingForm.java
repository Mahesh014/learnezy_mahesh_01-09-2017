package com.itech.elearning.forms;

import org.apache.struts.action.ActionForm;

public class RatingForm extends ActionForm
{
  private int ratingid;
  private String ratingname;
  private boolean active;
  
  private int courseid;
  private String coursename,emailid;
  
  
  
public int getCourseid() {
	return courseid;
}
public void setCourseid(int courseid) {
	this.courseid = courseid;
}
public String getCoursename() {
	return coursename;
}
public void setCoursename(String coursename) {
	this.coursename = coursename;
}
public int getRatingid() {
	return ratingid;
}
public void setRatingid(int ratingid) {
	this.ratingid = ratingid;
}
public String getRatingname() {
	return ratingname;
}
public void setRatingname(String ratingname) {
	this.ratingname = ratingname;
}
public boolean isActive() {
	return active;
}
public void setActive(boolean active) {
	this.active = active;
}
public void setEmailid(String emailid) {
	this.emailid = emailid;
}
public String getEmailid() {
	return emailid;
}
  
  
}
