package com.itech.elearning.forms;

import org.apache.struts.action.ActionForm;

public class GetCertificateForm extends ActionForm{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String studentName;
	private Long courseId;
	private String courseName;
	private String passpercent;
	private String passGrade;
	private String courseComplDate;
	public Long getCourseId() {
		return courseId;
	}
	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getPasspercent() {
		return passpercent;
	}
	public void setPasspercent(String passpercent) {
		this.passpercent = passpercent;
	}
	public String getPassGrade() {
		return passGrade;
	}
	public void setPassGrade(String passGrade) {
		this.passGrade = passGrade;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public String getCourseComplDate() {
		return courseComplDate;
	}
	public void setCourseComplDate(String courseComplDate) {
		this.courseComplDate = courseComplDate;
	}
}
