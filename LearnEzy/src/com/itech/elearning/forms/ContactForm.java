package com.itech.elearning.forms;

import org.apache.struts.action.ActionForm;

public class ContactForm extends ActionForm{
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private int contactId;
private String name;
private String emailId;
private String mobileNo;
private String message;
public int getContactId() {
	return contactId;
}
public void setContactId(int contactId) {
	this.contactId = contactId;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getEmailId() {
	return emailId;
}
public void setEmailId(String emailId) {
	this.emailId = emailId;
}
public String getMobileNo() {
	return mobileNo;
}
public void setMobileNo(String mobileNo) {
	this.mobileNo = mobileNo;
}
public String getMessage() {
	return message;
}
public void setMessage(String message) {
	this.message = message;
}

}
