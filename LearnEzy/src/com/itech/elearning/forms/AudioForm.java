package com.itech.elearning.forms;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class AudioForm extends ActionForm{
   /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int audioId;
	private int courseId;
	private String fileName;
	private String courseName;
	private FormFile audioFile;
	private boolean active;
	private String editJson;
	public int getAudioId() {
		return audioId;
	}
	public void setAudioId(int audioId) {
		this.audioId = audioId;
	}
	public int getCourseId() {
		return courseId;
	}
	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public FormFile getAudioFile() {
		return audioFile;
	}
	public void setAudioFile(FormFile audioFile) {
		this.audioFile = audioFile;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public String getEditJson() {
		return editJson;
	}
	public void setEditJson(String editJson) {
		this.editJson = editJson;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	 
}
