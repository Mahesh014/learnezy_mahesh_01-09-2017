package com.itech.elearning.forms;

import org.apache.struts.validator.ValidatorForm;

@SuppressWarnings("serial")
public class QuestionMasterForm extends ValidatorForm {
	private Long courseId;
	private Long topicId;
	private Long testId;
	private Long questionId;
	private String courseName;
	private String topicName;
	private String testName;
	private boolean active;
	private Long testType;
	private String question;
	private String descriptiveAns;
	private String ansA;
	private String ansB;
	private String ansC;
	private String ansD;
	private String anscrctAns;
	private String crctAns;
	private String testTypeName;
	private String jEdit;

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public Long getTopicId() {
		return topicId;
	}

	public void setTopicId(Long topicId) {
		this.topicId = topicId;
	}

	public Long getTestId() {
		return testId;
	}

	public void setTestId(Long testId) {
		this.testId = testId;
	}

	public Long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public String getTestName() {
		return testName;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Long getTestType() {
		return testType;
	}

	public void setTestType(Long testType) {
		this.testType = testType;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getDescriptiveAns() {
		return descriptiveAns;
	}

	public void setDescriptiveAns(String descriptiveAns) {
		this.descriptiveAns = descriptiveAns;
	}

	public String getAnsA() {
		return ansA;
	}

	public void setAnsA(String ansA) {
		this.ansA = ansA;
	}

	public String getAnsB() {
		return ansB;
	}

	public void setAnsB(String ansB) {
		this.ansB = ansB;
	}

	public String getAnsC() {
		return ansC;
	}

	public void setAnsC(String ansC) {
		this.ansC = ansC;
	}

	public String getAnsD() {
		return ansD;
	}

	public void setAnsD(String ansD) {
		this.ansD = ansD;
	}

	public String getAnscrctAns() {
		return anscrctAns;
	}

	public void setAnscrctAns(String anscrctAns) {
		this.anscrctAns = anscrctAns;
	}

	public String getCrctAns() {
		return crctAns;
	}

	public void setCrctAns(String crctAns) {
		this.crctAns = crctAns;
	}

	public String getTestTypeName() {
		return testTypeName;
	}

	public void setTestTypeName(String testTypeName) {
		this.testTypeName = testTypeName;
	}

	 

	public String getjEdit() {
		return jEdit;
	}

	public void setjEdit(String jEdit) {
		this.jEdit = jEdit;
	}

}
