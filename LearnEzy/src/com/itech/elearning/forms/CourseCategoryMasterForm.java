package com.itech.elearning.forms;

import org.apache.struts.action.ActionForm;

public class CourseCategoryMasterForm extends  ActionForm
{
  private int coursecategoryId;
  private String coursecategoryname;
  private boolean active;
public int getCoursecategoryId() {
	return coursecategoryId;
}
public void setCoursecategoryId(int coursecategoryId) {
	this.coursecategoryId = coursecategoryId;
}
public String getCoursecategoryname() {
	return coursecategoryname;
}
public void setCoursecategoryname(String coursecategoryname) {
	this.coursecategoryname = coursecategoryname;
}
public boolean isActive() {
	return active;
}
public void setActive(boolean active) {
	this.active = active;
}
  
  
}
