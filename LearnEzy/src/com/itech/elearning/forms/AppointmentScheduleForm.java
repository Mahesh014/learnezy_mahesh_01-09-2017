package com.itech.elearning.forms;

import org.apache.struts.action.ActionForm;

 

public class AppointmentScheduleForm extends ActionForm{

	private static final long serialVersionUID = 1L;
	private int appointmentScheduleId;
	private int appointmentId;
	private int appointmentCreatedID;
	private String studentId; 
	private int courseid;
	private String appointmentType;
	private String status;
	private String firstName;
	private String courseName;
	private String appointmentDateTime;
	private boolean active;
	
	public int getAppointmentScheduleId() {
		return appointmentScheduleId;
	}
	public void setAppointmentScheduleId(int appointmentScheduleId) {
		this.appointmentScheduleId = appointmentScheduleId;
	}
	public int getAppointmentId() {
		return appointmentId;
	}
	public void setAppointmentId(int appointmentId) {
		this.appointmentId = appointmentId;
	}
	public int getAppointmentCreatedID() {
		return appointmentCreatedID;
	}
	public void setAppointmentCreatedID(int appointmentCreatedID) {
		this.appointmentCreatedID = appointmentCreatedID;
	}
	public String getStudentId() {
		return studentId;
	}
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
	public String getAppointmentType() {
		return appointmentType;
	}
	public void setAppointmentType(String appointmentType) {
		this.appointmentType = appointmentType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	 
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public int getCourseid() {
		return courseid;
	}
	public void setCourseid(int courseid) {
		this.courseid = courseid;
	}
	public String getAppointmentDateTime() {
		return appointmentDateTime;
	}
	public void setAppointmentDateTime(String appointmentDateTime) {
		this.appointmentDateTime = appointmentDateTime;
	}
	
		
}
