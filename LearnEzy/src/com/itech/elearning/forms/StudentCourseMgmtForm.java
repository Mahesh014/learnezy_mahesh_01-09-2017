package com.itech.elearning.forms;

import org.apache.struts.validator.ValidatorForm;

@SuppressWarnings("serial")
public class StudentCourseMgmtForm extends ValidatorForm {

	private Long stdId;
	private String stdName;
	private Long courseId;
	private String courseName;
	private int feepaid;
	private String paidDate;
	private String dueDate;
	private boolean active;
	private int remaining;
	private int totalFee;
	private int totfeePaid;

	public Long getStdId() {
		return stdId;
	}

	public void setStdId(Long stdId) {
		this.stdId = stdId;
	}

	public String getStdName() {
		return stdName;
	}

	public void setStdName(String stdName) {
		this.stdName = stdName;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public int getFeepaid() {
		return feepaid;
	}

	public void setFeepaid(int feepaid) {
		this.feepaid = feepaid;
	}

	public String getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(String paidDate) {
		this.paidDate = paidDate;
	}

	public int getRemaining() {
		return remaining;
	}

	public void setRemaining(int remaining) {
		this.remaining = remaining;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public int getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(int totalFee) {
		this.totalFee = totalFee;
	}

	public int getTotfeePaid() {
		return totfeePaid;
	}

	public void setTotfeePaid(int totfeePaid) {
		this.totfeePaid = totfeePaid;
	}

}
