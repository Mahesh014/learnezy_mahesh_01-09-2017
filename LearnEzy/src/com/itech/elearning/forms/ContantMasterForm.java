package com.itech.elearning.forms;

import org.apache.struts.upload.FormFile;
import org.apache.struts.validator.ValidatorForm;

public class ContantMasterForm  extends ValidatorForm {
	
	private static final long serialVersionUID = 1L;
	
	private long topicId;
	private String topicName;
	
	private long courseId;
	private String courseName;
	private String videofilename;
	
	private long subTopicId;
	private String subTopicName;
	
	private long contentId;
	private String contentText;
	
	private String youTubeLink;
	private FormFile imageFile;
	private FormFile videoFile;
	private FormFile excelFile;
	private boolean	active;
	
	public long getTopicId() {
		return topicId;
	}
	public void setTopicId(long topicId) {
		this.topicId = topicId;
	}
	public String getTopicName() {
		return topicName;
	}
	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}
	public long getCourseId() {
		return courseId;
	}
	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public long getSubTopicId() {
		return subTopicId;
	}
	public void setSubTopicId(long subTopicId) {
		this.subTopicId = subTopicId;
	}
	public String getSubTopicName() {
		return subTopicName;
	}
	public void setSubTopicName(String subTopicName) {
		this.subTopicName = subTopicName;
	}
	public long getContentId() {
		return contentId;
	}
	public void setContentId(long contentId) {
		this.contentId = contentId;
	}
	 
	public String getYouTubeLink() {
		return youTubeLink;
	}
	public void setYouTubeLink(String youTubeLink) {
		this.youTubeLink = youTubeLink;
	}
	public FormFile getImageFile() {
		return imageFile;
	}
	public void setImageFile(FormFile imageFile) {
		this.imageFile = imageFile;
	}
	public FormFile getVideoFile() {
		return videoFile;
	}
	public void setVideoFile(FormFile videoFile) {
		this.videoFile = videoFile;
	}
	 
	public void setContentText(String contentText) {
		this.contentText = contentText;
	}
	public String getContentText() {
		return contentText;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public boolean isActive() {
		return active;
	}
	public void setExcelFile(FormFile excelFile) {
		this.excelFile = excelFile;
	}
	public FormFile getExcelFile() {
		return excelFile;
	}
	public void setVideofilename(String videofilename) {
		this.videofilename = videofilename;
	}
	public String getVideofilename() {
		return videofilename;
	}
	 
	

}
