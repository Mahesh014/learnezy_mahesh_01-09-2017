package com.itech.elearning.forms;

import org.apache.struts.action.ActionForm;


public class LinkDetailsMasterForm extends ActionForm {
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long linkID;
	private String URL;
	private String linkName;
	private boolean active;
	public Long getLinkID() {
		return linkID;
	}
	public void setLinkID(Long linkID) {
		this.linkID = linkID;
	}
	public String getURL() {
		return URL;
	}
	public void setURL(String uRL) {
		URL = uRL;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public String getLinkName() {
		return linkName;
	}
	public void setLinkName(String linkName) {
		this.linkName = linkName;
	}
	
	 
}
