package com.itech.elearning.forms;

import org.apache.struts.validator.ValidatorForm;

public class ForgotPasswordForm extends ValidatorForm {

	private static final long serialVersionUID = 1L;
	private String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
