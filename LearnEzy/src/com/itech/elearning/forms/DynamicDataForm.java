package com.itech.elearning.forms;

import org.apache.struts.action.ActionForm;

public class DynamicDataForm extends ActionForm
{
	private int coursesid;
	private int elearning;
	private int classroom;
	private int companyid;
	private double elearningcount;
	private int catid;
	private int areaids;
	private String areaname;
	private int cityids;
   private String comppid;
   private String coursestartdate;
   private String seatsavailable;
   public String getSeatsavailable() {
	return seatsavailable;
}
public void setSeatsavailable(String seatsavailable) {
	this.seatsavailable = seatsavailable;
}

private String compname;
   private String logo;
   private String testingstring;
   private String address;
   private String countryid;
   private String areaid;
   private String courseName;
   private String cityid;
   private String cityname;
   private String instituteName;
   private String instituteLogo;
   private String coursesoffered;
   private double fees; 
   private int count;
   private String username;
   private String instructorname;
   private String insimage;
   private String courseimage;
   
   private String name;
   private String password;
   private String emailid;
   public String getUsername() {
	return username;
}
public void setUsername(String username) {
	this.username = username;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getEmailid() {
	return emailid;
}
public void setEmailid(String emailid) {
	this.emailid = emailid;
}

/**
 * 
 */
private String duration;   
   public String getDuration() {
	return duration;
}
public void setDuration(String duration) {
	this.duration = duration;
}

private String courseId;
   private String coursenamelatest;
   private double amount;
   private String coursecategory;
   private String courselatestlogo;
   private String topicname;
   private String subtopicname;
  private String contentimage;
  private String description;
  private String yutube;
   public String getDescription() {
	return description;
}
public String getYutube() {
	return yutube;
}
public void setYutube(String yutube) {
	this.yutube = yutube;
}
public void setDescription(String description) {
	this.description = description;
}
public String getContentimage() {
	return contentimage;
}
public void setContentimage(String contentimage) {
	this.contentimage = contentimage;
}
public String getTopicname() {
	return topicname;
}
public void setTopicname(String topicname) {
	this.topicname = topicname;
}
public String getSubtopicname() {
	return subtopicname;
}
public void setSubtopicname(String subtopicname) {
	this.subtopicname = subtopicname;
}

private String subscribeemail;
   
   public String getSubscribeemail() {
	return subscribeemail;
}
   
   //search latest page 
   
   private String courseNamelatests;
   private String locality;
   private String city;
   
   private String level;
   
   
   
   
   
   
   
   
   
   
   
   

public String getLevel() {
	return level;
}
public void setLevel(String level) {
	this.level = level;
}
public String getCourseNamelatests() {
	return courseNamelatests;
}
public void setCourseNamelatests(String courseNamelatests) {
	this.courseNamelatests = courseNamelatests;
}
public String getLocality() {
	return locality;
}
public void setLocality(String locality) {
	this.locality = locality;
}
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
}
public void setSubscribeemail(String subscribeemail) {
	this.subscribeemail = subscribeemail;
}
private int coursecatid;
   private String coursecatname;
   
   
   private int courseidnew;
   private String coursenamenew;
   private double amountnew;
   
   
   private int coursecatcount;
   private int coursecount;
   private int academycount;
   private int studentcount;
   
   private String imagename;
   private String rating;
   
   
   private String studentname;
   private String studentphoto;
   private String studentreview;
   private String studentcourse;
   
   
   private String popularcourse;
   private String popularcompname;
   private String popularrating;
   private String popularlogo;
   
   
   
   
   
   
   
   
   
public String getPopularcourse() {
	return popularcourse;
}
public void setPopularcourse(String popularcourse) {
	this.popularcourse = popularcourse;
}
public String getPopularcompname() {
	return popularcompname;
}
public void setPopularcompname(String popularcompname) {
	this.popularcompname = popularcompname;
}
public String getPopularrating() {
	return popularrating;
}
public void setPopularrating(String popularrating) {
	this.popularrating = popularrating;
}
public String getPopularlogo() {
	return popularlogo;
}
public void setPopularlogo(String popularlogo) {
	this.popularlogo = popularlogo;
}
public String getStudentname() {
	return studentname;
}
public void setStudentname(String studentname) {
	this.studentname = studentname;
}
public String getStudentphoto() {
	return studentphoto;
}
public void setStudentphoto(String studentphoto) {
	this.studentphoto = studentphoto;
}
public String getStudentreview() {
	return studentreview;
}
public void setStudentreview(String studentreview) {
	this.studentreview = studentreview;
}
public String getStudentcourse() {
	return studentcourse;
}
public void setStudentcourse(String studentcourse) {
	this.studentcourse = studentcourse;
}
public String getRating() {
	return rating;
}
public void setRating(String rating) {
	this.rating = rating;
}
public String getImagename() {
	return imagename;
}
public void setImagename(String imagename) {
	this.imagename = imagename;
}
public int getCoursecatcount() {
	return coursecatcount;
}
public void setCoursecatcount(int coursecatcount) {
	this.coursecatcount = coursecatcount;
}
public int getCoursecount() {
	return coursecount;
}
public void setCoursecount(int coursecount) {
	this.coursecount = coursecount;
}
public int getAcademycount() {
	return academycount;
}
public void setAcademycount(int academycount) {
	this.academycount = academycount;
}
public int getStudentcount() {
	return studentcount;
}
public void setStudentcount(int studentcount) {
	this.studentcount = studentcount;
}
public int getCourseidnew() {
	return courseidnew;
}
public void setCourseidnew(int courseidnew) {
	this.courseidnew = courseidnew;
}
public String getCoursenamenew() {
	return coursenamenew;
}
public void setCoursenamenew(String coursenamenew) {
	this.coursenamenew = coursenamenew;
}
public double getAmountnew() {
	return amountnew;
}
public void setAmountnew(double amountnew) {
	this.amountnew = amountnew;
}
public int getCoursecatid() {
	return coursecatid;
}
public void setCoursecatid(int coursecatid) {
	this.coursecatid = coursecatid;
}
public String getCoursecatname() {
	return coursecatname;
}
public void setCoursecatname(String coursecatname) {
	this.coursecatname = coursecatname;
}
public String getCourseId() {
	return courseId;
}
public void setCourseId(String courseId) {
	this.courseId = courseId;
}
public String getCoursenamelatest() {
	return coursenamelatest;
}
public void setCoursenamelatest(String coursenamelatest) {
	this.coursenamelatest = coursenamelatest;
}
public double getAmount() {
	return amount;
}
public void setAmount(double amount) {
	this.amount = amount;
}
public String getCoursecategory() {
	return coursecategory;
}
public void setCoursecategory(String coursecategory) {
	this.coursecategory = coursecategory;
}
public String getCourselatestlogo() {
	return courselatestlogo;
}
public void setCourselatestlogo(String courselatestlogo) {
	this.courselatestlogo = courselatestlogo;
}
public double getFees() {
	return fees;
}
public void setFees(double fees) {
	this.fees = fees;
}
public String getInstituteName() {
	return instituteName;
}
public void setInstituteName(String instituteName) {
	this.instituteName = instituteName;
}
public String getInstituteLogo() {
	return instituteLogo;
}
public void setInstituteLogo(String instituteLogo) {
	this.instituteLogo = instituteLogo;
}
public String getCoursesoffered() {
	return coursesoffered;
}
public void setCoursesoffered(String coursesoffered) {
	this.coursesoffered = coursesoffered;
}
public String getCityid() {
	return cityid;
}
public void setCityid(String cityid) {
	this.cityid = cityid;
}
public String getCityname() {
	return cityname;
}
public void setCityname(String cityname) {
	this.cityname = cityname;
}
public String getCourseName() {
	return courseName;
}
public void setCourseName(String courseName) {
	this.courseName = courseName;
}
public String getCoursename() {
	return coursename;
}
public void setCoursename(String coursename) {
	this.coursename = coursename;
}
private String stateid;
   
   private String coursename;
   
   
   
public String getComppid() {
	return comppid;
}
public void setComppid(String comppid) {
	this.comppid = comppid;
}
public String getCompname() {
	return compname;
}
public void setCompname(String compname) {
	this.compname = compname;
}
public String getLogo() {
	return logo;
}
public void setLogo(String logo) {
	this.logo = logo;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public String getCountryid() {
	return countryid;
}
public void setCountryid(String countryid) {
	this.countryid = countryid;
}
public String getAreaid() {
	return areaid;
}
public void setAreaid(String areaid) {
	this.areaid = areaid;
}
public String getStateid() {
	return stateid;
}
public void setStateid(String stateid) {
	this.stateid = stateid;
}
public void setCoursesid(int coursesid) {
	this.coursesid = coursesid;
}
public int getCoursesid() {
	return coursesid;
}
public void setCatid(int catid) {
	this.catid = catid;
}
public int getCatid() {
	return catid;
}
public void setCount(int count) {
	this.count = count;
}
public int getCount() {
	return count;
}
public void setName(String name) {
	this.name = name;
}
public String getName() {
	return name;
}
public void setCoursestartdate(String coursestartdate) {
	this.coursestartdate = coursestartdate;
}
public String getCoursestartdate() {
	return coursestartdate;
}
public void setInstructorname(String instructorname) {
	this.instructorname = instructorname;
}
public String getInstructorname() {
	return instructorname;
}

public void setInsimage(String insimage) {
	this.insimage = insimage;
}
public String getInsimage() {
	return insimage;
}
public void setElearningcount(double elearningcount) {
	this.elearningcount = elearningcount;
}
public double getElearningcount() {
	return elearningcount;
}
public void setCourseimage(String courseimage) {
	this.courseimage = courseimage;
}
public String getCourseimage() {
	return courseimage;
}
public void setCompanyid(int companyid) {
	this.companyid = companyid;
}
public int getCompanyid() {
	return companyid;
}
public void setAreaids(int areaids) {
	this.areaids = areaids;
}
public int getAreaids() {
	return areaids;
}
public void setAreaname(String areaname) {
	this.areaname = areaname;
}
public String getAreaname() {
	return areaname;
}
public void setCityids(int cityids) {
	this.cityids = cityids;
}
public int getCityids() {
	return cityids;
}
public void setTestingstring(String testingstring) {
	this.testingstring = testingstring;
}
public String getTestingstring() {
	return testingstring;
}
public void setElearning(int elearning) {
	this.elearning = elearning;
}
public int getElearning() {
	return elearning;
}
public void setClassroom(int classroom) {
	this.classroom = classroom;
}
public int getClassroom() {
	return classroom;
}
   
   
}
