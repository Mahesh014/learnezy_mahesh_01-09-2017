package com.itech.elearning.forms;

import org.apache.struts.validator.ValidatorForm;

public class Login1Form extends ValidatorForm{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	private String userName;
	private String password;
	private String emailid;
	public String getEmailid() {
		return emailid;
	}
	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}
}
