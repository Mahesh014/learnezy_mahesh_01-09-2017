package com.itech.elearning.forms;

import org.apache.struts.validator.ValidatorForm;

public class PassPercentForm extends ValidatorForm {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	private int passpercentid;
	private long courseId;
	private Double passpercent;
	private String courseName;
	private boolean active;

	
	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}

	public long getCourseId() {
		return courseId;
	}

	public void setPasspercent(Double passpercent) {
		this.passpercent = passpercent;
	}

	public Double getPasspercent() {
		return passpercent;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isActive() {
		return active;
	}

	public void setPasspercentid(int passpercentid) {
		this.passpercentid = passpercentid;
	}

	public int getPasspercentid() {
		return passpercentid;
	}




}
