package com.itech.elearning.forms;

import org.apache.struts.validator.ValidatorForm;

@SuppressWarnings("serial")
public class KBMasterForm extends ValidatorForm {
	private long courseId;
	private String courseName;
	private long kbId;
	private String question;
	private String answer;
	private String jEdit;
	private boolean active;
	public long getCourseId() {
		return courseId;
	}
	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public long getKbId() {
		return kbId;
	}
	public void setKbId(long kbId) {
		this.kbId = kbId;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	 
	public void setActive(boolean active) {
		this.active = active;
	}
	public boolean isActive() {
		return active;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public String getAnswer() {
		return answer;
	}
	public String getjEdit() {
		return jEdit;
	}
	public void setjEdit(String jEdit) {
		this.jEdit = jEdit;
	}
	
	 
	
}
