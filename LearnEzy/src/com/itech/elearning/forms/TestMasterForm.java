package com.itech.elearning.forms;

import org.apache.struts.upload.FormFile;
import org.apache.struts.validator.ValidatorForm;

@SuppressWarnings("serial")
public class TestMasterForm extends ValidatorForm {
	private Long courseId;
	private Long topicId;
	private Long testTypeId;
	private Long testId;
	private String courseName;
	private String topicName;
	private String testTypeName;
	private String tdate;
	private String tname;
	private int testTime;
	private int minScore;
	private String questionIds;
	private boolean active;
	private int achivementScore;
	private String jsonQIds;
	private String jsonEdit;
	
	private FormFile filename1;
	public String getTname() {
		return tname;
	}

	public void setTname(String tname) {
		this.tname = tname;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public Long getTopicId() {
		return topicId;
	}

	public void setTopicId(Long topicId) {
		this.topicId = topicId;
	}

	public Long getTestTypeId() {
		return testTypeId;
	}

	public void setTestTypeId(Long testTypeId) {
		this.testTypeId = testTypeId;
	}

	public Long getTestId() {
		return testId;
	}

	public void setTestId(Long testId) {
		this.testId = testId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public String getTestTypeName() {
		return testTypeName;
	}

	public void setTestTypeName(String testTypeName) {
		this.testTypeName = testTypeName;
	}

	 

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getQuestionIds() {
		return questionIds;
	}

	public void setQuestionIds(String questionIds) {
		this.questionIds = questionIds;
	}

	public int getMinScore() {
		return minScore;
	}

	public void setMinScore(int minScore) {
		this.minScore = minScore;
	}

	public int getTestTime() {
		return testTime;
	}

	public void setTestTime(int testTime) {
		this.testTime = testTime;
	}

	public String getTdate() {
		return tdate;
	}

	public void setTdate(String tdate) {
		this.tdate = tdate;
	}

	public int getAchivementScore() {
		return achivementScore;
	}

	public void setAchivementScore(int achivementScore) {
		this.achivementScore = achivementScore;
	}

	public String getJsonQIds() {
		return jsonQIds;
	}

	public void setJsonQIds(String jsonQIds) {
		this.jsonQIds = jsonQIds;
	}

	public String getJsonEdit() {
		return jsonEdit;
	}

	public void setJsonEdit(String jsonEdit) {
		this.jsonEdit = jsonEdit;
	}

	public void setFilename1(FormFile filename1) {
		this.filename1 = filename1;
	}

	public FormFile getFilename1() {
		return filename1;
	}

}
