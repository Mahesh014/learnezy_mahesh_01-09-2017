package com.itech.elearning.forms;

import org.apache.struts.action.ActionForm;

public class CourseOfferedForm extends ActionForm
{
   private int courseofferedid;
   private String courseofferedname;
   private boolean active;
   private int coursecategoryId;
   private String coursecategoryname;
   
public int getCoursecategoryId() {
	return coursecategoryId;
}
public void setCoursecategoryId(int coursecategoryId) {
	this.coursecategoryId = coursecategoryId;
}
public String getCoursecategoryname() {
	return coursecategoryname;
}
public void setCoursecategoryname(String coursecategoryname) {
	this.coursecategoryname = coursecategoryname;
}
public int getCourseofferedid() {
	return courseofferedid;
}
public void setCourseofferedid(int courseofferedid) {
	this.courseofferedid = courseofferedid;
}
public String getCourseofferedname() {
	return courseofferedname;
}
public void setCourseofferedname(String courseofferedname) {
	this.courseofferedname = courseofferedname;
}
public boolean isActive() {
	return active;
}
public void setActive(boolean active) {
	this.active = active;
}
}
