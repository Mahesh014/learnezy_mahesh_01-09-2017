package com.itech.elearning.forms;

import java.util.List;

import org.apache.struts.validator.ValidatorForm;

@SuppressWarnings("serial")
public class CourseToCouchForm extends ValidatorForm 
{
	private Long couchId;
	private Long courseId;
	private Long topicId;
	private String courseName;
	private String topicName;
	private String couchName;
	private List<?> topicList;
	private List<?> courseList;
	
	public Long getCouchId() {
		return couchId;
	}
	public void setCouchId(Long couchId) {
		this.couchId = couchId;
	}
	public Long getCourseId() {
		return courseId;
	}
	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}
	public Long getTopicId() {
		return topicId;
	}
	public void setTopicId(Long topicId) {
		this.topicId = topicId;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getTopicName() {
		return topicName;
	}
	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}
	public String getCouchName() {
		return couchName;
	}
	public void setCouchName(String couchName) {
		this.couchName = couchName;
	}
	public List<?> getTopicList() {
		return topicList;
	}
	public void setTopicList(List<?> topicList) {
		this.topicList = topicList;
	}
	public List<?> getCourseList() {
		return courseList;
	}
	public void setCourseList(List<?> courseList) {
		this.courseList = courseList;
	}
	
	
	

}
