package com.itech.elearning.forms;

import org.apache.struts.action.ActionForm;

public class TestReportsForm extends ActionForm{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String testName;
	private String firstName;
	private Long userid;
	private String courseName;
	private int numTestTaken;
	private Long courseId;
	private String regDate;
	private String maxScore;
	private String passStatus;
	private double passPercentage;
	
	public String getTestName() {
		return testName;
	}
	public void setTestName(String testName) {
		this.testName = testName;
	}
	public Long getUserid() {
		return userid;
	}
	public void setUserid(Long userid) {
		this.userid = userid;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public int getNumTestTaken() {
		return numTestTaken;
	}
	public void setNumTestTaken(int numTestTaken) {
		this.numTestTaken = numTestTaken;
	}
	public Long getCourseId() {
		return courseId;
	}
	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMaxScore() {
		return maxScore;
	}
	public void setMaxScore(String maxScore) {
		this.maxScore = maxScore;
	}
	public double getPassPercentage() {
		return passPercentage;
	}
	public void setPassPercentage(double passPercentage) {
		this.passPercentage = passPercentage;
	}
	public String getPassStatus() {
		return passStatus;
	}
	public void setPassStatus(String passStatus) {
		this.passStatus = passStatus;
	}
 
	
}
