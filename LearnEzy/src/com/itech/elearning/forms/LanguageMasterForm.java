package com.itech.elearning.forms;

import org.apache.struts.action.ActionForm;

public class LanguageMasterForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int languageid;
	private String languagename;
	private boolean active;
	public int getLanguageid() {
		return languageid;
	}
	public void setLanguageid(int languageid) {
		this.languageid = languageid;
	}
	public String getLanguagename() {
		return languagename;
	}
	public void setLanguagename(String languagename) {
		this.languagename = languagename;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}

}
