package com.itech.elearning.forms;

import org.apache.struts.upload.FormFile;
import org.apache.struts.validator.ValidatorForm;

public class TestTypeForm extends ValidatorForm {

	private static final long serialVersionUID = 1L;
	private Long testTypeId;
	private String testType;
	private boolean active;
	
	private FormFile fileName;

	public Long getTestTypeId() {
		return testTypeId;
	}

	public void setTestTypeId(Long testTypeId) {
		this.testTypeId = testTypeId;
	}

	public String getTestType() {
		return testType;
	}

	public void setTestType(String testType) {
		this.testType = testType;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public void setFileName(FormFile fileName) {
		this.fileName = fileName;
	}

	public FormFile getFileName() {
		return fileName;
	}

}
