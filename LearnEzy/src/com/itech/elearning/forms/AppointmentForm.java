package com.itech.elearning.forms;

import org.apache.struts.action.ActionForm;

 

public class AppointmentForm extends ActionForm{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int appointmentId;
	private String appointmentDate;
	private String appointmentTime;
	private String status;
	private boolean active;
	public int getAppointmentId() {
		return appointmentId;
	}
	public void setAppointmentId(int appointmentId) {
		this.appointmentId = appointmentId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public String getAppointmentDate() {
		return appointmentDate;
	}
	public void setAppointmentDate(String appointmentDate) {
		this.appointmentDate = appointmentDate;
	}
	public String getAppointmentTime() {
		return appointmentTime;
	}
	public void setAppointmentTime(String appointmentTime) {
		this.appointmentTime = appointmentTime;
	}
	
}
