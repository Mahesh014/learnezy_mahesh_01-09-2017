package com.itech.elearning.handler;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.simple.JSONArray;

import com.itech.elearning.daoimpl.ReviewMasterDao;
import com.itech.elearning.forms.CityForm;
import com.itech.elearning.forms.ReviewMasterForm;

public class ReviewMasterHandler {
	ReviewMasterDao reviewDao=new ReviewMasterDao();
	public List<ReviewMasterForm> list(String compid, DataSource dataSource) {
		// TODO Auto-generated method stub
		return reviewDao.list( compid,  dataSource);
	}

	public Object add(ReviewMasterForm reviewForm, String compid, Long userid,
			DataSource dataSource) {
		// TODO Auto-generated method stub
		return reviewDao.add( reviewForm,  compid,  userid, dataSource);
	}

	public Object changeStatus(ReviewMasterForm reviewForm, DataSource dataSource) {
		// TODO Auto-generated method stub
		return reviewDao.changeStatus(  reviewForm,  dataSource);
	}

	

	

	public List<ReviewMasterForm> reviewlist(DataSource dataSource, String letter) {
		// TODO Auto-generated method stub
		return reviewDao.reviewlist(dataSource,letter);
	}

	public List<ReviewMasterForm> reviewlistviewmore(DataSource dataSource) {
		
		return reviewDao.reviewlistviewmore(dataSource);
	}

}
