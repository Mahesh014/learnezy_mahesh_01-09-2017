package com.itech.elearning.handler;

import java.util.List;

import javax.sql.DataSource;

import com.itech.elearning.daoimpl.TopicMasterDaoIml;
import com.itech.elearning.forms.TopicMasterForm;

public class TopicMasterHandler {

	TopicMasterDaoIml impl = new TopicMasterDaoIml();

	public List<TopicMasterForm> listAllCourse(int roleid,String compid, Long userid,DataSource dataSource) {

		return impl.listAllCourse(roleid,compid,userid,dataSource);
	}

	public List<TopicMasterForm> listAll(int roleid,String compid, Long userid, Long courseid, DataSource dataSource) {

		return impl.listAll(roleid,compid,userid,courseid,dataSource);
	}

	public String addTopic(String compid, TopicMasterForm topicForm, DataSource dataSource) {

		return impl.addTopic(compid,topicForm, dataSource);
	}

	public String changeStatus(TopicMasterForm topicForm,
			DataSource dataSource, long topicId) {

		return impl.changeStatus(topicForm, dataSource, topicId);
	}

	public Object updateTopic(TopicMasterForm topicForm, DataSource dataSource) {

		return impl.updateToopic(topicForm, dataSource);
	}
	
	public String topicExcelUpload(String fileName, String filePath, String compid) {
		return impl.topicExcelUpload(fileName,filePath,compid);
	}


}
