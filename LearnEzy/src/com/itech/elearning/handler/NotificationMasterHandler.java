package com.itech.elearning.handler;

import java.util.List;


import javax.sql.DataSource;

import com.itech.elearning.daoimpl.NotificationMasterDao;
import com.itech.elearning.forms.NotificationMasterForm;

public class NotificationMasterHandler {
	NotificationMasterDao impl = new NotificationMasterDao();

	public List<NotificationMasterForm> listAllCourse(String compid, Long userid, DataSource dataSource) {
		
		return impl.listAllCourse(compid,userid, dataSource);
	}

	 

	public List<NotificationMasterForm> list(String compid, Long userid,Long courseId,DataSource dataSource) {
		
		return impl.list(compid,userid, dataSource,courseId);
	}

	public String add(String compid, NotificationMasterForm notiForm, DataSource dataSource) {
		
		return impl.add(compid,notiForm, dataSource);
	}
	public String changeStatus(NotificationMasterForm notiForm, DataSource dataSource, Long notificationID, boolean active) {
		
		return impl.changeStatus(notiForm, dataSource, notificationID, active);
	}

	 
	public String update(NotificationMasterForm notiForm, DataSource dataSource) {
		
		return impl.update(notiForm, dataSource);
	}

	 
}
