package com.itech.elearning.handler;

import java.util.List;

import javax.sql.DataSource;

import com.itech.elearning.daoimpl.CourseReportsDaoImpl;
import com.itech.elearning.forms.CourseReportsForm;

public class CourseReportsHandler {
	 CourseReportsDaoImpl impl= new CourseReportsDaoImpl();
	public List<CourseReportsForm> listAllCourse(int roleid,String compid, Long userid,DataSource dataSource) {
		return impl.listAllCourse(roleid,compid,userid, dataSource); 
	}
	public List<CourseReportsForm> courseDetialsList(int roleid,String compid, Long userid,	DataSource dataSource, String courseId, String fromDate, String todate) {
		return impl.courseDetialsList(roleid,compid,userid, dataSource,courseId,fromDate,todate);
	}

}
