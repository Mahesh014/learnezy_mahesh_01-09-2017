package com.itech.elearning.handler;

import java.util.List;
import javax.sql.DataSource;

import org.json.simple.JSONArray;

import com.itech.elearning.daoimpl.NotificationAddDaoImpl;
import com.itech.elearning.forms.NotificationMasterForm;

public class NotificationAddHandler {
	NotificationAddDaoImpl impl = new NotificationAddDaoImpl();
	public List<NotificationMasterForm> list(int roleid,String compid, Long userid, DataSource dataSource) {
		return impl.list(roleid,compid,userid, dataSource);
	}

	public Object add(String compid, NotificationMasterForm notiForm, DataSource dataSource, String studentId) {
		return impl.add(compid,notiForm, dataSource,studentId);
	}

	public Object changeStatus(NotificationMasterForm notiForm,	DataSource dataSource, Long notificationID, boolean active) {
		return impl.changeStatus(notiForm,dataSource,notificationID, active);
	}

	public Object update(NotificationMasterForm notiForm, DataSource dataSource) {
		return impl.update(notiForm,dataSource);
	}

	public List<NotificationMasterForm> studentList(int roleid,String compid, Long userid, DataSource dataSource) {
		return impl.studentList(roleid,compid,userid, dataSource);
	}

	public JSONArray studentListByCourse(String compid, DataSource dataSource, String courseId) {
		return impl.studentListByCourse(compid,courseId, dataSource);
	}

}
