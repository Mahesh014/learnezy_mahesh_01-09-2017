package com.itech.elearning.handler;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.simple.JSONArray;

import com.itech.elearning.daoimpl.RoleDaoImpl;
import com.itech.elearning.forms.RoleForm;

public class RoleHandler {

	RoleDaoImpl dao = new RoleDaoImpl();

	public List<RoleForm> list(int roleid,String compid, DataSource dataSource) {
		return dao.listAll(roleid,compid,dataSource);
	}

	public String addRole(RoleForm roleForm, String compid, DataSource dataSource) {
		return dao.add(roleForm,compid,dataSource);
	}

	public Object changeRole(RoleForm roleForm, DataSource dataSource) {
		return dao.changestatus(roleForm, dataSource);
	}

	public Object updateRole(RoleForm roleForm, DataSource dataSource) {
		return dao.update(roleForm, dataSource);
	}

	public JSONArray getlicenseid(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		return dao.getlicenseid(dataSource,request,response);
	}

	public JSONArray licenseadd(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String licensename, String space, String clientper, String licenseid) {
		// TODO Auto-generated method stub
		return dao.licenseadd(dataSource,request,response,licensename,space,clientper,licenseid);
	}

	public JSONArray licenselist(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		return dao.licenselist(dataSource,request,response);
	}

	public JSONArray chanstatus(DataSource dataSource,
			HttpServletRequest request, String licenseid, String status) {
		// TODO Auto-generated method stub
		return dao.chanstatus(dataSource,request,licenseid,status);
	}

	public JSONArray licenseupdate(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String licensename, String space, String clientper, String licenseid) {
		// TODO Auto-generated method stub
		return dao.licenseupdate(dataSource,request,response,licensename,space,clientper,licenseid);
	}

	

}
