package com.itech.elearning.handler;

import java.util.List;

import javax.sql.DataSource;

import org.json.simple.JSONArray;

import com.itech.elearning.daoimpl.AppointmentDao;
import com.itech.elearning.forms.AppointmentForm;

public class AppointmentHandler {
	AppointmentDao impl= new AppointmentDao();
	
	public String add(AppointmentForm apptForm, DataSource dataSource, String compid) {
		return impl.add(apptForm, dataSource,compid);
	}

	public String changeStatus(AppointmentForm apptForm, DataSource dataSource) {
		return impl.changeStatus(apptForm, dataSource);
	}

	public String updateCountry(AppointmentForm apptForm, DataSource dataSource, Long userid) {
	 
		return impl.update(apptForm, dataSource);
	}

	public List<AppointmentForm> activeList(int roleid,String compid,DataSource dataSource) {
		return impl.activeList(roleid,compid,dataSource);
	}

	public JSONArray ajaxActiveList(DataSource dataSource, String appointmentId) {
		return impl.ajaxActiveList(dataSource, appointmentId);
	}

	public List<AppointmentForm> list(int roleid,DataSource dataSource, String compid) {
		return impl.list(roleid,dataSource,compid);
	}
}
