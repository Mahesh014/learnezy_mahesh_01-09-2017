package com.itech.elearning.handler;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.simple.JSONArray;

import com.itech.elearning.daoimpl.CourseMasterDaoImpl;
import com.itech.elearning.forms.CourseMasterForm;
import com.itech.elearning.forms.CourseOfferedForm;

public class CourseMasterHandler {
	CourseMasterDaoImpl impl = new CourseMasterDaoImpl();

	public List<CourseMasterForm> listAll(int roleid,String compid, Long userid, DataSource dataSource) {
		return impl.listAll(roleid,compid,userid, dataSource);
	}
	
	public List<CourseMasterForm> courseListByCourseIds(String courseList, DataSource dataSource) {
		return impl.courseListByCourseIds(courseList, dataSource);
	}

	public String addCourse(CourseMasterForm courseForm, String compid, String imagefileName, String imagefileName1, DataSource dataSource) {
		return impl.addCourse(courseForm,compid ,imagefileName,imagefileName1,dataSource);
	}

	public String changeStatus(String compid, CourseMasterForm courseForm,	DataSource dataSource, long userId) {
		return impl.changeSatatus(compid,courseForm, dataSource, userId);
	}

	public CourseMasterForm get(long userId, DataSource dataSource) {
		return impl.get(userId, dataSource);
	}

	public Object updateCourse(CourseMasterForm courseForm,DataSource dataSource, String imagefileName, String imagefileName1) {
		return impl.updateCourse(courseForm, dataSource,imagefileName,imagefileName1);
	}

	public String getCourseNameByID(Long courseId,	DataSource dataSource) {
		return impl.getCourseNameByID(courseId, dataSource);
	}

	public JSONArray courseFees(DataSource dataSource, String courseId) {
		return impl.courseFees(courseId, dataSource);
	}

	

	public List<CourseMasterForm> listAllcategory(int roleid, String compid,
			Long userid, DataSource dataSource) {
		return impl.listAllcategory(roleid,compid,userid, dataSource);
	}

	public List<CourseMasterForm> listCourseOffered(
			CourseMasterForm courseForm, int roleid, String compid,
			Long userid, DataSource dataSource)
			{
	
		return impl.listCourseOffered(courseForm, roleid, compid, userid, dataSource);
	}

	public JSONArray getcourse(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String catid) {
		// TODO Auto-generated method stub
		return impl.getcourse(dataSource,request,response,catid);
	}

	public String excelUpload(DataSource dataSource, List<CourseMasterForm> ld,
			String filename, int n, int rowNo, int countofvalues,
			int totalcount, String compid) {
		// TODO Auto-generated method stub
		return impl.excelUpload(dataSource,ld,filename,n,rowNo,countofvalues,totalcount,compid);
	}

}
