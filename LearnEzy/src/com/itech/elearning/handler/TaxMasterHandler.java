package com.itech.elearning.handler;

import java.util.HashMap;

import java.util.List;

import javax.sql.DataSource;

import com.itech.elearning.daoimpl.TaxMasterDao;
import com.itech.elearning.forms.TaxMasterForm;
public class TaxMasterHandler {

	TaxMasterDao impl = new TaxMasterDao();

	public List<TaxMasterForm> list(int roleid,String compid, DataSource dataSource) {
		return impl.list(roleid,compid,dataSource);
	}

	public String add(String compid, TaxMasterForm taxMasterform,DataSource dataSource) {
		return impl.add(compid,taxMasterform, dataSource);
	}

	public String changestatus(DataSource dataSource, boolean active,int gid, String compid) {
		return impl.changestatus(dataSource, active, gid,compid);
	}

	public String update(String compid, TaxMasterForm taxMasterform, DataSource dataSource, int userid) {
		 
		return impl.update(compid,taxMasterform, dataSource);
	}

	public HashMap<String, String> globasetting( DataSource dataSource) {
		return impl.globasetting(dataSource);
	}
}
