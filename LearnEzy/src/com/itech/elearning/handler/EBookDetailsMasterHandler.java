package com.itech.elearning.handler;

import java.util.List;


import javax.sql.DataSource;

import com.itech.elearning.daoimpl.EBookDetailsMasterDao;
import com.itech.elearning.forms.EBookDetailsMasterForm;

public class EBookDetailsMasterHandler {
	EBookDetailsMasterDao impl = new EBookDetailsMasterDao();

	public List<EBookDetailsMasterForm> listAllCourse(int roleid,String compid, Long userid, DataSource dataSource) {
		
		return impl.listAllCourse(roleid,compid,userid, dataSource);
	}

	 

	public List<EBookDetailsMasterForm> list(int roleid,String compid, Long userid,Long courseId,DataSource dataSource) {
		
		return impl.list(roleid,compid,userid, dataSource,courseId);
	}

	public String add(String compid, EBookDetailsMasterForm ebForm, DataSource dataSource) {
		
		return impl.add(compid,ebForm, dataSource);
	}
	public String changeStatus(EBookDetailsMasterForm ebForm, DataSource dataSource, Long eBookID, boolean active) {
		
		return impl.changeStatus(ebForm, dataSource, eBookID, active);
	}

	 
	public String update(EBookDetailsMasterForm ebForm, DataSource dataSource) {
		
		return impl.update(ebForm, dataSource);
	}

	 
}
