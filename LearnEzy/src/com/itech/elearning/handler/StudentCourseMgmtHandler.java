package com.itech.elearning.handler;

import java.util.List;


import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import com.itech.elearning.daoimpl.StudentCourseMgmtDaoImpl;
import com.itech.elearning.forms.StudentCourseMgmtForm;

public class StudentCourseMgmtHandler {

	StudentCourseMgmtDaoImpl impl = new StudentCourseMgmtDaoImpl();

	public List<StudentCourseMgmtForm> listAll(Long userid,
			DataSource dataSource) {
		return impl.listAll(userid, dataSource);
	}

	public Object addDetails(StudentCourseMgmtForm courseForm,
			DataSource dataSource, long stdid) {
		// TODO Auto-generated method stub
		return impl.addDetails(courseForm, dataSource, stdid);
	}

	public void getStdInfo(DataSource dataSource, long parseLong,HttpServletRequest request) {
		impl.getStdInfo(dataSource, parseLong, request);

	}

	public List<StudentCourseMgmtForm> listStdList(Long userid,
			DataSource dataSource, StudentCourseMgmtForm stdForm) {

		return impl.listStdList(userid, dataSource, stdForm);
	}

}
