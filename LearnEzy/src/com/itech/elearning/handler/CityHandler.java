package com.itech.elearning.handler;

import java.util.List;

import javax.sql.DataSource;

import org.json.simple.JSONArray;

import com.itech.elearning.daoimpl.CityDaoImpl;
import com.itech.elearning.forms.CityForm;


public class CityHandler {

	CityDaoImpl impl = new CityDaoImpl();

	public List<CityForm> list(int roleid,String compid,long stateid, long countryid,DataSource dataSource) {
		return impl.listAll(roleid,compid,stateid, countryid, dataSource);
	}

	public String add(CityForm cityForm, String compid, DataSource dataSource) {
		return impl.add(cityForm,compid, dataSource);
	}

	public String changeStatus(CityForm cityForm, DataSource dataSource) {
		return impl.changeStatus(cityForm, dataSource);
	}

	public String update(CityForm cityForm, DataSource dataSource, Long userid) {
		//**************Start Audit Trial Inserting Code********************
		//impl.auditTrail(dataSource, cityForm, userid); 
		//**************End Audit Trial Inserting Code********************
		return impl.update(cityForm, dataSource);
	}

	public List<CityForm> activeList(DataSource dataSource) {
		return impl.activeList(dataSource);
	}
	
	public List<CityForm> activeListByState(long stateid, DataSource dataSource) {
		return impl.activeListByState(stateid,dataSource);
	}

	public JSONArray ajaxActiveList(DataSource dataSource, String stateId) {
		return impl.ajaxActiveList(dataSource,stateId);
	}

	
}
