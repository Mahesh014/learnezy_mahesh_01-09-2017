package com.itech.elearning.handler;

import java.util.List;

import javax.sql.DataSource;

import com.itech.elearning.daoimpl.RatingDaoImpl;
import com.itech.elearning.forms.RatingForm;

public class RatingHandler
{

	RatingDaoImpl impl = new RatingDaoImpl();
	public List<RatingForm> list(Long userid, DataSource dataSource)
	{
		// TODO Auto-generated method stub
		return impl.list(userid,dataSource);
	}
	public String addrating(long userid,String emailid, RatingForm rateForm, DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.addrating(userid,rateForm,dataSource,emailid);
	}

}
