package com.itech.elearning.handler;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import com.itech.elearning.daoimpl.CourseToCouchDaoImpl;
import com.itech.elearning.forms.CourseToCouchForm;

public class CourseToCouchHandler {

	CourseToCouchDaoImpl impl = new CourseToCouchDaoImpl();
	public void getCouchInfo(DataSource dataSource, long parseLong,
			HttpServletRequest request) {
		 impl.getCouchInfo(dataSource, parseLong, request);
		
	}

	public List<CourseToCouchForm> listAllCourse(String compid, Long userid,
			DataSource dataSource) {
	
		return impl.listAllCourse(compid,userid, dataSource);
	}

	public List<CourseToCouchForm> listAllTopoic(Long userid,
			DataSource dataSource, Long courseId) {
		
		return impl.listAllTopoic(userid, dataSource, courseId);
	}

	public List<CourseToCouchForm> listCouchList(String compid, Long userid,
			DataSource dataSource, CourseToCouchForm couchForm, long couchId) {
		
		return impl.listCouchList(compid,userid, dataSource, couchForm, couchId);
	}

	public Object addDetails(String compid, CourseToCouchForm courseForm,
			DataSource dataSource, long parseLong) {
		// TODO Auto-generated method stub
		return impl.addDetails(compid,courseForm, dataSource, parseLong);
	}

}
