package com.itech.elearning.handler;

import java.util.List;

import javax.sql.DataSource;

import com.itech.elearning.daoimpl.LanguageMasterDao;
import com.itech.elearning.forms.LanguageMasterForm;

public class LanguageMasterHandler {
	LanguageMasterDao dao=new LanguageMasterDao(); 

	public List<LanguageMasterForm> list(int roleid, String compid,
			DataSource dataSource) {
		
		
		
		// TODO Auto-generated method stub
		return dao.list(roleid,compid,dataSource);
	}

	public Object add(LanguageMasterForm languageForm, String compid,
			DataSource dataSource) {
		// TODO Auto-generated method stub
		return dao.add(languageForm,compid,dataSource);
	}

	public Object changestatus(LanguageMasterForm languageForm,
			DataSource dataSource) {
		// TODO Auto-generated method stub
		return dao.changestatus(languageForm,dataSource);
	}

	public Object update(LanguageMasterForm languageForm, DataSource dataSource) {
		// TODO Auto-generated method stub
		return dao.update(languageForm,dataSource);
	}

}
