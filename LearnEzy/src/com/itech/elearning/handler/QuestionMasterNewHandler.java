package com.itech.elearning.handler;

import java.util.List;


import javax.sql.DataSource;

import org.json.simple.JSONArray;


import com.itech.elearning.daoimpl.QuestionMasterNewDaoImpl;

import com.itech.elearning.forms.QuestionMasterNewForm;

public class QuestionMasterNewHandler {

	QuestionMasterNewDaoImpl impl = new QuestionMasterNewDaoImpl();
	public List<QuestionMasterNewForm> listAllCourse(int roleid,long userid,
			String compid, DataSource dataSource) {
		return impl.listAllCourse(roleid,userid,compid,dataSource);
	}
	
	public List<QuestionMasterNewForm> listAllTopic(int roleid,long userid, String compid, long courseId,
			DataSource dataSource) {
		return impl.listAllTopic(roleid,userid, compid,courseId, dataSource);
	}

	public List<QuestionMasterNewForm> listAllQuestion(int roleid,String compid, long userid,
			DataSource dataSource, long courseId, long topicId) {
		return impl.listallquestions(roleid,compid,userid,dataSource,courseId,topicId);
	}
	
	public Object addQuestion(String compid, QuestionMasterNewForm testForm, DataSource dataSource) {
		return impl.addQuestion(compid,testForm, dataSource);
	}
	public String changeStatus(QuestionMasterNewForm courseForm,
			DataSource dataSource, long questid) {
		return impl.changeStatus(courseForm, dataSource, questid);
	}
	public Object updateTest(QuestionMasterNewForm testForm,
			DataSource dataSource) {
		return impl.updateTest(testForm, dataSource);
	}

	public String delete(QuestionMasterNewForm testForm,
			DataSource dataSource, long id) {
		return impl.delete(testForm,id,dataSource);
	}

	public JSONArray questionByCourseid(DataSource dataSource, String courseId) {
		return impl.questionByCourseid(dataSource,courseId);
	}

	public String testTypeExcelUpload(String fileName, String filePath, String compid) {
		
		return impl.testTypeExcelUpload(fileName,filePath,compid);
	}
	
}
