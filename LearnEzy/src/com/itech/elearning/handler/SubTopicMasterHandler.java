package com.itech.elearning.handler;

import java.util.List;

import javax.sql.DataSource;

import com.itech.elearning.daoimpl.SubTopicMasterDaoIml;
import com.itech.elearning.forms.SubTopicMasterForm;

public class SubTopicMasterHandler {

	SubTopicMasterDaoIml impl=new SubTopicMasterDaoIml();
	
	public List<SubTopicMasterForm> listAllCourse(int roleid,String compid,long userid,DataSource dataSource) {
		return impl.listAllCourse(roleid,compid,userid, dataSource); 
	}

	public List<SubTopicMasterForm> listAllTopic(int roleid,String compid,long userid, long courceid, long topicId, DataSource dataSource) {
		return impl.listAllTopic(roleid,compid,userid, dataSource,courceid,topicId);
	}

	public List<SubTopicMasterForm> listSubTopic(int roleid,String compid,long userid, long topicId,long courseid, DataSource dataSource) {
		return impl.listSubTopic(roleid,compid,userid,topicId,dataSource,courseid);
	}

	public String addsubTopic(SubTopicMasterForm subtopicForm,DataSource dataSource) {
		return impl.addSubTopic(subtopicForm,dataSource);
	}

	public String changeStatus(SubTopicMasterForm subtopicForm,DataSource dataSource, long parselong) {
		return impl.changeStatus(subtopicForm,parselong,dataSource);
	}

	public Object updateTopic(SubTopicMasterForm subtopicForm,DataSource dataSource) {
		return impl.updateSubTopic(subtopicForm,dataSource);
	}

	public String testTypeExcelUpload(String fileName, String filePath, String compid) {
		
		return impl.testTypeExcelUpload(fileName,filePath,compid);
	}

	 
}
