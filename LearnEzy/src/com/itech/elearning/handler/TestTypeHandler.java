package com.itech.elearning.handler;

import java.util.List;

import javax.sql.DataSource;

import com.itech.elearning.daoimpl.TestTypeDaoImp;
import com.itech.elearning.forms.TestTypeForm;

public class TestTypeHandler {
	TestTypeDaoImp impl = new TestTypeDaoImp();

	public List<TestTypeForm> list(int roleid,String compid, DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.list(roleid,compid,dataSource);
	}

	public Object addTestType(String compid, TestTypeForm testTypeForm, DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.addTestType(compid,testTypeForm, dataSource);
	}

	public Object changeTestType(TestTypeForm testTypeForm,
			DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.changeTestType(testTypeForm, dataSource);
	}

	public Object updateTestType( TestTypeForm testTypeForm,
			DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.updateTestType(testTypeForm, dataSource);
	}
	
	public String testTypeExcelUpload(String fileName, String filePath, String compid) {
		// TODO Auto-generated method stub
		return impl.testTypeExcelUpload(fileName,filePath,compid);
	}

}
