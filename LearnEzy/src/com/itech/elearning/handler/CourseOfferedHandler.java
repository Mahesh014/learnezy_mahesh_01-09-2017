package com.itech.elearning.handler;

import java.util.List;

import javax.sql.DataSource;

import com.itech.elearning.daoimpl.CourseOfferedDaoImpl;
import com.itech.elearning.forms.CourseCategoryMasterForm;
import com.itech.elearning.forms.CourseOfferedForm;

public class CourseOfferedHandler 
{

	CourseOfferedDaoImpl impl = new CourseOfferedDaoImpl();
	public List<CourseOfferedForm> list(int roleid, String compid,
			DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.list(roleid,compid,dataSource);
	}
	public String addRole(CourseOfferedForm roleForm, String compid,
			DataSource dataSource)
	{
		return impl.addRole(roleForm,compid,dataSource);
	}
	public String changeRole(CourseOfferedForm roleForm, DataSource dataSource) 
	{
		return impl.changeRole(roleForm,dataSource);
	}
	public String updateRole(CourseOfferedForm roleForm, DataSource dataSource) {
		return impl.update(roleForm, dataSource);
	}
	public List<CourseCategoryMasterForm> coursecatlist(int roleid,
			String compid, DataSource dataSource)
			{
		return impl.coursecatlist(roleid,compid,dataSource);
	}
   
	
}
