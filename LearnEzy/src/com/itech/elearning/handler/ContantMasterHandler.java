package com.itech.elearning.handler;

import java.util.List;

import javax.sql.DataSource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.elearning.daoimpl.ContantMasterDaoIml;
import com.itech.elearning.forms.ContantMasterForm;

public class ContantMasterHandler {

	ContantMasterDaoIml impl=new ContantMasterDaoIml();
	
	public List<ContantMasterForm> listAllCourse(int roleid,String compid, long userid,DataSource dataSource) {
		return impl.listAllCourse(roleid,compid,userid, dataSource); 
	}

	public List<ContantMasterForm> listAllTopic(int roleid,String compid,long userid, long courceid, DataSource dataSource) {
		return impl.listAllTopic(roleid,compid,userid, dataSource,courceid);
	}
	
	public List<ContantMasterForm> listSubTopic(int roleid,String compid,long userid, long topicId,long courseId, DataSource dataSource) {
		return impl.listSubTopic(roleid,compid,userid,topicId,dataSource,courseId);
	}

	public List<ContantMasterForm> listContent(int roleid,String compid,long userid,long subtopicID, long topicId, long courseid, DataSource dataSource) {
		return impl.listContent(roleid,compid,userid,subtopicID,topicId,dataSource,courseid);
	}

	public String addcontant(ContantMasterForm contentForm,DataSource dataSource, String imagefileName, String videofileName, String compid) {
		return impl.addcontant(contentForm,dataSource,imagefileName,videofileName,compid);
	}

	public String changeStatus(ContantMasterForm contentForm,DataSource dataSource, long contentId, boolean active) {
		return impl.changeStatus(contentForm,contentId,active,dataSource);
	}

	public JSONObject getEdit(DataSource dataSource, long contentID) {
		return impl.getEdit(dataSource,contentID);
	}
	
	public Object updateContent(ContantMasterForm contentForm,DataSource dataSource, String imagefileName, String videofileName) {
		return impl.updateContent(contentForm,dataSource,imagefileName,videofileName);
	}

	public JSONArray getTopic(DataSource dataSource, long courseId) {
		return impl.getTopic(dataSource,courseId);
	}

	public JSONArray getSubTopic(DataSource dataSource, long topicId) {
		return impl.getSubTopic(dataSource, topicId);
	}

	

	public String excelUpload(DataSource dataSource,
			List<ContantMasterForm> ld, String filename, int n, int rowNo,int countofvalues, int totalcount, String compid) {
		// TODO Auto-generated method stub
		return impl.excelUpload(dataSource,ld,filename,n,rowNo,countofvalues,totalcount,compid);
	}

	

}
