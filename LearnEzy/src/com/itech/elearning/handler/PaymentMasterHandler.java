package com.itech.elearning.handler;

import java.util.List;

import javax.sql.DataSource;

import org.json.simple.JSONArray;

import com.itech.elearning.daoimpl.PaymentMasterDaoImpl;
import com.itech.elearning.forms.PaymentMasterForm;
public class PaymentMasterHandler {

	PaymentMasterDaoImpl impl = new PaymentMasterDaoImpl();

	public List<PaymentMasterForm> list(int roleid,String compid, DataSource dataSource) {
		return impl.list(roleid,compid,dataSource);
	}

	public String add(String compid, PaymentMasterForm paymentForm,DataSource dataSource) {
		return impl.add(compid,paymentForm, dataSource);
	}

	public String changestatus(DataSource dataSource, boolean active,int paymentId, String compid) {
		return impl.changestatus(dataSource, active, paymentId,compid);
	}

	public String update(String compid, PaymentMasterForm paymentForm, DataSource dataSource, int userid) {
		 
		return impl.update(compid,paymentForm, dataSource);
	}

	public JSONArray courseFeesAcademy(DataSource dataSource, String courseId)
	{
		return impl.courseAcademy(courseId, dataSource);
	}

	 
}
