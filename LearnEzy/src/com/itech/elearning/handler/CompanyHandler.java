package com.itech.elearning.handler;

import java.util.List;

import javax.sql.DataSource;

import com.itech.elearning.daoimpl.CompanyDaoImpl;
import com.itech.elearning.forms.CompanyMasterForm;
import com.itech.elearning.forms.ParentCompanyForm;

public class CompanyHandler {

	CompanyDaoImpl daoiml = new CompanyDaoImpl();
	public List<CompanyMasterForm> list(DataSource dataSource) 
	{
		return daoiml.listAll(dataSource);
	}
	public String add(CompanyMasterForm companyForm, String imagefileName, DataSource dataSource) {
		// TODO Auto-generated method stub
		return daoiml.add(companyForm,imagefileName,dataSource);
	}
	public String changestatus(CompanyMasterForm companyForm,
			DataSource dataSource) {
		// TODO Auto-generated method stub
		return daoiml.changestatus(companyForm,dataSource);
	}
	public String update(CompanyMasterForm companyForm, DataSource dataSource, String imagefileName) {
		// TODO Auto-generated method stub
		return daoiml.update(companyForm,dataSource,imagefileName);
	}
	public List<CompanyMasterForm> list1(String compid, DataSource dataSource) {
		// TODO Auto-generated method stub
		return daoiml.listAll1(compid,dataSource);
	}
	public List<CompanyMasterForm> ActiveCountrylist(DataSource dataSource) {
		// TODO Auto-generated method stub
		return daoiml.ActiveCountrylist(dataSource);
	}
	public List<CompanyMasterForm> ActiveParentlist(DataSource dataSource) {
		// TODO Auto-generated method stub
		return daoiml.ActiveParentlist(dataSource);
	}
	public List<CompanyMasterForm> ActiveCountrylistadd(DataSource dataSource,
			String compid) {
		// TODO Auto-generated method stub
		return daoiml.ActiveCountrylistadd(dataSource,compid);
	}

	
}
