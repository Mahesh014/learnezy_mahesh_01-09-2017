package com.itech.elearning.handler;

import java.util.List;

import javax.sql.DataSource;

import COM.rsa.jsafe.da;

import com.itech.elearning.daoimpl.ReviewDaoImpl;
import com.itech.elearning.forms.ReviewForm;

public class ReviewHandler 
{
	ReviewDaoImpl impl = new ReviewDaoImpl();

	public List<ReviewForm> list(Long userid, DataSource dataSource)
	{
		return impl.list(userid,dataSource);
	}

	public String addrating(Long userid, ReviewForm review,
			DataSource dataSource,String emailid) {
		// TODO Auto-generated method stub
		return impl.addrating(userid,review,dataSource,emailid);
	}
  
}
