package com.itech.elearning.handler;

import java.util.List;

import javax.sql.DataSource;

import com.itech.elearning.daoimpl.LinkDetailsMasterDao;
import com.itech.elearning.forms.LinkDetailsMasterForm;


public class LinkDetailsMasterHandler {
	LinkDetailsMasterDao impl = new LinkDetailsMasterDao();

	 
	public List<LinkDetailsMasterForm> list(int roleid,String compid, Long userid,DataSource dataSource) {
		
		return impl.list(roleid,compid,userid, dataSource);
	}

	public String add(String compid, LinkDetailsMasterForm lForm, DataSource dataSource) {
		
		return impl.add(compid,lForm, dataSource);
	}
	public String changeStatus(LinkDetailsMasterForm lForm, DataSource dataSource, Long linkId, boolean active) {
		
		return impl.changeStatus(lForm, dataSource, linkId, active);
	}

	 
	public String update(LinkDetailsMasterForm lForm, DataSource dataSource) {
		
		return impl.update(lForm, dataSource);
	}

	 
}
