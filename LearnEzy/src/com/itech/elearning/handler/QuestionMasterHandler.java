package com.itech.elearning.handler;

import java.util.List;

import javax.sql.DataSource;

import org.json.simple.JSONArray;

import com.itech.elearning.daoimpl.QuestionMasterDaoImpl;
import com.itech.elearning.forms.QuestionMasterForm;

public class QuestionMasterHandler {

	QuestionMasterDaoImpl impl = new QuestionMasterDaoImpl();

	public List<QuestionMasterForm> listAllCourse(Long userid,
			DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.listAllCourse(userid, dataSource);
	}

	public List<QuestionMasterForm> listAllTopic(Long userid, Long courseId,
			DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.listAllTopic(userid, courseId, dataSource);
	}

	public List<QuestionMasterForm> listAllTest(Long userid,
			DataSource dataSource, Long topicId) {
		// TODO Auto-generated method stub
		return impl.listAllTest(userid, dataSource, topicId);
	}

	public List<QuestionMasterForm> listAllQuestion(Long userid,
			DataSource dataSource, Long testId) {
		// TODO Auto-generated method stub
		return impl.listAllQuestion(userid, dataSource, testId);
	}

	public Object addQuestion(QuestionMasterForm testForm, DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.addQuestion(testForm, dataSource);
	}

	public Object getTestType(DataSource dataSource, Long testId) {
		// TODO Auto-generated method stub
		return impl.getTestType(dataSource, testId);
	}

	public String changeStatus(QuestionMasterForm courseForm,
			DataSource dataSource, long questid) {
		// TODO Auto-generated method stub
		return impl.changeStatus(courseForm, dataSource, questid);
	}

	public Object updateTest(QuestionMasterForm testForm, DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.updateTest(testForm, dataSource);
	}

	public JSONArray questionByCourseid(DataSource dataSource, String courseId) {
		return impl.questionByCourseid(dataSource,courseId);
	}

}
