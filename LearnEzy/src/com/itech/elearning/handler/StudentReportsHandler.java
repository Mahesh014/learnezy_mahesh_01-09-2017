package com.itech.elearning.handler;

import java.util.List;

import javax.sql.DataSource;

import com.itech.elearning.daoimpl.StudentReportsDaoImpl;
import com.itech.elearning.forms.StudentReportsForm;

public class StudentReportsHandler {
	StudentReportsDaoImpl impl = new StudentReportsDaoImpl();
	public List<StudentReportsForm> studentList(int roleid,String compid, Long userid, DataSource dataSource) {
		return impl.studentList(roleid,compid,userid, dataSource);
	}
	public List<StudentReportsForm> studentDetailsList(int roleid,String compid, Long userid,DataSource dataSource, String studentId, String fromDate, String todate) {
		return impl.studentDetailsList(roleid,compid,userid, dataSource,studentId,fromDate,todate);
	}
	 
}
