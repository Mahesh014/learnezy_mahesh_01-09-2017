package com.itech.elearning.handler;

import java.util.List;

import javax.sql.DataSource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.elearning.daoimpl.StudentDao;
import com.itech.elearning.forms.StudentForm;
import com.itech.elearning.forms.StudentRegistrationForm;

public class StudentHandler {
	StudentDao impl= new StudentDao();
	public JSONArray courseList(DataSource dataSource, Long userid) {
		 return impl.courseList(dataSource,userid) ;
	}
	 
	public JSONObject contantList(DataSource dataSource, Long subTopicId,Long courseId, Long userid ) {
		 return impl.contantList(dataSource,subTopicId, courseId, userid) ;
	}

	public long lastView(DataSource dataSource, Long courseId ,Long userid) {
		 return impl.lastView(dataSource,courseId,userid ) ;
	}

	public Object addLastView(DataSource dataSource, Long courseId, Long topicId, Long subTopicId, Long userid ) {
		return null ; //impl.addLastView(dataSource,courseId,topicId,subTopicId,userid);
	}

	/*public JSONArray getAssignment(DataSource dataSource, Long userid) {
		return impl.getAssignment(dataSource,userid) ;
	}*/
	 

	public long addStudentAssignment(DataSource dataSource,Long assignmentId, Long userid) {
		return impl.addStudentAssignment(dataSource,assignmentId,userid) ;
	}

	public Object getAssignmentCount(DataSource dataSource, Long userid) {
		return impl.getAssignmentCount(dataSource,userid ) ;
	}

	public JSONArray getAssignmentByTopicId(DataSource dataSource, Long userid, Long courseId, Long topicId) {
		return impl.getAssignmentByTopicId(dataSource,userid,courseId,topicId);
	}

	public JSONArray getKnowledgeBank(DataSource dataSource, Long userid, Long courseId) {
		return impl.getKnowledgeBank(dataSource,userid,courseId);
	}

	public JSONArray getResources(DataSource dataSource, Long userid,	Long courseId) {
		return impl.getResources(dataSource,userid,courseId);
	}

	public JSONArray getAssignmentTopicId(DataSource dataSource, Long userid, Long courseId) {
		return impl.getAssignmentTopicId(dataSource,userid,courseId);
	}

	public JSONArray getLinks(DataSource dataSource, Long userid) {
		return impl.getLinks(dataSource,userid);
	}

	public JSONArray getNotification(DataSource dataSource, Long userid) {
		return impl.getNotification(dataSource,userid);
	}

	public Object getCourseCompleteCount(DataSource dataSource, Long userid) {
		return impl.getCourseCompleteCount(dataSource,userid ) ;
	}

	public Object getTestCompleteCount(DataSource dataSource, Long userid) {
		return impl.getTestCompleteCount(dataSource,userid ) ;
	}

	public Object getAchievementCompleteCount(DataSource dataSource, Long userid) {
		return impl.getAchievementCompleteCount(dataSource,userid ) ;
	}

	public JSONArray getAssignmentQuestionbySubTopicID(DataSource dataSource, Long userid, Long courseId, Long subTopicId) {
		return impl.getAssignmentQuestionbySubTopicID(dataSource,userid,courseId,subTopicId);
	}
	
	public int AcademyRegistration(StudentRegistrationForm studentForm,
			String compid, DataSource dataSource, String profilePhotoName) {
		// TODO Auto-generated method stub
		return impl.academyRegistration(studentForm,compid,dataSource,profilePhotoName);
	}
	
	public JSONArray addRating(DataSource dataSource, Long userid, String courseIdRating, String rating, int companyId) {
		// TODO Auto-generated method stub
		return impl.addRating( dataSource,  userid,courseIdRating,rating,companyId);
	}

	public JSONArray getAudioFiles(DataSource dataSource, Long userid, String audioUploadPath) {
		return impl.getAudioFiles( dataSource,  userid, audioUploadPath);
	}

	public List<StudentForm> list(DataSource dataSource, Long userid,
			int courseid) {
		// TODO Auto-generated method stub
		return impl.list( dataSource,  userid, courseid);
	}

	public Object addmynotes(DataSource dataSource, Long userid, int courseid, StudentForm studentform) {
		// TODO Auto-generated method stub
		return impl.addmynotes(dataSource,userid,courseid,studentform);
	}



}
