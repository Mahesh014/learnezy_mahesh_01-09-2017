package com.itech.elearning.handler;

import java.util.List;

import javax.sql.DataSource;

import com.itech.elearning.daoimpl.AudioDaoImpl;
import com.itech.elearning.forms.AudioForm;
public class AudioHandler {

	AudioDaoImpl impl = new AudioDaoImpl();

	public List<AudioForm> list(int roleid,String compid, DataSource dataSource) {
		return impl.list(roleid,compid,dataSource);
	}

	public String add(String compid, AudioForm paymentForm,DataSource dataSource) {
		return impl.add(compid,paymentForm, dataSource);
	}

	public String changestatus(DataSource dataSource, boolean active,int audioId, String compid) {
		return impl.changestatus(dataSource, active, audioId,compid);
	}

	public String update(String compid, AudioForm paymentForm, DataSource dataSource, int userid) {
		 
		return impl.update(compid,paymentForm, dataSource);
	}

	 
}
