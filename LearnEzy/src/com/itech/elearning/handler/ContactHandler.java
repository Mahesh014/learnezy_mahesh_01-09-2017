package com.itech.elearning.handler;

import javax.sql.DataSource;

import com.itech.elearning.daoimpl.ContactDaoImpl;
import com.itech.elearning.forms.ContactForm;

public class ContactHandler {
	ContactDaoImpl impl = new ContactDaoImpl();
	public String add(ContactForm contactForm, DataSource dataSource) {
		return impl.add(contactForm, dataSource);
	}

}
