package com.itech.elearning.handler;

import java.util.List;


import javax.sql.DataSource;


import com.itech.elearning.daoimpl.KBMasterDaoImpl;
import com.itech.elearning.forms.KBMasterForm;

public class KBMasterHandler {

	KBMasterDaoImpl impl = new KBMasterDaoImpl();
	public List<KBMasterForm> listAllCourse(int roleid,String compid, long userid,DataSource dataSource) {
		return impl.listAllCourse(roleid,compid,userid, dataSource);
	}
	
	 
	public List<KBMasterForm> list(int roleid,String compid, long userid, DataSource dataSource, long courseId) {
		return impl.list(roleid,compid,userid,dataSource,courseId);
	}
	
	public String add(String compid, KBMasterForm kbForm, DataSource dataSource) {
		return impl.add(compid,kbForm, dataSource);
	}
	public String changeStatus(DataSource dataSource, long kbId, boolean active) {
		return impl.changeStatus(dataSource, kbId,active);
	}
	

	public String update(KBMasterForm kbForm,
			DataSource dataSource) {
		return impl.updateTest(kbForm, dataSource);
	}

	
	
}
