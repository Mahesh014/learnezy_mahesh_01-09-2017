package com.itech.elearning.handler;

import java.util.List;

import javax.sql.DataSource;

import com.itech.elearning.daoimpl.CourseCategoryMasterDaoImpl;
import com.itech.elearning.forms.CourseCategoryMasterForm;

public class CourseCategoryMasterHandler {

	CourseCategoryMasterDaoImpl dao = new CourseCategoryMasterDaoImpl();
	public List<CourseCategoryMasterForm> list(int roleid, String compid,
			DataSource dataSource) {
		return dao.listAll(roleid,compid,dataSource);
	}
	public String addRole(CourseCategoryMasterForm roleForm, String compid,
			DataSource dataSource) {
		return dao.add(roleForm,compid,dataSource);
	}
	public String changeRole(CourseCategoryMasterForm roleForm,
			DataSource dataSource)
	{
		return dao.changestatus(roleForm, dataSource);
	}
	public String updateRole(CourseCategoryMasterForm roleForm,
			DataSource dataSource) {
		return dao.update(roleForm, dataSource);
	}

	
}
