package com.itech.elearning.handler;

import javax.sql.DataSource;

import com.itech.elearning.daoimpl.TransactionDao;

public class TransactionHandler {
	TransactionDao  impl= new TransactionDao();
	public int addTransaction( DataSource dataSource, String courseList, Long userId, int tax, int transactionStatus, int totalAmount, String compid) {
		return impl.addTransaction(dataSource, courseList, userId, tax, transactionStatus,totalAmount,compid);
	}
	public int updateTransactionStatus(DataSource dataSource, String transactionID, int transactionStatus, String cardNumber, String PGIID) {
		return impl.updateTransactionStatus(dataSource,transactionID, transactionStatus, cardNumber, PGIID  );
	}
}
