package com.itech.elearning.handler;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import com.itech.elearning.daoimpl.StartTestDaoImpl;
import com.itech.elearning.forms.StartTestForm;

public class StartTestHandler {

	StartTestDaoImpl impl = new StartTestDaoImpl();
	public List<StartTestForm> listAll(String testid, Long userid, String compid, StartTestForm testForm,
			DataSource dataSource) {
		System.out.println("Inside the Handler");
		return impl.listAll(testid,userid,compid ,testForm, dataSource);
	}
	public Object checkstatus(String topicID, Long courseID, Long userid, HttpServletRequest request,String[] Values,  String[] IDs, String compid, DataSource dataSource) {
		return impl.checkall(topicID,courseID,userid,request,Values,IDs,compid,dataSource);
	}                                              

}
