package com.itech.elearning.handler;

import java.util.List;

import javax.sql.DataSource;

import org.json.simple.JSONArray;

import com.itech.elearning.daoimpl.CountryDaoImpl;
import com.itech.elearning.forms.ContryForm;


public class CountryHandler {
	CountryDaoImpl impl = new CountryDaoImpl();

	public List<ContryForm> list(int roleid,String compid,DataSource dataSource) {
		return impl.listAll(roleid,compid,dataSource);
	}

	public String add(ContryForm countryForm, String compid, DataSource dataSource) {
		return impl.add(countryForm,compid ,dataSource);
	}

	public String changeStatus(ContryForm countryForm, DataSource dataSource) {
		return impl.changeStatus(countryForm, dataSource);
	}

	public String updateCountry(ContryForm countryForm, DataSource dataSource, Long userid) {
		//**************Start Audit Trial Inserting Code********************
		//impl.auditTrail(dataSource, countryForm, userid); 
		//**************End Audit Trial Inserting Code********************
		return impl.update(countryForm, dataSource);
	}

	public List<ContryForm> activeList(int roleid,String compid, DataSource dataSource) {
		return impl.activeList(roleid,compid,dataSource);
	}

	public JSONArray ajaxActiveList(DataSource dataSource, String countryid) {
		return impl.ajaxActiveList(dataSource,countryid);
	}

}
