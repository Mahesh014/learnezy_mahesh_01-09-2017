package com.itech.elearning.handler;

import java.util.List;

import javax.sql.DataSource;

import org.apache.struts.validator.ValidatorForm;

import com.itech.elearning.daoimpl.PassPercentDaoImpl;
import com.itech.elearning.forms.PassPercentForm;


public class PassPercentHandler extends ValidatorForm {
	
	private static final long serialVersionUID = 1L;
	PassPercentDaoImpl impl=new PassPercentDaoImpl();
	
	public List<PassPercentForm> courcelist(String compid, DataSource dataSource) {
		return impl.courcelist(compid,dataSource);
	}

	public List<PassPercentForm> passpercentList(DataSource dataSource, long courceID, String compid) {
		return  impl.passpercentList(dataSource,courceID,compid);
	}

	public String changeStatus(String compid, PassPercentForm passpercentForm,DataSource dataSource, long parseid) {
		return impl.changeStatus(compid,passpercentForm, dataSource, parseid);

	}

	public Object add(String compid, PassPercentForm passpercentForm, DataSource dataSource) {
		return impl.add(compid,passpercentForm,dataSource);
	}

	public Object update(PassPercentForm passpercentForm, DataSource dataSource) {
		return impl.update(passpercentForm,dataSource);
	}
	
	
	
	

}
