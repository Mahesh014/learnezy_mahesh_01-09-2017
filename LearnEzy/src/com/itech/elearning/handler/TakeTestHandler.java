package com.itech.elearning.handler;

import java.util.HashMap;

import java.util.List;

import javax.sql.DataSource;

import org.json.simple.JSONObject;

import com.itech.elearning.daoimpl.TakeTestDaoImpl;
import com.itech.elearning.forms.TestMasterForm;

public class TakeTestHandler {
	TakeTestDaoImpl impl = new TakeTestDaoImpl();
	public List<TestMasterForm> listTestByCourseId(String compid, Long userid,	DataSource dataSource, String courseId) {
		return impl.listTestByCourseId(compid,userid, dataSource, courseId);
	}
	public List<TestMasterForm> takeTest(String compid, Long userid, DataSource dataSource,String courseId, String testId) {
		return impl.takeTest(compid,userid, dataSource, courseId, testId);
	}
	public JSONObject getQuestion(DataSource dataSource, int questionId) {
		return impl.getQuestion( dataSource,questionId);
	}
	public JSONObject insertTest(String compid, DataSource dataSource,	HashMap<Integer, String> answerQuestion, Long courseId, Long testId, Long userId) {
		return impl.insertTest(compid,dataSource,answerQuestion,courseId,testId,userId);
	}
	 
 

}
