package com.itech.elearning.handler;

import java.util.List;

import javax.sql.DataSource;
import com.itech.elearning.daoimpl.WhiteBoardDao;
import com.itech.elearning.forms.WhiteBoardForm;

public class WhiteBoardHandler {
	WhiteBoardDao impl= new WhiteBoardDao();
	public List<WhiteBoardForm> appointmentUserlist(int roleid,String compid,DataSource dataSource) {
		return impl.appointmentUserlist(roleid,compid,dataSource);
	}
	public int addSession(WhiteBoardForm whiteBoardForm, DataSource dataSource, String appointmentScheduleId, String meetingtitle, String meetingpassword, String option, int whiteBoardSessionID) {
		return impl.addSession(whiteBoardForm, dataSource,appointmentScheduleId,meetingtitle,meetingpassword,option,whiteBoardSessionID);
	}
	public WhiteBoardForm getWhiteBoardSessionID(DataSource dataSource, String appointmentScheduleId) {
		return impl.getWhiteBoardSessionID(dataSource,appointmentScheduleId);
	}
	public int updateSession(DataSource dataSource, String appointmentScheduleId, String option, String whiteBoardSessionID) {
		return impl.updateSession(dataSource,appointmentScheduleId,option,whiteBoardSessionID);
		
	}
	public int addVideoSession(WhiteBoardForm whiteBoardForm, DataSource dataSource, String appointmentScheduleId, String meetingtitle, String meetingpassword, String option, String meetingName, String meetingURLLink) {
		return impl.addVideoSession(whiteBoardForm, dataSource,appointmentScheduleId,meetingtitle,meetingpassword,option,meetingName,meetingURLLink);
	}
	public int updateVideoSession(DataSource dataSource, String appointmentScheduleId, String option) {
		return impl.updateVideoSession(dataSource,appointmentScheduleId,option);
		
	}
	public WhiteBoardForm getAppointmentDetails(DataSource dataSource, String appointmentScheduleId) {
		return impl.getAppointmentDetails(dataSource,appointmentScheduleId);
	}

}
