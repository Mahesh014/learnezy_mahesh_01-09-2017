package com.itech.elearning.handler;

import java.util.ArrayList;
import java.util.List;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.simple.JSONArray;

import com.itech.elearning.daoimpl.MYStudentRegistrationDaoIml;
import com.itech.elearning.forms.StudentRegistrationForm;

public class MYStudentRegistrationHandler {

	MYStudentRegistrationDaoIml impl=new MYStudentRegistrationDaoIml();
	
	public int addUser(String compid, StudentRegistrationForm studentForm,String courseList, DataSource dataSource, String profilePhotoName, int paymentStatus) {
		return impl.addUser(compid,studentForm,dataSource,courseList,profilePhotoName,paymentStatus);
	}

	public List<StudentRegistrationForm> courselist1(String compid, DataSource dataSource) {
		return impl.courselist1(compid,dataSource);
	}
	public List<StudentRegistrationForm> courselist(String compid, DataSource dataSource) {
		return impl.courselist(compid,dataSource);
	}
	public Object getCourseBasedOnIds(String courseIdsList,DataSource dataSource) {
		return impl.getCourseBasedOnIds(courseIdsList,dataSource);
	}
	
	public int checkUserName(DataSource dataSource, String userName) {
		return impl.checkUserName(dataSource,userName);
	}
	
	public StudentRegistrationForm getStudentData(String userId, String courseIdsList, DataSource dataSource) {
		return impl.getStudentData(userId,courseIdsList,dataSource);
	}
	
	public int updatePaymentStatus(DataSource dataSource, String userId, int paymentStatus) {
		return impl.updatePaymentStatus(dataSource,userId, paymentStatus );
	}

	public int userRegistration(StudentRegistrationForm studentForm, String compid, DataSource dataSource, String profilePhotoName) {
		return impl.userRegistration(studentForm,compid,dataSource,profilePhotoName);
	}

	public int addCourse(DataSource dataSource, String courseList, long userId) {
		return impl.addCourse(dataSource,courseList,userId);
	}
	
	public List<StudentRegistrationForm> selectedcourselist(String compid, DataSource dataSource, String courseList) {
		return impl.selectedcourselist(compid,dataSource,courseList);
	}

	public int addNewCourse(DataSource dataSource, String courseList,long userId) {
		return impl.addNewCourse(dataSource,courseList,userId);
	}

	public List<StudentRegistrationForm> courseNotTakenlist(DataSource dataSource, Long userid, String compid) {
		return impl.courseNotTakenlist(dataSource,userid,compid);
	}
	public int AcademyRegistration(StudentRegistrationForm studentForm,	String compid, DataSource dataSource, String profilePhotoName) {
		// TODO Auto-generated method stub
		return impl.academyRegistration(studentForm,compid,dataSource,profilePhotoName);
	}

	public int addCompany(StudentRegistrationForm studentForm, String compid, DataSource dataSource) {
		return impl.addCompany(studentForm,compid,dataSource);
	}

	public int FreeLanceRegistration(StudentRegistrationForm studentForm,
			String compid, DataSource dataSource, String profilePhotoName) {
		return impl.FreeLanceRegistration(studentForm,compid,dataSource,profilePhotoName);
	}

	public JSONArray verifyemailid(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response, String email) {
		// TODO Auto-generated method stub
		return impl.verifyemailid(dataSource,request,response,email);
	}

	public JSONArray addcourses(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String email, String course) {
		// TODO Auto-generated method stub
		return impl.addcourses(dataSource,request,response,email,course);
	}

	public JSONArray addtrasnsactiondetails(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String userid, String courseid, String totalidamount, String tax) {
		// TODO Auto-generated method stub
		return impl.addtrasnsactiondetails(dataSource,request,response,userid,courseid,totalidamount,tax);
	}

	public ArrayList<String> getProductListArray1(DataSource dataSource,
			String term) {
		// TODO Auto-generated method stub
		return impl.getProductListArray1(dataSource,term);
	}
}
