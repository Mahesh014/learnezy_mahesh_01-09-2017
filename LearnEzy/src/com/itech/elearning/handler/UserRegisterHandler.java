package com.itech.elearning.handler;

import java.util.List;
import javax.sql.DataSource;
import com.itech.elearning.daoimpl.UserRegisterDaoImpl;
import com.itech.elearning.forms.CompanyMasterForm;
import com.itech.elearning.forms.UserRegisterForm;

public class UserRegisterHandler {

	UserRegisterDaoImpl impl = new UserRegisterDaoImpl();

	
	
	public String addUser(UserRegisterForm userForm, DataSource dataSource) {
		return impl.addUser(userForm, dataSource);
	}

	public List<UserRegisterForm> roleList(int roleid,String compid, DataSource dataSource) {
		return impl.roleList(roleid,compid,dataSource);
	}

	public List<UserRegisterForm> listAll(int roleid,String compid, Long userid, DataSource dataSource) {
		return impl.listAll(roleid,compid,userid, dataSource);
	}

	public String changeStatus(UserRegisterForm userForm, DataSource dataSource) {
		return impl.changeStatus(userForm, dataSource);
	}

	public UserRegisterForm get(long userid, DataSource dataSource) {
		return impl.get(userid, dataSource);
	}

	public String updateUser(UserRegisterForm userForm, DataSource ds) {
		return impl.updateUser(userForm, ds);
	}

	public List<UserRegisterForm> listAllCourse(int roleid,String compid,Long userid,DataSource dataSource) {
		return impl.listAllCourse(roleid,compid,userid, dataSource);
	}

	public int checkUserName(DataSource dataSource, String userName) {
		return impl.checkUserName(dataSource,userName);
	}

	public List<UserRegisterForm> list(DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.listallcompany(dataSource);
	}

	public List<UserRegisterForm> list(String compid, DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.listallcompanynew(compid,dataSource);
	}

	
	
	

}
