package com.itech.elearning.handler;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.simple.JSONArray;

import COM.rsa.jsafe.da;

import com.itech.elearning.daoimpl.DynamicDataDaoImpl;
import com.itech.elearning.forms.DynamicDataForm;

public class DynamicDataHandler {

	DynamicDataDaoImpl impl = new DynamicDataDaoImpl();
	public JSONArray ajaxActiveList(DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.ajaxActiveList(dataSource);
	}
	public List<DynamicDataForm> list(DataSource dataSource) {
		
		return impl.list(dataSource);
	}
	public List<DynamicDataForm> listcity(DataSource dataSource) {
		// TODO Auto-generated method stub
	return impl.listcity(dataSource);
	}
	public List<DynamicDataForm> searchlist(String couursename,String cityname,String coursemode,String cityid, DataSource dataSource, HttpServletRequest request)
	{
		// TODO Auto-generated method stub
		return impl.searchlist(couursename,cityname,coursemode,cityid,dataSource,request);
	}
	public List<DynamicDataForm> LatestCourselist(DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.LatestCourselist(dataSource);
	}
	public List<DynamicDataForm> LatestCourseCategory(DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.LatestCourseCategory(dataSource);
	}
	public List<DynamicDataForm> courselist(String id, DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.courselist(id,dataSource);
	}
	public List<DynamicDataForm> dynamicnumbers(DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.dynamicnumbers(dataSource);
	}
	public List<DynamicDataForm> reviewdetails(DataSource dataSource) 
	{
		return impl.reviewdetails(dataSource);
	}
	public String addsubscribe(DynamicDataForm roleForm, DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.addsubscribe(roleForm,dataSource);
	}
	public String addsubscribe2(String txtname,String txtemail,String contactnumber,String message, DataSource dataSource) {
		// TODO Auto-generated method stub
		System.out.println("Inside handler");
		return impl.addsubscribe2(txtname,txtemail,contactnumber,message,dataSource);
	}
	public List<DynamicDataForm> populardetails(DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.populardetails(dataSource);
	}
	public List<DynamicDataForm> recentdetails(DataSource dataSource) 
	{
		// TODO Auto-generated method stub
		return impl.recentdetails(dataSource);
	}
	public List<DynamicDataForm> latestcourses(DataSource dataSource) 
	{
		// TODO Auto-generated method stub
		return impl.latestcourses(dataSource);
	}
	
	public List<DynamicDataForm> latestcoursessearch(DynamicDataForm dynamicdataform, DataSource dataSource)
	{
		// TODO Auto-generated method stub
		return impl.latestcoursessearch(dynamicdataform,dataSource);
	}
	public List<DynamicDataForm> dynamicnumberslatest(DataSource dataSource) 
	{
		// TODO Auto-generated method stub
		return impl.dynamicnumberslatest(dataSource);
	}
	public List<DynamicDataForm> searchelearningcourse(DataSource dataSource) 
	{
		// TODO Auto-generated method stub
		return impl.searchelearningcourse(dataSource);
	}
	public List<DynamicDataForm> selectingcourses(DataSource dataSource, String eleaning, String classroom, String paid, String free, String beginner, String intermediate, String advanced, String lessfive, String graterfive, String gratertwo, String greaterfivethousand, String categorynames, String institutenames, String localitynames, String cities, String coursenames) {
		// TODO Auto-generated method stub
		return impl.selectingcourses(dataSource,eleaning,classroom,paid,free,beginner,intermediate,advanced,lessfive,graterfive,gratertwo,greaterfivethousand,categorynames,institutenames,localitynames,cities,coursenames);
	}
	public JSONArray selectcoursecategory(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		return impl.selectcoursecategory(dataSource,request,response);
	}
	public List<DynamicDataForm> coursesearch(DynamicDataForm dynaform,
			DataSource dataSource, HttpServletRequest request) {
		// TODO Auto-generated method stub
		return impl.coursesearch(dynaform,dataSource,request);
	}
	public List<DynamicDataForm> coursesdescription(DynamicDataForm dynaform,
			DataSource dataSource, HttpServletRequest request, int courseid) {
		// TODO Auto-generated method stub
		return impl.coursesdescription(dynaform,dataSource,request,courseid);
	}
	public List<DynamicDataForm> releatedcourse(DynamicDataForm dynaform,
			DataSource dataSource, HttpServletRequest request, int courseid, int coursecatid) {
		// TODO Auto-generated method stub
		return impl.releatedcourse(dynaform,dataSource,request,courseid,coursecatid);
	}
	public List<DynamicDataForm> coursecategory(DynamicDataForm dynaform,
			DataSource dataSource, HttpServletRequest request, int courseid,
			int coursecatid) {
		// TODO Auto-generated method stub
		return impl.coursecategory(dynaform,dataSource,request,courseid,coursecatid);
	}
	public List<DynamicDataForm> latestcoursesdescription(DataSource dataSource, int catid) {
		// TODO Auto-generated method stub
		return impl.latestcoursesdescription(dataSource,catid);
	}
	public String add(DynamicDataForm dynaform, DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.add(dynaform,dataSource);
	}
	
	public JSONArray elearningcount(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		return impl.elearningcount(dataSource,request,response);
	}
	public List<DynamicDataForm> pricelowtohigh(DynamicDataForm dynaform,
			DataSource dataSource, String coursenames, String countrys) {
		// TODO Auto-generated method stub
		return impl.pricelowtohigh(dynaform,dataSource,coursenames,countrys);
	}
	public List<DynamicDataForm> pricehightolow(DynamicDataForm dynaform,
			DataSource dataSource, String coursenames, String countrys) {
		// TODO Auto-generated method stub
		return impl.pricehightolow(dynaform,dataSource,coursenames,countrys);
	}
	public List<DynamicDataForm> rating(DynamicDataForm dynaform,
			DataSource dataSource, String coursenames, String countrys) {
		// TODO Auto-generated method stub
		return impl.rating(dynaform,dataSource,coursenames,countrys);
	}
	public JSONArray classroomcounts(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		return impl.classroomcounts(dataSource,request,response);
	}
	public JSONArray freecounts(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		return impl.freecounts(dataSource,request,response);
	}
	public JSONArray beginnercounts(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		return impl.beginnercounts(dataSource,request,response);
	}
	public JSONArray intermediatecounts(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		return impl.intermediatecounts(dataSource,request,response);
	}
	public JSONArray advancedcounts(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		return impl.advancedcounts(dataSource,request,response);
	}
	public JSONArray paidcounts(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		return impl.paidcounts(dataSource,request,response);
	}
	public List<DynamicDataForm> pricehightolowsearchagain(
			DynamicDataForm dynaform, DataSource dataSource,
			HttpServletRequest request, String coursename, String localitys,
			String city) {
		// TODO Auto-generated method stub
		return impl.pricehightolowsearchagain(dynaform,dataSource,request,coursename,localitys,city);
	}
	public List<DynamicDataForm> pricelowtohighsearchagain(
			DynamicDataForm dynaform, DataSource dataSource,
			HttpServletRequest request, String coursename, String localitys,
			String city) {
		// TODO Auto-generated method stub
		return impl.pricelowtohighsearchagain(dynaform,dataSource,request,coursename,localitys,city);
	}
	public List<DynamicDataForm> ratewisesearchagain(DynamicDataForm dynaform,
			DataSource dataSource, HttpServletRequest request,
			String coursename, String localitys, String city) {
		// TODO Auto-generated method stub
		return impl.ratewisesearchagain(dynaform,dataSource,request,coursename,localitys,city);
	}
	public List<DynamicDataForm> pricehightolowlatestcourses(
			DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.pricehightolowlatestcourses(dataSource);
	}
	public List<DynamicDataForm> pricelowtohighlatestcourses(
			DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.pricelowtohighlatestcourses(dataSource);
	}
	public List<DynamicDataForm> ratewiselatestcourses(DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.ratewiselatestcourses(dataSource);
	}
	public List<DynamicDataForm> pricelowtohighselectingcourses(
			DataSource dataSource, String eleaning, String classroom,
			String paid, String free, String beginner, String intermediate,
			String advanced, String lessfive, String graterfive,
			String gratertwo, String greaterfivethousand, String categorynames) {
		// TODO Auto-generated method stub
		return impl.pricelowtohighselectingcourses(dataSource,eleaning,classroom,paid,free,beginner,intermediate,advanced,lessfive,graterfive,gratertwo,greaterfivethousand,categorynames);
	}
	public List<DynamicDataForm> pricehightolowselectingcourses(
			DataSource dataSource, String eleaning, String classroom,
			String paid, String free, String beginner, String intermediate,
			String advanced, String lessfive, String graterfive,
			String gratertwo, String greaterfivethousand, String categorynames) {
		// TODO Auto-generated method stub
		return impl.pricehightolowselectingcourses(dataSource,eleaning,classroom,paid,free,beginner,intermediate,advanced,lessfive,graterfive,gratertwo,greaterfivethousand,categorynames);
	}
	public List<DynamicDataForm> priceratewiseselectingcourses(
			DataSource dataSource, String eleaning, String classroom,
			String paid, String free, String beginner, String intermediate,
			String advanced, String lessfive, String graterfive,
			String gratertwo, String greaterfivethousand, String categorynames) {
		// TODO Auto-generated method stub
		return impl.priceratewiseselectingcourses(dataSource,eleaning,classroom,paid,free,beginner,intermediate,advanced,lessfive,graterfive,gratertwo,greaterfivethousand,categorynames);
	}
	public List<DynamicDataForm> latestcourseratewise(DataSource dataSource,
			int catid) {
		// TODO Auto-generated method stub
		return impl.latestcourseratewise(dataSource,catid);
	}
	public List<DynamicDataForm> latestcourserpricelowtohigh(
			DataSource dataSource, int catid) {
		// TODO Auto-generated method stub
		return impl.latestcourserpricelowtohigh(dataSource,catid);
	}
	public List<DynamicDataForm> latestcourserpricehightolow(
			DataSource dataSource, int catid) {
		// TODO Auto-generated method stub
		return impl.latestcourserpricehightolow(dataSource,catid);
	}
	public JSONArray city(DataSource dataSource, HttpServletRequest request,
			HttpServletResponse response) {
		// TODO Auto-generated method stub
		return impl.city(dataSource,request,response);
	}
	public ArrayList<String> getProductListArray(DataSource dataSource,
			String term) {
		// TODO Auto-generated method stub
		return impl.getProductListArray(dataSource,term);
	}
	public ArrayList<String> getlocality(DataSource dataSource, String term) {
		// TODO Auto-generated method stub
		return impl.getlocality(dataSource,term);
	}
	public ArrayList<String> getcity(DataSource dataSource, String term) {
		// TODO Auto-generated method stub
		return impl.getcity(dataSource,term);

	}
	public List<DynamicDataForm> institutelist(DynamicDataForm dynaform,
			DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.institutelist(dynaform,dataSource);
	}
	public List<DynamicDataForm> localitylist(DynamicDataForm dynaform,
			DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.localitylist(dynaform,dataSource);
	}
	public List<DynamicDataForm> citylist(DynamicDataForm dynaform,
			DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.citylist(dynaform,dataSource);
	}
	public List<DynamicDataForm> courselists(DynamicDataForm dynaform,
			DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.courselists(dynaform,dataSource);
	}
	public JSONArray getcityindex(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response, int city) {
		// TODO Auto-generated method stub
		return impl.getcityindex(dataSource,request,response,city);
	}
	public JSONArray getinstitute(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String institute) {
		// TODO Auto-generated method stub
		return impl.getinstitute(dataSource,request,response,institute);
	}
	public JSONArray getcityurl(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String cityid) {
		// TODO Auto-generated method stub
		return impl.getcityurl(dataSource, request, response,cityid);
	}
	public JSONArray getlocalityurl(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String locality) {
		// TODO Auto-generated method stub
		return impl.getlocalityurl(dataSource,request,response,locality);
	}
	public JSONArray getcategoryurl(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String catname) {
		// TODO Auto-generated method stub
		return impl.getcategoryurl(dataSource,request,response,catname);
	}
	public JSONArray getcourse(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String coursename) {
		// TODO Auto-generated method stub
		return impl.getcourse(dataSource,request,response,coursename);
	}
	public List<DynamicDataForm> sitemaplist(DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.sitemaplist(dataSource);
	}
	
	public JSONArray subscribeMail(DataSource dataSource, HttpServletResponse response,
			String fromdate, HttpServletRequest request) {
		return impl.subscribeMail(fromdate,dataSource);
	}
	
	public JSONArray addtocart(DataSource dataSource,
			HttpServletResponse response, String cartid,
			HttpServletRequest request) {
		return impl.addtocart(cartid,dataSource);
	}
	public List<DynamicDataForm> cartlist(String cartid,DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.cartlist(cartid,dataSource);
	}
	

}
