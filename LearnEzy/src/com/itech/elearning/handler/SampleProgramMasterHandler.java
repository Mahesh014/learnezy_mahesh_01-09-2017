package com.itech.elearning.handler;

import java.util.List;

import javax.sql.DataSource;

import org.json.simple.JSONObject;

import com.itech.elearning.daoimpl.SampleProgramMasterDaoIml;
import com.itech.elearning.forms.SampleProgramMasterForm;

public class SampleProgramMasterHandler {

	SampleProgramMasterDaoIml impl=new SampleProgramMasterDaoIml();
	
	public List<SampleProgramMasterForm> listAllCourse(int roleid,long userid,String compid, DataSource dataSource) {
		return impl.listAllCourse(roleid,userid,compid,dataSource); 
	}


	public String add(SampleProgramMasterForm subtopicForm,String compid, DataSource dataSource) {
		return impl.add(subtopicForm,compid,dataSource);
	}

	public String changeStatus(SampleProgramMasterForm subtopicForm,DataSource dataSource, long sampleProgramID) {
		return impl.changeStatus(subtopicForm,sampleProgramID,dataSource);
	}

	public Object update(SampleProgramMasterForm subtopicForm,DataSource dataSource) {
		return impl.update(subtopicForm,dataSource);
	}

	public List<SampleProgramMasterForm> listSampleProgram(int roleid,long userid,	String compid, DataSource dataSource) {
		return impl.listSampleProgram(roleid,userid,compid,dataSource);
	}


	public JSONObject getEdit(DataSource dataSource, long sampleProgramID) {
		return impl.getEdit(dataSource,sampleProgramID);
	}
	
	public String sampleProgramExcelUpload(String fileName, String filePath, String compid) {
		return impl.sampleProgramExcelUpload(fileName,filePath,compid);
	}

	 
}
