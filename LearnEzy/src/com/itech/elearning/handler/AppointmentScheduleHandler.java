package com.itech.elearning.handler;

import java.util.List;

import javax.sql.DataSource;

import org.json.simple.JSONArray;

import com.itech.elearning.daoimpl.AppointmentScheduleDao;
import com.itech.elearning.forms.AppointmentScheduleForm;

public class AppointmentScheduleHandler {
	
	AppointmentScheduleDao impl= new AppointmentScheduleDao();
	
	public List<AppointmentScheduleForm> list(int roleid,DataSource dataSource, String compid) {
		return impl.list(roleid,dataSource,compid);
	}
	
	public List<AppointmentScheduleForm> userList(int roleid,String compid,DataSource dataSource) {
		return impl.userList(roleid,compid,dataSource);
	}
	
	public List<AppointmentScheduleForm> courseList(int roleid,String compid,DataSource dataSource) {
		return impl.courseList(roleid,compid,dataSource);
	}
	public String add(AppointmentScheduleForm apptForm, DataSource dataSource, Long userid, String compid) {
		return impl.add(apptForm, dataSource, userid,compid);
	}

	public String changeStatus(AppointmentScheduleForm apptForm, DataSource dataSource) {
		return impl.changeStatus(apptForm, dataSource);
	}

	public String update(AppointmentScheduleForm apptForm, DataSource dataSource, Long userid) {
	 
		return impl.update(apptForm, dataSource);
	}

	public List<AppointmentScheduleForm> activeList(DataSource dataSource) {
		return impl.activeList(dataSource);
	}

	public JSONArray getStudentName(DataSource dataSource, String studentIds) {
		return impl.getStudentName(dataSource,studentIds);
	}

	
}
