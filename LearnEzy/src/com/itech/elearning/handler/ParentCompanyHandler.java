package com.itech.elearning.handler;

import java.util.List;

import javax.sql.DataSource;

import org.json.simple.JSONArray;

import com.itech.elearning.daoimpl.ParentCompanyDaoImpl;
import com.itech.elearning.forms.ParentCompanyForm;

public class ParentCompanyHandler {

	ParentCompanyDaoImpl impl = new ParentCompanyDaoImpl();
	public List<ParentCompanyForm> list(int roleid, String compid,
			DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.listAll(roleid,compid,dataSource);
	}

	public String add(ParentCompanyForm countryForm, String compid,
			DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.add(countryForm,compid ,dataSource);
	}

	public String changeStatus(ParentCompanyForm countryForm,
			DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.changeStatus(countryForm,dataSource);
	}

	public String updateCountry(ParentCompanyForm countryForm,
			DataSource dataSource, Long userid) {
		// TODO Auto-generated method stub
		return impl.update(countryForm, dataSource);
	}

	public JSONArray ajaxActiveList(DataSource dataSource, String countryid) {
		return impl.ajaxActiveList(dataSource,countryid);
	
	}
	
	

	
}
