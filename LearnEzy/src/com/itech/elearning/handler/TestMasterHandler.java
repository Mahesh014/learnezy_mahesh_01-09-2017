package com.itech.elearning.handler;

import java.util.List;


import javax.sql.DataSource;

import com.itech.elearning.daoimpl.TestMasterDaoImpl;
import com.itech.elearning.forms.TestMasterForm;

public class TestMasterHandler {
	TestMasterDaoImpl impl = new TestMasterDaoImpl();

	public List<TestMasterForm> listAllCourse(int roleid,String compid,Long userid, DataSource dataSource) {
		
		return impl.listAllCourse(roleid,compid,userid, dataSource);
	}

	public List<TestMasterForm> listAllTopic(Long userid, Long courseId, DataSource dataSource) {
		
		return impl.listAllTopic(userid, courseId, dataSource);
	}

	public List<TestMasterForm> listAllTestType(int roleid,String compid,Long userid, DataSource dataSource) {
		
		return impl.listAllTestType(roleid,compid,userid, dataSource);
	}

	public String addTest(String compid,TestMasterForm testForm, DataSource dataSource, String courseId, Long testTypeId, String tname, String tdate, String qIds, String testTime, String minScore, String achivementScore) {
		return impl.addTest(compid,testForm, dataSource,courseId,testTypeId,tname,tdate,qIds,testTime,minScore,achivementScore);
	}

	public String changeTestType(TestMasterForm testForm, DataSource dataSource) {
		
		return impl.changeTestType(testForm, dataSource);
	}

	public List<TestMasterForm> listAllTest(int roleid,String compid,Long userid, DataSource dataSource,	Long topicId) {
		
		return impl.listAllTest(roleid,compid,userid, dataSource, topicId);
	}

	public String updateTest(TestMasterForm testForm, DataSource dataSource, String courseId, Long testTypeId, String tname, String tdate, String qIds, String testTime, String minScore, String achivementScore, String testId) {
		
		return impl.updateTest(testForm,dataSource,courseId,testTypeId,tname,tdate,qIds,testTime,minScore,achivementScore,testId);
	}
	
	public String testExcelUpload(String fileName, String filePath, String compid) {
		return impl.testExcelUpload(fileName,filePath,compid);
	}

}
