package com.itech.elearning.handler;

import java.util.List;





import javax.sql.DataSource;

import org.json.simple.JSONArray;

import com.itech.elearning.daoimpl.StateDaoImpl;
import com.itech.elearning.forms.StateForm;



public class StateHandler {

	StateDaoImpl impl = new StateDaoImpl();

	public List<StateForm> list(int roleid,long countryid, String compid, DataSource dataSource) {
		return impl.listAll(roleid,countryid,compid,dataSource);
	}

	public String addState(String compid, StateForm stateForm, DataSource dataSource) {
		return impl.add(compid,stateForm, dataSource);
	}

	public String changeState(StateForm stateForm, DataSource dataSource) {
		return impl.changeStatus(stateForm, dataSource);
	}

	public String updateState(StateForm stateForm, DataSource dataSource, Long userid) {
		/*//**************Start Audit Trial Inserting Code********************
		impl.auditTrail(dataSource, stateForm, userid); 
		//**************End Audit Trial Inserting Code*********************/		
		return impl.update(stateForm, dataSource);
	}

	public List<StateForm> activeList(int roleid,String compid,DataSource dataSource) {
		return impl.activeList(roleid,compid,dataSource);
	}
	
	public List<StateForm> activeListByCountry(int roleid,String compid,long countryid, DataSource dataSource) {
		return impl.activeListByCountry(roleid,compid,countryid, dataSource);
	}

	public JSONArray ajaxActiveList(DataSource dataSource, String countryId) {
		return impl.ajaxActiveList(dataSource,countryId);
	}
}
