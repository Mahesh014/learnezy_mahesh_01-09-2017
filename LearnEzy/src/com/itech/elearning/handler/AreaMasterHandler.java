package com.itech.elearning.handler;

import java.util.List;

import javax.sql.DataSource;

import org.json.simple.JSONArray;

import com.itech.elearning.daoimpl.AreaMasterDaoImpl;
import com.itech.elearning.forms.AreaMasterForm;
import com.itech.elearning.forms.CityForm;

public class AreaMasterHandler {
	AreaMasterDaoImpl impl = new AreaMasterDaoImpl();

	public List<AreaMasterForm> list(int roleid, String compid, long stateid,
			long countryid, long cityid, DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.list(roleid,compid,stateid,countryid,cityid,dataSource);
	}

	public String add(AreaMasterForm cityForm, String compid, DataSource dataSource) {
		return impl.add(cityForm,compid, dataSource);
	}

	public String update(AreaMasterForm cityForm, DataSource dataSource,
			Long userid) {
		// TODO Auto-generated method stub
		return impl.update(cityForm,dataSource,userid);
	}

	public String changeStatus(String areadid,AreaMasterForm cityForm, DataSource dataSource) {
		// TODO Auto-generated method stub
		return impl.changeStatus(areadid,cityForm,dataSource);
	}

	public JSONArray ajaxActiveList(DataSource dataSource, String cityid) {
		// TODO Auto-generated method stub
		return impl.ajaxActiveList(dataSource,cityid);
	}
}
