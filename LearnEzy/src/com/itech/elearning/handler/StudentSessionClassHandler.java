package com.itech.elearning.handler;

import java.util.List;


import javax.sql.DataSource;

import org.json.simple.JSONArray;

import com.itech.elearning.daoimpl.StudentSessionClassDao;
import com.itech.elearning.forms.StudentSessionClassForm;

public class StudentSessionClassHandler {
	StudentSessionClassDao impl = new StudentSessionClassDao();
	public List<StudentSessionClassForm> appointmentUserlist(DataSource dataSource, Long userid) {
		return impl.appointmentUserlist(dataSource,userid);
	}
	 
	public StudentSessionClassForm getWhiteBoardSessionID(DataSource dataSource, String appointmentScheduleId) {
		return impl.getWhiteBoardSessionID(dataSource,appointmentScheduleId);
	}

	public JSONArray getStudentSessionList(DataSource dataSource, Long userid) {
		return impl.getStudentSessionList(dataSource, userid);
	}
}
