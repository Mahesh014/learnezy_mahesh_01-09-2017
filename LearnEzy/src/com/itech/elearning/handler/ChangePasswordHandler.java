package com.itech.elearning.handler;

import javax.sql.DataSource;

import com.itech.elearning.daoimpl.ChangePasswordDaoImpl;
import com.itech.elearning.forms.ChangePasswordForm;

public class ChangePasswordHandler {

	ChangePasswordDaoImpl dao = new ChangePasswordDaoImpl();

	public int changePassword(ChangePasswordForm changepwdform,DataSource dataSource, Long userid) {
		return dao.updatepwd(changepwdform, dataSource,userid);
	}

}
