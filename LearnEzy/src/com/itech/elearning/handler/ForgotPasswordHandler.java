package com.itech.elearning.handler;

import javax.sql.DataSource;

import com.itech.elearning.daoimpl.ForgotPasswordDaoImpl;
import com.itech.elearning.forms.ForgotPasswordForm;

public class ForgotPasswordHandler {
	ForgotPasswordDaoImpl impl = new ForgotPasswordDaoImpl();

	public String forgotPassword(ForgotPasswordForm forgotPasswordForm,	DataSource dataSource) {
		return impl.forgotPassword(forgotPasswordForm, dataSource);
	}

}
