package com.itech.elearning.handler;

import java.util.List;

import javax.sql.DataSource;

import org.json.simple.JSONArray;

import com.itech.elearning.daoimpl.GetCertificateDaoImpl;
import com.itech.elearning.forms.GetCertificateForm;

public class GetCertificateHandler {
	GetCertificateDaoImpl impl= new GetCertificateDaoImpl();
	public List<GetCertificateForm> list(Long userid, DataSource dataSource) {
		return null;
		//return impl.list(userid, dataSource);
	}
	public JSONArray courseList(String compid, DataSource dataSource, Long userid) {
		 return impl.courseList(compid,userid,dataSource) ;
	}
	public List<GetCertificateForm> getCertificateDetials(String compid, Long userid,DataSource dataSource, String courseId) {
		return impl.getCertificateDetials(compid,userid,dataSource,courseId) ;
	}
	public int courseCompletedCheck(String compid, DataSource dataSource, Long userid, int courseId) {
		 return impl.courseCompletedCheck(compid,dataSource,userid,courseId) ;
	}
}
