package com.itech.elearning.handler;

import java.util.List;


import javax.sql.DataSource;

import org.json.simple.JSONArray;

import com.itech.elearning.daoimpl.AssignmentMasterDao;
import com.itech.elearning.forms.AssignmentMasterForm;

public class AssignmentMasterHandler {
	AssignmentMasterDao impl = new AssignmentMasterDao();

	public List<AssignmentMasterForm> listAllCourse(int roleid,String compid,Long userid, DataSource dataSource) {
		
		return impl.listAllCourse(roleid,compid,userid, dataSource);
	}
	
	public List<AssignmentMasterForm> listAllSubTopic(int roleid,String compid,Long userid, Long courseId,	Long topicId, DataSource dataSource) {
		
		return impl.listAllSubTopic(roleid,compid,userid, courseId,topicId, dataSource);
	}
	
public JSONArray listAllSubTopic1(String compid,Long userid, Long courseId,	Long topicId, DataSource dataSource) {
		
		return impl.listAllSubTopic1(compid,userid, courseId,topicId, dataSource);
	}
	 
	public List<AssignmentMasterForm> listAllTopic(int roleid,String compid,Long userid, Long courseId,	DataSource dataSource) {
		
		return impl.listAllTopic(roleid,compid,userid, courseId, dataSource);
	}

	public List<AssignmentMasterForm> assignmentList(int roleid,String compid,Long userid,Long courseId,Long topicId,DataSource dataSource) {
		
		return impl.assignmentList(roleid,compid,userid, dataSource,courseId,topicId);
	}

	public String add(AssignmentMasterForm asingForm, String compid, DataSource dataSource) {
		
		return impl.add(asingForm,compid ,dataSource);
	}

	public String changeStatus(AssignmentMasterForm asingForm, DataSource dataSource, Long assignmentId, boolean active) {
		
		return impl.changeStatus(asingForm, dataSource, assignmentId, active);
	}

	 
	public String update(AssignmentMasterForm asingForm, DataSource dataSource) {
		
		return impl.update(asingForm, dataSource);
	}

	public List<AssignmentMasterForm> listAssignmentById(Long userid,Long courseId, DataSource dataSource) {
		return impl.listAssignmentById(userid,courseId,dataSource);
	}

	public String testTypeExcelUpload(String fileName, String filePath) {
		
		return impl.testTypeExcelUpload(fileName,filePath);
	}

	public JSONArray getTopic(DataSource dataSource, long courseId) {
		// TODO Auto-generated method stub
		return impl.getTopic(dataSource,courseId);
	}

}
