package com.itech.elearning.handler;

import java.util.List;
import javax.sql.DataSource;
import com.itech.elearning.daoimpl.StudentProfileDaoImpl;
import com.itech.elearning.forms.StudentProfileForm;

public class StudentProfileHandler {
	
	StudentProfileDaoImpl impl=new StudentProfileDaoImpl();

	public StudentProfileForm getStudentData(Long userid, String courseIdsList, DataSource dataSource) {
		return impl.getStudentData(userid,courseIdsList,dataSource);
	}

	public String updateUser(StudentProfileForm studentForm,String[] theCourseList, DataSource dataSource, String profilePhotoName) {
		return impl.updateUserData(studentForm,theCourseList,dataSource,profilePhotoName);
	}

	public List<StudentProfileForm> getCourseBasedOnIds(String courseIdsList,	DataSource dataSource) {
		return impl.getCourseBasedOnIds(courseIdsList,dataSource);
	}

	public String getCourseList(DataSource dataSource, Long userid) {
		return impl.getCourseList(dataSource,userid);
	}
	
	

}
