package com.itech.elearning.handler;

import java.util.List;

import javax.sql.DataSource;

import com.itech.elearning.daoimpl.LoginDaoImpl;
import com.itech.elearning.forms.LoginForm;

public class LoginHandler {
	LoginDaoImpl impl = new LoginDaoImpl();

	public LoginForm loginValidation(String userName, String password,DataSource dataSource) {
		return impl.login(userName, password, dataSource);
	}

	public LoginForm loginValidation1(String userName, DataSource dataSource) {
		return impl.login1(userName,  dataSource);
	}

	public List<LoginForm> testDetails(Long userId, DataSource dataSource) {
		return impl.testDetails(userId, dataSource);
	}

	public List<LoginForm> testTake(Long userId, DataSource dataSource) {
		return impl.testTake(userId, dataSource);
	}

	public String LastID(DataSource dataSource) {
		return impl.LastID(dataSource);
	}

	public Object getCourseBasedOnIds(String CourseIdsList, DataSource dataSource) {
		return impl.getCourseBasedOnIds(CourseIdsList,dataSource);
	}

	public List<LoginForm> notificationsOnDashBoard(Long userId,DataSource dataSource) {
		return impl.notificationsOnDashBoard(userId,dataSource);
	}

	public List<LoginForm> getTheNumberOfCourseCompleted(Long userId,DataSource dataSource, String courseIdsList) {
		return impl.getTheNumberOfCourseCompleted(userId,dataSource,courseIdsList);
	}

	public List<LoginForm> getCourseProgress(Long userId,DataSource dataSource, String courseIdsList) {
		return impl.getCourseProgress(userId,dataSource,courseIdsList);
	}

	public int checkForCourseAndPayment(long userId, DataSource dataSource) {
		return impl.checkForCourseAndPayment(userId,dataSource);
	}
	public int checkForPayment(long userId, DataSource dataSource) {
		return impl.checkForPayment(userId,dataSource);
	}

}
