package com.itech.elearning.handler;

import java.util.List;

import javax.sql.DataSource;

import com.itech.elearning.daoimpl.TestReportsDaoImpl;
import com.itech.elearning.forms.TestReportsForm;

public class TestReportsHandler {
	TestReportsDaoImpl impl = new TestReportsDaoImpl();
	
	public List<TestReportsForm> studentList(int roleid,String compid, Long userid, DataSource dataSource) {
		return impl.studentList(roleid,compid,userid, dataSource);
	}
	public List<TestReportsForm> listAllCourse(int roleid,String compid, Long userid, DataSource dataSource) {
		return impl.listAllCourse(roleid,compid,userid, dataSource);
	}
	public List<TestReportsForm> testList(int roleid,String compid, Long userid,DataSource dataSource,String studentId, String courseId, String fromDate, String todate) {
		return impl.testList(roleid,compid,userid, dataSource,studentId,courseId,fromDate,todate);
	}
	 
}
