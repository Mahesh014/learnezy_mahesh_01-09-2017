package com.itech.elearning.utils;

import java.text.DateFormat;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateTimeUtil {

	public static String getCurrentDate() {
		int day, month, year;
		GregorianCalendar date = new GregorianCalendar();

		day = date.get(Calendar.DAY_OF_MONTH);
		month = date.get(Calendar.MONTH);
		year = date.get(Calendar.YEAR);

		String currentdate = +year + "-" + (month + 1) + "-" + day;

		return currentdate;
	}

	public static String getCurrentTime() {
		int second, minute, hour;
		GregorianCalendar date = new GregorianCalendar();

		second = date.get(Calendar.SECOND);
		minute = date.get(Calendar.MINUTE);
		hour = date.get(Calendar.HOUR);

		String currenttime = hour + ":" + minute + ":" + second;
		return currenttime;
	}

	public static String getCurrentDateTime() {
		GregorianCalendar date = new GregorianCalendar();
		int day = date.get(Calendar.DAY_OF_MONTH);
		int month = date.get(Calendar.MONTH);
		int year = date.get(Calendar.YEAR);
		int second = date.get(Calendar.SECOND);
		int minute = date.get(Calendar.MINUTE);
		int hour = date.get(Calendar.HOUR);
		String currentdatetime = +year + "-" + (month + 1) + "-" + day + " "
				+ hour + ":" + minute + ":" + second;
		;
		return currentdatetime;
	}

	/*---------------------STRING TO SQL DATE WITH TIME FORMAT--------------------*/
	public static String formatSqlDateTime(String dateString) {
		String formatDate = "0000-00-00 00:00:00";
		java.util.Date date = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat pattern = new SimpleDateFormat("yyyy-MM-dd HH:MM:SS");
			if (!dateString.equals(null) && !dateString.equals("") && !dateString.equals(formatDate)) {
				date = dateFormat.parse(dateString);
				formatDate = pattern.format(date);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return formatDate;
	}

	/*---------------------STRING TO SQL ONLY DATE FORMAT--------------------*/
	public static String formatSqlDate(String dateString) {
		String formatDate = "0000-00-00";
		java.util.Date date = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat pattern = new SimpleDateFormat("yyyy-MM-dd");
			if (!dateString.equals(null) && !dateString.equals("") && !dateString.equals(formatDate)) {
				date = dateFormat.parse(dateString);
				formatDate = pattern.format(date);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return formatDate;
	}

	
	
	
	/*---------------------SQL TO STRING DATE WITH TIME FORMAT--------------------*/
	public static String formatStringDateTime(String dateString) {
		String formatDate = "";
		java.util.Date date = null;
		// System.out.println("input"+dateString);
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat pattern = new SimpleDateFormat("dd/MM/yyyy");
			if (!dateString.equals(null) && !dateString.equals("") && !dateString.equals("0000-00-00 00:00:00")) {
				date = dateFormat.parse(dateString);
				formatDate = pattern.format(date);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return formatDate;
	}
	
	
	/*---------------------SQL TO STRING ONLY DATE FORMAT--------------------*/
	public static String formatStringDate(String dateString) {
		String formatDate="";
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat pattern = new SimpleDateFormat("dd/MM/yyyy");
			if (!dateString.equals(null) && !dateString.equals("") && !dateString.equalsIgnoreCase("0000-00-00")) {
				Date date = dateFormat.parse(dateString);
				formatDate = pattern.format(date);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return formatDate;
	}
	
	
	/* Geting the No of Days Between Two Dates */
	public static int noofdays(String dateStart, String dateStop) {
		long diffDays = 0;
		// HH converts hour in 24 hours format (0-23), day calculation
		SimpleDateFormat format = new SimpleDateFormat("DD/MM/yyyy");
		java.util.Date d1 = null;
		java.util.Date d2 = null;
		try {
			d1 = format.parse(dateStart);
			d2 = format.parse(dateStop);
			long diff = d2.getTime() - d1.getTime();
			diffDays = diff / (24 * 60 * 60 * 1000);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return (int) (diffDays + 1);
	}

	

	/* Formating The Date ie 12/12/2014 to 12/DEC/2014 */
	public static String formatMonthDate(String date) {
		if (!date.equalsIgnoreCase(null) && !date.equalsIgnoreCase("")) {
			String[] dateArray = date.split("/");
			String day = dateArray[0];
			String month = dateArray[1];
			String year = dateArray[2];

			if (month.equalsIgnoreCase("01")) {
				month = "JAN";
			} else if (month.equalsIgnoreCase("02")) {
				month = "FEB";
			} else if (month.equalsIgnoreCase("03")) {
				month = "MAR";
			} else if (month.equalsIgnoreCase("04")) {
				month = "APR";
			} else if (month.equalsIgnoreCase("05")) {
				month = "MAY";
			} else if (month.equalsIgnoreCase("06")) {
				month = "JUN";
			} else if (month.equalsIgnoreCase("07")) {
				month = "JUL";
			} else if (month.equalsIgnoreCase("08")) {
				month = "AUG";
			} else if (month.equalsIgnoreCase("09")) {
				month = "SEP";
			} else if (month.equalsIgnoreCase("10")) {
				month = "OCT";
			} else if (month.equalsIgnoreCase("11")) {
				month = "NOV";
			} else if (month.equalsIgnoreCase("12")) {
				month = "DEC";
			}
			date = day + "/" + month + "/" + year;
		}
		return date;
	}

	/* Formating The Date ie 12/12/2014 to 12/DEC/2014 */
	public static String formatMonth(int month) {
		String stringMonth = null;
		if (month == 01) {
			stringMonth = "JAN";
		} else if (month == 2) {
			stringMonth = "FEB";
		} else if (month == 3) {
			stringMonth = "MAR";
		} else if (month == 4) {
			stringMonth = "APR";
		} else if (month == 5) {
			stringMonth = "MAY";
		} else if (month == 6) {
			stringMonth = "JUN";
		} else if (month == 7) {
			stringMonth = "JUL";
		} else if (month == 8) {
			stringMonth = "AUG";
		} else if (month == 9) {
			stringMonth = "SEP";
		} else if (month == 10) {
			stringMonth = "OCT";
		} else if (month == 11) {
			stringMonth = "NOV";
		} else if (month == 12) {
			stringMonth = "DEC";
		}

		return stringMonth;
	}
	
	
	public static String formatBillDate(String billDate) {

		try {
			if (!billDate.equals("") && !billDate.equals(null)) {
				SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
				SimpleDateFormat format2 = new SimpleDateFormat("dd-MMM-yyyy");
				Date date = format1.parse(billDate);
				billDate = format2.format(date);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return billDate;
	}
}
