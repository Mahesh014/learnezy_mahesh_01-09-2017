package com.itech.elearning.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {
	/*Matching the String Having Only Characters using RegExpersion*/
	public static Boolean regexmatcherForCharecter(String INPUT){
		Boolean results=false;
		String CHAR_REGEX = "[A-Za-z]";
		if(!INPUT.equals("")&&!INPUT.equals(null)){
			Pattern p = Pattern.compile(CHAR_REGEX);
			Matcher m = p.matcher(INPUT);
			if(m.find()) {
			results=true;
		 }
	   }
		return results;
	}
	
	 /*Removing the Last Char from a String */
	 public static String removeLastChar(String INPUT,String removingChar) {
	    if(!INPUT.equals("")&&!INPUT.equals(null)){
	    	 INPUT = INPUT.trim();
	    	 if(INPUT.substring(INPUT.length() - 1).equalsIgnoreCase(removingChar.trim())){
		         	INPUT=INPUT.substring(0,INPUT.length() - 1).trim();
		      }
	     }
		return INPUT; 
	 }
	 
	 /*Trimming the String based on NULL */
	 public static String trimValue(String INPUT) {
		if(!INPUT.equals("")&&!INPUT.equals(null)){
		    INPUT=INPUT.trim();
		}
		return INPUT; 
	 }
	 
	 /*Replace Single Or Multiple spaces with underscore '_' */
	 public static String repStrUnScr(String input) {
		if(!input.equals("")&&!input.equals(null)){		
			input = input.replaceAll("\\s+","_");
		}
		return input; 
	 }
	 
	 public static String sapratedByComa(String[] input) {
			StringBuffer  returnValue = new StringBuffer();
			for (int i = 0; i < input.length; i++) {
				returnValue.append(input[i]);
				if (i<input.length-1) {
					returnValue.append(",");
				}
			}
			return returnValue.toString();
		}
	
	 
}
