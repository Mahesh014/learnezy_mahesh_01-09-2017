package com.itech.elearning.utils;

import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class FuctionMailClass extends Authenticator {
	
 
	private String gmailUserName = "trainee@itechsolutions.in";// User mailid
	private String gmailPassword = "Itech2016";// user password

	public void postMail(String recipients[], String subject, String message, String from) throws MessagingException {
		boolean debug = false;
		try {
			Properties props = new Properties();
			props.setProperty("mail.transport.protocol", "smtp");
			props.setProperty("mail.host", "smtp.gmail.com");
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.port", "465");
			props.put("mail.smtp.socketFactory.port", "465");
			props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
			props.put("mail.smtp.socketFactory.fallback", "false");
			props.setProperty("mail.smtp.quitwait", "false");
			Session session = Session.getDefaultInstance(props, this);
			session.setDebug(debug);
			Message msg = new MimeMessage(session);
			InternetAddress addressFrom = new InternetAddress(from);
			msg.setFrom(addressFrom);
			InternetAddress[] addressTo = new InternetAddress[recipients.length];
			for (int i = 0; i < recipients.length; i++) {
				addressTo[i] = new InternetAddress(recipients[i]);
			}
			msg.setRecipients(Message.RecipientType.TO, addressTo);
			msg.setSubject(subject);
			msg.setContent(message, "text/html");
			Transport.send(msg);
			System.out.println("SSSSSSss");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(gmailUserName, gmailPassword);
	}

	public static void main(String[] args) throws MessagingException {
		FuctionMailClass ff=new FuctionMailClass();
		String[] recipients={"trainee@itechsolutions.in"};
		String subject="test mail";
		String message="Thanking For Registering with LearnEZY <br><br>"+
"<strong>Student Details:</strong><br>"+
"<table width="+100+"% border="+0+" style="+"background:#81F7D8"+">"+
"<tr><td><strong>Name:</strong> Yaseen </td><td>Invoice No:1233 </td></tr>"+
"<tr><td><strong>Phone No:</strong> 7899319011 </td><td>Invoice Date:12-15-2018 </td></tr>"+
"<tr><td><strong>Email ID:</strong> yaseen@itechsolutions.in </td></tr>"+
"</table><br><br>"+
"<strong>Order Details:</strong><br>"+
"<table width="+100+"% border="+1+" style="+"border: 1px solid black;"+">"+
"<tr align="+"center"+"><td><strong>Sl No </strong></td><td><strong>Course </strong></td></tr>"+
"<tr><td width="+20+"%>1 </td><td>Course 1 </td></tr>"+
"<tr><td>2 </td><td>Course 2 </td></tr>"+
"</table><br><br>"+
"<strong>Grand Total: 2500.00 Rupees</strong>";
		 
		String from="trainee@itechsolutions.in";
		ff.postMail(recipients, subject, message, from);
	}
}
