package com.itech.elearning.utils;

import java.util.Properties;



import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeMessage.RecipientType;

public class MailSendingUtil extends Authenticator{
	static String userName = "trainee@itechsolutions.in";//User mailid
	static String password = "Itech2016";//user password
	
	public boolean postMail(String recipients[], String subject, String message){
		boolean flag=false;
		try {
			boolean debug = false;
			Properties props = new Properties();
			props.setProperty("mail.transport.protocol", "smtp");
			props.setProperty("mail.host", "smtp.gmail.com");
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.port", "465");
			props.put("mail.smtp.socketFactory.port", "465");
			props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
			props.put("mail.smtp.socketFactory.fallback", "false");
			props.setProperty("mail.smtp.quitwait", "false");
			Session session = Session.getDefaultInstance(props, this);
			session.setDebug(debug);
			Message msg = new MimeMessage(session);
			InternetAddress addressFrom = new InternetAddress(userName);
			msg.setFrom(addressFrom);
			InternetAddress[] addressTo = new InternetAddress[recipients.length];
			for (int i = 0; i < recipients.length; i++) {
				addressTo[i] = new InternetAddress(recipients[i]);
			}
			msg.setRecipients(Message.RecipientType.TO, addressTo);
			msg.setSubject(subject);
			msg.setText(message);
			Transport.send(msg);
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	public PasswordAuthentication getPasswordAuthentication() {
		String gmailUserName = userName;//User mailid
		String gmailPassword = password;//user password
		return new PasswordAuthentication(gmailUserName, gmailPassword);
	}
	
	 public static String postAttachedMail(String emailid[],String filepath, String subject, String messageBody, String fileName) {
		  String Result="Mail Sent Successfully";
	      String from = userName;
	      String pass = password;
	      final String username = from;//change accordingly
	      final String password = pass;//change accordingly
	      Properties props = new Properties();
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.socketFactory.port", "465");
			props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.port", "465");
	      System.out.println("1");
	      Session session = Session.getInstance(props,
	         new javax.mail.Authenticator() {
	            protected PasswordAuthentication getPasswordAuthentication() {
	               return new PasswordAuthentication(username, password);
	            }
	         });
	      try {
	         Message message = new MimeMessage(session);
	         message.setFrom(new InternetAddress(from));       
	         InternetAddress[] addressTo = new InternetAddress[emailid.length];
	         for (int i = 0; i < emailid.length; i++)
	         {
	             addressTo[i] = new InternetAddress(emailid[i]);
	         }
	         message.setRecipients(RecipientType.TO, addressTo); 
	         message.setSubject(subject);
	       
	         BodyPart messageBodyPart = new MimeBodyPart();
	         messageBodyPart.setText(messageBody);
	         Multipart multipart = new MimeMultipart();
	         multipart.addBodyPart(messageBodyPart);
	         messageBodyPart = new MimeBodyPart();
	         String filename = filepath;
	         DataSource source = new FileDataSource(filename);
	         messageBodyPart.setDataHandler(new DataHandler(source));
	         messageBodyPart.setFileName(fileName);
	         multipart.addBodyPart(messageBodyPart);
	         message.setContent(multipart);
	         Transport.send(message);           
	      } catch (MessagingException e) {
	         throw new RuntimeException(e);
	      }
		return Result;
	   }
	 public static String LoginMail(String emailid,String pass) {
		  String Result="Mail Sent Successfully";
	      String from = "trainee@itechsolutions.in";
	      final String email1 = "trainee@itechsolutions.in";//change accordingly
	      final String password = "Itech2016";//change accordingly
	      Properties props = new Properties();
			props.put("mail.smtp.host", "smtp.gmail.com");
			
			props.put("mail.smtp.socketFactory.port", "465");
			props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.port", "465");
	      System.out.println("1");
	      Session session = Session.getInstance(props,
	         new javax.mail.Authenticator() {
	            protected PasswordAuthentication getPasswordAuthentication() {
	               return new PasswordAuthentication(email1, password);
	            }
	         });
	      try {
	         Message message = new MimeMessage(session);
	         message.setFrom(new InternetAddress(from));       
             message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(emailid));
	         message.setSubject("Registerd Successfully");
	         message.setText("Dear user successfully registerd to Learnezy. \n username:"+emailid+" \n password:"+pass+"");
	         Transport.send(message);           
	      } catch (MessagingException e) {
	         e.printStackTrace();
	      }
		return Result;
	 }
 public static String enquiryMail(String emailid) {
	  String Result="Mail Sent Successfully";
      String from = "trainee@itechsolutions.in";
      final String email1 = "trainee@itechsolutions.in";//change accordingly
      final String password = "Itech2016";//change accordingly
      Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
      System.out.println("1");
      Session session = Session.getInstance(props,
         new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
               return new PasswordAuthentication(email1, password);
            }
         });
      try {
         Message message = new MimeMessage(session);
         message.setFrom(new InternetAddress(from));       
         message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(emailid));
         message.setSubject("Learnezy");
         message.setText("Dear user thanks for contacting us we will get in touch with you shortly");
         Transport.send(message);           
      } catch (MessagingException e) {
         e.printStackTrace();
      }
	return Result;
 }
 
 public static String contactMail(String emailid) {
	  String Result="Mail Sent Successfully";
     String from = "trainee@itechsolutions.in";
     final String email1 = "trainee@itechsolutions.in";//change accordingly
     final String password = "IteSol1!";//change accordingly
     Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
     System.out.println("1");
     Session session = Session.getInstance(props,
        new javax.mail.Authenticator() {
           protected PasswordAuthentication getPasswordAuthentication() {
              return new PasswordAuthentication(email1, password);
           }
        });
     try {
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(from));       
        message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(emailid));
        message.setSubject("Learnezy");
        message.setText("Dear user thanks for contacting us we will get in touch with you shortly");
        Transport.send(message);           
     } catch (MessagingException e) {
        e.printStackTrace();
     }
	return Result;
}

public static String aftercoursecomplete(String email, String courseName, String passStatus, int obtianPercentage) {
	// TODO Auto-generated method stub
	 String Result="Mail Sent Successfully";
     String from = "trainee@itechsolutions.in";
     final String email1 = "trainee@itechsolutions.in";//change accordingly
     final String password = "Itech2016";//change accordingly
     Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
     System.out.println("1");
     Session session = Session.getInstance(props,
        new javax.mail.Authenticator() {
           protected PasswordAuthentication getPasswordAuthentication() {
              return new PasswordAuthentication(email1, password);
           }
        });
     try {
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(from));       
        message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(email));
        message.setSubject("Learnezy");
        message.setText("Dear user you have given test for course "+courseName+" \n you got "+obtianPercentage+" Percentage and yous result status is "+passStatus+" ");
        Transport.send(message);           
     } catch (MessagingException e) {
        e.printStackTrace();
     }
	return Result;
}
public static String footerenquiryMail(String txtemail) {
	// TODO Auto-generated method stub
	 String Result="Mail Sent Successfully";
     String from = "trainee@itechsolutions.in";
     final String email1 = "trainee@itechsolutions.in";//change accordingly
     final String password = "IteSol1!";//change accordingly
     Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
     System.out.println("1");
     Session session = Session.getInstance(props,
        new javax.mail.Authenticator() {
           protected PasswordAuthentication getPasswordAuthentication() {
              return new PasswordAuthentication(email1, password);
           }
        });
     try {
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(from));       
        message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(txtemail));
        message.setSubject("Learnezy");
        message.setText("Dear user thanks for contacting us we will get in touch with you shortly");
        Transport.send(message);           
     } catch (MessagingException e) {
        e.printStackTrace();
     }
	return Result;
}}
