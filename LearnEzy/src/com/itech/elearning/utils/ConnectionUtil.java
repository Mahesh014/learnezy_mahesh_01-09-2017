package com.itech.elearning.utils;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionUtil {
	static {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}
	}

	public static Connection getMySqlConnection() throws SQLException {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return DriverManager.getConnection("jdbc:mysql://localhost:3306/elearningcom","root", "Itech123!");
	}

	/*-----------------------------------Method Overloading-----------------------------------------*/
	public static void closeResources(Connection con, Statement pstmt,ResultSet rs){
		try {
			if (rs != null) { rs.close(); }
			if (pstmt != null) {	pstmt.close();}
			if (con != null) { con.close();	}	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void closeResources(Connection con, Statement pstmt){
		try {
			if (pstmt != null) {	pstmt.close();}
			if (con != null) { con.close();	}	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void closeResources(ResultSet rs,Statement pstmt){
		try {
			if (rs != null) { rs.close(); }
			if (pstmt != null) {pstmt.close();}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
