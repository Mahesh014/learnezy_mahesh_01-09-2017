package com.itech.elearning.utils;

import java.io.File;
import java.io.FileOutputStream;

import org.apache.struts.upload.FormFile;

public class FileUploadDownloadUtil {
	// File Uploading  on the server
	public static boolean fileUploader(FormFile myfile, String filePath, String fileName) throws Exception {
		boolean uploadStatsus=false;
		if (!fileName.equals("")) {
			File fileToCreate = new File(filePath, fileName); // Create file
			/*if (!fileToCreate.exists()) { // If file does not exists create file
*/				FileOutputStream fileOutStream = new FileOutputStream(fileToCreate);
				fileOutStream.write(myfile.getFileData());
				fileOutStream.flush();
				fileOutStream.close();
				uploadStatsus=true;
			//}
		}
    	// String file = filePath+"/"+fileName;
		return uploadStatsus;
	}
}
