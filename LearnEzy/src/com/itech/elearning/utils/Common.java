package com.itech.elearning.utils;

public class Common {

	public static final String REC_ADDED = "Added successfully";
	public static final String REG_ADDED = "Registered  successfully";
	public static final String REC_UPDATED = "Updated successfully";
	public static final String REC_STATUS_CHANGED = "Status Changed successfully";
	public static final String REC_DELETED = "Deleted successfully";
	public static final String FAILURE_MESSAGE = "Process failed";
	public static final String DUPLICATE_NAME_MESSAGE = "Duplicate Name entry";
	public static final String DUPLICATE_EMAIL_MESSAGE = "Duplicate Email entry";
	public static final String MSG_SENT = "Message Sent successfully";
	public static final String No_Change = "No changes have been made";
	public static final String Session_Exp = "Your Session Expired, Re-Login";
	public static final String DUPLICATE_COURSE_MESSAGE = "Duplicate Course entry";
	public static final String DUPLICATE_TOPIC_MESSAGE = "Duplicate Topic entry";
	public static final String PASSWORD_CHAGED ="Your Password changed successfully ...!";
	public static final String CURRENT_PASS_NOTMATCH = "Current Password Invalid";
	public static final String NEW_CONF_PASS_NOTMATCH="New Password and Confirm Password Does not Match";
	public static final String SAME_NAMES_IN_ALL_FIELDS = "Your Entries are wrong ! Please use the different password";


}
