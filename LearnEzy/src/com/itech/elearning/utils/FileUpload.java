package com.itech.elearning.utils;

import java.io.File;


import java.io.FileOutputStream;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.apache.struts.upload.FormFile;

public class FileUpload {

	public static boolean fileUploader(FormFile myfile, String filePath, String fileName) throws Exception {
		boolean uploadStatus=false;
		// Get the servers upload directory real path name
try {
	// Save file on the server
	if (!fileName.equals("")) {
		
		// Create file
		File fileToCreate = new File(filePath, fileName);

		// If file does not exists create file
		if (!fileToCreate.exists()) {
			FileOutputStream fileOutStream = new FileOutputStream(fileToCreate);
			fileOutStream.write(myfile.getFileData());
			fileOutStream.flush();
			fileOutStream.close();
			uploadStatus=true;
		}
	}
	
} catch (Exception e) {
	e.printStackTrace();
}
return uploadStatus;
}
	public static Date formatDate(String dateString) {
		Date eventtimedate = null;
		java.util.Date date = null;
		try {
			/*DateFormat pattern = new SimpleDateFormat("dd/MM/yyyy hh:mm");
			SimpleDateFormat dateFormat  = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			if (dateString != null & dateString != "") {
				date = dateFormat.parse(dateString);
				formatDate = pattern.format(date);
				System.out.println(formatDate);
			}*/
			
			
			DateFormat fformatter = new SimpleDateFormat("MM/dd/yyyy");
			java.util.Date eventdate = (java.util.Date) fformatter.parse(dateString);
			  eventtimedate = new Date(eventdate.getTime());
		} catch (Exception e) {
			 //log.error(e.getMessage());
			e.printStackTrace();
		}
		return eventtimedate;
	}
	public static String formatSqlDateTime(String dateString) {
		String formatDate = "0000-00-00 00:00:00";
		java.util.Date date = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat pattern = new SimpleDateFormat("yyyy-MM-dd HH:MM:SS");
			if (!dateString.equals(null) && !dateString.equals("") && !dateString.equals(formatDate)) {
				date = dateFormat.parse(dateString);
				formatDate = pattern.format(date);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return formatDate;
	}
}
