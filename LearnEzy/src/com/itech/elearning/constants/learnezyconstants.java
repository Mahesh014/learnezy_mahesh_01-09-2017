package com.itech.elearning.constants;

public class learnezyconstants {
	

	public class UniqueConstants {
		public static final String FAILURE_MESSAGE = "Process failed..!";
		public static final String USERNAME_NOT_AVAILBALE = "username already exist, please try with other username";
		public static final String REC_ADDED="Added successfully";
		public static final String INVALID_USERNAME_PASSWORD ="Invalid Username or Password";
		public static final String DUPLICATE_NAME_MESSAGE ="Details Already Exists, Add new";
		public static final String No_Change ="No changes have been made";
		public static final String REC_UPDATED="Updated Successfully";
		public static final String REC_STATUS_CHANGED="Status Changed successfully";
		public static final String REC_DELETED="Deleted successfully";	
		public static final String MSG_SENT="Message Sent successfully";
		public static final String Rec_display ="You are Successfully Registered";
		public static final String ENTER_EMAIL ="Invalid Email-ID";
		public static final String CHECK_EMAIL ="Please check your Email: ";
		public static final String PASSWORD_CHAGED ="Your Password changed successfully ...!";
		public static final String EXCEL_UPLOAD ="  file successfully uploaded....!";
		public static final String REC_NOT_ADDED = "Record Is Not Added";
		public static final String DIVL_NOT_COURIER = "Not Have Couriers for delivery";
		public static final String REPORT_DOWNLOAD = "Report downloaded successfully, Please save it";
		public static final String REPORT_NO_AVL = "Reports not available on this selected date";
		public static final String INVOICE_GENRARTED_SCUCC = "Invoice genrated successfully";
		public static final String DUPLICATE_INVOICE = "Invoice  Already Exists";
		public static final String DUPLICATE_GLOBAL = "Global Name Exists, Please Enter other Name ! ";
		public static final String NEW_CONF_PASS_NOTMATCH="New Password and Confirm Password Does not Match";
		public static final String SAME_NAMES_IN_ALL_FIELDS = "Your Entries are wrong ! Please use the different password";
		public static final String CURRENT_PASS_NOTMATCH = "Current Password Invalid";
		public static final String BILLS_NOT_AVIL = "Bills not available";
		public static final String EXCEL_DOWN ="Save the excel file!";
		public static final String NO_RECORDS="Records not available";	
		public static final String PASS_DONOTMATCH="Old Password do not match. Please Reenter Old Password";
		public static final String FEEDBACK_RECEIVED="Your feedback received, we will get back you shortly!!";
		public static final String ACCOUNT_ACTIVATE="Your Account has been activated. Please login!!!";
		public static final String MAIL_SENT="Mail sent successfully!!!";
		
		public static final String IMAGES_UPLODED="Images uploaded successfully!!";
		public static final String IMAGES_DELETED="Images deleted successfully";
		
	}

	public static final String CHECK_EMAIL = null;

	public static final String ENTER_EMAIL = null;

	public static String FAILURE_MESSAGE;
	public static String INVALID_USERNAME_PASSWORD;
}
