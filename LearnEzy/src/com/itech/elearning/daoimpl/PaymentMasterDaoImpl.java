package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.sql.DataSource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.elearning.forms.PaymentMasterForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class PaymentMasterDaoImpl {

	@SuppressWarnings("unchecked")
	public List<PaymentMasterForm> list(int roleid,String compid, DataSource dataSource) {
		List<PaymentMasterForm> list = new ArrayList<PaymentMasterForm>();
		PaymentMasterForm paymentForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();
			if(roleid==9)
			{
			 sql = "select * from paymentmaster";
			}
			else
			{
				sql = "select * from paymentmaster";
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				paymentForm = new PaymentMasterForm();
				paymentForm.setPaymentId(rs.getInt("paymentId"));
				paymentForm.setPaymentName(rs.getString("paymentName"));
				paymentForm.setNoOfCourses(rs.getInt("noOfCourse"));
				paymentForm.setAmount(rs.getString("amount"));
				paymentForm.setActive(rs.getBoolean("ACTIVE"));

				JSONObject obj = new JSONObject();
				obj.put("paymentId",rs.getInt("paymentId"));
				obj.put("paymentName",rs.getString("paymentName"));
				obj.put("noOfCourses",rs.getInt("noOfCourse"));
				obj.put("amount",rs.getString("amount"));
				paymentForm.setEditJson(obj.toJSONString());
				list.add(paymentForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return list;
	}

	public String add(String compid, PaymentMasterForm paymentForm, DataSource dataSource) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String sql = "select * from paymentmaster where paymentName=? AND noOfCourse=? AND amount=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, paymentForm.getPaymentName());
			pstmt.setInt(2, paymentForm.getNoOfCourses());
			pstmt.setString(3, paymentForm.getAmount());
			rs = pstmt.executeQuery();
			if (rs.next()) {
				result = Common.DUPLICATE_NAME_MESSAGE;
			} else {
					sql = "insert into paymentmaster(paymentName, noOfCourse, amount, active ) values (?,?,?,?)";
					pstmt = con.prepareStatement(sql);
					pstmt.setString(1, paymentForm.getPaymentName());
					pstmt.setInt(2, paymentForm.getNoOfCourses());
					pstmt.setString(3, paymentForm.getAmount());
					pstmt.setBoolean(4, true);
					pstmt.executeUpdate();
					result = Common.REC_ADDED;
			}
		} catch (Exception e) {

			e.printStackTrace();
		}

		finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return result;
	}

	public String changestatus(DataSource dataSource, boolean active, int paymentId, String compid) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = dataSource.getConnection();
			String sql = "UPDATE paymentmaster SET ACTIVE=? WHERE paymentId=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setBoolean(1, active);
			pstmt.setInt(2, paymentId);
			pstmt.executeUpdate();
			result = Common.REC_STATUS_CHANGED;
		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			ConnectionUtil.closeResources(con, pstmt);
		}
		return result;
	}

	public String update(String compid, PaymentMasterForm paymentForm, DataSource dataSource) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String sql = "select * from paymentmaster where paymentName=? AND noOfCourse=? AND amount=?";
			System.out.println(sql);
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, paymentForm.getPaymentName());
			pstmt.setInt(2, paymentForm.getNoOfCourses());
			pstmt.setString(3, paymentForm.getAmount());
			rs = pstmt.executeQuery();
			if (rs.next()) {
				result = Common.No_Change;
			} else {
				sql = "update paymentmaster set paymentName=?, noOfCourse=?, amount=? where paymentId=?";
				pstmt = con.prepareStatement(sql);
				pstmt.setString(1, paymentForm.getPaymentName());
				pstmt.setInt(2, paymentForm.getNoOfCourses());
				pstmt.setString(3, paymentForm.getAmount());
				pstmt.setInt(4, paymentForm.getPaymentId());
				pstmt.executeUpdate();
				result = Common.REC_UPDATED;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return result;
	}

	public JSONArray courseFees(String courseId, DataSource dataSource) {
		JSONArray jArray= new JSONArray();
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		Connection con=null;
		int questionNo=0;
		 
		try{
			con = dataSource.getConnection();
			String sql = "select * from coursemater where CourseId in ("+courseId+")";
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	        	Date date1 = sdf.parse("2009-12-31");
	        	Date date2 = sdf.parse("2010-01-31");

	        	System.out.println(sdf.format(date1));
	        	System.out.println(sdf.format(date2));
	        	
	        	if(date1.after(date2)){
	        		System.out.println("Date1 is after Date2");
	        	}
	        	
	        	if(date1.before(date2)){
	        		System.out.println("Date1 is before Date2");
	        	}
	        	
	        	if(date1.equals(date2)){
	        		System.out.println("Date1 is equal Date2");
	        	}
				
				
				questionNo=questionNo+1;
				JSONObject obj = new JSONObject();
				obj.put("courseName", rs.getString("CourseName"));
				obj.put("courseId", rs.getString("courseId"));
				obj.put("fees", rs.getString("Fees"));
				obj.put("totalDiscountAmount",rs.getInt("TotalDiscountAmount"));
				obj.put("discountValidUpTo",rs.getString("DiscountValidUpTo"));
				jArray.add(obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}	
		finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
			return jArray;
	}

	public JSONArray courseAcademy(String courseId, DataSource dataSource) {
		JSONArray jArray= new JSONArray();
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		Connection con=null;
		int questionNo=0;
		 
		try{
			con = dataSource.getConnection();
			String sql = "select * from  licensemaster where licenseautoid  in ("+courseId+")";
			
			pstmt = con.prepareStatement(sql);
			System.out.println("vani testing daoimpl "+sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				
			
				
				questionNo=questionNo+1;
				JSONObject obj = new JSONObject();
				obj.put("courseName", rs.getString("licenseName"));
				obj.put("courseId", rs.getString("licenseautoid"));
				obj.put("fees", rs.getString("space"));
				//obj.put("totalDiscountAmount",rs.getInt("TotalDiscountAmount"));
				//obj.put("discountValidUpTo",rs.getString("DiscountValidUpTo"));
				jArray.add(obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}	
		finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
			return jArray;
	}
}
