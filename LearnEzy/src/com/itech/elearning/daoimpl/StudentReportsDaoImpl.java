package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.itech.elearning.forms.StudentReportsForm;
import com.itech.elearning.utils.ConnectionUtil;

public class StudentReportsDaoImpl {

	public List<StudentReportsForm> studentList(int roleid,String compid, Long userid, DataSource dataSource) {
		List<StudentReportsForm> studentList = new ArrayList<StudentReportsForm>();
		StudentReportsForm userData = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();
			if(roleid==9)
			{
			 sql = "SELECT * FROM USERMASTER where roleid=3 ";
			}
			else
			{
				 sql = "SELECT * FROM USERMASTER where roleid=3 and CompanyId = '"+compid+"'";
			}
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				userData = new StudentReportsForm();
				userData.setUserid(rs.getLong("USERID"));
				userData.setFirstName(rs.getString("FIRSTNAME")+" "+rs.getString("LASTNAME"));
				studentList.add(userData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return studentList;
	}

	
	public List<StudentReportsForm> studentDetailsList(int roleid,String compid, Long userid, DataSource dataSource, String studentId, String fromDate, String todate) {
		List<StudentReportsForm> userList = new ArrayList<StudentReportsForm>();
		StudentReportsForm userData = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sqlStudentId="";
		String sqlDate="";
		String sql= null;
		try {
			con = dataSource.getConnection();
			
			if (!studentId.equals("") && !studentId.equals("-1") && !studentId.equals("0")) {
				sqlStudentId= " AND USERID='"+studentId+"' ";
			}
			if (!fromDate.equals("") && !todate.equals("") && !fromDate.equals("0000-00-00") && !todate.equals("0000-00-00")) {
				sqlDate= " AND registerddate between '"+fromDate+"' and '"+todate+"' ";
			}
			String sqlCondition=sqlStudentId+sqlDate;
			//sqlCondition=sqlCondition.substring(4, sqlCondition.length());
			System.out.println(sqlCondition);
			if(roleid==9)
			{
			 sql = "SELECT um.*, GROUP_CONCAT(cm.CourseName) AS courseName FROM USERMASTER um inner join  studentcoursedetails scd on  scd.studentid=um.userid INNER JOIN coursemater cm ON FIND_IN_SET(cm.CourseId,scd.CourseId) where um.roleid=3 "+sqlCondition+"  group by um.userid";
			}
			else
			{
				 sql = "SELECT um.*, GROUP_CONCAT(cm.CourseName) AS courseName FROM USERMASTER um inner join  studentcoursedetails scd on  scd.studentid=um.userid INNER JOIN coursemater cm ON FIND_IN_SET(cm.CourseId,scd.CourseId) where um.roleid=3 "+sqlCondition+" and um.CompanyId = '"+compid+"' group by um.userid";
	
			}
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				userData = new StudentReportsForm();
				userData.setUserid(rs.getLong("USERID"));
				userData.setFirstName(rs.getString("FIRSTNAME")+" "+rs.getString("LASTNAME"));
				userData.setMiddleName(rs.getString("middlename"));
				userData.setLastName(rs.getString("LASTNAME"));
				userData.setQualification(rs.getString("Qualification"));
				userData.setContactno(rs.getString("ContactNumber"));
				userData.setEmailId(rs.getString("EMAILID"));
				userData.setPhone(rs.getString("Phone"));
				userData.setAddress(rs.getString("ADDRESS"));
				userData.setActive(rs.getBoolean("Active"));
				userData.setRegDate(rs.getString("registerddate"));
				userData.setCourseName(rs.getString("courseName"));
				userList.add(userData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return userList;
	}
}
