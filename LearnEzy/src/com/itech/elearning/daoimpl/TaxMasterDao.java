package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.sql.DataSource;

import org.json.simple.JSONObject;

import com.itech.elearning.forms.TaxMasterForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;
 
public class TaxMasterDao {

	@SuppressWarnings("unchecked")
	public List<TaxMasterForm> list(int roleid,String compid, DataSource dataSource) {
		List<TaxMasterForm> list = new ArrayList<TaxMasterForm>();
		TaxMasterForm globalForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();
			if(roleid==9)
			{
		 sql = "select * from taxmaster" ;
			}
			else
			{
			sql = "select * from taxmaster where CompanyId = '"+compid+"'" ;

			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				globalForm = new TaxMasterForm();
				globalForm.setTaxId(rs.getInt("TAXID"));
				globalForm.setTaxName(rs.getString("TAXNAME"));
				globalForm.setTaxValue(rs.getString("TAXPERCENTAGE"));
				globalForm.setActive(rs.getBoolean("ACTIVE"));

				JSONObject obj = new JSONObject();
				obj.put("tId", globalForm.getTaxId());
				obj.put("tName", globalForm.getTaxName());
				obj.put("tValue", globalForm.getTaxValue());
				obj.put("active", globalForm.isActive());
				globalForm.setTaxJson(obj.toJSONString());
				list.add(globalForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return list;
	}

	public String add(String compid, TaxMasterForm globalsettingform, DataSource dataSource) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String sql = "select * from taxmaster where TAXNAME = ? and CompanyId = '"+compid+"'";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, globalsettingform.getTaxName());
			rs = pstmt.executeQuery();
			if (rs.next()) {
				result = Common.DUPLICATE_NAME_MESSAGE;
			} else {
				if (!globalsettingform.getTaxName().equalsIgnoreCase("")
						&& !globalsettingform.getTaxName().equalsIgnoreCase(
								"null")) {
					sql = "insert into taxmaster(TAXID,TAXNAME,TAXPERCENTAGE,ACTIVE,CompanyId ) values (?,?,?,?,?)";
					pstmt = con.prepareStatement(sql);
					pstmt.setInt(1, globalsettingform.getTaxId());
					pstmt.setString(2, globalsettingform.getTaxName());
					pstmt.setString(3, globalsettingform.getTaxValue());
					pstmt.setBoolean(4, true);
					pstmt.setString(5, compid);
					pstmt.executeUpdate();
					result = Common.REC_ADDED;
				}
			}
		} catch (Exception e) {

			e.printStackTrace();
		}

		finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return result;
	}

	public String changestatus(DataSource dataSource, boolean active, int gid, String compid) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = dataSource.getConnection();
			String sql = "UPDATE taxmaster SET ACTIVE=? WHERE TAXID=? and CompanyId = '"+compid+"'";
			pstmt = con.prepareStatement(sql);
			pstmt.setBoolean(1, active);
			pstmt.setInt(2, gid);
			pstmt.executeUpdate();
			result = Common.REC_STATUS_CHANGED;
		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			ConnectionUtil.closeResources(con, pstmt);
		}
		return result;
	}

	public String update(String compid, TaxMasterForm globalsettingform, DataSource dataSource) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String sql = "select * from taxmaster where TAXNAME = ? and TAXPERCENTAGE=? and CompanyId = '"+compid+"'";
			System.out.println(sql);
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, globalsettingform.getTaxName());
			pstmt.setString(2, globalsettingform.getTaxValue());

			rs = pstmt.executeQuery();
			if (rs.next()) {
				result = Common.No_Change;
			} else {
				sql = "update taxmaster set TAXNAME=? ,  TAXPERCENTAGE=? where TAXID = ?";
				pstmt = con.prepareStatement(sql);
				pstmt.setString(1, globalsettingform.getTaxName());
				pstmt.setString(2, globalsettingform.getTaxValue());
				pstmt.setInt(3, globalsettingform.getTaxId());
				pstmt.executeUpdate();
				result = Common.REC_UPDATED;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return result;
	}

	public List<TaxMasterForm> taxList(DataSource dataSource) {
		List<TaxMasterForm> list = new ArrayList<TaxMasterForm>();
		TaxMasterForm globalForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			con = dataSource.getConnection();
			String sql = "select * from taxmaster";
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				globalForm = new TaxMasterForm();
				globalForm.setTaxId(rs.getInt("TAXID"));
				globalForm.setTaxName(rs.getString("TAXNAME"));
				globalForm.setTaxValue(rs.getString("TAXPERCENTAGE"));
				globalForm.setActive(rs.getBoolean("ACTIVE"));

				/*JSONObject obj = new JSONObject();
				obj.put("tId", globalForm.getTaxId());
				obj.put("tName", globalForm.getTaxName());
				obj.put("tValue", globalForm.getTaxValue());
				obj.put("active", globalForm.isActive());
				globalForm.setTaxJson(obj.toJSONString());
				list.add(globalForm);*/
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return list;
	}

	public HashMap<String, String> globasetting( DataSource dataSource) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		HashMap<String, String> hashmap = new HashMap<String, String>();
	    try {
			con=dataSource.getConnection();
			String sql="Select * from taxmaster where Active=1 ";//select query
			pstmt=con.prepareStatement(sql);
			rs=pstmt.executeQuery();
			while(rs.next()){
				 hashmap.put(rs.getString("TAXNAME").toLowerCase().trim(),rs.getString("TAXPERCENTAGE"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return hashmap;
	}

	

}
