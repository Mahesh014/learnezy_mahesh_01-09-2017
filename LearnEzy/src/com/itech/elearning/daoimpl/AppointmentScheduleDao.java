package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.elearning.forms.AppointmentScheduleForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class AppointmentScheduleDao {

	public List<AppointmentScheduleForm> list(int roleid,DataSource dataSource, String compid) {
		List<AppointmentScheduleForm> list = new ArrayList<AppointmentScheduleForm>();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		AppointmentScheduleForm apptForm = null;
		String sql = null;
		try	{
			con=dataSource.getConnection();
			if(roleid==9)
			{
	 sql="select * from appointmentschedule ass inner join appointment a on a.appointmentID = ass.appointmentId inner join coursemater cm on cm.CourseId=ass.courseid inner join usermaster um on um.userid=ass.appointmentCreatedId where ass.companyId="+compid;//select query
			}
			else
			{
		 sql="select * from appointmentschedule ass inner join appointment a on a.appointmentID = ass.appointmentId inner join coursemater cm on cm.CourseId=ass.courseid inner join usermaster um on um.userid=ass.appointmentCreatedId where ass.companyId="+compid;//select query
			
			}
			pstmt=con.prepareStatement(sql);
			rs=pstmt.executeQuery();
			while(rs.next()){
				apptForm = new AppointmentScheduleForm();	
				apptForm.setAppointmentScheduleId(rs.getInt("appointmentScheduleId"));
				apptForm.setAppointmentCreatedID(rs.getInt("appointmentCreatedId"));
				apptForm.setAppointmentId(rs.getInt("appointmentId"));
				apptForm.setCourseid(rs.getInt("courseId"));
				apptForm.setStudentId(rs.getString("studentId"));
				apptForm.setStatus(rs.getString("status"));
				apptForm.setAppointmentType(rs.getString("appointmentType"));
				
				apptForm.setFirstName(rs.getString("FirstName") +" "+ rs.getString("LastName"));
				apptForm.setCourseName(rs.getString("CourseName"));
				apptForm.setAppointmentDateTime(rs.getString("dateofappointment") +" "+ rs.getString("timeofappointment"));
				
				apptForm.setActive(rs.getBoolean("ACTIVE"));
				list.add(apptForm);
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}

		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return list;
	}

	public String add(AppointmentScheduleForm apptForm, DataSource dataSource, Long userid, String compid)
	{
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs =null;
		try
		{
			con=dataSource.getConnection();
			String sql="select * from appointmentschedule where appointmentCreatedId=? AND appointmentId=? AND courseId=? AND studentId=? AND status=? AND appointmentType=? and companyId=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, userid);
			pstmt.setInt(2, apptForm.getAppointmentId());
			pstmt.setInt(3, apptForm.getCourseid());
			pstmt.setString(4, apptForm.getStudentId());
			pstmt.setString(5, apptForm.getStatus());
			pstmt.setString(6, apptForm.getAppointmentType());
			pstmt.setString(7, compid);
			rs = pstmt.executeQuery();
			if(rs.next()) result=Common.DUPLICATE_NAME_MESSAGE;
			else{
					sql="insert into appointmentschedule (appointmentCreatedId, appointmentId, courseId, studentId, status, appointmentType,companyId, ACTIVE) values (?,?,?,?,?,?,?,?)";
					pstmt = con.prepareStatement(sql);
					pstmt.setLong(1, userid);
					pstmt.setInt(2, apptForm.getAppointmentId());
					pstmt.setInt(3, apptForm.getCourseid());
					pstmt.setString(4, apptForm.getStudentId());
					pstmt.setString(5, apptForm.getStatus());
					pstmt.setString(6, apptForm.getAppointmentType());
					pstmt.setString(7, compid);
					pstmt.setBoolean(8, true);
					pstmt.executeUpdate();
					result=Common.REC_ADDED;
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		finally	{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return result;
	}

	public String changeStatus(AppointmentScheduleForm apptForm, DataSource dataSource)
	{
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		try	{
			con=dataSource.getConnection();
			String sql="update appointmentschedule set ACTIVE = ? where appointmentScheduleId=?";
			pstmt=con.prepareStatement(sql);
			pstmt.setBoolean(1,apptForm.isActive());
			pstmt.setInt(2, apptForm.getAppointmentScheduleId());
			pstmt.executeUpdate();
			result=Common.REC_STATUS_CHANGED;
		}
		catch (Exception e){
			System.out.println(e);
		} finally{
			ConnectionUtil.closeResources(con, pstmt);
		}
		return result;
	}

	public String update(AppointmentScheduleForm apptForm, DataSource dataSource) 
	{
		String result=Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		try{
			con=dataSource.getConnection();
			String sql="select * from appointmentschedule where appointmentCreatedId=? AND appointmentId=? AND courseId=? AND studentId=? AND status=? AND appointmentType=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, apptForm.getAppointmentCreatedID());
			pstmt.setInt(2, apptForm.getAppointmentId());
			pstmt.setInt(3, apptForm.getCourseid());
			pstmt.setString(4, apptForm.getStudentId());
			pstmt.setString(5, apptForm.getStatus());
			pstmt.setString(6, apptForm.getAppointmentType());
			rs = pstmt.executeQuery();
			if(rs.next())	result=Common.No_Change;
			else{
					sql="UPDATE appointmentschedule SET appointmentCreatedId=? , appointmentId=? , courseId=? , studentId=? , status=? , appointmentType=? WHERE appointmentScheduleId=?";
					pstmt=con.prepareStatement(sql);
					pstmt.setLong(1, apptForm.getAppointmentCreatedID());
					pstmt.setInt(2, apptForm.getAppointmentId());
					pstmt.setInt(3, apptForm.getCourseid());
					pstmt.setString(4, apptForm.getStudentId());
					pstmt.setString(5, apptForm.getStatus());
					pstmt.setString(6, apptForm.getAppointmentType());
					pstmt.setInt(7, apptForm.getAppointmentScheduleId());
					pstmt.executeUpdate();
					result=Common.REC_UPDATED;
			}
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return result;
	}

 
	public List<AppointmentScheduleForm> activeList(DataSource dataSource){
		List<AppointmentScheduleForm> list = new ArrayList<AppointmentScheduleForm>();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		AppointmentScheduleForm apptForm = null;
		try{
			con=dataSource.getConnection();
			String sql="select * from appointmentschedule where active = true";
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while(rs.next()){
				apptForm = new AppointmentScheduleForm();
				apptForm.setAppointmentScheduleId(rs.getInt("appointmentScheduleId"));
				apptForm.setAppointmentCreatedID(rs.getInt("appointmentCreatedId"));
				apptForm.setCourseid(rs.getInt("courseId"));
				apptForm.setStudentId(rs.getString("studentId"));
				apptForm.setStatus(rs.getString("status"));
				apptForm.setAppointmentType(rs.getString("appointmentType"));
				list.add(apptForm);
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}

		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return list;
	}

	public List<AppointmentScheduleForm> userList(int roleid,String compid,DataSource dataSource) {
		List<AppointmentScheduleForm> studentList = new ArrayList<AppointmentScheduleForm>();
		AppointmentScheduleForm userData = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();
			if(roleid==9)
			{
			 sql = "SELECT * FROM USERMASTER where roleid=3 ";
			}
			else
			{
				 sql = "SELECT * FROM USERMASTER where roleid=3 and CompanyId= '"+compid+"'";

			}
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				userData = new AppointmentScheduleForm();
				userData.setAppointmentCreatedID(rs.getInt("USERID"));
				userData.setFirstName(rs.getString("FIRSTNAME")+" "+rs.getString("LASTNAME"));
				studentList.add(userData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return studentList;
	}
	
	public List<AppointmentScheduleForm> courseList(int roleid,String compid,DataSource ds) {

		List<AppointmentScheduleForm> courseList = new ArrayList<AppointmentScheduleForm>();
		AppointmentScheduleForm courseData = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			if(roleid==9)
			{
			 sql = "SELECT * from coursemater where active=1  ";
			}
			else
			{
			 sql = "SELECT * from coursemater where active=1 and CompanyId = '"+compid+"' ";
	
			}
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				courseData = new AppointmentScheduleForm();
				courseData.setCourseid(rs.getInt("CourseId"));
				courseData.setCourseName(rs.getString("CourseName"));
				courseList.add(courseData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return courseList;

	}

	@SuppressWarnings("unchecked")
	public JSONArray getStudentName(DataSource dataSource, String studentIds) {
		JSONArray jArray = new JSONArray();
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String sql = "SELECT * from usermaster where UserId in ("+studentIds+")";
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				JSONObject obj = new JSONObject();
				obj.put("studentName",rs.getString("FirstName")+" "+ rs.getString("LastName"));
				jArray.add(obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;
	}

}
