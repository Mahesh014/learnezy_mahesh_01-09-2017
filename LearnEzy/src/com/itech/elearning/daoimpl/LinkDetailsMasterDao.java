package com.itech.elearning.daoimpl;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.itech.elearning.forms.LinkDetailsMasterForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class LinkDetailsMasterDao {

	 

	public List<LinkDetailsMasterForm> list(int roleid,String compid, Long userid, DataSource ds) {

		List<LinkDetailsMasterForm> list = new ArrayList<LinkDetailsMasterForm>();
		LinkDetailsMasterForm lForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			if(roleid==9)
			{
			sql = "SELECT * FROM LinkDetailsmaster";
			}
			else
			{
				sql = "SELECT * FROM LinkDetailsmaster where CompanyId = '"+compid+"'";
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				lForm= new LinkDetailsMasterForm();
				lForm.setLinkID(rs.getLong("LinkId"));
				lForm.setURL(rs.getString("URL"));
				lForm.setLinkName(rs.getString("LinkName"));
				lForm.setActive(rs.getBoolean("Active"));
				list.add(lForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		} 
		return list;
	}
	
	public String add(String compid, LinkDetailsMasterForm lForm, DataSource dataSource) {
		String result = Common.FAILURE_MESSAGE;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM LinkDetailsmaster WHERE URL=? and LINKNAME=? and CompanyId = ?";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1,lForm.getURL());
			pstmt.setString(2,lForm.getLinkName());
			pstmt.setString(3,compid);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				result = Common.DUPLICATE_NAME_MESSAGE;
			} else {
				sql = "INSERT INTO LinkDetailsmaster (URL,ACTIVE,LINKNAME,CompanyId) VALUES(?,?,?,?)";
				if (pstmt != null)	pstmt.close();
				pstmt = con.prepareStatement(sql);
				pstmt.setString(1,lForm.getURL());
				pstmt.setBoolean(2, true);
				pstmt.setString(3,lForm.getLinkName());
				pstmt.setString(4,compid);
				pstmt.executeUpdate();
				result = Common.REC_ADDED;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	public String changeStatus(LinkDetailsMasterForm lForm, DataSource dataSource, Long linkId, boolean active) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = dataSource.getConnection();
			String sql = "UPDATE LinkDetailsmaster SET ACTIVE=? WHERE LinkId=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setBoolean(1,active);
			pstmt.setLong(2, linkId);
			pstmt.executeUpdate();
			result = Common.REC_STATUS_CHANGED;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	

	public String update(LinkDetailsMasterForm lForm, DataSource ds) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			sql = "SELECT * FROM LinkDetailsmaster WHERE URL=? and LINKNAME=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1,lForm.getURL());
			pstmt.setString(2,lForm.getLinkName());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				result = Common.No_Change;
			} else {
					sql = "UPDATE LinkDetailsmaster SET URL=?,LINKNAME=? where LinkId=?";
					if (pstmt != null)	pstmt.close();
					pstmt = con.prepareStatement(sql);
					pstmt.setString(1,lForm.getURL());
					pstmt.setString(2,lForm.getLinkName());
					pstmt.setLong(3,lForm.getLinkID());
					pstmt.executeUpdate();
					result = Common.REC_UPDATED;
				 
			} 

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;

	}

	 
}
