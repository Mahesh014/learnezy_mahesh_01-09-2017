package com.itech.elearning.daoimpl;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import com.itech.elearning.forms.StartTestForm;
import com.itech.elearning.utils.ConnectionUtil;

public class StartTestDaoImpl {

	public List<StartTestForm> listAll(String testid, Long userid, String compid, StartTestForm testForm,
			DataSource ds) {

		List<StartTestForm> courseList = new ArrayList<StartTestForm>();
		StartTestForm testData = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		System.out.println("INside the Dao");
		try {
			con = ds.getConnection();
			String sql = "select QuestionId, TestId,TopicID, Question, AnswerA, AnswerB, AnswerC, AnswerD, CorrectAnswer, DescriptiveAns, Active FROM  questionmaster  where TopicID="+testid+" and CompanyId = '"+compid+"' ORDER BY RAND()";
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			System.out.println("NExt to exequery");
			while (rs.next()) {
				testData = new StartTestForm();
				testData.setQuestionId(rs.getInt("QuestionId"));
				testData.setTestId(rs.getLong("TestId"));
				testData.setTopicID(rs.getString("TopicID"));
				testData.setQuestion(rs.getString("Question"));
				testData.setOptionA(rs.getString("AnswerA"));
				testData.setOptionB(rs.getString("AnswerB"));
				testData.setOptionC(rs.getString("AnswerC"));
				testData.setOptionD(rs.getString("AnswerD"));
				testData.setQuestionId(rs.getInt("QuestionId"));
				courseList.add(testData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return courseList;
	}

	public Object checkall(String topicID, Long courseID, Long userid, HttpServletRequest request,String[] Values, String[] IDs, String compid, DataSource dataSource) {
		
		String result="Success";
		int count=0;
		Connection con=null;
		PreparedStatement pstmt=null;
		String answer=null;
		ResultSet rs=null;

		int totalQuestion=IDs.length;
		for(int i=0;i<IDs.length;i++){
			try{
				con=dataSource.getConnection();
				String sql="Select CorrectAnswer from questionmaster where QuestionId='"+IDs[i]+"' and CompanyId = '"+compid+"' ";
				pstmt=con.prepareStatement(sql);
				rs=pstmt.executeQuery();
				while(rs.next())
				{
					answer=rs.getString("CorrectAnswer");
				}
				if(answer.equals(Values[i]))
				{
					count=count+1;				
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			finally {
				try {
					ConnectionUtil.closeResources(con, pstmt, rs);
				} catch (Exception e2) {
					e2.printStackTrace(); 
				}}
		}


		System.out.println("count value"+count);
		request.setAttribute("count", count);
		String sql=null;
		try{
			con=dataSource.getConnection();
			sql="Select TopicID from testanswerscore where TopicID='"+topicID+"' and StudentId='"+userid+"'";
			pstmt=con.prepareStatement(sql);
			rs=pstmt.executeQuery();
			if(rs.next())
			{
				sql="update testanswerscore set Score='"+count+"',UpdatedDate=now() where TopicID='"+topicID+"' and StudentId='"+userid+"'";
				pstmt=con.prepareStatement(sql);
				pstmt.executeUpdate();
				System.out.println(sql);

			}else{
				sql="insert into testanswerscore(StudentId,totalquestion,Score,CourseID,TopicID)values('"+userid+"','"+totalQuestion+"','"+count+"','"+courseID+"','"+topicID+"')";
				pstmt=con.prepareStatement(sql);
				pstmt.executeUpdate();
			}}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace(); 
			}}
		return result;
	}
}