package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.elearning.forms.AreaMasterForm;
import com.itech.elearning.forms.CityForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class AreaMasterDaoImpl
{

	public List<AreaMasterForm> list(int roleid, String compid, long stateid,
			long countryid, long cityid, DataSource dataSource) {
		List<AreaMasterForm> areList = new ArrayList<AreaMasterForm>();
		AreaMasterForm areaform = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql=null;
		try	{
			con=dataSource.getConnection();
			if(roleid==9)
			{
		
				System.out.println("3");
				sql="select am.areaname,am.ACTIVE,am.CITYID ,cm.city , ctm.COUNTRY,am.areaid,cm.cityid,ctm.countryid,sm.STATEID from areamaster am inner join citymaster cm on cm.cityid= am.CITYID  join statemaster sm on sm.stateid=am.stateid inner join countrymaster ctm on ctm.countryid=am.countryid ORDER BY am .AREANAME  ";
				System.out.println("1st "+sql);
				pstmt = con.prepareStatement(sql);
				
			}
		
			else
			{
				
					System.out.println("3");
					sql="select am.areaname , am.ACTIVE ,am.CITYID ,cm.city , ctm.COUNTRY,am.areaid,cm.cityid,ctm.countryid,sm.STATEID from areamaster am inner join citymaster cm on cm.cityid= am.CITYID  join statemaster sm on sm.stateid=am.stateid inner join countrymaster ctm on ctm.countryid=am.countryid ORDER BY am .AREANAME  ";

					System.out.println("2nd "+sql);
					pstmt = con.prepareStatement(sql);
					
				}
		
			rs=pstmt.executeQuery();
			while(rs.next()){
				areaform = new AreaMasterForm();
				areaform.setCityid(rs.getLong("CITYID"));
				areaform.setStateid(rs.getLong("STATEID"));
				areaform.setAreaname(rs.getString("AREANAME"));
				areaform.setCountryid(rs.getLong("COUNTRYID"));
				areaform.setActive(rs.getBoolean("ACTIVE"));
				areaform.setAreaid(rs.getLong("AREAID"));
				areList.add(areaform);
				
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return areList;
	}

	public String add(AreaMasterForm cityForm, String compid, DataSource dataSource) 
	{
		String result = Common.FAILURE_MESSAGE;
	    Connection con = null;
	    PreparedStatement pstmt = null;
	    try
	    {
	    	con=dataSource.getConnection();
			String sql="SELECT * FROM areamaster WHERE AREANAME=? and CITYID=? and stateid=? and countryid=? and AREANAME=? and CompanyId='"+compid+"'";
	    	pstmt = con.prepareStatement(sql);
	    	pstmt.setString(1, cityForm.getAreaname());
	    	pstmt.setLong(2, cityForm.getCityid());
	    	pstmt.setLong(3, cityForm.getStateid());
	    	pstmt.setLong(4, cityForm.getCountryid());
			pstmt.setString(5,cityForm.getAreaname());

	    	ResultSet rs = pstmt.executeQuery();
	    	if(rs.next())result = Common.DUPLICATE_NAME_MESSAGE;
	    	else {
	    		if(!cityForm.getAreaname().equalsIgnoreCase("") && !cityForm.getAreaname().equalsIgnoreCase(null))	{
	    			sql="insert into areamaster(stateid,CITYID,countryid,AREANAME,Active,CompanyId) values (?,?,?,?,?,?)";
	    			pstmt=con.prepareStatement(sql);
	    			pstmt.setLong(1, cityForm.getStateid());
	    			pstmt.setLong(2, cityForm.getCityid());
	    			pstmt.setLong(3, cityForm.getCountryid());
	    			pstmt.setString(4,cityForm.getAreaname());
	    			pstmt.setBoolean(5, true);
	    			pstmt.setString(6,compid);
	    			pstmt.executeUpdate();
	    			result = Common.REC_ADDED;
	    		}
	    	}
	    }
	    catch (Exception e) 
	    {
			e.printStackTrace();
		}
	    finally{
			ConnectionUtil.closeResources(con, pstmt);
		}
		return result;
	}

	public String update(AreaMasterForm cityForm, DataSource dataSource,
			Long userid) {
		String result=Common.FAILURE_MESSAGE;
		Connection con=null;
		PreparedStatement pstmt=null;
		try {
			
			con=dataSource.getConnection();
			String sql="SELECT * FROM areamaster WHERE AREANAME=? and CITYID=? and stateid=? and countryid=?";
			pstmt=con.prepareStatement(sql);			
			pstmt.setString(1, cityForm.getAreaname());
			pstmt.setLong(2, cityForm.getCountryid());
	    	pstmt.setLong(3, cityForm.getStateid());
	    	pstmt.setLong(4,cityForm.getCountryid());
			ResultSet rs=pstmt.executeQuery();
			if(rs.next())result= Common.No_Change;
			else{
				sql="UPDATE areamaster SET AREANAME=?, STATEID=?,countryid=?,CITYID=? WHERE AREAID=?";
				if(pstmt!=null)pstmt.close();
				pstmt=con.prepareStatement(sql);
				pstmt.setString(1, cityForm.getAreaname());
				pstmt.setLong(2, cityForm.getStateid());
				pstmt.setLong(3, cityForm.getCountryid());
				pstmt.setLong(4, cityForm.getCityid());
				pstmt.setLong(5, cityForm.getAreaid());
				System.out.println("vani testing "+cityForm.getAreaid());
				pstmt.executeUpdate();
				result=Common.REC_UPDATED;
				}
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			ConnectionUtil.closeResources(con, pstmt);
		}
		return result;
	}

	public String changeStatus(String areaid,AreaMasterForm cityForm, DataSource dataSource) 
	{
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		try
		{
			con=dataSource.getConnection();
			String sql="UPDATE areamaster SET Active=? WHERE AREAID=?";
			pstmt=con.prepareStatement(sql);
			pstmt.setBoolean(1,cityForm.isActive());
			pstmt.setString(2, areaid);
			pstmt.executeUpdate();
			result = Common.REC_STATUS_CHANGED;
		}
		catch (Exception e) 
		{
		  e.printStackTrace();
		}
		finally{
			ConnectionUtil.closeResources(con, pstmt);
		}
		return result;
	}

	public JSONArray ajaxActiveList(DataSource dataSource, String cityid) {
		JSONArray jArray= new JSONArray();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql=null;
		try{
			con=dataSource.getConnection();
			sql="select * from areamaster WHERE active=1 and CITYID=?";//select query
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1,cityid);
			rs = pstmt.executeQuery();
			while(rs.next()){
				JSONObject obj= new JSONObject();
				obj.put("AREAID",rs.getLong("AREAID"));
				obj.put("AREANAME",rs.getString("AREANAME"));
				jArray.add(obj);
				System.out.println(sql);
			}
		}
		catch (Exception e){
		   e.printStackTrace();
		}
		
		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;
	}

}
