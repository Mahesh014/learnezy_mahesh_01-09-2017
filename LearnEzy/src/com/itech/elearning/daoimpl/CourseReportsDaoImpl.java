package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.itech.elearning.forms.CourseReportsForm;
import com.itech.elearning.utils.ConnectionUtil;

public class CourseReportsDaoImpl {
	public List<CourseReportsForm> listAllCourse(int roleid,String compid, Long userid,DataSource dataSource) {
	List<CourseReportsForm> courseList = new ArrayList<CourseReportsForm>();
	CourseReportsForm cForm = null;
	Connection con = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	String sql = null;
	try {
		con = dataSource.getConnection();
		if(roleid==9)
		{
	         sql = "SELECT * FROM coursemater WHERE ACTIVE=TRUE";
		}
		else
		{
			 sql = "SELECT * FROM coursemater WHERE ACTIVE=TRUE and CompanyId = '"+compid+"'";
		}
		pstmt = con.prepareStatement(sql);
		rs = pstmt.executeQuery();
		System.out.println(sql);
		while (rs.next()) {
			cForm = new CourseReportsForm();
			cForm.setCourseId(rs.getLong("CourseId"));
			cForm.setCourseName(rs.getString("CourseName"));
			courseList.add(cForm);
		}
	} catch (Exception e) {
		e.printStackTrace();
	} finally {
		try {
			ConnectionUtil.closeResources(con, pstmt, rs);
		} catch (Exception e2) {
		}
	}
	return courseList;
	}

	public List<CourseReportsForm> courseDetialsList(int roleid,String compid, Long userid, DataSource dataSource, String courseId, String fromDate, String todate) {
		List<CourseReportsForm> userList = new ArrayList<CourseReportsForm>();
		CourseReportsForm userData = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sqlcourseId="";
		String sqlDate="";
		String sql = null;
		try {
			con = dataSource.getConnection();
			if (!courseId.equals("") && !courseId.equals("-1") && !courseId.equals("0")) {
				sqlcourseId= " AND cm.courseid='"+courseId+"' ";
			}
			if (!fromDate.equals("") && !todate.equals("") && !fromDate.equals("0000-00-00") && !todate.equals("0000-00-00")) {
				sqlDate= " AND um.registerddate between '"+fromDate+"' and '"+todate+"' ";
			}
			String sqlCondition=sqlcourseId+sqlDate;
			//sqlCondition=sqlCondition.substring(4, sqlCondition.length());
			System.out.println(sqlCondition);
			if(roleid==9)
			{
			
		 sql = "SELECT count(*) AS noofstudentTakenCourse,cm.courseName FROM  studentcoursedetails scd inner join usermaster um on um.userid=scd.studentid  INNER JOIN coursemater cm ON FIND_IN_SET(cm.CourseId,scd.CourseId) where um.roleid=3 "+sqlCondition+"  group by cm.coursename";
			}
			else
			{
				sql = "SELECT count(*) AS noofstudentTakenCourse,cm.courseName FROM  studentcoursedetails scd inner join usermaster um on um.userid=scd.studentid  INNER JOIN coursemater cm ON FIND_IN_SET(cm.CourseId,scd.CourseId) where um.roleid=3 "+sqlCondition+" and um.CompanyId = '"+compid+"' group by cm.coursename";
			}
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				userData = new CourseReportsForm();
				userData.setUserid(rs.getLong("noofstudentTakenCourse"));
				userData.setCourseName(rs.getString("cm.courseName"));
				userList.add(userData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return userList;
	}
	 
}
