package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

import com.itech.elearning.forms.ForgotPasswordForm;
import com.itech.elearning.utils.EncryptUtil;

public class ForgotPasswordDaoImpl {

	public String forgotPassword(ForgotPasswordForm forgotPasswordForm,
			DataSource ds) {
		String pass = null;
		System.out.println("Inside Forgot Password Action");

		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = ds.getConnection();
			if (forgotPasswordForm.getEmail() != null) {
				String sql = "SELECT PASSWORD FROM USERMASTER WHERE EMAILID='"+ forgotPasswordForm.getEmail() + "'";
				pstmt = con.prepareStatement(sql);
				ResultSet rs = pstmt.executeQuery();
				while (rs.next()) {
					pass = EncryptUtil.decrypt(rs.getString("password"));

				}
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			try {
				if (con != null)
					con.close();
			} catch (Exception e2) {

			}
		}
		return pass;
	}

}
