package com.itech.elearning.daoimpl;

import java.io.BufferedInputStream;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import javax.servlet.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFPictureData;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.PictureData;
import org.apache.poi.ss.usermodel.Row;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import javax.mail.Part;
import javax.servlet.ServletOutputStream;

import javax.servlet.http.HttpSession;


import com.itech.elearning.forms.ContantMasterForm;
import com.itech.elearning.forms.CourseMasterForm;
import com.itech.elearning.forms.CourseOfferedForm;
import com.itech.elearning.forms.SubTopicMasterForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;
import com.itech.elearning.utils.DateTimeUtil;
import org.apache.poi.hssf.usermodel.HSSFPictureData;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.PictureData;
import org.apache.poi.ss.usermodel.Row;

public class CourseMasterDaoImpl {

	public List<CourseMasterForm> listAll(int roleid,String compid,Long userid, DataSource ds) {

		List<CourseMasterForm> courseList = new ArrayList<CourseMasterForm>();
		CourseMasterForm courseData = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			if(roleid==9)
			{
		 sql = "SELECT * from coursemater";
			}
			else
			{
				sql = "SELECT * from coursemater where CompanyId='"+compid+"'";
			}
			pstmt = con.prepareStatement(sql);
		//	pstmt.setString(1, compid);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				courseData = new CourseMasterForm();
				courseData.setCourseId(rs.getLong("CourseId"));
				courseData.setCourseName(rs.getString("CourseName"));
				courseData.setFees(rs.getLong("fees"));
				courseData.setMaxStd(rs.getInt("MaxStudents"));
				courseData.setDuration(rs.getInt("CourseDuration"));
				courseData.setActive(rs.getBoolean("Active"));
				courseData.setLevel(rs.getString("level"));

				courseData.setPasspercent(rs.getString("passpercent"));
				courseData.setTotalDiscountAmount(rs.getInt("TotalDiscountAmount"));
				courseData.setDiscountValidUpTo(DateTimeUtil.formatStringDateTime(rs.getString("DiscountValidUpTo")));
				courseData.setDiscountValidUpToDisplay(DateTimeUtil.formatStringDate(rs.getString("DiscountValidUpTo")));
				courseData.setCoursecategoryId(rs.getInt("coursecatId"));
				courseData.setInstructorname(rs.getString("instructorname"));
				System.out.println("instructorname"+rs.getString("instructorname"));
				courseData.setAvailableseats(rs.getString("availableseats"));
				courseData.setCoursestartdate(rs.getString("coursestartingdate"));
				courseData.setCourseimage(rs.getString("imagename"));
				courseData.setInstructorimages(rs.getString("instructorimage"));
				int free=rs.getInt("free");
				int classroom=rs.getInt("classroomtraining");
				int elearning=rs.getInt("elearning");

				if(elearning==1)
				{
					courseData.setElearning("elearn");
				}
				if(classroom==1)
				{
					
					courseData.setElearning("classroom");
				}
				if(free==1)
				{
					
					courseData.setFree("free");

				}

				courseList.add(courseData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return courseList;

	}
	public List<CourseMasterForm> courseListByCourseIds(String courseList, DataSource dataSource) {

		List<CourseMasterForm> list = new ArrayList<CourseMasterForm>();
		CourseMasterForm courseData = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String sql = "select * FROM coursemater where CourseId in ("+courseList+"); ";
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()){
				courseData= new CourseMasterForm();
				courseData.setCourseName(rs.getString("CourseName"));
				courseData.setFees(rs.getLong("fees"));
				courseData.setTotalDiscountAmount(rs.getInt("TotalDiscountAmount"));
				list.add(courseData);
			}
			 
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return list;

	}
	public String addCourse(CourseMasterForm courseForm, String compid, String imagefileName, String imagefileName1, DataSource ds) {
		String result = Common.FAILURE_MESSAGE;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			java.sql.Timestamp ts = new java.sql.Timestamp(System.currentTimeMillis());
		    Timestamp ftimets = new Timestamp(ts.getTime());

			con = ds.getConnection();
			String sql1 = "SELECT * FROM coursemater WHERE CourseName=? and CompanyId = '"+compid+"'";
			pstmt = con.prepareStatement(sql1);
			pstmt.setString(1, courseForm.getCourseName());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				result = Common.DUPLICATE_COURSE_MESSAGE;
			} else {
				String sql = "INSERT INTO coursemater (CourseName,fees,MaxStudents,CourseDuration,ACTIVE,passpercent,totalDiscountAmount,DiscountValidUpTo,CompanyId,addeddate,coursecatId,imagename,classroomtraining,elearning,free,level,instructorimage,instructorname,availableseats,coursestartingdate) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				if (pstmt != null)
					pstmt.close();
				pstmt = con.prepareStatement(sql);
				System.out.println(sql);
				System.out.println("vani "+courseForm.getCourseofferedname());
				pstmt.setString(1, courseForm.getCourseofferedname());
				pstmt.setLong(2, courseForm.getFees());
				pstmt.setInt(3, courseForm.getMaxStd());
				pstmt.setLong(4, courseForm.getDuration());
				pstmt.setBoolean(5, true);
				pstmt.setString(6, courseForm.getPasspercent());
				pstmt.setInt(7, courseForm.getTotalDiscountAmount());
				pstmt.setString(8, DateTimeUtil.formatSqlDate(courseForm.getDiscountValidUpTo()));
				pstmt.setString(9, compid);
				pstmt.setTimestamp(10,ftimets);
				pstmt.setInt(11,courseForm.getCoursecategoryId());
				pstmt.setString(12,imagefileName);
				pstmt.setString(13,courseForm.getClassroom());
				System.out.println("Classroommmmmmmmmm"+courseForm.getClassroom());
				
				pstmt.setString(14, courseForm.getElearning());
				System.out.println("Elearningggggggggg"+courseForm.getElearning());

				pstmt.setString(15,courseForm.getFree());
				pstmt.setString(16,courseForm.getLevel());
				pstmt.setString(17,imagefileName1);
				pstmt.setString(18,courseForm.getInstructorname());
				pstmt.setString(19,courseForm.getAvailableseats());
				pstmt.setString(20,courseForm.getCoursestartdate());
				pstmt.executeUpdate();
				result = Common.REC_ADDED;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	public String changeSatatus(String compid, CourseMasterForm courseForm, DataSource ds,
			long userId) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = ds.getConnection();
			String sql = "UPDATE coursemater SET ACTIVE=? WHERE CourseId=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setBoolean(1, courseForm.isActive());
			pstmt.setLong(2, userId);
			pstmt.executeUpdate();
			result = Common.REC_STATUS_CHANGED;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return result;

	}

	public CourseMasterForm get(long id, DataSource ds) {
		CourseMasterForm courseData = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ds.getConnection();
			String sql = "SELECT CourseId, CourseName,fees ,MaxStudents , CourseDuration,Active,passpercent,TotalDiscountAmount,DiscountValidUpTo FROM  coursemater where CourseId = ? ";
			System.out.println(sql);
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, id);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				courseData = new CourseMasterForm();
				courseData.setCourseId(rs.getLong("CourseId"));
				courseData.setCourseName(rs.getString("CourseName"));
				courseData.setFees(rs.getLong("fees"));
				courseData.setMaxStd(rs.getInt("MaxStudents"));
				courseData.setDuration(rs.getInt("CourseDuration"));
                courseData.setPasspercent(rs.getString("passpercent"));
                courseData.setTotalDiscountAmount(rs.getInt("TotalDiscountAmount"));
                courseData.setDiscountValidUpTo(DateTimeUtil.formatSqlDate(rs.getString("DiscountValidUpTo")));
           }
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return courseData;
	}

	public String updateCourse(CourseMasterForm courseForm, DataSource ds, String imagefileName, String imagefileName1) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			sql = "SELECT * FROM coursemater WHERE CourseName =? AND fees =? AND MaxStudents=? and CourseDuration =? and passpercent=? and TotalDiscountAmount=? and DiscountValidUpTo=? and coursecatId=? and instructorname=? and availableseats=? and coursestartingdate=? and imagename=? and instructorimage=? and classroomtraining=? and elearning=? and free=? ";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1,  courseForm.getCourseofferedname());
			System.out.println("course name in dao"+courseForm.getCourseofferedname());
			pstmt.setLong(2, courseForm.getFees());
			pstmt.setInt(3, courseForm.getMaxStd());
			pstmt.setLong(4, courseForm.getDuration());
			pstmt.setString(5, courseForm.getPasspercent());
			pstmt.setInt(6, courseForm.getTotalDiscountAmount());
			pstmt.setString(7, DateTimeUtil.formatSqlDate(courseForm.getDiscountValidUpTo()));
			pstmt.setInt(8,courseForm.getCoursecategoryId());
			pstmt.setString(9, courseForm.getInstructorname());
			pstmt.setString(10, courseForm.getAvailableseats());
			pstmt.setString(11, courseForm.getCoursestartdate());
			pstmt.setString(12, imagefileName);
			pstmt.setString(13, imagefileName1);
			pstmt.setString(14, courseForm.getClassroom());
			System.out.println("Classroommmmmmmmmm"+courseForm.getClassroom());

			pstmt.setString(15, courseForm.getElearning());
			System.out.println("Elearningggggggggg"+courseForm.getElearning());

			pstmt.setString(16, courseForm.getFree());
			System.out.println("classroomtraining"+ courseForm.getClassroom());
			System.out.println("elaerning"+ courseForm.getElearning());







			rs = pstmt.executeQuery();
			if (rs.next()) { result = Common.No_Change; }
			else {
				sql = "UPDATE coursemater SET CourseName  = ?,fees = ?,MaxStudents = ?,CourseDuration = ?, passpercent = ?, TotalDiscountAmount=? , DiscountValidUpTo=?,coursecatId=?,instructorname=?,availableseats=?,coursestartingdate=?,imagename=?,instructorimage=?,classroomtraining=?,elearning=?,free=?  where CourseId=? ";
				pstmt = con.prepareStatement(sql);
				pstmt.setString(1, courseForm.getCourseofferedname());
				pstmt.setLong(2, courseForm.getFees());
				pstmt.setInt(3, courseForm.getMaxStd());
				pstmt.setInt(4, courseForm.getDuration());
				pstmt.setString(5, courseForm.getPasspercent());
				pstmt.setInt(6, courseForm.getTotalDiscountAmount());
				pstmt.setString(7, DateTimeUtil.formatSqlDate(courseForm.getDiscountValidUpTo()));
				pstmt.setInt(8,courseForm.getCoursecategoryId());
				pstmt.setString(9, courseForm.getInstructorname());
				pstmt.setString(10, courseForm.getAvailableseats());
				pstmt.setString(11, courseForm.getCoursestartdate());
				pstmt.setString(12, imagefileName);
				pstmt.setString(13, imagefileName1);
				pstmt.setString(14, courseForm.getClassroom());
				pstmt.setString(15, courseForm.getElearning());
				pstmt.setString(16, courseForm.getFree());

                pstmt.setDouble(17, courseForm.getCourseId());



				//pstmt.setLong(9, courseForm.getCourseId());
				pstmt.executeUpdate();
				result = Common.REC_UPDATED;
				
			}	

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;
	}
	
	public String getCourseNameByID(long CourseId, DataSource ds) {
		String courseName = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ds.getConnection();
			String sql = "SELECT CourseName FROM coursemater where CourseId = ? ";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, CourseId);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				courseName=rs.getString("CourseName");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
			
		}
		return courseName;
	}

	public String getCourseName(Long userid, Long courseId,	DataSource dataSource) {
		 
		return null;
	}

	@SuppressWarnings("unchecked")
	public JSONArray courseFees(String courseId, DataSource dataSource) {
		JSONArray jArray= new JSONArray();
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		Connection con=null;
		int questionNo=0;
		 
		try{
			con = dataSource.getConnection();
			String sql = "select * from coursemater where CourseId in ("+courseId+")";
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	        	Date date1 = sdf.parse("2009-12-31");
	        	Date date2 = sdf.parse("2010-01-31");

	        	System.out.println(sdf.format(date1));
	        	System.out.println(sdf.format(date2));
	        	
	        	if(date1.after(date2)){
	        		System.out.println("Date1 is after Date2");
	        	}
	        	
	        	if(date1.before(date2)){
	        		System.out.println("Date1 is before Date2");
	        	}
	        	
	        	if(date1.equals(date2)){
	        		System.out.println("Date1 is equal Date2");
	        	}
				
				
				questionNo=questionNo+1;
				JSONObject obj = new JSONObject();
				obj.put("courseName", rs.getString("CourseName"));
				obj.put("courseId", rs.getString("courseId"));
				obj.put("fees", rs.getString("Fees"));
				obj.put("totalDiscountAmount",rs.getInt("TotalDiscountAmount"));
				obj.put("discountValidUpTo",rs.getString("DiscountValidUpTo"));
				jArray.add(obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}	
		finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
			return jArray;
	}
	
	
	 public static String StringNotNull(HSSFCell rowValue){
		   String results="";
		   if(rowValue!=null) results =rowValue.toString().trim();
		   return results;
		   
	   }
	public List<CourseMasterForm> listAllcategory(int roleid, String compid,
			Long userid, DataSource ds) {
		List<CourseMasterForm> courseList = new ArrayList<CourseMasterForm>();
		CourseMasterForm courseData = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			if(roleid==9)
			{
		 sql = "SELECT coursecatname,coursecatId from coursecategorymaster";
			}
			else
			{
				sql = "SELECT coursecatname,coursecatId from coursecategorymaster where CompanyId='"+compid+"'";
			}
			pstmt = con.prepareStatement(sql);
		//	pstmt.setString(1, compid);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				courseData = new CourseMasterForm();
				courseData.setCoursecategoryId(rs.getInt("coursecatId"));
				courseData.setCoursecategoryname(rs.getString("coursecatname"));
			

				courseList.add(courseData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return courseList;
	}
	public List<CourseMasterForm> listCourseOffered(
			CourseMasterForm courseForm, int roleid, String compid,
			Long userid, DataSource ds)
			{
		List<CourseMasterForm> courseList = new ArrayList<CourseMasterForm>();
		CourseMasterForm topicData = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			if(roleid==9)
			{
			 sql = "SELECT * from courseofferedmaster where active=true  and  coursecatId= '"+courseForm.getCoursecategoryId()+"'  " ;
			
			
			}
			else
			{
				 sql = "SELECT * from courseofferedmaster where active=true and  CompanyId='"+compid+"' and  coursecatId= '"+courseForm.getCoursecategoryId()+"'  ";
				
			}
			System.out.println(sql);
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				topicData = new CourseMasterForm();
				topicData.setCourseofferedid(rs.getString("CourseofferedId"));
				topicData.setCourseofferedname(rs.getString("CourseofferedName"));
				courseList.add(topicData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return courseList;
	      }
	public JSONArray getcourse(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String catid) {
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
       JSONObject obj=null;

		
		try {
			con=dataSource.getConnection();      
			String sql="select CourseofferedName from  courseofferedmaster where Active=1 and coursecatId="+catid+"";

			pstmt=con.prepareStatement(sql);
			rs=pstmt.executeQuery();	
			while(rs.next()){
					obj=new JSONObject(); 
				obj.put("CourseofferedName", rs.getString("CourseofferedName"));
				
					jArray.add(obj);
				}
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;

}
	public String excelUpload(DataSource dataSource, List<CourseMasterForm> ld,
			String filename, int n, int rowNo, int countofvalues,
			int totalcount, String compid) {
		String result=Common.FAILURE_MESSAGE;		   
		Connection con=null;
		   PreparedStatement pstmt=null;
		   boolean flag=false;
		   ResultSet rs=null;
		   int no=5;
		   int temp=6;
		   String sql="";
		   String roleName="";
		   boolean res=false;
		  String catName=null;
		   try {
			   con = dataSource.getConnection();
			   
			  
			   
               String sql1="select *  from  coursemater where CourseName=?";
			   for(CourseMasterForm coursemaster : ld){
			   pstmt=con.prepareStatement(sql1);
	            pstmt.setString(1, coursemaster.getCourseName());
	           rs=pstmt.executeQuery();
	            
	          
	            if(rs.next())
	            {
	            result = "Duplicate Entry";
	            res=true;
	            }
			   }
			   if(res==false){
					   for(CourseMasterForm coursemaster : ld){
				         	String sqls="insert into coursemater(CourseName,Fees,MaxStudents,CourseDuration,Active,passpercent,TotalDiscountAmount,DiscountValidUpTo,CompanyId,coursecatId,imagename,classroomtraining,elearning,free,level,instructorimage,instructorname,availableseats,coursestartingdate)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	            pstmt=con.prepareStatement(sqls);
	            pstmt.setString(1, coursemaster.getCourseName());
	            pstmt.setLong(2, coursemaster.getFees());
	            pstmt.setLong(3, coursemaster.getMaxStd());

	          /* if(totalcount2==1){
	        	   pstmt.setString(2, filename2);
	           }else{*/
	            pstmt.setInt(4, coursemaster.getDuration());
	            pstmt.setBoolean(5, true);
	            pstmt.setString(6, coursemaster.getPasspercent());
	            pstmt.setString(7, coursemaster.getDispercentage());
	            pstmt.setString(8, coursemaster.getDiscountValidUpTo());
	        
                  pstmt.setString(9, compid);
                  pstmt.setInt(10, coursemaster.getCoursecategoryId());
                


	            
	            
	            String tempimage[]=filename.split(",");
	        	   System.out.println("ppppppppppppp"+tempimage[tempimage.length-1]);
	        	   pstmt.setString(11, tempimage[tempimage.length-1]);
	        	   pstmt.setString(12, coursemaster.getClassroom());
	        	   pstmt.setString(13, coursemaster.getElearning());
	        	   pstmt.setString(14, coursemaster.getFree());
	        	   pstmt.setString(15, coursemaster.getLevel());
	        	   pstmt.setString(16, tempimage[tempimage.length-1]);
	        	   pstmt.setString(17, coursemaster.getInstructorname());
	        	   pstmt.setString(18, coursemaster.getAvailableseats());
	        	   pstmt.setString(19, coursemaster.getCoursestartdate());
	        	   pstmt.executeUpdate();
	            result=Common.REC_ADDED;
	    		 }
	            
	            
			   }
		   }
			   

             
		   
       catch (Exception e) {
	            e.printStackTrace();
	        }
       finally{
	            ConnectionUtil.closeResources(con, pstmt);
	        }
	        return result; 

	}}
