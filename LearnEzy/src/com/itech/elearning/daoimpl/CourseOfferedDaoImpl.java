package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.itech.elearning.forms.CourseCategoryMasterForm;
import com.itech.elearning.forms.CourseOfferedForm;

import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class CourseOfferedDaoImpl {

	public List<CourseOfferedForm> list(int roleid, String compid,
			DataSource dataSource) 
			{
		List<CourseOfferedForm> list = new ArrayList<CourseOfferedForm>();
		CourseOfferedForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
        String sql = null;
		try {
			con = dataSource.getConnection();
			if(roleid==9 )
			{
			 sql = "SELECT * FROM courseofferedmaster  ORDER BY CourseofferedName ASC";
			}
			else
			{
				 sql = "SELECT * FROM courseofferedmaster where CompanyId = '"+compid+"' ORDER BY CourseofferedName ASC";
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new CourseOfferedForm();
				roleForm.setCourseofferedid(rs.getInt("CourseofferedId"));
				roleForm.setCourseofferedname(rs.getString("CourseofferedName"));
				roleForm.setCoursecategoryId(rs.getInt("coursecatId"));
				roleForm.setActive(rs.getBoolean("active"));
				list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;

	}

	public String addRole(CourseOfferedForm roleForm, String compid,
			DataSource dataSource)
	{
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM courseofferedmaster WHERE CourseofferedName=? and CompanyId = '"+compid+"'";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, roleForm.getCourseofferedname());
			rs = pstmt.executeQuery();
			if (rs.next())
				result = Common.DUPLICATE_NAME_MESSAGE;
			else {
				if (!roleForm.getCourseofferedname().equalsIgnoreCase("")
						&& !roleForm.getCourseofferedname().equalsIgnoreCase(null)) {
					sql = "INSERT INTO courseofferedmaster (CourseofferedName,coursecatId,ACTIVE,CompanyId) VALUES (?,?,?,?)";
					if (pstmt != null)
						pstmt.close();
					pstmt = con.prepareStatement(sql);

					pstmt.setString(1, roleForm.getCourseofferedname());
					pstmt.setInt(2, roleForm.getCoursecategoryId());
					pstmt.setBoolean(3, true);
					pstmt.setString(4,compid);
					pstmt.executeUpdate();
					if (rs != null)
						rs.close();
					result = Common.REC_ADDED;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	public String changeRole(CourseOfferedForm roleForm, DataSource dataSource) 
	{
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = dataSource.getConnection();
			String sql = "UPDATE courseofferedmaster SET ACTIVE=? WHERE CourseofferedId=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setBoolean(1, roleForm.isActive());
			pstmt.setLong(2, roleForm.getCourseofferedid());
			pstmt.executeUpdate();
			result = Common.REC_STATUS_CHANGED;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	public String update(CourseOfferedForm roleForm, DataSource dataSource)
	{
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM courseofferedmaster WHERE CourseofferedName COLLATE latin1_general_cs LIKE ? and coursecatId=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, roleForm.getCourseofferedname());
			pstmt.setLong(2, roleForm.getCoursecategoryId());

			rs = pstmt.executeQuery();
			if (rs.next())
				result = Common.No_Change;
			else {
				if (!roleForm.getCourseofferedname().equalsIgnoreCase("")
						&& !roleForm.getCourseofferedname().equalsIgnoreCase(null)) {
					sql = "UPDATE courseofferedmaster SET CourseofferedName=?,coursecatId=?  WHERE CourseofferedId=?";
					if (pstmt != null)
						pstmt.close();
					pstmt = con.prepareStatement(sql);
					pstmt.setString(1, roleForm.getCourseofferedname());
					pstmt.setLong(2, roleForm.getCoursecategoryId());
                    pstmt.setLong(3, roleForm.getCourseofferedid());
					pstmt.executeUpdate();
					if (rs != null)
						rs.close();
					result = Common.REC_UPDATED;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	public List<CourseCategoryMasterForm> coursecatlist(int roleid,
			String compid, DataSource dataSource)
			{
		List<CourseCategoryMasterForm> list = new ArrayList<CourseCategoryMasterForm>();
		CourseCategoryMasterForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
        String sql = null;
		try {
			con = dataSource.getConnection();
			if(roleid==9 )
			{
			 sql = "SELECT * FROM coursecategorymaster  ORDER BY coursecatname ASC";
			}
			else
			{
				 sql = "SELECT * FROM coursecategorymaster where CompanyId = '"+compid+"' ORDER BY coursecatname ASC";
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new CourseCategoryMasterForm();
				roleForm.setCoursecategoryId(rs.getInt("coursecatId"));
				roleForm.setCoursecategoryname(rs.getString("coursecatname"));
				roleForm.setActive(rs.getBoolean("active"));
				list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}

	
	
	
}
