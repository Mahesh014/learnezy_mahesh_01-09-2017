package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.elearning.forms.CompanyMasterForm;
import com.itech.elearning.forms.ContryForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;


public class CountryDaoImpl {
	public List<ContryForm> listAll(int roleid,String compid,DataSource dataSource) {
		List<ContryForm> list = new ArrayList<ContryForm>();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ContryForm countryForm = null;
		String sql = null;
		try	{
			con=dataSource.getConnection();
			if(roleid == 9)
			{
			sql ="SELECT * FROM countrymaster ORDER BY COUNTRY ASC";//select query
			}
			else
			{
				sql ="SELECT * FROM countrymaster where CompanyId='"+compid+"' ORDER BY COUNTRY ASC";//select query

			}
			pstmt=con.prepareStatement(sql);
			rs=pstmt.executeQuery();
			while(rs.next()){
				countryForm = new ContryForm();
				countryForm.setCountryid(rs.getInt("COUNTRYID"));
				countryForm.setCountryname(rs.getString("COUNTRY"));
				countryForm.setActive(rs.getBoolean("ACTIVE"));
				list.add(countryForm);
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}

		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return list;
	}

	public String add(ContryForm countryForm, String compid, DataSource dataSource)
	{
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs =null;
		try
		{
			con=dataSource.getConnection();
			String sql="select * from countrymaster where COUNTRY=? and CompanyId='"+compid+"'";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, countryForm.getCountryname());
			rs = pstmt.executeQuery();
			if(rs.next()) result=Common.DUPLICATE_NAME_MESSAGE;
			else{
				if(!countryForm.getCountryname().equalsIgnoreCase("") && !countryForm.getCountryname().equalsIgnoreCase(null)){
					sql="insert into countrymaster (COUNTRY,ACTIVE,CompanyId) values (?,?,?)";
					pstmt = con.prepareStatement(sql);
					pstmt.setString(1, countryForm.getCountryname());
					pstmt.setBoolean(2, true);
					pstmt.setString(3,compid);
					pstmt.executeUpdate();
					result=Common.REC_ADDED;
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		finally	{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return result;
	}

	public String changeStatus(ContryForm countryForm, DataSource dataSource)
	{
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		try	{
			con=dataSource.getConnection();
			String sql="update countrymaster set ACTIVE = ? where COUNTRYID=?";
			pstmt=con.prepareStatement(sql);
			pstmt.setBoolean(1,countryForm.isActive());
			pstmt.setInt(2, countryForm.getCountryid());
			pstmt.executeUpdate();
			result=Common.REC_STATUS_CHANGED;
		}
		catch (Exception e){
			System.out.println(e);
		} finally{
			ConnectionUtil.closeResources(con, pstmt);
		}
		return result;
	}

	public String update(ContryForm countryForm, DataSource dataSource) 
	{
		String result=Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		try{
			con=dataSource.getConnection();
			String sql="select * from countrymaster where COUNTRY=?";
			pstmt=con.prepareStatement(sql);
			pstmt.setString(1,countryForm.getCountryname());
			rs = pstmt.executeQuery();
			if(rs.next())	result=Common.No_Change;
			else{
					sql="UPDATE COUNTRYMASTER SET COUNTRY=? WHERE COUNTRYID=?";
					pstmt=con.prepareStatement(sql);
					pstmt.setString(1, countryForm.getCountryname());
					pstmt.setLong(2, countryForm.getCountryid());
					pstmt.executeUpdate();
					result=Common.REC_UPDATED;
			}
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return result;
	}

	/*//**************START Audit Trial Inserting Code********************
	public int auditTrail(DataSource dataSource,ContryForm countryForm,int userid) {
		int flag=0;
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		try {
			con=dataSource.getConnection();
			String screen="Country Master Screen";
			String tablename="countrymaster";
			String sql="select * FROM countrymaster WHERE COUNTRYID=?";
			pstmt= con.prepareStatement(sql);
			pstmt.setLong(1,countryForm.getCountryid());
			rs = pstmt.executeQuery();
			while (rs.next()) {
									
				    if(!rs.getString("COUNTRY").equals(countryForm.getCountryname())){
				    	String fieldname = "COUNTRY";
				    	String currentvalue = rs.getString("COUNTRY");
				    	String changedvalue = countryForm.getCountryname();
				    	AuditTrailDao.auditTrail(con,userid,screen,tablename,fieldname,currentvalue,changedvalue);
					}	
			}
		}
		 catch (Exception e) {
			e.printStackTrace();
		}finally{
			ConnectionUtil.closeResources(con, pstmt,rs);
		}
		return flag;
		//**************End Audit Trial Inserting Code********************
	}*/
	public List<ContryForm> activeList(int roleid,String compid, DataSource dataSource){
		List<ContryForm> list = new ArrayList<ContryForm>();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ContryForm countryForm = null;
		String sql = null;
		try{
			con=dataSource.getConnection();
			if(roleid==9)
			{
		 sql="select * from countrymaster where active = true order by COUNTRY ASC";
			}
			else
			{
				 sql="select * from countrymaster where active = true and CompanyId= '"+compid+"' order by COUNTRY ASC";
	
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while(rs.next()){
				countryForm = new ContryForm();
				countryForm.setCountryid(rs.getInt("COUNTRYID"));
				countryForm.setCountryname(rs.getString("COUNTRY"));
				countryForm.setActive(rs.getBoolean("ACTIVE"));
				list.add(countryForm);
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}

		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public JSONArray ajaxActiveList(DataSource dataSource, String countryid) {
		JSONArray jArray= new JSONArray();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql=null;
		try{
			con=dataSource.getConnection();
			sql="select * from countrymaster WHERE COUNTRYID=?";//select query
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1,countryid);
			rs = pstmt.executeQuery();
			while(rs.next()){
				JSONObject obj= new JSONObject();
				obj.put("countryid",rs.getLong("COUNTRYID"));
				obj.put("countryname",rs.getString("COUNTRY"));
				jArray.add(obj);
				System.out.println(sql);
			}
		}
		catch (Exception e){
		   e.printStackTrace();
		}
		
		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;
	}
	
	
	public List<CompanyMasterForm> listcomp(DataSource dataSource) {
		List<CompanyMasterForm> list = new ArrayList<CompanyMasterForm>();
		CompanyMasterForm companyForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM companymaster ORDER BY CompanyName ASC";
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				companyForm = new CompanyMasterForm();
				companyForm.setCompanyID(rs.getInt("CompanyId"));
				companyForm.setCompanyName(rs.getString("CompanyName"));
				companyForm.setActive(rs.getBoolean("active"));
				list.add(companyForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}
}
