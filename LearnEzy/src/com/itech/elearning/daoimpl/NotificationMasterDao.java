package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.json.simple.JSONObject;

import com.itech.elearning.forms.NotificationMasterForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class NotificationMasterDao {

	public List<NotificationMasterForm> listAllCourse(String compid, Long userid, DataSource ds) {
		List<NotificationMasterForm> courseList = new ArrayList<NotificationMasterForm>();
		NotificationMasterForm notiForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ds.getConnection();
			String sql = "SELECT * FROM coursemater WHERE ACTIVE=TRUE and CompanyId = '"+compid+"'";
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				notiForm = new NotificationMasterForm();
				notiForm.setCourseId(rs.getLong("CourseId"));
				notiForm.setCourseName(rs.getString("CourseName"));
				courseList.add(notiForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return courseList;
	}

	@SuppressWarnings("unchecked")
	public List<NotificationMasterForm> list(String compid, Long userid, DataSource ds, Long courseId) {

		List<NotificationMasterForm> list = new ArrayList<NotificationMasterForm>();
		NotificationMasterForm notiForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			if (courseId!=null) {
				sql = "SELECT * FROM NOTIFICATIONMASTER AS NM  JOIN coursemater AS CM ON (CM.COURSEID=NM.COURSEID) where NM.COURSEID=? and NM.CompanyId = '"+compid+"'";
				pstmt = con.prepareStatement(sql);
				pstmt.setLong(1, courseId);
			}else{
				sql = "SELECT * FROM NOTIFICATIONMASTER AS NM  JOIN coursemater AS CM ON (CM.COURSEID=NM.COURSEID) and NM.NM.CompanyId = '"+compid+"' ";
				pstmt = con.prepareStatement(sql);
			}
			
			rs = pstmt.executeQuery();
			while (rs.next()) {
				notiForm = new NotificationMasterForm();
				notiForm.setCourseId(rs.getLong("CourseId"));
				notiForm.setCourseName(rs.getString("CourseName"));
				notiForm.setNotificationID(rs.getLong("NOTIFICATIONID"));
				notiForm.setNotification(rs.getString("NOTIFICATION"));
				notiForm.setExpiryDate(rs.getString("EXPIRYDATE"));
				notiForm.setActive(rs.getBoolean("Active"));
				
				JSONObject obj=new JSONObject();
				obj.put("courseId",rs.getLong("CourseId"));
				obj.put("notificationID",rs.getLong("NOTIFICATIONID"));
				obj.put("notification",rs.getString("NOTIFICATION"));
				obj.put("expiryDate",rs.getString("EXPIRYDATE"));
				notiForm.setjEdit(obj.toString());
				list.add(notiForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		} 
		return list;
	}
	
	public String add(String compid, NotificationMasterForm notiForm, DataSource dataSource) {
		String result = Common.FAILURE_MESSAGE;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM NOTIFICATIONMASTER WHERE NOTIFICATION=? and CourseId=?  and EXPIRYDATE=? and CompanyId = '"+compid+"' ";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1,notiForm.getNotification());
			pstmt.setLong(2,notiForm.getCourseId());
			pstmt.setString(3,notiForm.getExpiryDate());
			rs = pstmt.executeQuery();
			if (rs.next()) {
				result = Common.DUPLICATE_NAME_MESSAGE;
			} else {
				sql = "INSERT INTO NOTIFICATIONMASTER (NOTIFICATION,CourseId,EXPIRYDATE,ACTIVE,CREATEDDATE,CompanyId) VALUES(?,?,?,?,NOW(),?)";
				if (pstmt != null)	pstmt.close();
				pstmt = con.prepareStatement(sql);
				pstmt.setString(1,notiForm.getNotification());
				pstmt.setLong(2,notiForm.getCourseId());
				pstmt.setString(3,notiForm.getExpiryDate());
				pstmt.setBoolean(4, true);
				pstmt.setString(5,compid);
				pstmt.executeUpdate();
				result = Common.REC_ADDED;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	public String changeStatus(NotificationMasterForm notiForm, DataSource dataSource, Long notificationID, boolean active) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = dataSource.getConnection();
			String sql = "UPDATE NOTIFICATIONMASTER SET ACTIVE=? WHERE NOTIFICATIONID=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setBoolean(1,active);
			pstmt.setLong(2, notificationID);
			pstmt.executeUpdate();
			result = Common.REC_STATUS_CHANGED;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	

	public String update(NotificationMasterForm notiForm, DataSource ds) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			sql = "SELECT * FROM NOTIFICATIONMASTER WHERE NOTIFICATION=? and CourseId=?  and EXPIRYDATE=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1,notiForm.getNotification());
			pstmt.setLong(2,notiForm.getCourseId());
			pstmt.setString(3,notiForm.getExpiryDate());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				result = Common.No_Change;
			} else {
					sql = "UPDATE NOTIFICATIONMASTER SET NOTIFICATION=?, CourseId=?, EXPIRYDATE=? where NOTIFICATIONID=?";
					if (pstmt != null)	pstmt.close();
					pstmt = con.prepareStatement(sql);
					pstmt.setString(1,notiForm.getNotification());
					pstmt.setLong(2,notiForm.getCourseId());
					pstmt.setString(3,notiForm.getExpiryDate());
					pstmt.setLong(4,notiForm.getNotificationID());
					pstmt.executeUpdate();
					result = Common.REC_UPDATED;
				 
			} 

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;

	}

	 
}
