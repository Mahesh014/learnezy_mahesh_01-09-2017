package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import com.itech.elearning.forms.StudentCourseMgmtForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class StudentCourseMgmtDaoImpl {

	public List<StudentCourseMgmtForm> listAll(Long userid, DataSource ds) {

		List<StudentCourseMgmtForm> courseList = new ArrayList<StudentCourseMgmtForm>();
		StudentCourseMgmtForm courseData = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ds.getConnection();
			String sql = "SELECT * from coursemater";
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				courseData = new StudentCourseMgmtForm();
				courseData.setCourseId(rs.getLong("CourseId"));
				courseData.setCourseName(rs.getString("CourseName"));

				courseData.setActive(rs.getBoolean("Active"));
				courseList.add(courseData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return courseList;
	}

	public String addDetails(StudentCourseMgmtForm stdForm, DataSource ds,
			long stdid) {
		String result = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		Timestamp feeDate1 = null;
		Timestamp dueDate1 = null;
		java.util.Date fdate1 = null;
		java.util.Date dueDate11 = null;
		try {
			try {
				con = ds.getConnection();
				String sql = "SELECT * FROM studentcoursemgmt  WHERE CourseID=? and StudentID=?";
				pstmt = con.prepareStatement(sql);
				pstmt.setLong(1, stdForm.getCourseId());
				pstmt.setLong(2, stdForm.getStdId());
				rs = pstmt.executeQuery();
				if (rs.next())
					result = Common.DUPLICATE_COURSE_MESSAGE;
				else {
					sql = "INSERT INTO studentcoursemgmt (StudentID,CourseId,FeePaid,Dateofpayment,DueDate) VALUES(?,?,?,?,?)";
					if (pstmt != null)
						pstmt.close();
					pstmt = con.prepareStatement(sql);
					System.out.println(sql);
					pstmt.setLong(1, stdForm.getStdId());
					pstmt.setLong(2, stdForm.getCourseId());
					pstmt.setInt(3, stdForm.getFeepaid());

					String feedate = stdForm.getPaidDate();
					String dueDate = stdForm.getDueDate();

					DateFormat fformatter = new SimpleDateFormat("dd-MM-yyyy");
					try {
						if (feedate != "dd-mm-yyyy" && feedate != "") {
							fdate1 = (java.util.Date) fformatter.parse(feedate);
							dueDate11 = (java.util.Date) fformatter
									.parse(dueDate);
							feeDate1 = new Timestamp(fdate1.getTime());
							dueDate1 = new Timestamp(dueDate11.getTime());
						}
					} catch (ParseException e) {
						// e.printStackTrace();
					} catch (NullPointerException e) {
						// e.printStackTrace();
					}

					pstmt.setTimestamp(4, feeDate1);
					pstmt.setTimestamp(5, dueDate1);

					pstmt.executeUpdate();
					result = Common.REC_ADDED;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}

		return result;
	}

	public void getStdInfo(DataSource ds, long userId,
			HttpServletRequest request) {

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ds.getConnection();
			String sql = "SELECT * from usermaster where UserId=" + userId + "";
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {

				request.setAttribute("fname", rs.getString("FirstName"));
				request.setAttribute("lname", rs.getString("MiddleName"));
				request.setAttribute("stdId", userId);

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}

	}

	public List<StudentCourseMgmtForm> listStdList(Long userid, DataSource ds,
			StudentCourseMgmtForm stdForm) {
		List<StudentCourseMgmtForm> stdList = new ArrayList<StudentCourseMgmtForm>();
		StudentCourseMgmtForm topicData = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		System.out.println(" Funtion Called By LIst");
		try {
			con = ds.getConnection();
			String sql = "select smt.*, um.FirstName, um.LastName ,cmt.CourseId, cmt.CourseName,  cmt.Fees, sum(smt.FeePaid) as feePaid,(cmt.Fees - sum(smt.FeePaid) ) as Remaining from studentcoursemgmt as smt join coursemater cmt on smt.CourseId = cmt.CourseId join usermaster as um on um.UserId = smt.StudentID where smt.StudentID = "+stdForm.getStdId() +" group by smt.CourseId;";
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				topicData = new StudentCourseMgmtForm();
				topicData.setStdId(rs.getLong("smt.StudentID"));
				topicData.setStdName(rs.getString("um.FirstName") + "  "
						+ rs.getString("um.LastName"));
				topicData.setFeepaid(rs.getInt("smt.FeePaid"));
				topicData.setTotalFee(rs.getInt("cmt.Fees"));
				topicData.setRemaining(rs.getInt("Remaining"));
				topicData.setTotfeePaid(rs.getInt("feePaid"));
				topicData.setCourseName(rs.getString("cmt.CourseName"));

				stdList.add(topicData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return stdList;
	}

}
