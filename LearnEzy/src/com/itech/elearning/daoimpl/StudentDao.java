package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import com.itech.elearning.utils.*;
import com.itech.elearning.forms.RoleForm;
import com.itech.elearning.forms.StudentForm;
import com.itech.elearning.forms.StudentRegistrationForm;
import com.itech.elearning.utils.ConnectionUtil;

public class StudentDao {

	@SuppressWarnings("unchecked")
	public JSONArray courseList(DataSource dataSource, Long userid) {
		JSONArray jArray= new JSONArray();
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		Connection con=null;
		String courseIdsList=null;
	try{		
		con = dataSource.getConnection();
		String sql = "select CourseId FROM studentcoursedetails where StudentId=?";
		pstmt = con.prepareStatement(sql);
		pstmt.setLong(1, userid);
		rs = pstmt.executeQuery();
		if(rs.next()) {
			courseIdsList=rs.getString("CourseId");
		}
		
		
		if (rs != null) { rs.close(); }
		if (pstmt != null) {	pstmt.close();}
		sql = "select CourseId,CourseName FROM coursemater where CourseId in ("+courseIdsList+"); ";
		pstmt = con.prepareStatement(sql);
		System.out.println(sql);
		rs = pstmt.executeQuery();
		while (rs.next()) {
			JSONObject obj = new JSONObject();
			obj.put("courseId", rs.getString("CourseId"));
			obj.put("courseName", rs.getString("CourseName"));
			jArray.add(obj);
		}
	} catch (Exception e) {
		e.printStackTrace();
	}	
	finally {
		ConnectionUtil.closeResources(con, pstmt, rs);
	}
		return jArray;
	}

	 
	@SuppressWarnings("unchecked")
	public JSONObject contantList(DataSource dataSource, Long subTopicId,Long courseId, Long userid) {
		JSONObject obj = new JSONObject();
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		Connection con=null;
		int flag=0;
		String sql=""; 
		try{
			
			con = dataSource.getConnection();
			if(subTopicId==0){
				sql = "select * from contentmaster where SubTopicID=(select min(SubTopicID) from subtopicmaster where CourseId=?) And CourseId=?";
				pstmt = con.prepareStatement(sql);
				pstmt.setLong(1, courseId);
				pstmt.setLong(2, courseId);
			}else{
				sql = "select * from contentmaster where SubTopicID=? And CourseId=?";
				pstmt = con.prepareStatement(sql);
				pstmt.setLong(1, subTopicId);
				pstmt.setLong(2, courseId);
			}
			
			
			System.out.println(sql+subTopicId);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				
				obj.put("subTopicText",rs.getString("contentText"));
				obj.put("subTopicVideos",rs.getString("contentVideos"));
				obj.put("subTopicImages",rs.getString("contentImages"));
				String youTubeLink=rs.getString("youTubeLink");
				if(youTubeLink!=""){
					StringBuffer link = new StringBuffer(youTubeLink);
					youTubeLink=link.reverse().toString();
					String[] aryLink=youTubeLink.split("=");
					youTubeLink= youTubeLink= link.delete(0, link.length()).append(aryLink[0]).reverse().toString();;
				}
				obj.put("youTubeLink",youTubeLink);
				flag=1;
			}else{
				obj.put("subTopicText","");
				obj.put("subTopicVideos","");
				obj.put("subTopicImages","");
				obj.put("youTubeLink","");
			}
			if(flag==1){
			sql = "select * from sampleprogrammaster where SubTopicID=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, subTopicId);
			System.out.println(sql+subTopicId);
			JSONArray jAray= new JSONArray();
			rs = pstmt.executeQuery();
			while (rs.next()) {
				JSONObject spObj= new JSONObject();
				spObj.put("name",rs.getString("name"));
				spObj.put("sampleProgramContent",rs.getString("sampleProgramContent"));
				jAray.add(spObj);
			}
			obj.put("jAray",jAray);
			}
			StudentDao.addLastView(dataSource, subTopicId,0 ,courseId, userid);
			
		} catch (Exception e) {
			e.printStackTrace();
		}	
		finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
			return obj;
	}


	public long lastView(DataSource dataSource, Long courseId ,Long userid ) {
		long subTopicId=0;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		Connection con=null;
		try{
			con = dataSource.getConnection();
			String sql = "select LastViewed from usermaster where userid=? ";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, userid);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				String LastViewed=rs.getString("LastViewed");
				String lastView[]=LastViewed.split(",");
				subTopicId=Long.parseLong(lastView[2]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}	
		finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
			return subTopicId;
	 }
	
	
	public static long addLastView(DataSource dataSource, Long subTopicId,int topicID, Long courseId,Long userid) {
		long flag=0;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		Connection con=null;
		try{
			con = dataSource.getConnection();
			String sql = "UPDATE usermaster SET LastViewed=? Where UserId=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, courseId+","+topicID+","+subTopicId);
			pstmt.setLong(2, userid);
			pstmt.executeUpdate();
			flag=2;
			System.out.println(sql);
				 
		} catch (Exception e) {
			e.printStackTrace();
		}	finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
			return flag;
	 }


/*	@SuppressWarnings("unchecked")
	public JSONArray getAssignment(DataSource dataSource, Long userid) {
		JSONArray jArray= new JSONArray();
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		Connection con=null;
		String courseIdsList=null;
		System.out.println();
	try{
		
		con = dataSource.getConnection();
		String sql = "select CourseId FROM studentcoursedetails where StudentId=?";
		pstmt = con.prepareStatement(sql);
		pstmt.setLong(1, userid);
		rs = pstmt.executeQuery();
		if(rs.next()) {
			courseIdsList=rs.getString("CourseId");
		}
		
		
		if (rs != null) { rs.close(); }
		if (pstmt != null) {	pstmt.close();}
		sql = "select am.CourseId,cm.CourseName FROM assignmentmaster am inner join coursemater cm on am.CourseId=cm.CourseId where am.CourseId in ("+courseIdsList+") group by am.CourseId ; ";
		pstmt = con.prepareStatement(sql);
		System.out.println(sql);
		rs = pstmt.executeQuery();
		while (rs.next()) {
			JSONObject obj = new JSONObject();
			obj.put("courseId", rs.getString("CourseId"));
			obj.put("courseName", rs.getString("CourseName"));
			jArray.add(obj);
		}
	} catch (Exception e) {
		e.printStackTrace();
	}	
	finally {
		ConnectionUtil.closeResources(con, pstmt, rs);
	}
		return jArray;
	}
*/

	public long addStudentAssignment(DataSource dataSource, Long assignmentId, Long userid) {
		long flag=0;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		Connection con=null;
		try{
			con = dataSource.getConnection();
			String sql = "select * FROM studentassignmentdetails where StudentId=? and AssignmentId=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, userid);
			pstmt.setLong(2,assignmentId);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				flag=1;
			}
			
			if (flag==0) {
			sql = "insert into studentassignmentdetails (StudentId,AssignmentId,Date) values(?,?,now())";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, userid);
			pstmt.setLong(2,assignmentId);
			pstmt.executeUpdate();
			flag=2;
			System.out.println(sql);
			}
			
				 
		} catch (Exception e) {
			e.printStackTrace();
		}	finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
			return flag;
	}


	public long getAssignmentCount(DataSource dataSource, Long userid) {
		long count=0;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		Connection con=null;
		try{
			con = dataSource.getConnection();
			String sql = "select count(*) as completed FROM studentassignmentdetails where StudentId=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, userid);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				count=rs.getLong("completed");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}	finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
			return count;
	}


	@SuppressWarnings("unchecked")
	public JSONArray getAssignmentByTopicId(DataSource dataSource, Long userid, Long courseId, Long topicId) {
		JSONArray jArray= new JSONArray();
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		Connection con=null;
	 
	try{
		
		con = dataSource.getConnection();
		//String sql = "select * FROM assignmentmaster  where CourseId =? and TopicId=? and Active=1";
		String sql = "select * FROM assignmentmaster  where  TopicId=? and Active=1";
		pstmt = con.prepareStatement(sql);
		pstmt.setLong(1, topicId);
		System.out.println(sql);
		int questionNo=0;
		rs = pstmt.executeQuery();
		while (rs.next()) {
			questionNo=questionNo+1;
			JSONObject obj = new JSONObject();
			obj.put("questionNo",questionNo);
			obj.put("assignmentId", rs.getString("AssignmentId"));
			obj.put("assignmentQuestion", rs.getString("AssignmentQuestion"));
			obj.put("assignmentAnswer", rs.getString("AssignmentAnswer"));
			
			new StudentDao().addStudentAssignment(dataSource, rs.getLong("AssignmentId"), userid);
			jArray.add(obj);
		}
	} catch (Exception e) {
		e.printStackTrace();
	}	
	finally {
		ConnectionUtil.closeResources(con, pstmt, rs);
	}
		return jArray;
	}

	 
	@SuppressWarnings("unchecked")
	public JSONArray getKnowledgeBank(DataSource dataSource, Long userid, Long courseId) {
		JSONArray jArray= new JSONArray();
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		Connection con=null;
		int questionNo=0;
		try{
			con = dataSource.getConnection();
			String sql = "select * FROM kbmaster where CourseId="+courseId;
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				questionNo=questionNo+1;
				JSONObject obj = new JSONObject();
				obj.put("questionNo",questionNo);
				obj.put("kBQuestion", rs.getString("KBQuestion"));
				obj.put("kBAnswer", rs.getString("KBAnswer"));
				jArray.add(obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}	
		finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
			return jArray;
	}


	@SuppressWarnings("unchecked")
	public JSONArray getResources(DataSource dataSource, Long userid,Long courseId) {
		JSONArray jArray= new JSONArray();
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		Connection con=null;
		int questionNo=0;
		try{
			con = dataSource.getConnection();
			String sql = "select * FROM ebooknamemaster where CourseId="+courseId;
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				questionNo=questionNo+1;
				JSONObject obj = new JSONObject();
				obj.put("questionNo",questionNo);
				obj.put("eBookName", rs.getString("EBOOKNAME"));
				obj.put("fileName", rs.getString("FILENAME"));
				jArray.add(obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}	
		finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
			return jArray;
	}


	@SuppressWarnings("unchecked")
	public JSONArray getAssignmentTopicId(DataSource dataSource, Long userid, Long courseId) {
		JSONArray jArray= new JSONArray();
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		Connection con=null;
		int questionNo=0;
		try{
			con = dataSource.getConnection();
			String sql = "select * FROM assignmentmaster am inner join topicmaster tm on tm.topicid=am.topicid where am.CourseId="+courseId+" group by am.topicid";
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				questionNo=questionNo+1;
				JSONObject obj = new JSONObject();
				obj.put("topicId", rs.getString("topicId"));
				obj.put("topicName", rs.getString("topicName"));
				jArray.add(obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}	
		finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
			return jArray;
	}


	@SuppressWarnings("unchecked")
	public JSONArray getLinks(DataSource dataSource, Long userid) {
		JSONArray jArray= new JSONArray();
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		Connection con=null;
		int questionNo=0;
		try{
			con = dataSource.getConnection();
			String sql = "select * FROM linkdetailsmaster where active=1";
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				questionNo=questionNo+1;
				JSONObject obj = new JSONObject();
				obj.put("linkName",rs.getString("linkName"));
				obj.put("questionNo",questionNo);
				obj.put("url", rs.getString("URL"));
				jArray.add(obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}	
		finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
			return jArray;
	}


	@SuppressWarnings("unchecked")
	public JSONArray getNotification(DataSource dataSource, Long userid) {
		JSONArray jArray= new JSONArray();
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		Connection con=null;
		int questionNo=0;
		//String courseIdsList=null;
		try{
			con = dataSource.getConnection();
			/*
			 * String sql = "select CourseId FROM studentcoursedetails where StudentId=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, userid);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				courseIdsList=rs.getString("CourseId");
			}
			
			
			
			sql = "select * from notificationmaster nm inner join coursemater cm on nm.courseid=cm.courseid where nm.CourseId in ("+courseIdsList+") and  nm.active=1 and nm.EXPIRYDATE>=CURDATE() order by nm.courseid";
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				questionNo=questionNo+1;
				JSONObject obj = new JSONObject();
				obj.put("questionNo",questionNo);
				obj.put("courseName", rs.getString("CourseName"));
				obj.put("notification", rs.getString("NOTIFICATION"));
				jArray.add(obj);
			}
			*/
			String sql = "select * from notificationstudent where studentid='"+userid+"' and  active=1 and EXPIRYDATE>=CURDATE() ";
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				questionNo=questionNo+1;
				JSONObject obj = new JSONObject();
				obj.put("questionNo",questionNo);
				obj.put("notification", rs.getString("NOTIFICATION"));
				jArray.add(obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}	
		finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
			return jArray;
	}


	public Object getTestCompleteCount(DataSource dataSource, Long userid) {
		long count=0;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		Connection con=null;
		try{
			con = dataSource.getConnection();
			String sql = "select count(DISTINCT testid ) as completed FROM testanswer where passStatus='pass' and  StudentId=? ";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, userid);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				count=rs.getLong("completed");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}	finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
			return count;
	}
	
	
	public Object getCourseCompleteCount(DataSource dataSource, Long userid) {
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		
		PreparedStatement pstmt1=null;
		ResultSet rs1=null;
		
		PreparedStatement pstmt2=null;
		ResultSet rs2=null;
		
		Connection con=null;
		String courseIdsList=null;
		 
		int courseId=0;
		
		int courseCompCount=0;
		try{
			con = dataSource.getConnection();
			String sql = "select CourseId FROM studentcoursedetails where StudentId=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, userid);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				courseIdsList=rs.getString("CourseId");
			}
			String[] courseIdsArray=courseIdsList.split(",");
			
			
			
			
			
			
			for (int j = 0; j < courseIdsArray.length; j++) {
				boolean testPassFlag=false;
				int testId=0;
				courseId=Integer.parseInt(courseIdsArray[j]);
				
				String sql1 = "select * from testmaster  where courseid='"+courseId+"' and Active=1";
				pstmt1 = con.prepareStatement(sql1);
				//pstmt.setInt(1, courseId);
				rs1 = pstmt1.executeQuery();
				System.out.println(sql1);
				
				if(rs1.next()) {	testPassFlag=true;	}
				
				if(testPassFlag){
					if(rs1!=null) rs1.close();
					if(pstmt1!=null) pstmt1.close();
					sql1 = "select * from testmaster  where courseid='"+courseId+"' and Active=1";
					pstmt1 = con.prepareStatement(sql1);
					//pstmt.setInt(1, courseId);
					rs1 = pstmt1.executeQuery();
					System.out.println(sql1);
					while (rs1.next()) {
						testId=rs1.getInt("testId");
					
						String sql2 = "select * from testanswer  where passStatus='pass' and testId='"+testId+"' and studentid="+userid;
						pstmt2 = con.prepareStatement(sql2);
						/*pstmt.setInt(1, testId);
						pstmt.setLong(2, userid)*/;
						rs2 = pstmt2.executeQuery();
						System.out.println(sql2);
						System.out.println("TestID :"+testId);
						if(rs2.next()) {
							System.out.println("TestID PASSS:"+testId);
						}else{
							testPassFlag=false;
							System.out.println("TestID FAILS:"+testId);
						}
				
					}
				}
				if (testPassFlag) {
					courseCompCount=courseCompCount+1;
					 
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}	finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
			return courseCompCount;
	}
	
	public Object getAchievementCompleteCount(DataSource dataSource, Long userid) {
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		
		PreparedStatement pstmt1=null;
		ResultSet rs1=null;
		
		PreparedStatement pstmt2=null;
		ResultSet rs2=null;
		
		Connection con=null;
		String courseIdsList=null;
		int testId=0;
		int courseId=0;
		boolean testPassFlag=true;
		int courseCompCount=0;
		int passPercentage=0;
		int achivementScore=0;
		try{
			con = dataSource.getConnection();
			String sql = "select CourseId FROM studentcoursedetails where StudentId=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, userid);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				courseIdsList=rs.getString("CourseId");
			}
			String[] courseIdsArray=courseIdsList.split(",");
			for (int j = 0; j < courseIdsArray.length; j++) {
				System.out.println("Course: "+courseIdsArray[j]);
				courseId=Integer.parseInt(courseIdsArray[j]);
				
				String sql1 = "select * from testmaster  where courseid='"+courseId+"' and Active=1";
				pstmt1 = con.prepareStatement(sql1);
				//pstmt.setInt(1, courseId);
				rs1 = pstmt1.executeQuery();
				System.out.println(sql1);
				while (rs1.next()) {
					testId=rs1.getInt("testId");
					achivementScore=rs1.getInt("AchivementScore");
					
					String sql2 = "select * from testanswer  where passStatus='pass' and testId='"+testId+"' and studentid="+userid;
					pstmt2 = con.prepareStatement(sql2);
					/*pstmt.setInt(1, testId);
					pstmt.setLong(2, userid)*/;
					rs2 = pstmt2.executeQuery();
					System.out.println(sql2);
					System.out.println("TestID :"+testId);
					while(rs2.next()) {
						passPercentage=rs2.getInt("passPercentage");
						if (passPercentage>=achivementScore) {
							courseCompCount=courseCompCount+1;
							System.out.println("PassPertage:"+passPercentage+" AchivementScore:"+achivementScore);	
						}else{
							System.out.println("PassPertage:"+passPercentage+" AchivementScore:"+achivementScore);	
						}
						
						
					}
				
				}
				if (testPassFlag) {
					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}	finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
			return courseCompCount;
	}


	@SuppressWarnings("unchecked")
	public JSONArray getAssignmentQuestionbySubTopicID(DataSource dataSource,Long userid, Long courseId, Long subTopicId) {
		JSONArray jArray= new JSONArray();
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		Connection con=null;
	 
	try{
		
		con = dataSource.getConnection();
		//String sql = "select * FROM assignmentmaster  where CourseId =? and TopicId=? and Active=1";
		String sql = "select * FROM assignmentmaster  where  subTopicId=? and Active=1";
		pstmt = con.prepareStatement(sql);
		pstmt.setLong(1, subTopicId);
		System.out.println(sql);
		int questionNo=0;
		rs = pstmt.executeQuery();
		while (rs.next()) {
			questionNo=questionNo+1;
			JSONObject obj = new JSONObject();
			obj.put("questionNo",questionNo);
			obj.put("assignmentId", rs.getString("AssignmentId"));
			obj.put("assignmentQuestion", rs.getString("AssignmentQuestion"));
			obj.put("assignmentAnswer", rs.getString("AssignmentAnswer"));
			new StudentDao().addStudentAssignment(dataSource, rs.getLong("AssignmentId"), userid);
			jArray.add(obj);
		}
	} catch (Exception e) {
		e.printStackTrace();
	}	
	finally {
		ConnectionUtil.closeResources(con, pstmt, rs);
	}
		return jArray;
	}

	public int academyRegistration(StudentRegistrationForm studentForm,
			String compid, DataSource dataSource, String profilePhotoName) {
		// TODO Auto-generated method stub
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		   //get current date time with Date()
		   Date date = new Date();
		   dateFormat.format(date);
		   System.out.println(dateFormat.format(date));
		Connection con = null;
		int roleid = 5;
		int userId=0;
		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM USERMASTER WHERE USERNAME=? and CompanyId= '"+compid+"'";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, studentForm.getUname());
			rs = pstmt.executeQuery();
			if (rs.next()) {
			} else {
				sql = "INSERT INTO USERMASTER (UserName,Password,FIRSTNAME,MiddleName,LASTNAME,DateofBirth,Gender,Qualification,ContactNumber,Address,Phone,emailid,RoleID,ACTIVE,profilePhoto,paymentStatus,countryId, stateId, cityId, pincode,otherCountry,otherState,otherCity,CompanyId,areaofexpertise) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				if (pstmt != null) pstmt.close();
				pstmt = con.prepareStatement(sql);
				pstmt.setString(1, studentForm.getUname());
				pstmt.setString(2, EncryptUtil.encrypt(studentForm.getPassword()));
				pstmt.setString(3, studentForm.getFirstName());
				pstmt.setString(4, studentForm.getMiddleName());
				pstmt.setString(5, studentForm.getLastName());
				pstmt.setString(6, studentForm.getDob());	
				System.out.println("dob "+studentForm.getDob());
				pstmt.setString(7, studentForm.getGender());
				pstmt.setString(8, studentForm.getQualification());
				pstmt.setString(9, studentForm.getContactno());
				pstmt.setString(10,studentForm.getAddress());
				pstmt.setString(11,studentForm.getPhone());
				pstmt.setString(12,studentForm.getEmailId());
				pstmt.setInt(13,roleid);
				pstmt.setBoolean(14,true);
				pstmt.setString(15,profilePhotoName);
				pstmt.setInt(16,0);
				pstmt.setInt(17,studentForm.getCountryid());
				pstmt.setInt(18,studentForm.getStateid());
				pstmt.setInt(19,studentForm.getCityid());
				pstmt.setInt(20,studentForm.getPincode());
				pstmt.setString(21,studentForm.getOthercountryname());
				pstmt.setString(22,studentForm.getOtherstatename());
				pstmt.setString(23,studentForm.getOthercityname());
				pstmt.setString(24,compid);
				pstmt.setString(25,studentForm.getAreofexper());
				pstmt.executeUpdate();
				
				sql = "SELECT UserId FROM USERMASTER WHERE USERNAME=? and CompanyId='"+compid+"'";
				pstmt1 = con.prepareStatement(sql);
				pstmt1.setString(1, studentForm.getUname());
				rs1 = pstmt1.executeQuery();
				if (rs1.next()){
					userId = rs1.getInt("UserId");
					System.out.println(userId);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		
		return userId;
	}
	
	public JSONArray addRating(DataSource dataSource, Long userid, String courseIdRating, String rating, int companyId) {
		// TODO Auto-generated method stub
		Connection con = null;
		PreparedStatement pstmt = null;
		try{
		System.out.println("************************************");
		con = dataSource.getConnection();
		
		
		
		String sql ="insert into ratingtable(userId,rating,courseId,companyId,active) values(?,?,?,?,?)";
		System.out.println("Query="+sql);
		pstmt = con.prepareStatement(sql);
		pstmt.setLong(1,userid);
		pstmt.setString(2,rating);
		pstmt.setString(3,courseIdRating);
		
		
		System.out.println("course Id==========="+courseIdRating);
		System.out.println("rating==========="+rating);
		pstmt.setInt(4,companyId);
		pstmt.setBoolean(5,true);
		
		int i=pstmt.executeUpdate();
		System.out.println("inserted="+i);
		
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}


	@SuppressWarnings("unchecked")
	public JSONArray getAudioFiles(DataSource dataSource, Long userid, String audioUploadPath) {
		JSONArray jArray= new JSONArray();
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		Connection con=null;
		String courseIdsList=null;
	try{		
		con = dataSource.getConnection();
		String sql = "select CourseId FROM studentcoursedetails where StudentId=?";
		pstmt = con.prepareStatement(sql);
		pstmt.setLong(1, userid);
		rs = pstmt.executeQuery();
		if(rs.next()) {
			courseIdsList=rs.getString("CourseId");
		}
		
		
		if (rs != null) { rs.close(); }
		if (pstmt != null) {	pstmt.close();}
		sql = "select * from audiofiles af inner join coursemater cm on cm.courseid=af.courseid where af.CourseId in ("+courseIdsList+"); ";
		pstmt = con.prepareStatement(sql);
		System.out.println(sql);
		rs = pstmt.executeQuery();
		while (rs.next()) {
			JSONObject obj = new JSONObject();
			obj.put("courseId", rs.getString("CourseId"));
			obj.put("courseName", rs.getString("CourseName"));
			obj.put("fileName", rs.getString("fileName"));
			jArray.add(obj);
		}
	} catch (Exception e) {
		e.printStackTrace();
	}	
	finally {
		ConnectionUtil.closeResources(con, pstmt, rs);
	}
		return jArray;
	}


	public List<StudentForm> list(DataSource dataSource, Long userid,
			int courseid) {
		List<StudentForm> list = new ArrayList<StudentForm>();
		StudentForm studentForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
        String sql = null;
		try {
			con = dataSource.getConnection();
			
			 sql = "SELECT * FROM mynotes where courseid=?";
			
			
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, courseid);

			rs = pstmt.executeQuery();
			while (rs.next()) {
				studentForm = new StudentForm();
				studentForm.setCourseId(rs.getLong("courseid"));
				studentForm.setNotes(rs.getString("notes"));
				list.add(studentForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}


	public Object addmynotes(DataSource dataSource, Long userid, int courseid, StudentForm studentform) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int courseid1=0;
		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM mynotes WHERE notes=? and courseid=? and userid=? ";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, studentform.getNotes());
			pstmt.setLong(2, courseid);
            pstmt.setString(3, studentform.getNotes());
			rs = pstmt.executeQuery();
			if (rs.next())
				result = Common.No_Change;
			else
			{
				
				if (!studentform.getNotes().equalsIgnoreCase("")
						&& !studentform.getNotes().equalsIgnoreCase(null)) {
					sql="select * from mynotes where courseid=? and userid=?";
					if (pstmt != null)
						pstmt.close();
					pstmt = con.prepareStatement(sql);
					pstmt.setLong(1, courseid);
					pstmt.setLong(2, userid);

					rs=pstmt.executeQuery();
					while(rs.next())
					{
						courseid1=rs.getInt("courseid");
						
					}
				if(courseid1==courseid)
				{
					System.out.println("inside dao update method");
					
					sql="update mynotes set notes=? where courseid=?";
					if (pstmt != null)
						pstmt.close();
					pstmt = con.prepareStatement(sql);
                 pstmt.setString(1, studentform.getNotes());
					pstmt.setLong(2, courseid);

					pstmt.executeUpdate();
					if (rs != null)
						rs.close();
					result ="Notes Added successfully";

					
				}
				else
				{
					System.out.println("inside dao insert method");


				sql = "INSERT INTO mynotes (courseid,notes,userid) VALUES (?,?,?)";
					if (pstmt != null)
						pstmt.close();
					pstmt = con.prepareStatement(sql);
					pstmt.setLong(1, courseid);

					pstmt.setString(2, studentform.getNotes());
					pstmt.setLong(3, userid);

					pstmt.executeUpdate();
					if (rs != null)
						rs.close();
					result ="Notes Updated successfully";
				}
				}
			
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return result;

	}

	 
}
