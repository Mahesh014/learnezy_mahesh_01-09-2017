package com.itech.elearning.daoimpl;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.itech.elearning.forms.WhiteBoardForm;
import com.itech.elearning.utils.ConnectionUtil;

public class WhiteBoardDao {

	public List<WhiteBoardForm> appointmentUserlist(int roleid,String compid,DataSource dataSource) {
		List<WhiteBoardForm> list = new ArrayList<WhiteBoardForm>();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		WhiteBoardForm whiteBoardForm = null;
		String sql = null;
		try	{
			con=dataSource.getConnection();
			if(roleid ==9 )
			{
		 sql="select * from appointmentschedule ass inner join appointment a on a.appointmentID = ass.appointmentId inner join coursemater cm on cm.CourseId=ass.courseid left join whiteborad wb on wb.appointmentScheduleId = ass.appointmentScheduleId ";//select query
			}
			else
			{
				 sql="select * from appointmentschedule ass inner join appointment a on a.appointmentID = ass.appointmentId inner join coursemater cm on cm.CourseId=ass.courseid left join whiteborad wb on wb.appointmentScheduleId = ass.appointmentScheduleId where ass.companyId= '"+compid+"' ";//select query
	
			}
		 pstmt=con.prepareStatement(sql);
			rs=pstmt.executeQuery();
			while(rs.next()){
				whiteBoardForm = new WhiteBoardForm();	
				whiteBoardForm.setAppointmentScheduleId(rs.getInt("appointmentScheduleId"));
				whiteBoardForm.setAppointmentCreatedID(rs.getInt("appointmentCreatedId"));
				whiteBoardForm.setCourseid(rs.getInt("courseId"));
				whiteBoardForm.setStatus(rs.getString("status"));
				whiteBoardForm.setAppointmentType(rs.getString("appointmentType"));
				whiteBoardForm.setCourseName(rs.getString("CourseName"));
				whiteBoardForm.setAppointmentDateTime(rs.getString("dateofappointment") +" "+ rs.getString("timeofappointment"));
				whiteBoardForm.setActive(rs.getBoolean("ACTIVE"));
				whiteBoardForm.setSessionStatus(rs.getString("SessionStatus"));
				whiteBoardForm.setMeetingName(rs.getString("meetingName"));
				whiteBoardForm.setMeetingURLLink(rs.getString("meetingURl"));
				list.add(whiteBoardForm);
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}

		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return list;
	}

	public int addSession(WhiteBoardForm whiteBoardForm, DataSource dataSource, String appointmentScheduleId, 
			String meetingtitle, String meetingpassword, String option, int whiteBoardSessionID) {
			int flag =0;
			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs =null;
			try
			{
				con=dataSource.getConnection();
				String sql="insert into whiteborad(appointmentScheduleId, whiteBoardSessionID, accessPassword, whiteBoardUserName, "
						+ "whiteBoardSessionTitle, SessionStatus) values (?, ?, ?, ?, ?, ?)";
				pstmt = con.prepareStatement(sql);
				
				pstmt.setInt(1, Integer.parseInt(appointmentScheduleId));
				pstmt.setString(2, String.valueOf(whiteBoardSessionID));
				pstmt.setString(3, meetingpassword);
				pstmt.setString(4, "");
				pstmt.setString(5, meetingtitle);
				pstmt.setString(6,option);
				
				/*pstmt.setInt(1, whiteBoardForm.getAppointmentScheduleId());
				pstmt.setString(2, whiteBoardForm.getWhiteBoardSessionID());
				pstmt.setString(3, whiteBoardForm.getAccessPassword());
				pstmt.setString(4, whiteBoardForm.getWhiteBoardUserName());
				pstmt.setString(5, whiteBoardForm.getWhiteBoardSessionTitle());
				pstmt.setString(6, whiteBoardForm.getSessionStatus());*/
				pstmt.executeUpdate();
				flag=1;
				
			}
			catch (Exception e) 
			{
				e.printStackTrace();
			}

			finally	{
				ConnectionUtil.closeResources(con, pstmt, rs);
			}
			return flag;
		}

	public WhiteBoardForm getWhiteBoardSessionID(DataSource dataSource, String appointmentScheduleId) {
		WhiteBoardForm whiteBoardForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		 
		try	{
			con=dataSource.getConnection();
			String sql="select * from whiteborad wm inner join appointmentschedule aps on aps.appointmentScheduleId=wm.appointmentScheduleId inner join coursemater cm on cm.courseid=aps.courseid inner join appointment appt on appt.appointmentID=aps.appointmentID  where wm.appointmentScheduleId=?";//select query
			pstmt=con.prepareStatement(sql);
			pstmt.setString(1, appointmentScheduleId);
			rs=pstmt.executeQuery();
			while(rs.next()){
				whiteBoardForm = new WhiteBoardForm();
				whiteBoardForm.setWhiteBoardSessionID(rs.getString("whiteBoardSessionID"));
				whiteBoardForm.setWhiteBoardSessionTitle(rs.getString("courseName"));
				whiteBoardForm.setAccessPassword(rs.getString("accessPassword"));
				whiteBoardForm.setSessionStatus(rs.getString("SessionStatus"));
				whiteBoardForm.setAppointmentDateTime(rs.getString("dateofappointment")+rs.getString("timeofappointment"));
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}

		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return whiteBoardForm;
	}

	public int updateSession(DataSource dataSource, String appointmentScheduleId, String option, String whiteBoardSessionID) {
		int flag =0;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs =null;
		try
		{
			System.out.println("Options: "+ option);
			con=dataSource.getConnection();
			String sql="Update whiteborad SET SessionStatus=?  Where appointmentScheduleId=? and whiteBoardSessionID=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1,option);
			pstmt.setString(2,appointmentScheduleId);
			pstmt.setString(3,whiteBoardSessionID);
			pstmt.executeUpdate();
			flag=1;
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		finally	{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return flag;
	}

	public int addVideoSession(WhiteBoardForm whiteBoardForm, DataSource dataSource, String appointmentScheduleId,
			String meetingtitle, String meetingpassword, String option, String meetingName, String meetingURLLink) {
			
			int flag =0;
			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs =null;
			try
			{
				con=dataSource.getConnection();
				String sql="insert into whiteborad(appointmentScheduleId, meetingURl, accessPassword, meetingName, "
						+ "whiteBoardSessionTitle, SessionStatus) values (?, ?, ?, ?, ?, ?)";
				pstmt = con.prepareStatement(sql);
				
				pstmt.setInt(1, Integer.parseInt(appointmentScheduleId));
				pstmt.setString(2,meetingURLLink);
				pstmt.setString(3, meetingpassword);
				pstmt.setString(4, meetingName);
				pstmt.setString(5, meetingtitle);
				pstmt.setString(6,option);
				
				/*pstmt.setInt(1, whiteBoardForm.getAppointmentScheduleId());
				pstmt.setString(2, whiteBoardForm.getWhiteBoardSessionID());
				pstmt.setString(3, whiteBoardForm.getAccessPassword());
				pstmt.setString(4, whiteBoardForm.getWhiteBoardUserName());
				pstmt.setString(5, whiteBoardForm.getWhiteBoardSessionTitle());
				pstmt.setString(6, whiteBoardForm.getSessionStatus());*/
				pstmt.executeUpdate();
				flag=1;
				
			}
			catch (Exception e) 
			{
				e.printStackTrace();
			}

			finally	{
				ConnectionUtil.closeResources(con, pstmt, rs);
			}
			return flag;
	}

	public int updateVideoSession(DataSource dataSource, String appointmentScheduleId, String option) {
		int flag =0;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs =null;
		try
		{
			System.out.println("Options: "+ option);
			con=dataSource.getConnection();
			String sql="Update whiteborad SET SessionStatus=?  Where appointmentScheduleId=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1,option);
			pstmt.setString(2,appointmentScheduleId);
			pstmt.executeUpdate();
			flag=1;
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		finally	{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return flag;
	}

	public WhiteBoardForm getAppointmentDetails(DataSource dataSource, String appointmentScheduleId) {
		WhiteBoardForm whiteBoardForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		 
		try	{
			con=dataSource.getConnection();
			String sql="select * from appointmentschedule aps  inner join coursemater cm on cm.courseid=aps.courseid inner join appointment appt on appt.appointmentID=aps.appointmentID  where aps.appointmentScheduleId=?";//select query
			pstmt=con.prepareStatement(sql);
			pstmt.setString(1,appointmentScheduleId);
			rs=pstmt.executeQuery();
			while(rs.next()){
				whiteBoardForm = new WhiteBoardForm();
				whiteBoardForm.setWhiteBoardSessionTitle(rs.getString("courseName"));
				whiteBoardForm.setAccessPassword("Itech123");
				whiteBoardForm.setAppointmentDateTime(rs.getString("dateofappointment")+rs.getString("timeofappointment"));				 
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}

		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return whiteBoardForm;
	}
	 
}
