package com.itech.elearning.daoimpl;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.sql.DataSource;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;

import com.itech.elearning.forms.TestTypeForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class TestTypeDaoImp {

	public List<TestTypeForm> list(int roleid,String compid, DataSource dataSource) {
		List<TestTypeForm> list = new ArrayList<TestTypeForm>();
		TestTypeForm testTypeForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
        String sql = null;
		try {
			con = dataSource.getConnection();
			if(roleid == 9)
			{
		 sql = "SELECT * FROM testtypemaster ORDER BY TestType ASC";
			}
			else
			{
				 sql = "SELECT * FROM testtypemaster where CompanyId = "+compid+" ORDER BY TestType ASC";
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				testTypeForm = new TestTypeForm();
				testTypeForm.setTestTypeId(rs.getLong("TestTypeId"));
				testTypeForm.setTestType(rs.getString("TestType"));
				testTypeForm.setActive(rs.getBoolean("active"));
				list.add(testTypeForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}

	public String addTestType(String compid, TestTypeForm testTypeForm, DataSource dataSource) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM testtypemaster WHERE TestType=? and CompanyId = '"+compid+"'";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, testTypeForm.getTestType());
			rs = pstmt.executeQuery();
			if (rs.next())
				result = Common.DUPLICATE_NAME_MESSAGE;
			else {
				if (!testTypeForm.getTestType().equalsIgnoreCase("")
						&& !testTypeForm.getTestType().equalsIgnoreCase(null)) {
					sql = "INSERT INTO testtypemaster (TestType,ACTIVE,CompanyId) VALUES (?,?,?)";
					if (pstmt != null)
						pstmt.close();
					pstmt = con.prepareStatement(sql);

					pstmt.setString(1, testTypeForm.getTestType());
					pstmt.setBoolean(2, true);
					pstmt.setString(3, compid);
					pstmt.executeUpdate();
					if (rs != null)
						rs.close();
					result = Common.REC_ADDED;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	public Object updateTestType( TestTypeForm testTypeForm,
			DataSource dataSource) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM testtypemaster WHERE  TestType COLLATE latin1_general_cs LIKE ? ";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, testTypeForm.getTestType());
			rs = pstmt.executeQuery();
			if (rs.next())
				result = Common.No_Change;
			else {
				if (!testTypeForm.getTestType().equalsIgnoreCase("")
						&& !testTypeForm.getTestType().equalsIgnoreCase(null)) {
					sql = "UPDATE testtypemaster SET TestType=? WHERE TestTypeId=?";
					if (pstmt != null)
						pstmt.close();
					pstmt = con.prepareStatement(sql);
					pstmt.setString(1, testTypeForm.getTestType());
					pstmt.setLong(2, testTypeForm.getTestTypeId());
					pstmt.executeUpdate();
					if (rs != null)
						rs.close();
					result = Common.REC_UPDATED;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return result;
	}
	public Object changeTestType(TestTypeForm testTypeForm,
			DataSource dataSource) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = dataSource.getConnection();
			String sql = "UPDATE testtypemaster SET ACTIVE=? WHERE TestTypeId=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setBoolean(1, testTypeForm.isActive());
			pstmt.setLong(2, testTypeForm.getTestTypeId());
			pstmt.executeUpdate();
			result = Common.REC_STATUS_CHANGED;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return result;
	}
	
	public String testTypeExcelUpload(String fileName, String filePath, String compid) {
		   String result=Common.FAILURE_MESSAGE;
		   
		   int daoflag=0;
		   Connection con=null;
		   PreparedStatement pstmt=null;
		   
		   try {
			   
			    con=ConnectionUtil.getMySqlConnection();
	         	long start = System.currentTimeMillis();
	            
	         	
	         	String sql="insert into testtypemaster(TestType,CompanyId,Active) values(?,?,1)";
	              
	            if(pstmt!=null)pstmt.close();
	            pstmt=con.prepareStatement(sql);
	            
	         	InputStream input = new BufferedInputStream(new FileInputStream(filePath+fileName));
	         	
	         	 
	         	System.out.println("Excel File path="+filePath);
	         	System.out.println("Excel File Name="+fileName);
	            POIFSFileSystem fs = new POIFSFileSystem(input);
	            HSSFWorkbook wb = new HSSFWorkbook(fs);
	            HSSFSheet sheet = wb.getSheetAt(0);
	            Iterator<Row> rows = sheet.rowIterator();
	          
	            long count=0;
	            while (rows.hasNext()) {
	            		
	            		 HSSFRow row = (HSSFRow) rows.next();
	                     
	 
	                     String test_type=TestTypeDaoImp.StringNotNull(row.getCell(0));
	                     String company_id=TestTypeDaoImp.StringNotNull(row.getCell(1));
	                    
	                     
	                     
	                     
	                     try {
	                    	 if(daoflag==1){
	                      		count=count+1;
	                         	System.out.println("excel file adding............");
	                   	       
	                   	       	pstmt.setString(1,test_type);
	                   	       	pstmt.setString(2,compid);
	                   	        
	     	                   
	     	               
	     	                    pstmt.addBatch();
	     	                    
	     	                    
	     	                    if(count%1000 == 0) { 
	     	                    	pstmt.executeBatch();
	     	                    	System.out.println("Batch Count: "+count+ " Time Taken: "+(System.currentTimeMillis()-start)+" MilliSecons");
	     	                    }            
	     	                 
	     	                    
	     	                    System.out.println("count="+count);
	     	                   //result=count+" "+"Records uploaded";
	     	                    
	     	                    System.out.println("%%%%%%%%%%%%%"+result);
	     	                 }
	                 	} catch (Exception einner) {
	                 		einner.printStackTrace();
						}
	                      
	                      daoflag=1;
	            }
	            pstmt.executeBatch();
	            System.out.println("Batch Count: "+count+ " Time Taken: "+(System.currentTimeMillis()-start)+" MilliSecons");
	        
		   } catch (Exception e) {
	            e.printStackTrace();
	        }
	        finally{
	            ConnectionUtil.closeResources(con, pstmt);
	        }
	        result=Common.REC_ADDED;

	        return result; 
			//return flag;
	    }    
	
	 public static String StringNotNull(HSSFCell rowValue){
		   String results="";
		   if(rowValue!=null) results =rowValue.toString().trim();
		   return results;
		   
	   }

}
