package com.itech.elearning.daoimpl;

import java.sql.Connection;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.json.simple.JSONObject;

import com.itech.elearning.forms.EBookDetailsMasterForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class EBookDetailsMasterDao {

	public List<EBookDetailsMasterForm> listAllCourse(int roleid,String compid, Long userid, DataSource ds) {
		List<EBookDetailsMasterForm> courseList = new ArrayList<EBookDetailsMasterForm>();
		EBookDetailsMasterForm ebForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			if(roleid==9)
			{
			 sql = "SELECT * FROM coursemater WHERE ACTIVE=TRUE ";
			}
			else
			{
		       sql = "SELECT * FROM coursemater WHERE ACTIVE=TRUE and CompanyId= '"+compid+"'";
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				ebForm = new EBookDetailsMasterForm();
				ebForm.setCourseId(rs.getLong("CourseId"));
				ebForm.setCourseName(rs.getString("CourseName"));
				courseList.add(ebForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return courseList;
	}

	@SuppressWarnings("unchecked")
	public List<EBookDetailsMasterForm> list(int roleid,String compid, Long userid, DataSource ds, Long courseId) {

		List<EBookDetailsMasterForm> list = new ArrayList<EBookDetailsMasterForm>();
		EBookDetailsMasterForm ebForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			if(roleid==9)
			{
			sql = "SELECT * FROM EBOOKNAMEMASTER AS NM  JOIN coursemater AS CM ON (CM.COURSEID=NM.COURSEID) ";
			}
			else
			{
				sql = "SELECT * FROM EBOOKNAMEMASTER AS NM  JOIN coursemater AS CM ON (CM.COURSEID=NM.COURSEID) where NM.CompanyId = '"+compid+"' ";	
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				ebForm = new EBookDetailsMasterForm();
				ebForm.setCourseId(rs.getLong("CourseId"));
				ebForm.setCourseName(rs.getString("CourseName"));
				ebForm.seteBookID(rs.getLong("EBOOKNAMEID"));
				ebForm.seteBookName(rs.getString("EBOOKNAME"));
				ebForm.setFileName(rs.getString("FILENAME"));
				ebForm.setActive(rs.getBoolean("Active"));
				
				JSONObject obj=new JSONObject();
				obj.put("courseId",rs.getLong("CourseId"));
				obj.put("courseName",rs.getString("CourseName"));
				obj.put("eBookID",rs.getLong("EBOOKNAMEID"));
				obj.put("eBookName",rs.getString("EBOOKNAME"));
				obj.put("fileName",rs.getString("FILENAME"));
				
				ebForm.setjEdit(obj.toString());
				list.add(ebForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		} 
		return list;
	}
	
	public String add(String compid, EBookDetailsMasterForm ebForm, DataSource dataSource) {
		String result = Common.FAILURE_MESSAGE;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM EBOOKNAMEMASTER WHERE EBOOKNAME=? and CourseId=?   and FILENAME=? and CompanyId=? ";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1,ebForm.geteBookName());
			pstmt.setLong(2,ebForm.getCourseId());
			pstmt.setString(3,ebForm.getFileName());
			pstmt.setString(4,compid);
			
			rs = pstmt.executeQuery();
			if (rs.next()) {
				result = Common.DUPLICATE_NAME_MESSAGE;
			} else {
				sql = "INSERT INTO EBOOKNAMEMASTER (EBOOKNAME,CourseId,FILENAME,ACTIVE,CompanyId) VALUES(?,?,?,?,?)";
				if (pstmt != null)	pstmt.close();
				pstmt = con.prepareStatement(sql);
				pstmt.setString(1,ebForm.geteBookName());
				pstmt.setLong(2,ebForm.getCourseId());
				pstmt.setString(3,ebForm.getFileName());
				pstmt.setBoolean(4, true);
				pstmt.setString(5,compid);
				pstmt.executeUpdate();
				result = Common.REC_ADDED;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	public String changeStatus(EBookDetailsMasterForm ebForm, DataSource dataSource, Long eBookID, boolean active) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = dataSource.getConnection();
			String sql = "UPDATE EBOOKNAMEMASTER SET ACTIVE=? WHERE EBOOKNAMEID=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setBoolean(1,active);
			pstmt.setLong(2, eBookID);
			pstmt.executeUpdate();
			result = Common.REC_STATUS_CHANGED;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	

	public String update(EBookDetailsMasterForm ebForm, DataSource ds) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			sql = "SELECT * FROM EBOOKNAMEMASTER WHERE EBOOKNAME=? and CourseId=? and FILENAME=? ";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1,ebForm.geteBookName());
			pstmt.setLong(2,ebForm.getCourseId());
			pstmt.setString(3,ebForm.getFileName());

			rs = pstmt.executeQuery();

			if (rs.next()) {
				result = Common.No_Change;
			} else {
					sql = "UPDATE EBOOKNAMEMASTER SET EBOOKNAME=?, CourseId=?, FILENAME=? where EBOOKNAMEID=?";
					if (pstmt != null)	pstmt.close();
					pstmt = con.prepareStatement(sql);
					pstmt.setString(1,ebForm.geteBookName());
					pstmt.setLong(2,ebForm.getCourseId());
					pstmt.setString(3,ebForm.getFileName());
					pstmt.setLong(4,ebForm.geteBookID());
					pstmt.executeUpdate();
					result = Common.REC_UPDATED;
				 
			} 

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;

	}

	 
}
