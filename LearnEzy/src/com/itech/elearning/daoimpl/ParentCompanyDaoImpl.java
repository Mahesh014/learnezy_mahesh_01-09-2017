package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.elearning.forms.ParentCompanyForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class ParentCompanyDaoImpl {

	public List<ParentCompanyForm> listAll(int roleid, String compid,
			DataSource dataSource) {
	
		List<ParentCompanyForm> list = new ArrayList<ParentCompanyForm>();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ParentCompanyForm countryForm = null;
		String sql = null;
		try	{
			con=dataSource.getConnection();
		
		    sql ="SELECT * FROM parentcompanymaster ORDER BY pcompanyname ASC";//select query

			
			pstmt=con.prepareStatement(sql);
			rs=pstmt.executeQuery();
			while(rs.next()){
				countryForm = new ParentCompanyForm();
				countryForm.setPcompid(rs.getInt("Pcompid"));
				countryForm.setPcompanyname(rs.getString("pcompanyname"));
				countryForm.setActive(rs.getBoolean("ACTIVE"));
				list.add(countryForm);
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}

		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return list;
	}

	public String add(ParentCompanyForm countryForm, String compid,
			DataSource dataSource) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs =null;
		try
		{
			con=dataSource.getConnection();
			String sql="select * from parentcompanymaster where pcompanyname=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, countryForm.getPcompanyname());
			rs = pstmt.executeQuery();
			if(rs.next()) result=Common.DUPLICATE_NAME_MESSAGE;
			else{
				if(!countryForm.getPcompanyname().equalsIgnoreCase("") && !countryForm.getPcompanyname().equalsIgnoreCase(null)){
					sql="insert into parentcompanymaster (pcompanyname,ACTIVE) values (?,?)";
					pstmt = con.prepareStatement(sql);
					pstmt.setString(1, countryForm.getPcompanyname());
					pstmt.setBoolean(2, true);
					pstmt.executeUpdate();
					result=Common.REC_ADDED;
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		finally	{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return result;
	}

	public String changeStatus(ParentCompanyForm countryForm,
			DataSource dataSource) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		try	{
			con=dataSource.getConnection();
			String sql="update parentcompanymaster set ACTIVE = ? where Pcompid=?";
			pstmt=con.prepareStatement(sql);
			pstmt.setBoolean(1,countryForm.isActive());
			pstmt.setInt(2, countryForm.getPcompid());
			pstmt.executeUpdate();
			result=Common.REC_STATUS_CHANGED;
		}
		catch (Exception e){
			System.out.println(e);
		} finally{
			ConnectionUtil.closeResources(con, pstmt);
		}
		return result;
	}

	public String update(ParentCompanyForm countryForm, DataSource dataSource) {
		// TODO Auto-generated method stub
		String result=Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		try{
			con=dataSource.getConnection();
			String sql="select * from parentcompanymaster where pcompanyname=?";
			pstmt=con.prepareStatement(sql);
			pstmt.setString(1,countryForm.getPcompanyname());
			rs = pstmt.executeQuery();
			if(rs.next())	result=Common.No_Change;
			else{
					sql="UPDATE parentcompanymaster SET pcompanyname=? WHERE Pcompid=?";
					pstmt=con.prepareStatement(sql);
					pstmt.setString(1, countryForm.getPcompanyname());
					pstmt.setLong(2, countryForm.getPcompid());
					pstmt.executeUpdate();
					result=Common.REC_UPDATED;
			}
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return result;
	}

	public JSONArray ajaxActiveList(DataSource dataSource, String countryid) {
		JSONArray jArray= new JSONArray();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql=null;
		try{
			con=dataSource.getConnection();
			sql="select * from parentcompanymaster WHERE Pcompid=?";//select query
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1,countryid);
			rs = pstmt.executeQuery();
			while(rs.next()){
				JSONObject obj= new JSONObject();
				obj.put("countryid",rs.getInt("Pcompid"));
				obj.put("countryname",rs.getString("pcompanyname"));
				jArray.add(obj);
				System.out.println(sql);
			}
		}
		catch (Exception e){
		   e.printStackTrace();
		}
		
		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;
	}

	
	
}
