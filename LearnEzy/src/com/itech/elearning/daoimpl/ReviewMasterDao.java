package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.elearning.forms.CityForm;
import com.itech.elearning.forms.ReviewMasterForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class ReviewMasterDao {

	public List<ReviewMasterForm> list(String compid, DataSource dataSource) {
		List<ReviewMasterForm> reviewList = new ArrayList<ReviewMasterForm>();
		ReviewMasterForm reviewForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql=null;
		try	{
			con=dataSource.getConnection();
			
				sql="select r.*,cr.*,co.* from reviewmaster r join coursemater cr on r.courseId=cr.CourseId join companymaster co on r.companyId=co.CompanyId ";
				pstmt = con.prepareStatement(sql);
				rs=pstmt.executeQuery();
			
				while(rs.next()){
				reviewForm = new ReviewMasterForm();
				reviewForm.setReviewId(rs.getInt("reviewId"));
				reviewForm.setReviewDescription(rs.getString("reviewDescription"));
				
				reviewForm.setCourseId(rs.getInt("courseId"));
				reviewForm.setCourseName(rs.getString("CourseName"));
				
				reviewForm.setCompanyId(rs.getInt("companyId"));
				reviewForm.setCompanyName(rs.getString("CompanyName"));
				
				reviewForm.setCreateDateTime(rs.getString("createdDateTime"));
				
				reviewForm.setApprovalStatus(rs.getBoolean("approvalStatus"));
				reviewForm.setActive(rs.getBoolean("active"));
				reviewForm.setEmailid(rs.getString("emailid"));
				reviewList.add(reviewForm);
				
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return reviewList;
	}

	public Object add(ReviewMasterForm reviewForm, String compid, Long userid,
			DataSource dataSource) {
		String result = Common.FAILURE_MESSAGE;
	    Connection con = null;
	    PreparedStatement pstmt = null;
	    try
	    {
	    	con=dataSource.getConnection();
			String sql="select * from reviewmaster where reviewDescription=? and  courseId=? ";
	    	pstmt = con.prepareStatement(sql);
	    	pstmt.setString(1, reviewForm.getReviewDescription());
	    	pstmt.setLong(2, reviewForm.getCourseList());
	    	
	    	ResultSet rs = pstmt.executeQuery();
	    	if(rs.next())result = Common.DUPLICATE_NAME_MESSAGE;
	    	else {
	    		
	    			sql="insert into reviewmaster(userId,reviewDescription,courseId,companyId,approvalStatus,active) values(?,?,?,?,?,?) ";
	    			pstmt=con.prepareStatement(sql);
	    			pstmt.setLong(1, userid);
	    			pstmt.setString(2, reviewForm.getReviewDescription());
	    	    	pstmt.setInt(3,reviewForm.getCourseList());
	    	    	pstmt.setString(4, compid);
	    	    	pstmt.setBoolean(5, false);
	    			pstmt.setBoolean(6, true);
	    			
	    			pstmt.executeUpdate();
	    			result = Common.REC_ADDED;
	    		}
	    	
	    }
	    catch (Exception e) 
	    {
			e.printStackTrace();
		}
	    finally{
			ConnectionUtil.closeResources(con, pstmt);
		}
		return result;
	}


	public Object changeStatus(ReviewMasterForm reviewForm, DataSource dataSource) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		try
		{
			con=dataSource.getConnection();
			String sql="UPDATE reviewmaster SET active=? WHERE reviewId=?";
			pstmt=con.prepareStatement(sql);
			pstmt.setBoolean(1,reviewForm.isActive());
			pstmt.setInt(2, reviewForm.getReviewId());
			pstmt.executeUpdate();
			result = Common.REC_STATUS_CHANGED;
		}
		catch (Exception e) 
		{
		  e.printStackTrace();
		}
		finally{
			ConnectionUtil.closeResources(con, pstmt);
		}
		return result;
	}

	

	

	public List<ReviewMasterForm> reviewlist(DataSource dataSource, String letter) {
		List<ReviewMasterForm> reviewList = new ArrayList<ReviewMasterForm>();
		ReviewMasterForm reviewForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql=null;
		try	{
			con=dataSource.getConnection();
			
				sql="select rt.*,rm.* from ratingtable rt  join reviewmaster rm on rt.courseId=rm.courseId  where rm.courseId ='"+letter+"' limit 1";
				pstmt = con.prepareStatement(sql);
				rs=pstmt.executeQuery();
			
				while(rs.next()){
				reviewForm = new ReviewMasterForm();
			
				reviewForm.setReviewDescription(rs.getString("reviewDescription"));
				
				reviewForm.setCourseId(rs.getInt("courseId"));
				
				int rating =rs.getInt("rating");
				 
				if (rating==0) {
					reviewForm.setRating("images/smallrating-1.png");
				}
				
				if (rating==1) {
					reviewForm.setRating("images/smallrating-1.png");
			}
				if (rating==2) {
					reviewForm.setRating("images/smallrating-2.png");
			}
				
				if (rating==3) {
					reviewForm.setRating("images/smallrating-3.png");
			}
				if (rating==4) {
					reviewForm.setRating("images/smallrating-4.png");
			}
				if (rating==5) {
					reviewForm.setRating("images/smallrating-5.png");
			}
				
				
				reviewForm.setCreateDateTime(rs.getString("createdDateTime"));
			
				reviewForm.setEmailid(rs.getString("emailid"));
				
				reviewList.add(reviewForm);
				
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return reviewList;
	}

	public List<ReviewMasterForm> reviewlistviewmore(DataSource dataSource) {
		List<ReviewMasterForm> reviewList = new ArrayList<ReviewMasterForm>();
		ReviewMasterForm reviewForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql=null;
		try	{
			con=dataSource.getConnection();
			
				sql="select rt.*,rm.* from ratingtable rt  join reviewmaster rm on rt.courseId=rm.courseId";
				pstmt = con.prepareStatement(sql);
				rs=pstmt.executeQuery();
			
				while(rs.next()){
				reviewForm = new ReviewMasterForm();
				//reviewForm.setReviewId(rs.getInt("reviewId"));
				reviewForm.setReviewDescription(rs.getString("reviewDescription"));
				
				reviewForm.setCourseId(rs.getInt("courseId"));
				
				int rating =rs.getInt("rating");
				 
				if (rating==0) {
					reviewForm.setRating("images/smallrating-1.png");
				}
				
				if (rating==1) {
					reviewForm.setRating("images/smallrating-1.png");
			}
				if (rating==2) {
					reviewForm.setRating("images/smallrating-2.png");
			}
				
				if (rating==3) {
					reviewForm.setRating("images/smallrating-3.png");
			}
				if (rating==4) {
					reviewForm.setRating("images/smallrating-4.png");
			}
				if (rating==5) {
					reviewForm.setRating("images/smallrating-5.png");
			}
				
				reviewForm.setCreateDateTime(rs.getString("createdDateTime"));
				
				
				reviewForm.setEmailid(rs.getString("emailid"));
				
				reviewList.add(reviewForm);
				
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return reviewList;
	}

}