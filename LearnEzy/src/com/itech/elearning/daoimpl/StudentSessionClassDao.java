package com.itech.elearning.daoimpl;

import java.sql.Connection;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.elearning.forms.StudentSessionClassForm;
import com.itech.elearning.utils.ConnectionUtil;

public class StudentSessionClassDao {
	public List<StudentSessionClassForm> appointmentUserlist(DataSource dataSource, Long userid) {
		List<StudentSessionClassForm> list = new ArrayList<StudentSessionClassForm>();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		StudentSessionClassForm studentSessionClassForm = null;
		try	{
			con=dataSource.getConnection();
			String sql="select * from appointmentschedule ass inner join appointment a on a.appointmentID = ass.appointmentId inner join coursemater cm on cm.CourseId=ass.courseid left join whiteborad wb on wb.appointmentScheduleId = ass.appointmentScheduleId where  find_in_set('"+userid+"',ass.studentId)";//select query
			pstmt=con.prepareStatement(sql);
			rs=pstmt.executeQuery();
			System.out.println(sql);
			while(rs.next()){
				studentSessionClassForm = new StudentSessionClassForm();	
				studentSessionClassForm.setAppointmentScheduleId(rs.getInt("appointmentScheduleId"));
				studentSessionClassForm.setAppointmentCreatedID(rs.getInt("appointmentCreatedId"));
				studentSessionClassForm.setCourseid(rs.getInt("courseId"));
				studentSessionClassForm.setStatus(rs.getString("status"));
				studentSessionClassForm.setAppointmentType(rs.getString("appointmentType"));
				studentSessionClassForm.setCourseName(rs.getString("CourseName"));
				studentSessionClassForm.setAppointmentDateTime(rs.getString("dateofappointment") +" "+ rs.getString("timeofappointment"));
				studentSessionClassForm.setActive(rs.getBoolean("ACTIVE"));
				studentSessionClassForm.setSessionStatus(rs.getString("SessionStatus"));
				studentSessionClassForm.setMeetingName(rs.getString("meetingName"));
				studentSessionClassForm.setMeetingURLLink(rs.getString("meetingURl"));
				list.add(studentSessionClassForm);
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}

		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return list;
	}

	public StudentSessionClassForm getWhiteBoardSessionID(DataSource dataSource, String appointmentScheduleId) {
		StudentSessionClassForm studentSessionClassForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		 
		try	{
			con=dataSource.getConnection();
			String sql="select * from whiteborad  where appointmentScheduleId=?";//select query
			pstmt=con.prepareStatement(sql);
			pstmt.setString(1, appointmentScheduleId);
			rs=pstmt.executeQuery();
			while(rs.next()){
				studentSessionClassForm = new StudentSessionClassForm();
				studentSessionClassForm.setWhiteBoardSessionID(rs.getString("whiteBoardSessionID"));
				studentSessionClassForm.setWhiteBoardSessionTitle(rs.getString("whiteBoardSessionTitle"));
				studentSessionClassForm.setAccessPassword(rs.getString("accessPassword"));
				studentSessionClassForm.setSessionStatus(rs.getString("SessionStatus"));
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}

		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return studentSessionClassForm;
	}

	@SuppressWarnings("unchecked")
	public JSONArray getStudentSessionList(DataSource dataSource, Long userid) {
		JSONArray jArray = new JSONArray();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String sql="select * from appointmentschedule ass inner join appointment a on a.appointmentID = ass.appointmentId inner join coursemater cm on cm.CourseId=ass.courseid left join whiteborad wb on wb.appointmentScheduleId = ass.appointmentScheduleId where  find_in_set('"+userid+"',ass.studentId)";//select query
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				JSONObject obj = new JSONObject();
				obj.put("appointmentScheduleId",rs.getInt("appointmentScheduleId"));
				obj.put("appointmentCreatedID",rs.getInt("appointmentCreatedId"));
				obj.put("courseid",rs.getInt("courseId"));
				obj.put("status",rs.getString("status"));
				obj.put("appointmentType",rs.getString("appointmentType"));
				obj.put("courseName",rs.getString("CourseName"));
				obj.put("appointmentDateTime",rs.getString("dateofappointment") +" "+ rs.getString("timeofappointment"));
				obj.put("active",rs.getBoolean("ACTIVE"));
				obj.put("sessionStatus",rs.getString("SessionStatus"));
				obj.put("meetingURl",rs.getString("meetingURl"));
				obj.put("meetingName",rs.getString("meetingName"));
				jArray.add(obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;
	}
	
}
