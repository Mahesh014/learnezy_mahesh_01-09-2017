package com.itech.elearning.daoimpl;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.elearning.forms.GetCertificateForm;
import com.itech.elearning.utils.ConnectionUtil;

public class GetCertificateDaoImpl {

	@SuppressWarnings("unchecked")
	public JSONArray courseList(String compid, Long userid, DataSource dataSource) {
		
		JSONArray jArray= new JSONArray();
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		Connection con=null;
		 
	try{
		con = dataSource.getConnection();
		ArrayList<Integer> courseCompleteList= GetCertificateDaoImpl.courseCompleteList(userid, dataSource);
		if (!courseCompleteList.isEmpty()) {
			String idList = courseCompleteList.toString();
			String courseIdsList = idList.substring(1, idList.length() - 1).replace(", ", ",");
			System.out.println("courseIds List: " +courseIdsList);
			String sql = "select CourseId,CourseName FROM coursemater where CourseId in ("+courseIdsList+") and  CompanyId in ("+compid+"); ";
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				JSONObject obj = new JSONObject();
				obj.put("courseId", rs.getString("CourseId"));
				obj.put("courseName", rs.getString("CourseName"));
				jArray.add(obj);
			}
		}
		
	} catch (Exception e) {
		e.printStackTrace();
	}	
	finally {
		ConnectionUtil.closeResources(con, pstmt, rs);
	}
		return jArray;
	}
		
		
			
	public static ArrayList<Integer> courseCompleteList(Long userid, DataSource dataSource) {
		ArrayList<Integer> courseCompleteList = new ArrayList<Integer>();
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		
		PreparedStatement pstmt1=null;
		ResultSet rs1=null;
		
		PreparedStatement pstmt2=null;
		ResultSet rs2=null;
		
		Connection con=null;
		String courseIdsList=null;
		int testId=0;
		int courseId=0;
		boolean testPassFlag=true;
		int courseCompCount=0;
		try{
			con = dataSource.getConnection();
			String sql = "select CourseId FROM studentcoursedetails where StudentId=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, userid);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				courseIdsList=rs.getString("CourseId");
			}
			String[] courseIdsArray=courseIdsList.split(",");
			for (int j = 0; j < courseIdsArray.length; j++) {
				System.out.println("Course: "+courseIdsArray[j]);
				courseId=Integer.parseInt(courseIdsArray[j]);
				
				String sql1 = "select * from testmaster  where courseid='"+courseId+"' and Active=1";
				pstmt1 = con.prepareStatement(sql1);
				//pstmt.setInt(1, courseId);
				rs1 = pstmt1.executeQuery();
				System.out.println(sql1);
				while (rs1.next()) {
					testId=rs1.getInt("testId");
					
					String sql2 = "select * from testanswer where passStatus='pass' and testId='"+testId+"' and studentid="+userid;
					pstmt2 = con.prepareStatement(sql2);
					/*pstmt.setInt(1, testId);
					pstmt.setLong(2, userid)*/;
					rs2 = pstmt2.executeQuery();
					System.out.println(sql2);
					System.out.println("TestID :"+testId);
					if(rs2.next()) {
						System.out.println("TestID PASSS:"+testId);
					}else{
						testPassFlag=false;
						System.out.println("TestID FAILS:"+testId);
					}
					if (testPassFlag) {
						courseCompCount=courseCompCount+1;
						courseCompleteList.add(courseId);
					}
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}	finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
			return courseCompleteList;
	}

	public int courseCompletedCheck( String compid, DataSource dataSource,Long userid, int courseId ) {
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		
		PreparedStatement pstmt1=null;
		ResultSet rs1=null;
		
		PreparedStatement pstmt2=null;
		ResultSet rs2=null;
		
		Connection con=null;
		int testId=0;
		int courseCompStatus=0;
		try{
			con = dataSource.getConnection();
			String sql1 = "select * from testmaster  where courseid='"+courseId+"' and Active=1 and CompanyId = '"+compid+"'";
				pstmt1 = con.prepareStatement(sql1);
				//pstmt.setInt(1, courseId);
				rs1 = pstmt1.executeQuery();
				System.out.println(sql1);
				while (rs1.next()) {
					testId=rs1.getInt("testId");
					
					String sql2 = "select * from testanswer where passStatus='pass' and testId='"+testId+"' and CompanyId = '"+compid+"' and studentid="+userid ;
					pstmt2 = con.prepareStatement(sql2);
					/*pstmt.setInt(1, testId);
					pstmt.setLong(2, userid)*/;
					rs2 = pstmt2.executeQuery();
					System.out.println(sql2);
					System.out.println("TestID :"+testId);
					if(rs2.next()) {
						System.out.println("TestID PASSS:"+testId);
						courseCompStatus=1;
					}else{
						
						System.out.println("TestID FAILS:"+testId);
					}
					 
				}
		 } catch (Exception e) {
			e.printStackTrace();
		}	finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
			return courseCompStatus;
	}


	public List<GetCertificateForm> getCertificateDetials(String compid, Long userid,DataSource dataSource, String courseId) {
		List<GetCertificateForm> list = new ArrayList<GetCertificateForm>(); 
		GetCertificateForm GCForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String testIds=null;
		String courseComplDate="";
		double maxScore=0;
		double noTestTaken=0;
		try {
			con = dataSource.getConnection();
			
			String sql = "SELECT GROUP_CONCAT(TestId) as testIds  FROM testmaster where CompanyId= '"+compid+"' CourseId="+courseId;
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				testIds = rs.getString("testIds");
			}
			
			sql = "select max(passPercentage) as passPercentage1 from testanswer where TestId in ("+testIds+") and studentId='"+userid+"' and CompanyId= '"+compid+"' group by TestId";
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				maxScore=maxScore+rs.getDouble("passPercentage1");
				noTestTaken=noTestTaken+1;
			}
			
			int passPercentage=(int) (maxScore/noTestTaken);
			
			
			sql = "select max(testTakenDate) as testTakenDate1 from testanswer where TestId in ("+testIds+") and studentId='"+userid+"' CompanyId= '"+compid+"' group by TestId";
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				courseComplDate=rs.getString("testTakenDate1");
				
			}
			
			sql = "select * from  usermaster as um, coursemater cm where cm.courseid=? and um.userid=? and um.CompanyId= '"+compid+"'";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, courseId);
			pstmt.setLong(2, userid);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				GCForm = new GetCertificateForm();
				GCForm.setStudentName(rs.getString("FIRSTNAME") + " "+ rs.getString("LASTNAME"));
				GCForm.setCourseName(rs.getString("CourseName"));
				GCForm.setPasspercent(String.valueOf(passPercentage));
				GCForm.setCourseComplDate(courseComplDate);
				list.add(GCForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		
		}
		return list;
	}
	

}
