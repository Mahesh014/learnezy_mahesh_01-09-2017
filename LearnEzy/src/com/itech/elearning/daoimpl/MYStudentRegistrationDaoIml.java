package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.elearning.forms.CompanyMasterForm;
import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.forms.StudentRegistrationForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;
import com.itech.elearning.utils.EncryptUtil;
import com.itech.elearning.utils.Learnezysms;
import com.itech.elearning.utils.MailSendingUtil;

public class MYStudentRegistrationDaoIml {

	public int addUser(String compid, StudentRegistrationForm studentForm,DataSource ds, String courseList, String profilePhotoName,int paymentStatus) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int i=0;
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		
		PreparedStatement pstmt2 = null;
		Connection con = null;
		int roleid = 3;
		int userId=0;
		System.out.println("theCourseList: "+courseList);
		try {
			con = ds.getConnection();
			String sql = "SELECT * FROM USERMASTER WHERE USERNAME=? and CompanyId='"+compid+"'";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, studentForm.getUname());
			rs = pstmt.executeQuery();
			if (rs.next()) {
			} else {
				sql = "INSERT INTO USERMASTER (UserName,Password,FIRSTNAME,MiddleName,LASTNAME,DateofBirth,Gender,Qualification,ContactNumber,Address,Phone,emailid,RoleID,ACTIVE,registerddate,profilePhoto,paymentStatus,countryId, stateId, cityId, pincode,CompanyId) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now(),?,?,?,?,?,?)";
				if (pstmt != null) pstmt.close();
				pstmt = con.prepareStatement(sql);
				pstmt.setString(1, studentForm.getUname());
				pstmt.setString(2, EncryptUtil.encrypt(studentForm.getPassword()));
				pstmt.setString(3, studentForm.getFirstName());
				pstmt.setString(4, studentForm.getMiddleName());
				pstmt.setString(5, studentForm.getLastName());
				
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); // your template here
				java.util.Date dateStr = formatter.parse(studentForm.getDob());
				new java.sql.Date(dateStr.getTime());
				
				pstmt.setString(6, "ghghghgh");				
				pstmt.setString(7, studentForm.getGender());
				pstmt.setString(8, studentForm.getQualification());
				pstmt.setString(9, studentForm.getContactno());
				pstmt.setString(10,studentForm.getAddress());
				pstmt.setString(11,studentForm.getPhone());
				pstmt.setString(12,studentForm.getEmailId());
				pstmt.setInt(13,roleid);
				pstmt.setBoolean(14,true);
				pstmt.setString(15,profilePhotoName);
				pstmt.setInt(16,paymentStatus);
				pstmt.setInt(17,studentForm.getCountryid());
				pstmt.setInt(18,studentForm.getStateid());
				pstmt.setInt(19,studentForm.getCityid());
				pstmt.setInt(20,studentForm.getPincode());
				pstmt.setString(21,compid);
				pstmt.executeUpdate();
				
				Learnezysms.getmobilenumber(studentForm.getContactno());
				sql = "SELECT UserId FROM USERMASTER WHERE USERNAME=? and CompanyId='"+compid+"'";
				pstmt1 = con.prepareStatement(sql);
				pstmt1.setString(1, studentForm.getUname());
				rs1 = pstmt1.executeQuery();
				if (rs1.next()){
					userId = rs1.getInt("UserId");
					System.out.println(userId);
				}
				if(userId!=0){
					sql = "INSERT INTO studentcoursedetails (StudentId,CourseId,CourseStatus,CompanyId) VALUES(?,?,?,?)";
					pstmt2 = con.prepareStatement(sql);
					pstmt2.setLong(1, userId);
					pstmt2.setString(2, courseList);
					pstmt2.setString(3,"Registered");
					pstmt2.setString(4, compid);
					pstmt2.executeUpdate();
					System.out.println(userId+sql);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		
		return userId;
	}

	public List<StudentRegistrationForm> studentlist(DataSource dataSource) {
		
		List<StudentRegistrationForm> list = new ArrayList<StudentRegistrationForm>();
		StudentRegistrationForm form = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM USERMASTER ORDER BY FirstName ASC";
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				form = new StudentRegistrationForm();
				form.setStudenid(rs.getInt(""));
				form.setFirstName(rs.getString("")+" "+rs.getString(""));
				form.setPhone(rs.getString(""));
				form.setEmailId(rs.getString(""));
				form.setAddress(rs.getString(""));
				form.setActive(rs.getBoolean("active"));
				list.add(form);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;

	}
public List<StudentRegistrationForm> courselist1(String compid, DataSource ds) {
		


	List<StudentRegistrationForm> list = new ArrayList<StudentRegistrationForm>();
	StudentRegistrationForm form = null;
	Connection con = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	try {
		con = ds.getConnection();
		String sql = "SELECT * FROM coursemater WHERE ACTIVE=TRUE";
		pstmt = con.prepareStatement(sql);
		rs = pstmt.executeQuery();
		form = new StudentRegistrationForm();form.setCourseid(-1);
		
		while (rs.next()) {
			form = new StudentRegistrationForm();
			form.setCourseid(rs.getInt("CourseId"));
			form.setCoursename(rs.getString("CourseName"));
			list.add(form);
		}
	} catch (Exception e) {
		e.printStackTrace();
	} finally {
		try {
			ConnectionUtil.closeResources(con, pstmt, rs);
		} catch (Exception e2) {
		}
	}
	return list;



	}
	

	public List<StudentRegistrationForm> courselist(String compid, DataSource ds) {
		

		List<StudentRegistrationForm> list = new ArrayList<StudentRegistrationForm>();
		StudentRegistrationForm form = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ds.getConnection();
			String sql = "SELECT * FROM licensemaster WHERE ACTIVE=TRUE ";
			System.out.println("Vani Testing "+sql);
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			form = new StudentRegistrationForm();form.setCourseid(-1);
			
			while (rs.next()) {
				form = new StudentRegistrationForm();
				form.setCourseid(rs.getInt("licenseautoid"));
				form.setCoursename(rs.getString("licenseName"));
				list.add(form);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;


	}

	public Object getCourseBasedOnIds(String courseIdsList,
			DataSource dataSource) {
		
		
		
		List<LoginForm> list = new ArrayList<LoginForm>();
		LoginForm loginForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
	
		
		try {
			
			
			con = dataSource.getConnection();
			String sql = "select CourseId,CourseName FROM coursemater where CourseId in ("+courseIdsList+");";
			System.out.println(sql);
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				loginForm = new LoginForm();
				
				loginForm.setCourseid(rs.getInt("CourseId"));
				loginForm.setCoursename(rs.getString("CourseName"));
				
				list.add(loginForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;

	}
	
	public int checkUserName(DataSource dataSource, String userName) {
		int flag=1;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM USERMASTER where username=? ";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, userName);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				flag=0;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return flag;
	}

	
	
	public StudentRegistrationForm getStudentData(String userId, String courseIdsList, DataSource ds) {
		StudentRegistrationForm form = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ds.getConnection();
			String sql = "SELECT * FROM usermaster WHERE UserId="+userId;

			System.out.println(sql);
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();

			while (rs.next()) {

				form = new StudentRegistrationForm();			
				
				form.setFirstName(rs.getString("FirstName"));
				form.setLastName(rs.getString("LastName"));
				form.setGender(rs.getString("Gender"));
				form.setDob(rs.getString("DateofBirth"));
				form.setQualification(rs.getString("Qualification"));
				form.setContactno(rs.getString("ContactNumber"));
				form.setAddress(rs.getString("Address"));
				form.setEmailId(rs.getString("emailid"));
				form.setPhone(rs.getString("Phone"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return form;
	}

	public int  updatePaymentStatus(DataSource dataSource, String userId, int paymentStatus) {
		int flag=0;
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = dataSource.getConnection();
			System.out.println("Vani checking "+paymentStatus);
			String sql = "UPDATE USERMASTER SET paymentStatus=? WHERE USERID=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, paymentStatus);
			pstmt.setString(2, userId);
			pstmt.executeUpdate();
			flag=1;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return flag;

	}

	public int userRegistration(StudentRegistrationForm studentForm,String compid, DataSource dataSource, String profilePhotoName) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		   //get current date time with Date()
		   Date date = new Date();
		   dateFormat.format(date);
		   System.out.println(dateFormat.format(date));
		Connection con = null;
		int roleid = 3;
		int userId=0;
		int i=0;
		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM USERMASTER WHERE USERNAME=? and CompanyId= '"+compid+"'";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, studentForm.getUname());
			rs = pstmt.executeQuery();
			if (rs.next()) {
			} else {
				sql = "INSERT INTO USERMASTER (UserName,Password,FIRSTNAME,MiddleName,LASTNAME,DateofBirth,Gender,Qualification,ContactNumber,Address,Phone,emailid,RoleID,ACTIVE,profilePhoto,paymentStatus,countryId, stateId, cityId, pincode,otherCountry,otherState,otherCity,CompanyId) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				if (pstmt != null) pstmt.close();
				pstmt = con.prepareStatement(sql);
				pstmt.setString(1, studentForm.getUname());
				pstmt.setString(2, EncryptUtil.encrypt(studentForm.getPassword()));
				pstmt.setString(3, studentForm.getFirstName());
				pstmt.setString(4, studentForm.getMiddleName());
				pstmt.setString(5, studentForm.getLastName());
				pstmt.setString(6, studentForm.getDob());	
				System.out.println("dob "+studentForm.getDob());
				pstmt.setString(7, studentForm.getGender());
				pstmt.setString(8, studentForm.getQualification());
				pstmt.setString(9, studentForm.getContactno());
				pstmt.setString(10,studentForm.getAddress());
				pstmt.setString(11,studentForm.getPhone());
				pstmt.setString(12,studentForm.getEmailId());
				pstmt.setInt(13,roleid);
				pstmt.setBoolean(14,true);
				pstmt.setString(15,profilePhotoName);
				pstmt.setInt(16,0);
				pstmt.setInt(17,studentForm.getCountryid());
				pstmt.setInt(18,studentForm.getStateid());
				pstmt.setInt(19,studentForm.getCityid());
				pstmt.setInt(20,studentForm.getPincode());
				pstmt.setString(21,studentForm.getOthercountryname());
				pstmt.setString(22,studentForm.getOtherstatename());
				pstmt.setString(23,studentForm.getOthercityname());
				pstmt.setString(24,compid);
				
				i=pstmt.executeUpdate();
				
				
				if(i>0)
				{
					String emailid=studentForm.getEmailId();
					String pass=studentForm.getPassword();
					System.out.println("fffffffffffff"+emailid);
					
					MailSendingUtil.LoginMail(emailid,pass);
					
					
					
				}
				
				
				Learnezysms.getmobilenumber(studentForm.getContactno());
				sql = "SELECT UserId FROM USERMASTER WHERE emailid=? and CompanyId='"+compid+"'";
				pstmt1 = con.prepareStatement(sql);
				pstmt1.setString(1, studentForm.getEmailId());
				rs1 = pstmt1.executeQuery();
				if (rs1.next()){
					userId = rs1.getInt("UserId");
					System.out.println(userId);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		
		return userId;
	}
	
	
	public int addCourse(DataSource dataSource, String courseList,long userId) {
		int flag=0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		int i=0;
		try {
			con = dataSource.getConnection();
			String sql = "INSERT INTO academycoursedetails (StudentId,CourseId,CourseStatus) VALUES(?,?,?)";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, userId);
			pstmt.setString(2, courseList);
			pstmt.setString(3,"Registered");
			i=pstmt.executeUpdate();
			flag=1;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return flag;
	}
	
	public String getUserName(DataSource dataSource, String courseList,long userId) {
		String userName="";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con =dataSource.getConnection();
			String sql = "SELECT * FROM USERNAME WHERE USERID="+userId;
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				userName=rs.getString("USERNAME");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return userName;
	}
	
	public int addNewCourse(DataSource dataSource, String courseList,long userId) {
		int flag=0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dataSource.getConnection();
			String sql = "UPDATE studentcoursedetails set CourseId= CONCAT(CourseId,',','"+courseList+"') where StudentId=?";
			System.out.println("sqlquery"+sql);
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, userId);
			pstmt.executeUpdate();
			flag=1;
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return flag;
	}

	public List<StudentRegistrationForm> selectedcourselist(String compid, DataSource dataSource, String courseList) {
			List<StudentRegistrationForm> list = new ArrayList<StudentRegistrationForm>();
			StudentRegistrationForm form = null;
			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			try {
				con =dataSource.getConnection();
				String sql = "SELECT * FROM paymentmaster where paymentId in ("+courseList+") ;";
				pstmt = con.prepareStatement(sql);
				rs = pstmt.executeQuery();
				form = new StudentRegistrationForm();form.setCourseid(-1);
				
				while (rs.next()) {
					form = new StudentRegistrationForm();
					form.setCourseid(rs.getInt("paymentId"));
					form.setCoursename(rs.getString("paymentName"));
					form.setCourseAmount(rs.getInt("amount"));
					//form.setDiscountPercentage(rs.getString("TotalDiscountAmount"));
					list.add(form);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					ConnectionUtil.closeResources(con, pstmt, rs);
				} catch (Exception e2) {
				}
			}
			return list;
	}

	public List<StudentRegistrationForm> courseNotTakenlist(DataSource dataSource, Long userid, String compid) {
		List<StudentRegistrationForm> list = new ArrayList<StudentRegistrationForm>();
		StudentRegistrationForm form = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String courseIdList="";
		try {
			con = dataSource.getConnection();
			String sql = "SELECT CourseId FROM studentcoursedetails WHERE studentid=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, userid);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				courseIdList=rs.getString("CourseId");
			}
			sql = "SELECT * FROM coursemater WHERE active=1 and CourseId NOT IN ("+courseIdList+") and CompanyId='"+compid+"'";
			if(courseIdList.equals("")){
				sql = "SELECT * FROM coursemater WHERE active=1 AND CompanyId='"+compid+"'";
			}
			
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			form = new StudentRegistrationForm();form.setCourseid(-1);
			
			while (rs.next()) {
				form = new StudentRegistrationForm();
				form.setCourseid(rs.getInt("CourseId"));
				form.setCoursename(rs.getString("CourseName"));
				list.add(form);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;


	}
	
	
	public int academyRegistration(StudentRegistrationForm studentForm,	String compid, DataSource dataSource, String profilePhotoName) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		   //get current date time with Date()
		   Date date = new Date();
		   dateFormat.format(date);
		   System.out.println(dateFormat.format(date));
		Connection con = null;
		int roleid = 5;
		int userId=0;
		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM USERMASTER WHERE emailid=? and CompanyId= '"+compid+"'";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, studentForm.getEmailId());
			rs = pstmt.executeQuery();
			if (rs.next()) {
			} else {
				sql = "INSERT INTO USERMASTER (UserName,Password,FIRSTNAME,MiddleName,LASTNAME,DateofBirth,Gender,Qualification,ContactNumber,Address,Phone,emailid,RoleID,ACTIVE,profilePhoto,paymentStatus,countryId, stateId, cityId, pincode,otherCountry,otherState,otherCity,CompanyId,areaofexpertise,coursesoffered,areaid) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				if (pstmt != null) pstmt.close();
				pstmt = con.prepareStatement(sql);
				pstmt.setString(1, studentForm.getUname());
				pstmt.setString(2, EncryptUtil.encrypt(studentForm.getPassword()));
				pstmt.setString(3, studentForm.getFirstName());
				pstmt.setString(4, studentForm.getMiddleName());
				pstmt.setString(5, studentForm.getLastName());
				pstmt.setString(6, studentForm.getDob());	
				System.out.println("dob "+studentForm.getDob());
				pstmt.setString(7, studentForm.getGender());
				pstmt.setString(8, studentForm.getQualification());
				pstmt.setString(9, studentForm.getContactno());
				pstmt.setString(10,studentForm.getAddress());
				pstmt.setString(11,studentForm.getPhone());
				pstmt.setString(12,studentForm.getEmailId());
				pstmt.setInt(13,roleid);
				pstmt.setBoolean(14,true);
				pstmt.setString(15,profilePhotoName);
				pstmt.setInt(16,0);
				pstmt.setInt(17,studentForm.getCountryid());
				pstmt.setInt(18,studentForm.getStateid());
				pstmt.setInt(19,studentForm.getCityid());
				pstmt.setInt(20,studentForm.getPincode());
				pstmt.setString(21,studentForm.getOthercountryname());
				pstmt.setString(22,studentForm.getOtherstatename());
				pstmt.setString(23,studentForm.getOthercityname());
				pstmt.setString(24,compid);
				pstmt.setString(25,studentForm.getAreofexper());
				pstmt.setString(26, studentForm.getCoursesoffered());
				pstmt.setString(27,studentForm.getAreaid());
				pstmt.executeUpdate();
				Learnezysms.getmobilenumber1(studentForm.getContactno());

				sql = "SELECT UserId FROM USERMASTER WHERE emailid=? and CompanyId='"+compid+"'";
				pstmt1 = con.prepareStatement(sql);
				pstmt1.setString(1, studentForm.getEmailId());
				rs1 = pstmt1.executeQuery();
				if (rs1.next()){
					userId = rs1.getInt("UserId");
					System.out.println(userId);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		
		return userId;
	}

	public int addCompany(StudentRegistrationForm studentForm, String compid, DataSource dataSource) {
		int compaId = 0;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String sql = "INSERT INTO companymaster (CompanyName,ACTIVE) VALUES (?,?)";
					pstmt = con.prepareStatement(sql);
					pstmt.setString(1, studentForm.getFirstName());
					pstmt.setBoolean(2, true);
					pstmt.executeUpdate();
					 
				
					sql = "SELECT * FROM companymaster where CompanyName = ?";
					if (pstmt != null) {	pstmt.close();}
					pstmt = con.prepareStatement(sql);
					pstmt.setString(1, studentForm.getFirstName());
					rs = pstmt.executeQuery();
					if (rs.next()) {
						compaId=rs.getInt("CompanyId");
					}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return compaId;
	}

	public int FreeLanceRegistration(StudentRegistrationForm studentForm,
			String compid, DataSource dataSource, String profilePhotoName) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		   //get current date time with Date()
		   Date date = new Date();
		   dateFormat.format(date);
		   System.out.println(dateFormat.format(date));
		Connection con = null;
		int roleid = 6;
		int userId=0;
		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM USERMASTER WHERE emailid=? and CompanyId= '"+compid+"'";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, studentForm.getEmailId());
			rs = pstmt.executeQuery();
			if (rs.next()) {
			} else {
				sql = "INSERT INTO USERMASTER (UserName,Password,FIRSTNAME,MiddleName,LASTNAME,DateofBirth,Gender,Qualification,ContactNumber,Address,Phone,emailid,RoleID,ACTIVE,profilePhoto,paymentStatus,countryId, stateId, cityId, pincode,otherCountry,otherState,otherCity,CompanyId,areaofexpertise,coursesoffered,areaid) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				if (pstmt != null) pstmt.close();
				pstmt = con.prepareStatement(sql);
				pstmt.setString(1, studentForm.getUname());
				pstmt.setString(2, EncryptUtil.encrypt(studentForm.getPassword()));
				pstmt.setString(3, studentForm.getFirstName());
				pstmt.setString(4, studentForm.getMiddleName());
				pstmt.setString(5, studentForm.getLastName());
				pstmt.setString(6, studentForm.getDob());	
				System.out.println("dob "+studentForm.getDob());
				pstmt.setString(7, studentForm.getGender());
				pstmt.setString(8, studentForm.getQualification());
				pstmt.setString(9, studentForm.getContactno());
				pstmt.setString(10,studentForm.getAddress());
				pstmt.setString(11,studentForm.getPhone());
				pstmt.setString(12,studentForm.getEmailId());
				pstmt.setInt(13,roleid);
				pstmt.setBoolean(14,true);
				pstmt.setString(15,profilePhotoName);
				pstmt.setInt(16,0);
				pstmt.setInt(17,studentForm.getCountryid());
				pstmt.setInt(18,studentForm.getStateid());
				pstmt.setInt(19,studentForm.getCityid());
				pstmt.setInt(20,studentForm.getPincode());
				pstmt.setString(21,studentForm.getOthercountryname());
				pstmt.setString(22,studentForm.getOtherstatename());
				pstmt.setString(23,studentForm.getOthercityname());
				pstmt.setString(24,compid);
				pstmt.setString(25,studentForm.getAreofexper());
				pstmt.setString(26, studentForm.getCoursesoffered());
				pstmt.setString(27,studentForm.getAreaid());
				pstmt.executeUpdate();
				
				sql = "SELECT UserId FROM USERMASTER WHERE emailid=? and CompanyId='"+compid+"'";
				pstmt1 = con.prepareStatement(sql);
				pstmt1.setString(1, studentForm.getEmailId());
				rs1 = pstmt1.executeQuery();
				if (rs1.next()){
					userId = rs1.getInt("UserId");
					System.out.println(userId);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		
		return userId;
	}

	public JSONArray verifyemailid(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response, String email) {
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
       JSONObject obj=null;

		
		try {
			con=dataSource.getConnection();      
			String sql="select emailid from usermaster where emailid='"+email+"'";

			pstmt=con.prepareStatement(sql);
			rs=pstmt.executeQuery();	
			while(rs.next()){
					obj=new JSONObject(); 
				obj.put("email","Email already Exist");
				
					jArray.add(obj);
				}
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;
	}

	public JSONArray addcourses(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String email, String course) {
		Connection con=null;
		PreparedStatement pstmt=null;
		PreparedStatement pstmt1=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
       JSONObject obj=null;

		
		try {
			con=dataSource.getConnection();  
			String sql1="select UserId from usermaster where emailid='"+email+"'";
			pstmt=con.prepareStatement(sql1);
			rs=pstmt.executeQuery();	
			if(rs.next()){
				obj=new JSONObject(); 
				long userId=rs.getInt("UserId");
				obj.put("userid",rs.getInt("UserId"));

				
				
				
				
				String sql = "INSERT INTO academycoursedetails (StudentId,CourseId,CourseStatus) VALUES(?,?,?)";
				pstmt1 = con.prepareStatement(sql);
				pstmt1.setLong(1, userId);
				pstmt1.setString(2, course);
				pstmt1.setString(3,"Registered");
				pstmt1.executeUpdate();
				obj.put("status","success");
				jArray.add(obj);
				
					
				}
			
			
			
		
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;
}

	public JSONArray addtrasnsactiondetails(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String userid, String courseid, String totalidamount, String tax) {
		Connection con=null;
		PreparedStatement pstmt=null;
		PreparedStatement pstmt1=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
       JSONObject obj=null;
       long amount=Long.parseLong(totalidamount);
     
       long taxes=Long.parseLong(tax);
      int transactionStatus=0;
      int userids=0;


		
		try {
			String compid="1";
			con = dataSource.getConnection();
			
			String sql1="select UserId from usermaster where emailid='"+userid+"'";
			pstmt=con.prepareStatement(sql1);
			rs=pstmt.executeQuery();	
			if(rs.next()){
				obj=new JSONObject(); 
				userids=rs.getInt("UserId");
				
			String sql = "INSERT INTO transactionmaster (TotalAmount, Userid, CoursesId, Tax, PGIID, CardNumber, TransactionStatus, CreatedDateTime,CompanyId ) VALUES(?,?,?,?,?,?,?, now(),'"+compid+"')";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1,amount);
			pstmt.setLong(2, userids);
			pstmt.setString(3, courseid);
			pstmt.setLong(4,taxes);
			pstmt.setString(5, "");
			pstmt.setString(6, "");				
			pstmt.setLong(7, transactionStatus);
			//pstmt.setString(9,compid);
			pstmt.executeUpdate();
			System.out.println(sql);
			
			sql = "SELECT max(transactionID) as transID FROM transactionmaster WHERE USERID=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, userids);
			rs=pstmt.executeQuery();
			if(rs.next()){
				obj=new JSONObject(); 
				
				obj.put("transactionstatus",rs.getInt("transID"));
				jArray.add(obj);
				
			}
			
			}
			
			
		
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;
	}

	public ArrayList<String> getProductListArray1(DataSource dataSource,
			String term) {
		// TODO Auto-generated method stub
		ArrayList<String> list = new ArrayList<String>();
		PreparedStatement pstmt = null;
		Connection con=null;
		String sql=null;
		int count=0;
		String products;
		ResultSet rs=null;
		System.out.println("Employeelist="+term);
		try {
			con=dataSource.getConnection();
				sql="select * from coursemater where CourseName LIKE ?";

				
				pstmt=con.prepareStatement(sql);
				System.out.println("auto query******&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&"+sql);
				pstmt.setString(1,"%"+term+"%");
			System.out.println("term ="+term);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
			count++;
				products=rs.getString("CourseName");
				
				list.add(products);
			}
			if(count==0)
			{
				list.add("No Results Found...");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return list;
	}
}