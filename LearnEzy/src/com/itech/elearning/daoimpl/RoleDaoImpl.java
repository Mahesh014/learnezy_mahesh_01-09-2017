package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.elearning.forms.RoleForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class RoleDaoImpl {

	public List<RoleForm> listAll(int roleid,String compid, DataSource dataSource) {

		List<RoleForm> list = new ArrayList<RoleForm>();
		RoleForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
        String sql = null;
		try {
			con = dataSource.getConnection();
			if(roleid==9 )
			{
			 sql = "SELECT * FROM ROLEMASTER  ORDER BY rolename ASC";
			}
			else
			{
				 sql = "SELECT * FROM ROLEMASTER where CompanyId = '"+compid+"' ORDER BY rolename ASC";
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new RoleForm();
				roleForm.setRoleid(rs.getLong("roleid"));
				roleForm.setRolename(rs.getString("rolename"));
				roleForm.setActive(rs.getBoolean("active"));
				list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;

	}

	public String add(RoleForm roleForm, String compid, DataSource dataSource) {

		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM ROLEMASTER WHERE ROLENAME=? and CompanyId = '"+compid+"'";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, roleForm.getRolename());
			rs = pstmt.executeQuery();
			if (rs.next())
				result = Common.DUPLICATE_NAME_MESSAGE;
			else {
				if (!roleForm.getRolename().equalsIgnoreCase("")
						&& !roleForm.getRolename().equalsIgnoreCase(null)) {
					sql = "INSERT INTO ROLEMASTER (ROLENAME,ACTIVE,CompanyId) VALUES (?,?,?)";
					if (pstmt != null)
						pstmt.close();
					pstmt = con.prepareStatement(sql);

					pstmt.setString(1, roleForm.getRolename());
					pstmt.setBoolean(2, true);
					pstmt.setString(3,compid);
					pstmt.executeUpdate();
					if (rs != null)
						rs.close();
					result = Common.REC_ADDED;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;

	}

	public Object changestatus(RoleForm roleForm, DataSource dataSource) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = dataSource.getConnection();
			String sql = "UPDATE ROLEMASTER SET ACTIVE=? WHERE ROLEID=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setBoolean(1, roleForm.isActive());
			pstmt.setLong(2, roleForm.getRoleid());
			pstmt.executeUpdate();
			result = Common.REC_STATUS_CHANGED;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	public Object update(RoleForm roleForm, DataSource dataSource) {

		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM ROLEMASTER WHERE ROLENAME COLLATE latin1_general_cs LIKE ? ";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, roleForm.getRolename());
			rs = pstmt.executeQuery();
			if (rs.next())
				result = Common.No_Change;
			else {
				if (!roleForm.getRolename().equalsIgnoreCase("")
						&& !roleForm.getRolename().equalsIgnoreCase(null)) {
					sql = "UPDATE ROLEMASTER SET ROLENAME=? WHERE ROLEID=?";
					if (pstmt != null)
						pstmt.close();
					pstmt = con.prepareStatement(sql);
					pstmt.setString(1, roleForm.getRolename());
					pstmt.setLong(2, roleForm.getRoleid());
					pstmt.executeUpdate();
					if (rs != null)
						rs.close();
					result = Common.REC_UPDATED;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return result;

	}

	public JSONArray getlicenseid(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response) {
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
       JSONObject obj=null;

		
		try {
			con=dataSource.getConnection();      
			String sql="select Autoid from licensequtoid ";

			pstmt=con.prepareStatement(sql);
			rs=pstmt.executeQuery();	
			while(rs.next()){
					obj=new JSONObject(); 
				obj.put("Autoid", rs.getString("Autoid"));
				
					jArray.add(obj);
				}
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;
	}

	public JSONArray licenseadd(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String licensename, String space, String clientper, String licenseid) {
		Connection con=null;
		PreparedStatement pstmt=null;
		PreparedStatement pstmt1=null;

		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
       JSONObject obj=null;
       int i=0;

		
		try {
			con=dataSource.getConnection();      
			String sql="select * from licensemaster where licenseName=? ";
       pstmt=con.prepareStatement(sql);
       pstmt.setString(1, licensename);
			rs=pstmt.executeQuery();	
			if(rs.next()){
					obj=new JSONObject(); 
				obj.put("status", "Duplicate Entry");
				
					jArray.add(obj);
				}
			else
			{
				sql = "insert into licensemaster(licenseautoid,licenseName,space,clientpercentage,active)values(?,?,?,?,?)";
				
				pstmt = con.prepareStatement(sql);

				pstmt.setString(1, licenseid);
				pstmt.setString(2, licensename);
				pstmt.setString(3,space);
				pstmt.setString(4,clientper);
				pstmt.setBoolean(5,true);
				i=pstmt.executeUpdate();
				
					int autoid=Integer.parseInt(licenseid);
					autoid++;
					String sql1="update licensequtoid set Autoid="+autoid+" where id=1";
					System.out.println("sqllllllllllllllllllllllll"+sql1);
					pstmt1 = con.prepareStatement(sql1);
					pstmt1.executeUpdate();
					obj=new JSONObject(); 
					obj.put("status", "Added sucessfully");
					jArray.add(obj);

					
					
				}
				
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;
	}

	public JSONArray licenselist(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response) {
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
       JSONObject obj=null;

		
		try {
			con=dataSource.getConnection();      
			String sql="select * from licensemaster order by id asc; ";

			pstmt=con.prepareStatement(sql);
			rs=pstmt.executeQuery();	
			while(rs.next()){
					obj=new JSONObject(); 
				obj.put("licenseautoid", rs.getString("licenseautoid"));
				obj.put("licenseName", rs.getString("licenseName"));
				obj.put("space", rs.getString("space"));
				obj.put("clientpercentage", rs.getString("clientpercentage"));
				obj.put("active", rs.getBoolean("active"));



				

				
					jArray.add(obj);
				}
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;

}

	public JSONArray chanstatus(DataSource dataSource,
			HttpServletRequest request, String licenseid, String status) {
		Connection con=null;
		PreparedStatement pstmt=null;
		PreparedStatement pstmt1=null;

		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
       JSONObject obj=null;
       int i=0;

		
		try {
			con=dataSource.getConnection();      
			
			
				String sql = "update licensemaster set active="+status+" where licenseautoid="+licenseid+"";
				
				pstmt = con.prepareStatement(sql);
				pstmt.executeUpdate();
				obj=new JSONObject(); 
				
					obj.put("status", "Status Changed Sucessfully");
					
					jArray.add(obj);

				
				
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;
		
	
	}

	public JSONArray licenseupdate(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String licensename, String space, String clientper, String licenseid) {
		System.out.println("licensename"+licensename);
		System.out.println("space"+space);
		System.out.println("clientper"+clientper);
		System.out.println("licenseid"+licenseid);



		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
       JSONObject obj=null;

		
		try {
			con=dataSource.getConnection();      
			String sql="select * from licensemaster where licenseautoid=? and licenseName=? and space=? and clientpercentage=?";

			pstmt=con.prepareStatement(sql);
			pstmt.setString(1, licenseid);
			pstmt.setString(2, licensename);
			pstmt.setString(3, space);
			pstmt.setString(4, clientper);
              rs=pstmt.executeQuery();	
			if(rs.next()){
					obj=new JSONObject(); 
				obj.put("status", "No Changes Have Been Made");
				
					jArray.add(obj);
				}
			
			else
			{
				String sql1="update licensemaster set  licenseName=? ,space=?,clientpercentage=? where licenseautoid=?";

				pstmt=con.prepareStatement(sql1);
				pstmt.setString(1, licensename);
				pstmt.setString(2, space);
				pstmt.setString(3, clientper);
				pstmt.setString(4, licenseid);
	              pstmt.executeUpdate();	
	              obj=new JSONObject(); 
					obj.put("status", "Update Sucessfully");
					
						jArray.add(obj);
				
			}
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;
	}

	}
