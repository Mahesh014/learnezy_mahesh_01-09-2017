package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.itech.elearning.forms.TopicTextForm;
import com.itech.elearning.utils.ConnectionUtil;

public class TopicTextDaoImpl {

	public List<TopicTextForm> listAllTopic(String compid, String compid2, Long courseID, Long userid, DataSource ds) {

		List<TopicTextForm> courseList = new ArrayList<TopicTextForm>();
		TopicTextForm topicData = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ds.getConnection();
			String sql = "SELECT * from topicmaster where CourseID='"+courseID+"' and CompanyId = '"+compid+"'";
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				topicData = new TopicTextForm();
				topicData.setTopicID(rs.getLong("TopicID"));
				topicData.setTopicName(rs.getString("TopicName"));
				System.out.println("" + topicData.getTopicName());
				courseList.add(topicData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return courseList;
	}
	public String listAll(String compid,String testID, Long userid, DataSource ds, Long topicId) {
		String result = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		System.out.println(" Funtion Called By LIst");
		try {
			con = ds.getConnection();
			String sql = "SELECT TopicText from topicmaster where TopicID = '"+testID+"' and CompanyId = '"+compid+"'";
			pstmt = con.prepareStatement(sql);
			//pstmt.setLong(1, topicId);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				result = rs.getString("TopicText");
				System.out.println(result + "~~~~~~~~~~~~~~~~~~~~~~~~~");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return result;
	}
}
