package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

import com.itech.elearning.forms.ChangePasswordForm;
import com.itech.elearning.utils.ConnectionUtil;
import com.itech.elearning.utils.EncryptUtil;

public class ChangePasswordDaoImpl {

	public int updatepwd(ChangePasswordForm changepwdform, DataSource dataSource, Long userid) {
		int flag = 0;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String oldPassword=null;
		try {
			con = dataSource.getConnection();
			String sql="select PASSWORD from usermaster where USERID=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1,userid);
			rs=pstmt.executeQuery();	
			if(rs.next()){
					oldPassword=EncryptUtil.decrypt(rs.getString("PASSWORD"));
					if(oldPassword.equals(changepwdform.getCurrpass())){
						if(pstmt!=null) pstmt.close();
						sql="UPDATE USERMASTER SET PASSWORD=? WHERE USERID=?";
						pstmt = con.prepareStatement(sql);
						pstmt.setString(1,EncryptUtil.encrypt(changepwdform.getNewpass()));
						pstmt.setLong(2,userid);
						pstmt.executeUpdate();
						flag = 1;
					}else{
						flag = 2;
					}
			}
		} catch (Exception e) {
			e.printStackTrace();
			flag = 0;
			return flag;
		} 	finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
			
			}
		return flag;
	
	}

}
