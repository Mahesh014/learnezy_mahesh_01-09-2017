package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.itech.elearning.forms.CompanyMasterForm;
import com.itech.elearning.forms.ContryForm;
import com.itech.elearning.forms.ParentCompanyForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class CompanyDaoImpl {

	public List<CompanyMasterForm> listAll(DataSource dataSource) {
		List<CompanyMasterForm> list = new ArrayList<CompanyMasterForm>();
		CompanyMasterForm companyForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM companymaster ORDER BY CompanyName ASC";
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				companyForm = new CompanyMasterForm();
				companyForm.setCompanyID(rs.getInt("CompanyId"));
				companyForm.setCompanyName(rs.getString("CompanyName"));
				companyForm.setComplogo(rs.getString("logoname"));
				companyForm.setCompsubdomain(rs.getString("subdomain"));
				companyForm.setCountryid(rs.getInt("countryid"));
				companyForm.setStateid(rs.getInt("stateid"));
				companyForm.setCityid(rs.getInt("cityid"));
				companyForm.setPcompid(rs.getInt("pcompid"));
				
				
				companyForm.setActive(rs.getBoolean("active"));
				list.add(companyForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}

	public String add(CompanyMasterForm companyForm, String imagefileName, DataSource dataSource) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM companymaster WHERE CompanyName=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, companyForm.getCompanyName());
			rs = pstmt.executeQuery();
			if (rs.next())
				result = Common.DUPLICATE_NAME_MESSAGE;
			else {
				if (!companyForm.getCompanyName().equalsIgnoreCase("")
						&& !companyForm.getCompanyName().equalsIgnoreCase(null)) {
					sql = "INSERT INTO companymaster (CompanyName,ACTIVE,logoname,subdomain,COUNTRYID,STATEID,CITYID,Pcompid) VALUES (?,?,?,?,?,?,?,?)";
					if (pstmt != null)
						pstmt.close();
					pstmt = con.prepareStatement(sql);

					pstmt.setString(1, companyForm.getCompanyName());
					pstmt.setBoolean(2, true);
					pstmt.setString(3,imagefileName);
					pstmt.setString(4, companyForm.getCompsubdomain());
					pstmt.setInt(5,companyForm.getCountryid());
					pstmt.setInt(6,companyForm.getStateid());
					pstmt.setInt(7,companyForm.getCityid());
					pstmt.setInt(8,companyForm.getPcompid());
					
					pstmt.executeUpdate();
					if (rs != null)
						rs.close();
					result = Common.REC_ADDED;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;

	}

	public String changestatus(CompanyMasterForm companyForm,
			DataSource dataSource) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = dataSource.getConnection();
			String sql = "UPDATE companymaster SET ACTIVE=? WHERE CompanyId=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setBoolean(1, companyForm.isActive());
			pstmt.setLong(2, companyForm.getCompanyID());
			pstmt.executeUpdate();
			result = Common.REC_STATUS_CHANGED;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	public String update(CompanyMasterForm companyForm, DataSource dataSource, String imagefileName) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM companymaster WHERE CompanyName=? AND subdomain=? AND logoname=? ";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, companyForm.getCompanyName());
			pstmt.setString(2, companyForm.getCompsubdomain());
			pstmt.setString(3, imagefileName);
			rs = pstmt.executeQuery();
			if (rs.next())
				result = Common.No_Change;
			else {
				if (!companyForm.getCompanyName().equalsIgnoreCase("") && !companyForm.getCompanyName().equalsIgnoreCase(null)) {
					sql = "UPDATE companymaster SET CompanyName=?,subdomain=?,logoname=? WHERE CompanyId=?";
					if (pstmt != null)
						pstmt.close();
					pstmt = con.prepareStatement(sql);
					pstmt.setString(1, companyForm.getCompanyName());
					pstmt.setString(2, companyForm.getCompsubdomain());
					pstmt.setString(3, imagefileName);
					pstmt.setLong(4, companyForm.getCompanyID());
					pstmt.executeUpdate();
					if (rs != null)
						rs.close();
					result = Common.REC_UPDATED;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return result;

	}

	public List<CompanyMasterForm> listAll1(String compid, DataSource dataSource) {
		List<CompanyMasterForm> list = new ArrayList<CompanyMasterForm>();
		CompanyMasterForm companyForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM companymaster where CompanyId= '"+compid+"' ORDER BY CompanyName ASC";
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				companyForm = new CompanyMasterForm();
				companyForm.setCompanyID(rs.getInt("CompanyId"));
				companyForm.setCompanyName(rs.getString("CompanyName"));
				companyForm.setComplogo(rs.getString("logoname"));
				companyForm.setCompsubdomain(rs.getString("subdomain"));
				companyForm.setActive(rs.getBoolean("active"));
				list.add(companyForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	
	}

	public List<CompanyMasterForm> ActiveCountrylist(DataSource dataSource) {
		List<CompanyMasterForm> list = new ArrayList<CompanyMasterForm>();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		CompanyMasterForm countryForm = null;
		String sql = null;
		try{
			con=dataSource.getConnection();
			
		 sql="select * from countrymaster where active = true order by COUNTRY ASC";
					
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while(rs.next()){
				countryForm = new CompanyMasterForm();
				countryForm.setCountryid(rs.getInt("COUNTRYID"));
				countryForm.setCountryname(rs.getString("COUNTRY"));
				countryForm.setActive(rs.getBoolean("ACTIVE"));
				list.add(countryForm);
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}

		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return list;
	}

	public List<CompanyMasterForm> ActiveParentlist(DataSource dataSource) 
	{
		List<CompanyMasterForm> list = new ArrayList<CompanyMasterForm>();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		CompanyMasterForm countryForm = null;
		String sql = null;
		try{
			con=dataSource.getConnection();
			
		 sql="select * from parentcompanymaster where active = true order by pcompanyname ASC";
					
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while(rs.next()){
				countryForm = new CompanyMasterForm();
				countryForm.setPcompid(rs.getInt("Pcompid"));
				countryForm.setPcompanyname(rs.getString("pcompanyname"));
				countryForm.setActive(rs.getBoolean("ACTIVE"));
				list.add(countryForm);
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}

		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return list;
	}

	public List<CompanyMasterForm> ActiveCountrylistadd(DataSource dataSource,
			String compid) {
		List<CompanyMasterForm> list = new ArrayList<CompanyMasterForm>();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		CompanyMasterForm countryForm = null;
		String sql = null;
		try{
			con=dataSource.getConnection();
			
		 sql="select * from countrymaster where CompanyId="+compid+" and  active = true order by COUNTRY ASC";
					
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while(rs.next()){
				countryForm = new CompanyMasterForm();
				countryForm.setCountryid(rs.getInt("COUNTRYID"));
				countryForm.setCountryname(rs.getString("COUNTRY"));
				countryForm.setActive(rs.getBoolean("ACTIVE"));
				list.add(countryForm);
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}

		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return list;
	}
 
	
	
}
