package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.itech.elearning.forms.LanguageMasterForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class LanguageMasterDao {

	public List<LanguageMasterForm> list(int roleid, String compid,
			DataSource dataSource) {
		List<LanguageMasterForm> list = new ArrayList<LanguageMasterForm>();
		LanguageMasterForm languageform= null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
        String sql = null;
		try {
			con = dataSource.getConnection();
			if(roleid==9 )
			{
			 sql = "SELECT * FROM languagemaster  ORDER BY Languagename ASC";
			}
			else
			{
				 sql = "SELECT * FROM languagemaster where CompanyId = '"+compid+"' ORDER BY Languagename ASC";
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				languageform = new LanguageMasterForm();
				languageform.setLanguageid(rs.getInt("Languageid"));
				languageform.setLanguagename(rs.getString("Languagename"));
				languageform.setActive(rs.getBoolean("active"));
				list.add(languageform);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;

}

	public Object add(LanguageMasterForm languageForm, String compid,
			DataSource dataSource) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM languagemaster WHERE Languagename=? and CompanyId = '"+compid+"'";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, languageForm.getLanguagename());
			rs = pstmt.executeQuery();
			if (rs.next())
				result = Common.DUPLICATE_NAME_MESSAGE;
			else {
				if (!languageForm.getLanguagename().equalsIgnoreCase("")
						&& !languageForm.getLanguagename().equalsIgnoreCase(null)) {
					sql = "INSERT INTO languagemaster (Languagename,Active,CompanyId) VALUES (?,?,?)";
					if (pstmt != null)
						pstmt.close();
					pstmt = con.prepareStatement(sql);

					pstmt.setString(1, languageForm.getLanguagename());
					pstmt.setBoolean(2, true);
					pstmt.setString(3,compid);
					pstmt.executeUpdate();
					if (rs != null)
						rs.close();
					result = Common.REC_ADDED;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;

}

	public Object changestatus(LanguageMasterForm languageForm,
			DataSource dataSource) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = dataSource.getConnection();
			String sql = "UPDATE languagemaster SET Active=? WHERE Languageid=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setBoolean(1, languageForm.isActive());
			pstmt.setLong(2, languageForm.getLanguageid());
			pstmt.executeUpdate();
			result = Common.REC_STATUS_CHANGED;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	public Object update(LanguageMasterForm languageForm, DataSource dataSource) {
		// TODO Auto-generated method stub
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM languagemaster WHERE Languagename COLLATE latin1_general_cs LIKE ? ";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, languageForm.getLanguagename());
			rs = pstmt.executeQuery();
			if (rs.next())
				result = Common.No_Change;
			else {
				if (!languageForm.getLanguagename().equalsIgnoreCase("")
						&& !languageForm.getLanguagename().equalsIgnoreCase(null)) {
					sql = "UPDATE languagemaster SET Languagename=? WHERE Languageid=?";
					if (pstmt != null)
						pstmt.close();
					pstmt = con.prepareStatement(sql);
					pstmt.setString(1, languageForm.getLanguagename());
					pstmt.setLong(2, languageForm.getLanguageid());
					pstmt.executeUpdate();
					if (rs != null)
						rs.close();
					result = Common.REC_UPDATED;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return result;

	}
}
