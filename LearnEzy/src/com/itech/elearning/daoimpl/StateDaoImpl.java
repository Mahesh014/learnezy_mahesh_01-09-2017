package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.elearning.forms.StateForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;
public class StateDaoImpl {
	public List<StateForm> listAll(int roleid,long countryid, String compid, DataSource dataSource){
		List<StateForm> stateList = new ArrayList<StateForm>();
		StateForm stateform = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql=null;
		try{
			con=dataSource.getConnection();
			if(roleid==9)
			{
			if(countryid>0){
				sql="SELECT * FROM statemaster where COUNTRYID=?  ORDER BY STATE ASC";//select query
				pstmt = con.prepareStatement(sql);
				pstmt.setLong(1,countryid);
			}else{
				sql="SELECT * FROM statemaster ORDER BY STATE ASC";//select query
				pstmt = con.prepareStatement(sql);
			}
			}
			else
			{
				if(countryid>0){
					sql="SELECT * FROM statemaster where COUNTRYID=? and CompanyId = '"+compid+"' ORDER BY STATE ASC";//select query
					pstmt = con.prepareStatement(sql);
					pstmt.setLong(1,countryid);
				}else{
					sql="SELECT * FROM statemaster where CompanyId= '"+compid+"' ORDER BY STATE ASC";//select query
					pstmt = con.prepareStatement(sql);
				}
			}
			
			rs = pstmt.executeQuery();
			while(rs.next()){
				stateform = new StateForm();
				stateform.setStateid(rs.getLong("STATEID"));
				stateform.setCountryid(rs.getLong("COUNTRYID"));
				stateform.setStatename(rs.getString("STATE"));
				stateform.setActive(rs.getBoolean("ACTIVE"));
				stateList.add(stateform);
			}
		}
		catch (Exception e){
		   e.printStackTrace();
		}
		
		finally{
			try{
				if(con!=null)con.close();
			}
			catch (Exception e2)
			{
				System.out.println(e2);
			}
		}
		return stateList;
	}

	
	
	public String add(String compid, StateForm stateForm, DataSource dataSource){
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		try	{
			con=dataSource.getConnection();
			String sql="select * from statemaster WHERE STATE=? and countryid=? and CompanyId = '"+compid+"'";//select query
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, stateForm.getStatename());
			pstmt.setLong(2,stateForm.getCountryid());
			ResultSet rs = pstmt.executeQuery();
			if(rs.next())result =  Common.DUPLICATE_NAME_MESSAGE;
			else{
				if(!stateForm.getStatename().equalsIgnoreCase("") && !stateForm.getStatename().equalsIgnoreCase(null)){
				sql="insert into statemaster(countryid,STATE,Active,CompanyId) VALUES (?,?,?,?)";//select query
				pstmt=con.prepareStatement(sql);
				pstmt.setLong(1,stateForm.getCountryid());
				pstmt.setString(2, stateForm.getStatename());
				pstmt.setBoolean(3,true);
				pstmt.setString(4,compid);
				pstmt.executeUpdate();
				result =Common.REC_ADDED;
				}
			}
		}
		catch (Exception e){
		 e.printStackTrace();
		}
		finally{
			ConnectionUtil.closeResources(con, pstmt);
		}
		return result;
	}

	public String changeStatus(StateForm stateForm, DataSource dataSource) 
	{
		
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con=dataSource.getConnection();
			String sql="UPDATE statemaster SET Active=? WHERE stateid=?";//select query
			pstmt = con.prepareStatement(sql);
			pstmt.setBoolean(1, stateForm.isActive());
			pstmt.setLong(2, stateForm.getStateid());
			pstmt.executeUpdate();
			result = Common.REC_STATUS_CHANGED;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null)
					con.close();
			} catch (Exception e2) {
			}
		}
		return result;
	}

	public String update(StateForm stateForm, DataSource dataSource) 
	{
		
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			con=dataSource.getConnection();
			String sql="select * from statemaster WHERE STATE=? and countryid=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, stateForm.getStatename());
			pstmt.setLong(2,stateForm.getCountryid());
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) result = Common.No_Change;
			else {
				sql="UPDATE statemaster SET STATE=? WHERE stateid=?";
				pstmt = con.prepareStatement(sql);
				pstmt.setString(1, stateForm.getStatename());
				pstmt.setLong(2, stateForm.getStateid());
				pstmt.executeUpdate();
				result = Common.REC_UPDATED;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null)
					con.close();
			} catch (Exception e2) {
			}
		}
		return result;
	}
	
	/*//**************START Audit Trial Inserting Code********************
	public int auditTrail(DataSource dataSource,StateForm stateForm,int userid) {
		int flag=0;
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		try {
			con=dataSource.getConnection();
			String screen="State Master Screen";
			String tablename="statemaster";
			String sql="select * FROM statemaster WHERE STATEID=?";
			pstmt= con.prepareStatement(sql);
			pstmt.setLong(1,stateForm.getStateid());
			rs = pstmt.executeQuery();
			while (rs.next()) {
					if(!rs.getString("STATE").equals(stateForm.getStatename())){
				    	String fieldname = "STATE";
				    	String currentvalue = rs.getString("STATE");
				    	String changedvalue = stateForm.getStatename();
				    	AuditTrailDao.auditTrail(con,userid,screen,tablename,fieldname,currentvalue,changedvalue);
					}
					
					 if(rs.getInt("COUNTRYID")!=stateForm.getCountryid()){
					    	String fieldname = "COUNTRYID";
					    	String currentvalue = rs.getString("COUNTRYID");
					    	String changedvalue = String.valueOf(stateForm.getCountryid());
					    	AuditTrailDao.auditTrail(con,userid,screen,tablename,fieldname,currentvalue,changedvalue);
						}	
			}
		}
		 catch (Exception e) {
			e.printStackTrace();
		}finally{
			ConnectionUtil.closeResources(con, pstmt,rs);
		}
		return flag;
		//**************End Audit Trial Inserting Code********************
	}*/
	

	public List<StateForm> activeList(int roleid,String compid,DataSource dataSource){
		List<StateForm> stateList = new ArrayList<StateForm>();
		StateForm stateform = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try{
			con=dataSource.getConnection();
			if(roleid==9)
			{
			 sql="SELECT * FROM statemaster where Active=true ORDER BY STATE ASC";
			}
			else
			{
				 sql="SELECT * FROM statemaster where Active=true and CompanyId= '"+compid+"' ORDER BY STATE ASC";

			}
			pstmt=con.prepareStatement(sql);
			rs=pstmt.executeQuery();
			while(rs.next()){
			  stateform = new StateForm();
			  stateform.setStateid(rs.getLong("stateid"));
			  stateform.setStatename(rs.getString("STATE"));
			  stateList.add(stateform);
		  }
		}
		catch (Exception e){
		  e.printStackTrace();
		}
		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return stateList;
	}
	public List<StateForm> activeListByCountry(int roleid,String compid,long countryid, DataSource dataSource){
		List<StateForm> stateList = new ArrayList<StateForm>();
		StateForm stateform = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql=null;
		try {
			con=dataSource.getConnection();
			if(roleid==9)
			{
			sql="SELECT * FROM statemaster where Active=true AND COUNTRYID=? ORDER BY STATE ASC";//select query
			}
			else
			{
			sql="SELECT * FROM statemaster where Active=true and CompanyId= '"+compid+"' AND COUNTRYID=? ORDER BY STATE ASC";//select query
		
			}
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1,countryid);
			rs = pstmt.executeQuery();
			while(rs.next()){
				stateform = new StateForm();
				stateform.setStateid(rs.getLong("STATEID"));
				stateform.setCountryid(rs.getLong("COUNTRYID"));
				stateform.setStatename(rs.getString("STATE"));
				stateform.setActive(rs.getBoolean("ACTIVE"));
				stateList.add(stateform);
			}
		}
		catch (Exception e){
		   e.printStackTrace();
		}
		
		finally{
			try{
				if(con!=null)con.close();
			}
			catch (Exception e2)
			{
				System.out.println(e2);
			}
		}
		return stateList;
	}



	@SuppressWarnings("unchecked")
	public JSONArray ajaxActiveList(DataSource dataSource, String countryId) {
		JSONArray jArray= new JSONArray();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql=null;
		try	{
			con=dataSource.getConnection();
			sql="SELECT * FROM statemaster where Active=true AND COUNTRYID=?   ORDER BY STATE ASC";//select query
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1,countryId);
			rs = pstmt.executeQuery();
			while(rs.next()){
				JSONObject obj= new JSONObject();
				obj.put("stateId",rs.getLong("STATEID"));
				obj.put("state",rs.getString("STATE"));
				jArray.add(obj);
			}
			System.out.println(sql);
		}
		catch (Exception e){
		   e.printStackTrace();
		}
		
		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;
	}
}
