package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.elearning.forms.NotificationMasterForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class NotificationAddDaoImpl {

	public List<NotificationMasterForm> list(int roleid,String compid, Long userid, DataSource dataSource) {
		List<NotificationMasterForm> list = new ArrayList<NotificationMasterForm>();
		NotificationMasterForm notiForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();
			if(roleid==9)
			{
			sql = "SELECT * FROM notificationstudent AS ns  JOIN userMaster AS um ON (um.userid=ns.studentId)";
			}
			else
			{
				sql = "SELECT * FROM notificationstudent AS ns  JOIN userMaster AS um ON (um.userid=ns.studentId) and ns.CompanyId = '"+compid+"'";

			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				notiForm = new NotificationMasterForm();
				notiForm.setUserid(rs.getLong("studentID"));
				notiForm.setFirstName(rs.getString("FIRSTNAME")+" "+rs.getString("LASTNAME"));
				notiForm.setNotificationID(rs.getLong("NOTIFICATIONID"));
				notiForm.setNotification(rs.getString("NOTIFICATION"));
				notiForm.setExpiryDate(rs.getString("EXPIRYDATE"));
				notiForm.setActive(rs.getBoolean("Active"));
				list.add(notiForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		} 
		return list;
	}

	public Object add(String compid, NotificationMasterForm notiForm, DataSource dataSource, String studentId) {
		String result = Common.FAILURE_MESSAGE;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		String sql = null;
		try {
			con = dataSource.getConnection();
			/*String sql = "SELECT * FROM notificationstudent WHERE NOTIFICATION=? and studentId=?  and EXPIRYDATE=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1,notiForm.getNotification());
			pstmt.setLong(2,notiForm.getUserid());
			pstmt.setString(3,notiForm.getExpiryDate());
			rs = pstmt.executeQuery();
			if (rs.next()) {
				result = Common.DUPLICATE_NAME_MESSAGE;
			} else {
			*/

		
			sql = "INSERT INTO notificationstudent (NOTIFICATION,studentId,EXPIRYDATE,ACTIVE,CREATEDDATE,CompanyId) VALUES(?,?,?,?,NOW(),?)";
		
			

		
			pstmt = con.prepareStatement(sql);
				pstmt.setString(1,notiForm.getNotification());
				pstmt.setString(2,studentId);
				pstmt.setString(3,notiForm.getExpiryDate());
				pstmt.setBoolean(4, true);
				pstmt.setString(5, compid);
				pstmt.executeUpdate();
				result = Common.REC_ADDED;
		/*	}*/
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	public Object changeStatus(NotificationMasterForm notiForm,
			DataSource dataSource, Long notificationID, boolean active) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = dataSource.getConnection();
			String sql = "DELETE FROM notificationstudent WHERE NOTIFICATIONID=?";
			pstmt = con.prepareStatement(sql);
			//pstmt.setBoolean(1,active);
			pstmt.setLong(1, notificationID);
			pstmt.executeUpdate();
			result = "Deleted successfully";

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return result;
	}


	public Object update(NotificationMasterForm notiForm, DataSource dataSource) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();
			sql = "SELECT * FROM notificationstudent WHERE NOTIFICATION=? and studentId=?  and EXPIRYDATE=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1,notiForm.getNotification());
			pstmt.setLong(2,notiForm.getUserid());
			pstmt.setString(3,notiForm.getExpiryDate());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				result = Common.No_Change;
			} else {
					sql = "UPDATE notificationstudent SET NOTIFICATION=?, studentId=?, EXPIRYDATE=? where NOTIFICATIONID=?";
					if (pstmt != null)	pstmt.close();
					pstmt = con.prepareStatement(sql);
					pstmt.setString(1,notiForm.getNotification());
					pstmt.setLong(2,notiForm.getUserid());
					pstmt.setString(3,notiForm.getExpiryDate());
					pstmt.setLong(4,notiForm.getNotificationID());
					pstmt.executeUpdate();
					result = Common.REC_UPDATED;
				 
			} 

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;

 }

	public List<NotificationMasterForm> studentList(int roleid,String compid, Long userid, DataSource dataSource) {
		List<NotificationMasterForm> studentList = new ArrayList<NotificationMasterForm>();
		NotificationMasterForm userData = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();
			if(roleid==9)
			{
			sql = "SELECT * FROM USERMASTER where roleid=3 and CompanyId  = '"+compid+"'";
			}
			else
			{
				sql = "SELECT * FROM USERMASTER where roleid=3 and CompanyId  = '"+compid+"'";
			}
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				userData = new NotificationMasterForm();
				userData.setUserid(rs.getLong("USERID"));
				userData.setFirstName(rs.getString("FIRSTNAME")+" "+rs.getString("LASTNAME"));
				studentList.add(userData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return studentList;
	}
 
	@SuppressWarnings("unchecked")
	public JSONArray studentListByCourse(String comid, String courseId, DataSource dataSource) {
		JSONArray jArray= new JSONArray();
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		Connection con=null;
		int questionNo=0;
		 
		try{
			con = dataSource.getConnection();
			String sql = "select * from usermaster um inner join studentcoursedetails scd on scd.studentid=um.userid where FIND_IN_SET('"+courseId+"', scd.CourseId) and um.CompanyId = '"+comid+"' order by um.firstname ASC;";
			pstmt = con.prepareStatement(sql);
			//System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				questionNo=questionNo+1;
				JSONObject obj = new JSONObject();
				obj.put("studentName", rs.getString("firstName")+" "+rs.getString("LastName"));
				obj.put("studentId", rs.getString("userId"));
				jArray.add(obj);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}	
		finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
			return jArray;
	}
}
