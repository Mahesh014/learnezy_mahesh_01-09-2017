package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.elearning.forms.AppointmentForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class AppointmentDao {

	public List<AppointmentForm> list(int roleid,DataSource dataSource, String compid) {
		List<AppointmentForm> list = new ArrayList<AppointmentForm>();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		AppointmentForm apptForm = null;
		String sql = null;
		try	{
			con=dataSource.getConnection();
			if(roleid ==9 )
			{
			 sql="SELECT * FROM appointment  ORDER BY dateofappointment ASC";//select query
			}
			else
			{
		   sql="SELECT * FROM appointment where companyId="+compid+" ORDER BY dateofappointment ASC";//select query
	
			}
			pstmt=con.prepareStatement(sql);
			rs=pstmt.executeQuery();
			while(rs.next()){
				apptForm = new AppointmentForm();
				apptForm.setAppointmentId(rs.getInt("appointmentID"));
				apptForm.setAppointmentDate(rs.getString("dateofappointment"));
				apptForm.setAppointmentTime(rs.getString("timeofappointment"));
				apptForm.setActive(rs.getBoolean("ACTIVE"));
				list.add(apptForm);
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}

		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return list;
	}

	public String add(AppointmentForm apptForm, DataSource dataSource, String compid)
	{
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs =null;
		try
		{
			con=dataSource.getConnection();
			String sql="select * from appointment where dateofappointment=? and timeofappointment=? and companyId=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, apptForm.getAppointmentDate());
			pstmt.setString(2, apptForm.getAppointmentTime());
			pstmt.setString(3, compid);
			rs = pstmt.executeQuery();
			if(rs.next()) result=Common.DUPLICATE_NAME_MESSAGE;
			else{
					sql="insert into appointment (dateofappointment,timeofappointment,companyId,ACTIVE) values (?,?,?,?)";
					pstmt = con.prepareStatement(sql);
					pstmt.setString(1, apptForm.getAppointmentDate());
					pstmt.setString(2, apptForm.getAppointmentTime());
					pstmt.setString(3, compid);
					pstmt.setBoolean(4, true);
					pstmt.executeUpdate();
					result=Common.REC_ADDED;
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		finally	{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return result;
	}

	public String changeStatus(AppointmentForm apptForm, DataSource dataSource)
	{
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		try	{
			con=dataSource.getConnection();
			String sql="update appointment set ACTIVE = ? where appointmentID=?";
			pstmt=con.prepareStatement(sql);
			pstmt.setBoolean(1,apptForm.isActive());
			pstmt.setInt(2, apptForm.getAppointmentId());
			pstmt.executeUpdate();
			result=Common.REC_STATUS_CHANGED;
		}
		catch (Exception e){
			System.out.println(e);
		} finally{
			ConnectionUtil.closeResources(con, pstmt);
		}
		return result;
	}

	public String update(AppointmentForm apptForm, DataSource dataSource) 
	{
		String result=Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		try{
			con=dataSource.getConnection();
			String sql="select * from appointment where COUNTRY=?";
			pstmt=con.prepareStatement(sql);
			sql="select * from appointment where dateofappointment=? and timeofappointment=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, apptForm.getAppointmentDate());
			pstmt.setString(2, apptForm.getAppointmentTime());
			rs = pstmt.executeQuery();
			if(rs.next())	result=Common.No_Change;
			else{
					sql="UPDATE appointment SET dateofappointment=?, timeofappointment=? WHERE appointmentID=?";
					pstmt=con.prepareStatement(sql);
					pstmt.setString(1, apptForm.getAppointmentDate());
					pstmt.setString(2, apptForm.getAppointmentTime());
					pstmt.setInt(3, apptForm.getAppointmentId());
					pstmt.executeUpdate();
					result=Common.REC_UPDATED;
			}
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return result;
	}

	/*//**************START Audit Trial Inserting Code********************
	public int auditTrail(DataSource dataSource,AppointmentForm apptForm,int userid) {
		int flag=0;
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		try {
			con=dataSource.getConnection();
			String screen="Country Master Screen";
			String tablename="appointment";
			String sql="select * FROM appointment WHERE COUNTRYID=?";
			pstmt= con.prepareStatement(sql);
			pstmt.setLong(1,apptForm.getCountryid());
			rs = pstmt.executeQuery();
			while (rs.next()) {
									
				    if(!rs.getString("COUNTRY").equals(apptForm.getCountryname())){
				    	String fieldname = "COUNTRY";
				    	String currentvalue = rs.getString("COUNTRY");
				    	String changedvalue = apptForm.getCountryname();
				    	AuditTrailDao.auditTrail(con,userid,screen,tablename,fieldname,currentvalue,changedvalue);
					}	
			}
		}
		 catch (Exception e) {
			e.printStackTrace();
		}finally{
			ConnectionUtil.closeResources(con, pstmt,rs);
		}
		return flag;
		//**************End Audit Trial Inserting Code********************
	}*/
	public List<AppointmentForm> activeList(int roleid,String compid,DataSource dataSource){
		List<AppointmentForm> list = new ArrayList<AppointmentForm>();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		AppointmentForm apptForm = null;
		String sql = null;
		try{
			con=dataSource.getConnection();
			if(roleid==9)
			{
	 sql="select * from appointment where active = true ";
			}
			else
			{
		 sql="select * from appointment where active = true and CompanyId = '"+compid+"'";
	
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while(rs.next()){
				apptForm = new AppointmentForm();
				apptForm.setAppointmentId(rs.getInt("appointmentID"));
				apptForm.setAppointmentDate(rs.getString("dateofappointment") +" "+ rs.getString("timeofappointment"));
				list.add(apptForm);
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}

		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public JSONArray ajaxActiveList(DataSource dataSource, String appointmentID) {
		JSONArray jArray= new JSONArray();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql=null;
		try{
			con=dataSource.getConnection();
			sql="select * from appointment WHERE appointmentID=?";//select query
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1,appointmentID);
			rs = pstmt.executeQuery();
			while(rs.next()){
				JSONObject obj= new JSONObject();
				obj.put("appointmentID",rs.getLong("COUNTRYID"));
				obj.put("dateofappointment",rs.getString("dateofappointment")+" - "+rs.getString("timeofappointment"));
				jArray.add(obj);
				System.out.println(sql);
			}
		}
		catch (Exception e){
		   e.printStackTrace();
		}
		
		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;
	}
 

}
