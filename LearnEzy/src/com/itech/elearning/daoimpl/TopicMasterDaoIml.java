package com.itech.elearning.daoimpl;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.sql.DataSource;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;

import com.itech.elearning.forms.TopicMasterForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class TopicMasterDaoIml {

	public List<TopicMasterForm> listAllCourse(int roleid,String compid, Long userid, DataSource ds) {
		List<TopicMasterForm> courseList = new ArrayList<TopicMasterForm>();
		TopicMasterForm topicForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			if(roleid==9)
			{
		 sql = "SELECT * FROM coursemater WHERE ACTIVE=TRUE";
			}
			else
			{
			 sql = "SELECT * FROM coursemater WHERE ACTIVE=TRUE and CompanyId = '"+compid+"'";
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				topicForm = new TopicMasterForm();
				topicForm.setCourseId(rs.getLong("CourseId"));
				topicForm.setCourseName(rs.getString("CourseName"));

				courseList.add(topicForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return courseList;
	}

	public List<TopicMasterForm> listAll(int roleid,String compid, Long userid, Long courseid, DataSource ds) {

		List<TopicMasterForm> courseList = new ArrayList<TopicMasterForm>();
		TopicMasterForm topicData = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		System.out.println(" Funtion Called By LIst");
		
		System.out.println("course id="+courseid);
		System.out.println("Company id="+compid);
		try {
			con = ds.getConnection();
			if(roleid == 9)
			{
			 sql = "SELECT * from topicmaster where ACTIVE=true ";
			if(courseid!=0 && courseid!=-1 && courseid!=null){
				sql = "SELECT * from topicmaster where  courseid="+courseid;
			}
			}
			else
			{
				 sql = "SELECT * from topicmaster where ACTIVE=true and CompanyId = "+compid+"";
				if(courseid!=0 && courseid!=-1 && courseid!=null){
					sql = "SELECT * from topicmaster where CompanyId = "+compid+" and courseid="+courseid;
				}
			}
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				topicData = new TopicMasterForm();
				topicData.setTopicId(rs.getLong("TopicID"));
				topicData.setTopicName(rs.getString("TopicName"));
				topicData.setCourseId(rs.getLong("CourseId"));
				topicData.setActive(rs.getBoolean("Active"));

				courseList.add(topicData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return courseList;
	}
	public String addTopic(String compid, TopicMasterForm topicForm, DataSource ds) {
		String result = Common.FAILURE_MESSAGE;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		Connection con = null;
		try {

			con = ds.getConnection();
			String sql1 = "SELECT * FROM topicmaster WHERE TopicName=? and CourseId =? and CompanyId = '"+compid+"'";
			pstmt = con.prepareStatement(sql1);
			pstmt.setString(1, topicForm.getTopicName());
			System.out.println(topicForm.getTopicName());
			pstmt.setLong(2, topicForm.getCourseId());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				result = Common.DUPLICATE_NAME_MESSAGE;
			} else {
				String sql = "INSERT INTO topicmaster (TopicName,CourseId,ACTIVE,CompanyId) VALUES(?,?,?,?)";
				if (pstmt != null)	pstmt.close();
				pstmt = con.prepareStatement(sql);
				System.out.println(sql);
				pstmt.setString(1, topicForm.getTopicName());
				pstmt.setLong(2, topicForm.getCourseId());				
				pstmt.setBoolean(3, true);
				pstmt.setString(4,compid);
				pstmt.executeUpdate();
				result = Common.REC_ADDED;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);

			} catch (Exception e2) {
			}
		}
		return result;
	}

	public String changeStatus(TopicMasterForm topicForm, DataSource ds,
			long topicId) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = ds.getConnection();
			String sql = "UPDATE topicmaster SET ACTIVE=? WHERE TopicID=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setBoolean(1, topicForm.isActive());
			pstmt.setLong(2, topicId);
			pstmt.executeUpdate();
			result = Common.REC_STATUS_CHANGED;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	public Object updateToopic(TopicMasterForm topicForm, DataSource ds) {

		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			String sql1 = "SELECT * FROM topicmaster WHERE TopicName=? and CourseId =?";
			pstmt = con.prepareStatement(sql1);
			pstmt.setString(1, topicForm.getTopicName());
			System.out.println(topicForm.getTopicName());
			pstmt.setLong(2, topicForm.getCourseId());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				result = Common.No_Change;
			} else {
			
				sql = "UPDATE topicmaster SET TopicName  = ?,CourseId = ? where TopicID = ?";
				if (rs != null)		rs.close();
				if (pstmt != null)	pstmt.close();
				System.out.println(sql);
				pstmt = con.prepareStatement(sql);
				pstmt.setString(1, topicForm.getTopicName());
				pstmt.setLong(2, topicForm.getCourseId());
		
				pstmt.setLong(3, topicForm.getTopicId());
				pstmt.executeUpdate();
				result = Common.REC_UPDATED;
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public String topicExcelUpload(String fileName, String filePath, String compid) {
		 String result="process Failed"; 
		   //int flag=0;
		   int daoflag=0;
		   Connection con=null;
		   PreparedStatement pstmt=null;
		   
		   try {
			   
			    con=ConnectionUtil.getMySqlConnection();
	         	long start = System.currentTimeMillis();
	            
	         	
	         	String sql="insert into topicmaster(TopicName,CourseId,CompanyId,Active) values(?,?,?,1)";
	              
	            if(pstmt!=null)pstmt.close();
	            pstmt=con.prepareStatement(sql);
	            
	         	InputStream input = new BufferedInputStream(new FileInputStream(filePath+fileName));
	         	
	         	 
	         	System.out.println("Excel File path="+filePath);
	         	System.out.println("Excel File Name="+fileName);
	            POIFSFileSystem fs = new POIFSFileSystem(input);
	            HSSFWorkbook wb = new HSSFWorkbook(fs);
	            HSSFSheet sheet = wb.getSheetAt(0);
	            Iterator<Row> rows = sheet.rowIterator();
	          
	            long count=0;
	            while (rows.hasNext()) {
	            		// flag=0;
	            		 HSSFRow row = (HSSFRow) rows.next();
	                     
	            		   String course_id=TopicMasterDaoIml.StringNotNull(row.getCell(0));
	            		   System.out.println("courseidddddddd"+course_id);
	                     String topic_name=TopicMasterDaoIml.StringNotNull(row.getCell(1));
	                     System.out.println("topicname"+topic_name);
	                   
	                     
	                     
	                     
	                     try {
	                    	 if(daoflag==1){
	                      		count=count+1;
	                         	System.out.println("excel file adding............");
	                   	       
	                   	       	pstmt.setString(1,topic_name);
	                   	        pstmt.setString(2,course_id);
	                   	        pstmt.setString(3,compid);
	     	                   
	     	               
	     	                    pstmt.addBatch();
	     	                    
	     	                    
	     	                    if(count%1000 == 0) { 
	     	                    	pstmt.executeBatch();
	     	                    	System.out.println("Batch Count: "+count+ " Time Taken: "+(System.currentTimeMillis()-start)+" MilliSecons");
	     	                    }            
	     	                  //  flag=1;
	     	                    
	     	                    System.out.println("count="+count);
	     	                    result=count+" "+"Records uploaded";
	     	                    
	     	                    System.out.println("%%%%%%%%%%%%%"+result);
	     	                 }
	                 	} catch (Exception einner) {
	                 		einner.printStackTrace();
						}
	                      
	                      daoflag=1;
	            }
	            pstmt.executeBatch();
	            System.out.println("Batch Count: "+count+ " Time Taken: "+(System.currentTimeMillis()-start)+" MilliSecons");
	        
		   } catch (Exception e) {
	            e.printStackTrace();
	        }
	        finally{
	            ConnectionUtil.closeResources(con, pstmt);
	        }
	        
	        return result; 
			//return flag;
	    }    
	
	 public static String StringNotNull(HSSFCell rowValue){
		   String results="";
		   if(rowValue!=null) results =rowValue.toString().trim();
		   return results;
		   
	   }
	   

/*	public TopicMasterForm get(long topicId, DataSource ds) {

		TopicMasterForm topicData = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ds.getConnection();
			String sql = "SELECT t.TopicID,t.TopicName ,t.CourseId , t.TopicText, c.CourseName, TopicDuration  FROM  topicmaster AS t JOIN coursemater AS c ON (t.CourseId = c.CourseId) where t.TopicID = ?";
			System.out.println(sql);
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, topicId);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				topicData = new TopicMasterForm();
				topicData.setTopicId(rs.getLong("t.TopicID"));
				topicData.setTopicName(rs.getString("t.TopicName"));
				
				topicData.setCourseId(rs.getLong("t.CourseId"));
				topicData.setCourseName(rs.getString("c.CourseName"));
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return topicData;
	}*/

}
