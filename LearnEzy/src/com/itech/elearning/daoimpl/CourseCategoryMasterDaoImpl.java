package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.itech.elearning.forms.CourseCategoryMasterForm;
import com.itech.elearning.forms.RoleForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class CourseCategoryMasterDaoImpl {

	public List<CourseCategoryMasterForm> listAll(int roleid, String compid,
			DataSource dataSource) {
		List<CourseCategoryMasterForm> list = new ArrayList<CourseCategoryMasterForm>();
		CourseCategoryMasterForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
        String sql = null;
		try {
			con = dataSource.getConnection();
			if(roleid==9 )
			{
			 sql = "SELECT * FROM coursecategorymaster  ORDER BY coursecatname ASC";
			}
			else
			{
				 sql = "SELECT * FROM coursecategorymaster where CompanyId = '"+compid+"' ORDER BY coursecatname ASC";
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new CourseCategoryMasterForm();
				roleForm.setCoursecategoryId(rs.getInt("coursecatId"));
				roleForm.setCoursecategoryname(rs.getString("coursecatname"));
				roleForm.setActive(rs.getBoolean("active"));
				list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}

	public String add(CourseCategoryMasterForm roleForm, String compid,
			DataSource dataSource)
	{
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM coursecategorymaster WHERE coursecatname=? and CompanyId = '"+compid+"'";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, roleForm.getCoursecategoryname());
			rs = pstmt.executeQuery();
			if (rs.next())
				result = Common.DUPLICATE_NAME_MESSAGE;
			else {
				if (!roleForm.getCoursecategoryname().equalsIgnoreCase("")
						&& !roleForm.getCoursecategoryname().equalsIgnoreCase(null)) {
					sql = "INSERT INTO coursecategorymaster (coursecatname,ACTIVE,CompanyId) VALUES (?,?,?)";
					if (pstmt != null)
						pstmt.close();
					pstmt = con.prepareStatement(sql);

					pstmt.setString(1, roleForm.getCoursecategoryname());
					pstmt.setBoolean(2, true);
					pstmt.setString(3,compid);
					pstmt.executeUpdate();
					if (rs != null)
						rs.close();
					result = Common.REC_ADDED;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	public String changestatus(CourseCategoryMasterForm roleForm,
			DataSource dataSource) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = dataSource.getConnection();
			String sql = "UPDATE coursecategorymaster SET ACTIVE=? WHERE coursecatId=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setBoolean(1, roleForm.isActive());
			pstmt.setLong(2, roleForm.getCoursecategoryId());
			pstmt.executeUpdate();
			result = Common.REC_STATUS_CHANGED;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	public String update(CourseCategoryMasterForm roleForm,
			DataSource dataSource) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM coursecategorymaster WHERE coursecatname COLLATE latin1_general_cs LIKE ? ";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, roleForm.getCoursecategoryname());
			rs = pstmt.executeQuery();
			if (rs.next())
				result = Common.No_Change;
			else {
				if (!roleForm.getCoursecategoryname().equalsIgnoreCase("")
						&& !roleForm.getCoursecategoryname().equalsIgnoreCase(null)) {
					sql = "UPDATE coursecategorymaster SET coursecatname=? WHERE coursecatId=?";
					if (pstmt != null)
						pstmt.close();
					pstmt = con.prepareStatement(sql);
					pstmt.setString(1, roleForm.getCoursecategoryname());
					pstmt.setLong(2, roleForm.getCoursecategoryId());
					pstmt.executeUpdate();
					if (rs != null)
						rs.close();
					result = Common.REC_UPDATED;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	
	
}
