package com.itech.elearning.daoimpl;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.itech.elearning.forms.PassPercentForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class PassPercentDaoImpl {

	public List<PassPercentForm> courcelist(String compid, DataSource ds) {
		
		List<PassPercentForm> courseList = new ArrayList<PassPercentForm>();
		PassPercentForm topicForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ds.getConnection();
			String sql = "SELECT * FROM coursemater WHERE ACTIVE=TRUE and CompanyId= '"+compid+"'";
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				topicForm = new PassPercentForm();
				topicForm.setCourseId(rs.getLong("CourseId"));
				topicForm.setCourseName(rs.getString("CourseName"));

				courseList.add(topicForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return courseList;
	}

	public List<PassPercentForm> passpercentList(DataSource ds, long courceID, String compid) {
		List<PassPercentForm> courseList = new ArrayList<PassPercentForm>();
		PassPercentForm topicForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ds.getConnection();
			String sql = "select pp.* ,cm.CourseName from passpercentage pp inner join coursemater cm on pp.CourseId = cm.CourseId and cm.CompanyId= '"+compid+"'";
			if(courceID!=0 && courceID!=-1){
				sql = "select pp.* ,cm.CourseName from passpercentage pp inner join coursemater cm on pp.CourseId = cm.CourseId where cm.CompanyId= '"+compid+"' and cm.courseId="+courceID ;
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				topicForm = new PassPercentForm();
				topicForm.setCourseId(rs.getLong("pp.CourseId"));
				topicForm.setCourseName(rs.getString("cm.CourseName"));
				topicForm.setPasspercentid(rs.getInt("pp.PassPercentId"));
				topicForm.setPasspercent(rs.getDouble("pp.PassPercent"));
				topicForm.setActive(rs.getBoolean("pp.active"));
				courseList.add(topicForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return courseList;
	}

	public String changeStatus(String compid, PassPercentForm passpercentForm,
			DataSource ds, long parseid) {
		
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = ds.getConnection();
			String sql = "UPDATE passpercentage SET ACTIVE=? WHERE PassPercentId=? and CompanyId = '"+compid+"'";
			System.out.println(sql);
			pstmt = con.prepareStatement(sql);
			pstmt.setBoolean(1, passpercentForm.isActive());
			pstmt.setLong(2, parseid);
			pstmt.executeUpdate();
			result = Common.REC_STATUS_CHANGED;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	public String add(String compid, PassPercentForm passpercentForm, DataSource dataSource) {
		

		String result = Common.FAILURE_MESSAGE;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;

		try {
			con = dataSource.getConnection();
			String sql1 = "SELECT PassPercent FROM passpercentage WHERE CourseId=? and CompanyId= '"+compid+"'";
			System.out.println(sql1);
			pstmt = con.prepareStatement(sql1);
			pstmt.setLong(1, passpercentForm.getCourseId());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				result = Common.DUPLICATE_NAME_MESSAGE;
			} else {
				String sql = "INSERT INTO passpercentage (PassPercent,CreatedDate,CourseId,active,CompanyId) VALUES(?,now(),?,?,?)";
				if (pstmt != null)
					pstmt.close();
				pstmt = con.prepareStatement(sql);
				System.out.println(sql);
				pstmt.setDouble(1, passpercentForm.getPasspercent());

				pstmt.setLong(2, passpercentForm.getCourseId());
				pstmt.setBoolean(3, true);
				pstmt.setString(4,compid);
				pstmt.executeUpdate();
				result = Common.REC_ADDED;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;

	}

	public String  update(PassPercentForm passpercentForm, DataSource ds) {
		
		
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		 

		try {
			con = ds.getConnection();
			sql = "SELECT * FROM passpercentage WHERE PassPercent =? AND CourseId =?";
			pstmt = con.prepareStatement(sql);
			pstmt.setDouble(1, passpercentForm.getPasspercent());
			pstmt.setLong(2, passpercentForm.getCourseId());
			
					rs = pstmt.executeQuery();
			System.out.println(sql);
			if (rs.next())
				result=Common.No_Change;
			else {
				
			
					sql = "UPDATE passpercentage SET PassPercent =?,UpdatedDate = now() where PassPercentId = ?";
					if (rs != null)
						rs.close();
					if (pstmt != null)
						pstmt.close();
				//	System.out.println("sad "+passpercentForm.getPasspercentid()+"ssss "+  passpercentForm.getPasspercent());
					System.out.println(sql);
					pstmt = con.prepareStatement(sql);
					pstmt.setDouble(1, passpercentForm.getPasspercent());
					//pstmt.setLong(2, passpercentForm.getCourseId());
					pstmt.setInt(2, passpercentForm.getPasspercentid());
					
					pstmt.executeUpdate();
					result = Common.REC_UPDATED;
				
			} 
				//result = Common.No_Change;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;
						
	}

}
