package com.itech.elearning.daoimpl;

import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.elearning.forms.DynamicDataForm;
import com.itech.elearning.forms.RoleForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;
import com.itech.elearning.utils.EncryptUtil;

public class DynamicDataDaoImpl {

	public JSONArray ajaxActiveList(DataSource dataSource) {

		JSONArray jArray = new JSONArray();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();
			sql = "select * from companymaster WHERE active=1";// select query
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				JSONObject obj = new JSONObject();
				obj.put("CompanyId", rs.getLong("CompanyId"));
				obj.put("CompanyName", rs.getString("CompanyName"));
				obj.put("Active", rs.getString("Active"));
				obj.put("subdomain", rs.getString("subdomain"));
				obj.put("logoname", rs.getString("logoname"));
				jArray.add(obj);
				System.out.println(sql);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;
	}

	public List<DynamicDataForm> list(DataSource dataSource) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();
			sql = "select c.CompanyId,c.CompanyName,u.coursesoffered,u.Address,profilePhoto from companymaster c,usermaster u where c.CompanyId=u.CompanyId and c.active=1;";
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setComppid(rs.getString("CompanyId"));
				roleForm.setCompname(rs.getString("CompanyName"));
				roleForm.setAddress(rs.getString("u.Address"));
				roleForm.setLogo(rs.getString("u.profilePhoto"));
				roleForm.setCountryid(rs.getString("coursesoffered"));
				// roleForm.setActive(rs.getBoolean("active"));
				list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;

	}

	public List<DynamicDataForm> listcity(DataSource dataSource) {

		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();

			sql = "select city,cityid from citymaster where active = 1;";

			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setCityid(rs.getString("cityid"));
				roleForm.setCityname(rs.getString("city"));

				list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;

	}

	public List<DynamicDataForm> searchlist(String couursename,String cityname,String coursemode,String cityid,
			DataSource dataSource, HttpServletRequest request) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		int count=0;
		try {
			con = dataSource.getConnection();
			System.out.println(couursename+"AND"+cityname);
			System.out.println("1" + couursename);
			if (cityname == null && coursemode != null
					&& couursename != null) {
				System.out.println(1);
				sql = "SELECT coursemater.CourseId,coursemater.coursecatId,coursemater.CourseName,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+ " LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where coursemater.active=1  and coursemater.elearning=1 and coursemater.CourseName like '%"
						+ couursename + "%' group by coursemater.courseid";
			}
			//System.out.println("2" + dynaform.getCityid());
			else if (couursename != null && coursemode == null
					&& cityname == null) {
				System.out.println(2);
				sql = "SELECT coursemater.CourseId,coursemater.coursecatId, coursemater.CourseName,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where coursemater.active=1 and coursemater.CourseName like '%"
						+ couursename + "%' group by coursemater.courseid  ";

			}
//			System.out.println("Third condition " + dynaform.getCityid()
//					+ "hi " + dynaform.getCourseName());
			else if (cityname != null && coursemode !=null
					&& couursename != null) {
				System.out.println(3);
				// sql =
				// "select um.Firstname,um.profilePhoto,um.coursesoffered,cm.fees from usermaster um, coursemater cm  where coursesoffered like '%"+dynaform.getCourseName()+"%' or cityId= '"+dynaform.getCityid()+"' and RoleID = '5' ";
				sql = "SELECT coursemater.CourseId,coursemater.coursecatId, coursemater.CourseName,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where coursemater.active=1   and coursemater.CourseName like '%"
						+ couursename
						+ "%' and companymaster.cityid="
						+ cityid + " and coursemater.classroomtraining=1 group by coursemater.courseid ";
			}
			System.out.println("sql query " + sql);
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				count++;
				roleForm = new DynamicDataForm();
				roleForm.setCoursesid(rs.getInt("CourseId"));
				roleForm.setCatid(rs.getInt("coursecatId"));
				roleForm.setCoursenamelatest(rs
						.getString("coursemater.CourseName"));
				System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
				roleForm.setCompname(rs.getString("companymaster.companyname"));
				roleForm.setCourselatestlogo(rs
						.getString("coursemater.imagename"));
				System.out.println("company"+rs.getString("coursemater.imagename"));
				roleForm.setFees(rs.getInt("coursemater.Fees"));
				roleForm.setCityname(rs.getString("citymaster.city"));
				roleForm.setCount(count);
				String rating = rs.getString("rate");
				
				if (rating.equals("0.0000")) {
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("1.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("2.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-2.png");
				}
				if (rating.equals("3.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-3.png");
				}
				if (rating.equals("4.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-4.png");
				}
				if (rating.equals("5.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-5.png");
				}



				System.out.println("vani " + rating);
				list.add(roleForm);

			}
			if(count==0)
			{
				request.setAttribute("coursenotfound","course "+couursename+" not foud");
				
				
			}
			if(count>0)
			{
				
				request.setAttribute("coursefound",""+count+" course foud");

				
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}

	public List<DynamicDataForm> LatestCourselist(DataSource dataSource) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();
		

			sql = "SELECT coursemater.CourseId,coursemater.coursecatId,coursemater.CourseName,coursemater.fees,coursemater.imagename,coursecategorymaster.coursecatname , avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ "LEFT JOIN coursecategorymaster ON "
					+ "coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					"LEFT JOIN companymaster ON "
					+ "coursecategorymaster.CompanyId=companymaster.CompanyId "
					+

					" LEFT "
					+ " JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where coursemater.active=1 group by coursemater.courseid";

			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setCoursenamelatest(rs
						.getString("coursemater.CourseName"));
				roleForm.setAmount(rs.getDouble("coursemater.fees"));
				roleForm.setCourselatestlogo(rs
						.getString("coursemater.imagename"));
				roleForm.setCoursecategory(rs
						.getString("coursecategorymaster.coursecatname"));
				
				roleForm.setCatid(rs
						.getInt("coursemater.coursecatId"));
				
				roleForm.setCourseId(rs
						.getString("coursemater.CourseId"));
				
				String rating = rs.getString("rate");
				if (rating.equals("0.0000")) {
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("1.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("2.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-2.png");
				}
				if (rating.equals("3.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-3.png");
				}
				if (rating.equals("4.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-4.png");
				}
				if (rating.equals("5.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-5.png");
				}

				System.out.println("vani " + rating);
				list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}

	public List<DynamicDataForm> LatestCourseCategory(DataSource dataSource) {

		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();

			sql = "select coursecatId,coursecatname from coursecategorymaster where Active=1 ";

			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setCoursecatid(rs.getInt("coursecatId"));
				roleForm.setCoursecatname(rs.getString("coursecatname"));
				list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}

	public List<DynamicDataForm> courselist(String id, DataSource dataSource) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();

//sql = "SELECT cat.*,course.* FROM `coursecategorymaster` cat JOIN `coursemater` course ON cat.coursecatId = course.coursecatId where Active=1 and coursecatname='"
//			+ id + "'";
			sql = "select CourseId,CourseName,Fees,imagename,coursecatId  from coursemater where Active=1 and coursecatId='"
					+ id + "'";
//coursecatId
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setCourseidnew(rs.getInt("CourseId"));
				roleForm.setCoursenamenew(rs.getString("CourseName"));
				roleForm.setAmountnew(rs.getDouble("Fees"));
				roleForm.setImagename(rs.getString("imagename"));
				roleForm.setCoursecatid(rs.getInt("coursecatId"));
				
				System.out.println("category id_++++++++++++"+rs.getInt("coursecatId"));
				

				list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}

	public List<DynamicDataForm> dynamicnumbers(DataSource dataSource) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();

			sql = "SELECT (SELECT COUNT(coursecatId) FROM coursecategorymaster) AS coursecategorymaster,(SELECT COUNT(CourseId) FROM coursemater) AS coursemater,(SELECT COUNT(CompanyId) FROM companymaster) AS companymaster,(SELECT COUNT(userid) FROM usermaster where roleid=3) AS usermaster";

			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setCoursecatcount(rs.getInt("coursecategorymaster"));
				roleForm.setCoursecount(rs.getInt("coursemater"));
				roleForm.setAcademycount(rs.getInt("companymaster"));
				roleForm.setStudentcount(rs.getInt("usermaster"));
				list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}

	public List<DynamicDataForm> reviewdetails(DataSource dataSource) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();

			sql = "SELECT coursemater.CourseName,usermaster.firstname,usermaster.profilePhoto,reviewmaster.reviewDescription FROM reviewmaster "
					+ " LEFT JOIN coursemater ON"
					+ " coursemater.CourseId=reviewmaster.reviewId"
					+

					" LEFT JOIN usermaster ON"
					+ " usermaster.UserId=reviewmaster.userid";

			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setStudentcourse(rs
						.getString("coursemater.CourseName"));
				roleForm.setStudentname(rs.getString("usermaster.firstname"));
				roleForm.setStudentphoto(rs
						.getString("usermaster.profilePhoto"));
				roleForm.setStudentreview(rs
						.getString("reviewmaster.reviewDescription"));

				list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}

	public String addsubscribe(DynamicDataForm roleForm, DataSource dataSource) {

		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String sql = "SELECT emialid FROM subscribe WHERE emialid=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, roleForm.getSubscribeemail());
			rs = pstmt.executeQuery();
			if (rs.next())
				result = Common.DUPLICATE_EMAIL_MESSAGE;
			else {
				if (!roleForm.getSubscribeemail().equalsIgnoreCase("")
						&& !roleForm.getSubscribeemail().equalsIgnoreCase(null)) {
					sql = "INSERT INTO subscribe (emialid,ACTIVE) VALUES (?,?)";
					if (pstmt != null)
						pstmt.close();
					pstmt = con.prepareStatement(sql);

					pstmt.setString(1, roleForm.getSubscribeemail());
					pstmt.setBoolean(2, true);

					pstmt.executeUpdate();
					if (rs != null)
						rs.close();
					result = Common.REC_ADDED;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;
	}
	
	public String addsubscribe2(String txtname,String txtemail,String contactnumber,String message,DataSource dataSource) {

		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			System.out.println("inside DAO_______________________________");
			con = dataSource.getConnection();
			String sql = "SELECT emialid FROM subscribe WHERE emialid=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1,txtemail);
			rs = pstmt.executeQuery();
			if (rs.next())
				result = Common.DUPLICATE_EMAIL_MESSAGE;
			else {
				if (!txtemail.equalsIgnoreCase("")
						&& !txtemail.equalsIgnoreCase(null)) {
					sql = "INSERT INTO subscribe (emialid,ACTIVE,Name,MobileNo,Message,EnquiryType) VALUES (?,?,?,?,?,?)";
					System.out.println(sql);			
					if (pstmt != null)
						pstmt.close();
					pstmt = con.prepareStatement(sql);

					pstmt.setString(1,txtemail);
					pstmt.setBoolean(2, true);
					pstmt.setString(3, txtname);
					pstmt.setString(4, contactnumber);
					pstmt.setString(5, message);
					pstmt.setString(6, "Enquiry");
					pstmt.executeUpdate();
					if (rs != null)
						rs.close();
					result = Common.REC_ADDED;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	public List<DynamicDataForm> populardetails(DataSource dataSource) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();

			sql = "SELECT coursemater.CourseName,coursemater.imagename,companymaster.CompanyName, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+

					" LEFT "
					+ " JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					"  where coursemater.active=1   group by ratingtable.rating desc limit 4";

			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setPopularcourse(rs
						.getString("coursemater.CourseName"));
				roleForm.setPopularcompname(rs
						.getString("companymaster.CompanyName"));
				roleForm.setPopularlogo(rs.getString("coursemater.imagename"));

				String rating = rs.getString("rate");
				if (rating.equals("0.0000")) {
					roleForm.setPopularrating("images/smallrating-1.png");
				}
				if (rating.equals("1.0000")) {
					System.out.println("yes");
					roleForm.setPopularrating("images/smallrating-1.png");
				}
				if (rating.equals("2.0000")) {
					System.out.println("yes");
					roleForm.setPopularrating("images/smallrating-2.png");
				}
				if (rating.equals("3.0000")) {
					System.out.println("yes");
					roleForm.setPopularrating("images/smallrating-3.png");
				}
				if (rating.equals("4.0000")) {
					System.out.println("yes");
					roleForm.setPopularrating("images/smallrating-4.png");
				}
				if (rating.equals("5.0000")) {
					System.out.println("yes");
					roleForm.setPopularrating("images/smallrating-5.png");
				}

				System.out.println("vani " + rating);
				list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}

	public List<DynamicDataForm> recentdetails(DataSource dataSource) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();

			sql = "SELECT coursemater.CourseName,coursemater.imagename,companymaster.CompanyName, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+

					" LEFT "
					+ " JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					"  where coursemater.active=1 group by coursemater.addeddate desc limit 3";

			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setPopularcourse(rs
						.getString("coursemater.CourseName"));
				roleForm.setPopularcompname(rs
						.getString("companymaster.CompanyName"));
				roleForm.setPopularlogo(rs.getString("coursemater.imagename"));

				String rating = rs.getString("rate");
				if (rating.equals("0.0000")) {
					roleForm.setPopularrating("images/smallrating-1.png");
				}
				if (rating.equals("1.0000")) {
					System.out.println("yes");
					roleForm.setPopularrating("images/smallrating-1.png");
				}
				if (rating.equals("2.0000")) {
					System.out.println("yes");
					roleForm.setPopularrating("images/smallrating-2.png");
				}
				if (rating.equals("3.0000")) {
					System.out.println("yes");
					roleForm.setPopularrating("images/smallrating-3.png");
				}
				if (rating.equals("4.0000")) {
					System.out.println("yes");
					roleForm.setPopularrating("images/smallrating-4.png");
				}
				if (rating.equals("5.0000")) {
					System.out.println("yes");
					roleForm.setPopularrating("images/smallrating-5.png");
				}

				System.out.println("vani " + rating);
				list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}

	public List<DynamicDataForm> latestcourses(DataSource dataSource) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();

			sql = "SELECT coursemater.CourseName,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where coursemater.active=1 group by coursemater.courseid limit 12";

			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setCoursenamelatest(rs
						.getString("coursemater.CourseName"));
				System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
				roleForm.setCompname(rs.getString("companymaster.companyname"));
				roleForm.setCourselatestlogo(rs
						.getString("coursemater.imagename"));
				roleForm.setFees(rs.getInt("coursemater.Fees"));
				roleForm.setCityname(rs.getString("citymaster.city"));
				String rating = rs.getString("rate");
				if (rating.equals("0.0000")) {
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("1.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("2.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-2.png");
				}
				if (rating.equals("3.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-3.png");
				}
				if (rating.equals("4.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-4.png");
				}
				if (rating.equals("5.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-5.png");
				}

				System.out.println("vani " + rating);
				list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}

	public List<DynamicDataForm> latestcoursessearch(
			DynamicDataForm dynamicdataform, DataSource dataSource) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();
            if(dynamicdataform.getCity().length()==0 && dynamicdataform.getLocality().length()==0)
            {
			
			sql = "SELECT coursemater.CourseName,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ "	coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON  "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid "
					+ " LEFT JOIN areamaster on "
					+ " areamaster.CompanyId  = companymaster.CompanyId  "
					+

					" where coursemater.active=1  and coursemater.coursename like '%"
					+ dynamicdataform.getCourseNamelatests()
					+ "%'  "
					

					+" group by coursemater.courseid ";
            }
            else if(dynamicdataform.getCourseNamelatests().length()==0 && dynamicdataform.getLocality().length()==0)
            {
            	sql = "SELECT coursemater.CourseName,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ "	coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON  "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid "
					+ " LEFT JOIN areamaster on "
					+ " areamaster.CompanyId  = companymaster.CompanyId  "
					+

					" where coursemater.active=1  and " 
					
				
					+ " citymaster.city like '%" 
					+ dynamicdataform.getCity() + "%' " +

					" group by coursemater.courseid ";
            }
            
            else if(dynamicdataform.getCourseNamelatests().length()==0 && dynamicdataform.getCity().length()==0)
            {
            	sql = "SELECT coursemater.CourseName,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ "	coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON  "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid "
					+ " LEFT JOIN areamaster on "
					+ " areamaster.CompanyId  = companymaster.CompanyId  "
					+

					" where coursemater.active=1  and "
				
					+

					" areamaster.areaname like '%"
					+ dynamicdataform.getLocality()
					+ "%' "
					+

					

					" group by coursemater.courseid ";
            }
            else
            {
            	sql = "SELECT coursemater.CourseName,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ "	coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON  "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid "
					+ " LEFT JOIN areamaster on "
					+ " areamaster.CompanyId  = companymaster.CompanyId  "
					+

					" where coursemater.active=1  and coursemater.coursename like '%"
					+ dynamicdataform.getCourseNamelatests()
					+ "%'  "
					+ "  or "
					+

					" areamaster.areaname like '%"
					+ dynamicdataform.getLocality()
					+ "%' "
					+

					" or "
					+ " citymaster.city like '%"
					+ dynamicdataform.getCity() + "%' " +

					" group by coursemater.courseid ";
            }
			System.out.println(sql);

			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setCoursenamelatest(rs
						.getString("coursemater.CourseName"));
				roleForm.setCompname(rs.getString("companymaster.companyname"));
				roleForm.setCourselatestlogo(rs
						.getString("coursemater.imagename"));
				roleForm.setCityname(rs.getString("citymaster.city"));
				String rating = rs.getString("rate");
				if (rating.equals("0.0000")) {
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("1.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("2.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-2.png");
				}
				if (rating.equals("3.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-3.png");
				}
				if (rating.equals("4.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-4.png");
				}
				if (rating.equals("5.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-5.png");
				}

				System.out.println("vani " + rating);
				list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}

	public List<DynamicDataForm> dynamicnumberslatest(DataSource dataSource)
	{
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();

			sql = "SELECT (SELECT COUNT(classroomtraining) FROM coursemater where classroomtraining='1') AS classroomtraining, "+
"  (SELECT COUNT(elearning) FROM coursemater where elearning= '1' ) AS elearning";

			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setCoursecatcount(rs.getInt("classroomtraining"));
				roleForm.setCoursecount(rs.getInt("elearning"));
				//roleForm.setAcademycount(rs.getInt("companymaster"));
				//roleForm.setStudentcount(rs.getInt("usermaster"));
				list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}

	public List<DynamicDataForm> searchelearningcourse(DataSource dataSource)
	{


			List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
			DynamicDataForm roleForm = null;

			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			String sql = null;
			try {
				con = dataSource.getConnection();
	          
	        
				
				sql = "SELECT coursemater.CourseName,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
						+ " LEFT JOIN coursecategorymaster ON "
						+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
						+

						" LEFT JOIN companymaster ON "
						+ "	coursecategorymaster.CompanyId=companymaster.CompanyId "
						+ " LEFT JOIN citymaster ON  "
						+ " citymaster.cityid=companymaster.cityid "
						+

						" LEFT JOIN ratingtable ON "
						+ " coursemater.courseid=ratingtable.courseid "
						+ " LEFT JOIN areamaster on "
						+ " areamaster.CompanyId  = companymaster.CompanyId  "
						+

						" where coursemater.active=1  and coursemater.elearning = 1"
						
						

						+" group by coursemater.courseid ";
	        
	            
	            
				System.out.println(sql);

				pstmt = con.prepareStatement(sql);
				rs = pstmt.executeQuery();
				while (rs.next()) {
					roleForm = new DynamicDataForm();
					roleForm.setCoursenamelatest(rs
							.getString("coursemater.CourseName"));
					roleForm.setCompname(rs.getString("companymaster.companyname"));
					roleForm.setCourselatestlogo(rs
							.getString("coursemater.imagename"));
					roleForm.setCityname(rs.getString("citymaster.city"));
					String rating = rs.getString("rate");
					if (rating.equals("0.0000")) {
						roleForm.setRating("images/smallrating-1.png");
					}
					if (rating.equals("1.0000")) {
						System.out.println("yes");
						roleForm.setRating("images/smallrating-1.png");
					}
					if (rating.equals("2.0000")) {
						System.out.println("yes");
						roleForm.setRating("images/smallrating-2.png");
					}
					if (rating.equals("3.0000")) {
						System.out.println("yes");
						roleForm.setRating("images/smallrating-3.png");
					}
					if (rating.equals("4.0000")) {
						System.out.println("yes");
						roleForm.setRating("images/smallrating-4.png");
					}
					if (rating.equals("5.0000")) {
						System.out.println("yes");
						roleForm.setRating("images/smallrating-5.png");
					}

					System.out.println("vani " + rating);
					list.add(roleForm);
				}

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					ConnectionUtil.closeResources(con, pstmt, rs);
				} catch (Exception e2) {
				}
			}
			return list;
	}

	public List<DynamicDataForm> selectingcourses(DataSource dataSource, String eleaning, String classroom, String paid, String free, String beginner, String intermediate, String advanced, String lessfive, String graterfive, String gratertwo, String greaterfivethousand, String categorynames, String institutenames, String localitynames, String cities, String coursenames) {
		// TODO Auto-generated method stub
System.out.println("testing elearning courses"+eleaning);
System.out.println("testing classroom courses"+classroom);
System.out.println("testing classroom beginners"+classroom);
String greaterfivehun=graterfive.substring(0, 3);
System.out.println("graterfive"+greaterfivehun);
String lesstwot=graterfive.substring(3, 7);
System.out.println("lesstwot"+lesstwot);
String greatertwothousnd=gratertwo.substring(0, 4);
String lesseqfive=gratertwo.substring(4, 8);
System.out.println("beginnerssssssssss"+beginner);
System.out.println("localitynamessssssssssssssssssss"+localitynames);
System.out.println("institute"+institutenames);
System.out.println("coursenames"+coursenames);
String testcondition="";

int count=0;

String where="";

String query1="";
String query2="";

if((eleaning.equals("1")||classroom.equals("1"))&&!(paid.equals("0")|| free.equals("1"))&&!(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&!(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000")))))){
	query1="(coursemater.classroomtraining="+classroom+" or coursemater.elearning="+eleaning+")";
	}
else if(!(eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&!(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&!(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000")))))){

	
		query1="coursemater.Fees>"+paid+" or coursemater.free="+free+"";
		
		
	}
else if((eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&!(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&!(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000")))))){
	
		query1="(coursemater.classroomtraining="+classroom+" or coursemater.elearning="+eleaning+") and (coursemater.Fees>"+paid+" or coursemater.free="+free+") ";

	}
		
else if(!(eleaning.equals("1")||classroom.equals("1"))&&!(paid.equals("0")|| free.equals("1"))&&(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&!(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000")))))){
	
		query1="coursemater.level='"+beginner+"' or coursemater.level='"+intermediate+"' or coursemater.level='"+advanced+"'";
	}
else if((eleaning.equals("1")||classroom.equals("1"))&&!(paid.equals("0")|| free.equals("1"))&&(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))){
	
		query1="(coursemater.classroomtraining="+classroom+" or coursemater.elearning="+eleaning+") and (coursemater.level='"+beginner+"' or coursemater.level='"+intermediate+"' or coursemater.level='"+advanced+"') ";
	}
else if(!(eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&!(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000")))))){
	
		query1="(coursemater.Fees>"+paid+" or coursemater.free="+free+") and (coursemater.level='"+beginner+"' or coursemater.level='"+intermediate+"' or coursemater.level='"+advanced+"')";
	}

else if((eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&!(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
	{
		query1="(coursemater.classroomtraining="+classroom+" or coursemater.elearning="+eleaning+") and (coursemater.Fees>"+paid+" or coursemater.free="+free+") and (coursemater.level='"+beginner+"' or coursemater.level='"+intermediate+"' or coursemater.level='"+advanced+"')";
	}
else if(!(eleaning.equals("1")||classroom.equals("1"))&&!(paid.equals("0")|| free.equals("1"))&&!(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
{
	query1="coursemater.Fees<="+lessfive+" or coursemater.Fees>"+greaterfivehun+" and coursemater.Fees<="+lesstwot+" or coursemater.Fees>"+greatertwothousnd+" and coursemater.Fees<="+lesseqfive+" or coursemater.Fees>"+greaterfivethousand+"";
	
}
else if((eleaning.equals("1")||classroom.equals("1"))&&!(paid.equals("0")|| free.equals("1"))&&!(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
{
	query1="(coursemater.classroomtraining="+classroom+" or coursemater.elearning="+eleaning+")and(coursemater.Fees<="+lessfive+" or coursemater.Fees>"+greaterfivehun+" and coursemater.Fees<="+lesstwot+" or coursemater.Fees>"+greatertwothousnd+" and coursemater.Fees<="+lesseqfive+" or coursemater.Fees>"+greaterfivethousand+")";
	
}
else if(!(eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&!(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
{
	query1="(coursemater.Fees>"+paid+" or coursemater.free="+free+")and(coursemater.Fees<="+lessfive+" or coursemater.Fees>"+greaterfivehun+" and coursemater.Fees<="+lesstwot+" or coursemater.Fees>"+greatertwothousnd+" and coursemater.Fees<="+lesseqfive+" or coursemater.Fees>"+greaterfivethousand+")";
	
}

else if(!(eleaning.equals("1")||classroom.equals("1"))&&!(paid.equals("0")|| free.equals("1"))&&(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
{
	query1="(coursemater.level='"+beginner+"' or coursemater.level='"+intermediate+"' or coursemater.level='"+advanced+"')and(coursemater.Fees<="+lessfive+" or coursemater.Fees>"+greaterfivehun+" and coursemater.Fees<="+lesstwot+" or coursemater.Fees>"+greatertwothousnd+" and coursemater.Fees<="+lesseqfive+" or coursemater.Fees>"+greaterfivethousand+")";
	
}
else if((eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&!(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
{
	query1="(coursemater.classroomtraining="+classroom+" or coursemater.elearning="+eleaning+") and(coursemater.Fees>"+paid+" or coursemater.free="+free+")and(coursemater.Fees<="+lessfive+" or coursemater.Fees>"+greaterfivehun+" and coursemater.Fees<="+lesstwot+" or coursemater.Fees>"+greatertwothousnd+" and coursemater.Fees<="+lesseqfive+" or coursemater.Fees>"+greaterfivethousand+")";
	
}
else if((eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
{
	query1="(coursemater.classroomtraining="+classroom+" or coursemater.elearning="+eleaning+") and(coursemater.Fees>"+paid+" or coursemater.free="+free+")and(coursemater.level='"+beginner+"' or coursemater.level='"+intermediate+"' or coursemater.level='"+advanced+"')and(coursemater.Fees<="+lessfive+" or coursemater.Fees>"+greaterfivehun+" and coursemater.Fees<="+lesstwot+" or coursemater.Fees>"+greatertwothousnd+" and coursemater.Fees<="+lesseqfive+" or coursemater.Fees>"+greaterfivethousand+")";
	
}

else if(!(eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
{
	query1="(coursemater.Fees>"+paid+" or coursemater.free="+free+")and(coursemater.level='"+beginner+"' or coursemater.level='"+intermediate+"' or coursemater.level='"+advanced+"')and(coursemater.Fees<="+lessfive+" or coursemater.Fees>"+greaterfivehun+" and coursemater.Fees<="+lesstwot+" or coursemater.Fees>"+greatertwothousnd+" and coursemater.Fees<="+lesseqfive+" or coursemater.Fees>"+greaterfivethousand+")";
	
}



	List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;
String getnumber="";
String getcompid="";
String concatnumber="";
String coursecatid="";
String companyid="";
String areaid="";
String cityid="";
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();
			if(categorynames.equalsIgnoreCase("")&&institutenames.equalsIgnoreCase("")&&localitynames.equalsIgnoreCase("")&&cities.equalsIgnoreCase("")&&coursenames.equalsIgnoreCase(""))
			{

			sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where(coursemater.active=1) and "+query1+"  group by coursemater.courseid";
			

						System.out.println("sqlllllllllllllllllll"+sql);
						pstmt = con.prepareStatement(sql);
						rs = pstmt.executeQuery();
						while (rs.next()) {
							System.out.println("first condition");
							roleForm = new DynamicDataForm();
							roleForm.setCoursenamelatest(rs
									.getString("coursemater.CourseName"));
							System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
							roleForm.setFees(rs.getDouble("coursemater.fees"));
							roleForm.setCoursesid(rs.getInt("CourseId"));
							roleForm.setCatid(rs.getInt("coursecatId"));
                            roleForm.setCompname(rs.getString("companymaster.companyname"));
							roleForm.setCourselatestlogo(rs.getString("coursemater.imagename"));
							roleForm.setCityname(rs.getString("citymaster.city"));
							String rating = rs.getString("rate");
							if (rating.equals("0.0000")) {
								roleForm.setRating("images/smallrating-1.png");
							}
							if (rating.equals("1.0000")) {
								System.out.println("yes");
								roleForm.setRating("images/smallrating-1.png");
							}
							if (rating.equals("2.0000")) {
								System.out.println("yes");
								roleForm.setRating("images/smallrating-2.png");
							}
							if (rating.equals("3.0000")) {
								System.out.println("yes");
								roleForm.setRating("images/smallrating-3.png");
							}
							if (rating.equals("4.0000")) {
								System.out.println("yes");
								roleForm.setRating("images/smallrating-4.png");
							}
							if (rating.equals("5.0000")) {
								System.out.println("yes");
								roleForm.setRating("images/smallrating-5.png");
							}

							System.out.println("vani " + rating);
							list.add(roleForm);
						}
			}
			else if(!categorynames.equalsIgnoreCase("")&&!query1.equalsIgnoreCase("")&&institutenames.equalsIgnoreCase("")&&localitynames.equalsIgnoreCase("")&&cities.equalsIgnoreCase("")&&coursenames.equalsIgnoreCase(""))
						{
							System.out.println("both");
						      String[] stringArray = categorynames.split(",");

						      for (int i = 0; i < stringArray.length; i++) { 
						    	  
						    	  
						    	  sql="select distinct  coursecatId from coursemater where active=1 and coursecatId="+stringArray[i]+" ";
						    	  
									pstmt = con.prepareStatement(sql);

									rs = pstmt.executeQuery();
									while (rs.next()) {
										getnumber=rs.getString("coursecatId");
										concatnumber=concatnumber.concat(",").concat(getnumber);
										System.out.println("concatnumber"+concatnumber);
										coursecatid=concatnumber.substring(1, concatnumber.length());
										System.out.println("coursecatid"+coursecatid);
										
									}

						      }
						      String[] stringArrays = coursecatid.split(",");
						      for (int i = 0; i < stringArrays.length; i++) { 


						      
						    	  sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
										+ " LEFT JOIN coursecategorymaster ON "
										+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
										+

										" LEFT JOIN companymaster ON "
										
										+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
										+ " LEFT JOIN citymaster ON "
										+ " citymaster.cityid=companymaster.cityid "
										+

										" LEFT JOIN ratingtable ON "
										+ " coursemater.courseid=ratingtable.courseid " +

										" where (coursemater.active=1) and "+query1+" and(coursemater.coursecatId="+stringArrays[i]+")  group by coursemater.courseid";
					System.out.println("sqllllllllllllllllllllllllll"+sql);
						pstmt = con.prepareStatement(sql);
						rs = pstmt.executeQuery();
						while (rs.next()) {
							System.out.println("second condition");
							roleForm = new DynamicDataForm();
							roleForm.setCoursenamelatest(rs
									.getString("coursemater.CourseName"));
							System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
							roleForm.setCoursesid(rs.getInt("CourseId"));
							roleForm.setCatid(rs.getInt("coursecatId"));
							roleForm.setFees(rs.getDouble("coursemater.fees"));
							roleForm.setCompname(rs.getString("companymaster.companyname"));
							roleForm.setCourselatestlogo(rs
									.getString("coursemater.imagename"));
							roleForm.setCityname(rs.getString("citymaster.city"));
							String rating = rs.getString("rate");
							if (rating.equals("0.0000")) {
								roleForm.setRating("images/smallrating-1.png");
							}
							if (rating.equals("1.0000")) {
								System.out.println("yes");
								roleForm.setRating("images/smallrating-1.png");
							}
							if (rating.equals("2.0000")) {
								System.out.println("yes");
								roleForm.setRating("images/smallrating-2.png");
							}
							if (rating.equals("3.0000")) {
								System.out.println("yes");
								roleForm.setRating("images/smallrating-3.png");
							}
							if (rating.equals("4.0000")) {
								System.out.println("yes");
								roleForm.setRating("images/smallrating-4.png");
							}
							if (rating.equals("5.0000")) {
								System.out.println("yes");
								roleForm.setRating("images/smallrating-5.png");
							}

							System.out.println("vani " + rating);
							list.add(roleForm);
						}
						
						
						
						      }

						}	
			
			
			else if(categorynames.equalsIgnoreCase("")&&query1.equalsIgnoreCase("")&&!institutenames.equalsIgnoreCase("")&&localitynames.equalsIgnoreCase("")&&cities.equalsIgnoreCase("")&&coursenames.equalsIgnoreCase(""))

			{
				testcondition="onlyforinstitute";
				System.out.println("only for institute");
			      String[] stringArray = institutenames.split(",");

			      for (int i = 0; i < stringArray.length; i++) { 
			    	  
			    	  System.out.println("stringArray.length"+stringArray.length);
			    	  sql="select distinct CompanyId from coursemater where Active=1 and CompanyId="+stringArray[i]+" ";
			    	  
						pstmt = con.prepareStatement(sql);

						rs = pstmt.executeQuery();
						while (rs.next()) {
							getnumber=rs.getString("CompanyId");
							concatnumber=concatnumber.concat(",").concat(getnumber);
							System.out.println("concatnumber"+concatnumber);
							companyid=concatnumber.substring(1, concatnumber.length());
							System.out.println("companyid"+companyid);							
						}

			      }
			      String[] stringArrays = companyid.split(",");
			      for (int i = 0; i < stringArrays.length; i++) { 


			      
			    	  sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
							+ " LEFT JOIN coursecategorymaster ON "
							+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
							+

							" LEFT JOIN companymaster ON "
							
							+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
							+ " LEFT JOIN citymaster ON "
							+ " citymaster.cityid=companymaster.cityid "
							+

							" LEFT JOIN ratingtable ON "
							+ " coursemater.courseid=ratingtable.courseid " +

							" where (coursemater.active=1) and(coursemater.CompanyId="+stringArrays[i]+")  group by coursemater.courseid";
		System.out.println("sqllllllllllllllllllllllllll"+sql);
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				System.out.println("second condition");
				roleForm = new DynamicDataForm();
				roleForm.setCoursenamelatest(rs
						.getString("coursemater.CourseName"));
				roleForm.setTestingstring(testcondition);
				System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
				roleForm.setCoursesid(rs.getInt("CourseId"));
				roleForm.setCatid(rs.getInt("coursecatId"));
				roleForm.setFees(rs.getDouble("coursemater.fees"));
				roleForm.setCompname(rs.getString("companymaster.companyname"));
				roleForm.setCourselatestlogo(rs
						.getString("coursemater.imagename"));
				roleForm.setCityname(rs.getString("citymaster.city"));
				String rating = rs.getString("rate");
				if (rating.equals("0.0000")) {
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("1.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("2.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-2.png");
				}
				if (rating.equals("3.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-3.png");
				}
				if (rating.equals("4.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-4.png");
				}
				if (rating.equals("5.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-5.png");
				}

				System.out.println("vani " + rating);
				list.add(roleForm);
			}
			
			
			
			      }

			}	
			
			
			
			
			
			
			
    else if(!categorynames.equalsIgnoreCase("")&&query1.equalsIgnoreCase("")&&institutenames.equalsIgnoreCase("")&&localitynames.equalsIgnoreCase("")&&cities.equalsIgnoreCase("")&&coursenames.equalsIgnoreCase(""))
			{
				System.out.println("other options");
			      String[] stringArray = categorynames.split(",");

			      for (int i = 0; i < stringArray.length; i++) { 
			    	  
			    	  
			    	  sql="select distinct coursecatId from coursemater where active=1 and coursecatId="+stringArray[i]+" ";
			    	  
						pstmt = con.prepareStatement(sql);

						rs = pstmt.executeQuery();
						while (rs.next()) {
							getnumber=rs.getString("coursecatId");
							concatnumber=concatnumber.concat(",").concat(getnumber);
							System.out.println("concatnumber"+concatnumber);
							coursecatid=concatnumber.substring(1, concatnumber.length());
							System.out.println("coursecatid"+coursecatid);
							
						}

			      }
			      String[] stringArrays = coursecatid.split(",");
			      for (int i = 0; i < stringArrays.length; i++) { 


			      
			    	  sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
							+ " LEFT JOIN coursecategorymaster ON "
							+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
							+

							" LEFT JOIN companymaster ON "
							+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
							+ " LEFT JOIN citymaster ON "
							+ " citymaster.cityid=companymaster.cityid "
							+

							" LEFT JOIN ratingtable ON "
							+ " coursemater.courseid=ratingtable.courseid " +

							" where (coursemater.active=1)  and(coursemater.coursecatId="+stringArrays[i]+")  group by coursemater.courseid";
		System.out.println("sqllllllllllllllllllllllllll"+sql);
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				System.out.println("third condition");
				roleForm = new DynamicDataForm();
				roleForm.setCoursenamelatest(rs
						.getString("coursemater.CourseName"));
				System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
				roleForm.setFees(rs.getDouble("coursemater.fees"));
				roleForm.setCoursesid(rs.getInt("CourseId"));
				roleForm.setCatid(rs.getInt("coursecatId"));
				roleForm.setCompname(rs.getString("companymaster.companyname"));
				roleForm.setCourselatestlogo(rs
						.getString("coursemater.imagename"));
				roleForm.setCityname(rs.getString("citymaster.city"));
				String rating = rs.getString("rate");
				if (rating.equals("0.0000")) {
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("1.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("2.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-2.png");
				}
				if (rating.equals("3.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-3.png");
				}
				if (rating.equals("4.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-4.png");
				}
				if (rating.equals("5.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-5.png");
				}

				System.out.println("vani " + rating);
				list.add(roleForm);
			}
			
			}

			}
    else if(categorynames.equalsIgnoreCase("")&&query1.equalsIgnoreCase("")&&institutenames.equalsIgnoreCase("")&&!localitynames.equalsIgnoreCase("")&&cities.equalsIgnoreCase("")&&coursenames.equalsIgnoreCase(""))
	{
		System.out.println("other options");
	      String[] stringArray = localitynames.split(",");

	      for (int i = 0; i < stringArray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct areaid from coursemater where Active=1 and areaid="+stringArray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getnumber=rs.getString("areaid");
					concatnumber=concatnumber.concat(",").concat(getnumber);
					System.out.println("concatnumber"+concatnumber);
					areaid=concatnumber.substring(1, concatnumber.length());
					System.out.println("areaid"+areaid);
					
				}

	      }
	      String[] stringArrays = areaid.split(",");
	      for (int i = 0; i < stringArrays.length; i++) { 


	      
	    	  sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where (coursemater.active=1)  and(coursemater.areaid="+stringArrays[i]+")  group by coursemater.courseid";
System.out.println("sqllllllllllllllllllllllllll"+sql);
	pstmt = con.prepareStatement(sql);
	rs = pstmt.executeQuery();
	while (rs.next()) {
		System.out.println("third condition");
		roleForm = new DynamicDataForm();
		roleForm.setCoursenamelatest(rs
				.getString("coursemater.CourseName"));
		System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
		roleForm.setFees(rs.getDouble("coursemater.fees"));
		roleForm.setCoursesid(rs.getInt("CourseId"));
		roleForm.setCatid(rs.getInt("coursecatId"));
		roleForm.setCompname(rs.getString("companymaster.companyname"));
		roleForm.setCourselatestlogo(rs
				.getString("coursemater.imagename"));
		roleForm.setCityname(rs.getString("citymaster.city"));
		String rating = rs.getString("rate");
		if (rating.equals("0.0000")) {
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("1.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("2.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-2.png");
		}
		if (rating.equals("3.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-3.png");
		}
		if (rating.equals("4.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-4.png");
		}
		if (rating.equals("5.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-5.png");
		}

		System.out.println("vani " + rating);
		list.add(roleForm);
	}
	
	}

	}
    else if(categorynames.equalsIgnoreCase("")&&query1.equalsIgnoreCase("")&&institutenames.equalsIgnoreCase("")&&localitynames.equalsIgnoreCase("")&&!cities.equalsIgnoreCase("")&&coursenames.equalsIgnoreCase(""))
	{
		System.out.println("other options");
	      String[] stringArray = cities.split(",");

	      for (int i = 0; i < stringArray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct cityid from coursemater where active=1 and cityid="+stringArray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getnumber=rs.getString("cityid");
					concatnumber=concatnumber.concat(",").concat(getnumber);
					System.out.println("concatnumber"+concatnumber);
					cityid=concatnumber.substring(1, concatnumber.length());
					System.out.println("cityid"+cityid);
					
				}

	      }
	      String[] stringArrays = cityid.split(",");
	      for (int i = 0; i < stringArrays.length; i++) { 


	      
	    	  sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where (coursemater.active=1)  and(coursemater.cityid="+stringArrays[i]+")  group by coursemater.courseid";
System.out.println("sqllllllllllllllllllllllllll"+sql);
	pstmt = con.prepareStatement(sql);
	rs = pstmt.executeQuery();
	while (rs.next()) {
		System.out.println("third condition");
		roleForm = new DynamicDataForm();
		roleForm.setCoursenamelatest(rs
				.getString("coursemater.CourseName"));
		System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
		roleForm.setFees(rs.getDouble("coursemater.fees"));
		roleForm.setCoursesid(rs.getInt("CourseId"));
		roleForm.setCatid(rs.getInt("coursecatId"));
		roleForm.setCompname(rs.getString("companymaster.companyname"));
		roleForm.setCourselatestlogo(rs
				.getString("coursemater.imagename"));
		roleForm.setCityname(rs.getString("citymaster.city"));
		String rating = rs.getString("rate");
		if (rating.equals("0.0000")) {
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("1.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("2.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-2.png");
		}
		if (rating.equals("3.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-3.png");
		}
		if (rating.equals("4.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-4.png");
		}
		if (rating.equals("5.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-5.png");
		}
		System.out.println("vani " + rating);
		list.add(roleForm);
	}
	
	}

	}
		
    else if(categorynames.equalsIgnoreCase("")&&query1.equalsIgnoreCase("")&&!institutenames.equalsIgnoreCase("")&&!localitynames.equalsIgnoreCase("")&&cities.equalsIgnoreCase("")&&coursenames.equalsIgnoreCase(""))
	{
    	System.out.println("inside action institute and locality");
	      String[] institutearray = institutenames.split(",");

	      for (int i = 0; i < institutearray.length; i++) { 
	    	  
	    	  System.out.println("stringArray.length"+institutearray.length);
	    	  sql="select distinct CompanyId from coursemater where Active=1 and CompanyId="+institutearray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getnumber=rs.getString("CompanyId");
					concatnumber=concatnumber.concat(",").concat(getnumber);
					System.out.println("concatnumber"+concatnumber);
					companyid=concatnumber.substring(1, concatnumber.length());
					System.out.println("companyid"+companyid);
					
				}

	      }
	      System.out.println("other options");
	      String[] localityarray = localitynames.split(",");

	      for (int i = 0; i < localityarray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct areaid from coursemater where Active=1 and areaid="+localityarray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getnumber=rs.getString("areaid");
					concatnumber=concatnumber.concat(",").concat(getnumber);
					System.out.println("concatnumber"+concatnumber);
					areaid=concatnumber.substring(1, concatnumber.length());
					System.out.println("areaid"+areaid);
					
				}

	      }

	      String[] companyarraysplit = companyid.split(",");
	      String[] localitysplit = areaid.split(",");

	      for (int i = 0; i < companyarraysplit.length||i < localitysplit.length ; i++) { 


	    	  sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where (coursemater.active=1)  and(coursemater.CompanyId="+companyarraysplit[i]+")and(coursemater.areaid="+localitysplit[i]+")  group by coursemater.courseid";
System.out.println("sqllllllllllllllllllllllllllbothhhhhhhhhhhhhhhhhhhh"+sql);
	pstmt = con.prepareStatement(sql);
	rs = pstmt.executeQuery();

	while (rs.next()) {
		System.out.println("third condition");
		roleForm = new DynamicDataForm();
		roleForm.setCoursenamelatest(rs
				.getString("coursemater.CourseName"));
		System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
		roleForm.setFees(rs.getDouble("coursemater.fees"));
		roleForm.setCoursesid(rs.getInt("CourseId"));
		roleForm.setCatid(rs.getInt("coursecatId"));
		roleForm.setCompname(rs.getString("companymaster.companyname"));
		roleForm.setCourselatestlogo(rs
				.getString("coursemater.imagename"));
		roleForm.setCityname(rs.getString("citymaster.city"));
		String rating = rs.getString("rate");
		if (rating.equals("0.0000")) {
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("1.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("2.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-2.png");
		}
		if (rating.equals("3.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-3.png");
		}
		if (rating.equals("4.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-4.png");
		}
		if (rating.equals("5.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-5.png");
		}

		System.out.println("vani " + rating);
		list.add(roleForm);
	}
	
	}

	}
    else if(categorynames.equalsIgnoreCase("")&&query1.equalsIgnoreCase("")&&!institutenames.equalsIgnoreCase("")&&localitynames.equalsIgnoreCase("")&&!cities.equalsIgnoreCase("")&&coursenames.equalsIgnoreCase(""))
	{
    	System.out.println("inside action institute and locality");
	      String[] institutearray = institutenames.split(",");

	      for (int i = 0; i < institutearray.length; i++) { 
	    	  
	    	  System.out.println("stringArray.length"+institutearray.length);
	    	  sql="select distinct CompanyId from coursemater where Active=1 and CompanyId="+institutearray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getnumber=rs.getString("CompanyId");
					concatnumber=concatnumber.concat(",").concat(getnumber);
					System.out.println("concatnumber"+concatnumber);
					companyid=concatnumber.substring(1, concatnumber.length());
					System.out.println("companyid"+companyid);
					
				}

	      }
	      System.out.println("other options");
	      String[] cityarray = cities.split(",");

	      for (int i = 0; i < cityarray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct cityid from coursemater where active=1 and areaid="+cityarray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getnumber=rs.getString("cityid");
					concatnumber=concatnumber.concat(",").concat(getnumber);
					System.out.println("concatnumber"+concatnumber);
					cityid=concatnumber.substring(1, concatnumber.length());
					System.out.println("cityies"+cityid);
					
				}

	      }

	      String[] companyarraysplit = companyid.split(",");
	      String[] citysplit = cityid.split(",");

	      for (int i = 0; i < companyarraysplit.length||i < citysplit.length ; i++) { 


	    	  sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where (coursemater.active=1)  and(coursemater.CompanyId="+companyarraysplit[i]+")and(coursemater.cityid="+citysplit[i]+")  group by coursemater.courseid";
System.out.println("sqllllllllllllllllllllllllllbothhhhhhhhhhhhhhhhhhhh"+sql);
	pstmt = con.prepareStatement(sql);
	rs = pstmt.executeQuery();

	while (rs.next()) {
		System.out.println("third condition");
		roleForm = new DynamicDataForm();
		roleForm.setCoursenamelatest(rs
				.getString("coursemater.CourseName"));
		System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
		roleForm.setFees(rs.getDouble("coursemater.fees"));
		roleForm.setCoursesid(rs.getInt("CourseId"));
		roleForm.setCatid(rs.getInt("coursecatId"));
		roleForm.setCompname(rs.getString("companymaster.companyname"));
		roleForm.setCourselatestlogo(rs
				.getString("coursemater.imagename"));
		roleForm.setCityname(rs.getString("citymaster.city"));
		String rating = rs.getString("rate");
		if (rating.equals("0.0000")) {
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("1.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("2.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-2.png");
		}
		if (rating.equals("3.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-3.png");
		}
		if (rating.equals("4.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-4.png");
		}
		if (rating.equals("5.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-5.png");
		}

		System.out.println("vani " + rating);
		list.add(roleForm);
	}
	
	}

	}
    else if(categorynames.equalsIgnoreCase("")&&query1.equalsIgnoreCase("")&&!institutenames.equalsIgnoreCase("")&&!localitynames.equalsIgnoreCase("")&&!cities.equalsIgnoreCase("")&&coursenames.equalsIgnoreCase(""))
	{
    	System.out.println("inside action institute and locality and city");
    	testcondition="institutelocalityandcity";
	      String[] institutearray = institutenames.split(",");

	      for (int i = 0; i < institutearray.length; i++) { 
	    	  
	    	  System.out.println("stringArray.length"+institutearray.length);
	    	  sql="select distinct CompanyId from coursemater where Active=1 and CompanyId="+institutearray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getnumber=rs.getString("CompanyId");
					concatnumber=concatnumber.concat(",").concat(getnumber);
					System.out.println("concatnumber"+concatnumber);
					companyid=concatnumber.substring(1, concatnumber.length());
					System.out.println("companyid"+companyid);
					
				}

	      }
	      System.out.println("other options");
	      String[] cityarray = cities.split(",");

	      for (int i = 0; i < cityarray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct cityid from coursemater where active=1 and areaid="+cityarray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getnumber=rs.getString("cityid");
					concatnumber=concatnumber.concat(",").concat(getnumber);
					System.out.println("concatnumber"+concatnumber);
					cityid=concatnumber.substring(1, concatnumber.length());
					System.out.println("cityies"+cityid);
					
				}

	      }
	      String[] localityarray = localitynames.split(",");

	      for (int i = 0; i < localityarray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct areaid from coursemater where Active=1 and areaid="+localityarray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getnumber=rs.getString("areaid");
					concatnumber=concatnumber.concat(",").concat(getnumber);
					System.out.println("concatnumber"+concatnumber);
					areaid=concatnumber.substring(1, concatnumber.length());
					System.out.println("areaid"+areaid);
					
				}

	      }

	      String[] companyarraysplit = companyid.split(",");
	      String[] localitysplit = areaid.split(",");
       String[] citysplit = cityid.split(",");

	      for (int i = 0; i < companyarraysplit.length||i < citysplit.length||i<localitysplit.length ; i++) { 


	    	  sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where (coursemater.active=1)  and(coursemater.CompanyId="+companyarraysplit[i]+")and(coursemater.cityid="+citysplit[i]+")and(coursemater.areaid="+localitysplit[i]+")  group by coursemater.courseid";
System.out.println("sqllllllllllllllllllllllllllbothhhhhhhhhhhhhhhhhhhh"+sql);
	pstmt = con.prepareStatement(sql);
	rs = pstmt.executeQuery();

	while (rs.next()) {
		System.out.println("third condition");
		roleForm = new DynamicDataForm();
		roleForm.setCoursenamelatest(rs
				.getString("coursemater.CourseName"));
		System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
		roleForm.setFees(rs.getDouble("coursemater.fees"));
		roleForm.setTestingstring(testcondition);
		roleForm.setCoursesid(rs.getInt("CourseId"));
		roleForm.setCatid(rs.getInt("coursecatId"));
		roleForm.setCompname(rs.getString("companymaster.companyname"));
		roleForm.setCourselatestlogo(rs
				.getString("coursemater.imagename"));
		roleForm.setCityname(rs.getString("citymaster.city"));
		String rating = rs.getString("rate");
		if (rating.equals("0.0000")) {
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("1.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("2.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-2.png");
		}
		if (rating.equals("3.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-3.png");
		}
		if (rating.equals("4.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-4.png");
		}
		if (rating.equals("5.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-5.png");
		}

		System.out.println("vani " + rating);
		list.add(roleForm);
	}
	
	}

	}
    else if(!categorynames.equalsIgnoreCase("")&&query1.equalsIgnoreCase("")&&!institutenames.equalsIgnoreCase("")&&localitynames.equalsIgnoreCase("")&&cities.equalsIgnoreCase("")&&coursenames.equalsIgnoreCase(""))
	{
    	testcondition="Ctaegory and institute";
    	System.out.println("inside action institute and category");
	      String[] institutearray = institutenames.split(",");

	      for (int i = 0; i < institutearray.length; i++) { 
	    	  
	    	  System.out.println("stringArray.length"+institutearray.length);
	    	  sql="select distinct CompanyId from coursemater where Active=1 and CompanyId="+institutearray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getnumber=rs.getString("CompanyId");
					concatnumber=concatnumber.concat(",").concat(getnumber);
					System.out.println("concatnumber"+concatnumber);
					companyid=concatnumber.substring(1, concatnumber.length());
					System.out.println("companyid"+companyid);
					
				}

	      }
	      System.out.println("other options");
	      String[] stringArray = categorynames.split(",");

	      for (int i = 0; i < stringArray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct  coursecatId from coursemater where active=1 and coursecatId="+stringArray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getnumber=rs.getString("coursecatId");
					concatnumber=concatnumber.concat(",").concat(getnumber);
					System.out.println("concatnumber"+concatnumber);
					coursecatid=concatnumber.substring(1, concatnumber.length());
					System.out.println("coursecatid"+coursecatid);
					
				}

	      }
	      String[] stringArrays = coursecatid.split(",");
	      String[] companyarraysplit = companyid.split(",");

	      for (int i = 0; i < companyarraysplit.length||i < stringArrays.length; i++) { 


	    	  sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursecategorymaster.coursecatname ,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where (coursemater.active=1)  and(coursemater.CompanyId="+companyarraysplit[i]+")and(coursemater.coursecatId="+stringArrays[i]+")  group by coursemater.courseid";
System.out.println("sqllllllllllllllllllllllllllbothhhhhhhhhhhhhhhhhhhh"+sql);
	pstmt = con.prepareStatement(sql);
	rs = pstmt.executeQuery();

	while (rs.next()) {
		System.out.println("third condition");
		roleForm = new DynamicDataForm();
		roleForm.setCoursenamelatest(rs
				.getString("coursemater.CourseName"));
		System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
		roleForm.setFees(rs.getDouble("coursemater.fees"));
		roleForm.setTestingstring(testcondition);
		roleForm.setCoursesid(rs.getInt("CourseId"));
		roleForm.setCatid(rs.getInt("coursecatId"));
		roleForm.setCompname(rs.getString("companymaster.companyname"));
		roleForm.setCoursecatname(rs.getString("coursecategorymaster.coursecatname"));
		roleForm.setCourselatestlogo(rs
				.getString("coursemater.imagename"));
		roleForm.setCityname(rs.getString("citymaster.city"));
		String rating = rs.getString("rate");
		if (rating.equals("0.0000")) {
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("1.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("2.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-2.png");
		}
		if (rating.equals("3.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-3.png");
		}
		if (rating.equals("4.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-4.png");
		}
		if (rating.equals("5.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-5.png");
		}

		System.out.println("vani " + rating);
		list.add(roleForm);
	}
	
	}

	}
    else if(!categorynames.equalsIgnoreCase("")&&query1.equalsIgnoreCase("")&&!institutenames.equalsIgnoreCase("")&&localitynames.equalsIgnoreCase("")&&!cities.equalsIgnoreCase("")&&coursenames.equalsIgnoreCase(""))
	{
    	testcondition="Category and institute and city";

    	System.out.println("category institute and city");
	      String[] institutearray = institutenames.split(",");

	      for (int i = 0; i < institutearray.length; i++) { 
	    	  
	    	  System.out.println("stringArray.length"+institutearray.length);
	    	  sql="select distinct CompanyId from coursemater where Active=1 and CompanyId="+institutearray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getnumber=rs.getString("CompanyId");
					concatnumber=concatnumber.concat(",").concat(getnumber);
					System.out.println("concatnumber"+concatnumber);
					companyid=concatnumber.substring(1, concatnumber.length());
					System.out.println("companyid"+companyid);
					
				}

	      }
	      System.out.println("other options");
	      String[] cityarray = cities.split(",");

	      for (int i = 0; i < cityarray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct cityid from coursemater where active=1 and areaid="+cityarray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getnumber=rs.getString("cityid");
					concatnumber=concatnumber.concat(",").concat(getnumber);
					System.out.println("concatnumber"+concatnumber);
					cityid=concatnumber.substring(1, concatnumber.length());
					System.out.println("cityies"+cityid);
					
				}

	      }
	      String[] stringArray = categorynames.split(",");

	      for (int i = 0; i < stringArray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct  coursecatId from coursemater where active=1 and coursecatId="+stringArray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getnumber=rs.getString("coursecatId");
					concatnumber=concatnumber.concat(",").concat(getnumber);
					System.out.println("concatnumber"+concatnumber);
					coursecatid=concatnumber.substring(1, concatnumber.length());
					System.out.println("coursecatid"+coursecatid);
					
				}

	      }
	      String[] stringArrays = coursecatid.split(",");
	      String[] companyarraysplit = companyid.split(",");
       String[] citysplit = cityid.split(",");

	      for (int i = 0; i < companyarraysplit.length||i < citysplit.length||i<stringArrays.length ; i++) { 


	    	  sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursecategorymaster.coursecatname,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where (coursemater.active=1)  and(coursemater.CompanyId="+companyarraysplit[i]+")and(coursemater.cityid="+citysplit[i]+")and(coursemater.coursecatId="+stringArrays[i]+")  group by coursemater.courseid";
System.out.println("sqllllllllllllllllllllllllllbothhhhhhhhhhhhhhhhhhhh"+sql);
	pstmt = con.prepareStatement(sql);
	rs = pstmt.executeQuery();

	while (rs.next()) {

		System.out.println("third condition");
		roleForm = new DynamicDataForm();
		roleForm.setCoursenamelatest(rs
				.getString("coursemater.CourseName"));
		roleForm.setTestingstring(testcondition);
		System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
		roleForm.setFees(rs.getDouble("coursemater.fees"));
		roleForm.setCoursesid(rs.getInt("CourseId"));
		roleForm.setCoursecatname(rs.getString("coursecategorymaster.coursecatname"));
		roleForm.setCatid(rs.getInt("coursecatId"));
		roleForm.setCompname(rs.getString("companymaster.companyname"));
		roleForm.setCourselatestlogo(rs
				.getString("coursemater.imagename"));
		roleForm.setCityname(rs.getString("citymaster.city"));
		String rating = rs.getString("rate");
		if (rating.equals("0.0000")) {
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("1.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("2.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-2.png");
		}
		if (rating.equals("3.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-3.png");
		}
		if (rating.equals("4.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-4.png");
		}
		if (rating.equals("5.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-5.png");
		}

		System.out.println("vani " + rating);
		list.add(roleForm);
	}
	
	}

	}
    else if(!categorynames.equalsIgnoreCase("")&&query1.equalsIgnoreCase("")&&!institutenames.equalsIgnoreCase("")&&!localitynames.equalsIgnoreCase("")&&!cities.equalsIgnoreCase("")&&coursenames.equalsIgnoreCase(""))
	{
    	System.out.println("inside action institute and locality and city");
    	testcondition="institute locality and city and category";
	      String[] institutearray = institutenames.split(",");

	      for (int i = 0; i < institutearray.length; i++) { 
	    	  
	    	  System.out.println("stringArray.length"+institutearray.length);
	    	  sql="select distinct CompanyId from coursemater where Active=1 and CompanyId="+institutearray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getnumber=rs.getString("CompanyId");
					concatnumber=concatnumber.concat(",").concat(getnumber);
					System.out.println("concatnumber"+concatnumber);
					companyid=concatnumber.substring(1, concatnumber.length());
					System.out.println("companyid"+companyid);
					
				}

	      }
	      System.out.println("other options");
	      String[] cityarray = cities.split(",");

	      for (int i = 0; i < cityarray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct cityid from coursemater where active=1 and areaid="+cityarray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getnumber=rs.getString("cityid");
					concatnumber=concatnumber.concat(",").concat(getnumber);
					System.out.println("concatnumber"+concatnumber);
					cityid=concatnumber.substring(1, concatnumber.length());
					System.out.println("cityies"+cityid);
					
				}

	      }
	      String[] stringArray = categorynames.split(",");

	      for (int i = 0; i < stringArray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct  coursecatId from coursemater where active=1 and coursecatId="+stringArray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getnumber=rs.getString("coursecatId");
					concatnumber=concatnumber.concat(",").concat(getnumber);
					System.out.println("concatnumber"+concatnumber);
					coursecatid=concatnumber.substring(1, concatnumber.length());
					System.out.println("coursecatid"+coursecatid);
					
				}

	      }
	      
	      String[] localityarray = localitynames.split(",");

	      for (int i = 0; i < localityarray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct areaid from coursemater where Active=1 and areaid="+localityarray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getnumber=rs.getString("areaid");
					concatnumber=concatnumber.concat(",").concat(getnumber);
					System.out.println("concatnumber"+concatnumber);
					areaid=concatnumber.substring(1, concatnumber.length());
					System.out.println("areaid"+areaid);
					
				}

	      }

	      String[] companyarraysplit = companyid.split(",");
	      String[] localitysplit = areaid.split(",");
       String[] citysplit = cityid.split(",");

	      String[] stringArrays = coursecatid.split(",");
	     
	      for (int i = 0; i < companyarraysplit.length||i < citysplit.length||i<stringArrays.length||i<localitysplit.length ; i++) { 


	    	  sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursecategorymaster.coursecatname,areamaster.AREANAME,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+ " LEFT JOIN areamaster ON "
					+ " coursemater.areaid=areamaster.AREAID "
				
					+
					

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where (coursemater.active=1)  and(coursemater.CompanyId="+companyarraysplit[i]+")and(coursemater.cityid="+citysplit[i]+")and(coursemater.coursecatId="+stringArrays[i]+")and(coursemater.areaid="+localitysplit[i]+")  group by coursemater.courseid";
System.out.println("sqllllllllllllllllllllllllllbothhhhhhhhhhhhhhhhhhhh"+sql);
	pstmt = con.prepareStatement(sql);
	rs = pstmt.executeQuery();

	while (rs.next()) {
		System.out.println("third condition");
		roleForm = new DynamicDataForm();
		roleForm.setCoursenamelatest(rs
				.getString("coursemater.CourseName"));
		System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
		roleForm.setFees(rs.getDouble("coursemater.fees"));
		roleForm.setTestingstring(testcondition);
		roleForm.setCoursesid(rs.getInt("CourseId"));
		roleForm.setCoursecatname(rs.getString("coursecategorymaster.coursecatname"));
		roleForm.setAreaname(rs.getString("areamaster.AREANAME"));
		roleForm.setCatid(rs.getInt("coursecatId"));
		roleForm.setCompname(rs.getString("companymaster.companyname"));
		roleForm.setCourselatestlogo(rs
				.getString("coursemater.imagename"));
		roleForm.setCityname(rs.getString("citymaster.city"));
		String rating = rs.getString("rate");
		if (rating.equals("0.0000")) {
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("1.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("2.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-2.png");
		}
		if (rating.equals("3.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-3.png");
		}
		if (rating.equals("4.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-4.png");
		}
		if (rating.equals("5.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-5.png");
		}

		System.out.println("vani " + rating);
		list.add(roleForm);
	}
	
	}

	}
	
    else if(!categorynames.equalsIgnoreCase("")&&query1.equalsIgnoreCase("")&&institutenames.equalsIgnoreCase("")&&!localitynames.equalsIgnoreCase("")&&cities.equalsIgnoreCase("")&&coursenames.equalsIgnoreCase(""))
	{
    		      String[] stringArray = categorynames.split(",");

	      for (int i = 0; i < stringArray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct  coursecatId from coursemater where active=1 and coursecatId="+stringArray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getnumber=rs.getString("coursecatId");
					concatnumber=concatnumber.concat(",").concat(getnumber);
					System.out.println("concatnumber"+concatnumber);
					coursecatid=concatnumber.substring(1, concatnumber.length());
					System.out.println("coursecatid"+coursecatid);
					
				}

	      }
	      
	      String[] localityarray = localitynames.split(",");

	      for (int i = 0; i < localityarray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct areaid from coursemater where Active=1 and areaid="+localityarray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getnumber=rs.getString("areaid");
					concatnumber=concatnumber.concat(",").concat(getnumber);
					System.out.println("concatnumber"+concatnumber);
					areaid=concatnumber.substring(1, concatnumber.length());
					System.out.println("areaid"+areaid);
					
				}

	      }

	      String[] localitysplit = areaid.split(",");

	      String[] stringArrays = coursecatid.split(",");
	     
	      for (int i = 0; i < localitysplit.length||i < stringArrays.length; i++) { 


	    	  sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where (coursemater.active=1)  and(coursemater.areaid="+localitysplit[i]+")and(coursemater.coursecatId="+stringArrays[i]+")  group by coursemater.courseid";
System.out.println("sqllllllllllllllllllllllllllbothhhhhhhhhhhhhhhhhhhh"+sql);
	pstmt = con.prepareStatement(sql);
	rs = pstmt.executeQuery();

	while (rs.next()) {
		System.out.println("third condition");
		roleForm = new DynamicDataForm();
		roleForm.setCoursenamelatest(rs
				.getString("coursemater.CourseName"));
		System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
		roleForm.setFees(rs.getDouble("coursemater.fees"));
		roleForm.setCoursesid(rs.getInt("CourseId"));
		roleForm.setCatid(rs.getInt("coursecatId"));
		roleForm.setCompname(rs.getString("companymaster.companyname"));
		roleForm.setCourselatestlogo(rs
				.getString("coursemater.imagename"));
		roleForm.setCityname(rs.getString("citymaster.city"));
		String rating = rs.getString("rate");
		if (rating.equals("0.0000")) {
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("1.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("2.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-2.png");
		}
		if (rating.equals("3.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-3.png");
		}
		if (rating.equals("4.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-4.png");
		}
		if (rating.equals("5.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-5.png");
		}

		System.out.println("vani " + rating);
		list.add(roleForm);
	}
	
	}

	}
    else if(!categorynames.equalsIgnoreCase("")&&query1.equalsIgnoreCase("")&&institutenames.equalsIgnoreCase("")&&localitynames.equalsIgnoreCase("")&&!cities.equalsIgnoreCase("")&&coursenames.equalsIgnoreCase(""))
	{
    		      String[] stringArray = categorynames.split(",");

	      for (int i = 0; i < stringArray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct  coursecatId from coursemater where active=1 and coursecatId="+stringArray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getnumber=rs.getString("coursecatId");
					concatnumber=concatnumber.concat(",").concat(getnumber);
					System.out.println("concatnumber"+concatnumber);
					coursecatid=concatnumber.substring(1, concatnumber.length());
					System.out.println("coursecatid"+coursecatid);
					
				}

	      }
	      
	      String[] cityarray = cities.split(",");

	      for (int i = 0; i < cityarray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct cityid from coursemater where active=1 and areaid="+cityarray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getnumber=rs.getString("cityid");
					concatnumber=concatnumber.concat(",").concat(getnumber);
					System.out.println("concatnumber"+concatnumber);
					cityid=concatnumber.substring(1, concatnumber.length());
					System.out.println("cityies"+cityid);
					
				}

	      }
	       String[] citysplit = cityid.split(",");

	      String[] stringArrays = coursecatid.split(",");
	     
	      for (int i = 0; i < citysplit.length||i < stringArrays.length; i++) { 


	    	  sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where (coursemater.active=1)  and(coursemater.cityid="+citysplit[i]+")and(coursemater.coursecatId="+stringArrays[i]+")  group by coursemater.courseid";
System.out.println("sqllllllllllllllllllllllllllbothhhhhhhhhhhhhhhhhhhh"+sql);
	pstmt = con.prepareStatement(sql);
	rs = pstmt.executeQuery();

	while (rs.next()) {
		System.out.println("third condition");
		roleForm = new DynamicDataForm();
		roleForm.setCoursenamelatest(rs
				.getString("coursemater.CourseName"));
		System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
		roleForm.setFees(rs.getDouble("coursemater.fees"));
		roleForm.setCoursesid(rs.getInt("CourseId"));
		roleForm.setCatid(rs.getInt("coursecatId"));
		roleForm.setCompname(rs.getString("companymaster.companyname"));
		roleForm.setCourselatestlogo(rs
				.getString("coursemater.imagename"));
		roleForm.setCityname(rs.getString("citymaster.city"));
		String rating = rs.getString("rate");
		if (rating.equals("0.0000")) {
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("1.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("2.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-2.png");
		}
		if (rating.equals("3.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-3.png");
		}
		if (rating.equals("4.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-4.png");
		}
		if (rating.equals("5.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-5.png");
		}

		System.out.println("vani " + rating);
		list.add(roleForm);
	}
	
	}

	}

    else if(!categorynames.equalsIgnoreCase("")&&query1.equalsIgnoreCase("")&&institutenames.equalsIgnoreCase("")&&localitynames.equalsIgnoreCase("")&&!cities.equalsIgnoreCase("")&&coursenames.equalsIgnoreCase(""))
	{
    		      String[] stringArray = categorynames.split(",");

	      for (int i = 0; i < stringArray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct  coursecatId from coursemater where active=1 and coursecatId="+stringArray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getnumber=rs.getString("coursecatId");
					concatnumber=concatnumber.concat(",").concat(getnumber);
					System.out.println("concatnumber"+concatnumber);
					coursecatid=concatnumber.substring(1, concatnumber.length());
					System.out.println("coursecatid"+coursecatid);
					
				}

	      }
	      
	      String[] cityarray = cities.split(",");

	      for (int i = 0; i < cityarray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct cityid from coursemater where active=1 and cityid="+cityarray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getnumber=rs.getString("cityid");
					concatnumber=concatnumber.concat(",").concat(getnumber);
					System.out.println("concatnumber"+concatnumber);
					cityid=concatnumber.substring(1, concatnumber.length());
					System.out.println("cityies"+cityid);
					
				}

	      }
	      String[] localityarray = localitynames.split(",");

	      for (int i = 0; i < localityarray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct areaid from coursemater where Active=1 and areaid="+localityarray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getnumber=rs.getString("areaid");
					concatnumber=concatnumber.concat(",").concat(getnumber);
					System.out.println("concatnumber"+concatnumber);
					areaid=concatnumber.substring(1, concatnumber.length());
					System.out.println("areaid"+areaid);
					
				}

	      }

	      String[] localitysplit = areaid.split(",");


	       String[] citysplit = cityid.split(",");

	      String[] stringArrays = coursecatid.split(",");
	     
	      for (int i = 0; i < citysplit.length||i < stringArrays.length||i<localitysplit.length; i++) { 


	    	  sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where (coursemater.active=1)  and(coursemater.cityid="+citysplit[i]+")and(coursemater.coursecatId="+stringArrays[i]+")and(coursemater.areaid="+localitysplit[i]+")  group by coursemater.courseid";
System.out.println("sqllllllllllllllllllllllllllbothhhhhhhhhhhhhhhhhhhh"+sql);
	pstmt = con.prepareStatement(sql);
	rs = pstmt.executeQuery();

	while (rs.next()) {
		System.out.println("third condition");
		roleForm = new DynamicDataForm();
		roleForm.setCoursenamelatest(rs
				.getString("coursemater.CourseName"));
		System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
		roleForm.setFees(rs.getDouble("coursemater.fees"));
		roleForm.setCoursesid(rs.getInt("CourseId"));
		roleForm.setCatid(rs.getInt("coursecatId"));
		roleForm.setCompname(rs.getString("companymaster.companyname"));
		roleForm.setCourselatestlogo(rs
				.getString("coursemater.imagename"));
		roleForm.setCityname(rs.getString("citymaster.city"));
		String rating = rs.getString("rate");
		if (rating.equals("0.0000")) {
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("1.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("2.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-2.png");
		}
		if (rating.equals("3.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-3.png");
		}
		if (rating.equals("4.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-4.png");
		}
		if (rating.equals("5.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-5.png");
		}

		System.out.println("vani " + rating);
		list.add(roleForm);
	}
	
	}

	}
    else if(categorynames.equalsIgnoreCase("")&&query1.equalsIgnoreCase("")&&!institutenames.equalsIgnoreCase("")&&localitynames.equalsIgnoreCase("")&&cities.equalsIgnoreCase("")&&!coursenames.equalsIgnoreCase(""))
	{
    	testcondition="institute and course";

        String courseid="";
    	String getcourseid="";
    	String getconcatcourse="";
    	String companyids="";
    	String getcompanyids="";
    	String concatcompanyids="";
   
    	System.out.println("institute and coursename");
    		      String[] stringArray = coursenames.split(",");

	      for (int i = 0; i < stringArray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct  CourseId from coursemater where active=1 and CourseId="+stringArray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getconcatcourse=rs.getString("CourseId");
					getcourseid=getcourseid.concat(",").concat(getconcatcourse);
					System.out.println("getcourseid"+getcourseid);
					getconcatcourse=getcourseid.substring(1, getcourseid.length());
					System.out.println("coursecatid"+coursecatid);
					
				}

	      }
	      
	      String[] institutearray = institutenames.split(",");

	      for (int i = 0; i < institutearray.length; i++) { 
	    	  
	    	  System.out.println("stringArray.length"+institutearray.length);
	    	  sql="select distinct CompanyId from coursemater where Active=1 and CompanyId="+institutearray[i]+" ";
	    	  System.out.println("sqllllllllllllllllllllllllllllllll"+sql);
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getcompanyids=rs.getString("CompanyId");
					companyids=companyids.concat(",").concat(getcompanyids);
					System.out.println("concatnumber"+companyids);
					concatcompanyids=companyids.substring(1, companyids.length());
					System.out.println("companyid"+companyid);
					
				}

	      }
	      String[] companyarraysplit = concatcompanyids.split(",");

	      String[] stringArrays = getconcatcourse.split(",");
	     
	      for (int i = 0; i < companyarraysplit.length||i < stringArrays.length; i++) { 


	    	  sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where (coursemater.active=1)  and(coursemater.CompanyId="+companyarraysplit[i]+")and(coursemater.CourseId="+stringArrays[i]+") group by coursemater.courseid";
System.out.println("sqllllllllllllllllllllllllllbothhhhhhhhhhhhhhhhhhhh"+sql);
	pstmt = con.prepareStatement(sql);
	rs = pstmt.executeQuery();

	while (rs.next()) {
		System.out.println("third condition");
		roleForm = new DynamicDataForm();
		roleForm.setCoursenamelatest(rs
				.getString("coursemater.CourseName"));
		System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
		roleForm.setTestingstring(testcondition);
		roleForm.setFees(rs.getDouble("coursemater.fees"));
		roleForm.setCoursesid(rs.getInt("CourseId"));
		roleForm.setCatid(rs.getInt("coursecatId"));
		roleForm.setCompname(rs.getString("companymaster.companyname"));
		roleForm.setCourselatestlogo(rs
				.getString("coursemater.imagename"));
		roleForm.setCityname(rs.getString("citymaster.city"));
		String rating = rs.getString("rate");
		if (rating.equals("0.0000")) {
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("1.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("2.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-2.png");
		}
		if (rating.equals("3.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-3.png");
		}
		if (rating.equals("4.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-4.png");
		}
		if (rating.equals("5.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-5.png");
		}

		System.out.println("vani " + rating);
		list.add(roleForm);
	}
	
	}

	}
    else if(categorynames.equalsIgnoreCase("")&&query1.equalsIgnoreCase("")&&institutenames.equalsIgnoreCase("")&&localitynames.equalsIgnoreCase("")&&!cities.equalsIgnoreCase("")&&!coursenames.equalsIgnoreCase(""))
	{
    	testcondition="Courses and city";

    	String courseid="";
    	String getcourseid="";
    	String getconcatcourse="";
    	String cityids="";
    	String getcityid="";
    	String concatcityid="";
    	System.out.println("institute and coursename");
    	String[] cityarray = cities.split(",");

	      for (int i = 0; i < cityarray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct cityid from coursemater where active=1 and cityid="+cityarray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					cityids=rs.getString("cityid");
					concatcityid=concatcityid.concat(",").concat(cityids);
					System.out.println("concatnumber"+concatcityid);
					getcityid=concatcityid.substring(1, concatcityid.length());
					System.out.println("cityies"+getcityid);
					
				}

	      }
	      
	      String[] stringArray = coursenames.split(",");

	      for (int i = 0; i < stringArray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct  CourseId from coursemater where active=1 and CourseId="+stringArray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getnumber=rs.getString("CourseId");
					concatnumber=concatnumber.concat(",").concat(getnumber);
					System.out.println("concatnumber"+concatnumber);
					coursecatid=concatnumber.substring(1, concatnumber.length());
					System.out.println("coursecatid"+coursecatid);
					
				}

	      }
	      String[] companyarraysplit = getcityid.split(",");

	      String[] stringArrays = coursecatid.split(",");
	     
	      for (int i = 0; i < companyarraysplit.length||i < stringArrays.length; i++) { 


	    	  sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where (coursemater.active=1)  and(coursemater.cityid="+companyarraysplit[i]+")and(coursemater.CourseId="+stringArrays[i]+") group by coursemater.courseid";
System.out.println("sqllllllllllllllllllllllllllbothhhhhhhhhhhhhhhhhhhh"+sql);
	pstmt = con.prepareStatement(sql);
	rs = pstmt.executeQuery();

	while (rs.next()) {
		System.out.println("third condition");
		roleForm = new DynamicDataForm();
		roleForm.setCoursenamelatest(rs
				.getString("coursemater.CourseName"));
		System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
		roleForm.setFees(rs.getDouble("coursemater.fees"));
		roleForm.setCoursesid(rs.getInt("CourseId"));
		roleForm.setTestingstring(testcondition);
		roleForm.setCatid(rs.getInt("coursecatId"));
		roleForm.setCompname(rs.getString("companymaster.companyname"));
		roleForm.setCourselatestlogo(rs
				.getString("coursemater.imagename"));
		roleForm.setCityname(rs.getString("citymaster.city"));
		String rating = rs.getString("rate");
		if (rating.equals("0.0000")) {
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("1.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("2.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-2.png");
		}
		if (rating.equals("3.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-3.png");
		}
		if (rating.equals("4.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-4.png");
		}
		if (rating.equals("5.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-5.png");
		}

		System.out.println("vani " + rating);
		list.add(roleForm);
	}
	
	}
	     
	}
    else if(categorynames.equalsIgnoreCase("")&&query1.equalsIgnoreCase("")&&institutenames.equalsIgnoreCase("")&&!localitynames.equalsIgnoreCase("")&&!cities.equalsIgnoreCase("")&&!coursenames.equalsIgnoreCase(""))
	{
    	testcondition="course and city and area";

    	String courseid="";
    	String getcourseid="";
    	String getconcatcourse="";
    	String cityids="";
    	String getcityid="";
    	String concatcityid="";
    	String lcalityid="";
    	String getlocalityid="";
    	String getconcatlocality="";
    	System.out.println("institute and coursename");
    	String[] cityarray = cities.split(",");

	      for (int i = 0; i < cityarray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct cityid from coursemater where active=1 and cityid="+cityarray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					cityids=rs.getString("cityid");
					concatcityid=concatcityid.concat(",").concat(cityids);
					System.out.println("concatnumber"+concatcityid);
					getcityid=concatcityid.substring(1, concatcityid.length());
					System.out.println("cityies"+getcityid);
					
				}

	      }
	      
	      String[] stringArray = coursenames.split(",");

	      for (int i = 0; i < stringArray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct  CourseId from coursemater where active=1 and CourseId="+stringArray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getnumber=rs.getString("CourseId");
					concatnumber=concatnumber.concat(",").concat(getnumber);
					System.out.println("concatnumber"+concatnumber);
					coursecatid=concatnumber.substring(1, concatnumber.length());
					System.out.println("coursecatid"+coursecatid);
					
				}

	      }
	      
	      String[] localityarray = localitynames.split(",");

	      for (int i = 0; i < localityarray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct areaid from coursemater where Active=1 and areaid="+localityarray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					lcalityid=rs.getString("areaid");
					getlocalityid=getlocalityid.concat(",").concat(lcalityid);
					System.out.println("concatnumber"+getlocalityid);
					getconcatlocality=getlocalityid.substring(1, getlocalityid.length());
					System.out.println("areaid"+areaid);
					
				}

	      }
	      String[] companyarraysplit = getcityid.split(",");

	      String[] stringArrays = coursecatid.split(",");
	      
	      String[] localitysplit = getconcatlocality.split(",");
       String[] citysplit = getcityid.split(",");

	     
	      for (int i = 0; i < companyarraysplit.length||i < citysplit.length||i<localitysplit.length ; i++) { 


	    	  sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursecategorymaster.coursecatname,areamaster.AREANAME,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+ " LEFT JOIN areamaster ON "
					+ " coursemater.areaid=areamaster.AREAID "
				
					+
					

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where (coursemater.active=1)  and(coursemater.CourseId="+stringArrays[i]+")and(coursemater.cityid="+citysplit[i]+")and(coursemater.areaid="+localitysplit[i]+")  group by coursemater.courseid";
System.out.println("sqllllllllllllllllllllllllllbothhhhhhhhhhhhhhhhhhhh"+sql);
	pstmt = con.prepareStatement(sql);
	rs = pstmt.executeQuery();

	while (rs.next()) {
		System.out.println("third condition");
		roleForm = new DynamicDataForm();
		roleForm.setCoursenamelatest(rs
				.getString("coursemater.CourseName"));
		System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
		roleForm.setFees(rs.getDouble("coursemater.fees"));
		roleForm.setTestingstring(testcondition);
		roleForm.setCoursesid(rs.getInt("CourseId"));
		roleForm.setCoursecatname(rs.getString("coursecategorymaster.coursecatname"));
		roleForm.setAreaname(rs.getString("areamaster.AREANAME"));
		roleForm.setCatid(rs.getInt("coursecatId"));
		roleForm.setCompname(rs.getString("companymaster.companyname"));
		roleForm.setCourselatestlogo(rs
				.getString("coursemater.imagename"));
		roleForm.setCityname(rs.getString("citymaster.city"));
		String rating = rs.getString("rate");
		if (rating.equals("0.0000")) {
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("1.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("2.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-2.png");
		}
		if (rating.equals("3.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-3.png");
		}
		if (rating.equals("4.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-4.png");
		}
		if (rating.equals("5.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-5.png");
		}

		System.out.println("vani " + rating);
		list.add(roleForm);
	}
	
	}

	}
	
    else if(categorynames.equalsIgnoreCase("")&&!query1.equalsIgnoreCase("")&&institutenames.equalsIgnoreCase("")&&localitynames.equalsIgnoreCase("")&&!cities.equalsIgnoreCase("")&&!coursenames.equalsIgnoreCase(""))
	{
    	testcondition="course and city and area";
    	int elearning=0;
    	int classrooms=0;

    	String courseid="";
    	String getcourseid="";
    	String getconcatcourse="";
    	String cityids="";
    	String getcityid="";
    	String concatcityid="";
    	String lcalityid="";
    	String getlocalityid="";
    	String getconcatlocality="";
    	System.out.println("institute and coursename");
    	String[] cityarray = cities.split(",");

	      for (int i = 0; i < cityarray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct cityid from coursemater where active=1 and cityid="+cityarray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					cityids=rs.getString("cityid");
					concatcityid=concatcityid.concat(",").concat(cityids);
					System.out.println("concatnumber"+concatcityid);
					getcityid=concatcityid.substring(1, concatcityid.length());
					System.out.println("cityies"+getcityid);
					
				}

	      }
	      
	      String[] stringArray = coursenames.split(",");

	      for (int i = 0; i < stringArray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct  CourseId from coursemater where active=1 and CourseId="+stringArray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getnumber=rs.getString("CourseId");
					concatnumber=concatnumber.concat(",").concat(getnumber);
					System.out.println("concatnumber"+concatnumber);
					coursecatid=concatnumber.substring(1, concatnumber.length());
					System.out.println("coursecatid"+coursecatid);
					
				}

	      }
	      
	      	      String[] companyarraysplit = getcityid.split(",");

	      String[] stringArrays = coursecatid.split(",");
	      
       String[] citysplit = getcityid.split(",");

	     
	      for (int i = 0; i < companyarraysplit.length||i < citysplit.length; i++) { 


	    	  sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.classroomtraining,coursemater.elearning,coursecategorymaster.coursecatname,areamaster.AREANAME,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+ " LEFT JOIN areamaster ON "
					+ " coursemater.areaid=areamaster.AREAID "
				
					+
					

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where (coursemater.active=1)  and(coursemater.CourseId="+stringArrays[i]+")and(coursemater.cityid="+citysplit[i]+")and "+query1+"  group by coursemater.courseid";
System.out.println("sqllllllllllllllllllllllllllbothhhhhhhhhhhhhhhhhhhh"+sql);
	pstmt = con.prepareStatement(sql);
	rs = pstmt.executeQuery();

	while (rs.next()) {
		count++;
		System.out.println("third condition");
		roleForm = new DynamicDataForm();
		roleForm.setCoursenamelatest(rs
				.getString("coursemater.CourseName"));
		System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
		roleForm.setFees(rs.getDouble("coursemater.fees"));
		roleForm.setElearning(rs.getInt("coursemater.elearning"));
		elearning=rs.getInt("coursemater.elearning");
		if(elearning==1)
		{
			testcondition="elearningmode";
			
		}
		else if(elearning==0)
		{
			testcondition="classroommode";
			
		}
		roleForm.setClassroom(rs.getInt("coursemater.elearning"));
		roleForm.setTestingstring(testcondition);
		roleForm.setCoursesid(rs.getInt("CourseId"));
		roleForm.setCoursecatname(rs.getString("coursecategorymaster.coursecatname"));
		roleForm.setAreaname(rs.getString("areamaster.AREANAME"));
		roleForm.setCatid(rs.getInt("coursecatId"));
		roleForm.setCompname(rs.getString("companymaster.companyname"));
		roleForm.setCourselatestlogo(rs
				.getString("coursemater.imagename"));
		roleForm.setCityname(rs.getString("citymaster.city"));
		String rating = rs.getString("rate");
		if (rating.equals("0.0000")) {
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("1.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("2.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-2.png");
		}
		if (rating.equals("3.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-3.png");
		}
		if (rating.equals("4.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-4.png");
		}
		if (rating.equals("5.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-5.png");
		}

		System.out.println("vani " + rating);
		list.add(roleForm);
	}
	
	
	}

	}
    else if(categorynames.equalsIgnoreCase("")&&!query1.equalsIgnoreCase("")&&institutenames.equalsIgnoreCase("")&&!localitynames.equalsIgnoreCase("")&&!cities.equalsIgnoreCase("")&&!coursenames.equalsIgnoreCase(""))
	{
    	int elearning=0;
    	int classrooms=0;
         String courseid="";
    	String getcourseid="";
    	String getconcatcourse="";
    	String cityids="";
    	String getcityid="";
    	String concatcityid="";
    	String lcalityid="";
    	String getlocalityid="";
    	String getconcatlocality="";
    	System.out.println("institute and coursename");
    	String[] cityarray = cities.split(",");

	      for (int i = 0; i < cityarray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct cityid from coursemater where active=1 and cityid="+cityarray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					cityids=rs.getString("cityid");
					concatcityid=concatcityid.concat(",").concat(cityids);
					System.out.println("concatnumber"+concatcityid);
					getcityid=concatcityid.substring(1, concatcityid.length());
					System.out.println("cityies"+getcityid);
					
				}

	      }
	      
	      String[] stringArray = coursenames.split(",");

	      for (int i = 0; i < stringArray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct  CourseId from coursemater where active=1 and CourseId="+stringArray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getnumber=rs.getString("CourseId");
					concatnumber=concatnumber.concat(",").concat(getnumber);
					System.out.println("concatnumber"+concatnumber);
					coursecatid=concatnumber.substring(1, concatnumber.length());
					System.out.println("coursecatid"+coursecatid);
					
				}

	      }
	      
	      String[] localityarray = localitynames.split(",");

	      for (int i = 0; i < localityarray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct areaid from coursemater where Active=1 and areaid="+localityarray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					lcalityid=rs.getString("areaid");
					getlocalityid=getlocalityid.concat(",").concat(lcalityid);
					System.out.println("concatnumber"+getlocalityid);
					getconcatlocality=getlocalityid.substring(1, getlocalityid.length());
					System.out.println("areaid"+areaid);
					
				}

	      }
	      String[] companyarraysplit = getcityid.split(",");

	      String[] stringArrays = coursecatid.split(",");
	      
	      String[] localitysplit = getconcatlocality.split(",");
       String[] citysplit = getcityid.split(",");

	     
	      for (int i = 0; i < companyarraysplit.length||i < citysplit.length||i<localitysplit.length ; i++) { 


	    	  sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.elearning,coursecategorymaster.coursecatname,areamaster.AREANAME,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+ " LEFT JOIN areamaster ON "
					+ " coursemater.areaid=areamaster.AREAID "
				
					+
					

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where (coursemater.active=1)  and(coursemater.CourseId="+stringArrays[i]+")and(coursemater.cityid="+citysplit[i]+")and(coursemater.areaid="+localitysplit[i]+")and "+query1+"   group by coursemater.courseid";
System.out.println("sqllllllllllllllllllllllllllbothhhhhhhhhhhhhhhhhhhh"+sql);
	pstmt = con.prepareStatement(sql);
	rs = pstmt.executeQuery();

	while (rs.next()) {
		System.out.println("third condition");
		roleForm = new DynamicDataForm();
		roleForm.setCoursenamelatest(rs
				.getString("coursemater.CourseName"));
		System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
		roleForm.setFees(rs.getDouble("coursemater.fees"));
		elearning=rs.getInt("coursemater.elearning");
		if(elearning==1)
		{
			testcondition="elearningmodeofloctioncity";
			
		}
		else if(elearning==0)
		{
			testcondition="classroommodemodeoflocality";
			
		}
		roleForm.setTestingstring(testcondition);
		roleForm.setCoursesid(rs.getInt("CourseId"));
		roleForm.setCoursecatname(rs.getString("coursecategorymaster.coursecatname"));
		roleForm.setAreaname(rs.getString("areamaster.AREANAME"));
		roleForm.setCatid(rs.getInt("coursecatId"));
		roleForm.setCompname(rs.getString("companymaster.companyname"));
		roleForm.setCourselatestlogo(rs
				.getString("coursemater.imagename"));
		roleForm.setCityname(rs.getString("citymaster.city"));
		String rating = rs.getString("rate");
		if (rating.equals("0.0000")) {
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("1.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("2.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-2.png");
		}
		if (rating.equals("3.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-3.png");
		}
		if (rating.equals("4.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-4.png");
		}
		if (rating.equals("5.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-5.png");
		}

		System.out.println("vani " + rating);
		list.add(roleForm);
	}
	
	}

	}
			
			
    else if(categorynames.equalsIgnoreCase("")&&query1.equalsIgnoreCase("")&&institutenames.equalsIgnoreCase("")&&localitynames.equalsIgnoreCase("")&&cities.equalsIgnoreCase("")&&!coursenames.equalsIgnoreCase(""))
	{
    	testcondition=" course";

        String courseid="";
    	String getcourseid="";
    	String getconcatcourse="";
    	String companyids="";
    	String getcompanyids="";
    	String concatcompanyids="";
   
    	System.out.println("institute and coursename");
    		      String[] stringArray = coursenames.split(",");

	      for (int i = 0; i < stringArray.length; i++) { 
	    	  
	    	  
	    	  sql="select distinct  CourseId from coursemater where active=1 and CourseId="+stringArray[i]+" ";
	    	  
				pstmt = con.prepareStatement(sql);

				rs = pstmt.executeQuery();
				while (rs.next()) {
					getconcatcourse=rs.getString("CourseId");
					getcourseid=getcourseid.concat(",").concat(getconcatcourse);
					System.out.println("getcourseid"+getcourseid);
					getconcatcourse=getcourseid.substring(1, getcourseid.length());
					System.out.println("coursecatid"+coursecatid);
					
				}

	      }
	      
	     

	      String[] stringArrays = getconcatcourse.split(",");
	     
	      for (int i = 0;i < stringArrays.length; i++) { 


	    	  sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where (coursemater.active=1) and(coursemater.CourseId="+stringArrays[i]+") group by coursemater.courseid";
System.out.println("sqllllllllllllllllllllllllllbothhhhhhhhhhhhhhhhhhhh"+sql);
	pstmt = con.prepareStatement(sql);
	rs = pstmt.executeQuery();

	while (rs.next()) {
		System.out.println("third condition");
		roleForm = new DynamicDataForm();
		roleForm.setCoursenamelatest(rs
				.getString("coursemater.CourseName"));
		System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
		roleForm.setTestingstring(testcondition);
		roleForm.setFees(rs.getDouble("coursemater.fees"));
		roleForm.setCoursesid(rs.getInt("CourseId"));
		roleForm.setCatid(rs.getInt("coursecatId"));
		roleForm.setCompname(rs.getString("companymaster.companyname"));
		roleForm.setCourselatestlogo(rs
				.getString("coursemater.imagename"));
		roleForm.setCityname(rs.getString("citymaster.city"));
		String rating = rs.getString("rate");
		if (rating.equals("0.0000")) {
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("1.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-1.png");
		}
		if (rating.equals("2.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-2.png");
		}
		if (rating.equals("3.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-3.png");
		}
		if (rating.equals("4.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-4.png");
		}
		if (rating.equals("5.0000")) {
			System.out.println("yes");
			roleForm.setRating("images/smallrating-5.png");
		}

		System.out.println("vani " + rating);
		list.add(roleForm);
	}
	
	}

	}
			
			
			



	}

		 catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;

}

	public JSONArray selectcoursecategory(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response) {
		System.out.println("homedaooooooooooooooooooooooooooooooooooooooooooooo");
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
       JSONObject obj=null;
       int count=0;

		
		try {
			con=dataSource.getConnection();      
			String sql="select distinct coursecatId,coursecatname from coursecategorymaster where Active=1";
             pstmt=con.prepareStatement(sql);
             rs=pstmt.executeQuery();	
			while(rs.next()){
               obj=new JSONObject(); 
				obj.put("catid", rs.getInt("coursecatId"));
				obj.put("coursecatname", rs.getString("coursecatname"));

				jArray.add(obj);

				       }
			}
		 catch (Exception e) {
			e.printStackTrace();
		} finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;
	}

	public List<DynamicDataForm> coursesearch(DynamicDataForm dynaform,
			DataSource dataSource, HttpServletRequest request) {
		System.out.println("couse"+dynaform.getCourseName());
		System.out.println("location"+dynaform.getLocality());
		System.out.println("city"+dynaform.getCity());


		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;
		int count=0;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();
			if(((!dynaform.getCourseName().equalsIgnoreCase("null"))||(!dynaform.getCourseName().equals("")))&&((dynaform.getLocality().equalsIgnoreCase("null"))||(dynaform.getLocality().equals("")))&&((dynaform.getCity().equalsIgnoreCase("null"))||(dynaform.getCity().equals(""))))
			{
				System.out.println("firstcondtion");

			sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where coursemater.active=1 and coursemater.CourseName like '%"
						+ dynaform.getCourseName() + "%' group by coursemater.courseid";
			}
			
			else if(((!dynaform.getCourseName().equalsIgnoreCase("null"))||(!dynaform.getCourseName().equals("")))&&((!dynaform.getLocality().equalsIgnoreCase("null"))||(!dynaform.getLocality().equals("")))&&((dynaform.getCity().equalsIgnoreCase("null"))||(dynaform.getCity().equals(""))))

			{
				System.out.println("second condition");
				sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater"
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ "	coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON  "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid "
					+ " LEFT JOIN areamaster on "
					+ " areamaster.CompanyId  = companymaster.CompanyId  "
					+

					" where coursemater.active=1  and coursemater.coursename like '%"
					+ dynaform.getCourseName()
					+ "%'  "
					+ " and "
					+

					" areamaster.areaname like '%"
					+ dynaform.getLocality()
					+ "%' "
					+
					" group by coursemater.courseid ";
			}
			
			else if(((!dynaform.getCourseName().equalsIgnoreCase("null"))||(!dynaform.getCourseName().equals("")))&&((dynaform.getLocality().equalsIgnoreCase("null"))||(dynaform.getLocality().equals("")))&&((!dynaform.getCity().equalsIgnoreCase("null"))||(!dynaform.getCity().equals(""))))

			{
				System.out.println("third condition");
				sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater"
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ "	coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON  "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid "
					+

					" where coursemater.active=1  and coursemater.coursename like '%"
					+ dynaform.getCourseName()
					+ "%'  "
					+ " and "
					+
                     " citymaster.city like '%"
					+ dynaform.getCity() + "%' " +
					" group by coursemater.courseid ";
			}

			else if(((!dynaform.getCourseName().equalsIgnoreCase("null"))||(!dynaform.getCourseName().equals("")))&&((!dynaform.getLocality().equalsIgnoreCase("null"))||(!dynaform.getLocality().equals("")))&&((!dynaform.getCity().equalsIgnoreCase("null"))||(!dynaform.getCity().equals(""))))

			{
				System.out.println("all condition");
				sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater"
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ "	coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON  "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid "
					
					+ " LEFT JOIN areamaster on "
					+ " areamaster.CompanyId  = companymaster.CompanyId  "
					+


					" where coursemater.active=1  and coursemater.coursename like '%"
					+ dynaform.getCourseName()
					+ "%'  "
					+ " and "
					+
                     " citymaster.city like '%"
					+ dynaform.getCity() + "%' " 
					+ " and "
					+

					" areamaster.areaname like '%"
					+ dynaform.getLocality()
					+ "%' "
					+
					
					
					" group by coursemater.courseid ";
			}




			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				count++;
				roleForm = new DynamicDataForm();
				roleForm.setCoursesid(rs.getInt("CourseId"));
				roleForm.setCatid(rs.getInt("coursecatId"));
				roleForm.setCoursenamelatest(rs
						.getString("coursemater.CourseName"));
				System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
				roleForm.setCompname(rs.getString("companymaster.companyname"));
				roleForm.setCourselatestlogo(rs
						.getString("coursemater.imagename"));
				roleForm.setFees(rs.getInt("coursemater.Fees"));
				roleForm.setCityname(rs.getString("citymaster.city"));
				String rating = rs.getString("rate");
				if (rating.equals("0.0000")) {
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("1.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("2.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-2.png");
				}
				if (rating.equals("3.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-3.png");
				}
				if (rating.equals("4.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-4.png");
				}
				if (rating.equals("5.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-5.png");
				}

				System.out.println("vani " + rating);
				list.add(roleForm);
			}
			if(count==0)
			{
				request.setAttribute("results","No results found");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;

		
		
	}

	public List<DynamicDataForm> coursesdescription(DynamicDataForm dynaform,
			DataSource dataSource, HttpServletRequest request, int courseid) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();

			sql = "select cm.*,top.*,stop.*,cont.*,comp.* from coursemater cm join topicmaster top on cm.CourseId=top.CourseId join subtopicmaster stop on top.CourseId=stop.CourseId join contentmaster cont on  stop.SubTopicID=cont.SubTopicID join companymaster comp on comp.CompanyId=cont.CompanyId  where cont.Active=1 and cont.CourseId=?  group by stop.SubTopicID" ;
			
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1,courseid);

			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setTopicname(rs.getString("TopicName"));
				roleForm.setSubtopicname(rs.getString("SubTopicName"));
				roleForm.setCoursecatname(rs.getString("CourseName"));
				roleForm.setCoursestartdate(rs.getString("coursestartingdate"));
				roleForm.setSeatsavailable(rs.getString("availableseats"));
				roleForm.setCourseimage(rs.getString("imagename"));

				roleForm.setCoursenamelatest(rs.getString("CourseName"));
				System.out.println("coursenameee"+(rs.getString("CourseName")));
				roleForm.setCompname(rs.getString("companyname"));
				roleForm.setLevel(rs.getString("level"));
				roleForm.setFees(rs.getInt("Fees"));
				roleForm.setYutube(rs.getString("YoutubeLink"));
				roleForm.setContentimage(rs.getString("contentImages"));
				roleForm.setInstructorname(rs.getString("instructorname"));
				roleForm.setInsimage(rs.getString("instructorimage"));
				roleForm.setCourseId(rs.getString("courseId"));
				System.out.println("kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk"+rs.getString("courseId"));

				roleForm.setDuration(rs.getString("CourseDuration"));
				roleForm.setDescription(rs.getString("contentText"));

				list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}

	public List<DynamicDataForm> releatedcourse(DynamicDataForm dynaform,
			DataSource dataSource, HttpServletRequest request, int courseid,
			int coursecatid) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();

			sql = "select cp.companyname,cp.logoname,cs.Fees, cs.CourseName,cs.imagename,cs.CourseId,cs.coursecatId from companymaster cp, coursemater cs where cp.CompanyId = cs.CompanyId and cs.coursecatId=? and  cs.CourseId!=?  ";
			
			System.out.println("sql query " + sql);
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1,coursecatid);
			pstmt.setInt(2,courseid);

			
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setCoursesid(rs.getInt("CourseId"));
				roleForm.setCatid(rs.getInt("coursecatId"));
				roleForm.setInstituteName(rs.getString("cp.companyname"));
				roleForm.setInstituteLogo(rs.getString("cp.logoname"));
				roleForm.setCoursesoffered(rs.getString("cs.CourseName"));
				roleForm.setCourseimage(rs.getString("cs.imagename"));
				roleForm.setFees(rs.getDouble("cs.Fees"));
				list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;

	}

	public List<DynamicDataForm> coursecategory(DynamicDataForm dynaform,
			DataSource dataSource, HttpServletRequest request, int courseid,
			int coursecatid) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();

			sql = "select ccat.*,cm.*,cm.coursecatId, count(*) as NUM  from coursecategorymaster ccat join coursemater cm on ccat.coursecatId=cm.coursecatId GROUP BY cm.coursecatId";
			pstmt = con.prepareStatement(sql);
			
			
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setCatid(rs.getInt("coursecatId"));
				roleForm.setCoursecategory(rs.getString("coursecatname"));
				roleForm.setCount(rs.getInt("NUM"));
				list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;

}

	public List<DynamicDataForm> latestcoursesdescription(DataSource dataSource, int catid) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();

			sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where coursemater.active=1 and coursemater.coursecatId=?  group by coursemater.courseid";

			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, catid);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setCoursenamelatest(rs
						.getString("coursemater.CourseName"));
				System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
				roleForm.setCompname(rs.getString("companymaster.companyname"));
				roleForm.setCoursesid(rs.getInt("CourseId"));
				roleForm.setCatid(rs.getInt("coursecatId"));
			     roleForm.setCourselatestlogo(rs
						.getString("coursemater.imagename"));
				roleForm.setFees(rs.getInt("coursemater.Fees"));
				roleForm.setCityname(rs.getString("citymaster.city"));
				String rating = rs.getString("rate");
				if (rating.equals("0.0000")) {
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("1.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("2.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-2.png");
				}
				if (rating.equals("3.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-3.png");
				}
				if (rating.equals("4.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-4.png");
				}
				if (rating.equals("5.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-5.png");
				}

				System.out.println("vani " + rating);
				list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
		
	}

	public String add(DynamicDataForm dynaform, DataSource dataSource) {
		String result = Common.FAILURE_MESSAGE;
	    Connection con = null;
	    PreparedStatement pstmt = null;
	    String sql="";
	    ResultSet rs=null;
	    
	   
	    
	    try
	    {
	    	con=dataSource.getConnection();

			sql="select * from usermaster where emailid=?";
			pstmt = con.prepareStatement(sql);
	    	pstmt.setString(1,dynaform.getEmailid());
	    	System.out.println(sql);
	    	rs= pstmt.executeQuery();
	    	if(rs.next())
	    		 result ="Email-id already exist";
	    	else{
	    	
	    		sql="insert into usermaster(FirstName,emailid,Password,RoleID,Active)values(?,?,?,?,?)";
				pstmt = con.prepareStatement(sql);
				pstmt.setString(1,dynaform.getName());
		    	pstmt.setString(2,dynaform.getEmailid());
			    pstmt.setString(3,EncryptUtil.encrypt(dynaform.getPassword()));	
			    pstmt.setInt(4, 3);
			    pstmt.setBoolean(5,true);

			    int i=pstmt.executeUpdate();
				con.commit();				
				result = Common.REC_ADDED;
				}

	}
      catch (Exception e) {
		e.printStackTrace();
	}
      finally{
    	  ConnectionUtil.closeResources(con, pstmt,rs);
      }
      return result;
}

	

	public JSONArray elearningcount(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response) {
		System.out.println("homedaooooooooooooooooooooooooooooooooooooooooooooo");
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
       JSONObject obj=null;
       int count=0;
       
		try {
			con=dataSource.getConnection();      
			String sql = "select  count(*) as NUM  from coursemater where elearning=1 and Active=1   GROUP BY elearning";
             pstmt=con.prepareStatement(sql);
             rs=pstmt.executeQuery();	
			while(rs.next()){
               obj=new JSONObject(); 
				obj.put("elearningcount", rs.getInt("NUM"));

				jArray.add(obj);

				       }
			}
		 catch (Exception e) {
			e.printStackTrace();
		} finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;
}

	public List<DynamicDataForm> pricelowtohigh(DynamicDataForm dynaform,
			DataSource dataSource, String coursenames, String countrys) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();

			System.out.println("1" + dynaform.getCourseName());
			if (dynaform.getCityid() == null
					&& dynaform.getCourseName() != null) {
				System.out.println(1);
				sql = "SELECT coursemater.CourseId,coursemater.coursecatId,coursemater.CourseName,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where coursemater.active=1   and coursemater.CourseName like '%"
						+ coursenames+ "%' group by coursemater.courseid order by coursemater.Fees asc";
			}
			System.out.println("2" + dynaform.getCityid());
			if (dynaform.getCoursename() == null
					&& dynaform.getCityid() != null) {
				System.out.println(2);
				sql = "SELECT coursemater.CourseId,coursemater.coursecatId, coursemater.CourseName,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where coursemater.active=1   and companymaster.cityid= '"
						+countrys + "' group by coursemater.courseid order by coursemater.Fees asc";

			}
			System.out.println("Third condition " + dynaform.getCityid()
					+ "hi " + dynaform.getCourseName());
			if (dynaform.getCityid() != null
					&& dynaform.getCourseName() != null) {
				System.out.println(3);
				// sql =
				// "select um.Firstname,um.profilePhoto,um.coursesoffered,cm.fees from usermaster um, coursemater cm  where coursesoffered like '%"+dynaform.getCourseName()+"%' or cityId= '"+dynaform.getCityid()+"' and RoleID = '5' ";
				sql = "SELECT coursemater.CourseId,coursemater.coursecatId, coursemater.CourseName,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where coursemater.active=1   and coursemater.CourseName like '%"
						+ coursenames
						+ "%' and companymaster.cityid="
						+ countrys + "  group by coursemater.courseid order by coursemater.Fees asc ";
			}
			System.out.println("sql query " + sql);
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setCoursesid(rs.getInt("CourseId"));
				roleForm.setCatid(rs.getInt("coursecatId"));
				roleForm.setCoursenamelatest(rs
						.getString("coursemater.CourseName"));
				System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
				roleForm.setCompname(rs.getString("companymaster.companyname"));
				roleForm.setCourselatestlogo(rs
						.getString("coursemater.imagename"));
				roleForm.setFees(rs.getInt("coursemater.Fees"));
				roleForm.setCityname(rs.getString("citymaster.city"));
				String rating = rs.getString("rate");
				if (rating.equals("0.0000")) {
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("1.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("2.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-2.png");
				}
				if (rating.equals("3.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-3.png");
				}
				if (rating.equals("4.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-4.png");
				}
				if (rating.equals("5.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-5.png");
				}



				System.out.println("vani " + rating);
				list.add(roleForm);

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}

	public List<DynamicDataForm> pricehightolow(DynamicDataForm dynaform,
			DataSource dataSource, String coursenames, String countrys) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();

			System.out.println("1" + dynaform.getCourseName());
			if (dynaform.getCityid() == null
					&& dynaform.getCourseName() != null) {
				System.out.println(1);
				sql = "SELECT coursemater.CourseId,coursemater.coursecatId,coursemater.CourseName,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where coursemater.active=1   and coursemater.CourseName like '%"
						+ coursenames+ "%' group by coursemater.courseid order by coursemater.Fees desc";
			}
			System.out.println("2" + dynaform.getCityid());
			if (dynaform.getCoursename() == null
					&& dynaform.getCityid() != null) {
				System.out.println(2);
				sql = "SELECT coursemater.CourseId,coursemater.coursecatId, coursemater.CourseName,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where coursemater.active=1   and companymaster.cityid= '"
						+countrys + "' group by coursemater.courseid order by coursemater.Fees desc";

			}
			System.out.println("Third condition " + dynaform.getCityid()
					+ "hi " + dynaform.getCourseName());
			if (dynaform.getCityid() != null
					&& dynaform.getCourseName() != null) {
				System.out.println(3);
				// sql =
				// "select um.Firstname,um.profilePhoto,um.coursesoffered,cm.fees from usermaster um, coursemater cm  where coursesoffered like '%"+dynaform.getCourseName()+"%' or cityId= '"+dynaform.getCityid()+"' and RoleID = '5' ";
				sql = "SELECT coursemater.CourseId,coursemater.coursecatId, coursemater.CourseName,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where coursemater.active=1   and coursemater.CourseName like '%"
						+ coursenames
						+ "%' and companymaster.cityid="
						+ countrys + "  group by coursemater.courseid order by coursemater.Fees desc";
			}
			System.out.println("sql query " + sql);
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setCoursesid(rs.getInt("CourseId"));
				roleForm.setCatid(rs.getInt("coursecatId"));
				roleForm.setCoursenamelatest(rs
						.getString("coursemater.CourseName"));
				System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
				roleForm.setCompname(rs.getString("companymaster.companyname"));
				roleForm.setCourselatestlogo(rs
						.getString("coursemater.imagename"));
				roleForm.setFees(rs.getInt("coursemater.Fees"));
				roleForm.setCityname(rs.getString("citymaster.city"));
				String rating = rs.getString("rate");
				if (rating.equals("0.0000")) {
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("1.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("2.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-2.png");
				}
				if (rating.equals("3.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-3.png");
				}
				if (rating.equals("4.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-4.png");
				}
				if (rating.equals("5.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-5.png");
				}



				System.out.println("vani " + rating);
				list.add(roleForm);

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}

	public List<DynamicDataForm> rating(DynamicDataForm dynaform,
			DataSource dataSource, String coursenames, String countrys) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();

			System.out.println("1" + dynaform.getCourseName());
			if (dynaform.getCityid() == null
					&& dynaform.getCourseName() != null) {
				System.out.println(1);
				sql = "SELECT coursemater.CourseId,coursemater.coursecatId,coursemater.CourseName,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where coursemater.active=1   and coursemater.CourseName like '%"
						+ coursenames+ "%' group by coursemater.courseid order by ratingtable.rating asc";
			}
			System.out.println("2" + dynaform.getCityid());
			if (dynaform.getCoursename() == null
					&& dynaform.getCityid() != null) {
				System.out.println(2);
				sql = "SELECT coursemater.CourseId,coursemater.coursecatId, coursemater.CourseName,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where coursemater.active=1   and companymaster.cityid= '"
						+countrys + "' group by coursemater.courseid order by ratingtable.rating asc";

			}
			System.out.println("Third condition " + dynaform.getCityid()
					+ "hi " + dynaform.getCourseName());
			if (dynaform.getCityid() != null
					&& dynaform.getCourseName() != null) {
				System.out.println(3);
				// sql =
				// "select um.Firstname,um.profilePhoto,um.coursesoffered,cm.fees from usermaster um, coursemater cm  where coursesoffered like '%"+dynaform.getCourseName()+"%' or cityId= '"+dynaform.getCityid()+"' and RoleID = '5' ";
				sql = "SELECT coursemater.CourseId,coursemater.coursecatId, coursemater.CourseName,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where coursemater.active=1   and coursemater.CourseName like '%"
						+ coursenames
						+ "%' and companymaster.cityid="
						+ countrys + "  group by coursemater.courseid order by ratingtable.rating asc";
			}
			System.out.println("sql query " + sql);
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setCoursesid(rs.getInt("CourseId"));
				roleForm.setCatid(rs.getInt("coursecatId"));
				roleForm.setCoursenamelatest(rs
						.getString("coursemater.CourseName"));
				System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
				roleForm.setCompname(rs.getString("companymaster.companyname"));
				roleForm.setCourselatestlogo(rs
						.getString("coursemater.imagename"));
				roleForm.setFees(rs.getInt("coursemater.Fees"));
				roleForm.setCityname(rs.getString("citymaster.city"));
				String rating = rs.getString("rate");
				if (rating.equals("0.0000")) {
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("1.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("2.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-2.png");
				}
				if (rating.equals("3.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-3.png");
				}
				if (rating.equals("4.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-4.png");
				}
				if (rating.equals("5.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-5.png");
				}



				System.out.println("vani " + rating);
				list.add(roleForm);

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}

	public JSONArray classroomcounts(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response) {
		System.out.println("homedaooooooooooooooooooooooooooooooooooooooooooooo");
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
       JSONObject obj=null;
       int count=0;

		
		try {
			con=dataSource.getConnection();      
			String sql = "select  count(*) as NUM  from coursemater where classroomtraining=1 and Active=1   GROUP BY classroomtraining";
             pstmt=con.prepareStatement(sql);
             rs=pstmt.executeQuery();	
			while(rs.next()){
               obj=new JSONObject(); 
				obj.put("classroom", rs.getInt("NUM"));

				jArray.add(obj);

				       }
			}
		 catch (Exception e) {
			e.printStackTrace();
		} finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;
	}

	public JSONArray freecounts(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response) {
		System.out.println("homedaooooooooooooooooooooooooooooooooooooooooooooo");
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
       JSONObject obj=null;
       int count=0;

		
		try {
			con=dataSource.getConnection();      
			String sql = "select  count(*) as NUM  from coursemater where free=1 and Active=1  GROUP BY free";
             pstmt=con.prepareStatement(sql);
             rs=pstmt.executeQuery();	
			while(rs.next()){
               obj=new JSONObject(); 
				obj.put("freecount", rs.getInt("NUM"));

				jArray.add(obj);

				       }
			}
		 catch (Exception e) {
			e.printStackTrace();
		} finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;
	}

	public JSONArray beginnercounts(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response) {
		System.out.println("homedaooooooooooooooooooooooooooooooooooooooooooooo");
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
       JSONObject obj=null;
       int count=0;

		
		try {
			con=dataSource.getConnection();      
			String sql = "select  count(*) as NUM  from coursemater where level='Beginner' and Active=1 GROUP BY level";
             pstmt=con.prepareStatement(sql);
             rs=pstmt.executeQuery();	
			while(rs.next()){
               obj=new JSONObject(); 
				obj.put("beginnercount", rs.getInt("NUM"));

				jArray.add(obj);

				       }
			}
		 catch (Exception e) {
			e.printStackTrace();
		} finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;
	}

	public JSONArray intermediatecounts(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response) {
		System.out.println("homedaooooooooooooooooooooooooooooooooooooooooooooo");
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
       JSONObject obj=null;
       int count=0;

		
		try {
			con=dataSource.getConnection();      
			String sql = "select  count(*) as NUM  from coursemater where level='Intermediate' and Active=1 GROUP BY level";
             pstmt=con.prepareStatement(sql);
             rs=pstmt.executeQuery();	
			while(rs.next()){
               obj=new JSONObject(); 
				obj.put("intermediatecount", rs.getInt("NUM"));

				jArray.add(obj);

				       }
			}
		 catch (Exception e) {
			e.printStackTrace();
		} finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;
	}

	public JSONArray advancedcounts(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response) {
		System.out.println("homedaooooooooooooooooooooooooooooooooooooooooooooo");
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
       JSONObject obj=null;
       int count=0;

		
		try {
			con=dataSource.getConnection();      
			String sql = "select  count(*) as NUM  from coursemater where level='Advanced' and Active=1 GROUP BY level";
             pstmt=con.prepareStatement(sql);
             rs=pstmt.executeQuery();	
			while(rs.next()){
               obj=new JSONObject(); 
				obj.put("Advanced", rs.getInt("NUM"));

				jArray.add(obj);

				       }
			}
		 catch (Exception e) {
			e.printStackTrace();
		} finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;
	}

	public JSONArray paidcounts(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		System.out.println("homedaooooooooooooooooooooooooooooooooooooooooooooo");
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
       JSONObject obj=null;
       int count=0;

		
		try {
			con=dataSource.getConnection();      
			String sql = "SELECT COUNT(Fees) as NUM  FROM coursemater where Fees!=0  and Active=1";
             pstmt=con.prepareStatement(sql);
             rs=pstmt.executeQuery();	
			while(rs.next()){
               obj=new JSONObject(); 
				obj.put("paidcount", rs.getInt("NUM"));

				jArray.add(obj);

				       }
			}
		 catch (Exception e) {
			e.printStackTrace();
		} finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;
		}

	public List<DynamicDataForm> pricehightolowsearchagain(
			DynamicDataForm dynaform, DataSource dataSource,
			HttpServletRequest request, String coursename, String localitys,
			String city) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;
		int count=0;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();
			if(((!coursename.equalsIgnoreCase("null"))||(!coursename.equals("")))&&((localitys.equalsIgnoreCase("null"))||(localitys.equals("")))&&((city.equalsIgnoreCase("null"))||(city.equals(""))))
			{
				System.out.println("firstcondtion");

			sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where coursemater.active=1 and coursemater.CourseName like '%"
						+ dynaform.getCourseName() + "%' group by coursemater.courseid order by coursemater.Fees desc";
			}
			
			else if(((!coursename.equalsIgnoreCase("null"))||(!coursename.equals("")))&&((!localitys.equalsIgnoreCase("null"))||(!localitys.equals("")))&&((city.equalsIgnoreCase("null"))||(city.equals(""))))

			{
				System.out.println("second condition");
				sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater"
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ "	coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON  "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid "
					+ " LEFT JOIN areamaster on "
					+ " areamaster.CompanyId  = companymaster.CompanyId  "
					+

					" where coursemater.active=1  and coursemater.coursename like '%"
					+ dynaform.getCourseName()
					+ "%'  "
					+ " and "
					+

					" areamaster.areaname like '%"
					+ dynaform.getLocality()
					+ "%' "
					+
					" group by coursemater.courseid order by coursemater.Fees desc ";
			}
			
			else if(((!coursename.equalsIgnoreCase("null"))||(!coursename.equals("")))&&((localitys.equalsIgnoreCase("null"))||(localitys.equals("")))&&((!city.equalsIgnoreCase("null"))||(!city.equals(""))))

			{
				System.out.println("third condition");
				sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater"
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ "	coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON  "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid "
					+

					" where coursemater.active=1  and coursemater.coursename like '%"
					+ dynaform.getCourseName()
					+ "%'  "
					+ " and "
					+
                     " citymaster.city like '%"
					+ dynaform.getCity() + "%' " +
					" group by coursemater.courseid order by coursemater.Fees desc ";
			}

			else if(((!coursename.equalsIgnoreCase("null"))||(!coursename.equals("")))&&((!localitys.equalsIgnoreCase("null"))||(!localitys.equals("")))&&((!city.equalsIgnoreCase("null"))||(!city.equals(""))))

			{
				System.out.println("all condition");
				sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater"
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ "	coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON  "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid "
					
					+ " LEFT JOIN areamaster on "
					+ " areamaster.CompanyId  = companymaster.CompanyId  "
					+


					" where coursemater.active=1  and coursemater.coursename like '%"
					+ dynaform.getCourseName()
					+ "%'  "
					+ " and "
					+
                     " citymaster.city like '%"
					+ dynaform.getCity() + "%' " 
					+ " and "
					+

					" areamaster.areaname like '%"
					+ dynaform.getLocality()
					+ "%' "
					+
					
					
					" group by coursemater.courseid order by coursemater.Fees desc ";
			}




			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				count++;
				roleForm = new DynamicDataForm();
				roleForm.setCoursesid(rs.getInt("CourseId"));
				roleForm.setCatid(rs.getInt("coursecatId"));
				roleForm.setCoursenamelatest(rs
						.getString("coursemater.CourseName"));
				System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
				roleForm.setCompname(rs.getString("companymaster.companyname"));
				roleForm.setCourselatestlogo(rs
						.getString("coursemater.imagename"));
				roleForm.setFees(rs.getInt("coursemater.Fees"));
				roleForm.setCityname(rs.getString("citymaster.city"));
				String rating = rs.getString("rate");
				if (rating.equals("0.0000")) {
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("1.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("2.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-2.png");
				}
				if (rating.equals("3.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-3.png");
				}
				if (rating.equals("4.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-4.png");
				}
				if (rating.equals("5.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-5.png");
				}

				System.out.println("vani " + rating);
				list.add(roleForm);
			}
			if(count==0)
			{
				request.setAttribute("results","No results found");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;

		

		
		
		
		
	}

	public List<DynamicDataForm> pricelowtohighsearchagain(
			DynamicDataForm dynaform, DataSource dataSource,
			HttpServletRequest request, String coursename, String localitys,
			String city) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;
		int count=0;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();
			if(((!coursename.equalsIgnoreCase("null"))||(!coursename.equals("")))&&((localitys.equalsIgnoreCase("null"))||(localitys.equals("")))&&((city.equalsIgnoreCase("null"))||(city.equals(""))))
			{
				System.out.println("firstcondtion");

			sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where coursemater.active=1 and coursemater.CourseName like '%"
						+ dynaform.getCourseName() + "%' group by coursemater.courseid order by coursemater.Fees asc";
			}
			
			else if(((!coursename.equalsIgnoreCase("null"))||(!coursename.equals("")))&&((!localitys.equalsIgnoreCase("null"))||(!localitys.equals("")))&&((city.equalsIgnoreCase("null"))||(city.equals(""))))

			{
				System.out.println("second condition");
				sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater"
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ "	coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON  "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid "
					+ " LEFT JOIN areamaster on "
					+ " areamaster.CompanyId  = companymaster.CompanyId  "
					+

					" where coursemater.active=1  and coursemater.coursename like '%"
					+ dynaform.getCourseName()
					+ "%'  "
					+ " and "
					+

					" areamaster.areaname like '%"
					+ dynaform.getLocality()
					+ "%' "
					+
					" group by coursemater.courseid order by coursemater.Fees asc ";
			}
			
			else if(((!coursename.equalsIgnoreCase("null"))||(!coursename.equals("")))&&((localitys.equalsIgnoreCase("null"))||(localitys.equals("")))&&((!city.equalsIgnoreCase("null"))||(!city.equals(""))))

			{
				System.out.println("third condition");
				sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater"
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ "	coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON  "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid "
					+

					" where coursemater.active=1  and coursemater.coursename like '%"
					+ dynaform.getCourseName()
					+ "%'  "
					+ " and "
					+
                     " citymaster.city like '%"
					+ dynaform.getCity() + "%' " +
					" group by coursemater.courseid order by coursemater.Fees asc ";
			}

			else if(((!coursename.equalsIgnoreCase("null"))||(!coursename.equals("")))&&((!localitys.equalsIgnoreCase("null"))||(!localitys.equals("")))&&((!city.equalsIgnoreCase("null"))||(!city.equals(""))))

			{
				System.out.println("all condition");
				sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater"
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ "	coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON  "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid "
					
					+ " LEFT JOIN areamaster on "
					+ " areamaster.CompanyId  = companymaster.CompanyId  "
					+


					" where coursemater.active=1  and coursemater.coursename like '%"
					+ dynaform.getCourseName()
					+ "%'  "
					+ " and "
					+
                     " citymaster.city like '%"
					+ dynaform.getCity() + "%' " 
					+ " and "
					+

					" areamaster.areaname like '%"
					+ dynaform.getLocality()
					+ "%' "
					+
					
					
					" group by coursemater.courseid order by coursemater.Fees asc ";
			}




			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				count++;
				roleForm = new DynamicDataForm();
				roleForm.setCoursesid(rs.getInt("CourseId"));
				roleForm.setCatid(rs.getInt("coursecatId"));
				roleForm.setCoursenamelatest(rs
						.getString("coursemater.CourseName"));
				System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
				roleForm.setCompname(rs.getString("companymaster.companyname"));
				roleForm.setCourselatestlogo(rs
						.getString("coursemater.imagename"));
				roleForm.setFees(rs.getInt("coursemater.Fees"));
				roleForm.setCityname(rs.getString("citymaster.city"));
				String rating = rs.getString("rate");
				if (rating.equals("0.0000")) {
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("1.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("2.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-2.png");
				}
				if (rating.equals("3.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-3.png");
				}
				if (rating.equals("4.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-4.png");
				}
				if (rating.equals("5.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-5.png");
				}

				System.out.println("vani " + rating);
				list.add(roleForm);
			}
			if(count==0)
			{
				request.setAttribute("results","No results found");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}

	public List<DynamicDataForm> ratewisesearchagain(DynamicDataForm dynaform,
			DataSource dataSource, HttpServletRequest request,
			String coursename, String localitys, String city) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;
		int count=0;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();
			if(((!coursename.equalsIgnoreCase("null"))||(!coursename.equals("")))&&((localitys.equalsIgnoreCase("null"))||(localitys.equals("")))&&((city.equalsIgnoreCase("null"))||(city.equals(""))))
			{
				System.out.println("firstcondtion");

			sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where coursemater.active=1 and coursemater.CourseName like '%"
						+ dynaform.getCourseName() + "%' group by coursemater.courseid order by ratingtable.rating asc";
			}
			
			else if(((!coursename.equalsIgnoreCase("null"))||(!coursename.equals("")))&&((!localitys.equalsIgnoreCase("null"))||(!localitys.equals("")))&&((city.equalsIgnoreCase("null"))||(city.equals(""))))

			{
				System.out.println("second condition");
				sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater"
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ "	coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON  "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid "
					+ " LEFT JOIN areamaster on "
					+ " areamaster.CompanyId  = companymaster.CompanyId  "
					+

					" where coursemater.active=1  and coursemater.coursename like '%"
					+ dynaform.getCourseName()
					+ "%'  "
					+ " and "
					+

					" areamaster.areaname like '%"
					+ dynaform.getLocality()
					+ "%' "
					+
					" group by coursemater.courseid order by ratingtable.rating asc ";
			}
			
			else if(((!coursename.equalsIgnoreCase("null"))||(!coursename.equals("")))&&((localitys.equalsIgnoreCase("null"))||(localitys.equals("")))&&((!city.equalsIgnoreCase("null"))||(!city.equals(""))))

			{
				System.out.println("third condition");
				sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater"
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ "	coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON  "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid "
					+

					" where coursemater.active=1  and coursemater.coursename like '%"
					+ dynaform.getCourseName()
					+ "%'  "
					+ " and "
					+
                     " citymaster.city like '%"
					+ dynaform.getCity() + "%' " +
					" group by coursemater.courseid order by ratingtable.rating asc ";
			}

			else if(((!coursename.equalsIgnoreCase("null"))||(!coursename.equals("")))&&((!localitys.equalsIgnoreCase("null"))||(!localitys.equals("")))&&((!city.equalsIgnoreCase("null"))||(!city.equals(""))))

			{
				System.out.println("all condition");
				sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater"
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ "	coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON  "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid "
					
					+ " LEFT JOIN areamaster on "
					+ " areamaster.CompanyId  = companymaster.CompanyId  "
					+


					" where coursemater.active=1  and coursemater.coursename like '%"
					+ dynaform.getCourseName()
					+ "%'  "
					+ " and "
					+
                     " citymaster.city like '%"
					+ dynaform.getCity() + "%' " 
					+ " and "
					+

					" areamaster.areaname like '%"
					+ dynaform.getLocality()
					+ "%' "
					+
					
					
					" group by coursemater.courseid order by ratingtable.rating asc ";
			}




			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				count++;
				roleForm = new DynamicDataForm();
				roleForm.setCoursesid(rs.getInt("CourseId"));
				roleForm.setCatid(rs.getInt("coursecatId"));
				roleForm.setCoursenamelatest(rs
						.getString("coursemater.CourseName"));
				System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
				roleForm.setCompname(rs.getString("companymaster.companyname"));
				roleForm.setCourselatestlogo(rs
						.getString("coursemater.imagename"));
				roleForm.setFees(rs.getInt("coursemater.Fees"));
				roleForm.setCityname(rs.getString("citymaster.city"));
				String rating = rs.getString("rate");
				if (rating.equals("0.0000")) {
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("1.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("2.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-2.png");
				}
				if (rating.equals("3.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-3.png");
				}
				if (rating.equals("4.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-4.png");
				}
				if (rating.equals("5.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-5.png");
				}

				System.out.println("vani " + rating);
				list.add(roleForm);
			}
			if(count==0)
			{
				request.setAttribute("results","No results found");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;

	}

	public List<DynamicDataForm> pricehightolowlatestcourses(
			DataSource dataSource) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();

			sql = "SELECT coursemater.CourseName,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where coursemater.active=1 group by coursemater.courseid order by coursemater.Fees desc";

			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setCoursenamelatest(rs
						.getString("coursemater.CourseName"));
				System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
				roleForm.setCompname(rs.getString("companymaster.companyname"));
				roleForm.setCourselatestlogo(rs
						.getString("coursemater.imagename"));
				roleForm.setFees(rs.getInt("coursemater.Fees"));
				roleForm.setCityname(rs.getString("citymaster.city"));
				String rating = rs.getString("rate");
				if (rating.equals("0.0000")) {
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("1.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("2.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-2.png");
				}
				if (rating.equals("3.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-3.png");
				}
				if (rating.equals("4.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-4.png");
				}
				if (rating.equals("5.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-5.png");
				}

				System.out.println("vani " + rating);
				list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}

	public List<DynamicDataForm> pricelowtohighlatestcourses(
			DataSource dataSource) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();

			sql = "SELECT coursemater.CourseName,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where coursemater.active=1 group by coursemater.courseid order by coursemater.Fees asc";

			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setCoursenamelatest(rs
						.getString("coursemater.CourseName"));
				System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
				roleForm.setCompname(rs.getString("companymaster.companyname"));
				roleForm.setCourselatestlogo(rs
						.getString("coursemater.imagename"));
				roleForm.setFees(rs.getInt("coursemater.Fees"));
				roleForm.setCityname(rs.getString("citymaster.city"));
				String rating = rs.getString("rate");
				if (rating.equals("0.0000")) {
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("1.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("2.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-2.png");
				}
				if (rating.equals("3.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-3.png");
				}
				if (rating.equals("4.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-4.png");
				}
				if (rating.equals("5.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-5.png");
				}

				System.out.println("vani " + rating);
				list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}

	public List<DynamicDataForm> ratewiselatestcourses(DataSource dataSource) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();

			sql = "SELECT coursemater.CourseName,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where coursemater.active=1 group by coursemater.courseid order by ratingtable.rating asc ";

			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setCoursenamelatest(rs
						.getString("coursemater.CourseName"));
				System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
				roleForm.setCompname(rs.getString("companymaster.companyname"));
				roleForm.setCourselatestlogo(rs
						.getString("coursemater.imagename"));
				roleForm.setFees(rs.getInt("coursemater.Fees"));
				roleForm.setCityname(rs.getString("citymaster.city"));
				String rating = rs.getString("rate");
				if (rating.equals("0.0000")) {
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("1.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("2.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-2.png");
				}
				if (rating.equals("3.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-3.png");
				}
				if (rating.equals("4.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-4.png");
				}
				if (rating.equals("5.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-5.png");
				}

				System.out.println("vani " + rating);
				list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;

	}

	public List<DynamicDataForm> pricelowtohighselectingcourses(
			DataSource dataSource, String eleaning, String classroom,
			String paid, String free, String beginner, String intermediate,
			String advanced, String lessfive, String graterfive,
			String gratertwo, String greaterfivethousand, String categorynames) {
		
		System.out.println("testing elearning courses"+eleaning);
		System.out.println("testing classroom courses"+classroom);
		System.out.println("testing classroom beginners"+classroom);
		String greaterfivehun=graterfive.substring(0, 3);
		System.out.println("graterfive"+greaterfivehun);
		String lesstwot=graterfive.substring(3, 7);
		System.out.println("lesstwot"+lesstwot);
		String greatertwothousnd=gratertwo.substring(0, 4);
		String lesseqfive=gratertwo.substring(4, 8);
		System.out.println("beginnerssssssssss"+beginner);

		String where="";

		String query1="";
		String query2="";

		if((eleaning.equals("1")||classroom.equals("1"))&&!(paid.equals("0")|| free.equals("1"))&&!(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&!(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000")))))){
			query1="coursemater.classroomtraining="+classroom+" or coursemater.elearning="+eleaning+"";
			}
		else if(!(eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&!(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&!(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000")))))){

			
				query1="coursemater.Fees>"+paid+" or coursemater.free="+free+"";
				
				
			}
		else if((eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&!(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&!(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000")))))){
			
				query1="(coursemater.classroomtraining="+classroom+" or coursemater.elearning="+eleaning+") and (coursemater.Fees>"+paid+" or coursemater.free="+free+") ";

			}
				
		else if(!(eleaning.equals("1")||classroom.equals("1"))&&!(paid.equals("0")|| free.equals("1"))&&(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&!(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000")))))){
			
				query1="coursemater.level='"+beginner+"' or coursemater.level='"+intermediate+"' or coursemater.level='"+advanced+"'";
			}
		else if((eleaning.equals("1")||classroom.equals("1"))&&!(paid.equals("0")|| free.equals("1"))&&(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))){
			
				query1="(coursemater.classroomtraining="+classroom+" or coursemater.elearning="+eleaning+") and (coursemater.level='"+beginner+"' or coursemater.level='"+intermediate+"' or coursemater.level='"+advanced+"') ";
			}
		else if(!(eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&!(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000")))))){
			
				query1="(coursemater.Fees>"+paid+" or coursemater.free="+free+") and (coursemater.level='"+beginner+"' or coursemater.level='"+intermediate+"' or coursemater.level='"+advanced+"')";
			}

		else if((eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&!(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
			{
				query1="(coursemater.classroomtraining="+classroom+" or coursemater.elearning="+eleaning+") and (coursemater.Fees>"+paid+" or coursemater.free="+free+") and (coursemater.level='"+beginner+"' or coursemater.level='"+intermediate+"' or coursemater.level='"+advanced+"')";
			}
		else if(!(eleaning.equals("1")||classroom.equals("1"))&&!(paid.equals("0")|| free.equals("1"))&&!(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
		{
			query1="coursemater.Fees<="+lessfive+" or coursemater.Fees>"+greaterfivehun+" and coursemater.Fees<="+lesstwot+" or coursemater.Fees>"+greatertwothousnd+" and coursemater.Fees<="+lesseqfive+" or coursemater.Fees>"+greaterfivethousand+"";
			
		}
		else if((eleaning.equals("1")||classroom.equals("1"))&&!(paid.equals("0")|| free.equals("1"))&&!(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
		{
			query1="(coursemater.classroomtraining="+classroom+" or coursemater.elearning="+eleaning+")and(coursemater.Fees<="+lessfive+" or coursemater.Fees>"+greaterfivehun+" and coursemater.Fees<="+lesstwot+" or coursemater.Fees>"+greatertwothousnd+" and coursemater.Fees<="+lesseqfive+" or coursemater.Fees>"+greaterfivethousand+")";
			
		}
		else if(!(eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&!(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
		{
			query1="(coursemater.Fees>"+paid+" or coursemater.free="+free+")and(coursemater.Fees<="+lessfive+" or coursemater.Fees>"+greaterfivehun+" and coursemater.Fees<="+lesstwot+" or coursemater.Fees>"+greatertwothousnd+" and coursemater.Fees<="+lesseqfive+" or coursemater.Fees>"+greaterfivethousand+")";
			
		}

		else if(!(eleaning.equals("1")||classroom.equals("1"))&&!(paid.equals("0")|| free.equals("1"))&&(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
		{
			query1="(coursemater.level='"+beginner+"' or coursemater.level='"+intermediate+"' or coursemater.level='"+advanced+"')and(coursemater.Fees<="+lessfive+" or coursemater.Fees>"+greaterfivehun+" and coursemater.Fees<="+lesstwot+" or coursemater.Fees>"+greatertwothousnd+" and coursemater.Fees<="+lesseqfive+" or coursemater.Fees>"+greaterfivethousand+")";
			
		}
		else if((eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&!(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
		{
			query1="(coursemater.classroomtraining="+classroom+" or coursemater.elearning="+eleaning+") and(coursemater.Fees>"+paid+" or coursemater.free="+free+")and(coursemater.Fees<="+lessfive+" or coursemater.Fees>"+greaterfivehun+" and coursemater.Fees<="+lesstwot+" or coursemater.Fees>"+greatertwothousnd+" and coursemater.Fees<="+lesseqfive+" or coursemater.Fees>"+greaterfivethousand+")";
			
		}
		else if((eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
		{
			query1="(coursemater.classroomtraining="+classroom+" or coursemater.elearning="+eleaning+") and(coursemater.Fees>"+paid+" or coursemater.free="+free+")and(coursemater.level='"+beginner+"' or coursemater.level='"+intermediate+"' or coursemater.level='"+advanced+"')and(coursemater.Fees<="+lessfive+" or coursemater.Fees>"+greaterfivehun+" and coursemater.Fees<="+lesstwot+" or coursemater.Fees>"+greatertwothousnd+" and coursemater.Fees<="+lesseqfive+" or coursemater.Fees>"+greaterfivethousand+")";
			
		}

		else if(!(eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
		{
			query1="(coursemater.Fees>"+paid+" or coursemater.free="+free+")and(coursemater.level='"+beginner+"' or coursemater.level='"+intermediate+"' or coursemater.level='"+advanced+"')and(coursemater.Fees<="+lessfive+" or coursemater.Fees>"+greaterfivehun+" and coursemater.Fees<="+lesstwot+" or coursemater.Fees>"+greatertwothousnd+" and coursemater.Fees<="+lesseqfive+" or coursemater.Fees>"+greaterfivethousand+")";
			
		}


            List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
				DynamicDataForm roleForm = null;
		String getnumber="";
		String concatnumber="";
		String coursecatid="";
				Connection con = null;
				PreparedStatement pstmt = null;
				ResultSet rs = null;
				String sql = null;
				try {
					con = dataSource.getConnection();
					if(categorynames.equalsIgnoreCase(""))
					{

					sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
							+ " LEFT JOIN coursecategorymaster ON "
							+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
							+

							" LEFT JOIN companymaster ON "
							+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
							+ " LEFT JOIN citymaster ON "
							+ " citymaster.cityid=companymaster.cityid "
							+

							" LEFT JOIN ratingtable ON "
							+ " coursemater.courseid=ratingtable.courseid " +

							" where(coursemater.active=1) and "+query1+"  group by coursemater.courseid order by coursemater.Fees asc";
					

								System.out.println("sqlllllllllllllllllll"+sql);
								pstmt = con.prepareStatement(sql);
								rs = pstmt.executeQuery();
								while (rs.next()) {
									System.out.println("first condition");
									roleForm = new DynamicDataForm();
									roleForm.setCoursenamelatest(rs
											.getString("coursemater.CourseName"));
									roleForm.setCoursesid(rs.getInt("CourseId"));
									roleForm.setCatid(rs.getInt("coursecatId"));
		                          
									System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
									roleForm.setFees(rs.getDouble("coursemater.fees"));
									roleForm.setCompname(rs.getString("companymaster.companyname"));
									roleForm.setCourselatestlogo(rs
											.getString("coursemater.imagename"));
									roleForm.setCityname(rs.getString("citymaster.city"));
									String rating = rs.getString("rate");
									if (rating.equals("0.0000")) {
										roleForm.setRating("images/smallrating-1.png");
									}
									if (rating.equals("1.0000")) {
										System.out.println("yes");
										roleForm.setRating("images/smallrating-1.png");
									}
									if (rating.equals("2.0000")) {
										System.out.println("yes");
										roleForm.setRating("images/smallrating-2.png");
									}
									if (rating.equals("3.0000")) {
										System.out.println("yes");
										roleForm.setRating("images/smallrating-3.png");
									}
									if (rating.equals("4.0000")) {
										System.out.println("yes");
										roleForm.setRating("images/smallrating-4.png");
									}
									if (rating.equals("5.0000")) {
										System.out.println("yes");
										roleForm.setRating("images/smallrating-5.png");
									}

									System.out.println("vani " + rating);
									list.add(roleForm);
								}
					}
					else if(!categorynames.equalsIgnoreCase("")&&!query1.equalsIgnoreCase(""))

								{
									System.out.println("both");
								      String[] stringArray = categorynames.split(",");

								      for (int i = 0; i < stringArray.length; i++) { 
								    	  
								    	  
								    	  sql="select distinct  coursecatId from coursemater where active=1 and coursecatId="+stringArray[i]+" ";
								    	  
											pstmt = con.prepareStatement(sql);

											rs = pstmt.executeQuery();
											while (rs.next()) {
												getnumber=rs.getString("coursecatId");
												concatnumber=concatnumber.concat(",").concat(getnumber);
												System.out.println("concatnumber"+concatnumber);
												coursecatid=concatnumber.substring(1, concatnumber.length());
												System.out.println("coursecatid"+coursecatid);
												
											}

								      }
								      String[] stringArrays = coursecatid.split(",");
								      for (int i = 0; i < stringArrays.length; i++) { 


								      
								    	  sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
												+ " LEFT JOIN coursecategorymaster ON "
												+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
												+

												" LEFT JOIN companymaster ON "
												
												+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
												+ " LEFT JOIN citymaster ON "
												+ " citymaster.cityid=companymaster.cityid "
												+

												" LEFT JOIN ratingtable ON "
												+ " coursemater.courseid=ratingtable.courseid " +

												" where (coursemater.active=1) and "+query1+" and(coursemater.coursecatId="+stringArrays[i]+")  group by coursemater.courseid order by coursemater.Fees asc";
							System.out.println("sqllllllllllllllllllllllllll"+sql);
								pstmt = con.prepareStatement(sql);
								rs = pstmt.executeQuery();
								while (rs.next()) {
									System.out.println("second condition");
									roleForm = new DynamicDataForm();
									roleForm.setCoursenamelatest(rs
											.getString("coursemater.CourseName"));
									roleForm.setCoursesid(rs.getInt("CourseId"));
									roleForm.setCatid(rs.getInt("coursecatId"));
		                          
									System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
									roleForm.setFees(rs.getDouble("coursemater.fees"));
									roleForm.setCompname(rs.getString("companymaster.companyname"));
									roleForm.setCourselatestlogo(rs
											.getString("coursemater.imagename"));
									roleForm.setCityname(rs.getString("citymaster.city"));
									String rating = rs.getString("rate");
									if (rating.equals("0.0000")) {
										roleForm.setRating("images/smallrating-1.png");
									}
									if (rating.equals("1.0000")) {
										System.out.println("yes");
										roleForm.setRating("images/smallrating-1.png");
									}
									if (rating.equals("2.0000")) {
										System.out.println("yes");
										roleForm.setRating("images/smallrating-2.png");
									}
									if (rating.equals("3.0000")) {
										System.out.println("yes");
										roleForm.setRating("images/smallrating-3.png");
									}
									if (rating.equals("4.0000")) {
										System.out.println("yes");
										roleForm.setRating("images/smallrating-4.png");
									}
									if (rating.equals("5.0000")) {
										System.out.println("yes");
										roleForm.setRating("images/smallrating-5.png");
									}

									System.out.println("vani " + rating);
									list.add(roleForm);
								}
								
								
								
								      }

								}	
					else if(!categorynames.equalsIgnoreCase("")&&query1.equalsIgnoreCase(""))
					{
						System.out.println("other options");
					      String[] stringArray = categorynames.split(",");

					      for (int i = 0; i < stringArray.length; i++) { 
					    	  
					    	  
					    	  sql="select distinct coursecatId from coursemater where active=1 and coursecatId="+stringArray[i]+" ";
					    	  
								pstmt = con.prepareStatement(sql);

								rs = pstmt.executeQuery();
								while (rs.next()) {
									getnumber=rs.getString("coursecatId");
									concatnumber=concatnumber.concat(",").concat(getnumber);
									System.out.println("concatnumber"+concatnumber);
									coursecatid=concatnumber.substring(1, concatnumber.length());
									System.out.println("coursecatid"+coursecatid);
									
								}

					      }
					      String[] stringArrays = coursecatid.split(",");
					      for (int i = 0; i < stringArrays.length; i++) { 


					      
					    	  sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
									+ " LEFT JOIN coursecategorymaster ON "
									+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
									+

									" LEFT JOIN companymaster ON "
									+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
									+ " LEFT JOIN citymaster ON "
									+ " citymaster.cityid=companymaster.cityid "
									+

									" LEFT JOIN ratingtable ON "
									+ " coursemater.courseid=ratingtable.courseid " +

									" where (coursemater.active=1)  and(coursemater.coursecatId="+stringArrays[i]+")  group by coursemater.courseid order by coursemater.Fees asc";
				System.out.println("sqllllllllllllllllllllllllll"+sql);
					pstmt = con.prepareStatement(sql);
					rs = pstmt.executeQuery();
					while (rs.next()) {
						System.out.println("third condition");
						roleForm = new DynamicDataForm();
						roleForm.setCoursenamelatest(rs
								.getString("coursemater.CourseName"));
						System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
						roleForm.setFees(rs.getDouble("coursemater.fees"));
						roleForm.setCoursesid(rs.getInt("CourseId"));
						roleForm.setCatid(rs.getInt("coursecatId"));
                      
						roleForm.setCompname(rs.getString("companymaster.companyname"));
						roleForm.setCourselatestlogo(rs
								.getString("coursemater.imagename"));
						roleForm.setCityname(rs.getString("citymaster.city"));
						String rating = rs.getString("rate");
						if (rating.equals("0.0000")) {
							roleForm.setRating("images/smallrating-1.png");
						}
						if (rating.equals("1.0000")) {
							System.out.println("yes");
							roleForm.setRating("images/smallrating-1.png");
						}
						if (rating.equals("2.0000")) {
							System.out.println("yes");
							roleForm.setRating("images/smallrating-2.png");
						}
						if (rating.equals("3.0000")) {
							System.out.println("yes");
							roleForm.setRating("images/smallrating-3.png");
						}
						if (rating.equals("4.0000")) {
							System.out.println("yes");
							roleForm.setRating("images/smallrating-4.png");
						}
						if (rating.equals("5.0000")) {
							System.out.println("yes");
							roleForm.setRating("images/smallrating-5.png");
						}

						System.out.println("vani " + rating);
						list.add(roleForm);
					}
					
					}

					}
						



				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					try {
						ConnectionUtil.closeResources(con, pstmt, rs);
					} catch (Exception e2) {
					}
				}
				return list;

		}

		
		
		
		
	
		
	public List<DynamicDataForm> pricehightolowselectingcourses(
			DataSource dataSource, String eleaning, String classroom,
			String paid, String free, String beginner, String intermediate,
			String advanced, String lessfive, String graterfive,
			String gratertwo, String greaterfivethousand, String categorynames) {
		System.out.println("testing elearning courses"+eleaning);
		System.out.println("testing classroom courses"+classroom);
		System.out.println("testing classroom beginners"+classroom);
		String greaterfivehun=graterfive.substring(0, 3);
		System.out.println("graterfive"+greaterfivehun);
		String lesstwot=graterfive.substring(3, 7);
		System.out.println("lesstwot"+lesstwot);
		String greatertwothousnd=gratertwo.substring(0, 4);
		String lesseqfive=gratertwo.substring(4, 8);
		System.out.println("beginnerssssssssss"+beginner);

		String where="";

		String query1="";
		String query2="";

		if((eleaning.equals("1")||classroom.equals("1"))&&!(paid.equals("0")|| free.equals("1"))&&!(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&!(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000")))))){
			query1="coursemater.classroomtraining="+classroom+" or coursemater.elearning="+eleaning+"";
			}
		else if(!(eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&!(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&!(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000")))))){

			
				query1="coursemater.Fees>"+paid+" or coursemater.free="+free+"";
				
				
			}
		else if((eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&!(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&!(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000")))))){
			
				query1="(coursemater.classroomtraining="+classroom+" or coursemater.elearning="+eleaning+") and (coursemater.Fees>"+paid+" or coursemater.free="+free+") ";

			}
				
		else if(!(eleaning.equals("1")||classroom.equals("1"))&&!(paid.equals("0")|| free.equals("1"))&&(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&!(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000")))))){
			
				query1="coursemater.level='"+beginner+"' or coursemater.level='"+intermediate+"' or coursemater.level='"+advanced+"'";
			}
		else if((eleaning.equals("1")||classroom.equals("1"))&&!(paid.equals("0")|| free.equals("1"))&&(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))){
			
				query1="(coursemater.classroomtraining="+classroom+" or coursemater.elearning="+eleaning+") and (coursemater.level='"+beginner+"' or coursemater.level='"+intermediate+"' or coursemater.level='"+advanced+"') ";
			}
		else if(!(eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&!(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000")))))){
			
				query1="(coursemater.Fees>"+paid+" or coursemater.free="+free+") and (coursemater.level='"+beginner+"' or coursemater.level='"+intermediate+"' or coursemater.level='"+advanced+"')";
			}

		else if((eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&!(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
			{
				query1="(coursemater.classroomtraining="+classroom+" or coursemater.elearning="+eleaning+") and (coursemater.Fees>"+paid+" or coursemater.free="+free+") and (coursemater.level='"+beginner+"' or coursemater.level='"+intermediate+"' or coursemater.level='"+advanced+"')";
			}
		else if(!(eleaning.equals("1")||classroom.equals("1"))&&!(paid.equals("0")|| free.equals("1"))&&!(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
		{
			query1="coursemater.Fees<="+lessfive+" or coursemater.Fees>"+greaterfivehun+" and coursemater.Fees<="+lesstwot+" or coursemater.Fees>"+greatertwothousnd+" and coursemater.Fees<="+lesseqfive+" or coursemater.Fees>"+greaterfivethousand+"";
			
		}
		else if((eleaning.equals("1")||classroom.equals("1"))&&!(paid.equals("0")|| free.equals("1"))&&!(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
		{
			query1="(coursemater.classroomtraining="+classroom+" or coursemater.elearning="+eleaning+")and(coursemater.Fees<="+lessfive+" or coursemater.Fees>"+greaterfivehun+" and coursemater.Fees<="+lesstwot+" or coursemater.Fees>"+greatertwothousnd+" and coursemater.Fees<="+lesseqfive+" or coursemater.Fees>"+greaterfivethousand+")";
			
		}
		else if(!(eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&!(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
		{
			query1="(coursemater.Fees>"+paid+" or coursemater.free="+free+")and(coursemater.Fees<="+lessfive+" or coursemater.Fees>"+greaterfivehun+" and coursemater.Fees<="+lesstwot+" or coursemater.Fees>"+greatertwothousnd+" and coursemater.Fees<="+lesseqfive+" or coursemater.Fees>"+greaterfivethousand+")";
			
		}

		else if(!(eleaning.equals("1")||classroom.equals("1"))&&!(paid.equals("0")|| free.equals("1"))&&(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
		{
			query1="(coursemater.level='"+beginner+"' or coursemater.level='"+intermediate+"' or coursemater.level='"+advanced+"')and(coursemater.Fees<="+lessfive+" or coursemater.Fees>"+greaterfivehun+" and coursemater.Fees<="+lesstwot+" or coursemater.Fees>"+greatertwothousnd+" and coursemater.Fees<="+lesseqfive+" or coursemater.Fees>"+greaterfivethousand+")";
			
		}
		else if((eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&!(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
		{
			query1="(coursemater.classroomtraining="+classroom+" or coursemater.elearning="+eleaning+") and(coursemater.Fees>"+paid+" or coursemater.free="+free+")and(coursemater.Fees<="+lessfive+" or coursemater.Fees>"+greaterfivehun+" and coursemater.Fees<="+lesstwot+" or coursemater.Fees>"+greatertwothousnd+" and coursemater.Fees<="+lesseqfive+" or coursemater.Fees>"+greaterfivethousand+")";
			
		}
		else if((eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
		{
			query1="(coursemater.classroomtraining="+classroom+" or coursemater.elearning="+eleaning+") and(coursemater.Fees>"+paid+" or coursemater.free="+free+")and(coursemater.level='"+beginner+"' or coursemater.level='"+intermediate+"' or coursemater.level='"+advanced+"')and(coursemater.Fees<="+lessfive+" or coursemater.Fees>"+greaterfivehun+" and coursemater.Fees<="+lesstwot+" or coursemater.Fees>"+greatertwothousnd+" and coursemater.Fees<="+lesseqfive+" or coursemater.Fees>"+greaterfivethousand+")";
			
		}

		else if(!(eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
		{
			query1="(coursemater.Fees>"+paid+" or coursemater.free="+free+")and(coursemater.level='"+beginner+"' or coursemater.level='"+intermediate+"' or coursemater.level='"+advanced+"')and(coursemater.Fees<="+lessfive+" or coursemater.Fees>"+greaterfivehun+" and coursemater.Fees<="+lesstwot+" or coursemater.Fees>"+greatertwothousnd+" and coursemater.Fees<="+lesseqfive+" or coursemater.Fees>"+greaterfivethousand+")";
			
		}




			List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
				DynamicDataForm roleForm = null;
		String getnumber="";
		String concatnumber="";
		String coursecatid="";
				Connection con = null;
				PreparedStatement pstmt = null;
				ResultSet rs = null;
				String sql = null;
				try {
					con = dataSource.getConnection();
					if(categorynames.equalsIgnoreCase(""))
					{

					sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
							+ " LEFT JOIN coursecategorymaster ON "
							+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
							+

							" LEFT JOIN companymaster ON "
							+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
							+ " LEFT JOIN citymaster ON "
							+ " citymaster.cityid=companymaster.cityid "
							+

							" LEFT JOIN ratingtable ON "
							+ " coursemater.courseid=ratingtable.courseid " +

							" where(coursemater.active=1) and "+query1+"  group by coursemater.courseid order by coursemater.Fees desc";
					

								System.out.println("sqlllllllllllllllllll"+sql);
								pstmt = con.prepareStatement(sql);
								rs = pstmt.executeQuery();
								while (rs.next()) {
									System.out.println("first condition");
									roleForm = new DynamicDataForm();
									roleForm.setCoursenamelatest(rs
											.getString("coursemater.CourseName"));
									System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
									roleForm.setFees(rs.getDouble("coursemater.fees"));
									roleForm.setCoursesid(rs.getInt("CourseId"));
									roleForm.setCatid(rs.getInt("coursecatId"));

									roleForm.setCompname(rs.getString("companymaster.companyname"));
									roleForm.setCourselatestlogo(rs
											.getString("coursemater.imagename"));
									roleForm.setCityname(rs.getString("citymaster.city"));
									String rating = rs.getString("rate");
									if (rating.equals("0.0000")) {
										roleForm.setRating("images/smallrating-1.png");
									}
									if (rating.equals("1.0000")) {
										System.out.println("yes");
										roleForm.setRating("images/smallrating-1.png");
									}
									if (rating.equals("2.0000")) {
										System.out.println("yes");
										roleForm.setRating("images/smallrating-2.png");
									}
									if (rating.equals("3.0000")) {
										System.out.println("yes");
										roleForm.setRating("images/smallrating-3.png");
									}
									if (rating.equals("4.0000")) {
										System.out.println("yes");
										roleForm.setRating("images/smallrating-4.png");
									}
									if (rating.equals("5.0000")) {
										System.out.println("yes");
										roleForm.setRating("images/smallrating-5.png");
									}

									System.out.println("vani " + rating);
									list.add(roleForm);
								}
					}
					else if(!categorynames.equalsIgnoreCase("")&&!query1.equalsIgnoreCase(""))

								{
									System.out.println("both");
								      String[] stringArray = categorynames.split(",");

								      for (int i = 0; i < stringArray.length; i++) { 
								    	  
								    	  
								    	  sql="select distinct  coursecatId from coursemater where active=1 and coursecatId="+stringArray[i]+" ";
								    	  
											pstmt = con.prepareStatement(sql);

											rs = pstmt.executeQuery();
											while (rs.next()) {
												getnumber=rs.getString("coursecatId");
												concatnumber=concatnumber.concat(",").concat(getnumber);
												System.out.println("concatnumber"+concatnumber);
												coursecatid=concatnumber.substring(1, concatnumber.length());
												System.out.println("coursecatid"+coursecatid);
												
											}

								      }
								      String[] stringArrays = coursecatid.split(",");
								      for (int i = 0; i < stringArrays.length; i++) { 


								      
								    	  sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
												+ " LEFT JOIN coursecategorymaster ON "
												+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
												+

												" LEFT JOIN companymaster ON "
												
												+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
												+ " LEFT JOIN citymaster ON "
												+ " citymaster.cityid=companymaster.cityid "
												+

												" LEFT JOIN ratingtable ON "
												+ " coursemater.courseid=ratingtable.courseid " +

												" where (coursemater.active=1) and "+query1+" and(coursemater.coursecatId="+stringArrays[i]+")  group by coursemater.courseid order by coursemater.Fees desc";
							System.out.println("sqllllllllllllllllllllllllll"+sql);
								pstmt = con.prepareStatement(sql);
								rs = pstmt.executeQuery();
								while (rs.next()) {
									System.out.println("second condition");
									roleForm = new DynamicDataForm();
									roleForm.setCoursenamelatest(rs
											.getString("coursemater.CourseName"));
									roleForm.setCoursesid(rs.getInt("CourseId"));
									roleForm.setCatid(rs.getInt("coursecatId"));
		                            
									System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
									roleForm.setFees(rs.getDouble("coursemater.fees"));
									roleForm.setCompname(rs.getString("companymaster.companyname"));
									roleForm.setCourselatestlogo(rs
											.getString("coursemater.imagename"));
									roleForm.setCityname(rs.getString("citymaster.city"));
									String rating = rs.getString("rate");
									if (rating.equals("0.0000")) {
										roleForm.setRating("images/smallrating-1.png");
									}
									if (rating.equals("1.0000")) {
										System.out.println("yes");
										roleForm.setRating("images/smallrating-1.png");
									}
									if (rating.equals("2.0000")) {
										System.out.println("yes");
										roleForm.setRating("images/smallrating-2.png");
									}
									if (rating.equals("3.0000")) {
										System.out.println("yes");
										roleForm.setRating("images/smallrating-3.png");
									}
									if (rating.equals("4.0000")) {
										System.out.println("yes");
										roleForm.setRating("images/smallrating-4.png");
									}
									if (rating.equals("5.0000")) {
										System.out.println("yes");
										roleForm.setRating("images/smallrating-5.png");
									}

									System.out.println("vani " + rating);
									list.add(roleForm);
								}
								
								
								
								      }

								}	
					else if(!categorynames.equalsIgnoreCase("")&&query1.equalsIgnoreCase(""))
					{
						System.out.println("other options");
					      String[] stringArray = categorynames.split(",");

					      for (int i = 0; i < stringArray.length; i++) { 
					    	  
					    	  
					    	  sql="select distinct coursecatId from coursemater where active=1 and coursecatId="+stringArray[i]+" ";
					    	  
								pstmt = con.prepareStatement(sql);

								rs = pstmt.executeQuery();
								while (rs.next()) {
									getnumber=rs.getString("coursecatId");
									concatnumber=concatnumber.concat(",").concat(getnumber);
									System.out.println("concatnumber"+concatnumber);
									coursecatid=concatnumber.substring(1, concatnumber.length());
									System.out.println("coursecatid"+coursecatid);
									
								}

					      }
					      String[] stringArrays = coursecatid.split(",");
					      for (int i = 0; i < stringArrays.length; i++) { 


					      
					    	  sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
									+ " LEFT JOIN coursecategorymaster ON "
									+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
									+

									" LEFT JOIN companymaster ON "
									+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
									+ " LEFT JOIN citymaster ON "
									+ " citymaster.cityid=companymaster.cityid "
									+

									" LEFT JOIN ratingtable ON "
									+ " coursemater.courseid=ratingtable.courseid " +

									" where (coursemater.active=1)  and(coursemater.coursecatId="+stringArrays[i]+")  group by coursemater.courseid order by coursemater.Fees desc";
				System.out.println("sqllllllllllllllllllllllllll"+sql);
					pstmt = con.prepareStatement(sql);
					rs = pstmt.executeQuery();
					while (rs.next()) {
						System.out.println("third condition");
						roleForm = new DynamicDataForm();
						roleForm.setCoursenamelatest(rs
								.getString("coursemater.CourseName"));
						System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
						roleForm.setFees(rs.getDouble("coursemater.fees"));
						roleForm.setCoursesid(rs.getInt("CourseId"));
						roleForm.setCatid(rs.getInt("coursecatId"));
                        
						roleForm.setCompname(rs.getString("companymaster.companyname"));
						roleForm.setCourselatestlogo(rs
								.getString("coursemater.imagename"));
						roleForm.setCityname(rs.getString("citymaster.city"));
						String rating = rs.getString("rate");
						if (rating.equals("0.0000")) {
							roleForm.setRating("images/smallrating-1.png");
						}
						if (rating.equals("1.0000")) {
							System.out.println("yes");
							roleForm.setRating("images/smallrating-1.png");
						}
						if (rating.equals("2.0000")) {
							System.out.println("yes");
							roleForm.setRating("images/smallrating-2.png");
						}
						if (rating.equals("3.0000")) {
							System.out.println("yes");
							roleForm.setRating("images/smallrating-3.png");
						}
						if (rating.equals("4.0000")) {
							System.out.println("yes");
							roleForm.setRating("images/smallrating-4.png");
						}
						if (rating.equals("5.0000")) {
							System.out.println("yes");
							roleForm.setRating("images/smallrating-5.png");
						}

						System.out.println("vani " + rating);
						list.add(roleForm);
					}
					
					}

					}
						



				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					try {
						ConnectionUtil.closeResources(con, pstmt, rs);
					} catch (Exception e2) {
					}
				}
				return list;

		}

	public List<DynamicDataForm> priceratewiseselectingcourses(
			DataSource dataSource, String eleaning, String classroom,
			String paid, String free, String beginner, String intermediate,
			String advanced, String lessfive, String graterfive,
			String gratertwo, String greaterfivethousand, String categorynames) {
		System.out.println("testing elearning courses"+eleaning);
		System.out.println("testing classroom courses"+classroom);
		System.out.println("testing classroom beginners"+classroom);
		String greaterfivehun=graterfive.substring(0, 3);
		System.out.println("graterfive"+greaterfivehun);
		String lesstwot=graterfive.substring(3, 7);
		System.out.println("lesstwot"+lesstwot);
		String greatertwothousnd=gratertwo.substring(0, 4);
		String lesseqfive=gratertwo.substring(4, 8);
		System.out.println("beginnerssssssssss"+beginner);

		String where="";

		String query1="";
		String query2="";

		if((eleaning.equals("1")||classroom.equals("1"))&&!(paid.equals("0")|| free.equals("1"))&&!(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&!(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000")))))){
			query1="coursemater.classroomtraining="+classroom+" or coursemater.elearning="+eleaning+"";
			}
		else if(!(eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&!(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&!(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000")))))){

			
				query1="coursemater.Fees>"+paid+" or coursemater.free="+free+"";
				
				
			}
		else if((eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&!(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&!(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000")))))){
			
				query1="(coursemater.classroomtraining="+classroom+" or coursemater.elearning="+eleaning+") and (coursemater.Fees>"+paid+" or coursemater.free="+free+") ";

			}
				
		else if(!(eleaning.equals("1")||classroom.equals("1"))&&!(paid.equals("0")|| free.equals("1"))&&(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&!(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000")))))){
			
				query1="coursemater.level='"+beginner+"' or coursemater.level='"+intermediate+"' or coursemater.level='"+advanced+"'";
			}
		else if((eleaning.equals("1")||classroom.equals("1"))&&!(paid.equals("0")|| free.equals("1"))&&(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))){
			
				query1="(coursemater.classroomtraining="+classroom+" or coursemater.elearning="+eleaning+") and (coursemater.level='"+beginner+"' or coursemater.level='"+intermediate+"' or coursemater.level='"+advanced+"') ";
			}
		else if(!(eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&!(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000")))))){
			
				query1="(coursemater.Fees>"+paid+" or coursemater.free="+free+") and (coursemater.level='"+beginner+"' or coursemater.level='"+intermediate+"' or coursemater.level='"+advanced+"')";
			}

		else if((eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&!(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
			{
				query1="(coursemater.classroomtraining="+classroom+" or coursemater.elearning="+eleaning+") and (coursemater.Fees>"+paid+" or coursemater.free="+free+") and (coursemater.level='"+beginner+"' or coursemater.level='"+intermediate+"' or coursemater.level='"+advanced+"')";
			}
		else if(!(eleaning.equals("1")||classroom.equals("1"))&&!(paid.equals("0")|| free.equals("1"))&&!(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
		{
			query1="coursemater.Fees<="+lessfive+" or coursemater.Fees>"+greaterfivehun+" and coursemater.Fees<="+lesstwot+" or coursemater.Fees>"+greatertwothousnd+" and coursemater.Fees<="+lesseqfive+" or coursemater.Fees>"+greaterfivethousand+"";
			
		}
		else if((eleaning.equals("1")||classroom.equals("1"))&&!(paid.equals("0")|| free.equals("1"))&&!(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
		{
			query1="(coursemater.classroomtraining="+classroom+" or coursemater.elearning="+eleaning+")and(coursemater.Fees<="+lessfive+" or coursemater.Fees>"+greaterfivehun+" and coursemater.Fees<="+lesstwot+" or coursemater.Fees>"+greatertwothousnd+" and coursemater.Fees<="+lesseqfive+" or coursemater.Fees>"+greaterfivethousand+")";
			
		}
		else if(!(eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&!(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
		{
			query1="(coursemater.Fees>"+paid+" or coursemater.free="+free+")and(coursemater.Fees<="+lessfive+" or coursemater.Fees>"+greaterfivehun+" and coursemater.Fees<="+lesstwot+" or coursemater.Fees>"+greatertwothousnd+" and coursemater.Fees<="+lesseqfive+" or coursemater.Fees>"+greaterfivethousand+")";
			
		}

		else if(!(eleaning.equals("1")||classroom.equals("1"))&&!(paid.equals("0")|| free.equals("1"))&&(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
		{
			query1="(coursemater.level='"+beginner+"' or coursemater.level='"+intermediate+"' or coursemater.level='"+advanced+"')and(coursemater.Fees<="+lessfive+" or coursemater.Fees>"+greaterfivehun+" and coursemater.Fees<="+lesstwot+" or coursemater.Fees>"+greatertwothousnd+" and coursemater.Fees<="+lesseqfive+" or coursemater.Fees>"+greaterfivethousand+")";
			
		}
		else if((eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&!(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
		{
			query1="(coursemater.classroomtraining="+classroom+" or coursemater.elearning="+eleaning+") and(coursemater.Fees>"+paid+" or coursemater.free="+free+")and(coursemater.Fees<="+lessfive+" or coursemater.Fees>"+greaterfivehun+" and coursemater.Fees<="+lesstwot+" or coursemater.Fees>"+greatertwothousnd+" and coursemater.Fees<="+lesseqfive+" or coursemater.Fees>"+greaterfivethousand+")";
			
		}
		else if((eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
		{
			query1="(coursemater.classroomtraining="+classroom+" or coursemater.elearning="+eleaning+") and(coursemater.Fees>"+paid+" or coursemater.free="+free+")and(coursemater.level='"+beginner+"' or coursemater.level='"+intermediate+"' or coursemater.level='"+advanced+"')and(coursemater.Fees<="+lessfive+" or coursemater.Fees>"+greaterfivehun+" and coursemater.Fees<="+lesstwot+" or coursemater.Fees>"+greatertwothousnd+" and coursemater.Fees<="+lesseqfive+" or coursemater.Fees>"+greaterfivethousand+")";
			
		}

		else if(!(eleaning.equals("1")||classroom.equals("1"))&&(paid.equals("0")|| free.equals("1"))&&(beginner.equalsIgnoreCase("Beginner")||(intermediate.equals("Intermediate")||(advanced.equals("Advanced"))))&&(lessfive.equals("500")||(graterfive.equals("5002000")||(gratertwo.equals("20005000")||(greaterfivethousand.equals("5000"))))))
		{
			query1="(coursemater.Fees>"+paid+" or coursemater.free="+free+")and(coursemater.level='"+beginner+"' or coursemater.level='"+intermediate+"' or coursemater.level='"+advanced+"')and(coursemater.Fees<="+lessfive+" or coursemater.Fees>"+greaterfivehun+" and coursemater.Fees<="+lesstwot+" or coursemater.Fees>"+greatertwothousnd+" and coursemater.Fees<="+lesseqfive+" or coursemater.Fees>"+greaterfivethousand+")";
			
		}




			List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
				DynamicDataForm roleForm = null;
		String getnumber="";
		String concatnumber="";
		String coursecatid="";
				Connection con = null;
				PreparedStatement pstmt = null;
				ResultSet rs = null;
				String sql = null;
				try {
					con = dataSource.getConnection();
					if(categorynames.equalsIgnoreCase(""))
					{

					sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
							+ " LEFT JOIN coursecategorymaster ON "
							+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
							+

							" LEFT JOIN companymaster ON "
							+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
							+ " LEFT JOIN citymaster ON "
							+ " citymaster.cityid=companymaster.cityid "
							+

							" LEFT JOIN ratingtable ON "
							+ " coursemater.courseid=ratingtable.courseid " +

							" where(coursemater.active=1) and "+query1+"  group by coursemater.courseid order by ratingtable.rating asc";
					

								System.out.println("sqlllllllllllllllllll"+sql);
								pstmt = con.prepareStatement(sql);
								rs = pstmt.executeQuery();
								while (rs.next()) {
									System.out.println("first condition");
									roleForm = new DynamicDataForm();
									roleForm.setCoursenamelatest(rs
											.getString("coursemater.CourseName"));
									System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
									roleForm.setFees(rs.getDouble("coursemater.fees"));
									roleForm.setCoursesid(rs.getInt("CourseId"));
									roleForm.setCatid(rs.getInt("coursecatId"));
                                    roleForm.setCompname(rs.getString("companymaster.companyname"));
									roleForm.setCourselatestlogo(rs
											.getString("coursemater.imagename"));
									roleForm.setCityname(rs.getString("citymaster.city"));
									String rating = rs.getString("rate");
									if (rating.equals("0.0000")) {
										roleForm.setRating("images/smallrating-1.png");
									}
									if (rating.equals("1.0000")) {
										System.out.println("yes");
										roleForm.setRating("images/smallrating-1.png");
									}
									if (rating.equals("2.0000")) {
										System.out.println("yes");
										roleForm.setRating("images/smallrating-2.png");
									}
									if (rating.equals("3.0000")) {
										System.out.println("yes");
										roleForm.setRating("images/smallrating-3.png");
									}
									if (rating.equals("4.0000")) {
										System.out.println("yes");
										roleForm.setRating("images/smallrating-4.png");
									}
									if (rating.equals("5.0000")) {
										System.out.println("yes");
										roleForm.setRating("images/smallrating-5.png");
									}

									System.out.println("vani " + rating);
									list.add(roleForm);
								}
					}
					else if(!categorynames.equalsIgnoreCase("")&&!query1.equalsIgnoreCase(""))

								{
									System.out.println("both");
								      String[] stringArray = categorynames.split(",");

								      for (int i = 0; i < stringArray.length; i++) { 
								    	  
								    	  
								    	  sql="select distinct  coursecatId from coursemater where active=1 and coursecatId="+stringArray[i]+" ";
								    	  
											pstmt = con.prepareStatement(sql);

											rs = pstmt.executeQuery();
											while (rs.next()) {
												getnumber=rs.getString("coursecatId");
												concatnumber=concatnumber.concat(",").concat(getnumber);
												System.out.println("concatnumber"+concatnumber);
												coursecatid=concatnumber.substring(1, concatnumber.length());
												System.out.println("coursecatid"+coursecatid);
												
											}

								      }
								      String[] stringArrays = coursecatid.split(",");
								      for (int i = 0; i < stringArrays.length; i++) { 


								      
								    	  sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
												+ " LEFT JOIN coursecategorymaster ON "
												+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
												+

												" LEFT JOIN companymaster ON "
												
												+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
												+ " LEFT JOIN citymaster ON "
												+ " citymaster.cityid=companymaster.cityid "
												+

												" LEFT JOIN ratingtable ON "
												+ " coursemater.courseid=ratingtable.courseid " +

												" where (coursemater.active=1) and "+query1+" and(coursemater.coursecatId="+stringArrays[i]+")  group by coursemater.courseid order by ratingtable.rating asc";
							System.out.println("sqllllllllllllllllllllllllll"+sql);
								pstmt = con.prepareStatement(sql);
								rs = pstmt.executeQuery();
								while (rs.next()) {
									System.out.println("second condition");
									roleForm = new DynamicDataForm();
									roleForm.setCoursenamelatest(rs
											.getString("coursemater.CourseName"));
									roleForm.setCoursesid(rs.getInt("CourseId"));
									roleForm.setCatid(rs.getInt("coursecatId"));
                                    System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
									roleForm.setFees(rs.getDouble("coursemater.fees"));
									roleForm.setCompname(rs.getString("companymaster.companyname"));
									roleForm.setCourselatestlogo(rs
											.getString("coursemater.imagename"));
									roleForm.setCityname(rs.getString("citymaster.city"));
									String rating = rs.getString("rate");
									if (rating.equals("0.0000")) {
										roleForm.setRating("images/smallrating-1.png");
									}
									if (rating.equals("1.0000")) {
										System.out.println("yes");
										roleForm.setRating("images/smallrating-1.png");
									}
									if (rating.equals("2.0000")) {
										System.out.println("yes");
										roleForm.setRating("images/smallrating-2.png");
									}
									if (rating.equals("3.0000")) {
										System.out.println("yes");
										roleForm.setRating("images/smallrating-3.png");
									}
									if (rating.equals("4.0000")) {
										System.out.println("yes");
										roleForm.setRating("images/smallrating-4.png");
									}
									if (rating.equals("5.0000")) {
										System.out.println("yes");
										roleForm.setRating("images/smallrating-5.png");
									}

									System.out.println("vani " + rating);
									list.add(roleForm);
								}
								
								
								
								      }

								}	
					else if(!categorynames.equalsIgnoreCase("")&&query1.equalsIgnoreCase(""))
					{
						System.out.println("other options");
					      String[] stringArray = categorynames.split(",");

					      for (int i = 0; i < stringArray.length; i++) { 
					    	  
					    	  
					    	  sql="select distinct coursecatId from coursemater where active=1 and coursecatId="+stringArray[i]+" ";
					    	  
								pstmt = con.prepareStatement(sql);

								rs = pstmt.executeQuery();
								while (rs.next()) {
									getnumber=rs.getString("coursecatId");
									concatnumber=concatnumber.concat(",").concat(getnumber);
									System.out.println("concatnumber"+concatnumber);
									coursecatid=concatnumber.substring(1, concatnumber.length());
									System.out.println("coursecatid"+coursecatid);
									
								}

					      }
					      String[] stringArrays = coursecatid.split(",");
					      for (int i = 0; i < stringArrays.length; i++) { 


					      
					    	  sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
									+ " LEFT JOIN coursecategorymaster ON "
									+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
									+

									" LEFT JOIN companymaster ON "
									+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
									+ " LEFT JOIN citymaster ON "
									+ " citymaster.cityid=companymaster.cityid "
									+

									" LEFT JOIN ratingtable ON "
									+ " coursemater.courseid=ratingtable.courseid " +

									" where (coursemater.active=1)  and(coursemater.coursecatId="+stringArrays[i]+")  group by coursemater.courseid order by ratingtable.rating asc";
				System.out.println("sqllllllllllllllllllllllllll"+sql);
					pstmt = con.prepareStatement(sql);
					rs = pstmt.executeQuery();
					while (rs.next()) {
						System.out.println("third condition");
						roleForm = new DynamicDataForm();
						roleForm.setCoursenamelatest(rs
								.getString("coursemater.CourseName"));
						System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
						roleForm.setFees(rs.getDouble("coursemater.fees"));
						roleForm.setCoursesid(rs.getInt("CourseId"));
						roleForm.setCatid(rs.getInt("coursecatId"));
                        roleForm.setCompname(rs.getString("companymaster.companyname"));
						roleForm.setCourselatestlogo(rs
								.getString("coursemater.imagename"));
						roleForm.setCityname(rs.getString("citymaster.city"));
						String rating = rs.getString("rate");
						if (rating.equals("0.0000")) {
							roleForm.setRating("images/smallrating-1.png");
						}
						if (rating.equals("1.0000")) {
							System.out.println("yes");
							roleForm.setRating("images/smallrating-1.png");
						}
						if (rating.equals("2.0000")) {
							System.out.println("yes");
							roleForm.setRating("images/smallrating-2.png");
						}
						if (rating.equals("3.0000")) {
							System.out.println("yes");
							roleForm.setRating("images/smallrating-3.png");
						}
						if (rating.equals("4.0000")) {
							System.out.println("yes");
							roleForm.setRating("images/smallrating-4.png");
						}
						if (rating.equals("5.0000")) {
							System.out.println("yes");
							roleForm.setRating("images/smallrating-5.png");
						}

						System.out.println("vani " + rating);
						list.add(roleForm);
					}
					
					}

					}
						



				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					try {
						ConnectionUtil.closeResources(con, pstmt, rs);
					} catch (Exception e2) {
					}
				}
				return list;

		}

	public List<DynamicDataForm> latestcourseratewise(DataSource dataSource,
			int catid) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();

			sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where coursemater.active=1 and coursemater.coursecatId=?  group by coursemater.courseid order by ratingtable.rating asc";

			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, catid);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setCoursenamelatest(rs
						.getString("coursemater.CourseName"));
				System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
				roleForm.setCompname(rs.getString("companymaster.companyname"));
				roleForm.setCoursesid(rs.getInt("CourseId"));
				roleForm.setCatid(rs.getInt("coursecatId"));
			     roleForm.setCourselatestlogo(rs
						.getString("coursemater.imagename"));
				roleForm.setFees(rs.getInt("coursemater.Fees"));
				roleForm.setCityname(rs.getString("citymaster.city"));
				String rating = rs.getString("rate");
				if (rating.equals("0.0000")) {
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("1.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("2.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-2.png");
				}
				if (rating.equals("3.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-3.png");
				}
				if (rating.equals("4.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-4.png");
				}
				if (rating.equals("5.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-5.png");
				}

				System.out.println("vani " + rating);
				list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;

	}

	public List<DynamicDataForm> latestcourserpricelowtohigh(
			DataSource dataSource, int catid) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();

			sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where coursemater.active=1 and coursemater.coursecatId=?  group by coursemater.courseid order by coursemater.Fees asc";

			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, catid);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setCoursenamelatest(rs
						.getString("coursemater.CourseName"));
				System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
				roleForm.setCompname(rs.getString("companymaster.companyname"));
				roleForm.setCoursesid(rs.getInt("CourseId"));
				roleForm.setCatid(rs.getInt("coursecatId"));
			     roleForm.setCourselatestlogo(rs
						.getString("coursemater.imagename"));
				roleForm.setFees(rs.getInt("coursemater.Fees"));
				roleForm.setCityname(rs.getString("citymaster.city"));
				String rating = rs.getString("rate");
				if (rating.equals("0.0000")) {
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("1.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("2.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-2.png");
				}
				if (rating.equals("3.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-3.png");
				}
				if (rating.equals("4.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-4.png");
				}
				if (rating.equals("5.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-5.png");
				}

				System.out.println("vani " + rating);
				list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;

	}

	public List<DynamicDataForm> latestcourserpricehightolow(
			DataSource dataSource, int catid) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();

			sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where coursemater.active=1 and coursemater.coursecatId=?  group by coursemater.courseid order by coursemater.Fees desc";

			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, catid);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setCoursenamelatest(rs
						.getString("coursemater.CourseName"));
				System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
				roleForm.setCompname(rs.getString("companymaster.companyname"));
				roleForm.setCoursesid(rs.getInt("CourseId"));
				roleForm.setCatid(rs.getInt("coursecatId"));
			     roleForm.setCourselatestlogo(rs
						.getString("coursemater.imagename"));
				roleForm.setFees(rs.getInt("coursemater.Fees"));
				roleForm.setCityname(rs.getString("citymaster.city"));
				String rating = rs.getString("rate");
				if (rating.equals("0.0000")) {
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("1.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("2.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-2.png");
				}
				if (rating.equals("3.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-3.png");
				}
				if (rating.equals("4.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-4.png");
				}
				if (rating.equals("5.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-5.png");
				}

				System.out.println("vani " + rating);
				list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;


	}

	public JSONArray city(DataSource dataSource, HttpServletRequest request,
			HttpServletResponse response) {
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
       JSONObject obj=null;
       int count=0;

		
		try {
			con=dataSource.getConnection();      
			String sql = "select CITY from citymaster where active=1";
             pstmt=con.prepareStatement(sql);
             rs=pstmt.executeQuery();	
			while(rs.next()){
               obj=new JSONObject(); 
				obj.put("city", rs.getString("CITY"));

				jArray.add(obj);

				       }
			}
		 catch (Exception e) {
			e.printStackTrace();
		} finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;

	}

	public ArrayList<String> getProductListArray(DataSource dataSource,
			String term) {
		ArrayList<String> list = new ArrayList<String>();
		PreparedStatement pstmt = null;
		Connection con=null;
		String sql=null;
		int count=0;
		String CourseName;
		ResultSet rs=null;
		System.out.println("productList="+term);
		
		try {
			con=dataSource.getConnection();
			
				sql="select distinct CourseName from coursemater where CourseName like ? and Active=1";
				pstmt=con.prepareStatement(sql);
				pstmt.setString(1, "%"+term +"%");
			
			
			System.out.println("term ="+term);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
			count++;
			CourseName=rs.getString("CourseName");
				
				
				
				list.add(CourseName);
			}
			if(count==0)
			{
				list.add("No Results Found...");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return list;
}

	public ArrayList<String> getlocality(DataSource dataSource, String term) {
		ArrayList<String> list = new ArrayList<String>();
		PreparedStatement pstmt = null;
		Connection con=null;
		String sql=null;
		int count=0;
		String areaname;
		ResultSet rs=null;
		System.out.println("productList="+term);
		
		try {
			con=dataSource.getConnection();
			
				sql="select * from areamaster where AREANAME like ? and ACTIVE=1";
				pstmt=con.prepareStatement(sql);
				pstmt.setString(1, "%"+term +"%");
			
			
			System.out.println("term ="+term);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
			count++;
			areaname=rs.getString("AREANAME");
				
				
				
				list.add(areaname);
			}
			if(count==0)
			{
				list.add("No Results Found...");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return list;
}

	public ArrayList<String> getcity(DataSource dataSource, String term) {
		// TODO Auto-generated method stub
		ArrayList<String> list = new ArrayList<String>();
		PreparedStatement pstmt = null;
		Connection con=null;
		String sql=null;
		int count=0;
		String city;
		ResultSet rs=null;
		System.out.println("productList="+term);
		
		try {
			con=dataSource.getConnection();
			
				sql="select * from citymaster where CITY like ? and ACTIVE=1";
				pstmt=con.prepareStatement(sql);
				pstmt.setString(1, "%"+term +"%");
			
			
			System.out.println("term ="+term);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
			count++;
			city=rs.getString("CITY");
				
				
				
				list.add(city);
			}
			if(count==0)
			{
				list.add("No Results Found...");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return list;
		}

	public List<DynamicDataForm> institutelist(DynamicDataForm dynaform,
			DataSource dataSource) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();

			sql = "select distinct CompanyId,CompanyName from companymaster where Active=1";

			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setCompname(rs.getString("CompanyName"));
				roleForm.setCompanyid(rs.getInt("CompanyId"));
						list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
}

	public List<DynamicDataForm> localitylist(DynamicDataForm dynaform,
			DataSource dataSource) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();

			sql = "select AREAID,AREANAME from areamaster where ACTIVE=1";

			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setAreaids(rs.getInt("AREAID"));
				roleForm.setAreaname(rs.getString("AREANAME"));
						list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
}

	public List<DynamicDataForm> citylist(DynamicDataForm dynaform,
			DataSource dataSource) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();

			sql = "select CITYID,CITY from citymaster where ACTIVE=1";

			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setCityids(rs.getInt("CITYID"));
				roleForm.setCityname(rs.getString("CITY"));
						list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
}

	public List<DynamicDataForm> courselists(DynamicDataForm dynaform,
			DataSource dataSource) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();

			sql = "select CourseId,CourseName from coursemater where Active=1";

			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setCoursesid(rs.getInt("CourseId"));
				roleForm.setCourseName(rs.getString("CourseName"));
						list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
}

	public JSONArray getcityindex(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response, int city) {
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
       JSONObject obj=null;

		
		try {
			con=dataSource.getConnection();      
			String sql="select CITY from citymaster where CITYID=?";

			pstmt=con.prepareStatement(sql);
			pstmt.setInt(1,city);
			rs=pstmt.executeQuery();	
			while(rs.next()){
					obj=new JSONObject(); 
				obj.put("CITY", rs.getString("CITY"));
				
					jArray.add(obj);
				}
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;

}

	public JSONArray getinstitute(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String institute) {
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
       JSONObject obj=null;

		
		try {
			con=dataSource.getConnection();      
			String sql="select distinct CompanyName from companymaster where CompanyId="+institute+"";

			pstmt=con.prepareStatement(sql);
			rs=pstmt.executeQuery();	
			while(rs.next()){
					obj=new JSONObject(); 
				obj.put("CompanyName", rs.getString("CompanyName"));
				
					jArray.add(obj);
				}
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;
	}

	public JSONArray getcityurl(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String cityid) {
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
       JSONObject obj=null;

		
		try {
			con=dataSource.getConnection();      
			String sql="select CITY from citymaster where CITYID="+cityid+"";

			pstmt=con.prepareStatement(sql);
			rs=pstmt.executeQuery();	
			while(rs.next()){
					obj=new JSONObject(); 
				obj.put("CITY", rs.getString("CITY"));
				
					jArray.add(obj);
				}
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;
	}

	public JSONArray getlocalityurl(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String locality) {
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
       JSONObject obj=null;

		
		try {
			con=dataSource.getConnection();      
			String sql="select distinct  AREANAME from areamaster where AREAID="+locality+"";

			pstmt=con.prepareStatement(sql);
			rs=pstmt.executeQuery();	
			while(rs.next()){
					obj=new JSONObject(); 
				obj.put("AREANAME", rs.getString("AREANAME"));
				
					jArray.add(obj);
				}
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;
	}

	public JSONArray getcategoryurl(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String catname) {
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
       JSONObject obj=null;

		
		try {
			con=dataSource.getConnection();      
			String sql="select coursecatname from coursecategorymaster where coursecatId="+catname+"";

			pstmt=con.prepareStatement(sql);
			rs=pstmt.executeQuery();	
			while(rs.next()){
					obj=new JSONObject(); 
				obj.put("coursecatname", rs.getString("coursecatname"));
				
					jArray.add(obj);
				}
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;
	}

	public JSONArray getcourse(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String coursename) {
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
       JSONObject obj=null;

		
		try {
			con=dataSource.getConnection();      
			String sql="select distinct CourseName from coursemater where CourseId="+coursename+"";

			pstmt=con.prepareStatement(sql);
			rs=pstmt.executeQuery();	
			while(rs.next()){
					obj=new JSONObject(); 
				obj.put("CourseName", rs.getString("CourseName"));
				
					jArray.add(obj);
				}
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;
	}

	public List<DynamicDataForm> xmllist(DataSource dataSource) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			
			con = dataSource.getConnection();

			sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where coursemater.active=1 and coursemater.coursecatId=?  group by coursemater.courseid order by coursemater.Fees desc";

			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setCoursenamelatest(rs
						.getString("coursemater.CourseName"));
				System.out.println("coursenameee"+(rs.getString("coursemater.CourseName")));
				roleForm.setCompname(rs.getString("companymaster.companyname"));
				roleForm.setCoursesid(rs.getInt("CourseId"));
				roleForm.setCatid(rs.getInt("coursecatId"));
			     roleForm.setCourselatestlogo(rs
						.getString("coursemater.imagename"));
				roleForm.setFees(rs.getInt("coursemater.Fees"));
				roleForm.setCityname(rs.getString("citymaster.city"));
				String rating = rs.getString("rate");
				if (rating.equals("0.0000")) {
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("1.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-1.png");
				}
				if (rating.equals("2.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-2.png");
				}
				if (rating.equals("3.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-3.png");
				}
				if (rating.equals("4.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-4.png");
				}
				if (rating.equals("5.0000")) {
					System.out.println("yes");
					roleForm.setRating("images/smallrating-5.png");
				}

				System.out.println("vani " + rating);
				list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}

	public List<DynamicDataForm> sitemaplist(DataSource dataSource) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			 Element urlset = new Element("urlset");
				Document doc = new Document(urlset);
				doc.setRootElement(urlset);
				con = dataSource.getConnection();

			sql = "SELECT coursemater.CourseName,coursemater.CourseId,coursecategorymaster.coursecatname,areamaster.AREANAME,coursemater.coursecatId,coursemater.Fees,coursemater.imagename,companymaster.companyname,citymaster.city, avg(ifnull(rating,0)) as rate  FROM coursemater "
					+ " LEFT JOIN coursecategorymaster ON "
					+ " coursemater.coursecatId=coursecategorymaster.coursecatId "
					+

					" LEFT JOIN companymaster ON "
					+ " coursecategorymaster.CompanyId=companymaster.CompanyId "
					+ " LEFT JOIN citymaster ON "
					+ " citymaster.cityid=companymaster.cityid "
					+ " LEFT JOIN areamaster ON "
					+ " coursemater.areaid=areamaster.AREAID "
				
					+
					

					" LEFT JOIN ratingtable ON "
					+ " coursemater.courseid=ratingtable.courseid " +

					" where coursemater.active=1   group by coursemater.courseid";

			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
			String course=rs.getString("coursemater.CourseName");
			String city=rs.getString("citymaster.city");
			String categoryname=rs.getString("coursecatname");
			String institutename=rs.getString("companyname");
			String locationname=rs.getString("AREANAME");


				Element url = new Element("url");
				Element url1=new Element("url");
				Element url2=new Element("url");
				Element url3=new Element("url");
				Element url4=new Element("url");
				Element url5=new Element("url");
				Element url6=new Element("url");
				Element url7=new Element("url");
				Element url8=new Element("url");
				Element url9=new Element("url");
				Element url10=new Element("url");
				Element url11=new Element("url");
                url.addContent(new Element("loc").setText("http://www.learnezy.in/"+course.toLowerCase()+"-courses-in-"+city.toLowerCase()+""));
                url.addContent(new Element("priority").setText(".5"));
				url.addContent(new Element("changefreq").setText("daily"));
				url1.addContent(new Element("loc").setText("http://www.learnezy.in/"+institutename.toLowerCase()+"-courses-in-"+categoryname.toLowerCase()+""));
				url1.addContent(new Element("priority").setText(".5"));
				url1.addContent(new Element("changefreq").setText("daily"));
			
				url2.addContent(new Element("loc").setText("http://www.learnezy.in/courses-in-"+institutename.toLowerCase()+"-"+city.toLowerCase()+"-"+locationname.toLowerCase()+""));
				url2.addContent(new Element("priority").setText(".5"));
				url2.addContent(new Element("changefreq").setText("daily"));
			
				url3.addContent(new Element("loc").setText("http://www.learnezy.in/"+categoryname.toLowerCase()+"-courses-in-"+institutename.toLowerCase()+"-"+city.toLowerCase()+""));
				url3.addContent(new Element("priority").setText(".5"));
				url3.addContent(new Element("changefreq").setText("daily"));
			
				url4.addContent(new Element("loc").setText("http://www.learnezy.in/"+course.toLowerCase()+"-courses-in-"+institutename.toLowerCase()+""));
				url4.addContent(new Element("priority").setText(".5"));
				url4.addContent(new Element("changefreq").setText("daily"));
			
				url5.addContent(new Element("loc").setText("http://www.learnezy.in/"+course.toLowerCase()+"-courses-in-"+city.toLowerCase()+""));
				url5.addContent(new Element("priority").setText(".5"));
				url5.addContent(new Element("changefreq").setText("daily"));
			
				url6.addContent(new Element("loc").setText("http://www.learnezy.in/"+categoryname.toLowerCase()+"-courses-in-"+institutename.toLowerCase()+"-"+locationname.toLowerCase()+"-"+city.toLowerCase()+""));
				url6.addContent(new Element("priority").setText(".5"));
				url6.addContent(new Element("changefreq").setText("daily"));
			
				url7.addContent(new Element("loc").setText("http://www.learnezy.in/"+course.toLowerCase()+"-courses-in-"+locationname.toLowerCase()+"-"+city.toLowerCase()+""));
				url7.addContent(new Element("priority").setText(".5"));
				url7.addContent(new Element("changefreq").setText("daily"));
			
				url8.addContent(new Element("loc").setText("http://www.learnezy.in/online-"+course.toLowerCase()+"-courses-in-"+city.toLowerCase()+""));
				url8.addContent(new Element("priority").setText(".5"));
				url8.addContent(new Element("changefreq").setText("daily"));
			
				url9.addContent(new Element("loc").setText("http://www.learnezy.in/classroom-"+course.toLowerCase()+"-courses-in-"+city.toLowerCase()+""));
				url9.addContent(new Element("priority").setText(".5"));
				url9.addContent(new Element("changefreq").setText("daily"));
			
				url10.addContent(new Element("loc").setText("http://www.learnezy.in/online-"+course.toLowerCase()+"-courses-in-"+locationname.toLowerCase()+"-"+city.toLowerCase()+""));
				url10.addContent(new Element("priority").setText(".5"));
				url10.addContent(new Element("changefreq").setText("daily"));
			
				
				url11.addContent(new Element("loc").setText("http://www.learnezy.in/classroom-"+course.toLowerCase()+"-courses-in-"+locationname.toLowerCase()+"-"+city.toLowerCase()+""));
				url11.addContent(new Element("priority").setText(".5"));
				url11.addContent(new Element("changefreq").setText("daily"));
				
				doc.getRootElement().addContent(url);
				doc.getRootElement().addContent(url1);
				doc.getRootElement().addContent(url2);
				doc.getRootElement().addContent(url3);
				doc.getRootElement().addContent(url4);
				doc.getRootElement().addContent(url5);
				doc.getRootElement().addContent(url6);
				doc.getRootElement().addContent(url7);
				doc.getRootElement().addContent(url8);
				doc.getRootElement().addContent(url9);
				doc.getRootElement().addContent(url10);
				doc.getRootElement().addContent(url11);

                // new XMLOutputter().output(doc, System.out);
				XMLOutputter xmlOutput = new XMLOutputter();

				// display nice nice
				xmlOutput.setFormat(Format.getPrettyFormat());
				//xmlOutput.output(doc, new FileWriter("c:\\file.xml"));

				xmlOutput.output(doc, new FileWriter("E:/Learnezy21.4.2017/Learnezy/WebContent/sitemap.xml"));
				System.out.println("File updated!");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public JSONArray subscribeMail(String fromdate, DataSource dataSource) {
		System.out.println("Inside DAOOOOOOOOOOOO+++++++++++++++++++++");
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		JSONArray jArray= new JSONArray();
	       JSONObject obj=null;
		try {
			obj=new JSONObject(); 

			con = dataSource.getConnection();
			String sql = "SELECT emialid FROM subscribe WHERE emialid=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, fromdate);
			rs = pstmt.executeQuery();
			if (rs.next())
				
				
			{	
				result = Common.DUPLICATE_EMAIL_MESSAGE;
		System.out.println(result);
			obj.put("result",result);
			jArray.add(obj);
		     }
			else {
				if (!fromdate.equalsIgnoreCase("")
						&& !fromdate.equalsIgnoreCase(null)) {
					sql = "INSERT INTO subscribe (emialid,ACTIVE,EnquiryType) VALUES (?,?,?)";
					if (pstmt != null)
						pstmt.close();
					pstmt = con.prepareStatement(sql);

					pstmt.setString(1, fromdate);
					pstmt.setBoolean(2, true);
					pstmt.setString(3, "Subscribe");
					pstmt.executeUpdate();
					if (rs != null)
						rs.close();
					result = Common.REC_ADDED;
					System.out.println(result);
					obj.put("result",result);
					jArray.add(obj);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return jArray;
	
	}
	
	
	public JSONArray addtocart(String cartid, DataSource dataSource) {
	
		JSONArray jArray = new JSONArray();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();
			sql = "select * from coursemater WHERE active=1 and CourseId="+cartid+"";// select query
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				JSONObject obj = new JSONObject();
				obj.put("CourseName", rs.getString("CourseName"));
				obj.put("CourseID", rs.getString("CourseId"));
				obj.put("CourseCatID", rs.getString("coursecatId"));
				obj.put("CoursePrice", rs.getString("Fees"));
				obj.put("CourseImage", rs.getString("imagename"));
				
				
				jArray.add(obj);
				System.out.println(sql);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;
	
	}

	public List<DynamicDataForm> cartlist(String cartid, DataSource dataSource) {
		List<DynamicDataForm> list = new ArrayList<DynamicDataForm>();
		DynamicDataForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		System.out.println("Inside DAO ******************************");
		System.out.println("Value of array ******************************"+cartid);

		
		try {
			con = dataSource.getConnection();

//sql = "SELECT cat.*,course.* FROM `coursecategorymaster` cat JOIN `coursemater` course ON cat.coursecatId = course.coursecatId where Active=1 and coursecatname='"
//			+ id + "'";
			sql = "select CourseId,CourseName,Fees,imagename,coursecatId  from coursemater where Active=1 and coursecatId='"
					+ cartid + "'";
//coursecatId
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				roleForm = new DynamicDataForm();
				roleForm.setCourseidnew(rs.getInt("CourseId"));
				roleForm.setCoursenamenew(rs.getString("CourseName"));
				roleForm.setAmountnew(rs.getDouble("Fees"));
				roleForm.setImagename(rs.getString("imagename"));
				roleForm.setCoursecatid(rs.getInt("coursecatId"));
				
				System.out.println("category id_++++++++++++"+rs.getInt("coursecatId"));
				

				list.add(roleForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}
	

	
}