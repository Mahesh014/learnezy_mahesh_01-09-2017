package com.itech.elearning.daoimpl;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.sql.DataSource;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.elearning.forms.TestMasterForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;
import com.itech.elearning.utils.DateTimeUtil;

public class TestMasterDaoImpl {

	public List<TestMasterForm> listAllCourse(int roleid,String compid,Long userid, DataSource ds) {
		List<TestMasterForm> courseList = new ArrayList<TestMasterForm>();
		TestMasterForm topicForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			if(roleid == 9)
			{
	         sql = "SELECT * FROM coursemater WHERE ACTIVE=TRUE ";
			}
			else
			{
			 sql = "SELECT * FROM coursemater WHERE ACTIVE=TRUE and CompanyId= '"+compid+"'";
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				topicForm = new TestMasterForm();
				topicForm.setCourseId(rs.getLong("CourseId"));
				topicForm.setCourseName(rs.getString("CourseName"));

				courseList.add(topicForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return courseList;
	}

	public List<TestMasterForm> listAllTopic(Long userid, Long courseId,
			DataSource ds) {
		List<TestMasterForm> topicList = new ArrayList<TestMasterForm>();
		TestMasterForm topicForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			if (courseId > 0)
				sql = "SELECT * FROM topicmaster WHERE ACTIVE=TRUE and CourseId="
						+ courseId + "";
			else
				sql = "SELECT * FROM TOPICMASTER WHERE ACTIVE=TRUE";
			pstmt = con.prepareStatement(sql);

			rs = pstmt.executeQuery();
			while (rs.next()) {
				topicForm = new TestMasterForm();
				topicForm.setTopicId(rs.getLong("TopicID"));
				topicForm.setTopicName(rs.getString("TopicName"));

				topicList.add(topicForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return topicList;
	}

	public List<TestMasterForm> listAllTestType(int roleid,String compid,Long userid, DataSource ds) {
		List<TestMasterForm> testTypeList = new ArrayList<TestMasterForm>();
		TestMasterForm topicForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			if(roleid==9)
			{
		 sql = "SELECT * FROM testtypemaster WHERE ACTIVE=TRUE ";
			}
			else
			{
		 sql = "SELECT * FROM testtypemaster WHERE ACTIVE=TRUE and CompanyId= '"+compid+"'";
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				topicForm = new TestMasterForm();
				topicForm.setTestTypeId(rs.getLong("TestTypeId"));
				topicForm.setTestTypeName(rs.getString("TestType"));
				testTypeList.add(topicForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return testTypeList;
	}

	public String addTest(String compid,TestMasterForm testForm, DataSource dataSource, String courseId, 
		Long testTypeId, String tname, String tdate, String qIds, String testTime, String minScore, String achivementScore) {
		String result = Common.FAILURE_MESSAGE;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dataSource.getConnection();
			String sql1 = "SELECT Testname FROM testmaster WHERE Testname=? and CompanyId= '"+compid+"'";
			pstmt = con.prepareStatement(sql1);
			pstmt.setString(1, testForm.getTname());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				result = Common.DUPLICATE_NAME_MESSAGE;
			} else {
				String sql = "INSERT INTO testmaster (Testname,QuestionIds,CourseId,TestType,TestDate,TestTime,MinScore,ACTIVE,achivementScore,CompanyId) VALUES(?,?,?,?,?,?,?,?,?,?)";
				if (pstmt != null)	pstmt.close();
				pstmt = con.prepareStatement(sql);
				pstmt.setString(1,tname );
				pstmt.setString(2,qIds );
				pstmt.setString(3,courseId );
				pstmt.setLong(4,testTypeId );
				pstmt.setString(5, DateTimeUtil.formatSqlDate(tdate));
				pstmt.setString(6, testTime);
				pstmt.setString(7, minScore);
				pstmt.setBoolean(8, true);
				pstmt.setString(9, achivementScore);
				pstmt.setString(10,compid);
				
				pstmt.executeUpdate();
				result = Common.REC_ADDED;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	public String changeTestType(TestMasterForm testForm, DataSource dataSource) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = dataSource.getConnection();
			String sql = "UPDATE testmaster SET ACTIVE=? WHERE TestId=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setBoolean(1, testForm.isActive());
			pstmt.setLong(2, testForm.getTestId());
			pstmt.executeUpdate();
			result = Common.REC_STATUS_CHANGED;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TestMasterForm> listAllTest(int roleid ,String compid,Long userid, DataSource ds,
			Long topicId) {

		List<TestMasterForm> list = new ArrayList<TestMasterForm>();
		TestMasterForm testForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		PreparedStatement pstmt2 = null;
		ResultSet rs2 = null;
		
		String sql = null;
		try {
			con = ds.getConnection();
			if(roleid==9)
			{
			sql = "SELECT ttm.* , tm.* , cm.* FROM testmaster AS tm JOIN testtypemaster AS ttm ON (ttm.TESTTYPEID=tm.TESTTYPE)  JOIN coursemater AS cm ON (cm.COURSEID=tm.COURSEID) ";
		
			}
			else
				{
				sql = "SELECT ttm.* , tm.* , cm.* FROM testmaster AS tm JOIN testtypemaster AS ttm ON (ttm.TESTTYPEID=tm.TESTTYPE)  JOIN coursemater AS cm ON (cm.COURSEID=tm.COURSEID) and ttm.CompanyId= '"+compid+"'";
	
				
				}
				pstmt = con.prepareStatement(sql);
				
			rs = pstmt.executeQuery();
			while (rs.next()) {
				testForm = new TestMasterForm();
				
				testForm.setCourseId(rs.getLong("tm.CourseId"));
				testForm.setCourseName(rs.getString("cm.CourseName"));
				testForm.setTestTypeId(rs.getLong("ttm.TestTypeID"));
				testForm.setTestTypeName(rs.getString("ttm.TestType"));
				testForm.setTestId(rs.getLong("tm.TestId"));
				testForm.setTname(rs.getString("tm.Testname"));
				testForm.setTdate(rs.getString("tm.TestDate"));
				testForm.setTestTime(rs.getInt("tm.TestTime"));
				testForm.setMinScore(rs.getInt("tm.MinScore"));
				testForm.setAchivementScore(rs.getInt("tm.achivementScore"));
				testForm.setActive(rs.getBoolean("tm.active"));
								
				JSONObject objj= new JSONObject();
				objj.put("courseId",rs.getLong("tm.CourseId"));
				objj.put("testTypeID",rs.getLong("ttm.TestTypeID"));
				objj.put("testId",rs.getLong("tm.TestId"));
				objj.put("testname",rs.getString("tm.Testname"));
				objj.put("testDate",DateTimeUtil.formatStringDate(rs.getString("tm.TestDate")));
				objj.put("testTime",rs.getInt("tm.TestTime"));
				objj.put("minScore",rs.getInt("tm.MinScore"));
				objj.put("questionIds",rs.getString("tm.QuestionIds"));
				objj.put("achivementScore",rs.getString("tm.achivementScore"));
				
				
				String sql2 = "SELECT * FROM questionmaster  where QuestionId in ("+rs.getString("tm.QuestionIds")+")";
				pstmt2 = con.prepareStatement(sql2);
				rs2 = pstmt2.executeQuery();
				JSONArray jArray= new JSONArray();
				while (rs2.next()) {
					JSONObject obj= new JSONObject();
					obj.put("Qid", rs2.getInt("QuestionId"));
					obj.put("Question", rs2.getString("Question"));
					jArray.add(obj);
				}
				testForm.setJsonEdit(objj.toString());
				testForm.setJsonQIds(jArray.toString());
				list.add(testForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}

	public String updateTest(TestMasterForm testForm, DataSource dataSource, String courseId, 
			Long testTypeId, String tname, String tdate, String qIds, String testTime, String minScore, String achivementScore, String testId) {
			String result = Common.FAILURE_MESSAGE;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			Connection con = null;
			try {
				con = dataSource.getConnection();
				
					String sql = "UPDATE  testmaster SET Testname=?, QuestionIds=?, CourseId=?, TestType=?, TestDate=?, TestTime=?, MinScore=?, achivementScore=? WHERE TESTID=?";
					pstmt = con.prepareStatement(sql);
					pstmt.setString(1,tname );
					pstmt.setString(2,qIds );
					pstmt.setString(3,courseId );
					pstmt.setLong(4,testTypeId );
					pstmt.setString(5,DateTimeUtil.formatSqlDate(tdate));
					pstmt.setString(6,testTime);
					pstmt.setString(7,minScore);
					pstmt.setString(8,achivementScore);
					pstmt.setString(9,testId);
					pstmt.executeUpdate();
					result = Common.REC_UPDATED;
				
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					ConnectionUtil.closeResources(con, pstmt, rs);
				} catch (Exception e2) {
				}
			}
			return result;
		}
	
	public String testExcelUpload(String fileName, String filePath, String compid) {
		 String result="process Failed"; 
		  
		   int daoflag=0;
		   Connection con=null;
		   PreparedStatement pstmt=null;
		   
		   try {
			   
			    con=ConnectionUtil.getMySqlConnection();
	         	long start = System.currentTimeMillis();
	            
	         	
	         	String sql="insert into testmaster(Testname,CourseId,QuestionIds,TestType,TestDate,TestTime,MinScore,AchivementScore,CompanyId,Active) values(?,?,?,?,?,?,?,?,?,1)";
	              
	            if(pstmt!=null)pstmt.close();
	            pstmt=con.prepareStatement(sql);
	            
	         	InputStream input = new BufferedInputStream(new FileInputStream(filePath+fileName));
	         	
	         	 
	         	System.out.println("Excel File path="+filePath);
	         	System.out.println("Excel File Name="+fileName);
	            POIFSFileSystem fs = new POIFSFileSystem(input);
	            HSSFWorkbook wb = new HSSFWorkbook(fs);
	            HSSFSheet sheet = wb.getSheetAt(0);
	            Iterator<Row> rows = sheet.rowIterator();
	          
	            long count=0;
	            while (rows.hasNext()) {
	            		
	            		 HSSFRow row = (HSSFRow) rows.next();
	                     
	 
	                     String test_name=TestMasterDaoImpl.StringNotNull(row.getCell(0));
	                     String course_id=TestMasterDaoImpl.StringNotNull(row.getCell(1));
	                     String question_id=TestMasterDaoImpl.StringNotNull(row.getCell(2));
	                     String test_type=TestMasterDaoImpl.StringNotNull(row.getCell(3));
	                     String test_date=TestMasterDaoImpl.StringNotNull(row.getCell(4));
	                     String test_time=TestMasterDaoImpl.StringNotNull(row.getCell(5));
	                     String min_score=TestMasterDaoImpl.StringNotNull(row.getCell(6));
	                     String achivement_score=TestMasterDaoImpl.StringNotNull(row.getCell(7));
	                     String company_id=TestMasterDaoImpl.StringNotNull(row.getCell(8));
	                     
	                     
	                     
	                     try {
	                    	 if(daoflag==1){
	                      		count=count+1;
	                         	System.out.println("excel file adding............");
	                   	       
	                   	       	pstmt.setString(1,test_name);
	                   	        pstmt.setString(2,course_id);
	                   	        pstmt.setString(3,question_id);
	                   	        pstmt.setString(4,test_type);
	                   	        pstmt.setString(5,test_date);
	                   	        pstmt.setString(6,test_time);
	                   	        pstmt.setString(7,min_score);
	                   	        pstmt.setString(8,achivement_score);
	                   	        pstmt.setString(9,compid);
	                   	        
	                   	     
	     	                    pstmt.addBatch();
	     	                    
	     	                    
	     	                    if(count%1000 == 0) { 
	     	                    	pstmt.executeBatch();
	     	                    	System.out.println("Batch Count: "+count+ " Time Taken: "+(System.currentTimeMillis()-start)+" MilliSecons");
	     	                    }            
	     	                  //  flag=1;
	     	                    
	     	                    System.out.println("count="+count);
	     	                    result=count+" "+"Records uploaded";
	     	                    
	     	                    System.out.println("%%%%%%%%%%%%%"+result);
	     	                 }
	                 	} catch (Exception einner) {
	                 		einner.printStackTrace();
						}
	                      
	                      daoflag=1;
	            }
	            pstmt.executeBatch();
	            System.out.println("Batch Count: "+count+ " Time Taken: "+(System.currentTimeMillis()-start)+" MilliSecons");
	        
		   } catch (Exception e) {
	            e.printStackTrace();
	        }
	        finally{
	            ConnectionUtil.closeResources(con, pstmt);
	        }
	        
	        return result; 
			//return flag;
	    }    
	
	 public static String StringNotNull(HSSFCell rowValue){
		   String results="";
		   if(rowValue!=null) results =rowValue.toString().trim();
		   return results;
		   
	   }
	   
}
