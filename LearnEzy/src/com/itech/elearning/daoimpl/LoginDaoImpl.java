package com.itech.elearning.daoimpl;

import java.sql.Connection;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import com.itech.elearning.forms.LoginForm;
import com.itech.elearning.handler.CourseMasterHandler;
import com.itech.elearning.utils.ConnectionUtil;
import com.itech.elearning.utils.EncryptUtil;

public class LoginDaoImpl {
	CourseMasterHandler coursehandler=new CourseMasterHandler();
	public LoginForm login(String userName, String password, DataSource ds) {
		LoginForm loginform = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			
			con = ds.getConnection();
			String sql = "select * from usermaster um left join studentcoursedetails scd on scd.studentid=um.userid where binary um.emailid=? and um.password=? and um.Active=true";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, userName);
			pstmt.setString(2, EncryptUtil.encrypt(password));
			rs = pstmt.executeQuery();System.out.println(sql);
			while (rs.next()) {
				loginform = new LoginForm();
				loginform.setUserId(rs.getLong("userid"));
				loginform.setUserName(rs.getString("emailid"));
				loginform.setPassword(rs.getString("password"));
				loginform.setFirstname(rs.getString("firstname"));
				loginform.setLastname(rs.getString("lastname"));
				loginform.setProfilePhoto(rs.getString("profilePhoto"));
				loginform.setRole(rs.getInt("roleid"));
				loginform.setFullName(rs.getString("firstname")+"  "+rs.getString("lastname"));
				loginform.setCourseids(rs.getString("CourseId"));
				loginform.setCompanyId(rs.getString("CompanyId"));
				loginform.setEmailid(rs.getString("emailid"));
			}
		} catch (Exception e) {
			loginform = null;
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return loginform;
	}

	public LoginForm login1(String userName, DataSource ds) {
		
		LoginForm loginform = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			System.out.println(userName );
			con = ds.getConnection();
			String sql = "select * from usermaster where username=? and Active=true";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, userName);

			rs = pstmt.executeQuery();
			while (rs.next()) {
				loginform = new LoginForm();
				loginform.setUserId(rs.getLong("userid"));
				loginform.setUserName(rs.getString("username"));
				loginform.setPassword(rs.getString("password"));
				loginform.setFirstname(rs.getString("firstname"));
				loginform.setLastname(rs.getString("lastname"));
				loginform.setRole(rs.getInt("roleid"));

			}
		} catch (Exception e) {
			loginform = null;
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return loginform;
	}

	public List<LoginForm> testDetails(Long userId, DataSource dataSource) {
		List<LoginForm> list = new ArrayList<LoginForm>();
		LoginForm loginForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;


		try {
			con = dataSource.getConnection();
			String sql = "select tm.TestId, tm.Testname, tm.TopicId, tm.TestType, tm.TestDate, tm.Active, tom.TopicName, cm.CourseName, ttm.TestType FROM testmaster as tm join topicmaster as tom on (tm.TopicId = tom.TopicID) join coursemater as cm on (tom.CourseId = cm.CourseId) join testtypemaster as ttm on tm.TestType = ttm.TestTypeId join studentcoursemgmt as smt on smt.StudentID = "+userId+"  where tm.TestDate >=  '"+ new java.sql.Timestamp(System.currentTimeMillis()) +"' - INTERVAL 1 Day and tm.Active = true  ";
			System.out.println(sql);
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				loginForm = new LoginForm();
				loginForm.setTestType(rs.getString("ttm.TestType"));
				loginForm.setCourseName(rs.getString("cm.CourseName"));
				loginForm.setTestDate(new java.text.SimpleDateFormat("dd-MM-yyyy").format(rs.getTimestamp("tm.TestDate")));
				loginForm.setTopicName(rs.getString("tom.TopicName"));

				list.add(loginForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}

	public List<LoginForm> testTake(Long userId, DataSource dataSource) {

		List<LoginForm> list = new ArrayList<LoginForm>();
		LoginForm loginForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;


		try {
			con = dataSource.getConnection();
			String sql = "select tm.TestId, tm.Testname, tm.TopicId, tm.TestType, tm.TestDate, tm.Active , tom.TopicName, cm.CourseName , ttm.TestType, tm.TestType FROM testmaster as tm join topicmaster as tom on (tm.TopicId = tom.TopicID) join coursemater as cm on (tom.CourseId = cm.CourseId) join testtypemaster as ttm on tm.TestType = ttm.TestTypeId join studentcoursemgmt as smt on smt.StudentID = "+userId+"  where  tm.TestDate between  '"+ new java.text.SimpleDateFormat("dd-MM-yyyy").format(System.currentTimeMillis()) +"' and  '"+ new java.sql.Timestamp(System.currentTimeMillis()) +"' and tm.Active = true ";
			System.out.println(sql);
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				loginForm = new LoginForm();
				loginForm.setTestId(rs.getLong("tm.TestId"));
				loginForm.setTestType(rs.getString("ttm.TestType"));
				loginForm.setCourseName(rs.getString("cm.CourseName"));
				loginForm.setTestDate(new java.text.SimpleDateFormat("dd-MM-yyyy").format(rs.getTimestamp("tm.TestDate")));
				loginForm.setTopicName(rs.getString("tom.TopicName"));
				loginForm.setTestTypeId(rs.getInt("tm.TestType"));
				list.add(loginForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}

	public String LastID(DataSource dataSource) {
		

		String id=null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String sql="select * from topicmaster order by TopicID asc limit 1";
			System.out.println(sql);
			pstmt=con.prepareStatement(sql);
			rs=pstmt.executeQuery();
			while(rs.next())
			{

				id=rs.getString("TopicID");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}  finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return id;
	}

	public Object getCourseBasedOnIds(String courseIdsList,
			DataSource dataSource) {
		
		List<LoginForm> list = new ArrayList<LoginForm>();
		LoginForm loginForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;


		try {


			con = dataSource.getConnection();
			String sql = "select CourseId,CourseName FROM coursemater where CourseId in ("+courseIdsList+");";
			System.out.println(sql);
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				loginForm = new LoginForm();

				loginForm.setCourseid(rs.getInt("CourseId"));
				loginForm.setCoursename(rs.getString("CourseName"));

				list.add(loginForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}

	public List<LoginForm> notificationsOnDashBoard(Long userId,
			DataSource dataSource) {
		List<LoginForm> list = new ArrayList<LoginForm>();
		LoginForm loginForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;


		try {
			int total=0;
			int member=0;
			con = dataSource.getConnection();
			String sql = "select userid as membersJoined from usermaster where registerddate=now()";
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			loginForm = new LoginForm();
			while(rs.next()) {
				System.out.println("memner");	
				member++;

			}

			if(member!=0){
				loginForm.setNotifications(member+" new members joined today");
				loginForm.setNotificationfinder("1");
				list.add(loginForm);	
				total++;

			}


			if(pstmt!=null);
			pstmt.close();
			if(rs!=null);
			rs.close();

			sql = "select userid as changeUserNameOrPassword from usermaster where updatedDate=now()";


			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				System.out.println("pass");
				loginForm = new LoginForm();
				//loginForm.setTotalmemberjoined(rs.getInt("membersJoined")+" new members joined");
				loginForm.setNotifications("username or password changed ");
				loginForm.setNotificationfinder("2");
				total++;
				list.add(loginForm);	

			}

			System.out.println("counrfggv  "+total+"   hh  "+member);


		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}


	public List<LoginForm> getCourseProgress(Long userId,DataSource dataSource, String courseIdsList) {
		List<LoginForm> list = new ArrayList<LoginForm>();
		LoginForm loginForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql=null;
		try {
			con = dataSource.getConnection();
			String arry[]=courseIdsList.split(",");
			for(int i=0;i<arry.length;i++){
				sql = "SELECT cm.CourseName, (select (select ((100 * ta.Score/ta.totalquestion) > pp.PassPercent)  FROM passpercentage pp where courseId= ta.CourseID) as IsPassed FROM testanswerscore ta where ta.topicId=tm.TopicID and ta.CourseID = tm.CourseId and ta.StudentId = "+userId+" order by ta.UpdatedDate desc limit 1) IsPassed from topicmaster tm  inner join coursemater cm on cm.CourseId=tm.CourseId where tm.courseId="+arry[i]+"  order by tm.TopicID";
				pstmt = con.prepareStatement(sql);
				rs = pstmt.executeQuery();
				loginForm = new LoginForm();
				int completedTopics=0;
				int totalTopics=0;
				String CourseName=null;
				while (rs.next()) {
					if(rs.getInt("IsPassed")==1){
						completedTopics = completedTopics+1;
					}		totalTopics++;
					CourseName=rs.getString("cm.CourseName");
				}
				if(totalTopics==0 && completedTopics==0 && CourseName==null ){
					String theCourseName=coursehandler.getCourseNameByID(Long.parseLong(arry[i]),dataSource);
					if(theCourseName!=null){
						loginForm.setCourseNameForProgress(theCourseName);
						loginForm.setCourseProgressPercent(0);
					}
				}else{
					int  d1=  (completedTopics * 100 / totalTopics);
					loginForm.setCourseNameForProgress(CourseName);
					loginForm.setCourseProgressPercent(d1);
				}
		}
	} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return list;
	}


	public List<LoginForm> getTheNumberOfCourseCompleted(Long userId,
			DataSource dataSource, String courseIdsList) {
		



		return null;
	}

	public int checkForCourseAndPayment(long userId, DataSource dataSource) {
		int flag=0;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String sql = "select * from studentcoursedetails where StudentId=? ";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, userId);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				flag=1;
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return flag;
	}
	public int checkForPayment(long userId, DataSource dataSource) {
		int flag=0;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String sql = "select * from usermaster where userid=? ";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, userId);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				flag=rs.getInt("paymentStatus");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return flag;
	}

}
