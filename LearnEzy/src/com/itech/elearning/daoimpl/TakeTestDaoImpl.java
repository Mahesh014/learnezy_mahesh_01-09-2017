package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.json.simple.JSONObject;

import com.itech.elearning.forms.TestMasterForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class TakeTestDaoImpl {

	public List<TestMasterForm> listTestByCourseId(String compid, Long userid,	DataSource dataSource, String courseId) {
		List<TestMasterForm> list = new ArrayList<TestMasterForm>();
		TestMasterForm testForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();
			sql = "SELECT * FROM testmaster where COURSEID=? and active=1 and CompanyId = '"+compid+"'";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, courseId);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				testForm = new TestMasterForm();
				testForm.setTname(rs.getString("Testname"));
				testForm.setCourseId(rs.getLong("COURSEID"));
				testForm.setTestId(rs.getLong("TestId"));
				list.add(testForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}

	public List<TestMasterForm> takeTest(String compid, Long userid, DataSource dataSource, String courseId, String testId) {
		List<TestMasterForm> list = new ArrayList<TestMasterForm>();
		TestMasterForm testForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();
			sql = "SELECT * FROM testmaster where COURSEID=? and testId=? and CompanyId = '"+compid+"'";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, courseId);
			pstmt.setString(2, testId);
			rs = pstmt.executeQuery();
			System.out.println(sql);
			while (rs.next()) {
				testForm = new TestMasterForm();
				testForm.setTname(rs.getString("Testname"));
				testForm.setQuestionIds(rs.getString("QuestionIds"));
				testForm.setTestId(rs.getLong("TestId"));
				testForm.setCourseId(rs.getLong("COURSEID"));
				testForm.setTestTime(rs.getInt("TestTime"));
				testForm.setMinScore(rs.getInt("MinScore"));
				list.add(testForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public JSONObject getQuestion(DataSource dataSource, int questionId) {
		JSONObject obj = new JSONObject();
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		Connection con=null;
		try{
			
			con = dataSource.getConnection();
			String sql = "select * from questionmaster where QuestionId=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, questionId);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				obj.put("questionId",rs.getString("questionId"));
				obj.put("Question",rs.getString("Question"));
				obj.put("AnswerA",rs.getString("AnswerA"));
				obj.put("AnswerB",rs.getString("AnswerB"));
				obj.put("AnswerC",rs.getString("AnswerC"));
				obj.put("AnswerD",rs.getString("AnswerD"));
				obj.put("CorrectAnswer",rs.getString("CorrectAnswer"));
				obj.put("DescriptiveAns",rs.getString("DescriptiveAns"));
				obj.put("qFlag",1);
			}else{
				
				obj.put("qFlag",0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}	
		finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
			return obj;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public JSONObject insertTest(String compid, DataSource dataSource,	HashMap<Integer, String> answerQuestion, Long courseId, Long testId, Long userId) {
			JSONObject jObj=new JSONObject();
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			Connection con = null;
			int score=0;
			int rightAnswer=0;
			int rounghAnswer=0;
			boolean mapFlag=false;
			int obtianPercentage=0;
			String passStatus=Common.FAILURE_MESSAGE;
			try {
					con = dataSource.getConnection();
					 
					String qIds=answerQuestion.keySet().toString();
					String answers=answerQuestion.values().toString();
					
					 for(Map.Entry m:answerQuestion.entrySet()){  
						  
						   int questionId=(Integer) m.getKey();
						   String ans=m.getValue().toString();
						   
						   String sql = "SELECT IF(CorrectAnswer=?,1,0) as ans  From questionmaster where QuestionId=? and CompanyId = '"+compid+"'";
						   if (pstmt != null)	pstmt.close();
						   pstmt = con.prepareStatement(sql);
						   pstmt.setString(1,ans );
						   pstmt.setLong(2,questionId );
						   rs=pstmt.executeQuery();
						   if (rs.next()) {
							  score=rs.getInt("ans");
						   }
						   
						   if (score==1) {
							   rightAnswer=rightAnswer+1;
						   }else{
							   rounghAnswer=rounghAnswer+1;   
						   }
						   mapFlag=true;
						}
					   
					 if (mapFlag) {
						   int totalQuestion=rightAnswer+rounghAnswer;
						   jObj.put("rightAnswer",rightAnswer);
						   jObj.put("rounghAnswer",rounghAnswer);
						   jObj.put("totalQuestion",totalQuestion);
						   jObj.put("resultFlag",1);
						   
						   if(totalQuestion!=0) {
							   obtianPercentage=(int)(((double)rightAnswer/(double)totalQuestion)*100); 
							   System.out.println("Obtain: "+obtianPercentage);
							   String sql1 = "SELECT * FROM TESTMASTER WHERE TESTID=? and CompanyId= '"+compid+"'";
							   if (pstmt != null)	pstmt.close();
							   pstmt = con.prepareStatement(sql1);
							   pstmt.setLong(1,testId );
							   rs=pstmt.executeQuery();
							   if (rs.next()) {
								   if (obtianPercentage<rs.getDouble("MinScore")) {
									   passStatus="FAIL";
								   }else{
									   passStatus="Pass";
								   }
								
							   }
						   }
						   String sql1 = "INSERT INTO testanswer (TestId,QuestionId,Answer,Score,StudentId,passPercentage,passStatus,testTakenDate,CompanyId) VALUES(?,?,?,?,?,?,?,CURDATE(),?)";
						   if (pstmt != null)	pstmt.close();
						   pstmt = con.prepareStatement(sql1);
						   pstmt.setLong(1,testId );
						   pstmt.setString(2,qIds.substring(1, qIds.length()-1));
						   pstmt.setString(3,answers.substring(1, answers.length()-1));
						   pstmt.setLong(4,rightAnswer);
						   pstmt.setLong(5, userId);
						   pstmt.setDouble(6,obtianPercentage);
						   pstmt.setString(7, passStatus);
						   pstmt.setString(8,compid);
						   pstmt.executeUpdate();
					   }else{
						   
						   jObj.put("resultFlag",0);
					   }
					 
					 
					 /*String sql1 = "SELECT count(*) as rounghAnswer, "
					   				 + "(SELECT count(*) From testanswer where TestId='"+testId+"' and StudentId='"+userId+"' and testTakenDate=curDate() and Score=1) as rightAnswer "
					   				 + "From testanswer where TestId='"+testId+"' and StudentId='"+userId+"' and testTakenDate=curDate() and Score=0";
					   if (pstmt != null)	pstmt.close();
					   pstmt = con.prepareStatement(sql1);
					   System.out.println(sql1);
					   rs=pstmt.executeQuery();
					   if (rs.next()) {
						   jObj.put("rightAnswer",rs.getInt("rightAnswer"));
						   jObj.put("rounghAnswer",rs.getInt("rounghAnswer"));
						   jObj.put("totalQuestion",rs.getInt("rightAnswer")+rs.getInt("rounghAnswer"));
						   jObj.put("resultFlag",1);
					   }else{
						   
						   jObj.put("resultFlag",0);
					   }*/
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					ConnectionUtil.closeResources(con, pstmt, rs);
				} catch (Exception e2) {
				}
			}
			return jObj;
	}
	 

}
