package com.itech.elearning.daoimpl;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;

import javax.sql.DataSource;

import com.itech.elearning.forms.Forgotpassword1Form;
import com.itech.elearning.utils.ConnectionUtil;
import com.itech.elearning.utils.MailSendingUtil;
import com.itech.elearning.utils.Mailsending;


public class Forgotpassword1Dao {
	private Properties configProp = new Properties();
	String filename = "ApplicationResources.properties";
	

	public Forgotpassword1Form GetUsernamePassword(
			Forgotpassword1Form forgotPasswordForm, DataSource dataSource,String mail) {
		String pass = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String password="";
		String email="";
		System.out.println("inside Forgotpassword1Dao  ");
		try{
			con = dataSource.getConnection();
			System.out.println("hi once again");
			
			if(forgotPasswordForm.getEmailid()!= null){
				String sql = "SELECT * FROM homepageregistration WHERE emailid=?"; //Query to check email id in user master
				pstmt=con.prepareStatement(sql);
				System.out.println(sql);
				pstmt.setString(1, forgotPasswordForm.getEmailid());
				System.out.println("+++++++++"+forgotPasswordForm.getEmailid());
				rs=pstmt.executeQuery();
				while(rs.next()){
					forgotPasswordForm = new Forgotpassword1Form();
					forgotPasswordForm.setEmailid(rs.getString("emailid")); //Setting email id
					//pass = rs.getString("password"); //Decrypting password
					email=rs.getString("emailid");
					forgotPasswordForm.setPassword(rs.getString("password")); //Setting password
					password=rs.getString("password");
					System.out.println("password"+password);
					//forgotPasswordForm.setUsername(rs.getString("emailId")); //Setting username
					Mailsending.add(mail, password);
					
				}
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
		finally{
			ConnectionUtil.closeResources(con, pstmt,rs);
		}
		return forgotPasswordForm;
	}

}
