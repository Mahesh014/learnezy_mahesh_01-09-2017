package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.json.simple.JSONObject;

import com.itech.elearning.forms.KBMasterForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class KBMasterDaoImpl {



	public List<KBMasterForm> listAllCourse(int roleid,String CompanyId, long userid, DataSource ds) {
		List<KBMasterForm> courseList = new ArrayList<KBMasterForm>();
		KBMasterForm kbForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			if(roleid==9)
			{
			 sql = "SELECT * FROM coursemater WHERE ACTIVE=TRUE";
			}
			else
			{
				sql = "SELECT * FROM coursemater WHERE ACTIVE=TRUE and CompanyId="+CompanyId;
			
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				kbForm = new KBMasterForm();
				kbForm.setCourseId(rs.getLong("CourseId"));
				kbForm.setCourseName(rs.getString("CourseName"));
				courseList.add(kbForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return courseList;
	}


	 @SuppressWarnings("unchecked")
	public List<KBMasterForm> list(int roleid,String CompanyId, long userid, DataSource ds, long courseId) {
		List<KBMasterForm> list = new ArrayList<KBMasterForm>();
		KBMasterForm kbForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		String sql = null;
		ResultSet rs = null;
		try {
			con = ds.getConnection();
			if(roleid==9)
			{
			sql = "select  * from kbmaster km  inner join coursemater cm on cm.courseId=km.courseId ";  
			System.out.println(courseId);
			if (courseId != -1 && courseId != 0) {
				sql = "select  * from kbmaster km  inner join coursemater cm on cm.courseId=km.courseId where km.courseId="+courseId+" ";
			}
			}
			else
			{
				sql = "select  * from kbmaster km  inner join coursemater cm on cm.courseId=km.courseId and km.CompanyId= '"+CompanyId+"'";  
				System.out.println(courseId);
				if (courseId != -1 && courseId != 0) {
					sql = "select  * from kbmaster km  inner join coursemater cm on cm.courseId=km.courseId where km.courseId="+courseId+" and km.CompanyId='"+CompanyId+"' ";
				}
			}

			System.out.println("sql "+sql);
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				kbForm = new KBMasterForm();
				kbForm.setKbId(rs.getLong("KBID"));
				kbForm.setQuestion(rs.getString("KBQuestion"));
				kbForm.setAnswer(rs.getString("KBAnswer"));
				kbForm.setCourseId(rs.getLong("CourseId"));
				kbForm.setCourseName(rs.getString("CourseName"));
				kbForm.setActive(rs.getBoolean("active"));
				
				JSONObject obj=new JSONObject();
				obj.put("kbId",rs.getLong("KBID"));
				obj.put("question",rs.getString("KBQuestion"));
				obj.put("answer",rs.getString("KBAnswer"));
				obj.put("courseId",rs.getLong("CourseId"));
				kbForm.setjEdit(obj.toString());
				list.add(kbForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 

		return list;
	}

	public String add(String CompanyId, KBMasterForm kbForm, DataSource dataSource) {
		String result = Common.FAILURE_MESSAGE;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;

		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM kbmaster WHERE KBQuestion=? AND KBAnswer=?  AND CourseId=? and CompanyId = '"+CompanyId+"' ";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, kbForm.getQuestion());
			pstmt.setString(2, kbForm.getAnswer());
			pstmt.setLong(3, kbForm.getCourseId());
			rs = pstmt.executeQuery();
 System.out.println(sql);
			if (rs.next()) {
				result = Common.DUPLICATE_NAME_MESSAGE;
			} else {
				sql = "INSERT INTO kbmaster (KBQuestion, KBAnswer, CourseId, Active,CompanyId) VALUES(?,?,?,?,?)";
				if (pstmt != null)	pstmt.close();
				pstmt = con.prepareStatement(sql);
				pstmt.setString(1, kbForm.getQuestion());
				pstmt.setString(2, kbForm.getAnswer());
				pstmt.setLong(3, kbForm.getCourseId());
				pstmt.setBoolean(4, true);
				pstmt.setString(5,CompanyId);
				pstmt.executeUpdate();
				result = Common.REC_ADDED;
				 System.out.println(sql);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;
	}


	public String changeStatus(DataSource ds,
			long kbId, boolean active) {

		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = ds.getConnection();
			String sql = "UPDATE kbmaster SET ACTIVE=? WHERE KBID=?";
			System.out.println(sql);
			pstmt = con.prepareStatement(sql);
			pstmt.setBoolean(1, active);
			pstmt.setLong(2, kbId);
			pstmt.executeUpdate();
			result = Common.REC_STATUS_CHANGED;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	public String updateTest(KBMasterForm kbForm, DataSource ds) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			String sql1 = "SELECT * FROM kbmaster WHERE KBQuestion=? AND KBAnswer=?  AND CourseId=?";
			pstmt = con.prepareStatement(sql1);
			pstmt.setString(1, kbForm.getQuestion());
			pstmt.setString(2, kbForm.getAnswer());
			pstmt.setLong(3, kbForm.getCourseId());
			rs = pstmt.executeQuery();
			if (rs.next()) {result = Common.No_Change;	}
			else{
			sql = "UPDATE kbmaster  SET KBQuestion  =?,KBAnswer = ?,CourseId = ?  where KBID = ?";
					if (rs != null)	rs.close();
					if (pstmt != null) pstmt.close();
					pstmt = con.prepareStatement(sql);
					pstmt.setString(1, kbForm.getQuestion());
					pstmt.setString(2, kbForm.getAnswer());
					pstmt.setLong(3, kbForm.getCourseId());
					pstmt.setLong(4, kbForm.getKbId());
					pstmt.executeUpdate();
					result = Common.REC_UPDATED;
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;

	}


	 

}
