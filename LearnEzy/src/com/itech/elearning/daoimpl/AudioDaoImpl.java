package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import org.json.simple.JSONObject;

import com.itech.elearning.forms.AudioForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class AudioDaoImpl {

	@SuppressWarnings("unchecked")
	public List<AudioForm> list(int roleid,String compid, DataSource dataSource) {
		List<AudioForm> list = new ArrayList<AudioForm>();
		AudioForm paymentForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		
		try {
			con = dataSource.getConnection();
			if(roleid==9)
			{
		 sql = "select * from audiofiles af inner join coursemater cm on cm.courseid=af.courseid ;";
			}
			else
			{
				 sql = "select * from audiofiles af inner join coursemater cm on cm.courseid=af.courseid where af.CompanyId= '"+compid+"';";
	
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				paymentForm = new AudioForm();
				paymentForm.setAudioId(rs.getInt("audioId"));
				paymentForm.setCourseId(rs.getInt("courseId"));
				paymentForm.setFileName(rs.getString("fileName"));
				paymentForm.setCourseName(rs.getString("courseName"));
				paymentForm.setActive(rs.getBoolean("ACTIVE"));

				JSONObject obj = new JSONObject();
				obj.put("audioId",rs.getInt("audioId"));
				obj.put("courseId",rs.getString("courseId"));
				obj.put("fileName",rs.getString("fileName"));
				paymentForm.setEditJson(obj.toJSONString());
				list.add(paymentForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return list;
	}

	public String add(String compid, AudioForm paymentForm, DataSource dataSource) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String sql = "select * from audiofiles where courseId=? AND fileName=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, paymentForm.getCourseId());
			pstmt.setString(2, paymentForm.getFileName());
			rs = pstmt.executeQuery();
			if (rs.next()) {
				result = Common.DUPLICATE_NAME_MESSAGE;
			} else {
					sql = "insert into audiofiles(courseId,fileName,active,CompanyId) values (?,?,?,?)";
					pstmt = con.prepareStatement(sql);
					pstmt.setInt(1, paymentForm.getCourseId());
					pstmt.setString(2, paymentForm.getFileName());
					pstmt.setBoolean(3, true);
					pstmt.setString(4,compid);
					pstmt.executeUpdate();
					result = Common.REC_ADDED;
			}
		} catch (Exception e) {

			e.printStackTrace();
		}

		finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return result;
	}

	public String changestatus(DataSource dataSource, boolean active, int audioId, String compid) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = dataSource.getConnection();
			String sql = "UPDATE audiofiles SET ACTIVE=? WHERE audioId=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setBoolean(1, active);
			pstmt.setInt(2, audioId);
			pstmt.executeUpdate();
			result = Common.REC_STATUS_CHANGED;
		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			ConnectionUtil.closeResources(con, pstmt);
		}
		return result;
	}

	public String update(String compid, AudioForm paymentForm, DataSource dataSource) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String sql = "select * from audiofiles where courseId=? AND fileName=?";
			System.out.println(sql);
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, paymentForm.getCourseId());
			pstmt.setString(2, paymentForm.getFileName());
			rs = pstmt.executeQuery();
			if (rs.next()) {
				result = Common.No_Change;
			} else {
				sql = "update audiofiles set courseId=?, fileName=? where audioId=?";
				pstmt = con.prepareStatement(sql);
				pstmt.setInt(1, paymentForm.getCourseId());
				pstmt.setString(2, paymentForm.getFileName());
				pstmt.setInt(3, paymentForm.getAudioId());
				pstmt.executeUpdate();
				result = Common.REC_UPDATED;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return result;
	}

	 


	


	 

}
