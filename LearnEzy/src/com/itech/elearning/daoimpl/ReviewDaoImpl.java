package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.itech.elearning.forms.ReviewForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class ReviewDaoImpl 
{

	public List<ReviewForm> list(Long userid, DataSource dataSource) 
	{
		List<ReviewForm> list = new ArrayList<ReviewForm>();
		ReviewForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
        String sql = null;
        String courseIdsList=null;
        try{		
    		con = dataSource.getConnection();
    		 sql = "select CourseId FROM studentcoursedetails where StudentId=?";
    		pstmt = con.prepareStatement(sql);
    		pstmt.setLong(1, userid);
    		rs = pstmt.executeQuery();
    		if(rs.next()) {
    			courseIdsList=rs.getString("CourseId");
    		}
    		
    		
    		if (rs != null) { rs.close(); }
    		if (pstmt != null) {	pstmt.close();}
    		sql = "select CourseId,CourseName FROM coursemater where CourseId in ("+courseIdsList+"); ";
    		pstmt = con.prepareStatement(sql);
    		System.out.println(sql);
    		rs = pstmt.executeQuery();
    		while (rs.next()) {
    			roleForm = new ReviewForm();
				roleForm.setCourseid(rs.getInt("CourseId"));
				roleForm.setCoursename(rs.getString("CourseName"));


				list.add(roleForm);
    		}
    	} catch (Exception e) {
    		e.printStackTrace();
    	}	
    	finally {
    		ConnectionUtil.closeResources(con, pstmt, rs);
    	}
		return list;
	}

	public String addrating(Long userid, ReviewForm review,
			DataSource dataSource, String emailid)
	{
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		try	{
			con=dataSource.getConnection();
			String sql="select reviewDescription from reviewmaster WHERE userId=? and courseId=?";//select query
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1,userid);
			pstmt.setLong(2,review.getCourseid());
			ResultSet rs = pstmt.executeQuery();
			if(rs.next())
			{
				con=dataSource.getConnection();
			 sql="UPDATE reviewmaster SET reviewDescription=? WHERE userId=? and courseId=?";//select query
				pstmt = con.prepareStatement(sql);
				pstmt.setString(1,review.getReviewcomment());
				pstmt.setLong(2,userid);
				pstmt.setLong(3,review.getCourseid());
				pstmt.executeUpdate();
				result =  Common.REC_UPDATED;
			}
			else{
				if(!review.getReviewcomment().equalsIgnoreCase("") && !review.getReviewcomment().equalsIgnoreCase(null)){
				sql="insert into reviewmaster(userId,reviewDescription,courseId,approvalStatus,emailid) VALUES (?,?,?,?,?)";//select query
				pstmt=con.prepareStatement(sql);
				pstmt.setLong(1,userid);
				pstmt.setString(2,review.getReviewcomment());
				pstmt.setInt(3,review.getCourseid());
				pstmt.setBoolean(4,false);
				pstmt.setString(5,emailid);
				pstmt.executeUpdate();
				result =Common.REC_ADDED;
				}
			}
		}
		catch (Exception e){
		 e.printStackTrace();
		}
		finally{
			ConnectionUtil.closeResources(con, pstmt);
		}
		return result;
	}

}
