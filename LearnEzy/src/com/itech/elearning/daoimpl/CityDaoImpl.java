package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.elearning.forms.CityForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;


public class CityDaoImpl {
	public List<CityForm> listAll(int roleid,String compid,long stateid, long countryid,	DataSource dataSource){
		List<CityForm> cityList = new ArrayList<CityForm>();
		CityForm cityform = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql=null;
		try	{
			con=dataSource.getConnection();
			if(roleid==9)
			{
			if(countryid>0 && stateid>0 ){
				System.out.println("1");
				sql="select * from citymaster WHERE stateid=?";
				pstmt = con.prepareStatement(sql);
				pstmt.setLong(1,stateid);
				
			}else if(countryid>0){
				System.out.println("2");
				sql="select * from citymaster cm inner join statemaster sm on sm.stateid=cm.stateid inner join countrymaster ctym on ctym.countryid=sm.countryid where ctym.countryid=? ";
				pstmt = con.prepareStatement(sql);
				pstmt.setLong(1,countryid);
			}else{
				System.out.println("3");
				sql="SELECT * FROM citymaster ORDER BY CITY ASC";
				pstmt = con.prepareStatement(sql);
				
			}
			}
			else
			{
				if(countryid>0 && stateid>0 ){
					System.out.println("1");
					sql="select * from citymaster WHERE stateid=? and CompanyId= '"+compid+"'";
					pstmt = con.prepareStatement(sql);
					pstmt.setLong(1,stateid);
					
				}else if(countryid>0){
					System.out.println("2");
					sql="select * from citymaster cm inner join statemaster sm on sm.stateid=cm.stateid inner join countrymaster ctym on ctym.countryid=sm.countryid where ctym.countryid=? and ctym.CompanyId='"+compid+"'";
					pstmt = con.prepareStatement(sql);
					pstmt.setLong(1,countryid);
				}else{
					System.out.println("3");
					sql="SELECT * FROM citymaster where CompanyId= '"+compid+"' ORDER BY CITY ASC";
					pstmt = con.prepareStatement(sql);
					
				}
			}
			rs=pstmt.executeQuery();
			while(rs.next()){
				cityform = new CityForm();
				cityform.setCityid(rs.getLong("CITYID"));
				cityform.setStateid(rs.getLong("STATEID"));
				cityform.setCityname(rs.getString("CITY"));
				cityform.setCountryid(rs.getLong("COUNTRYID"));
				cityform.setActive(rs.getBoolean("ACTIVE"));
				cityList.add(cityform);
				
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return cityList;
	}

	public String add(CityForm cityForm, String compid, DataSource dataSource)
	{
	    String result = Common.FAILURE_MESSAGE;
	    Connection con = null;
	    PreparedStatement pstmt = null;
	    try
	    {
	    	con=dataSource.getConnection();
			String sql="SELECT * FROM citymaster WHERE CITY=? and stateid=? and countryid=? and CompanyId='"+compid+"'";
	    	pstmt = con.prepareStatement(sql);
	    	pstmt.setString(1, cityForm.getCityname());
	    	pstmt.setLong(2, cityForm.getStateid());
	    	pstmt.setLong(3, cityForm.getCountryid());
	    	ResultSet rs = pstmt.executeQuery();
	    	if(rs.next())result = Common.DUPLICATE_NAME_MESSAGE;
	    	else {
	    		if(!cityForm.getCityname().equalsIgnoreCase("") && !cityForm.getCityname().equalsIgnoreCase(null))	{
	    			sql="insert into citymaster(stateid,CITY,countryid,Active,CompanyId) values (?,?,?,?,?)";
	    			pstmt=con.prepareStatement(sql);
	    			pstmt.setLong(1, cityForm.getStateid());
	    			pstmt.setString(2, cityForm.getCityname());
	    			pstmt.setLong(3, cityForm.getCountryid());
	    			pstmt.setBoolean(4, true);
	    			pstmt.setString(5,compid);
	    			pstmt.executeUpdate();
	    			result = Common.REC_ADDED;
	    		}
	    	}
	    }
	    catch (Exception e) 
	    {
			e.printStackTrace();
		}
	    finally{
			ConnectionUtil.closeResources(con, pstmt);
		}
		return result;
	}

	public String changeStatus(CityForm cityForm, DataSource dataSource)
    {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		try
		{
			con=dataSource.getConnection();
			String sql="UPDATE citymaster SET Active=? WHERE cityid=?";
			pstmt=con.prepareStatement(sql);
			pstmt.setBoolean(1,cityForm.isActive());
			pstmt.setLong(2, cityForm.getCityid());
			pstmt.executeUpdate();
			result = Common.REC_STATUS_CHANGED;
		}
		catch (Exception e) 
		{
		  e.printStackTrace();
		}
		finally{
			ConnectionUtil.closeResources(con, pstmt);
		}
		return result;
	}

	public String update( CityForm cityForm, DataSource dataSource) 
	{
		String result=Common.FAILURE_MESSAGE;
		Connection con=null;
		PreparedStatement pstmt=null;
		try {
			
			con=dataSource.getConnection();
			String sql="SELECT * FROM citymaster WHERE CITY=? and stateid=? and countryid=?";
			pstmt=con.prepareStatement(sql);			
			pstmt.setString(1, cityForm.getCityname());
	    	pstmt.setLong(2, cityForm.getStateid());
	    	pstmt.setLong(3, cityForm.getCountryid());
			ResultSet rs=pstmt.executeQuery();
			if(rs.next())result= Common.No_Change;
			else{
				sql="UPDATE citymaster SET CITY=?, STATEID=?,countryid=? WHERE cityid=?";
				if(pstmt!=null)pstmt.close();
				pstmt=con.prepareStatement(sql);
				pstmt.setString(1, cityForm.getCityname());
				pstmt.setLong(2, cityForm.getStateid());
				pstmt.setLong(3, cityForm.getCountryid());
				pstmt.setLong(4, cityForm.getCityid());
				pstmt.executeUpdate();
				result=Common.REC_UPDATED;
				}
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			ConnectionUtil.closeResources(con, pstmt);
		}
		return result;
	}
	
	/*//**************START Audit Trial Inserting Code********************
	public int auditTrail(DataSource dataSource, CityForm cityForm, int userid) {
		int flag=0;
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		try {
			con=dataSource.getConnection();
			String screen="City Master Screen";
			String tablename="citymaster";
			String sql="select * FROM citymaster WHERE CITYID=?";
			pstmt= con.prepareStatement(sql);
			pstmt.setLong(1,cityForm.getCityid());
			rs = pstmt.executeQuery();
			while (rs.next()) {
									
				    if(!rs.getString("CITY").equals(cityForm.getCityname())){
				    	String fieldname = "CITY";
				    	String currentvalue = rs.getString("CITY");
				    	String changedvalue = cityForm.getCityname();
				    	AuditTrailDao.auditTrail(con,userid,screen,tablename,fieldname,currentvalue,changedvalue);
					}	
				    
				    if(rs.getInt("STATEID")!=cityForm.getStateid()){
				    	String fieldname = "STATEID";
				    	String currentvalue = rs.getString("STATEID");
				    	String changedvalue = String.valueOf(cityForm.getStateid());
				    	AuditTrailDao.auditTrail(con,userid,screen,tablename,fieldname,currentvalue,changedvalue);
					}	
				    
				    if(rs.getInt("COUNTRYID")!=cityForm.getCountryid()){
				    	String fieldname = "COUNTRYID";
				    	String currentvalue = rs.getString("COUNTRYID");
				    	String changedvalue = String.valueOf(cityForm.getCountryid());
				    	AuditTrailDao.auditTrail(con,userid,screen,tablename,fieldname,currentvalue,changedvalue);
					}	
			}
		}
		 catch (Exception e) {
			e.printStackTrace();
		}finally{
			ConnectionUtil.closeResources(con, pstmt,rs);
		}
		return flag;
		//**************End Audit Trial Inserting Code********************
	}*/
	
	
	public List<CityForm> activeList(DataSource dataSource) {
		List<CityForm> list = new ArrayList<CityForm>();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		CityForm CityForm = null;
		try
		{
			con=dataSource.getConnection();
			String sql="SELECT * FROM citymaster where active=1 ORDER BY CITY ASC";
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while(rs.next()){
				CityForm = new CityForm();
				CityForm.setCityid(rs.getInt("CITYID"));
				CityForm.setCityname(rs.getString("CITY"));
				CityForm.setActive(rs.getBoolean("active"));
				list.add(CityForm);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return list;
	}
	
	public List<CityForm> activeListByState(long stateid, DataSource dataSource) {
		List<CityForm> list = new ArrayList<CityForm>();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		CityForm CityForm = null;
		try
		{
			con=dataSource.getConnection();
			String sql="select * from citymaster WHERE active=1 and stateid=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, stateid);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				CityForm = new CityForm();
				CityForm.setCityid(rs.getInt("CITYID"));
				CityForm.setCityname(rs.getString("CITY"));
				CityForm.setActive(rs.getBoolean("active"));
				list.add(CityForm);

			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public JSONArray ajaxActiveList(DataSource dataSource, String cityId) {
		JSONArray jArray= new JSONArray();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql=null;
		try{
			con=dataSource.getConnection();
			sql="select * from citymaster WHERE active=1 and stateid=?";//select query
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1,cityId);
			rs = pstmt.executeQuery();
			while(rs.next()){
				JSONObject obj= new JSONObject();
				obj.put("cityId",rs.getLong("cityID"));
				obj.put("city",rs.getString("city"));
				jArray.add(obj);
				System.out.println(sql);
			}
		}
		catch (Exception e){
		   e.printStackTrace();
		}
		
		finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;
	}
}
