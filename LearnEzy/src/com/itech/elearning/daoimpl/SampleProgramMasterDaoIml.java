package com.itech.elearning.daoimpl;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.sql.DataSource;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;
import org.json.simple.JSONObject;

import com.itech.elearning.forms.SampleProgramMasterForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class SampleProgramMasterDaoIml {
	public List<SampleProgramMasterForm> listAllCourse(int roleid,long userid, String compid, DataSource ds) {
		List<SampleProgramMasterForm> courseList = new ArrayList<SampleProgramMasterForm>();
		SampleProgramMasterForm topicForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			if(roleid == 9)
			{
		 sql = "SELECT * FROM coursemater WHERE ACTIVE=TRUE  ";
			}
			else
			{
			 sql = "SELECT * FROM coursemater WHERE ACTIVE=TRUE and CompanyId = "+compid+" ";
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				topicForm = new SampleProgramMasterForm();
				topicForm.setCourseId(rs.getLong("CourseId"));
				topicForm.setCourseName(rs.getString("CourseName"));
				courseList.add(topicForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return courseList;
	}

	 
	public String add(SampleProgramMasterForm subtopicForm,String compid, DataSource ds) {
		String result = Common.FAILURE_MESSAGE;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		Connection con = null; 
		try {
			con = ds.getConnection();
			String sql1 = "SELECT * FROM sampleprogrammaster WHERE name=? and subTopicId =? and sampleProgramContent=? and CompanyId = '"+compid+"'";
			pstmt = con.prepareStatement(sql1);
			pstmt.setString(1, subtopicForm.getName());
			pstmt.setLong(2, subtopicForm.getSubTopicId());
			pstmt.setString(3, subtopicForm.getSampleProgramContent());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				result = Common.DUPLICATE_NAME_MESSAGE;
			} else {
				String sql = "INSERT INTO sampleprogrammaster (name, subTopicId, sampleProgramContent, active,CompanyId) VALUES(?,?,?,?,?)";
				if (pstmt != null)	pstmt.close();
				pstmt = con.prepareStatement(sql);
				pstmt.setString(1, subtopicForm.getName());
				pstmt.setLong(2, subtopicForm.getSubTopicId());
				pstmt.setString(3, subtopicForm.getSampleProgramContent());
				pstmt.setBoolean(4, true);
				pstmt.setString(5, compid);
				pstmt.executeUpdate();
				result = Common.REC_ADDED;
				System.out.println(subtopicForm.getSubTopicId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;
		
	
	}

	public String changeStatus(SampleProgramMasterForm subtopicForm, long sampleProgramID,	DataSource ds) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = ds.getConnection();
			String sql = "UPDATE sampleprogrammaster SET ACTIVE=? WHERE sampleProgramID=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setBoolean(1, subtopicForm.isActive());
			pstmt.setLong(2, sampleProgramID);
			pstmt.executeUpdate();
			result = Common.REC_STATUS_CHANGED;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	

	public Object update(SampleProgramMasterForm subtopicForm,DataSource ds) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			String sql1 = "SELECT * FROM sampleprogrammaster WHERE name=? and subTopicId =? and sampleProgramContent=?";
			pstmt = con.prepareStatement(sql1);
			pstmt.setString(1, subtopicForm.getName());
			pstmt.setLong(2, subtopicForm.getSubTopicId());
			pstmt.setString(3, subtopicForm.getSampleProgramContent());
			rs = pstmt.executeQuery();
			if(rs.next()) result = Common.No_Change;
			else {
				sql = "UPDATE sampleprogrammaster SET name=?, subTopicId=?, sampleProgramContent=? where sampleProgramID = ?";
				pstmt = con.prepareStatement(sql);
				pstmt.setString(1, subtopicForm.getName());
				pstmt.setLong(2, subtopicForm.getSubTopicId());
				pstmt.setString(3, subtopicForm.getSampleProgramContent());
				pstmt.setLong(4, subtopicForm.getSampleProgramID());
				pstmt.executeUpdate();
				result = Common.REC_UPDATED;
			} 
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	public List<SampleProgramMasterForm> listSampleProgram(int roleid,long userid,
			String compid, DataSource dataSource) {
		List<SampleProgramMasterForm> sampleProgramList = new ArrayList<SampleProgramMasterForm>();
		SampleProgramMasterForm subData = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		 
		try {
			con = dataSource.getConnection();
			if(roleid == 9)
			{
		 sql = "SELECT * from sampleprogrammaster sp inner join subtopicmaster st on st.subtopicid=sp.subtopicid  inner join coursemater cm on cm.courseid=st.courseid inner join topicmaster tm on tm.topicid=st.topicid ";
			}
			else
			{
			 sql = "SELECT * from sampleprogrammaster sp inner join subtopicmaster st on st.subtopicid=sp.subtopicid  inner join coursemater cm on cm.courseid=st.courseid inner join topicmaster tm on tm.topicid=st.topicid and tm.CompanyId= "+compid+" ";

			}
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				subData = new SampleProgramMasterForm();
				subData.setSubTopicName(rs.getString("SubTopicName"));
				subData.setSubTopicId(rs.getLong("SubTopicID"));	
				
				subData.setCourseId(rs.getLong("CourseId"));
				subData.setCourseName(rs.getString("CourseName"));
				
				subData.setTopicId(rs.getLong("TopicID"));
				subData.setTopicName(rs.getString("TopicName"));
				
				subData.setName(rs.getString("NAME"));
				//subData.setSampleProgramContent(rs.getString("sampleProgramContent"));
				subData.setSampleProgramID(rs.getLong("sampleProgramID"));
				subData.setActive(rs.getBoolean("ACTIVE"));
				
				/*JSONObject obj = new JSONObject();
				
				obj.put("subTopicId",rs.getLong("SubTopicID"));
				obj.put("topicId",rs.getLong("TopicID"));
				obj.put("courseId",rs.getLong("CourseId"));
				obj.put("name",rs.getString("NAME"));
				//obj.put("sampleProgramContent",rs.getString("sampleProgramContent"));
				obj.put("sampleProgramID",rs.getLong("sampleProgramID"));
				
				subData.setJsonEdit(obj.toString());*/
				sampleProgramList.add(subData);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return sampleProgramList;
	}


	@SuppressWarnings("unchecked")
	public JSONObject getEdit(DataSource dataSource, long sampleProgramID) {
		
		JSONObject obj = new JSONObject();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		 
		try {
			con = dataSource.getConnection();
			String sql = "SELECT * from sampleprogrammaster sp inner join subtopicmaster st on st.subtopicid=sp.subtopicid  inner join coursemater cm on cm.courseid=st.courseid inner join topicmaster tm on tm.topicid=st.topicid where sampleProgramID=? ";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, sampleProgramID);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				obj.put("subTopicId",rs.getLong("SubTopicID"));
				obj.put("topicId",rs.getLong("TopicID"));
				obj.put("courseId",rs.getLong("CourseId"));
				obj.put("name",rs.getString("NAME"));
				obj.put("sampleProgramContent",rs.getString("sampleProgramContent"));
				obj.put("sampleProgramID",rs.getLong("sampleProgramID"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return obj;
	}
	
	public String sampleProgramExcelUpload(String fileName, String filePath, String compid) {
		
		 String result="process Failed"; 
		   //int flag=0;
		   int daoflag=0;
		   Connection con=null;
		   PreparedStatement pstmt=null;
		   
		   try {
			   
			    con=ConnectionUtil.getMySqlConnection();
	         	long start = System.currentTimeMillis();
	            
	         	
	         	String sql="insert into sampleprogrammaster(name,subTopicId,sampleProgramContent,CompanyId,active) values(?,?,?,?,1)";
	              
	            pstmt=con.prepareStatement(sql);
	            
	         	InputStream input = new BufferedInputStream(new FileInputStream(filePath+fileName));
	         	
	         	 
	         	System.out.println("Excel File path="+filePath);
	         	System.out.println("Excel File Name="+fileName);
	            POIFSFileSystem fs = new POIFSFileSystem(input);
	            HSSFWorkbook wb = new HSSFWorkbook(fs);
	            HSSFSheet sheet = wb.getSheetAt(0);
	            Iterator<Row> rows = sheet.rowIterator();
	          
	            long count=0;
	            while (rows.hasNext()) {
	            		// flag=0;
	            		 HSSFRow row = (HSSFRow) rows.next();
	                     
	 
	                     String sample_program_name=TopicMasterDaoIml.StringNotNull(row.getCell(0));
	                     String sub_topic_id=TopicMasterDaoIml.StringNotNull(row.getCell(1));
	                     String sample_program_content=TopicMasterDaoIml.StringNotNull(row.getCell(2));
	                  
	                     
	                     
	                     
	                     try {
	                    	 if(daoflag==1){
	                      		count=count+1;
	                         	System.out.println("excel file adding............");
	                   	       
	                   	       	pstmt.setString(1,sample_program_name);
	                   	        pstmt.setString(2,sub_topic_id);
	                   	        pstmt.setString(3,sample_program_content);
	                   	        pstmt.setString(4,compid);
	     	                   
	                   	     
	     	                    pstmt.addBatch();
	     	                    
	     	                    
	     	                    if(count%1000 == 0) { 
	     	                    	pstmt.executeBatch();
	     	                    	System.out.println("Batch Count: "+count+ " Time Taken: "+(System.currentTimeMillis()-start)+" MilliSecons");
	     	                    }            
	     	                  //  flag=1;
	     	                    
	     	                    System.out.println("count="+count);
	     	                    result=count+" "+"Records uploaded";
	     	                    
	     	                    System.out.println("%%%%%%%%%%%%%"+result);
	     	                 }
	                 	} catch (Exception einner) {
	                 		einner.printStackTrace();
						}
	                      
	                      daoflag=1;
	            }
	            pstmt.executeBatch();
	            System.out.println("Batch Count: "+count+ " Time Taken: "+(System.currentTimeMillis()-start)+" MilliSecons");
	        
		   } catch (Exception e) {
	            e.printStackTrace();
	        }
	        finally{
	            ConnectionUtil.closeResources(con, pstmt);
	        }
	        
	        return result; 
			//return flag;
	    }    
	
	 public static String StringNotNull(HSSFCell rowValue){
		   String results="";
		   if(rowValue!=null) results =rowValue.toString().trim();
		   return results;
		   
	   }


	 

	
}
