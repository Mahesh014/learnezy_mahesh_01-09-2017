package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import com.itech.elearning.forms.CourseToCouchForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class CourseToCouchDaoImpl {

	public void getCouchInfo(DataSource ds, long userId,
			HttpServletRequest request) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ds.getConnection();
			String sql = "SELECT * from usermaster where UserId=" + userId + "";
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {

				request.setAttribute("fname", rs.getString("FirstName"));
				request.setAttribute("lname", rs.getString("LastName"));
				request.setAttribute("stdId", userId);

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		
	}

	public List<CourseToCouchForm> listAllCourse(String compid, Long userid,
			DataSource ds) {
		List<CourseToCouchForm> courseList = new ArrayList<CourseToCouchForm>();
		CourseToCouchForm courseData = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ds.getConnection();
			String sql = "SELECT * from coursemater where Active = true and CompanyId = '"+compid+"'";
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				courseData = new CourseToCouchForm();
				courseData.setCourseId(rs.getLong("CourseId"));
				courseData.setCourseName(rs.getString("CourseName"));

			
				courseList.add(courseData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return courseList;
	}

	public List<CourseToCouchForm> listAllTopoic(Long userid,
			DataSource ds, Long courseId) {
		List<CourseToCouchForm> courseList = new ArrayList<CourseToCouchForm>();
		CourseToCouchForm courseData = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql =  null;
		try {
			con = ds.getConnection();
			if(courseId != null)
			{
			sql = "SELECT TopicName, TopicID from topicmaster where CourseId = "+courseId+" and active=true";
			}
			else
			{
				sql = "SELECT TopicName, TopicID from topicmaster where active=true";
			}
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				courseData = new CourseToCouchForm();
				courseData.setTopicId(rs.getLong("TopicID"));
				courseData.setTopicName(rs.getString("TopicName"));

				courseList.add(courseData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return courseList;
	}

	public List<CourseToCouchForm> listCouchList(String compid, Long userid,
			DataSource ds, CourseToCouchForm couchForm, long couchId) {
		List<CourseToCouchForm> stdList = new ArrayList<CourseToCouchForm>();
		CourseToCouchForm topicData = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		System.out.println(" Funtion Called By LIst");
		try {
			con = ds.getConnection();
			String sql = "select cmt.CouchId, cmt.CourseID, cm.CourseName, um.FirstName, um.LastName  from  coursecouchmgmt as cmt join coursemater cm on (cmt.CourseID = cm.CourseId) join usermaster um on (cmt.CouchId = um.UserId)  where CouchId = "+ couchId +" and CompanyId = '"+compid+"' ";
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				topicData = new CourseToCouchForm();
				topicData.setCouchId(rs.getLong("cmt.CouchId"));
				topicData.setCouchName(rs.getString("um.FirstName") + "  "
						+ rs.getString("um.LastName"));
				topicData.setCouchId(rs.getLong("cmt.CourseID"));				
				topicData.setCourseName(rs.getString("cm.CourseName"));			

				stdList.add(topicData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return stdList;
	}

	public Object addDetails(String compid, CourseToCouchForm courseForm,
			DataSource dataSource, long couchId) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM coursecouchmgmt  WHERE CourseID=? and CouchId=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, courseForm.getCourseId());
			pstmt.setLong(2, couchId);
			rs = pstmt.executeQuery();
			if (rs.next())
				result = Common.DUPLICATE_NAME_MESSAGE;
			else {
				
					sql = "INSERT INTO coursecouchmgmt (CourseID,CouchId,CompanyId) VALUES (?,?,?)";					
					pstmt = con.prepareStatement(sql);
					System.out.println(sql);
					pstmt.setLong(1, courseForm.getCourseId());
					pstmt.setLong(2,couchId);
					pstmt.setString(3,compid);
					pstmt.executeUpdate();
					result = Common.REC_ADDED;
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;
	}
}
