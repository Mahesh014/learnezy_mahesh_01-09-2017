package com.itech.elearning.daoimpl;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.sql.DataSource;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;





import com.itech.elearning.forms.QuestionMasterNewForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class QuestionMasterNewDaoImpl {



	public List<QuestionMasterNewForm> listAllCourse(int roleid,long userid, String compid, DataSource ds) {
		List<QuestionMasterNewForm> courseList = new ArrayList<QuestionMasterNewForm>();
		QuestionMasterNewForm topicForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			if(roleid==9)
			{
		 sql = "SELECT * FROM coursemater WHERE ACTIVE=TRUE ";
			}
			else
			{
		 sql = "SELECT * FROM coursemater WHERE ACTIVE=TRUE and CompanyId= '"+compid+"'";

			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				topicForm = new QuestionMasterNewForm();
				topicForm.setCourseId(rs.getLong("CourseId"));
				topicForm.setCourseName(rs.getString("CourseName"));

				courseList.add(topicForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return courseList;
	}


	public List<QuestionMasterNewForm> listAllTopic(int roleid,long userid, String compid, long courseId,
			DataSource ds) {
		List<QuestionMasterNewForm> topicList = new ArrayList<QuestionMasterNewForm>();
		QuestionMasterNewForm topicForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			System.out.println("vani Testing "+courseId);
			if(roleid==9)
			{
			if (courseId != -1 || courseId != 0)
				sql = "SELECT * FROM topicmaster WHERE ACTIVE=TRUE and CourseId='"+courseId+"'";
			else
				sql = "SELECT * FROM TOPICMASTER WHERE ACTIVE=TRUE";
			}
			else
			{
				if (courseId != -1 || courseId != 0)
					sql = "SELECT * FROM topicmaster WHERE ACTIVE=TRUE and CourseId='"+courseId+"' and CompanyId = '"+compid+"'";
				else
					sql = "SELECT * FROM TOPICMASTER WHERE ACTIVE=TRUE and CompanyId= '"+compid+"'";
			}
			pstmt = con.prepareStatement(sql);

			System.out.println(sql);

			rs = pstmt.executeQuery();
			while (rs.next()) {
				topicForm = new QuestionMasterNewForm();
				topicForm.setTopicId(rs.getLong("TopicID"));
				topicForm.setTopicName(rs.getString("TopicName"));
				topicList.add(topicForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return topicList;
	}


	@SuppressWarnings("unchecked")
	public List<QuestionMasterNewForm> listallquestions(int roleid,String compid, long userid,DataSource ds, long courseId, long topicId) {
		List<QuestionMasterNewForm> list = new ArrayList<QuestionMasterNewForm>();
		QuestionMasterNewForm topicForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		String sql = null;
		ResultSet rs = null;
		try {
			con = ds.getConnection();
			if(roleid==9)
			{
			sql = "select qm.QuestionId, qm.CourseId, qm.DescriptiveAns,qm.Question, qm.AnswerA, qm.AnswerB, qm.AnswerC, qm.AnswerD, qm.CorrectAnswer, qm.Active,cm.CourseName  From questionmaster qm  inner join coursemater cm on cm.courseId=qm.courseId where qm.active=true ";
			System.out.println(courseId +" "+topicId);
			if ((courseId != -1 && courseId != 0)) {
				sql = "select qm.QuestionId, qm.CourseId,qm.DescriptiveAns, qm.Question, qm.AnswerA, qm.AnswerB, qm.AnswerC, qm.AnswerD, qm.CorrectAnswer, qm.Active,cm.CourseName from questionmaster qm "+
				" inner join coursemater cm on cm.courseId=qm.courseId where qm.courseId="+courseId+" and qm.CompanyId= '"+compid+"' ";
			}
			}
			else
			{
				sql = "select qm.QuestionId, qm.CourseId, qm.DescriptiveAns,qm.Question, qm.AnswerA, qm.AnswerB, qm.AnswerC, qm.AnswerD, qm.CorrectAnswer, qm.Active,cm.CourseName  From questionmaster qm  inner join coursemater cm on cm.courseId=qm.courseId where qm.active=true ";
				System.out.println(courseId +" "+topicId);
				if ((courseId != -1 && courseId != 0)) {
					sql = "select qm.QuestionId, qm.CourseId,qm.DescriptiveAns, qm.Question, qm.AnswerA, qm.AnswerB, qm.AnswerC, qm.AnswerD, qm.CorrectAnswer, qm.Active,cm.CourseName from questionmaster qm "+
					" inner join coursemater cm on cm.courseId=qm.courseId where qm.courseId="+courseId+" and qm.CompanyId= '"+compid+"' ";
				}
			}
			System.out.println("sql "+sql);
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				topicForm = new QuestionMasterNewForm();

				System.out.println(" ==============");

				topicForm.setQuestionId(rs.getLong("qm.QuestionId"));
				topicForm.setQuestion(rs.getString("qm.Question"));
				topicForm.setAnsA(rs.getString("qm.AnswerA"));
				topicForm.setAnsB(rs.getString("qm.AnswerB"));
				topicForm.setAnsC(rs.getString("qm.AnswerC"));
				topicForm.setAnsD(rs.getString("qm.AnswerD"));
				topicForm.setCrctAns(rs.getString("qm.CorrectAnswer"));
				topicForm.setDescriptiveAns(rs.getString("qm.DescriptiveAns"));
				topicForm.setActive(rs.getBoolean("qm.active"));
				topicForm.setCourseId(rs.getLong("qm.CourseId"));
				topicForm.setCourseName(rs.getString("cm.CourseName"));
				
				JSONObject obj=new JSONObject();
				obj.put("questionId",rs.getLong("qm.QuestionId"));
				obj.put("question",rs.getString("qm.Question"));
				obj.put("ansA",rs.getString("qm.AnswerA"));
				obj.put("ansB",rs.getString("qm.AnswerB"));
				obj.put("ansC",rs.getString("qm.AnswerC"));
				obj.put("ansD",rs.getString("qm.AnswerD"));
				obj.put("crctAns",rs.getString("qm.CorrectAnswer"));
				obj.put("descriptiveAns",rs.getString("qm.DescriptiveAns"));
				obj.put("active",rs.getBoolean("qm.active"));
				obj.put("courseId",rs.getLong("qm.CourseId"));
				obj.put("courseName",rs.getString("cm.CourseName"));
				topicForm.setjEdit(obj.toString());
				
				list.add(topicForm);
				
				 
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 

		return list;
	}

	public String addQuestion(String compid, QuestionMasterNewForm testForm, DataSource dataSource) {
		String result = Common.FAILURE_MESSAGE;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;

		try {
			con = dataSource.getConnection();
			String sql1 = "SELECT Question FROM questionmaster WHERE Question=? and CompanyId='"+compid+"'";
			pstmt = con.prepareStatement(sql1);
			pstmt.setString(1, testForm.getQuestion());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				result = Common.DUPLICATE_NAME_MESSAGE;
			} else {
				String sql = "INSERT INTO questionmaster (Question,AnswerA,AnswerB,AnswerC,AnswerD,CorrectAnswer,CourseId,Active,DescriptiveAns,CompanyId) VALUES(?,?,?,?,?,?,?,?,?,?)";
				if (pstmt != null)	pstmt.close();
				pstmt = con.prepareStatement(sql);
				pstmt.setString(1, testForm.getQuestion());
				pstmt.setString(2, testForm.getAnsA());
				pstmt.setString(3, testForm.getAnsB());
				pstmt.setString(4, testForm.getAnsC());
				pstmt.setString(5, testForm.getAnsD());
				pstmt.setString(6, testForm.getCrctAns());
				pstmt.setLong(7, testForm.getCourseId());
				pstmt.setBoolean(8, true);
				pstmt.setString(9, testForm.getDescriptiveAns());
				pstmt.setString(10,compid);
				pstmt.executeUpdate();
				result = Common.REC_ADDED;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;
	}


	public String changeStatus(QuestionMasterNewForm courseForm, DataSource ds,
			long questid) {

		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = ds.getConnection();
			String sql = "UPDATE questionmaster SET ACTIVE=? WHERE QuestionId=?";
			System.out.println(sql);
			pstmt = con.prepareStatement(sql);
			pstmt.setBoolean(1, courseForm.isActive());
			pstmt.setLong(2, questid);
			pstmt.executeUpdate();
			result = Common.REC_STATUS_CHANGED;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	public Object updateTest(QuestionMasterNewForm testForm, DataSource ds) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			sql = "SELECT * FROM questionmaster  WHERE Question =? AND QuestionId =? and AnswerA = ?  and AnswerB = ? and AnswerC = ?  and AnswerD = ? and CorrectAnswer = ?  and CourseId = ? and DescriptiveAns=? ";
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			pstmt.setString(1, testForm.getQuestion());
			pstmt.setLong(2, testForm.getQuestionId());
			pstmt.setString(3, testForm.getAnsA());
			pstmt.setString(4, testForm.getAnsB());
			pstmt.setString(5, testForm.getAnsC());
			pstmt.setString(6, testForm.getAnsD());
			pstmt.setString(7, testForm.getCrctAns());
			pstmt.setLong(8, testForm.getCourseId());
			pstmt.setString(9, testForm.getDescriptiveAns());
			rs = pstmt.executeQuery();
			if (rs.next()) {result = Common.No_Change;	}
			else{
			sql = "UPDATE questionmaster  SET Question  =?,AnswerA = ?,AnswerB = ?, AnswerC =?, AnswerD =?, CorrectAnswer =?,CourseId = ?, DescriptiveAns=?  where QuestionId = ?";
					if (rs != null)	rs.close();
					if (pstmt != null) pstmt.close();
					pstmt = con.prepareStatement(sql);
					pstmt.setString(1, testForm.getQuestion());
					pstmt.setString(2, testForm.getAnsA());
					pstmt.setString(3, testForm.getAnsB());
					pstmt.setString(4, testForm.getAnsC());
					pstmt.setString(5, testForm.getAnsD());
					pstmt.setString(6, testForm.getCrctAns());
					pstmt.setLong(7, testForm.getCourseId());
					pstmt.setString(8, testForm.getDescriptiveAns());
					pstmt.setLong(9, testForm.getQuestionId());
					pstmt.executeUpdate();
					result = Common.REC_UPDATED;
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;

	}


	public String delete(QuestionMasterNewForm testForm, long id, DataSource ds) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = ds.getConnection();
			String sql = "delete from questionmaster  WHERE QuestionId="+id ;
			System.out.println(sql);
			pstmt = con.prepareStatement(sql);

		
			pstmt.executeUpdate();
			result = Common.REC_DELETED ;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return result;
	}
	@SuppressWarnings("unchecked")
	public JSONArray questionByCourseid(DataSource dataSource, String courseId) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		JSONArray jarry= new JSONArray();

		try {
			con = dataSource.getConnection();
			sql = "SELECT * FROM questionmaster WHERE  CourseId = ?";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, courseId);
			rs=pstmt.executeQuery();
			while(rs.next()){
				JSONObject obj= new JSONObject();
				obj.put("QuestionId", rs.getString("QuestionId"));
				obj.put("Question", rs.getString("Question"));
				jarry.add(obj);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return jarry;
	}


	public String testTypeExcelUpload(String fileName, String filePath, String compid) {
		 String result=Common.FAILURE_MESSAGE;
		   int daoflag=0;
		   Connection con=null;
		   PreparedStatement pstmt=null;
		   String temp_Question="";
		   String temp_AnswerA="";
           String temp_AnswerB="";
           String temp_AnswerC="";
           String temp_AnswerD="";
           String temp_CorrectAnswer="";
           String temp_DescriptiveAns="";
           String temp_companyId="";
           String CourseId="";
           ResultSet rs=null;
           boolean status=false;
		   try {
			
	        
	        con=ConnectionUtil.getMySqlConnection();
         	long start = System.currentTimeMillis();
            
         	
         	String sql="insert into questionmaster(Question,AnswerA,AnswerB,AnswerC,AnswerD,CorrectAnswer,DescriptiveAns,CourseId,Active,CompanyID) values(?,?,?,?,?,?,?,?,?,?)";
              
            if(pstmt!=null)pstmt.close();
            pstmt=con.prepareStatement(sql);
            
         	InputStream input = new BufferedInputStream(new FileInputStream(filePath+fileName));
         	
         	 
         	System.out.println("Excel File path="+filePath);
         	System.out.println("Excel File Name="+fileName);
            POIFSFileSystem fs = new POIFSFileSystem(input);
            HSSFWorkbook wb = new HSSFWorkbook(fs);
            HSSFSheet sheet = wb.getSheetAt(0);
            Iterator<Row> rows = sheet.rowIterator();
          
            long count=0;
            while (rows.hasNext()) {
            		
            		 HSSFRow row = (HSSFRow) rows.next();
                     
 
            		 temp_Question=QuestionMasterNewDaoImpl.StringNotNull(row.getCell(0));
                     temp_AnswerA=QuestionMasterNewDaoImpl.StringNotNull(row.getCell(1));
                     temp_AnswerB=QuestionMasterNewDaoImpl.StringNotNull(row.getCell(2));
                     temp_AnswerC=QuestionMasterNewDaoImpl.StringNotNull(row.getCell(3));
                     temp_AnswerD=QuestionMasterNewDaoImpl.StringNotNull(row.getCell(4));
                     temp_CorrectAnswer=QuestionMasterNewDaoImpl.StringNotNull(row.getCell(5));
                     temp_DescriptiveAns=QuestionMasterNewDaoImpl.StringNotNull(row.getCell(6));
                     CourseId=QuestionMasterNewDaoImpl.StringNotNull(row.getCell(7));
                     
                     
                     
                     try {
                    	 if(daoflag==1){
                      		count=count+1;
                         	System.out.println("excel file adding............");
                   	       
                        	pstmt.setString(1,temp_Question);
                   	       	pstmt.setString(2,temp_AnswerA);
                   	       	pstmt.setString(3,temp_AnswerB);
                   	       	pstmt.setString(4,temp_AnswerC);
                   	       	pstmt.setString(5,temp_AnswerD);
                   	       	pstmt.setString(6,temp_CorrectAnswer);
                   	       	pstmt.setString(7,temp_DescriptiveAns);
                   	       	pstmt.setString(8,CourseId);
                   	       	pstmt.setBoolean(9, true);
                   	       	pstmt.setString(10,compid);
                   	        
                   	     
     	                    pstmt.addBatch();
     	                    
     	                    
     	                    if(count%1000 == 0) { 
     	                    	pstmt.executeBatch();
     	                    	System.out.println("Batch Count: "+count+ " Time Taken: "+(System.currentTimeMillis()-start)+" MilliSecons");
     	                    }            
     	                  //  flag=1;
     	                    
     	                    System.out.println("count="+count);
     	                    result=count+" "+"Records uploaded";
     	                    
     	                    System.out.println("%%%%%%%%%%%%%"+result);
     	                 }
                 	} catch (Exception einner) {
                 		einner.printStackTrace();
					}
                      
                      daoflag=1;
            }
            pstmt.executeBatch();
            System.out.println("Batch Count: "+count+ " Time Taken: "+(System.currentTimeMillis()-start)+" MilliSecons");
        
	   } catch (Exception e) {
            e.printStackTrace();
        }
        finally{
            ConnectionUtil.closeResources(con, pstmt);
        }
        
        return result; 
	        
	        
	        
	        
	       
	        
	        
	        
	        
	        
	        
	        
	        
	        
	        
	        

			//return flag;
	    }    
	
	 public static String StringNotNull(HSSFCell rowValue){
		   String results="";
		   if(rowValue!=null) results =rowValue.toString().trim();
		   return results;
		   
	   }


}
