package com.itech.elearning.daoimpl;

import java.io.BufferedInputStream;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.sql.DataSource;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


import com.itech.elearning.forms.AssignmentMasterForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class AssignmentMasterDao {

	public List<AssignmentMasterForm> listAllCourse(int roleid,String compid, Long userid, DataSource ds) {
		List<AssignmentMasterForm> courseList = new ArrayList<AssignmentMasterForm>();
		AssignmentMasterForm assingForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			if(roleid==9)
			{
			sql = "SELECT * FROM coursemater WHERE ACTIVE=TRUE ";
			}
			else
			{
			 sql = "SELECT * FROM coursemater WHERE ACTIVE=TRUE and CompanyId='"+compid+"'";
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				assingForm = new AssignmentMasterForm();
				assingForm.setCourseId(rs.getLong("CourseId"));
				assingForm.setCourseName(rs.getString("CourseName"));
				courseList.add(assingForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return courseList;
	}
	
	public List<AssignmentMasterForm> listAllSubTopic(int roleid,String compid,Long userid, Long courseId,
			Long topicId, DataSource ds) {
		List<AssignmentMasterForm> subTopicList = new ArrayList<AssignmentMasterForm>();
		AssignmentMasterForm assingForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		JSONArray returnValue= new JSONArray();
		JSONObject obj= null;
		try {
			con = ds.getConnection();
			if(roleid==9)
			{
			if(topicId==null)
			{
				sql = "SELECT * FROM subtopicmaster WHERE ACTIVE=TRUE ";
			}else{
			sql = "SELECT * FROM subtopicmaster WHERE ACTIVE=TRUE and topicId="+topicId+" ";
			System.out.println("vani testing "+sql);
			}
			}
			else
			{
				if(topicId==null)
				{
					sql = "SELECT * FROM subtopicmaster WHERE ACTIVE=TRUE and CompanyId='"+compid+"' ";
				}else{
				sql = "SELECT * FROM subtopicmaster WHERE ACTIVE=TRUE and topicId="+topicId+" and CompanyId='"+compid+"' ";
				System.out.println("vani testing "+sql);
				}
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				obj=new JSONObject();
				assingForm = new AssignmentMasterForm();
				assingForm.setSubTopicId(rs.getLong("subTopicID"));
				assingForm.setSubTopicName(rs.getString("subTopicName"));
				obj.put("stid", rs.getLong("subTopicID"));
				obj.put("stname", rs.getString("subTopicName"));
				
				subTopicList.add(assingForm);
				
				returnValue.add(obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return subTopicList;
	}

	 
	public JSONArray listAllSubTopic1(String compid,Long userid, Long courseId,
			Long topicId, DataSource ds) {
		List<AssignmentMasterForm> subTopicList = new ArrayList<AssignmentMasterForm>();
		AssignmentMasterForm assingForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		JSONArray returnValue= new JSONArray();
		JSONObject obj= null;
		try {
			con = ds.getConnection();
			
			if(topicId==null)
			{
				sql = "SELECT * FROM subtopicmaster WHERE ACTIVE=TRUE and CompanyId='"+compid+"' ";
			}else{
			sql = "SELECT * FROM subtopicmaster WHERE ACTIVE=TRUE and topicId="+topicId+" and CompanyId='"+compid+"' ";
			System.out.println("vani testing "+sql);
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				obj=new JSONObject();
				assingForm = new AssignmentMasterForm();
				assingForm.setSubTopicId(rs.getLong("subTopicID"));
				assingForm.setSubTopicName(rs.getString("subTopicName"));
				obj.put("stid", rs.getLong("subTopicID"));
				obj.put("stname", rs.getString("subTopicName"));
				
				subTopicList.add(assingForm);
				
				returnValue.add(obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return returnValue;
	}

	public List<AssignmentMasterForm> listAllTopic(int roleid,String compid,Long userid, Long courseId,
			DataSource ds) {
		List<AssignmentMasterForm> topicList = new ArrayList<AssignmentMasterForm>();
		AssignmentMasterForm assingForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			if(roleid==9)
			{
			if(courseId==null ){
			sql = "SELECT * FROM topicmaster WHERE ACTIVE=TRUE";
			}else{
				sql = "SELECT * FROM topicmaster WHERE ACTIVE=TRUE and courseId="+courseId+"";
				}
			}
			else
			{
				if(courseId==null ){
					sql = "SELECT * FROM topicmaster WHERE ACTIVE=TRUE and CompanyId='"+compid+"'";
					}else{
						sql = "SELECT * FROM topicmaster WHERE ACTIVE=TRUE and courseId="+courseId+" and CompanyId = '"+compid+"' ";
						}
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				assingForm = new AssignmentMasterForm();
				assingForm.setTopicId(rs.getLong("TopicID"));
				assingForm.setTopicName(rs.getString("TopicName"));
				topicList.add(assingForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return topicList;
	}

	public String add(AssignmentMasterForm assingForm,String compid ,DataSource dataSource) {
		String result = Common.FAILURE_MESSAGE;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM assignmentmaster WHERE AssignmentQuestion=? and CourseId=?  and " +
					"TopicId=? and AssignmentAnswer=? and SubTopicId=? and CompanyId=?";
			pstmt = con.prepareStatement(sql);
			System.out.println("SQL: "+sql);
			System.out.println("assingForm.getCourseId(): "+assingForm.getCourseId());
			pstmt.setString(1,assingForm.getAssignmentQuestion());
			pstmt.setLong(2,assingForm.getCourseId());
			pstmt.setLong(3,assingForm.getTopicId());
			pstmt.setString(4,assingForm.getAssignmentAnswer());
			pstmt.setLong(5,assingForm.getSubTopicId());
			pstmt.setString(6, compid);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				result = Common.DUPLICATE_NAME_MESSAGE;
			} else {
				sql = "INSERT INTO assignmentmaster (AssignmentQuestion,CourseId,TopicId,AssignmentAnswer,SubTopicId,ACTIVE,CompanyId) VALUES(?,?,?,?,?,?,?)";
				if (pstmt != null)	pstmt.close();
				pstmt = con.prepareStatement(sql);
				pstmt.setString(1,assingForm.getAssignmentQuestion());
				pstmt.setLong(2,assingForm.getCourseId());
				pstmt.setLong(3,assingForm.getTopicId());
				pstmt.setString(4,assingForm.getAssignmentAnswer());
				pstmt.setLong(5,assingForm.getSubTopicId());
				pstmt.setBoolean(6, true);
				pstmt.setString(7,compid);
				pstmt.executeUpdate();
				result = Common.REC_ADDED;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	public String changeStatus(AssignmentMasterForm assingForm, DataSource dataSource, Long assignmentId, boolean active) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = dataSource.getConnection();
			String sql = "UPDATE assignmentmaster SET ACTIVE=? WHERE AssignmentId=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setBoolean(1,active);
			pstmt.setLong(2, assignmentId);
			pstmt.executeUpdate();
			result = Common.REC_STATUS_CHANGED;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<AssignmentMasterForm> assignmentList(int roleid ,String compid,Long userid, DataSource ds, Long courseId, Long topicId) {

		List<AssignmentMasterForm> assignmentList = new ArrayList<AssignmentMasterForm>();
		AssignmentMasterForm assingForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			if(roleid==9)
			{
			if (courseId!=null && courseId !=-1 && topicId!=null && topicId !=-1 ) {
				sql = "SELECT * FROM assignmentmaster AS am JOIN subtopicmaster AS stm ON (stm.subtopicid=am.subtopicid) JOIN topicmaster AS tm ON (tm.topicid=am.topicid)  JOIN coursemater AS c ON (c.COURSEID=am.COURSEID) Where am.COURSEID="+courseId+"  and am.topicid="+topicId;
			}else
			if (courseId!=null && courseId !=-1) {
				sql = "SELECT * FROM assignmentmaster AS am JOIN subtopicmaster AS stm ON (stm.subtopicid=am.subtopicid) JOIN topicmaster AS tm ON (tm.topicid=am.topicid)  JOIN coursemater AS c ON (c.COURSEID=am.COURSEID) Where  am.COURSEID="+courseId;	
			}else
			{
				sql = "SELECT * FROM assignmentmaster AS am JOIN subtopicmaster AS stm ON (stm.subtopicid=am.subtopicid) JOIN topicmaster AS tm ON (tm.topicid=am.topicid)  JOIN coursemater AS c ON (c.COURSEID=am.COURSEID) ";
			}
			}
			else
			{
				if (courseId!=null && courseId !=-1 && topicId!=null && topicId !=-1 ) {
					sql = "SELECT * FROM assignmentmaster AS am JOIN subtopicmaster AS stm ON (stm.subtopicid=am.subtopicid) JOIN topicmaster AS tm ON (tm.topicid=am.topicid)  JOIN coursemater AS c ON (c.COURSEID=am.COURSEID) Where am.COURSEID="+courseId+" and am.CompanyId='"+compid+"' and am.topicid="+topicId;
				}else
				if (courseId!=null && courseId !=-1) {
					sql = "SELECT * FROM assignmentmaster AS am JOIN subtopicmaster AS stm ON (stm.subtopicid=am.subtopicid) JOIN topicmaster AS tm ON (tm.topicid=am.topicid)  JOIN coursemater AS c ON (c.COURSEID=am.COURSEID) Where am.CompanyId='"+compid+"' and am.COURSEID="+courseId;	
				}else
				{
					sql = "SELECT * FROM assignmentmaster AS am JOIN subtopicmaster AS stm ON (stm.subtopicid=am.subtopicid) JOIN topicmaster AS tm ON (tm.topicid=am.topicid)  JOIN coursemater AS c ON (c.COURSEID=am.COURSEID) and am.CompanyId='"+compid+"'";
				}
			}
			System.out.println(sql);
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				assingForm = new AssignmentMasterForm();
				assingForm.setCourseId(rs.getLong("CourseId"));
				assingForm.setCourseName(rs.getString("CourseName"));
				assingForm.setTopicId(rs.getLong("topicid"));
				assingForm.setTopicName(rs.getString("TopicName"));
				assingForm.setAssignmentQuestion(rs.getString("AssignmentQuestion"));
				assingForm.setAssignmentAnswer(rs.getString("AssignmentAnswer"));
				assingForm.setActive(rs.getBoolean("Active"));
				assingForm.setAssignmentId(rs.getLong("AssignmentId"));
				assingForm.setSubTopicId(rs.getLong("SubTopicId"));
				assingForm.setSubTopicName(rs.getString("SubTopicName"));
				
				JSONObject obj=new JSONObject();
				obj.put("courseId",rs.getLong("CourseId"));
				obj.put("topicId",rs.getLong("topicid"));
				obj.put("assignmentQuestion",rs.getString("AssignmentQuestion"));
				obj.put("assignmentAnswer",rs.getString("AssignmentAnswer"));
				obj.put("assignmentId",rs.getLong("AssignmentId"));
				obj.put("subTopicId",rs.getLong("SubTopicId"));
				assingForm.setjEdit(obj.toString());
				assignmentList.add(assingForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return assignmentList;
	}

	public String update(AssignmentMasterForm assingForm, DataSource ds) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			sql = "SELECT * FROM assignmentmaster WHERE AssignmentQuestion=? and CourseId=? and " +
			"TopicId=? and AssignmentAnswer=? and SubTopicId=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1,assingForm.getAssignmentQuestion());
			pstmt.setLong(2,assingForm.getCourseId());
			pstmt.setLong(3,assingForm.getTopicId());
			pstmt.setString(4,assingForm.getAssignmentAnswer());
			pstmt.setLong(5,assingForm.getSubTopicId());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				result = Common.No_Change;
			} else {
					sql = "UPDATE assignmentmaster SET AssignmentQuestion=?, CourseId=?, TopicId=?, AssignmentAnswer=?,SubTopicId=? where AssignmentId=?";
					if (pstmt != null)	pstmt.close();
					pstmt = con.prepareStatement(sql);
					pstmt.setString(1,assingForm.getAssignmentQuestion());
					pstmt.setLong(2,assingForm.getCourseId());
					pstmt.setLong(3,assingForm.getTopicId());
					pstmt.setString(4,assingForm.getAssignmentAnswer());
					pstmt.setLong(5,assingForm.getSubTopicId());
					pstmt.setLong(6,assingForm.getAssignmentId());
					pstmt.executeUpdate();
					result = Common.REC_UPDATED;
				 
			} 

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;

	}

	public List<AssignmentMasterForm> listAssignmentById(Long userid,Long courseId, DataSource dataSource) {
		List<AssignmentMasterForm> assignmentList = new ArrayList<AssignmentMasterForm>();
		AssignmentMasterForm assingForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();
			sql = "SELECT * FROM assignmentmaster AS am JOIN subtopicmaster AS stm ON (stm.subtopicid=am.subtopicid) JOIN topicmaster AS tm ON (tm.topicid=am.topicid)  JOIN coursemater AS c ON (c.COURSEID=am.COURSEID) where am.COURSEID=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, courseId);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				assingForm = new AssignmentMasterForm();
				assingForm.setCourseId(rs.getLong("CourseId"));
				assingForm.setCourseName(rs.getString("CourseName"));
				assingForm.setTopicId(rs.getLong("topicid"));
				assingForm.setTopicName(rs.getString("TopicName"));
				assingForm.setAssignmentQuestion(rs.getString("AssignmentQuestion"));
				assingForm.setAssignmentAnswer(rs.getString("AssignmentAnswer"));
				assingForm.setActive(rs.getBoolean("Active"));
				assingForm.setAssignmentId(rs.getLong("AssignmentId"));
				assingForm.setSubTopicId(rs.getLong("SubTopicId"));
				assingForm.setSubTopicName(rs.getString("SubTopicName"));
				assignmentList.add(assingForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return assignmentList;
	}

	public String testTypeExcelUpload(String fileName, String filePath) {
		String result=Common.FAILURE_MESSAGE;
		   int daoflag=0;
		   Connection con=null;
		   PreparedStatement pstmt=null;
		   String temp_AssignmentQuestion="";
           String temp_CourseId="";
           String temp_TopicID="";
           String temp_subTopicId="";
           String AssignmentAnswer="";
           String temp_CompanyId="";
           ResultSet rs=null;
           boolean status=false;
		   try {
			    con=ConnectionUtil.getMySqlConnection();
			    long start = System.currentTimeMillis();
			   	InputStream input = new BufferedInputStream(new FileInputStream(filePath+fileName));
	            POIFSFileSystem fs = new POIFSFileSystem(input);
	            HSSFWorkbook wb = new HSSFWorkbook(fs);
	            HSSFSheet sheet = wb.getSheetAt(0);
	            Iterator<Row> rows = sheet.rowIterator();
	            long count=0;
	            while (rows.hasNext()) {
	            		 HSSFRow row = (HSSFRow) rows.next();
	                    // String test_type=TestTypeDaoImp.StringNotNull(row.getCell(0));
	                      temp_CourseId=TestTypeDaoImp.StringNotNull(row.getCell(0));
	                      temp_TopicID=TestTypeDaoImp.StringNotNull(row.getCell(1));
	                      temp_subTopicId=TestTypeDaoImp.StringNotNull(row.getCell(2));
	                      temp_AssignmentQuestion=TestTypeDaoImp.StringNotNull(row.getCell(3));
	                      AssignmentAnswer=TestTypeDaoImp.StringNotNull(row.getCell(4));
	                      temp_CompanyId=TestTypeDaoImp.StringNotNull(row.getCell(5));
	            }
	            
	         	String duplicate="select * from assignmentmaster where AssignmentQuestion=?";
	         	 pstmt=con.prepareStatement(duplicate);
	         	 pstmt.setString(1, temp_AssignmentQuestion);
	         	 rs=pstmt.executeQuery();
		            if(rs.next()){
		            	status=true;
		            	result=Common.DUPLICATE_NAME_MESSAGE;
		            }
	         	 if(status==false){
	         	String sql="insert into assignmentmaster(AssignmentQuestion, CourseId, TopicID ,subTopicId,AssignmentAnswer,Active,CompanyID) values(?,?,?,?,?,?,?)";
	            if(pstmt!=null)pstmt.close();
	            pstmt=con.prepareStatement(sql);
	      
	                     try {
	                    	 if(daoflag==0){
	                      		count=count+1;
	                         	pstmt.setString(1,temp_AssignmentQuestion);
	                   	       	pstmt.setString(2, temp_CourseId);
	                   	       	pstmt.setString(3, temp_TopicID);
	                   	       	pstmt.setString(4, temp_subTopicId);
	                   	       	pstmt.setString(5, AssignmentAnswer);
	                   	       	pstmt.setBoolean(6, true);
	                   	       	pstmt.setString(7, temp_CompanyId);
	     	                    pstmt.addBatch();
	     	                    result=Common.REC_ADDED;
	     	                    if(count%1000 == 0) { 
	     	                    	pstmt.executeBatch();
	     	                    	System.out.println("Batch Count: "+count+ " Time Taken: "+(System.currentTimeMillis()-start)+" MilliSecons");
	     	                    }            
	     	                   //result=count+" "+"Records uploaded";
	     	                 }
	                 	} catch (Exception einner) {
	                 		einner.printStackTrace();
						}
	                      daoflag=1;
	            
	            pstmt.executeBatch();
	            System.out.println("Batch Count: "+count+ " Time Taken: "+(System.currentTimeMillis()-start)+" MilliSecons");
	         	 }
		   } catch (Exception e) {
	            e.printStackTrace();
	        }
	        finally{
	            ConnectionUtil.closeResources(con, pstmt);
	        }
	        

	        return result; 
			//return flag;
	    }    
	
	 public static String StringNotNull(HSSFCell rowValue){
		   String results="";
		   if(rowValue!=null) results =rowValue.toString().trim();
		   return results;
		   
	   }

	public JSONArray getTopic(DataSource dataSource, long courseId) {
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
       JSONObject obj=null;

		
		try {
			con=dataSource.getConnection();      
		String	sql = "SELECT * FROM topicmaster WHERE ACTIVE=TRUE and courseId="+courseId+"";


			pstmt=con.prepareStatement(sql);
			rs=pstmt.executeQuery();	
			while(rs.next()){
					obj=new JSONObject(); 
				obj.put("topicid", rs.getLong("TopicID"));
				obj.put("topiname", rs.getString("TopicName"));

				
					jArray.add(obj);
				}
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return jArray;

}}

