package com.itech.elearning.daoimpl;

import java.sql.Connection;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.itech.elearning.forms.StudentProfileForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;
import com.itech.elearning.utils.EncryptUtil;

public class StudentProfileDaoImpl {

	public StudentProfileForm getStudentData(Long userid, String courseIdsList, DataSource ds) {
		StudentProfileForm form = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ds.getConnection();
			String sql = "SELECT * FROM usermaster WHERE UserId="+userid;

			System.out.println(sql);
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();

			while (rs.next()) {

				form = new StudentProfileForm();			
				form.setUserid(rs.getLong("UserId"));
				//form.setCourseidslist(rs.getString("CourseId"));
				form.setFirstName(rs.getString("FirstName"));
				form.setLastName(rs.getString("LastName"));
				form.setGender(rs.getString("Gender"));
				form.setDob(rs.getString("DateofBirth"));
				form.setQualification(rs.getString("Qualification"));
				form.setContactno(rs.getString("ContactNumber"));
				form.setAddress(rs.getString("Address"));
				form.setEmailId(rs.getString("emailid"));
				form.setUname(rs.getString("UserName"));
				form.setPhone(rs.getString("Phone"));
				form.setOldPhoto(rs.getString("profilePhoto"));
				form.setPassword(EncryptUtil.decrypt(rs.getString("Password")));
				form.setCountryid(rs.getInt("countryid"));
				form.setStateid(rs.getInt("stateid"));
				form.setCityid(rs.getInt("cityid"));
				form.setPincode(rs.getInt("pincode"));
				//	list.add(form);

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return form;
	}

	public String updateUserData(StudentProfileForm studentForm,String[] theCourseList, DataSource ds, String profilePhotoName) {
		String result = Common.FAILURE_MESSAGE;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;

		try {
			con = ds.getConnection();
			String sql = "SELECT * FROM USERMASTER WHERE FirstName =?  AND LastName=?  AND Gender = ? and Qualification = ? AND ContactNumber = ? AND Address = ? and Phone =? AND emailid = ? and  DateofBirth=? and countryId=? and stateId=? and cityId=? and pincode=? and profilePhoto=?";
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			pstmt.setString(1, studentForm.getFirstName());
			pstmt.setString(2, studentForm.getLastName());
			pstmt.setString(3, studentForm.getGender());
			pstmt.setString(4, studentForm.getQualification());
			pstmt.setString(5, studentForm.getContactno());
			pstmt.setString(6, studentForm.getAddress());
			pstmt.setString(7, studentForm.getPhone());
			pstmt.setString(8, studentForm.getEmailId());
			pstmt.setString(9, studentForm.getDob());
			pstmt.setInt(10,studentForm.getCountryid());
			pstmt.setInt(11,studentForm.getStateid());
			pstmt.setInt(12,studentForm.getCityid());
			pstmt.setInt(13,studentForm.getPincode());
			pstmt.setString(14,profilePhotoName);
			rs = pstmt.executeQuery();
			if (rs.next())	return result =Common.No_Change;
			else{
				sql = "UPDATE USERMASTER SET FirstName  = ?,LastName = ?,Gender = ?,Qualification = ?,ContactNumber = ?,Address = ?,Phone = ?,emailid = ?,DateofBirth=?,updatedDate=now(),profilePhoto=?,countryId=?, stateId=?, cityId=?, pincode=? where userid = ?";
				if (rs != null)	rs.close();
				if (pstmt != null)	pstmt.close();
				pstmt = con.prepareStatement(sql);
				pstmt.setString(1, studentForm.getFirstName());
				pstmt.setString(2, studentForm.getLastName());
				pstmt.setString(3, studentForm.getGender());
				pstmt.setString(4, studentForm.getQualification());
				pstmt.setString(5, studentForm.getContactno());
				pstmt.setString(6, studentForm.getAddress());
				pstmt.setString(7, studentForm.getPhone());
				pstmt.setString(8, studentForm.getEmailId());
				pstmt.setString(9, studentForm.getDob());
				pstmt.setString(10, profilePhotoName);
				pstmt.setInt(11,studentForm.getCountryid());
				pstmt.setInt(12,studentForm.getStateid());
				pstmt.setInt(13,studentForm.getCityid());
				pstmt.setInt(14,studentForm.getPincode());
				pstmt.setLong(15, studentForm.getUserid());
				pstmt.executeUpdate();
				result = Common.REC_UPDATED;


			}




		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
				 
			} catch (Exception e2) {
			}
		}
		return result;
	}

	public List<StudentProfileForm> getCourseBasedOnIds(String courseIdsList,	DataSource dataSource) {
		List<StudentProfileForm> list = new ArrayList<StudentProfileForm>();
		StudentProfileForm loginForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
	
		
		try {
			
			System.out.println("kljhvc");
			con = dataSource.getConnection();
			String sql = "select CourseId,CourseName FROM coursemater where CourseId in ("+courseIdsList+");";
			System.out.println(sql);
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				loginForm = new StudentProfileForm();
				
				loginForm.setCourseid(rs.getInt("CourseId"));
				loginForm.setCoursename(rs.getString("CourseName"));
				
				list.add(loginForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
		
		
		
	}

	public String getCourseList(DataSource dataSource, Long userid) {
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		Connection con=null;
		String courseIdsList=null;
		try{
			con = dataSource.getConnection();
			String sql = "select CourseId FROM studentcoursedetails where StudentId=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, userid);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				courseIdsList=rs.getString("CourseId");
			}
		 
		} catch (Exception e) {
			e.printStackTrace();
		}	
		finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return courseIdsList;
	}

}
