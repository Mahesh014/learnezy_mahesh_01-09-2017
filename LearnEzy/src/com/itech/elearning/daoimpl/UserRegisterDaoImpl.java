package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.sql.DataSource;

import com.itech.elearning.forms.CompanyMasterForm;
import com.itech.elearning.forms.UserRegisterForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;
import com.itech.elearning.utils.EncryptUtil;

public class UserRegisterDaoImpl {

	public String addUser(UserRegisterForm userForm, DataSource ds) {

		String result = Common.FAILURE_MESSAGE;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = ds.getConnection();
			String sql = "SELECT * FROM USERMASTER WHERE USERNAME=? and ContactNumber=? and emailid=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, userForm.getUname());
			pstmt.setString(2, userForm.getContactno());
			pstmt.setString(3, userForm.getEmailId());
			System.out.println("********************"+sql);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				result = Common.DUPLICATE_NAME_MESSAGE;
			}else {
				sql = "INSERT INTO USERMASTER (UserName,Password,FIRSTNAME,LASTNAME,Gender,Qualification,ContactNumber,Address,Phone,emailid,RoleID,ACTIVE,registerddate, profilePhoto, countryId, stateId, cityId, pincode,otherCountry,otherState,otherCity,CompanyId) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,NOW(),?,?,?,?,?,?,?,?)";
				if (pstmt != null)pstmt.close();
				pstmt = con.prepareStatement(sql);
				pstmt.setString(1, userForm.getUname());
				pstmt.setString(2, EncryptUtil.encrypt(userForm.getPassword()));
				pstmt.setString(3, userForm.getFirstName());
				pstmt.setString(4, userForm.getLastName());
				pstmt.setString(5, userForm.getGender());
				pstmt.setString(6, userForm.getQualification());
				pstmt.setString(7, userForm.getContactno());
				pstmt.setString(8, userForm.getAddess());
				pstmt.setString(9, userForm.getPhone());
				pstmt.setString(10, userForm.getEmailId());
				pstmt.setLong(11, userForm.getRoleId());
				pstmt.setBoolean(12, true);
				Date date = new Date();
				String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(date);
			      System.out.println("Current Date: " +modifiedDate);
				pstmt.setString(13,modifiedDate);
				pstmt.setInt(14,userForm.getCountryid());
				pstmt.setInt(15,userForm.getStateid());
				pstmt.setInt(16,userForm.getCityid());
				pstmt.setInt(17,userForm.getPincode());
				pstmt.setString(18,userForm.getOthercountryname());
				pstmt.setString(19,userForm.getOtherstatename());
				pstmt.setString(20,userForm.getOthercityname());
				pstmt.setInt(21,userForm.getCompanyID());
				pstmt.executeUpdate();
				result = Common.REC_ADDED;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;

	}

	public List<UserRegisterForm> roleList(int roleid,String compid,DataSource ds) {
		List<UserRegisterForm> roleList = new ArrayList<UserRegisterForm>();
		UserRegisterForm userData = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			if(roleid == 9)
			{
		 sql = "SELECT * FROM rolemaster WHERE ACTIVE=TRUE ";
			}
			else
			{
			 sql = "SELECT * FROM rolemaster WHERE ACTIVE=TRUE";
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				userData = new UserRegisterForm();
				userData.setRoleId(rs.getLong("ROLEID"));
				userData.setRolename(rs.getString("ROLENAME"));
				userData.setActive(rs.getBoolean("ACTIVE"));
				roleList.add(userData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return roleList;
	}

	public List<UserRegisterForm> listAll(int roleid,String compid, Long userid, DataSource ds) {
		List<UserRegisterForm> userList = new ArrayList<UserRegisterForm>();
		UserRegisterForm userData = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			if(roleid == 9)
			{
		 sql = "SELECT u.otherCity,u.otherState,u.otherCountry,u.countryid,u.stateid,u.cityid,u.UserId,u.pincode,u.UserName ,u.Password , u.FirstName,  u.LastName, u.Gender, u.Qualification, u.ContactNumber, u.Address,u.Phone, u.RoleID, r.RoleName, u.emailid, u.Active, u.CompanyId FROM  USERMASTER AS u JOIN ROLEMASTER AS r ON (u.ROLEID = r.ROLEID)";
			}
			else
			{
			 sql = "SELECT u.otherCity,u.otherState,u.otherCountry,u.countryid,u.stateid,u.cityid,u.UserId,u.pincode,u.UserName ,u.Password , u.FirstName, u.LastName, u.Gender, u.Qualification, u.ContactNumber, u.Address,u.Phone, u.RoleID, r.RoleName, u.emailid, u.Active, u.CompanyId FROM  USERMASTER AS u JOIN ROLEMASTER AS r ON (u.ROLEID = r.ROLEID)and u.CompanyId = '"+compid+"'"; 
	
			}
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				userData = new UserRegisterForm();
				userData.setUserid(rs.getLong("u.USERID"));
				userData.setUname(rs.getString("u.USERNAME"));
				userData.setPassword(EncryptUtil.decrypt(rs.getString("u.PASSWORD")));
				userData.setFirstName(rs.getString("u.FIRSTNAME"));
				
				userData.setLastName(rs.getString("u.LASTNAME"));
				userData.setGender(rs.getString("u.GENDER"));
				userData.setQualification(rs.getString("u.Qualification"));
				userData.setContactno(rs.getString("u.ContactNumber"));
				userData.setEmailId(rs.getString("u.EMAILID"));
				userData.setPhone(rs.getString("u.Phone"));
				userData.setAddress(rs.getString("u.ADDRESS"));
				userData.setRoleId(rs.getLong("u.ROLEID"));
				userData.setRolename(rs.getString("r.ROLENAME"));
				userData.setActive(rs.getBoolean("u.Active"));
				userData.setCountryid(rs.getInt("u.countryid"));
				userData.setStateid(rs.getInt("u.stateid"));
				userData.setCityid(rs.getInt("u.cityid"));
				userData.setPincode(rs.getInt("u.pincode"));
				userData.setOthercountryname(rs.getString("u.otherCountry"));
				userData.setOtherstatename(rs.getString("u.otherState"));
				userData.setOthercityname(rs.getString("u.otherCity"));
				userData.setCompanyID(rs.getInt("u.companyID"));
				userList.add(userData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return userList;

	}

	public String changeStatus(UserRegisterForm userForm, DataSource ds) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = ds.getConnection();
			String sql = "UPDATE USERMASTER SET ACTIVE=? WHERE USERID=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setBoolean(1, userForm.isActive());
			pstmt.setLong(2, userForm.getUserid());
			pstmt.executeUpdate();
			result = Common.REC_STATUS_CHANGED;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return result;

	}

	public UserRegisterForm get(long id, DataSource ds) {
		UserRegisterForm userData = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ds.getConnection();
		/*	String sql = "SELECT cm.CourseName,u.CourseId,u.UserId,u.UserName ,u.Password , u.FirstName, u.MiddleName, u.LastName, u.DateofBirth ,u.Gender, u.Qualification, u.ContactNumber, u.Address,u.Phone, u.RoleID, r.RoleName, u.emailid, u.Active"
+" FROM  USERMASTER AS u JOIN ROLEMASTER AS r ON (u.ROLEID = r.ROLEID) join coursemater cm on u.CourseId = cm.CourseId where u.UserId = ?";*/

			String sql="SELECT u.UserId,u.UserName ,u.Password , u.FirstName,  u.LastName, u.DateofBirth ,u.Gender, u.Qualification, u.ContactNumber, u.Address,u.Phone, u.RoleID, r.RoleName, u.emailid, u.Active"+
" FROM  USERMASTER AS u JOIN ROLEMASTER AS r ON (u.ROLEID = r.ROLEID)  where u.UserId = ?";
			
			System.out.println(sql);
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, id);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				userData = new UserRegisterForm();
			//	userData.setCourseName(rs.getString("cm.CourseName"));
			//	userData.setCourseId(rs.getLong("u.CourseId"));
				userData.setUserid(rs.getLong("u.USERID"));
				userData.setUname(rs.getString("u.USERNAME"));
				userData.setPassword(EncryptUtil.decrypt(rs.getString("u.PASSWORD")));
				userData.setFirstName(rs.getString("u.FIRSTNAME"));
			//	userData.setMiddleName(rs.getString("u.MiddleName"));
				userData.setLastName(rs.getString("u.LASTNAME"));
				//userData.setDob(new java.text.SimpleDateFormat("dd-MM-yyyy")
					//	.format(rs.getTimestamp("u.DateofBirth")));
				
			//	userData.setDob(rs.getString("u.DateofBirth"));
				System.out.println("kkkk "+userData.getDob());
				
				userData.setGender(rs.getString("u.Gender"));
				userData.setQualification(rs.getString("u.Qualification"));
				userData.setContactno(rs.getString("u.ContactNumber"));
				userData.setAddess(rs.getString("u.address"));
				userData.setPhone(rs.getString("u.Phone"));
				userData.setEmailId(rs.getString("u.EMAILID"));
				userData.setRoleId(rs.getLong("u.ROLEID"));
				userData.setRolename(rs.getString("r.RoleName"));

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return userData;
	}

	public String updateUser(UserRegisterForm userForm, DataSource ds) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			sql = "SELECT * FROM USERMASTER WHERE FirstName =?  AND LastName=?  AND Gender = ? and Qualification = ? AND ContactNumber = ? AND Address = ? and Phone =? AND UserName = ? AND RoleID = ? AND emailid = ? AND profilePhoto=? AND  countryId=? AND  stateId=? AND  cityId=? AND  pincode=? and  password=? and otherCountry=? and otherState=? and otherCity=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, userForm.getFirstName());
			pstmt.setString(2, userForm.getLastName());
			pstmt.setString(3, userForm.getGender());
			pstmt.setString(4, userForm.getQualification());
			pstmt.setString(5, userForm.getContactno());
			pstmt.setString(6, userForm.getAddress());
			pstmt.setString(7, userForm.getPhone());
			pstmt.setString(8, userForm.getUname());
			pstmt.setLong(9, userForm.getRoleId());
			pstmt.setString(10, userForm.getEmailId());
			pstmt.setString(11,"");
			pstmt.setInt(12,userForm.getCountryid());
			pstmt.setInt(13,userForm.getStateid());
			pstmt.setInt(14,userForm.getCityid());
			pstmt.setInt(15,userForm.getPincode());
			pstmt.setString(16, EncryptUtil.encrypt(userForm.getPassword()));
			pstmt.setString(17,userForm.getOthercountryname());
			pstmt.setString(18,userForm.getOtherstatename());
			pstmt.setString(19,userForm.getOthercityname());
			rs = pstmt.executeQuery();
			if (rs.next()) result = Common.No_Change;
			else{
					sql = "UPDATE USERMASTER SET FirstName  = ?,LastName = ?,Gender = ?,Qualification = ?,ContactNumber = ?,Address = ?,Phone = ?,UserName = ?,RoleID = ?,emailid = ?, password=?,  profilePhoto=?, countryId=?, stateId=?, cityId=?, pincode=?, otherCountry=?, otherState=?, otherCity=? where userid = ?";
					if (rs != null)	rs.close();
					if (pstmt != null)	pstmt.close();
					pstmt = con.prepareStatement(sql);
					pstmt.setString(1, userForm.getFirstName());
					pstmt.setString(2, userForm.getLastName());
					pstmt.setString(3, userForm.getGender());
					pstmt.setString(4, userForm.getQualification());
					pstmt.setString(5, userForm.getContactno());
					pstmt.setString(6, userForm.getAddess());
					pstmt.setString(7, userForm.getPhone());
					pstmt.setString(8, userForm.getUname());
					pstmt.setLong(9, userForm.getRoleId());
					pstmt.setString(10, userForm.getEmailId());
					pstmt.setString(11, EncryptUtil.encrypt(userForm.getPassword()));
					pstmt.setString(12,"");
					pstmt.setInt(13,userForm.getCountryid());
					pstmt.setInt(14,userForm.getStateid());
					pstmt.setInt(15,userForm.getCityid());
					pstmt.setInt(16,userForm.getPincode());
					pstmt.setString(17,userForm.getOthercountryname());
					pstmt.setString(18,userForm.getOtherstatename());
					pstmt.setString(19,userForm.getOthercityname());
					pstmt.setLong(20, userForm.getUserid());
					
					pstmt.executeUpdate();
					result = Common.REC_UPDATED;
				} 
				

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	public List<UserRegisterForm> listAllCourse(int roleid,String compid, Long userid,
			DataSource ds) {
	 
				
		List<UserRegisterForm> courseList = new ArrayList<UserRegisterForm>();
		UserRegisterForm topicForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			if(roleid==9)
			{
		sql = "SELECT * FROM coursemater WHERE ACTIVE=TRUE ";
			}
			else
			{
			sql = "SELECT * FROM coursemater WHERE ACTIVE=TRUE and CompanyId = '"+compid+"'";
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				topicForm = new UserRegisterForm();
				topicForm.setCourseId(rs.getLong("CourseId"));
				topicForm.setCourseName(rs.getString("CourseName"));
				courseList.add(topicForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return courseList;
		
		
		
	}

	public int checkUserName(DataSource dataSource, String userName) {
		int flag=1;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM USERMASTER where username=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, userName);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				flag=0;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return flag;
	}

	public List<UserRegisterForm> listallcompany(DataSource dataSource) {
		
			List<UserRegisterForm> list = new ArrayList<UserRegisterForm>();
			UserRegisterForm companyForm = null;

			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;

			try {
				con = dataSource.getConnection();
				String sql = "SELECT * FROM companymaster ORDER BY CompanyName ASC";
				pstmt = con.prepareStatement(sql);
				rs = pstmt.executeQuery();
				while (rs.next()) {
					companyForm = new UserRegisterForm();
					companyForm.setCompanyID(rs.getInt("CompanyId"));
					companyForm.setCompanyName(rs.getString("CompanyName"));
					companyForm.setActive(rs.getBoolean("active"));
					list.add(companyForm);
				}

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					ConnectionUtil.closeResources(con, pstmt, rs);
				} catch (Exception e2) {
				}
			}
			return list;
		}

	public List<UserRegisterForm> listallcompanynew(String compid,
			DataSource dataSource) 
			{
		List<UserRegisterForm> list = new ArrayList<UserRegisterForm>();
		UserRegisterForm companyForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM companymaster where CompanyId = '"+compid+"' ORDER BY CompanyName ASC";
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				companyForm = new UserRegisterForm();
				companyForm.setCompanyID(rs.getInt("CompanyId"));
				companyForm.setCompanyName(rs.getString("CompanyName"));
				companyForm.setActive(rs.getBoolean("active"));
				list.add(companyForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return list;
		
	       }
	

}
