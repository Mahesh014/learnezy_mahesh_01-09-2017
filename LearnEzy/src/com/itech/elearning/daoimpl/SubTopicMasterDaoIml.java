package com.itech.elearning.daoimpl;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.sql.DataSource;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;

import com.itech.elearning.forms.SubTopicMasterForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class SubTopicMasterDaoIml {
	public List<SubTopicMasterForm> listAllCourse(int roleid,String compid,long userid, DataSource ds) {
		List<SubTopicMasterForm> courseList = new ArrayList<SubTopicMasterForm>();
		SubTopicMasterForm topicForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			if(roleid==9)
			{
			 sql = "SELECT * FROM coursemater WHERE ACTIVE=TRUE";
			}
			else
			{
				 sql = "SELECT * FROM coursemater WHERE ACTIVE=TRUE and CompanyId= '"+compid+"'";
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				topicForm = new SubTopicMasterForm();
				topicForm.setCourseId(rs.getLong("CourseId"));
				topicForm.setCourseName(rs.getString("CourseName"));
				courseList.add(topicForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return courseList;
	}

	public List<SubTopicMasterForm> listAllTopic(int roleid,String compid,long userid,DataSource ds, long courceid, long topicId) {
		List<SubTopicMasterForm> courseList = new ArrayList<SubTopicMasterForm>();
		SubTopicMasterForm topicData = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			if(roleid==9)
			{
			 sql = "SELECT * from topicmaster where active=true  and  courseid="+courceid ;
			System.out.println(topicId);
			if (topicId==0 && topicId==-1) {
				 sql = "SELECT * from topicmaster where active=true ";
			}
			}
			else
			{
				 sql = "SELECT * from topicmaster where active=true and  CompanyId='"+compid+"' and  courseid="+courceid ;
				System.out.println(topicId);
				if (topicId==0 && topicId==-1) {
					 sql = "SELECT * from topicmaster where active=true and CompanyId='"+compid+"'";
				}
			}
			System.out.println(sql);
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				topicData = new SubTopicMasterForm();
				topicData.setTopicId(rs.getLong("TopicID"));
				topicData.setTopicName(rs.getString("TopicName"));
				courseList.add(topicData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return courseList;
		
		
	}

	public List<SubTopicMasterForm> listSubTopic(int roleid,String compid,long userid, long topicId,	DataSource ds, long courseId) {
		List<SubTopicMasterForm> subTopicList = new ArrayList<SubTopicMasterForm>();
		SubTopicMasterForm subData = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		System.out.println(" Funtion Called By LIst");
		try {
			con = ds.getConnection();
			if(roleid == 9)
			{
			 sql = "SELECT * from subtopicmaster ";
			
			if ((courseId != -1 && courseId != 0) && (topicId==0 || topicId==-1)) {
				sql = "SELECT * from subtopicmaster where  CourseId ="+courseId;
			} else  if(courseId != -1 && courseId != 0 && topicId!=0 && topicId!=-1) {
				sql = "SELECT * from subtopicmaster where TopicID="+topicId+ " and CourseId ="+courseId;
			}
			}
			else
			{
				sql = "SELECT * from subtopicmaster where CompanyId = '"+compid+"'";
				
				if ((courseId != -1 && courseId != 0) && (topicId==0 || topicId==-1)) {
					sql = "SELECT * from subtopicmaster where  CourseId ="+courseId;
				} else  if(courseId != -1 && courseId != 0 && topicId!=0 && topicId!=-1) {
					sql = "SELECT * from subtopicmaster where TopicID="+topicId+ " and CourseId ="+courseId;
				}
			}
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				subData = new SubTopicMasterForm();
				subData.setTopicId(rs.getLong("TopicID"));
				subData.setSubTopicName(rs.getString("SubTopicName"));
				subData.setSubTopicId(rs.getLong("SubTopicID"));				
				subData.setCourseId(rs.getLong("CourseId"));
				subData.setActive(rs.getBoolean("Active"));
				subTopicList.add(subData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return subTopicList;
	}

	public String addSubTopic(SubTopicMasterForm subtopicForm,DataSource ds) {
		String result = Common.FAILURE_MESSAGE;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		Connection con = null;
		try {
			con = ds.getConnection();
			String sql1 = "SELECT * FROM subtopicmaster WHERE SubTopicName=? and CourseId =? and CompanyId = '"+subtopicForm.getCompid()+"' ";
			pstmt = con.prepareStatement(sql1);
			pstmt.setString(1, subtopicForm.getSubTopicName());
			pstmt.setLong(2, subtopicForm.getCourseId());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				result = Common.DUPLICATE_NAME_MESSAGE;
			} else {
				String sql = "INSERT INTO subtopicmaster (SubTopicName,CourseId,ACTIVE,TopicID,CompanyId) VALUES(?,?,?,?,'"+subtopicForm.getCompid()+"')";
				if (pstmt != null)	pstmt.close();
				pstmt = con.prepareStatement(sql);
				pstmt.setString(1, subtopicForm.getSubTopicName());
				pstmt.setLong(2, subtopicForm.getCourseId());
				pstmt.setBoolean(3, true);
				pstmt.setLong(4, subtopicForm.getTopicId());
				pstmt.executeUpdate();
				result = Common.REC_ADDED;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;
		
	
	}

	public String changeStatus(SubTopicMasterForm subtopicForm, long subtopicid,	DataSource ds) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = ds.getConnection();
			String sql = "UPDATE subtopicmaster SET ACTIVE=? WHERE SubTopicID=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setBoolean(1, subtopicForm.isActive());
			pstmt.setLong(2, subtopicid);
			pstmt.executeUpdate();
			result = Common.REC_STATUS_CHANGED;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	

	public Object updateSubTopic(SubTopicMasterForm subtopicForm,DataSource ds) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			sql="select * from subtopicmaster where SubTopicName  = ? and CourseId = ? and  TopicID=? ";
			pstmt = con.prepareStatement(sql);
			pstmt=con.prepareStatement(sql);
			pstmt.setString(1, subtopicForm.getSubTopicName());
			pstmt.setLong(2, subtopicForm.getCourseId());
			pstmt.setLong(3, subtopicForm.getTopicId());
			rs=pstmt.executeQuery();
			System.out.println(sql);
			if(rs.next())result = Common.No_Change;
			else {
				sql = "UPDATE subtopicmaster SET SubTopicName  = ?,CourseId = ?, TopicID=? where SubTopicID = ?";
				System.out.println(sql+subtopicForm.getSubTopicId());
				pstmt = con.prepareStatement(sql);
				pstmt.setString(1, subtopicForm.getSubTopicName());
				pstmt.setLong(2, subtopicForm.getCourseId());
				pstmt.setLong(3, subtopicForm.getTopicId());
				pstmt.setLong(4, subtopicForm.getSubTopicId());
				pstmt.executeUpdate();
				result = Common.REC_UPDATED;
			} 
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;
	}

		public String testTypeExcelUpload(String fileName, String filePath, String compid) {
			
			String result=Common.FAILURE_MESSAGE;
			   int daoflag=0;
			   Connection con=null;
			   PreparedStatement pstmt=null;
			   String temp_SubTopicName="";
               String temp_CourseId="";
               String temp_TopicID="";
               String temp_companyId="";
               ResultSet rs=null;
               boolean status=false;
			   try {
			con=ConnectionUtil.getMySqlConnection();
          long start = System.currentTimeMillis();
       
    	
    	String sql="insert into subtopicmaster(SubTopicName, CourseId, TopicID ,Active,CompanyID) values(?,?,?,?,?)";
         
       if(pstmt!=null)pstmt.close();
       pstmt=con.prepareStatement(sql);
       
    	InputStream input = new BufferedInputStream(new FileInputStream(filePath+fileName));
    	
    	 
    	System.out.println("Excel File path="+filePath);
    	System.out.println("Excel File Name="+fileName);
       POIFSFileSystem fs = new POIFSFileSystem(input);
       HSSFWorkbook wb = new HSSFWorkbook(fs);
       HSSFSheet sheet = wb.getSheetAt(0);
       Iterator<Row> rows = sheet.rowIterator();
     
       long count=0;
       while (rows.hasNext()) {
       		// flag=0;
       		 HSSFRow row = (HSSFRow) rows.next();
                
       		temp_SubTopicName=SubTopicMasterDaoIml.StringNotNull(row.getCell(0));
   		 System.out.println("temp_SubTopicName"+temp_SubTopicName);
   		 temp_CourseId=SubTopicMasterDaoIml.StringNotNull(row.getCell(1));
   		temp_TopicID=SubTopicMasterDaoIml.StringNotNull(row.getCell(2));

   		 System.out.println("temp_CourseId"+temp_CourseId);

                
                
                
                try {
               	 if(daoflag==1){
                 		count=count+1;
                    	System.out.println("excel file adding............");
              	       
                    	pstmt.setString(1,temp_SubTopicName);
              	       	pstmt.setString(2, temp_CourseId);
              	       	pstmt.setString(3, temp_TopicID);
              	       	pstmt.setBoolean(4, true);
              	       	pstmt.setString(5, compid);
	                   
	               
	                    pstmt.addBatch();
	                    
	                    
	                    if(count%1000 == 0) { 
	                    	pstmt.executeBatch();
	                    	System.out.println("Batch Count: "+count+ " Time Taken: "+(System.currentTimeMillis()-start)+" MilliSecons");
	                    }            
	                  //  flag=1;
	                    
	                    System.out.println("count="+count);
	                    result=count+" "+"Records uploaded";
	                    
	                    System.out.println("%%%%%%%%%%%%%"+result);
	                 }
            	} catch (Exception einner) {
            		einner.printStackTrace();
				}
                 
                 daoflag=1;
       }
       pstmt.executeBatch();
       System.out.println("Batch Count: "+count+ " Time Taken: "+(System.currentTimeMillis()-start)+" MilliSecons");
   
  } catch (Exception e) {
       e.printStackTrace();
   }
   finally{
       ConnectionUtil.closeResources(con, pstmt);
   }
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
      

       return result; 
       
       
       
       
       
       
       
       
       
       
		//return flag;
		   }    
		
		 public static String StringNotNull(HSSFCell rowValue){
			   String results="";
			   if(rowValue!=null) results =rowValue.toString().trim();
			   return results;
			   
		   }

	 

	
}
