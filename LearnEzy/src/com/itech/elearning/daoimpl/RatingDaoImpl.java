package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;



import com.itech.elearning.forms.RatingForm;

import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class RatingDaoImpl {

	public List<RatingForm> list(Long userid, DataSource dataSource) 
	{
		List<RatingForm> list = new ArrayList<RatingForm>();
		RatingForm roleForm = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
        String sql = null;
        String courseIdsList=null;
        try{		
    		con = dataSource.getConnection();
    		 sql = "select CourseId FROM studentcoursedetails where StudentId=?";
    		pstmt = con.prepareStatement(sql);
    		pstmt.setLong(1, userid);
    		rs = pstmt.executeQuery();
    		if(rs.next()) {
    			courseIdsList=rs.getString("CourseId");
    		}
    		
    		
    		if (rs != null) { rs.close(); }
    		if (pstmt != null) {	pstmt.close();}
    		sql = "select CourseId,CourseName FROM coursemater where CourseId in ("+courseIdsList+"); ";
    		pstmt = con.prepareStatement(sql);
    		System.out.println(sql);
    		rs = pstmt.executeQuery();
    		while (rs.next()) {
    			roleForm = new RatingForm();
				roleForm.setCourseid(rs.getInt("CourseId"));
				roleForm.setCoursename(rs.getString("CourseName"));


				list.add(roleForm);
    		}
    	} catch (Exception e) {
    		e.printStackTrace();
    	}	
    	finally {
    		ConnectionUtil.closeResources(con, pstmt, rs);
    	}
		return list;
	}

	public String addrating(long userid,RatingForm rateForm, DataSource dataSource, String emailid) 
	{
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		try	{
			con=dataSource.getConnection();
			String sql="select rating from ratingtable WHERE userId=? and courseId=?";//select query
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1,userid);
			pstmt.setLong(2,rateForm.getCourseid());
			ResultSet rs = pstmt.executeQuery();
			if(rs.next())
			{
				con=dataSource.getConnection();
			 sql="UPDATE ratingtable SET rating=? WHERE userId=? and courseId=?";//select query
				pstmt = con.prepareStatement(sql);
				pstmt.setString(1,rateForm.getRatingname());
				pstmt.setLong(2,userid);
				pstmt.setLong(3,rateForm.getCourseid());
				pstmt.executeUpdate();
	
				result =  Common.REC_UPDATED;
			}
			else{
				if(!rateForm.getRatingname().equalsIgnoreCase("") && !rateForm.getRatingname().equalsIgnoreCase(null)){
				sql="insert into ratingtable(userId,rating,Active,courseId,emailid) VALUES (?,?,?,?,?)";//select query
				pstmt=con.prepareStatement(sql);
				pstmt.setLong(1,userid);
				pstmt.setString(2,rateForm.getRatingname());
				pstmt.setBoolean(3,true);
				pstmt.setInt(4,rateForm.getCourseid());
				pstmt.setString(5,emailid);
				pstmt.executeUpdate();
				result =Common.REC_ADDED;
				}
			}
		}
		catch (Exception e){
		 e.printStackTrace();
		}
		finally{
			ConnectionUtil.closeResources(con, pstmt);
		}
		return result;
	}

}
