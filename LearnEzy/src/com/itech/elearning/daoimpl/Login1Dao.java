package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.itech.elearning.forms.Login1Form;
import com.itech.elearning.utils.ConnectionUtil;
import com.itech.elearning.utils.EncryptUtil;

public class Login1Dao {

	public List<Login1Form> login(String userName, String password,
			DataSource dataSource) {
		Login1Form loginform =null;
		String sql="";
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		
		 List<Login1Form> userCredential = new ArrayList<Login1Form>();
		try{
			con=dataSource.getConnection();
			
			sql="select * from usermaster where emailid=? and password=? ";
		
			pstmt=con.prepareStatement(sql);
                pstmt.setString(1, userName);
			pstmt.setString(2, EncryptUtil.encrypt(password));
			
			
			rs=pstmt.executeQuery();
			 if(rs.next())
			    {
			    	loginform=new Login1Form();
			    	
			    	loginform.setEmailid(rs.getString("emailid"));
			    	 System.out.println("emailid in dao"+rs.getString("emailid"));
			    	loginform.setPassword(rs.getString("password"));
			    	
			    	
			    	userCredential.add(loginform);
			    			   
			    }
			   
			   } catch (Exception e) {
				e.printStackTrace();
		} finally {
			
		ConnectionUtil.closeResources(con, pstmt, rs);
		}
			return  userCredential;
	}

}
