package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.elearning.forms.QuestionMasterForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class QuestionMasterDaoImpl {

	public List<QuestionMasterForm> listAllCourse(Long userid, DataSource ds) {
		List<QuestionMasterForm> courseList = new ArrayList<QuestionMasterForm>();
		QuestionMasterForm qForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ds.getConnection();
			String sql = "SELECT * FROM coursemater WHERE ACTIVE=TRUE";
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				qForm = new QuestionMasterForm();
				qForm.setCourseId(rs.getLong("CourseId"));
				qForm.setCourseName(rs.getString("CourseName"));
				courseList.add(qForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return courseList;
	}

	public List<QuestionMasterForm> listAllTopic(Long userid, Long courseId,
			DataSource ds) {
		List<QuestionMasterForm> topicList = new ArrayList<QuestionMasterForm>();
		QuestionMasterForm qForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			if (courseId != -1 || courseId != null)
				sql = "SELECT * FROM topicmaster WHERE ACTIVE=TRUE and CourseId="
						+ courseId + "";
			else
				sql = "SELECT * FROM TOPICMASTER WHERE ACTIVE=TRUE";
			pstmt = con.prepareStatement(sql);

			rs = pstmt.executeQuery();
			while (rs.next()) {
				qForm = new QuestionMasterForm();
				qForm.setTopicId(rs.getLong("TopicID"));
				qForm.setTopicName(rs.getString("TopicName"));

				topicList.add(qForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return topicList;
	}

	public List<QuestionMasterForm> listAllTest(Long userid, DataSource ds,
			Long topicId) {
		List<QuestionMasterForm> courseList = new ArrayList<QuestionMasterForm>();
		QuestionMasterForm testForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			if (topicId != null && topicId != 0 && topicId != -1) {
				sql = "SELECT TestId, Testname,TestType FROM testmaster  where TOPICID = "
						+ topicId + " and active = true ";
			} else {
				sql = "SELECT TestId, Testname, TestType FROM testmaster where active = true";
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				testForm = new QuestionMasterForm();
				testForm.setTestName(rs.getString("Testname"));
				testForm.setTestId(rs.getLong("TestId"));
				testForm.setTestType(rs.getLong("TestType"));
				System.out.println("Test Type :---------  "
						+ rs.getLong("TestType"));
				courseList.add(testForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return courseList;
	}

	@SuppressWarnings("unchecked")
	public List<QuestionMasterForm> listAllQuestion(Long userid, DataSource ds,
			Long testId) {

		List<QuestionMasterForm> testTypeList = new ArrayList<QuestionMasterForm>();
		QuestionMasterForm qForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		String sql = null;
		ResultSet rs = null;
		try {
			con = ds.getConnection();
			if (testId != -1 || testId != null) {
				sql = "SELECT q.*, t.Testname, tt.TestType, cc.CourseId, cc.CourseName, tt.TestTypeId, t.TopicId, tp.TopicName  FROM questionmaster q  JOIN testmaster AS t ON (q.TESTID=t.TESTID) JOIN testtypemaster tt ON (tt.TESTTYPEID=t.TESTTYPE) join topicmaster tp on (tp.TopicID = t.TopicId) join coursemater cc on (tp.CourseId = cc.CourseId)  where q.TestId = "
						+ testId + "";
			} else {
				sql = "SELECT q.*, t.Testname, tt.TestType, cc.CourseId, cc.CourseName, tt.TestTypeId, t.TopicId, tp.TopicName  FROM questionmaster q  JOIN testmaster AS t ON (q.TESTID=t.TESTID) JOIN testtypemaster tt ON (tt.TESTTYPEID=t.TESTTYPE) join topicmaster tp on (tp.TopicID = t.TopicId) join coursemater cc on (tp.CourseId = cc.CourseId) ";
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				qForm = new QuestionMasterForm();
				qForm.setTestType(rs.getLong("tt.TestTypeId"));
				qForm.setQuestionId(rs.getLong("QuestionId"));
				qForm.setTestTypeName(rs.getString("tt.TestType"));
				qForm.setQuestion(rs.getString("q.Question"));
				qForm.setAnsA(rs.getString("q.AnswerA"));
				qForm.setAnsB(rs.getString("q.AnswerB"));
				qForm.setAnsC(rs.getString("q.AnswerC"));
				qForm.setAnsD(rs.getString("q.AnswerD"));
				qForm.setCrctAns(rs.getString("q.CorrectAnswer"));
				qForm.setDescriptiveAns(rs.getString("q.DescriptiveAns"));
				qForm.setActive(rs.getBoolean("q.active"));
				qForm.setCourseId(rs.getLong("cc.CourseId"));
				qForm.setCourseName(rs.getString("cc.CourseName"));
				qForm.setTopicId(rs.getLong("t.TopicId"));
				qForm.setTopicName(rs.getString("tp.TopicName"));
				
				JSONObject obj=new JSONObject();
				obj.put("testType",rs.getLong("tt.TestTypeId"));
				obj.put("courseId",rs.getLong("cc.CourseId"));
				obj.put("topicId",rs.getLong("t.TopicId"));
				obj.put("questionId",rs.getLong("QuestionId"));
				obj.put("question",rs.getString("q.Question"));
				obj.put("ansA",rs.getString("q.AnswerA"));
				obj.put("ansB",rs.getString("q.AnswerB"));
				obj.put("ansC",rs.getString("q.AnswerC"));
				obj.put("ansD",rs.getString("q.AnswerD"));
				obj.put("crctAns",rs.getString("q.CorrectAnswer"));
				obj.put("descriptiveAns",rs.getString("q.DescriptiveAns"));
				qForm.setjEdit(obj.toString());
				testTypeList.add(qForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return testTypeList;
	}

	public String addQuestion(QuestionMasterForm testForm, DataSource dataSource) {
		String result = Common.FAILURE_MESSAGE;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;

		try {
			con = dataSource.getConnection();
			String sql1 = "SELECT Question FROM questionmaster WHERE Question=?";
			pstmt = con.prepareStatement(sql1);
			pstmt.setString(1, testForm.getQuestion());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				result = Common.DUPLICATE_NAME_MESSAGE;
			} else {
				String sql = "INSERT INTO questionmaster (TestId,Question,AnswerA,AnswerB,AnswerC,AnswerD,CorrectAnswer, DescriptiveAns,Active,TopicID) VALUES(?,?,?,?,?,?,?,?,?,?)";
				if (pstmt != null)
					pstmt.close();
				pstmt = con.prepareStatement(sql);
				System.out.println(sql);
				pstmt.setLong(1, testForm.getTestId());

				pstmt.setString(2, testForm.getQuestion());
				pstmt.setString(3, testForm.getAnsA());
				pstmt.setString(4, testForm.getAnsB());
				pstmt.setString(5, testForm.getAnsC());
				pstmt.setString(6, testForm.getAnsD());
				pstmt.setString(7, testForm.getCrctAns());
				pstmt.setString(8, testForm.getDescriptiveAns());
				pstmt.setBoolean(9, true);
				pstmt.setLong(10, testForm.getTopicId());
				pstmt.executeUpdate();
				result = Common.REC_ADDED;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	public Long getTestType(DataSource dataSource, Long testId) {

		Long result = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;

		try {
			con = dataSource.getConnection();
			String sql1 = "SELECT TestType FROM testmaster WHERE TestId=?";
			pstmt = con.prepareStatement(sql1);
			pstmt.setLong(1, testId);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				result = rs.getLong("TestType");
			}

		} catch (Exception e) {
			 e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	public String changeStatus(QuestionMasterForm courseForm, DataSource ds,
			long questid) {

		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = ds.getConnection();
			String sql = "UPDATE questionmaster SET ACTIVE=? WHERE QuestionId=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setBoolean(1, courseForm.isActive());
			pstmt.setLong(2, questid);
			pstmt.executeUpdate();
			result = Common.REC_STATUS_CHANGED;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	public Object updateTest(QuestionMasterForm testForm, DataSource ds) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		int count = 0;

		try {
			con = ds.getConnection();
			sql = "SELECT * FROM questionmaster WHERE Question =? AND QuestionId =? AND TestId=? and AnswerA = ?  and AnswerB = ? and AnswerC = ?  and AnswerD = ? and CorrectAnswer = ?  and DescriptiveAns = ? ";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, testForm.getQuestion());
			pstmt.setLong(2, testForm.getQuestionId());
			pstmt.setLong(3, testForm.getTestId());
			pstmt.setString(4, testForm.getAnsA());
			pstmt.setString(5, testForm.getAnsB());
			pstmt.setString(6, testForm.getAnsC());
			pstmt.setString(7, testForm.getAnsD());
			pstmt.setString(8, testForm.getCrctAns());
			pstmt.setString(9, testForm.getDescriptiveAns());

			rs = pstmt.executeQuery();
			System.out.println(sql);
			if (rs.next())
				count = -1;
			if (count != -1) {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				sql = "SELECT * FROM questionmaster WHERE Question=? AND QuestionId!=?";
				pstmt = con.prepareStatement(sql);
				pstmt.setString(1, testForm.getQuestion());
				pstmt.setLong(2, testForm.getQuestionId());
				rs = pstmt.executeQuery();
				if (rs.next())
					count = 1;
				if (count == 0) {
					sql = "UPDATE questionmaster SET Question  =?,TestId = ?,AnswerA = ?,AnswerB = ?, AnswerC =?, AnswerD =?, CorrectAnswer =?,DescriptiveAns = ?  where QuestionId = ?";
					if (rs != null)
						rs.close();
					if (pstmt != null)
						pstmt.close();
					System.out.println(sql);
					pstmt = con.prepareStatement(sql);
					pstmt.setString(1, testForm.getQuestion());
					pstmt.setLong(2, testForm.getTestId());
					pstmt.setString(3, testForm.getAnsA());
					pstmt.setString(4, testForm.getAnsB());
					pstmt.setString(5, testForm.getAnsC());
					pstmt.setString(6, testForm.getAnsD());
					pstmt.setString(7, testForm.getCrctAns());
					pstmt.setString(8, testForm.getDescriptiveAns());
					pstmt.setLong(9, testForm.getQuestionId());
					pstmt.executeUpdate();
					result = Common.REC_UPDATED;
				} else if (count == 1)
					result = Common.DUPLICATE_NAME_MESSAGE;

			} else
				result = Common.No_Change;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;

	}

	@SuppressWarnings("unchecked")
	public JSONArray questionByCourseid(DataSource dataSource, String courseId) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		JSONArray jarry= new JSONArray();

		try {
			con = dataSource.getConnection();
			sql = "SELECT * FROM questionmaster WHERE  CourseId = ?";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, courseId);
			rs=pstmt.executeQuery();
			while(rs.next()){
				JSONObject obj= new JSONObject();
				obj.put("QuestionId", rs.getString("QuestionId"));
				obj.put("Question", rs.getString("Question"));
				jarry.add(obj);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return jarry;
	}
}
