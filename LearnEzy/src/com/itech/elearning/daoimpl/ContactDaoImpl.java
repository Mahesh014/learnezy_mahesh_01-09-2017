package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

import com.itech.elearning.forms.ContactForm;
import com.itech.elearning.utils.ConnectionUtil;

public class ContactDaoImpl {

	public String add(ContactForm contactForm, DataSource dataSource) {
		String result = "Message Sending Failed";
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			String sql = "insert into contactmessage(Name, emailId, mobileNo, message, active ) values (?,?,?,?,?)";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, contactForm.getName());
			pstmt.setString(2, contactForm.getEmailId());
			pstmt.setString(3, contactForm.getMobileNo());
			pstmt.setString(4, contactForm.getMessage());
			pstmt.setBoolean(5, true);
			pstmt.executeUpdate();
			result = "Thanks for sending message..! we will revert back to you soon..?";
			
		} catch (Exception e) {

			e.printStackTrace();
		}

		finally {
			ConnectionUtil.closeResources(con, pstmt, rs);
		}
		return result;
	}

}
