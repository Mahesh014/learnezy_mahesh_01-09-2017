package com.itech.elearning.daoimpl;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javax.sql.DataSource;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFPictureData;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.PictureData;
import org.apache.poi.ss.usermodel.Row;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.elearning.forms.ContantMasterForm;
import com.itech.elearning.utils.Common;
import com.itech.elearning.utils.ConnectionUtil;

public class ContantMasterDaoIml {

	public List<ContantMasterForm> listAllCourse(int roleid,String compid,Long userid, DataSource ds) {
		List<ContantMasterForm> courseList = new ArrayList<ContantMasterForm>();
		ContantMasterForm contentForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			if(roleid==9)
			{
		 sql = "SELECT * FROM coursemater WHERE ACTIVE=TRUE";
			}
			else
			{
				 sql = "SELECT * FROM coursemater WHERE ACTIVE=TRUE and CompanyId='"+compid+"'";
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				contentForm = new ContantMasterForm();
				contentForm.setCourseId(rs.getLong("CourseId"));
				contentForm.setCourseName(rs.getString("CourseName"));
				courseList.add(contentForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return courseList;
	}

	public List<ContantMasterForm> listAllTopic(int roleid,String compid,Long userid,DataSource ds, Long courceid) {
		List<ContantMasterForm> courseList = new ArrayList<ContantMasterForm>();
		ContantMasterForm contentForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			if(roleid==9)
			{
	 sql = "SELECT * from topicmaster where ACTIVE=TRUE and courseid="+courceid ;
			}
			else
			{
				 sql = "SELECT * from topicmaster where ACTIVE=TRUE and CompanyId='"+compid+"' and courseid="+courceid ;
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				contentForm = new ContantMasterForm();
				contentForm.setTopicId(rs.getLong("TopicID"));
				contentForm.setTopicName(rs.getString("TopicName"));
				contentForm.setCourseId(rs.getLong("CourseId"));
				contentForm.setActive(rs.getBoolean("Active"));
				courseList.add(contentForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return courseList;
		
		
	}

	public List<ContantMasterForm> listSubTopic(int roleid,String compid,long userid, long topicId,DataSource dataSource, long courseId) {
		List<ContantMasterForm> courseList = new ArrayList<ContantMasterForm>();
		ContantMasterForm contentForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String  sql = null;
		try {
			con = dataSource.getConnection();
			if(roleid==9)
			{
		sql = "SELECT * from subtopicmaster where ACTIVE=TRUE and courseid="+courseId+" and topicID="+topicId;
			}
			else
			{
				sql = "SELECT * from subtopicmaster where ACTIVE=TRUE and courseid="+courseId+" and CompanyId='"+compid+"' and topicID="+topicId;
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				contentForm = new ContantMasterForm();
				contentForm.setSubTopicId(rs.getLong("SubTopicID"));
				contentForm.setSubTopicName(rs.getString("SubTopicName"));
				courseList.add(contentForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return courseList;
		
	}
	
	public List<ContantMasterForm> listContent(int roleid,String compid,Long userid, long subtopicID,	long topicId, DataSource ds, long courseId) {
		List<ContantMasterForm> subTopicList = new ArrayList<ContantMasterForm>();
		ContantMasterForm contentForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		System.out.println(" Funtion Called By LIst");
		try {
			con = ds.getConnection();
			if(roleid==9)
			{
		 sql = "SELECT * from ContentMaster cm inner join topicmaster tm on tm.topicid=cm.topicid inner join subtopicmaster stm on stm.SubTopicID=cm.SubTopicID  inner join  coursemater csm on csm.CourseId=cm.CourseId ";
			}
			else
			{
	 sql = "SELECT * from ContentMaster cm inner join topicmaster tm on tm.topicid=cm.topicid inner join subtopicmaster stm on stm.SubTopicID=cm.SubTopicID  inner join  coursemater csm on csm.CourseId=cm.CourseId where cm.CompanyId='"+compid+"'";
	
			}
			/*if ((courseId != -1 && courseId != 0) && (topicId==0 || topicId==-1)) {
				sql = "SELECT * from ContentMaster cm inner join topicmaster tm on tm.topicid=cm.topicid inner join subtopicmaster stm on stm.SubTopicID=cm.SubTopicID  inner join  coursemater csm on csm.CourseId=cm.CourseId where  cm.CourseId ="+courseId;
			} else  if(courseId != -1 && courseId != 0 && topicId!=0 && topicId!=-1) {
				sql = "SELECT * from ContentMaster cm inner join topicmaster tm on tm.topicid=cm.topicid inner join subtopicmaster stm on stm.SubTopicID=cm.SubTopicID  inner join  coursemater csm on csm.CourseId=cm.CourseId where cm.TopicID="+topicId+ " and cm.CourseId ="+courseId;
			}
			
			if(courseId != -1 && courseId != 0 && topicId!=0 && topicId!=-1 && subtopicID!=0 && subtopicID!=-1) {
				sql = "SELECT * from ContentMaster cm inner join topicmaster tm on tm.topicid=cm.topicid inner join subtopicmaster stm on stm.SubTopicID=cm.SubTopicID  inner join  coursemater csm on csm.CourseId=cm.CourseId where cm.TopicID="+topicId+ " and cm.CourseId ="+courseId+" and cm.SubtopicID="+subtopicID;
			}*/
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				contentForm = new ContantMasterForm();
				
				contentForm.setCourseId(rs.getLong("CourseId"));
				contentForm.setCourseName(rs.getString("CourseName"));
				
				contentForm.setTopicId(rs.getLong("TopicID"));
				contentForm.setTopicName(rs.getString("TopicName"));
				
				contentForm.setContentId(rs.getLong("ContentID"));
				contentForm.setContentText(rs.getString("contentText"));
				
				contentForm.setSubTopicId(rs.getLong("SubTopicID"));				
				contentForm.setSubTopicName(rs.getString("SubTopicName"));
				
				contentForm.setActive(rs.getBoolean("Active"));
				
				subTopicList.add(contentForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return subTopicList;
	}

	public String addcontant(ContantMasterForm subtopicForm, DataSource ds, String imagefileName, String videofileName,String compid) {
		String result = Common.FAILURE_MESSAGE;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = ds.getConnection();
			String sql = "SELECT * FROM ContentMaster WHERE SubTopicID=? and CourseId =? and topicID=? and CompanyId = '"+compid+"'";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, subtopicForm.getSubTopicId());
			pstmt.setLong(2, subtopicForm.getCourseId());
			pstmt.setLong(3, subtopicForm.getTopicId());
			rs = pstmt.executeQuery();
			if (rs.next()) result = Common.DUPLICATE_NAME_MESSAGE;
			 else {
				sql = "INSERT INTO ContentMaster (CourseId,TopicID,subTopicId,contentText,contentImages,contentVideos,YoutubeLink,ACTIVE,CompanyId) VALUES(?,?,?,?,?,?,?,?,?)";
				if (pstmt != null)	pstmt.close();
				pstmt = con.prepareStatement(sql);
				pstmt.setLong(1, subtopicForm.getCourseId());
				pstmt.setLong(2, subtopicForm.getTopicId());
				pstmt.setLong(3, subtopicForm.getSubTopicId());
				pstmt.setString(4, subtopicForm.getContentText());
				pstmt.setString(5, imagefileName);
				pstmt.setString(6, videofileName);
				pstmt.setString(7, subtopicForm.getYouTubeLink());
				pstmt.setBoolean(8, true);
				pstmt.setString(9,compid);
				pstmt.executeUpdate();
				result = Common.REC_ADDED;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);

			} catch (Exception e2) {
			}
		}
		return result;
		
	
	}

	public String changeStatus(ContantMasterForm contentForm, long contentId,	boolean active, DataSource ds) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = ds.getConnection();
			String sql = "UPDATE ContentMaster SET ACTIVE=? WHERE contentId=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setBoolean(1,active);
			pstmt.setLong(2, contentId);
			pstmt.executeUpdate();
			result = Common.REC_STATUS_CHANGED;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	public Object updateContent(ContantMasterForm contentForm,DataSource ds, String imagefileName, String videofileName) {
		String result = Common.FAILURE_MESSAGE;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = ds.getConnection();
			sql = "SELECT * FROM ContentMaster WHERE subTopicID=? and CourseId =? and topicID=? and ContentText=? and contentImages=? and contentVideos=? and YoutubeLink=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1,contentForm.getSubTopicId());
			pstmt.setLong(2,contentForm.getCourseId());
			pstmt.setLong(3,contentForm.getTopicId());
			pstmt.setString(4,contentForm.getContentText());
			pstmt.setString(5,imagefileName);
			pstmt.setString(6,videofileName);
			pstmt.setString(7,contentForm.getYouTubeLink());
			rs=pstmt.executeQuery();
			if(rs.next())  result = Common.No_Change;
			else{
				if (imagefileName!="" && videofileName !="") {
					sql = "UPDATE ContentMaster SET CourseId=?, TopicID=?, subTopicID=?, ContentText =?, contentImages=?, contentVideos=?, YoutubeLink=? where ContentID = ?";
					if (pstmt != null) pstmt.close();
					pstmt = con.prepareStatement(sql);
					pstmt.setLong(1,contentForm.getCourseId());
					pstmt.setLong(2,contentForm.getTopicId());
					pstmt.setLong(3,contentForm.getSubTopicId());
					pstmt.setString(4,contentForm.getContentText());
					pstmt.setString(5,imagefileName);
					pstmt.setString(6,videofileName);
					pstmt.setString(7,contentForm.getYouTubeLink());
					pstmt.setLong(8,contentForm.getContentId());
					pstmt.executeUpdate();
					result = Common.REC_UPDATED;
				}else
				if (imagefileName!="" && videofileName =="") {
					sql = "UPDATE ContentMaster SET CourseId=?, TopicID=?, subTopicID=?, ContentText =?, contentImages=? , YoutubeLink=? where ContentID = ?";
					if (pstmt != null) pstmt.close();
					pstmt = con.prepareStatement(sql);
					pstmt.setLong(1,contentForm.getCourseId());
					pstmt.setLong(2,contentForm.getTopicId());
					pstmt.setLong(3,contentForm.getSubTopicId());
					pstmt.setString(4,contentForm.getContentText());
					pstmt.setString(5,imagefileName);
					pstmt.setString(6,contentForm.getYouTubeLink());
					pstmt.setLong(7,contentForm.getContentId());
					pstmt.executeUpdate();
					result = Common.REC_UPDATED;
					
				}else
				if (imagefileName=="" && videofileName !="") {
					sql = "UPDATE ContentMaster SET CourseId=?, TopicID=?, subTopicID=?, ContentText =?, contentVideos=?, YoutubeLink=? where ContentID = ?";
					if (pstmt != null) pstmt.close();
					pstmt = con.prepareStatement(sql);
					pstmt.setLong(1,contentForm.getCourseId());
					pstmt.setLong(2,contentForm.getTopicId());
					pstmt.setLong(3,contentForm.getSubTopicId());
					pstmt.setString(4,contentForm.getContentText());
					pstmt.setString(5,videofileName);
					pstmt.setString(6,contentForm.getYouTubeLink());
					pstmt.setLong(7,contentForm.getContentId());
					
					pstmt.executeUpdate();
					result = Common.REC_UPDATED;
				}else{
				
					sql = "UPDATE ContentMaster SET CourseId=?, TopicID=?, subTopicID=?, ContentText =? , YoutubeLink=? where ContentID = ?";
					if (pstmt != null) pstmt.close();
					pstmt = con.prepareStatement(sql);
					pstmt.setLong(1,contentForm.getCourseId());
					pstmt.setLong(2,contentForm.getTopicId());
					pstmt.setLong(3,contentForm.getSubTopicId());
					pstmt.setString(4,contentForm.getContentText());
					pstmt.setString(5,contentForm.getYouTubeLink());
					pstmt.setLong(6,contentForm.getContentId());
					
					pstmt.executeUpdate();
					result = Common.REC_UPDATED;
				}
				System.out.println(sql);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return result;
	}

	

	@SuppressWarnings("unchecked")
	public JSONObject getEdit(DataSource dataSource, long contentID) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		JSONObject obj= new JSONObject();
		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM  ContentMaster where ContentID=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, contentID);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				System.out.println("INSIDE JSON");
				 obj.put("cid",rs.getLong("CourseId"));
				 obj.put("tid",rs.getLong("TopicID"));
				 obj.put("sid",rs.getLong("SubTopicID"));
				 obj.put("conttxt",rs.getString("contentText"));
				 obj.put("imageFile",rs.getString("contentImages"));
				 obj.put("videoFile",rs.getString("contentVideos"));
				 obj.put("youTubeLink",rs.getString("YoutubeLink"));
			}
			
			System.out.println(obj.get("cid"));;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return obj;
	}

	@SuppressWarnings("unchecked")
	public JSONArray getTopic(DataSource dataSource, long courseId) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		JSONArray returnValue= new JSONArray();
		JSONObject obj= null;
		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM  topicmaster where CourseId=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, courseId);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				System.out.println("INSIDE JSON");
				 obj=new JSONObject();
				 obj.put("tid",rs.getLong("TopicID"));
				 obj.put("topicName",rs.getString("TopicName"));
				 returnValue.add(obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return returnValue;
	}

	@SuppressWarnings("unchecked")
	public JSONArray getSubTopic(DataSource dataSource, long topicId) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		JSONArray returnValue= new JSONArray();
		JSONObject obj= null;
		try {
			con = dataSource.getConnection();
			String sql = "SELECT * FROM  subtopicmaster where TopicID=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, topicId);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				System.out.println("INSIDE JSON");
				 obj=new JSONObject();
				 obj.put("sid",rs.getLong("SubTopicID"));
				 obj.put("subTopicName",rs.getString("SubTopicName"));
				 returnValue.add(obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return returnValue;
	}

	

	public String excelUpload(DataSource dataSource,
			List<ContantMasterForm> ld, String filename, int n, int rowNo,
			int countofvalues, int totalcount, String compid) {
		
		
		String result=Common.FAILURE_MESSAGE;		   
		Connection con=null;
		   PreparedStatement pstmt=null;
		   boolean flag=false;
		   ResultSet rs=null;
		   int no=5;
		   int temp=6;
		   String sql="";
		   String roleName="";
		   boolean res=false;
		  String catName=null;
		   try {
			   con = dataSource.getConnection();
			   
			  
			   
			   sql="select * from contentmaster where contentText=? ";
			   for(ContantMasterForm contentmaster : ld){
			   pstmt=con.prepareStatement(sql);
	            pstmt.setString(1, contentmaster.getContentText());
	           rs=pstmt.executeQuery();
	            
	          
	            if(rs.next())
	            {
	            result = "Duplicate Entry";
	            res=true;
	            }
			   }
			   if(res==false){
					   for(ContantMasterForm contentmaster : ld){
				         	 sql="insert into contentmaster(CourseId,TopicID,SubTopicID,contentImages,contentVideos,contentText,YoutubeLink,Active,CompanyId) values(?,?,?,?,?,?,?,?,?)";

	            pstmt=con.prepareStatement(sql);
	            pstmt.setLong(1, contentmaster.getCourseId());
	            pstmt.setLong(2, contentmaster.getTopicId());
	            pstmt.setLong(3, contentmaster.getSubTopicId());

	          /* if(totalcount2==1){
	        	   pstmt.setString(2, filename2);
	           }else{*/
	            String tempimage[]=filename.split(",");
	        	   System.out.println("ppppppppppppp"+tempimage[tempimage.length-1]);
	        	   pstmt.setString(4, tempimage[tempimage.length-1]);
                pstmt.setString(5, contentmaster.getVideofilename());
	            pstmt.setString(6, contentmaster.getContentText());
	            pstmt.setString(7, contentmaster.getYouTubeLink());
            pstmt.setBoolean(8, true);
            pstmt.setString(9, compid);

	           
	        	
	            
	            pstmt.executeUpdate();
	            result=Common.REC_ADDED;
	    		 }
	            
	            
			   }
		   }
			   

             
		   
       catch (Exception e) {
	            e.printStackTrace();
	        }
       finally{
	            ConnectionUtil.closeResources(con, pstmt);
	        }
	        return result; 

	}
}
