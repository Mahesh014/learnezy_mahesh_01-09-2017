package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

import com.itech.elearning.utils.Common;

public class TestQuestionMasterDao {

	public String addQuestions(String compid,String courseId, String topicId,
			String subTopicId, String testName, String[] question, String[] ansA,
			String[] ansB, String[] ansC, String[] ansD, String[] crctAns,
			DataSource dataSource) {
		// TODO Auto-generated method stub
		String result=null;
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		String sql=null;
		try
		{
			
			
			
			con=dataSource.getConnection();
			for(int i=0;i<question.length;i++)
			{
				
			sql="select * from TestmasterOverall where TopicID=? and Question=? and CourseId=? and CompanyId = '"+compid+"'";
			pstmt=con.prepareStatement(sql);
			pstmt.setString(1, topicId);
			pstmt.setString(2, question[i]);
			pstmt.setString(3, courseId);
			rs=pstmt.executeQuery();
	
			if(rs.next())
			{
				result=Common.DUPLICATE_NAME_MESSAGE;
			}else
			{
			 sql="insert into TestmasterOverall(TopicID,Question,AnswerA,AnswerB,AnswerC,AnswerD,CorrectAnswer,Active,CourseId,CompanyId) values (?,?,?,?,?,?,?,?,?,?)";
		     pstmt=con.prepareStatement(sql);
		     pstmt.setString(1, topicId);
		     pstmt.setString(2, question[i]);
		     pstmt.setString(3, ansA[i]);
		     pstmt.setString(4, ansB[i]);
		     pstmt.setString(5, ansC[i]);
		     pstmt.setString(6, ansD[i]);
		     pstmt.setString(7, crctAns[i]);
		     pstmt.setBoolean(8, true);
		     pstmt.setString(9, courseId);
		     pstmt.setString(10,compid);
		     pstmt.executeUpdate();
		 	result=Common.REC_ADDED;
			}
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}finally
		{
			try
			{
				if(rs!=null)rs.close();
				if(pstmt!=null)pstmt.close();
				if(con!=null)con.close();
			}catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		return result;
	}

}
