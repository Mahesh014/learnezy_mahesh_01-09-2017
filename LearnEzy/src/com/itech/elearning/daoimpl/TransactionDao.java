package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

import com.itech.elearning.utils.ConnectionUtil;

public class TransactionDao {
	public int addTransaction(DataSource dataSource, String courseList, Long userId, int tax, int transactionStatus, int totalAmount, String compid) {
		int transactionID=0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		
		try {
				con = dataSource.getConnection();
				String sql = "INSERT INTO transactionmaster (TotalAmount, Userid, CoursesId, Tax, PGIID, CardNumber, TransactionStatus, CreatedDateTime,CompanyId ) VALUES(?,?,?,?,?,?,?, now(),'"+compid+"')";
				pstmt = con.prepareStatement(sql);
				pstmt.setLong(1, totalAmount);
				pstmt.setLong(2, userId);
				pstmt.setString(3, courseList);
				pstmt.setLong(4, tax);
				pstmt.setString(5, "");
				pstmt.setString(6, "");				
				pstmt.setLong(7, transactionStatus);
				//pstmt.setString(9,compid);
				pstmt.executeUpdate();
				System.out.println(sql);
				
				sql = "SELECT max(transactionID) as transID FROM transactionmaster WHERE USERID=?";
				pstmt = con.prepareStatement(sql);
				pstmt.setLong(1, userId);
				rs=pstmt.executeQuery();
				if(rs.next()){
					transactionID=rs.getInt("transID");
				}
				System.out.println(sql);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		
		return transactionID;
	}
	
	
	public int  updateTransactionStatus(DataSource dataSource, String transactionID, int transactionStatus, String cardNumber, String pGIID){
		int flag=0;
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = dataSource.getConnection();
			String sql = "UPDATE transactionmaster SET PGIID=?, CardNumber=?, TransactionStatus=?, TransactionDateTime=NOW() WHERE TransactionID=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, pGIID);
			pstmt.setString(2, cardNumber);
			pstmt.setLong(3, transactionStatus);
			pstmt.setString(4, transactionID);
			pstmt.executeUpdate();
			
			flag=1;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt);
			} catch (Exception e2) {
			}
		}
		return flag;

	}
}
