package com.itech.elearning.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.itech.elearning.forms.TestReportsForm;
import com.itech.elearning.utils.ConnectionUtil;
import com.sun.org.apache.regexp.internal.recompile;

public class TestReportsDaoImpl {
	public List<TestReportsForm> studentList(int roleid,String compid, Long userid, DataSource dataSource) {
		
		List<TestReportsForm> studentList = new ArrayList<TestReportsForm>();
		TestReportsForm userData = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();
			if(roleid==9)
			{
			 sql = "SELECT * FROM USERMASTER where roleid=3";
			}
			else
			{
				 sql = "SELECT * FROM USERMASTER where roleid=3 and CompanyId= '"+compid+"' ";

			}
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				userData = new TestReportsForm();
				userData.setUserid(rs.getLong("USERID"));
				userData.setFirstName(rs.getString("FIRSTNAME")+" "+rs.getString("LASTNAME"));
				studentList.add(userData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return studentList;
	}

	
	public List<TestReportsForm> listAllCourse(int roleid,String compid, Long userid,DataSource dataSource) {
		List<TestReportsForm> courseList = new ArrayList<TestReportsForm>();
		TestReportsForm cForm = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			con = dataSource.getConnection();
			if(roleid==9)
			{
			 sql = "SELECT * FROM coursemater WHERE ACTIVE=TRUE ";
			}
			else
			{
				 sql = "SELECT * FROM coursemater WHERE ACTIVE=TRUE and CompanyId = '"+compid+"'";
			}
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			System.out.println(sql);
			while (rs.next()) {
				cForm = new TestReportsForm();
				cForm.setCourseId(rs.getLong("CourseId"));
				cForm.setCourseName(rs.getString("CourseName"));
				courseList.add(cForm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
			}
		}
		return courseList;
		}

	
	public List<TestReportsForm> testList(int roleid,String compid, Long userid, DataSource dataSource, String studentId, String courseId, String fromDate, String todate) {
		List<TestReportsForm> testList = new ArrayList<TestReportsForm>(); 
		TestReportsForm testDate = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sqlStudentId="";
		String sqlcourseId="";
		String sqlDate="";
		String sql = null;
		boolean sqlConditionFlag=false;
		try {
			con = dataSource.getConnection();
			
			if (!studentId.equals("") && !studentId.equals("-1") && !studentId.equals("0")) {
				sqlStudentId= " AND ta.STUDENTID='"+studentId+"' ";
				sqlConditionFlag=true;
			}
			if (!courseId.equals("") && !courseId.equals("-1") && !courseId.equals("0")) {
				sqlcourseId= " AND tm.CourseId='"+courseId+"' ";
				sqlConditionFlag=true;
			}
			if (!fromDate.equals("") && !todate.equals("") && !fromDate.equals("0000-00-00") && !todate.equals("0000-00-00")) {
				sqlDate= " AND ta.testTakenDate between '"+fromDate+"' and '"+todate+"' ";
				sqlConditionFlag=true;
			}
			
			if (!fromDate.equals("") && !todate.equals("") && !fromDate.equals("0000-00-00") && !todate.equals("0000-00-00")) {
				sqlDate= " AND ta.testTakenDate between '"+fromDate+"' and '"+todate+"' ";
				sqlConditionFlag=true;
			}
			String sqlCondition="";
			if (sqlConditionFlag) {
				sqlCondition=" Where "+(sqlStudentId+sqlcourseId+sqlDate).substring(4, (sqlStudentId+sqlcourseId+sqlDate).length());;	
			}
			System.out.println("SQL CONDITION------------> "+sqlCondition);
			if(roleid==9)
			{
			 sql = "select count(ta.testid) as noOfTest, max(ta.Score) as maxScore, max(ta.passPercentage) as passPercentage,ta.passStatus, tm.testname, tm.MinScore  from testmaster tm left join testanswer ta on ta.testId=tm.testId  "+sqlCondition+" and ta.CompanyId = '"+compid+"' group by tm.testId";
			}
			else
			{
				 sql = "select count(ta.testid) as noOfTest, max(ta.Score) as maxScore, max(ta.passPercentage) as passPercentage,ta.passStatus, tm.testname, tm.MinScore  from testmaster tm left join testanswer ta on ta.testId=tm.testId  "+sqlCondition+" and ta.CompanyId = '"+compid+"' group by tm.testId";
		
			}
			pstmt = con.prepareStatement(sql);
			System.out.println(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				testDate = new TestReportsForm();
				testDate.setNumTestTaken(rs.getInt("noOfTest"));
				testDate.setTestName(rs.getString("tm.testname"));
				testDate.setMaxScore(rs.getString("maxScore"));
				testDate.setPassPercentage(rs.getInt("passPercentage"));
				testDate.setPassStatus(rs.getString("ta.passStatus"));
				testList.add(testDate);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeResources(con, pstmt, rs);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return testList;
	}

	

}
