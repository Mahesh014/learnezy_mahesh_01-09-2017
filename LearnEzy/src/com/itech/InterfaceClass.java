package com.itech;

public class InterfaceClass implements InterfaceExample {

	public int mul(int a, int b) {
		return a*b;
	}

	public int sum(int a, int b) {
		return a+b;
	}

	public int sub(int a, int b) {
		return a-b;
	}

}
