<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->

<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200"> 
  
  <div id="wrapper"> 
    
    <!-- HEADER -->
    <header class="header">
      <div class="top-header">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <!--<div class="top-header__contacts"><i class="icon stroke icon-Phone2"></i>(123) 0800 12345</div>-->
              <div class="top-header__contacts"><i class="icon stroke icon-Mail"></i><a href="mailto:Info@learnezy.com">Info@learnezy.com</a></div>
              <div class="top-header__link">
                <button class="btn-header btn-effect">LATEST</button>
                <span>We have added new courses today ...</span> </div>
              <div class="header-login"> <a class="header-login__item" href="javascript:void(0);"><i class="icon stroke icon-User"></i>Sign In</a> <a class="header-login__item" href="javascript:void(0);">Register</a> </div>
            </div>
            <!-- end col  --> 
          </div>
          <!-- end row  --> 
        </div>
        <!-- end container  --> 
      </div>
      <!-- end top-header  -->
      
      <div class="container">
        <div class="row">
          <div class="col-xs-12"> <a class="header-logo" href="index.html"><img class="header-logo__img" src="assets/img/logo.png" alt="Logo"></a>
            <div class="header-inner">
              <div class="header-search">
                <div class="navbar-search">
                  <form id="search-global-form">
                    <div class="input-group">
                      <input type="text" placeholder="Type your search..." class="form-control" autocomplete="off" name="s" id="search" value="">
                      <span class="input-group-btn">
                      <button type="reset" class="btn search-close" id="search-close"><i class="fa fa-times"></i></button>
                      </span> </div>
                  </form>
                </div>
              </div>
              <a id="search-open" href="#fakelink"><i class="icon stroke icon-Search"></i></a> <a class="header-cart" href="/"> <i class="icon stroke icon-ShoppingCart"></i></a>
               <nav class="navbar yamm">
                <div class="navbar-header hidden-md  hidden-lg  hidden-sm ">
                  <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                </div>
                <div id="navbar-collapse-1" class="navbar-collapse collapse">
                  <ul class="nav navbar-nav">
                    <li class="dropdown"><a href="home.html">Home<span class="nav-subtitle">OUR World</span></a>
                     
                    </li>
                    <li class="dropdown"> <a href="courses-1.html">COURSES<span class="nav-subtitle">What We Offers</span></a>
                      <ul role="menu" class="dropdown-menu">
                        <li><a href="courses-category.html">courses category</a>
                          
                        </li>
                        <li><a href="course-details.html">course details</a></li>
                      </ul>
                    </li>
                   
                    <li><a href="contact.html">CONTACT<span class="nav-subtitle">say us hi</span></a></li>
                  </ul>
                </div>
              </nav>
              <!--end navbar --> 
              
            </div>
            <!-- end header-inner --> 
          </div>
          <!-- end col  --> 
        </div>
        <!-- end row  --> 
      </div>
      <!-- end container--> 
    </header>
    <!-- end header -->
    
     <div class="wrap-title-page">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h1 class="ui-title-page">Register to become a member of Learnezy</h1>
          </div>
        </div>
      </div>
      <!-- end container--> 
    </div>
    <!-- end wrap-title-page -->
    
    <div class="section-breadcrumb">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="wrap-breadcrumb clearfix">
              <ol class="breadcrumb">
                <li><a href="index.html"><i class="icon stroke icon-House"></i></a></li>
                <li><a href="register.html">Registration</a></li>
                <li class="active">Freelancer Registration</li>
                
              </ol>
            </div>
          </div>
        </div>
        <!-- end row --> 
      </div>
      <!-- end container --> 
    </div>
    <!-- end section-breadcrumb -->
       
    
    <div class="main-content">
          
              <div class="container">
    <div class="buttons">
            	
       	  <button class="btn1 login-btn"><i class="fa fa-facebook"></i>Login with Facebook</button>
                
                <button class="btn2 login-btn"><i class="fa fa-google-plus"></i>Login with Google</button>
                
                <button class="btn3 login-btn"><i class="fa fa-yahoo"></i>Login with Yahoo</button>
                
                <button class="btn4 login-btn"><i class="fa fa-linkedin"></i>Login with Linkein</button>
                
                <button class="btn5 login-btn"><i class="fa fa-windows"></i>Login with Window Live</button>
               
            </div>
            