<%@ page language="java" contentType="text/html; charset=ISO-8859-1"   pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ page import="java.io.*" %>
<%@ page import="java.net.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.*" %>
<%@ page import="org.w3c.dom.*" %>
<%@ page import="org.xml.sax.*" %>
<%@ page import="sun.misc.BASE64Encoder" %>
<%@ page import="javax.xml.parsers.*" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<b style="color: red;"><logic:notEmpty name="status"><bean:write name="status"/> </logic:notEmpty></b><br></br>
Course List : <%=session.getAttribute("courseList") %><br>
Userid  : <%=session.getAttribute("userId") %><br>
Total Amount  : <%=session.getAttribute("totalAmount") %><br>
tax  : <%=session.getAttribute("tax") %><br>
transactionID  : <%=session.getAttribute("transactionID") %><br>
subAmount  : <%=session.getAttribute("subAmount") %><br>
discountAmount  : <%=session.getAttribute("discountAmount") %><br>
serviceTax  : <%=session.getAttribute("serviceTax") %><br>
<a href="<%=request.getContextPath()%>/studentRegistration.do?action=PGI">CLICK</a>

<%-- <%
try
{
PrintWriter ot = response.getWriter();
URLConnection con;
URL url = null;
String vVenderURL;
String xmlURL = "";
String xmlttype = "";
String xmltempTxnId = "";
String xmltoken = "";
String xmltxnStage = "";
//String vOlnTxnNo = request.getParameter("iOlnRefNo");
//String vPayTo = request.getParameter("iPayTo");
//String vState = request.getParameter("iState");
//String vStateCd = request.getParameter("iStateCd");
//String vOlnAmt = request.getParameter("iOlnAmt");
//String vOlnPayMode = request.getParameter("iPayMode");


String vOlnTxnNo ="1234";
String vPayTo = "12345";
String vState = "kar";
String vStateCd = "bangalore";
String vOlnAmt = "0";
String vOlnPayMode = "credit";

out.println("<br><b>Online Reference  : </b>"+vOlnTxnNo);
out.println("<br><b>Payment To        : </b>"+vPayTo);
out.println("<br><b>State Name        : </b>"+vState);
out.println("<br><b>State Code        : </b>"+vStateCd);
out.println("<br><b>Amount            : </b>"+vOlnAmt);
out.println("<br><b>Mode              : </b>"+vOlnPayMode);
out.println("<br><br>");

String b64ClientCode = new BASE64Encoder().encode("shcil".getBytes("UTF-8"));
SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");  
String CurrDateTime = sdf.format(new Date()).toString();

vVenderURL = "http://203.114.240.183/paynetz/epi/fts?login=160&pass=Test@123&ttype=NBFundTransfer&prodid=NSE"
		+ "&amt="+vOlnAmt+"&txncurr=INR&txnscamt=0&clientcode="+b64ClientCode+"&txnid="+vOlnTxnNo
		+ "&date="+CurrDateTime+"&custacc=888888888888";
vVenderURL = vVenderURL.replace(" ","%20");
System.out.println(vVenderURL);
out.println("<br>Vender URL is : <br>");
out.println("<b>"+vVenderURL+"</b>");
url = new URL(vVenderURL.toString());

con = url.openConnection(Proxy.NO_PROXY);
String CntType = con.getContentType();
System.out.println("Cnt Type "+CntType+ " Length : " + con.getContentLength());

if (con.getContentLength() != 0 )
{
	System.out.println("Getting Content");
	
	BufferedReader inBuf = new BufferedReader(new InputStreamReader(con.getInputStream()));
	String inputLine;
	String vXMLStr = "";
	while ((inputLine = inBuf.readLine()) != null) 
	{
		System.out.println(inputLine);
		vXMLStr = vXMLStr + inputLine;
	}
	inBuf.close();
	System.out.println("Got Content");
	System.out.println(vXMLStr);
   
	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	InputSource isBuf = new InputSource();
	isBuf.setCharacterStream(new StringReader(vXMLStr));
	Document doc = dBuilder.parse(isBuf);
	doc.getDocumentElement().normalize();

	System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
	NodeList nList = doc.getElementsByTagName("RESPONSE");

	for (int tempN = 0; tempN < nList.getLength(); tempN++) 
	{
		Node nNode = nList.item(tempN);
		if (nNode.getNodeType() == Node.ELEMENT_NODE) 
		{
		  Element eElement = (Element) nNode;
		  System.out.println("URL : " + eElement.getElementsByTagName("url").item(0).getChildNodes().item(0).getNodeValue());
		  xmlURL = eElement.getElementsByTagName("url").item(0).getChildNodes().item(0).getNodeValue();
		  
		  NodeList aList = eElement.getElementsByTagName("param");
		  String vParamName;
		  for (int atrCnt = 0; atrCnt< aList.getLength();atrCnt++)
		  {
			  vParamName = aList.item(atrCnt).getAttributes().getNamedItem("name").getNodeValue();
			  System.out.println("<br>paramName : : " + vParamName);
			  
			  if (vParamName.equals("ttype") )
			  {
				  xmlttype = aList.item(atrCnt).getChildNodes().item(0).getNodeValue();
			  }
			  else if (vParamName.equals("tempTxnId") )
			  {
				  xmltempTxnId = aList.item(atrCnt).getChildNodes().item(0).getNodeValue();
			  }
			  else if (vParamName.equals("token") )
			  {
				  xmltoken = aList.item(atrCnt).getChildNodes().item(0).getNodeValue();
			  }
			  else if (vParamName.equals("txnStage") )
			  {
				  xmltxnStage = aList.item(atrCnt).getChildNodes().item(0).getNodeValue();
			  }
		  }
		  out.println("<br><b>URL               : </b>" + xmlURL);
		  out.println("<br><b>param : ttype     : </b>" + xmlttype);
		  out.println("<br><b>param : tempTxnId : </b>" + xmltempTxnId);
		  out.println("<br><b>param : token     : </b>" + xmltoken);
		  out.println("<br><b>param : txnStage  : </b>" + xmltxnStage);
		}
	}//for

	String Atom2Request = xmlURL+"?ttype="+xmlttype+"&tempTxnId="+xmltempTxnId+"&token="+xmltoken+"&txnStage="+xmltxnStage;
	Atom2Request = Atom2Request.replace(" ","%20");
	System.out.println("ATOM 2nd Request : " + Atom2Request);
	out.println("<br>ATOM 2nd Request : <br><b>" + Atom2Request+"</b>");
	response.sendRedirect(Atom2Request);
}
}
catch(Exception e)
{
	e.printStackTrace();
}
%> --%>
</body>
</html>