$(document).ready( function() {
	 
$("#countryid").change( function() {
//	alert("vani");
	var countryid=$(this).val();
	if(countryid!="0"){
	//	alert(countryid);
		getState(countryid,""); 
	}
});

$("#stateid").change( function() {
	var stateid=$(this).val();
	if(stateid!="0"){
		getCity(stateid,"");
	}
});

function getState(countryid,selectedId){
	$.ajax({	
		type: "GET",		
		url:"State.do?action=ajaxActiveList",
		data:{countryId:countryid},
		success: function(r){	
			var json= JSON.parse(r);
			console.log(json);			
			$("#stateid").empty();
			var firstOption= $("<option>",{text:"--Select State--",value:"-1"});
			var otherOption= $("<option>",{text:"Others",value:"0"});
			$("#stateid").append(firstOption);
			$.each(json,function(i,obj){
				var option= $("<option>",{text:obj.state, value:obj.stateId});
				$("#stateid").append(option);
			});
			$("#stateid").append(otherOption);
			if(selectedId!="" && selectedId!=null){
				$("#stateid").val(selectedId);
    		}
			
		} 
	});
}



function getCity(stateid,selectedId){
	$.ajax({	
		type: "GET",		
		url:"City.do?action=ajaxActiveList",
		data:{stateId:stateid},
		success: function(r){	
			var json= JSON.parse(r);
			console.log(json);			
			$("#cityid").empty();
			var firstOption= $("<option>",{text:"--Select City--",value:"-1"});
			var otherOption= $("<option>",{text:"Others",value:"0"});
			$("#cityid").append(firstOption);
			$.each(json,function(i,obj){
				var option= $("<option>",{text:obj.city, value:obj.cityId});
				$("#cityid").append(option);
			});
			$("#cityid").append(otherOption);
			if(selectedId!="" && selectedId!=null){
				$("#cityid").val(selectedId);
			}
			
		} 
	});
}



});