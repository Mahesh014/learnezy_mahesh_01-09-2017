 
function ratings(){			
		    var that = this;
  			var toolitup = $("#jRate").jRate({
  				rating: 1,
  				strokeColor: 'black',
  				precision: 1,
  				minSelected: 1,
  				onChange: function(rating) {  					
  					$("#rating").val(rating);
  					console.log("OnChange: Rating: "+rating);
  				},
  				onSet: function(rating) {
  					console.log("OnSet: Rating: "+rating);
  				}
  			});
  			
  			$('#btn-click').on('click', function() {
  				toolitup.setRating(0);				
  			});
			
		}
		

$(document).ready(function(){
	
	
	$("#tabTitle").html("COURSES");
		 $.ajax( {
			url : "Student.do?action=courseList",
			type : "GET",
			dataType : "json",
			success : function(res) {
				//$("#displayTab").html("");
				$('#courseTable').empty();
				var courslistTr="";
				$.each(res,function(i,obj){
					courslistTr=courslistTr+'<a href="Student.do?action=mynotes&amp;courseId='+obj.courseId+'"><span class="box1" >'+obj.courseName+'</span></a> ';	
					//alert(courslistTr);
				});
				courslistTr='<div style="float: left;width: 70%;" >'+courslistTr+'</div><div style="float: right;width: 30%;"><a href="AddNewCourse.do"><span class="box1" style="width: 175px;">Select New Course</span></a> </div>';
				$('#courseTable').append(courslistTr);
			}
	 	});

		$.ajax( {
			url : "Student.do?action=getCourseCompleteCount",
			type : "GET",
			dataType : "json",
			success : function(obj) {
					//alert(obj.courseCount);
					$('#courseCount').html(obj.courseCount);
			}
	 	});
		
		
		$.ajax( {
			url : "Student.do?action=getTestCompleteCount",
			type : "GET",
			dataType : "json",
			success : function(obj) {
					//alert(obj.testCount);
					$('#testCount').html(obj.testCount);
			}
	 	});
		
		$.ajax( {
			url : "Student.do?action=getAssignmentCount",
			type : "GET",
			dataType : "json",
			success : function(obj) {
					//alert(obj.assignmentCount);
					$('#assignmentCount').html(obj.assignmentCount);
			}
	 	});
		
		$.ajax( {
			url : "Student.do?action=getAchievementCompleteCount",
			type : "GET",
			dataType : "json",
			success : function(obj) {
					//alert(obj.assignmentCount);
					$('#achievementCount').html(obj.achievementCount);
			}
	 	});
		
		
		$.ajax( {
				url : "Student.do?action=getNotification",
				type : "GET",
				dataType : "json",
				success : function(res) {
					var  notificationMarque="";
					$.each(res,function(i,obj){
						notificationMarque=notificationMarque +'<p>'+obj.questionNo+'. '+obj.notification+'</p> <br />';
					});
					$('#notificationMarquee').append(notificationMarque);
				}
		});
		
		$("#mynotes").click(function(){
			$("#tabTitle").html("MYNOTES");
			$.ajax( {
				url : "Student.do?action=courseList",
				type : "GET",
				dataType : "json",
				success : function(res) {
					$('#courseTable').empty();
					var courslistTr="";
					$.each(res,function(i,obj){
						courslistTr=courslistTr+'<a href="Student.do?action=mynotes&amp;courseId='+obj.courseId+'" class="courselistses" id="cousid'+i+'"><input type="hidden" value='+obj.courseId+' id="mynotes'+i+'"><span class="box1">'+obj.courseName+'</span></a>';	
					});
					courslistTr='<div style="float: left;width: 70%;" >'+courslistTr+'</div><div style="float: right;width: 30%;">';
					$('#courseTable').append(courslistTr);
				}
		 	});
		});
		$("#courseMenu").click(function(){
			$("#tabTitle").html("COURSES");
			$.ajax( {
				url : "Student.do?action=courseList",
				type : "GET",
				dataType : "json",
				success : function(res) {
					$('#courseTable').empty();
					var courslistTr="";
					$.each(res,function(i,obj){
						courslistTr=courslistTr+'<a href="Student.do?action=courseDetials&amp;courseId='+obj.courseId+'"><span class="box1">'+obj.courseName+'</span></a>';	
					});
					courslistTr='<div style="float: left;width: 70%;" >'+courslistTr+'</div><div style="float: right;width: 30%;"><a href="AddNewCourse.do"><span class="box1" style="width: 175px;">Select New Course</span></a> </div>';
					$('#courseTable').append(courslistTr);
				}
		 	});
		});
		$(document).on('click', "#rtings", function(){
			$.ajax( {
				url : "Student.do?action=courseList",
				type : "GET",
				dataType : "json",
				success : function(res) {
					$('#courseTable').empty();
					var courslistTr="";
					$.each(res,function(i,obj){
						//alert(obj.courseName);
						courslistTr=courslistTr+'<option value='+obj.courseId+'>'+obj.courseName+'</option>';	
						//courslistTr=courslistTr+1;
					});
				//	alert(courslistTr);
					courslistTr='<select  id="courseIdRating" class="form-control">'+courslistTr+'</select>';
					var rating='<div id="jRate"><input type="hidden" id="rating" name="rating"></div><input type="submit" value="Submit" id="ratBtn" >';
				//	alert(rating);
					var divTr='<div class="courseRatingList">'+courslistTr+'</div><div class="RatingStarts">'+rating+'</div>';
					$('#courseTable').html(divTr);
					ratings();
				}
		 	});
				
		});
		
		
		$(document).on('click', "#rtingsnew", function(){
			$.ajax( {
				url : "Student.do?action=courseList",
				type : "GET",
				dataType : "json",
				success : function(res) {
					$('#courseTable').empty();
					var courslistTr="";
					$.each(res,function(i,obj){
						//alert(obj.courseName);
						courslistTr=courslistTr+'<option value='+obj.courseId+'>'+obj.courseName+'</option>';	
						//courslistTr=courslistTr+1;
					});
				//	alert(courslistTr);
					courslistTr='<select  id="courseIdRating" class="form-control">'+courslistTr+'</select>';
					var rating='<div id="jRate"><input type="hidden" id="rating" name="rating"></div><input type="submit" value="Submit" id="ratBtn" >';
				//	alert(rating);
					var divTr='<div class="courseRatingList">'+courslistTr+'</div><div class="RatingStarts">'+rating+'</div>';
					$('#courseTable').html(divTr);
					ratings();
				}
		 	});
				
		});
		
		
		
		$("#classSession").click(function(){
			$("#tabTitle").html("Student Session");
			$.ajax( {
				url : "StudentSessionClass.do?action=getStudentSessionList",
				type : "GET",
				dataType : "json",
				success : function(res) {
					$('#courseTable').empty();
					var sessionlistTr="";
					var count=0;
					if(res==""){
						sessionlistTr='<tr><td  colspan="5" align="center"> No Session Avalible</td></tr>'
						
					}else{
					$.each(res,function(i,obj){
						count=parseInt(count)+parseInt(1);
						var joinLink='<font color="blue">Not Started</font>';
						if(obj.sessionStatus=="Open" || obj.sessionStatus=="Join"){
							if(obj.appointmentType=="White Board"){
								joinLink='<a href="StudentSessionClass.do?action=accessSession&amp;appointmentScheduleId='+obj.appointmentScheduleId+'"><font color="green">Join</font></a>';	
							}
							if(obj.appointmentType=="Video"){
								//joinLink='<a href="'+obj.meetingURl+'"><font color="green">Join</font></a>';
								joinLink='<a href="StudentSessionClass.do?action=accessVideoSession&amp;appointmentScheduleId='+obj.appointmentScheduleId+'"><font color="green">Join</font></a>';
							}	
						}else
						if(obj.sessionStatus=="Close"){
						    joinLink='<font color="red">Closed</font>';	
						}
						
						sessionlistTr=sessionlistTr+'<tr><td>'+count+'</td><td>'+obj.appointmentDateTime+'</td>'+
														'<td>'+obj.courseName+'</td><td>'+obj.appointmentType+'</td>'+
														'<td>'+joinLink+'</td></tr>';                 	
					});
					}
					
					var tableHeader="<tr><th>SI No.</th><th>Appointment Data & Time</th><th>Course</th><th>Appointment Type</th><th>Status</th></tr>";
					var sessiontable='<table class="table dashTable" style="width:96%;">'+tableHeader+sessionlistTr+'</table>';
					//alert(sessiontable + res)
					$('#courseTable').html(sessiontable);
				}
		 	});
		});
		
		
		$("#getCertificateMenu").click(function(){
			$("#tabTitle").html("COURSES COMPLETED CERTIFICATE");
			$.ajax( {
				url : "Student.do?action=courseList",
				type : "GET",
				dataType : "json",
				success : function(res) {
					$('#courseTable').empty();
					var courslistTr="";
					$.each(res,function(i,obj){
						courslistTr=courslistTr+'<a href="GetCertificate.do?action=getCertificateDetials&amp;courseId='+obj.courseId+'"><span class="box1">'+obj.courseName+'</span></a>';
				  	});
					$('#courseTable').append(courslistTr);
				}
	 		});
		
		});
		
		
		$("#assignmentMenu").click(function(){
			$("#tabTitle").html("ASSINGNMENT");
			$.ajax( {
				url : "Student.do?action=courseList",
				type : "GET",
				dataType : "json",
				success : function(res) {
					$('#courseTable').empty();
					var courslistTr="";
					$.each(res,function(i,obj){
						courslistTr=courslistTr+ '<a href="javascript:void(0)" class="getAssignmentTopicId" id="'+obj.courseId+'" ><span class="box1">'+obj.courseName+'</span></a>';
				  	});
					$('#courseTable').append(courslistTr);
				}
	 		});
		});
		
		$("#knowledgeBankMenu").click(function(){
			$("#tabTitle").html("KNOWLEDGE BANK");
			$.ajax( {
				url : "Student.do?action=courseList",
				type : "GET",
				dataType : "json",
				success : function(res) {
					$('#courseTable').empty();
					var courslistTr="";
					$.each(res,function(i,obj){
					 	courslistTr=courslistTr+ '<a href="javascript:void(0)" class="getKBbyCourseID" id="'+obj.courseId+'" ><span class="box1">'+obj.courseName+'</span></a>';
					});
				 	$('#courseTable').append(courslistTr);
			 	}
		 	});
		});
		
		$("#resourcesMenu").click(function(){
			$("#tabTitle").html("RESOURCES");
			$.ajax( {
				url : "Student.do?action=courseList",
				type : "GET",
				dataType : "json",
				success : function(res) {
					$('#courseTable').empty();
					var courslistTr="";
					$.each(res,function(i,obj){
					 	courslistTr=courslistTr+ '<a href="javascript:void(0)" class="getResourcesbyCourseID" id="'+obj.courseId+'" ><span class="box1">'+obj.courseName+'</span></a>';
					 });
					$('#courseTable').append(courslistTr);
				}
		 	});
		});
		
		$("#linksMenu").click(function(){
			$("#tabTitle").html("LINKS");
			var courseId=this.id;
			 
			$.ajax( {
				url : "Student.do?action=getLinks",
				type : "GET",
				dataType : "json",
				data:{courseId:courseId},
				success : function(res) {
					$('#courseTable').empty();
					$.each(res,function(i,obj){
						courslistTr='<p style="color: rgb(229, 139, 48);"><a target="_blank" href="//'+obj.url+'" >'+obj.questionNo+'. '+obj.linkName+'</a></p>';
						$('#courseTable').append(courslistTr);
			 		});
				}
		 	});
		});
		
		$("#notificationMenu").click(function(){
			$("#tabTitle").html("NOTIFICATIONS");
			var courseId=this.id;
			 alert("vani"+courseId);
			$.ajax( {
				url : "Student.do?action=getNotification",
				type : "GET",
				dataType : "json",
				data:{courseId:courseId},
				success : function(res) {
					$('#courseTable').empty();
					$.each(res,function(i,obj){
					  courslistTr='<p style="color: rgb(229, 139, 48);">'+obj.questionNo+'. '+obj.notification+'</p> ';
					  $('#courseTable').append(courslistTr);
			 		});
				}
		 	});
		});
		$(document).on('click','.getAssignmentTopicId',function () {
			var courseId=this.id;
			$.ajax( {
				url : "Student.do?action=getAssignmentTopicId",
				type : "GET",
				dataType : "json",
				data:{courseId:courseId},
				success : function(res) {
					$('#courseTable').empty();
					var courslistTr="";
					$.each(res,function(i,obj){
					  courslistTr=courslistTr+ '<a href="javascript:void(0)" class="getAssignmentQuestions" id="'+obj.topicId+'" ><span class="box1">'+obj.topicName+'</span></a>';
					});
					$('#courseTable').append(courslistTr);
				}
		 	});
		});
		
		$(document).on('click','.getAssignmentQuestions',function () {
			var topicId=this.id;
			//alert(topicId);
			$.ajax( {
				url : "Student.do?action=getAssignmentQuestionbyTopicID",
				type : "GET",
				dataType : "json",
				data:{topicId : topicId},
				success : function(res) {
					$('#courseTable').empty();
					$.each(res,function(i,obj){
						var courslistTr= '<p style="color: rgb(229, 139, 48);">Q'+obj.questionNo+'. '   +obj.assignmentQuestion+' </p><p>Ans.'+obj.assignmentAnswer+'</a></p>';
						$('#courseTable').append(courslistTr);
			 		});
				}
		 	});
		});
		
		
		
	 
		$(document).on('click','.getKBbyCourseID',function () {
			 
			var courseId=this.id;
			$.ajax( {
				url : "Student.do?action=getKnowledgeBank",
				type : "GET",
				dataType : "json",
				data:{courseId:courseId},
				success : function(res) {
					$('#courseTable').empty();
					$.each(res,function(i,obj){
					 	var courslistTr='<p style="color: rgb(229, 139, 48);">Q'+obj.questionNo+'. '   +obj.kBQuestion+'</p><p>Ans. '+obj.kBAnswer+'</a></p> ';
					 	$('#courseTable').append(courslistTr);
			 		});
				}
		 	});
			
			
	
			
			
		});
		
		
		
		
	 
		$(document).on('click','.getResourcesbyCourseID',function () {
			var ctx = "${pageContext.request.contextPath}"; 
			var serverFilePath=$("#serverFilePath").val()+"/imageUpload/"; 
			//alert(serverFilePath);
			var courseId=this.id;
			//alert(courseId);
			$.ajax( {
				url : "Student.do?action=getResources",
				type : "GET",
				dataType : "json",
				data:{courseId:courseId},
				success : function(res) {
					$('#courseTable').empty();
					$.each(res,function(i,obj){
						var courslistTr= '<p style="color: rgb(229, 139, 48);" ><a href="'+serverFilePath+obj.fileName+'">'+obj.questionNo+'. '+obj.eBookName+'</a></p> ';
					    $('#courseTable').append(courslistTr);
			 		});
				}
		 	});
		});
		
		
		$("#settings").click(function(){
			$("#tabTitle").html("SETTINGS");
			$('#courseTable').empty();
			var buttons='<a href="ChangePassword.do" ><span class="box1" style="width: 170px;">Change Password</span></a>';
			$('#courseTable').append(buttons);
		});
		
		$(document).on('click', "#ratBtn", function(){
			var rating=$("#rating").val();
			 var courseIdRating=$("#courseIdRating").val();
			 alert("Rated Successfully");
			$.ajax( {
				url : "Student.do?action=addRating",
				type : "GET",
				dataType : "json",
				data: {rating:rating,courseIdRating : courseIdRating },
				success : function(res) {
					 alert("hi"+res);
				}
		 	});
				
		});
			
		
		$("#audio").click(function(){
			$("#tabTitle").html("Audio Files");
			var serverFilePath=$("#serverFilePath").val()+"/audioFiles/"; 
			//alert(serverFilePath);
			$.ajax( {
				url : "Student.do?action=getAudioFiles",
				type : "GET",
				dataType : "json",
				success : function(res) {
					$('#courseTable').empty();
					var audioListTr="";
					var count=0;
					if(res==""){
						audioListTr='<tr><td  colspan="5" align="center"> No Filese Avalible</td></tr>'
						
					}else{
					$.each(res,function(i,obj){
						count=parseInt(count)+parseInt(1);
						audioListTr=audioListTr+'<tr><td>'+count+'</td><td>'+obj.courseName+'</td>'+
														'<td><audio src="'+serverFilePath+'/'+obj.fileName+'" controls preload></audio><a href="'+serverFilePath+obj.fileName+'" > <span class="glyphicon glyphicon-download"></span> </a></td></tr>';                 	
					});
					}
					var tableHeader="<tr><th>SI No.</th><th>Course</th><th>Audio File</th></tr>";
					var audioTable='<table class="table table-striped dashTable" style="width:96%;">'+tableHeader+audioListTr+'</table>';
					//alert(audioTable + res)
					$('#courseTable').html(audioTable);
				}
		 	});
		});
		
		
});