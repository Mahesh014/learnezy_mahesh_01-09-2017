$(document).ready(function() {
    var courseId = $("#courseId").val();
    var serverPath = "/E-Learning";
    $.ajax({
        url: "Student.do?action=lastView",
        type: "GET",
        dataType: "json",
        data: {
            courseId: courseId
        },
        success: function(obj) {
            $("#contantDiv").html();
            $("#contantDiv").html(obj.subTopicText);
            //var serverPath = "${pageContext.request.contextPath}";
            if (obj.subTopicImages != "") {
                $("#imageUpload").attr("src", serverPath + "/imageUpload/" + obj.subTopicImages);
                $("#imageUploadDiv").show();
            } else {
                $("#imageUpload").attr("src", "");
                $("#imageUploadDiv").hide();
            }

            if (obj.subTopicVideos != "") {

                var videoSrc = '<video width="280" height="280" style="border:2px solid black;"  controls><source src="' + serverPath + '/videoUpload/' + obj.subTopicVideos + '" type="video/mp4"></video>';
                //alert(videoSrc);
                $("#videoUploadDiv").html(videoSrc);
                $("#videoUploadDiv").show();
            } else {
                $("#videoUploadDiv").html("");
                $("#videoUploadDiv").hide();
            }

            if (obj.youTubeLink != "") {
                $("#youTubeLink").attr("src", "http://www.youtube.com/embed/" + obj.youTubeLink);
                // alert(obj.youTubeLink);
                $("#youTubeLinkDiv").show();
            } else {
                $("#youTubeLink").attr("src", "");
                $("#youTubeLinkDiv").hide();
            }
            
            var sampleProgram=obj.jAray;
            if (sampleProgram!="") {
			   	var option="";
			   	$.each(sampleProgram, function(i, objj) {
                  		 option = option + '<p><span class="questionClass">Sample Program ' + (i + 1) + '.</span><span class="answerClass">' + objj.name + '</span></p><br /><div id="sampleid'+i+'"><p >'+objj.sampleProgramContent+'</p></div><br /><a id="compilerid'+i+'" href="#" class="compileandrunclass">ComplieandRun.</a>';
        		});
			 	$("#sampleProgDiv").html(option);
			 	
        	}else{
        		$("#sampleProgDiv").html("");
        	}
        }
    });
    $('.subTopic').click(function() {
        var subTopicId = this.id;
        var courseId = $("#courseId").val();
        // alert(subTopicId);
        $.ajax({
            url: "Student.do?action=contantList",
            type: "GET",
            dataType: "json",
            data: {
                subTopicId: subTopicId,
                courseId: courseId
            },
            success: function(obj) {
            	$("#contantDiv").html();
                var contentText = obj.subTopicText;
                // alert(contentText);
                if (contentText == "") {
                    contentText = "Content will be added later";
                }
                $("#contantDiv").html(contentText);
                //var serverPath = "${pageContext.request.contextPath}";
                if (obj.subTopicImages != "") {
                    $("#imageUpload").attr("src", serverPath + "/imageUpload/" + obj.subTopicImages);
                    $("#imageUploadDiv").show();
                } else {
                    $("#imageUpload").attr("src", "");
                    $("#imageUploadDiv").hide();
                }

                if (obj.subTopicVideos != "") {

                    var videoSrc = '<video width="280" height="280" style="border:2px solid black;" controls><source src="' + serverPath + '/videoUpload/' + obj.subTopicVideos + '" type="video/mp4"></video>';
                    // alert(videoSrc);
                    $("#videoUploadDiv").html(videoSrc);
                    $("#videoUploadDiv").show();
                } else {
                    $("#videoUploadDiv").html("");
                    $("#videoUploadDiv").hide();
                }

                if (obj.youTubeLink != "") {
                    $("#youTubeLink").attr("src", "http://www.youtube.com/embed/" + obj.youTubeLink);
                    // alert(obj.youTubeLink);
                    $("#youTubeLinkDiv").show();
                } else {
                    $("#youTubeLink").attr("src", "");
                    $("#youTubeLinkDiv").hide();
                }
                var sampleProgram=obj.jAray;
                if (sampleProgram!="") {
				   	var option="";
				   $.each(sampleProgram, function(i, objj) {
                      		 option = option + '<p><span class="questionClass">Sample Program ' + (i + 1) + '.</span><span class="answerClass">' + objj.name + '</span></p><br /><p>'+objj.sampleProgramContent+'</p>';
            		});
				   	$("#sampleProgDiv").html(option);
            	}else{
            		$("#sampleProgDiv").html("");
            	}
            }
        });
    });

    $('.assingment').click(function() {
                var topicId = this.id;
                var courseId = $("#courseId").val();
                $("#imageUploadDiv").hide();
                $("#videoUploadDiv").hide();
                $("#youTubeLinkDiv").hide();
                $("#sampleProgDiv").html("");
                // alert(topicId);
                $.ajax({
                        url: "Student.do?action=getAssignmentQuestionbyTopicID",
                        type: "GET",
                        data: {
                            topicId: topicId,
                            courseId: courseId
                        },
                        success: function(r) {
                            var json = JSON.parse(r);
                            i = 0;
                            var option = "";
                            $.each(json, function(i, obj) {
                                option = option + '<p><span class="questionClass">Q' + (i + 1) + '.</span><span class="answerClass">' + obj.assignmentQuestion + '</span></p><br />';
                                // alert(option);
                            });
                            if (option != "") {
                                $("#contantDiv").html(option);
                            } else {
                                $("#contantDiv").html(" NO ASSINGMENTS AVALIBALE");
                            }

                        }
                    });
            });
    
    
    $('.assingSubTopic').click(function() {
        var subTopicId = this.id;
        var courseId = $("#courseId").val();
        $("#imageUploadDiv").hide();
        $("#videoUploadDiv").hide();
        $("#youTubeLinkDiv").hide();
        $("#sampleProgDiv").html("");
        // alert(topicId);
        $.ajax({
                url: "Student.do?action=getAssignmentQuestionbySubTopicID",
                type: "GET",
                data: {
                	subTopicId: subTopicId,
                    courseId: courseId
                },
                success: function(r) {
                    var json = JSON.parse(r);
                    i = 0;
                    var option = "";
                    $.each(json, function(i, obj) {
                        option = option + '<p><span class="questionClass">Q' + (i + 1) + '.</span><span class="answerClass">' + obj.assignmentQuestion + '</span></p><br />';
                        // alert(option);
                    });
                    if (option != "") {
                        $("#contantDiv").html(option);
                    } else {
                        $("#contantDiv").html(" NO ASSINGMENTS AVALIBALE");
                    }

                }
            });
    });


    $(".panel2").each(function() {
        $(this).slideUp();
    });
    $(".flip").click(function() {
        var panel = "panel" + this.id;
        $(".panel2").each(function() {
            if (panel != this.id) {
                $(this).slideUp();
            } else {
                $("#" + panel).slideDown();
            }
        });
        // $(panel).slideToggle();
    });
    
    
    $("#javaComplier").click(function(){
    	window.open("JavaComplier.do");
	});
});

function filesdisp(path) {
    // alert("hgh");
    chatterinfo = window.open(path, "chatterwin", "scrollbars=no,resizable=yes, width=800, height=500, location=no, toolbar=no, status=no");
    chatterinfo.focus();
}