$(document).ready(function(){
	$("#fromdate").calendar();
	billCycleList();
	$("#fromdate_alert").click(function(){
		alert(popUpCal.parseDate($('#calendar1').val()));
	});
		var billcycleNo=$("#billcycleName").val();
		var pincode=$("#searchBox").val();
		var date=$("#fromdate").val();

	setAutoCompleteForPincode();
	$("#callAjaxAllocatedList").click(function(){
		document.getElementById('status').innerHTML = "";
		var billcycleNo=$("#billcycleName").val();
		var pincode=$("#searchBox").val();
		var date=$("#fromdate").val();
		
		$("#listForAllocationToDeliveryBoy").validate({
 		  	  rules: {
 				pinCodeNumber: {	required: true,digits:true},
 				searchdate: {	required: true},
 				billcycle:{required:true},
 			 agree: "required"
 		  	  },
 		  	  messages: {
 		  		pinCodeNumber: {  required: "Pincode required!"  },
 		  	searchdate: {  required: "Date required!"  },
 		billcycle: {  required: "BillCycle required!"  }
 			      }
 			 });
           $("#listForAllocationToDeliveryBoy").submit(function(e){
               e.preventDefault();
               if(billcycleNo!="" && pincode!="" && date!="")
               {
		loadingDropdownForDeliveryBoy();
		getListForAllocationToDeliveryBoy(billcycleNo,pincode,date);
		refreshGridData();	
               }
	});	
	});
	$("#allocateID").click(function(){
		allocateToDeiveryBoy();
	});
});

function getListForAllocationToDeliveryBoy(billcycleNo,pincode,date){
	
	$.ajax({
		url:"allocatetodeliveryboy.do?action=listForAllocationToDeliveryBoy&searchdate="+date+"&pinCodeNumber="+pincode+"&billcycle="+billcycleNo,
		success:function(r){
		if(r!=null || r!=""){
		var lst= JSON.parse(r);
			loadingGrid(lst);
	    }
	}
	});
}
function loadingGrid(lst)
{
	$("#tbl").jqGrid(
			{
				data:lst, 
			    height: 'auto',
			    autowidth: true,
			    shrinkToFit: true,
			    datatype: 'local',
			    altRows: true,
			    colNames:
			    [
			        'POD NUMBER',
			        'NAME',
			        'ACCOUNT NO',
			        'INVOICE NO',
			        ' ALLOCATE ',
			    ],
			    colModel:
			    [
			        { name: 'PODNO', index: 'PODNO',width:'115'},
			        { name: 'FIRSTNAME', index: 'FIRSTNAME' },
			        { name: 'ACCOUNTNO', index: 'ACCOUNTNO' },
                    { name: 'INVOICENO', index: 'INVOICENO' } ,
			        { name: 'COURIERTRANSID', index: 'COURIERTRANSID', width:'115', 
                    	formatter:function(cellVal,options,rowObj){
                    	  var cbx= $("<input>",{type:"checkbox",value:cellVal, class:"checkedValues"});
                    	  var div=$("<div>",{html:cbx});
                    	  return div.html();
                        }
			        },
			    ], 
			    rowNum: 10,
			    rowList: [10, 20, 30,40,50], // Page Selection Options
			    viewrecords: true,
			    gridview: true,
			    sortname: 'PODNO',
			    sortorder: "ASC",
			    caption: "DISPATCH TO DELIVERY BOY",
			    pager: '#divPager',
			    pgbuttons: true,
			    pginput: true,
			    rownumbers: true,
			    loadComplete: function () {
			        $('#tbl tr').removeClass("ui-priority-secondary");//To avoid Alternative Row column text color transparent

			        $("td", ".jqgrow").height(28).css({ "color": "#000000", "font-family": "Segoe UI", "font-size": "14px" });
			        $("tr.jqgrow:odd").css({ "background": "#FFFFFF" });
			        $("tr.jqgrow:even").css({ "background": '#F9F9F9' });
			        $(".ui-jqgrid-sortable").each(function () {
			            $(this).css({
			                "cursor": "pointer",
			                "color": "#386AA7",
			                "font-family": "Oswald",
			                "font-size": "15px",
			                "height": "28px",
			                "text-align": "left",
			                "padding-top": "8px"
			            });
			        });
			        
			        $(".ui-jqgrid-sortable").parent().css("background", "#EEEEEE");
			        $("#allocateTable").show();
			    }
			});
}
function billCycleList()
{
	$.ajax({
		url:"allocatetodeliveryboy.do?action=listForBillCycle",
		success:function(r){
		 var json= JSON.parse(r);
	     $("#billcycleName").empty();
	     var firstoption=$("<option>",{text:"---SELECT---",value:""});
	      $("#billcycleName").append(firstoption); 		 
	 		$.each(json,function(i,obj){
	 			var option= $("<option>",{text:obj.BILLCYCLE, value:obj.BILLCYCLE});
	 			$("#billcycleName").append(option);
	 		});
	}
	});	
}
function loadingDropdownForDeliveryBoy()
{
	$.ajax({
		url:"allocatetodeliveryboy.do?action=deliveryBoyList",
		success:function(r){
		 var json= JSON.parse(r);
	     $("#DELIVERYBOYID").empty();
	     var firstoption=$("<option>",{text:"----------SELECT----------",value:"-1"});
	      $("#DELIVERYBOYID").append(firstoption); 		 
	 		$.each(json,function(i,obj){
	 			var option= $("<option>",{text:obj.FIRSTNAME, value:obj.USERID});
	 			$("#DELIVERYBOYID").append(option);
	 		});
	}
	});	
}
function setAutoCompleteForPincode()
{

	$("#searchBox").autocomplete({
	    source: function (request, response) {
	        var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
	        $.ajax({
	            url: "allocatetodeliveryboy.do?action=listForPincode",
	            dataType: "json",
	            success: function (data) {
	                response($.map(data, function(v,i){
	                    var text = v.pincodenumber;
	                    if ( text && ( !request.term || matcher.test(text) ) ) {
	                        return {
	                                label: v.pincodenumber,
	                                value: v.pincodenumber
	                               
	                               };
	                    }
	                }));
	            }
	        });
	    }
	});
}

function allocateToDeiveryBoy()
{
	 var chkArray=[];
	var deliveryboyid=$("#DELIVERYBOYID").val();
	
	if ($('.checkedValues:checked').length == 0) {
	    alert("Please check atleast one CheckBox ");
	    return false;
	}
	else if(deliveryboyid=="-1")
	{
		alert("Please select Delivery Boy");
		return false;
	}
	else{
		$(".checkedValues:checked").each(function() {
			chkArray.push($(this).val());
		});
		
		var selected = chkArray.join(',');
		$.ajax({
			url:"allocatetodeliveryboy.do?action=updatedeliveryBoyList&COURIERTRANSID="+selected+"&deliveryboyid="+deliveryboyid,
			success: function (data) {	
				document.getElementById('status').innerHTML = "Successfully Couriers Allocated";
			}
	}).done(function(r){
		refreshGridData();
	});
		
	}
	
}
function refreshGridData()
{
	var billcycleNo=$("#billcycleName").val();
	var pincode=$("#searchBox").val();
	var date=$("#fromdate").val();
	$.ajax({
		url:"allocatetodeliveryboy.do?action=listForAllocationToDeliveryBoy&searchdate="+date+"&pinCodeNumber="+pincode+"&billcycle="+billcycleNo,
		success:function(r){
		if(r!=null || r!=""){
			var lst= JSON.parse(r);
			$("#tbl").jqGrid('clearGridData').jqGrid('setGridParam', { data: lst })
			.trigger('reloadGrid', [{ page: 1}]);
	    }
	}
	});

}
