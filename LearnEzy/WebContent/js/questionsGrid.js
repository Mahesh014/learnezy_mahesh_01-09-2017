$(document).ready(function(){
	var lst;
	
	$.ajax({
		url:"Testquestion.do?action=userNames",
		success:function(r){
		 var json= JSON.parse(r);
	     $("#UserName").empty();
	     var firstoption=$("<option>",{text:"---SELECT---",value:""});
	      $("#UserName").append(firstoption); 		 
	 		$.each(json,function(i,obj){
	 			var option= $("<option>",{text:obj.Name, value:obj.UserId});
	 			$("#UserName").append(option);
	 		});
	}
	});		
	

	 
$.ajax({
	url:"Testquestion.do?action=questionsGrid",
	success:function(r){
	if(r!=null || r!=""){
		
	 lst= JSON.parse(r);
		loadingGrid(lst);
    }
}
});

$("body").on("click",".checkedValues",function(){
	$("#divHide").show();
	var selected;

	$('.checkedValues:checked').each(function() {
		
		 var chkArray=[];
				$(".checkedValues:checked").each(function() {
		
					chkArray.push($(this).val());
				});
				selected = chkArray.join(',');

		});
	
	
	//alert(selected);
	var filteredlst;
	
	   $.ajax({

	    	url:"Testquestion.do?action=Filteredquestions&selected="+selected,
	    	success:function(r){
	    	if(r!=null || r!=""){
	    
	    		 filteredlst	= JSON.parse(r);    	
	    	}
	    	
		}
	    }).done(function() {
	   	 loadingGridnew(filteredlst);  
	    	$("#tbl1").jqGrid('clearGridData').jqGrid('setGridParam', { data: filteredlst })
			.trigger('reloadGrid', [{ page: 1}]);
	    	
	    	
	    });
	
	
});


function loadingGrid(lst)
{
	$("#tbl").jqGrid(
			{
				data:lst, 
			    height: 'auto',
			    autowidth: true,
			    shrinkToFit: true,
			    datatype: 'local',
			    altRows: true,
			    colNames:
			    [
			     'COURSE',
			     'TOPIC NAME',
			        'TEST QUESTIONS',
			        'ALLOCATE',
			    ],
			    colModel:
			    [
			        { name: 'CourseName', index: 'TopicName',width:'15' },
			        { name: 'TopicName', index: 'TopicName',width:'15' },
			        { name: 'Question', index: 'Question',width:'60' },			    
			        { name: 'QuestionId', index: 'QuestionId', width:'10', 
                    	formatter:function(cellVal,options,rowObj){
                    	  var cbx= $("<input>",{type:"checkbox",value:cellVal, class:"checkedValues"});
                    	  var div=$("<div>",{html:cbx});
                    	  return div.html();
                        }
			        },
			    ], 
			    rowNum: 10,
			    rowList: [10,20, 40, 60,80,100,120,140,160,180,200], // Page Selection Options
			    viewrecords: true,
			    gridview: true,
			    sortname: 'QuestionId',
			    sortorder: "ASC",
			    caption: "Question Bank",
			    pager: '#divPager',
			    pgbuttons: true,
			    pginput: true,
			    rownumbers: true,
			    loadComplete: function () {
				
			        $('#tbl tr').removeClass("ui-priority-secondary");//To avoid Alternative Row column text color transparent\
			        $("td", ".jqgrow").height(28).css({ "color": "#000000", "font-family": "Segoe UI", "font-size": "14px" });
			        $("tr.jqgrow:odd").css({ "background": "#FFFFFF" });
			        $("tr.jqgrow:even").css({ "background": '#F9F9F9' });
			        var DataGrid = $('#tbl');
			   	 DataGrid.jqGrid('setGridWidth', '900');
			        $(".ui-jqgrid-sortable").each(function () {
			            $(this).css({
			           
			                "cursor": "pointer",
			                "color": "#386AA7",
			                "font-family": "Oswald",
			                "font-size": "15px",
			                "height": "28px",
			                "text-align": "center",
			                "padding-top": "8px"
			            });
			        });
			        
			        $(".ui-jqgrid-sortable").parent().css("background", "#EEEEEE");
			      
			    }
			});
	
}




function loadingGridnew(filteredlst)
{
	
	$("#tbl1").jqGrid(
			{
				data:filteredlst, 
			    height: 'auto',
			    autowidth: true,
			    shrinkToFit: true,
			    datatype: 'local',
			    altRows: true, 
			    colNames:
			    [
	
			        'QUESTIONS',
			    ],
			    colModel:
			    [

			        { name: 'Question', index: 'Question',width:'85'},			    
			       
			    ], 
			    rowNum: 10,
			    rowList: [10,20, 30, 40,50,60,70,80,90,100,120], // Page Selection Options
			    viewrecords: true,
			    gridview: true,
			    sortname: 'QuestionId',
			    sortorder: "ASC",
			    caption: "Selected Questions",
			    pager: '#divPager1',
			    pgbuttons: true,
			    pginput: true,
			    rownumbers: true,
			    loadComplete: function () {
				
			        $('#tbl1 tr').removeClass("ui-priority-secondary");//To avoid Alternative Row column text color transparent\
			        $("td", ".jqgrow").height(28).css({ "color": "#000000", "font-family": "Segoe UI", "font-size": "14px" });
			        $("tr.jqgrow:odd").css({ "background": "#FFFFFF" });
			        $("tr.jqgrow:even").css({ "background": '#F9F9F9' });
			        var DataGrid = $('#tbl1');
				   	 DataGrid.jqGrid('setGridWidth', '900');
			        $(".ui-jqgrid-sortable").each(function () {
			            $(this).css({
			                "cursor": "pointer",
			                "color": "#386AA7",
			                "font-family": "Oswald",
			                "font-size": "15px",
			                "height": "28px",
			                "text-align": "center",
			                "padding-top": "8px"
			            });
			        });
			        
			        $(".ui-jqgrid-sortable").parent().css("background", "#EEEEEE");
			    }
			});
}


$("#allocateID").click(function(){

	var selected1;

	$('.checkedValues:checked').each(function() {

		 var chkArray1=[];
				$(".checkedValues:checked").each(function() {
		
					chkArray1.push($(this).val());
				});
				selected1 = chkArray1.join(',');


		});

	var userID=$("#UserName option:selected").val();
	var userName=$("#UserName option:selected").text();
	
	$.ajax({
		
		url:"Testquestion.do?action=UpdateQuestions&userID="+userID+"&QuestionsID="+selected1,
		success:function(r){
	 	$("#tbl").jqGrid('clearGridData').jqGrid('setGridParam', { data: lst })
		.trigger('reloadGrid', [{ page: 1}]);
		 $("#tbl1").empty();
		 $("#UserName").val("");
		 $("#status").append("Questions allocated to "+userName+" Successfully");
	}
	});
});


});