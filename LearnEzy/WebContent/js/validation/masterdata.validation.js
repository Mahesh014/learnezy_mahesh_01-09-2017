function preventBack() {
	window.history.forward();
}
setTimeout("preventBack()", 0);
window.onunload = function() {
	null;
};

$(document).ready(function() {
	// alert("Hi MasterData Valiidation");
		$("#topForm").validate( {
			rules : {
				top : {
					required : true,
					lettersonly : true,
					maxlength : 50
				},
				agree : "required"
			},
			messages : {
				top : {
					required : "Type of Place required!",
					lettersonly : "Enter only characters"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});

		$("#roleForm").validate( {
			rules : {
				role : {
					required : true,
					lettersonly : true,
					maxlength : 100
				},
				agree : "required"
			},
			messages : {
				role : {
					required : "Role Name required!",
					lettersonly : "Enter only characters"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});

		$("#statusForm").validate( {
			rules : {
				status : {
					required : true,
					lettersonly : true,
					maxlength : 50
				},
				agree : "required"
			},
			messages : {
				status : {
					required : "Status Name required!",
					lettersonly : "Enter only characters"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});

		$("#agentForm").validate( {
			rules : {
				agentName : {
					required : true,
					lettersonly : true,
					maxlength : 50
				},
				agree : "required"
			},
			messages : {
				agentName : {
					required : "Agent Name required!",
					lettersonly : "Enter only characters"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});
		$("#billcycleForm").validate( {
			rules : {
				billcycle : {
					required : true,
					lettersNumbersonly : true
				},
				fromdate : {
					required : true
				},
				todate : {
					required : true
				},
				agree : "required"
			},
			messages : {
				billcycle : {
					required : "Bill Cycle required!",
					lettersNumbersonly : "Enter only characters and Numbers"
				},
				fromdate : {
					required : "From date required!"
				},
				todate : {
					required : "To date required!"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});

		$("#userForm").validate( {
			rules : {
				firstname : {
					required : true,
					lettersonly : true,
					maxlength : 25
				},
				roleid : {
					required : true,
					min : 1
				},
				username : {
					required : true,
					minlength : 3,
					maxlength : 20
				},
				password : {
					required : true,
					minlength : 3,
					maxlength : 20
				},
				agree : "required"
			},
			messages : {
				firstname : {
					required : "First Name required!",
					lettersonly : "Enter only characters"
				},
				roleid : {
					required : "Select Role!",
					min : "Select Role!"
				},
				username : {
					required : "UserName required!",
					minlength : "3 Letter Minimum",
					maxlength : "20 Letter Maximum"
				},
				password : {
					required : "Password required!",
					minlength : "3 Letter Minimum",
					maxlength : "20 Letter Maximum"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});

		$("#employeeForm").validate( {
			rules : {
				firstname : {
					required : true,
					lettersonly : true,
					maxlength : 25
				},
				lastname : {
					required : true,
					lettersonly : true,
					maxlength : 25
				},
				emailid : {
					required : true,
					email : true
				},
				dob : {
					required : true
				},
				mobileno : {
					required : true,
					digits : true,
					minlength : 10,
					maxlength : 10
				},
				address : {
					required : true,
					maxlength : 100
				},
				agree : "required"
			},
			messages : {
				firstname : {
					required : "First Name required!",
					lettersonly : "Enter only characters"
				},
				lastname : {
					required : "Last name required!",
					lettersonly : "Enter only characters"
				},
				emailid : {
					required : "Email-ID required!",
					email : "Email-ID is not valid"
				},
				dob : {
					required : "DOB required!"
				},
				mobileno : {
					required : "Mobile number required!",
					digits : "Mobile is not valid",
					minlength : "Enter 10 digit Mobile number",
					maxlength : "Enter 10 digit Mobile number"
				},
				address : {
					required : "Address required!"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});

		$("#vendorForm").validate( {
			rules : {
				vendorname : {
					required : true,
					lettersNumbersonly : true,
					maxlength : 250
				},
				contactno : {
					required : true,
					digits : true,
					minlength : 10,
					maxlength : 12
				},
				emailid : {
					required : true,
					email : true
				},
				address : {
					required : true,
					maxlength : 100
				},
				agree : "required"
			},
			messages : {
				vendorname : {
					required : "Vendor Name required!",
					lettersNumbersonly : "Enter only characters and Numbers"
				},
				contactno : {
					required : "Contact number required!",
					digits : "Contact number Accept only numbers",
					minlength : "Enter 10 digit Contact number",
					maxlength : "Enter 10 digit Contact number"
				},
				emailid : {
					required : "Email-ID required!",
					email : "Email-ID is not valid"
				},
				address : {
					required : "Address required!"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});

		$("#countryForm").validate( {
			onkeyup : true,
			rules : {
				countryname : {
					required : true,
					lettersonly : true,
					maxlength : 50
				},
				agree : "required"
			},
			messages : {
				countryname : {
					required : "Country Name required!",
					lettersonly : "Enter only characters"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});

		$("#stateForm").validate( {
			rules : {
				countryid : {
					required : true,
					min : 1
					
				},
				statename : {
					required : true,
					maxlength : 50
				},
				agree : "required"
			},

			messages : {
				statename : {
					required : "State Name required!",
					lettersonly : "Enter only characters"
				},
				countryid : {
					required : "Select Country!",
					min : "Select Country!"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});

		$("#cityForm").validate( {
			rules : {
				countryid : {
					required : true,
					min : 1
				},
				stateid : {
					required : true,
					min : 1
				},
				cityname : {
					required : true,
					lettersonly : true
				},
				agree : "required"
			},
			messages : {
				countryid : {
					required : "Select Country!",
					min : "Select Country!"
				},
				stateid : {
					required : "Select State!",
					min : "Select State!"
				},
				cityname : {
					required : "City Name required!",
					lettersonly : "Enter only characters"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});

		$("#circleForm").validate( {
			rules : {
				countryid : {
					required : true,
					min : 1
				},
				stateid : {
					required : true,
					min : 1
				},
				cityid : {
					required : true,
					min : 1
				},
				circlename : {
					required : true,
					lettersonly : true,
					maxlength:50
				},
				agree : "required"
			},
			messages : {
				countryid : {
					required : "Select Country!",
					min : "Select Country!"
				},
				stateid : {
					required : "Select State!",
					min : "Select State!"
				},
				cityid : {
					required : "Select City!",
					min : "Select City!"
				},
				circlename : {
					required : "Branch Name required!",
					lettersonly : "only Letter!"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});

		$("#relattionshipForm").validate( {
			rules : {
				relationshipname : {
					required : true,
					lettersonly : true,
					maxlength:50
				},
				agree : "required"
			},
			messages : {
				relationshipname : {
					required : "Relationship Name required!",
					lettersonly : "Enter only characters"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});

		$("#courierMasterForm").validate( {
			rules : {
				billcycle : {
					required : true
				},
				vendorid : {
					required : true,
					min : 1
				},
				
				accountnumber : {
					required : true
					//digits : true
				},
				podnumber : {
					required : true
				},
				agree : "required"
			},
			messages : {
				billcycle : {
					required : "Select Bill Cycle!"
				},
				vendorid : {
					required : "Select vendor!",
					min : "Select vendor!"
				},
				statusid : {
					required : "Select status!",
					min : "Select status!"
				},
				accountnumber : {
					required : "Account number required!"
				},
				invoicenumber : {
					required : "Invoice number required!"
				},
				podnumber : {
					required : "POD number required!"
				},
				address : {
					required : "Address required!"
				},
				changeofaddress : {
					required : "Change of address required!"
				},
				mobileno : {
					required : "Mobile number required!",
					digits : "Mobile is not valid",
					minlength : "Enter 10 digit Mobile number",
					maxlength : "Enter 10 digit Mobile number"
				},
				billdate :{ required : "Bill Date required!"
				}

			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});

		$("#agencyForm").validate( {
			rules : {
				agencyname : {
					required : true,
					maxlength:50
				},
				address : {
					required : true,
					maxlength:250
				},
				zone : {
					required : true,
					maxlength: 100
				}
			},
			messages : {
				agencyname : {
					required : "Agency Name required!"
				},
				address : {
					required : "Address required!"
				},
				zone : {
					required : "Zone required!"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});

		$("#pincodeForm").validate( {
			rules : {
				pincode : {
					required : true,
					digits : true
				},
				stateid : {
					required : true,
					min : 1
				}
			},
			messages : {
				pincode : {
					required : "Pincode required!",
					digits : "Enter only numbers!"
				},
				stateid : {
					required : "Select city!",
					min : "Select city!"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});

		$("#areaForm").validate( {
			rules : {
				cityid : {
					required : true,
					min : 1
				},
				areaname : {
					required : true,
					lettersonly : true,
					maxlength : 50
				},
				agree : "required"
			},
			messages : {
				cityid : {
					required : "City Name required!",
					min : "Select City Name!"
				},
				areaname : {
					required : "Area Name required!",
					lettersonly : "Enter only characters"

				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});

		$("#taxsettingform").validate( {
			rules : {
				taxName : {
					required : true
				},
				taxValue : {
					required : true
				},
				agree : "required"
			},
			messages : {
				taxName : {
					required : "Tax Type required!"
				},
				taxValue : {
					required : "Tax Value required!"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});

		$("#functionalityForm").validate( {
			rules : {
				functionality  : {
					required : true
				},
				fileName  : {
					required : true
				},
				parentID  : {
					required : true
				},
				screenCode  : {
					required : true
				},
				agree : "required"
			},
			messages : {
				functionality : {
					required : "Functionality required!"
				},
				fileName : {
					required : "File Name required!"
				},
				parentID : {
					required : "Parent ID required!"
				},
				screenCode : {
					required : "Screen Code required!"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});
	});