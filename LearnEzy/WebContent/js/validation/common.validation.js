 
function preventBack(){window.history.forward();}
    setTimeout("preventBack()", 0);
    window.onunload=function(){null;};

$(document).ready(function () {
	//alert("Hi Common Valiidation");
		$("#changePasswordForm").validate({
	    	  rules: {
				currpass: {	required: true},
				newpass: {	required: true},
				confpass: {	required: true},
	    	    agree: "required"
	    	  },
	    	  messages: {
	    		  	currpass: {  required: "Current Password required!"  },
	    	  		newpass: {  required: "New Password required!"  },
	    	  		confpass: {  required: "Confirm Password required!"  }
		      }
		 });
 
		$("#courierExcelUploadForm").validate({
	    	  rules: {
	    	       	fileName: {	required: true,	extension: "xls|xlsx" },
	    	       	vendorid: {	required: true,	min:1 },
	    	       	agree: "required"
	    	  },
	    	  messages: {
	    		  fileName: {
		                required: "Select excel file!",
		                extension: "Select xls or xlsx type files."
		          },
		          vendorid: {
	                required: "Select vendor",
	                min: "Select vendor"
	             }
		      },
	    	  errorPlacement: function (error, element) {
		            var err= $("<br/>").add(error);   
		            $(element).after(err);
	    	  }
	   });
		
	   $("#lpt-form").validate({
				rules:{
				    awbNo:{required:true, maxlength:50},
				    bookingDate:{required: true, maxlength:50},
				    noOfBoxes:{ required: true,digits:true, maxlength:50},
				    noOfPackets:{required:true,digits: true, maxlength:50},
				    noOfBills:{required:true,digits: true},
				    receivedfrom:{required:true, maxlength:50},
				    billsReceivedDate:{required:true, maxlength:50}
				},
				messages: {
					awbNo: {
						required:  "Challan No. Required!"
					},
		           
					bookingDate: {
		            	required: "Booking Date Required!"
		            },
		            noOfBoxes: {
		            	required: "No of Boxes Required!",
		            	digits:"Enter only number"
		            },
		            noOfBills: {
		            	required: "No of Bill Required!",
		            	digits:"Enter only number"
		            },
		            noOfPackets: {
		            	required: "No of Packets Required!",
		            	digits:"Enter only number"
		            },
		            receivedfrom: {
		            	required: "Received Form Required!"
		            },
		            billsReceivedDate: {
		            	required: "Bill Received Date Required!"
		            }
		        },
		        errorPlacement: function (error, element) {
		            var err= $("<br/>").add(error);   
		            $(element).after(err);
		        }
		});

			
	   $("#bpc-rc-form").validate({
			rules:{
				billcycleid:{required:true,min:1},
			    billsCount:{required: true, maxlength:50, digits:true},
			    billsPickupDate:{required: true, maxlength:50},
			    reportsClosingDate:{required:true, maxlength:50},
			    statusid:{required:true,min:1}
			},
			messages: {
				billcycleid: {
					required: "Select Bill Cycle!",
					min: "Select Bill Cycle!"
	            },
	            	billsCount: {
	            	required: "Bill Count Required!"
	            },
	            billsPickupDate: {
	            	required: "Bill Pickup Date Required!"
	            },
	            reportsClosingDate: {
	            	required: "Report Closing Date Required!"
	            },
	            statusid: {
	            	required: " Select Status!",
	            	min: "Select Status!"
	            }
	        },
	        errorPlacement: function (error, element) {
	            var err= $("<br/>").add(error);   
	            $(element).after(err);
	        }
				
		});
		
		
		$("#agencyForm").validate({
			rules:{
			    agencyname:{required:true},
			    address:{required: true},
			    zone:{required: true}
			},
		    messages: {
				agencyname: {
					required: "Agency name Required!"
				},
	            address: {
	            	required: "Address Required!"
	            },
	            zone: {
	            	required: "Zone Required!"
	            }
	        },
	        errorPlacement: function (error, element) {
	            var err= $("<br/>").add(error);   
	            $(element).after(err);
	        }
			});
		
		
		$("#ucd-form").validate({
			rules:{
				//zone:{required:true,equel:1},
				agencyId:{required:true,min:1},
			    fl:{required: true,digits: true, maxlength:50},
			    mo:{required: true,digits: true, maxlength:50},
			    totalBillsCount:{required: true,digits: true, maxlength:50},
			    noofbags:{required: true,digits: true, maxlength:10},
			    dispatchdate:{required: true,maxlength:50},
			    docketNo:{required: true,digits: true, maxlength:50}
			},
			messages: {
				zone: {
					required: "Zone  Required!",
					min: "Zone  Required"
            	},
				agencyId: {
					required: "Agency name Required!",
					min: "Agency name Required"
	            },
	            fl: {
	            	required: "FL Required!"
	            },
	            mo: {
	            	required: "MO Required!"
	            },
	            totalBillsCount: {
	            	required: "Total Bills Count Required!"
	            },
	            noofbags: {
	            	required: "No of Bags/Boxes Required!"
	            },
	            dispatchdate: {
	            	required: "Dispatch Date Required!"
	            },
	            docketNo: {
	            	required: "Docket No Required!"
	            }
	        },
	        errorPlacement: function (error, element) {
	            var err= $("<br/>").add(error);   
	            $(element).after(err);
	        }
		});		
		
		$("#setDBFrom").validate({
			rules:{
				deliveryboyuserid:{required:true,min:1},
				pincode:{required:true,min:1},
				deliveryboyuserid:{required:true,min:1}
				
				/*pincode:{required: {
					depends: function(element){
		            	return $("#billdate").val()=="";
		        }}},
				billdate:{required: {
		        	depends: function(element){
		            	return $("#pincode").val()=="";
		        }}}*/
			},
			messages: {
				deliveryboyuserid: {
					required: "Select Delivery Boy!",
					min: "Select Delivery Boy!"
	            },
	            pincode: {
	            	required: "Pincode or Bill date Required!"
	            },
                billdate: {
                	required: "Bill Date or Pincode Required!"
                }
	        },
	        errorPlacement: function (error, element) {
	            var err= $("<br/>").add(error);   
	            $(element).after(err);
	        }
		});		
		
		$("#excelDownload-BillDateForm").validate({
			rules:{
				billdate:{required:true},
				billcycleid:{required:true}
			},
			messages: {
				billdate: {
                	required: "Bill Date Required!"
                },
                billcycleid: {
                	required: "Bill Cycle Required!"
                }
	        },
	        errorPlacement: function (error, element) {
	            var err= $("<br/>").add(error);   
	            $(element).after(err);
	        }
		});		


		$("#invoiceForm").validate({
			rules:{
				vendorid:{required:true, min:1},
				fromDate:{required:true},
				toDate:{required:true},
				agree: "required"
			},
			messages: {
				vendorid: {
                	required: "Select Vendor Name!",
                		min: "Select Vendor Name!"
                },
                fromDate: {
                	required: "From Date Required!"
                },
                toDate: {
                	required: "To Date Required!"
                }
	        },
	        errorPlacement: function (error, element) {
	            var err= $("<br/>").add(error);   
	            $(element).after(err);
	        }
		});	
		
		$("#scanCountForm").validate({
			rules:{
				userid:{required:true, min:1},
				statusid:{required:true, min:1},
				fromDate:{required:true},
				toDate:{required:true},
				agree: "required"
			},
			messages: {
				userid: {
                	required: "Select DEO Name!",
                		min: "Select DEO Name!"
                },
                statusid: {
                	required: "Select Status!",
                		min: "Select Status!"
                },
                fromDate: {
                	required: "From Date Required!"
                },
                toDate: {
                	required: "To Date Required!"
                }
	        },
	        errorPlacement: function (error, element) {
	            var err= $("<br/>").add(error);   
	            $(element).after(err);
	        }
		});	
		
		$("#courierExceldeleteForm").validate({
			rules:{
				vendorid:{required:true, min:1},
				billcycleid:{required:true, min:1},
				year:{required:true,min:1},
				month:{required:true,min:1},
				agree: "required"
			},
			messages: {
				vendorid: {
                	required: "Select Vendor Name!",
                		min: "Select Vendor Name!"
                },
                billcycleid: {
                	required: "Select Bill Cycle!",
                		min: "Select Bill Cycle!"
                },
                year: {
                	required: "Select Year!",
            		min: "Select Year!"
                },
                month: {
                	required: "Select Month!",
            		min: "Select Month!"
                }
	        },
	        errorPlacement: function (error, element) {
	            var err= $("<br/>").add(error);   
	            $(element).after(err);
	        }
		});
		
		$("#coaForm").validate({
			rules:{
				accountNo:{required:true},
				newAddress:{required:true}
			},
			messages: {
				accountNo: {
                	required: "Account No Required!"
                },
                newAddress: {
                	required: "New Address Required!"
            	}
            },
	        errorPlacement: function (error, element) {
	            var err= $("<br/>").add(error);   
	            $(element).after(err);
	        }
		});
		
		$("#psForm").validate( {
			rules : {
				billDate : {required : true},
				billcycleid : {required : true, min:0},
				agree : "required"
			},
			messages : {
				billDate : {
					required : "Bill Date required!"
				},
				billcycleid : {
					required : "Select the Bill Cycle!",
					min: "Select the Bill Cycle!"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});
		
		$("#listForAllocationToDeliveryBoy").validate( {
			rules : {
				selectid : {required : true , min:1},
				billcycle : {required : true },
				searchdate : {required : true},
				toSearchdate:{required : true},
				agree : "required"
			},
			messages : {
				selectid : {
					required : "Select the Delivery boy or Agent!",
					min: "Select the Delivery or Agent!"
				},
				billcycle : {
					required : "Select the Billcycle!"
				},
				searchdate : {
					required : "From Date required!"
				},
				toSearchdate : {
					required : "To Date required!"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});
		$("#coaepForm").validate( {
			rules : {
				fileName : {required : true , extension: "xls|xlsx"},
				agree : "required"
			},
			messages : {
				fileName : {
					required : "Select the File for Upload!",
					extension: "Select xls or xlsx type files."
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});
});