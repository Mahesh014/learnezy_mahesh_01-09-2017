var arrQuestions = [];
var arrAnswers = [];
var Controls = {
	Init : function() {
		this.tblQuestion = $("#tblQuestion");
		this.btnNextBelow = $("#btnNextBelow");
		this.btnPreviousBelow = $("#btnPreviousBelow");
		this.allButtons = $(".clsJsQuestionsButton");
		this.divQuestion = $("#divQuestion");
		this.spanOptionA = $("#spanOptionA");
		this.spanOptionB = $("#spanOptionB");
		this.spanOptionC = $("#spanOptionC");
		this.spanOptionD = $("#spanOptionD");
		this.radioOptionA = $("#radioOptionA");
		this.radioOptionB = $("#radioOptionB");
		this.radioOptionC = $("#radioOptionC");
		this.radioOptionD = $("#radioOptionD");
		this.allRadioOptions = $("input[name=radioQuestions]");
		this.tdSubmit = $("#tdSubmit");
	},
	tblQuestion : {},

	btnNextBelow : {},
	btnPreviousBelow : {},

	allButtons : {},

	divQuestion : {},
	spanOptionA : {},
	spanOptionB : {},
	spanOptionC : {},
	spanOptionD : {},

	radioOptionA : {},
	radioOptionB : {},
	radioOptionC : {},
	radioOptionD : {},
	allRadioOptions : {},
	tdSubmit : {}
};
var countdownTimer, seconds = 100;

$(document).ready(function() {	
	arrAnswers = [];
	arrQuestions = [];
	console.log("hi  ");
		Controls.Init();
		GetQuestionsOnTopicID();
		console.log("hi sffdfd ");
		NextPreviousButtons_Click();
		btnSubmit_Click();
		countdownTimer = setInterval('secondPassed()', 1000);
	});

function NextPreviousButtons_Click() {
	
	
	$(".clsJsQuestionsButton").click(function() {

		var isNextButton = JSON.parse($(this).attr("isNextButton"));
		var index = parseInt($(this).attr("Index"));
		console.log(index+"  ff  "+isNextButton);
		
		if (isNextButton) {
			SetAnswerForSubmit(this);
			index = index + 1;
			SetCurrentQuestion(index);

		} else {
			SetAnswerForSubmit(this);
			index = index - 1;
			SetCurrentQuestion(index);
		}
	});
}

function btnSubmit_Click() {
	$("#tblQuestion").on('click', '#btnSubmit', function() {
		SetAnswerForSubmit(this);
		console.log("pl");

		 console.log(arrAnswers);
	 console.log(JSON.stringify(arrAnswers));
	 alert();
			var testId = Common.GetQueryStringValueByName("testID");
			$.ajax( {
				type : "POST",
				url : "/E-Learning/QuestionServlet?topicId=" + testId,
				dataType : 'json',
				// contentType: 'application/json',
				data : JSON.stringify(arrAnswers),
				success : function(r) {
				if(r.Status)
					console.log(r);
					$("#TotalNotAnswered").val(r.TotalNotAnswered);
					$("#Result").val(r.Result);
					$("#TotalQuestions").val(r.TotalQuestions);
					$("#TotalAnsweredQuestions").val(r.TotalAnsweredQuestions);
				$("#frmTestResult").submit();
					//window.location = "/E-Learning/jsp/student/ResultPage.jsp?msg="+msg;
				},
				error : function(e) {
					Controls.tblQuestion.hide();
					// alert('error');
			}
			});
		});
}

function GetQuestionsOnTopicID() {
	var testId = Common.GetQueryStringValueByName("testID");
	console.log("gg"+testId);
	alert(testId);
	$.ajax( {
		type : "GET",
		url : "/E-Learning/QuestionServlet?topicId=" + testId,
		success : function(r) {
			var josn = JSON.parse(r);
			var jsonQuestions = JSON.parse(josn.Questions);
			// debugger;
			console.log(jsonQuestions);
		if (jsonQuestions.length > 0) {
			$.each(jsonQuestions, function(index, obj) {

				console.log(obj);
				var questionJson = {
					Index : index,
					Question : obj
				};
				arrQuestions.push(questionJson);
			});
			SetCurrentQuestion(0);
		} else {
			Controls.tblQuestion.hide();
		}
	},
	error : function(e) {
		Controls.tblQuestion.hide();
		// alert('error');
	}
	});
}

function SetCurrentQuestion(currentIndex) {
	var arrQuestion = GetQuestionOnCurrentIndex(currentIndex);
	if (arrQuestion.length > 0) {
		var questionJson = arrQuestion[0];
		console.log(questionJson);
		SetControlValues(questionJson);
	}
}
function GetQuestionOnCurrentIndex(currentIndex) {
	var arr = $.grep(arrQuestions, function(obj, i) {
		// console.log(JSON.stringify(obj)+", i= "+i);
			return (obj.Index == currentIndex);
		});
	return arr;
}
function SetControlValues(questionJson) {
	// debugger;
	var index = questionJson.Index;
	var question = questionJson.Question;
	// console.log("ss"+Controls.divQuestion.length);
	Controls.divQuestion.html("<b>" + question.Question + "</b>");
	Controls.spanOptionA.html("<span>" + question.AnswerA + "</span>");
	Controls.spanOptionB.html("<span>" + question.AnswerB + "</span>");
	Controls.spanOptionC.html("<span>" + question.AnswerC + "</span>");
	Controls.spanOptionD.html("<span>" + question.AnswerD + "</span>");
	Controls.allRadioOptions.attr("Index", index);
	console.log("Index  "+ index);
	Controls.allButtons.attr("Index", index).attr("QuestionId",
			question.QuestionId);

	SetCurrentQuestionAnswer(question);// If Answer alreay selected

	if (index == 0) {

		Controls.btnPreviousBelow.hide();
		Controls.tdSubmit.empty();
	} else if (index == (arrQuestions.length - 1)) {
		Controls.btnNextBelow.hide();
		var btnSubmit = $("<input>", {
			type : "button",
			value : "Submit",
			id : "btnSubmit",
			Index : index,
			QuestionId : question.QuestionId
		});
		Controls.tdSubmit.html(btnSubmit);
	} else {
		Controls.btnPreviousBelow.show();
		Controls.btnNextBelow.show();
		Controls.tdSubmit.empty();
	}
}
function SetAnswerForSubmit(btn) {
	var answer = $('input:radio[name="radioQuestions"]:checked').val();
	if (typeof answer === 'undefined') {
		answer = "";
	}
	$('input:radio[name="radioQuestions"]').removeAttr('checked');
	var questionId = $(btn).attr("QuestionId");
	var IsAnsAlredySet = IsQuestionAnswerSelected(questionId);

	if (IsAnsAlredySet) {
		UpdateSelectedAnswer(questionId, answer);
	} else {
		var currentAns = {
			QuestionId : questionId,
			Answer : answer
		};
		arrAnswers.push(currentAns);
	}
}

function IsQuestionAnswerSelected(questionId, answer) {
	var returnVal = false;
	if (arrAnswers.length > 0) {
		var arr = $.grep(arrAnswers, function(obj, i) {
			return (obj.QuestionId == questionId);
		});
		returnVal = (arr.length > 0);
	}
	return returnVal;
}

function UpdateSelectedAnswer(questionId, answer) {

	var arr = $.grep(arrAnswers, function(obj, i) {
		return (obj.QuestionId == questionId);
	});
	if (arr.length > 0) {
		var currentAns = arr[0];
		currentAns.Answer = answer;
	}
}

function GetSelectedAnswerOnQuestionId(questionId) {

	var arr = $.grep(arrAnswers, function(obj, i) {
		return (obj.QuestionId == questionId);
	});
	return arr;
}
function SetCurrentQuestionAnswer(question) {
	var arr = GetSelectedAnswerOnQuestionId(question.QuestionId);
	if (arr.length > 0) {
		var currentAns = arr[0];

		if (currentAns.Answer != "" && currentAns.Answer != null) {
			var selectedRadio = $('input:radio[name="radioQuestions"][value="' + currentAns.Answer + '"]');
			if (selectedRadio.length > 0) {
				selectedRadio.prop('checked', true);
			}
		}
	}
}

function secondPassed() {
	var minutes = Math.round((seconds - 30) / 60);
	var remainingSeconds = seconds % 60;
	if (seconds == 8) {
		alert("You have only Less than 10 Seconds");
	}
	document.getElementById('countdown').innerHTML = minutes + ":"
			+ remainingSeconds;
	if (seconds == 0) {
		clearInterval(countdownTimer);
		document.getElementById('countdown').innerHTML = "Time UP";
		alert("Your Time's Up");
		var index = $("#btnNextBelow").attr("index");
		var questionid = $("#btnNextBelow").attr("questionid");

		var btnSubmit = $("<input>", {
			type : "button",
			value : "Submit",
			id : "btnSubmit",
			Index : index,
			QuestionId : questionid,
			style:"display:none"
		});
		Controls.tdSubmit.html(btnSubmit);

		$(btnSubmit).click();
	} else {
		seconds--;
	}
}