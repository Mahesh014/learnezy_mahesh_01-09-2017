<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui">
<title>LearnEzy.com - Registration</title>
<link href="favicon.png" type="image/x-icon" rel="shortcut icon">
<link href="assets/css/master.css" rel="stylesheet">

<!-- BOOTSTRAP -->
<link href="assets/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="assets/css/bootstrap-responsive.css" rel="stylesheet" media="screen">

<!-- FONT AWESOME -->
<link href="assets/css/font-awesome.min.css" rel="stylesheet" media="screen">


<link rel="stylesheet" href="assets/css/tmm_form_wizard_style_demo.css" />
		<link rel="stylesheet" href="assets/css/grid.css" />
		<link rel="stylesheet" href="assets/css/tmm_form_wizard_layout.css" />
		
</head>

<body> 

<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->

<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200"> 
  
  <div id="wrapper"> 
    
    <!-- HEADER -->
    <header class="header">
      <div class="top-header">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <!--<div class="top-header__contacts"><i class="icon stroke icon-Phone2"></i>(123) 0800 12345</div>-->
              <div class="top-header__contacts"><i class="icon stroke icon-Mail"></i><a href="mailto:Info@learnezy.com">Info@learnezy.com</a></div>
              <div class="top-header__link">
                <button class="btn-header btn-effect">LATEST</button>
                <span>We have added new courses today ...</span> </div>
              <div class="header-login"> <a class="header-login__item" href="javascript:void(0);"><i class="icon stroke icon-User"></i>Sign In</a> <a class="header-login__item" href="javascript:void(0);">Register</a> </div>
            </div>
            <!-- end col  --> 
          </div>
          <!-- end row  --> 
        </div>
        <!-- end container  --> 
      </div>
      <!-- end top-header  -->
      
      <div class="container">
        <div class="row">
          <div class="col-xs-12"> <a class="header-logo" href="index.html"><img class="header-logo__img" src="assets/img/logo.png" alt="Logo"></a>
            <div class="header-inner">
              <div class="header-search">
                <div class="navbar-search">
                  <form id="search-global-form">
                    <div class="input-group">
                      <input type="text" placeholder="Type your search..." class="form-control" autocomplete="off" name="s" id="search" value="">
                      <span class="input-group-btn">
                      <button type="reset" class="btn search-close" id="search-close"><i class="fa fa-times"></i></button>
                      </span> </div>
                  </form>
                </div>
              </div>
              <a id="search-open" href="#fakelink"><i class="icon stroke icon-Search"></i></a> <a class="header-cart" href="/"> <i class="icon stroke icon-ShoppingCart"></i></a>
               <nav class="navbar yamm">
                <div class="navbar-header hidden-md  hidden-lg  hidden-sm ">
                  <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                </div>
                <div id="navbar-collapse-1" class="navbar-collapse collapse">
                  <ul class="nav navbar-nav">
                    <li class="dropdown"><a href="home.html">Home<span class="nav-subtitle">OUR World</span></a>
                     
                    </li>
                    <li class="dropdown"> <a href="courses-1.html">COURSES<span class="nav-subtitle">What We Offers</span></a>
                      <ul role="menu" class="dropdown-menu">
                        <li><a href="courses-category.html">courses category</a>
                          
                        </li>
                        <li><a href="course-details.html">course details</a></li>
                      </ul>
                    </li>
                   
                    <li><a href="contact.html">CONTACT<span class="nav-subtitle">say us hi</span></a></li>
                  </ul>
                </div>
              </nav>
              <!--end navbar --> 
              
            </div>
            <!-- end header-inner --> 
          </div>
          <!-- end col  --> 
        </div>
        <!-- end row  --> 
      </div>
      <!-- end container--> 
    </header>
    <!-- end header -->
    
     <div class="wrap-title-page">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h1 class="ui-title-page">Register to become a member of Learnezy</h1>
          </div>
        </div>
      </div>
      <!-- end container--> 
    </div>
    <!-- end wrap-title-page -->
    
    <div class="section-breadcrumb">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="wrap-breadcrumb clearfix">
              <ol class="breadcrumb">
                <li><a href="index.html"><i class="icon stroke icon-House"></i></a></li>
                <li><a href="register.html">Registration</a></li>
                <li class="active">Freelancer Registration</li>
                
              </ol>
            </div>
          </div>
        </div>
        <!-- end row --> 
      </div>
      <!-- end container --> 
    </div>
    <!-- end section-breadcrumb -->
       
    
    <div class="main-content">
          
              <div class="container">
    <div class="buttons">
            	
       	  <button class="btn1 login-btn"><i class="fa fa-facebook"></i>Login with Facebook</button>
                
                <button class="btn2 login-btn"><i class="fa fa-google-plus"></i>Login with Google</button>
                
                <button class="btn3 login-btn"><i class="fa fa-yahoo"></i>Login with Yahoo</button>
                
                <button class="btn4 login-btn"><i class="fa fa-linkedin"></i>Login with Linkein</button>
                
                <button class="btn5 login-btn"><i class="fa fa-windows"></i>Login with Window Live</button>
               
            </div>
            
            
          <div class="row">
          
          
            	<div class="span12">
                
                <div class="form-container">

				<div id="tmm-form-wizard" class="container substrate">

					<div class="row" align="center">
						<div class="col-xs-12">
							<h2 class="form-login-heading"><span>Registration Form For Freelancer</span></h2>
						</div>

					</div><!--/ .row-->

					<div class="row stage-container" align="center">

						<div class="stage tmm-current col-md-3 col-sm-3" id="topDiv1">
							<div class="stage-header head-icon head-icon-lock"></div>
							<div class="stage-content">
								<h3 class="stage-title">Registration</h3>
								<div class="stage-info">
									Registration details
								</div> 
							</div>
						</div><!--/ .stage-->

						<div class="stage col-md-3 col-sm-3 " id="topDiv2">
							<div class="stage-header head-icon head-icon-user"></div>

							<div class="stage-content">
								<h3 class="stage-title">Courses</h3>
								<div class="stage-info">
									Select the course
								</div>
							</div>
						</div><!--/ .stage-->

						<div class="stage col-md-3 col-sm-3" id="topDiv3">
							<div class="stage-header head-icon head-icon-payment"></div>
							<div class="stage-content">
								<h3 class="stage-title">Payment Information</h3>
								<div class="stage-info">
									Make the payment
								</div>
							</div>
						</div><!--/ .stage-->

						 <div class="stage col-md-3 col-sm-3" id="topDiv4">
							<div class="stage-header head-icon head-icon-details"></div>
							<div class="stage-content">
								<h3 class="stage-title">Login</h3>
								<div class="stage-info">
									Login details
								</div> 
							</div>
						</div><!--/ .stage-->

					</div><!--/ .row-->
<html:form action="studentRegistration.do?action=FreelancerRegistration" method="post" styleClass="register" styleId="register"   enctype="multipart/form-data">
						<div id="div1"   > 
					<div class="row">

						<div class="col-xs-12" >

							<div class="form-header">
								<div class="form-title form-icon title-icon-lock">
									<b>Registration details</b> 
								</div>
								<div   class="form-title" style="width: 725px;text-align: center;" >
									<b style="color: red;"><logic:notEmpty name="status"><bean:write name="status"/> </logic:notEmpty></b>
								</div>
								<div class="steps">
									Steps 1 - 4
								</div>
							</div><!--/ .form-header-->

						</div>

					</div><!--/ .row-->
			 
					
						


						<div class="form-wizard" >
							
							<div class="row">

								<div class="col-md-12 col-sm-7">

									
<div class="row">
										
										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="first-name">First Name <font style="color: red;">*</font></label>
												<input type="text" id="firstName" placeholder="First Name" name="firstName"  	onkeyup="validFNameonkey(this)" autofocus  />
												
											</fieldset><!--/ .input-first-name-->
										</div>
										
										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="last-name">Last Name</label>
												<input type="text" id="lastName"  name="lastName"	onkeyup="validLNameonkey(this)"  placeholder="Last Name"   />
												
											</fieldset><!--/ .input-first-name-->
										</div>
										
									</div><!--/ .row-->
									<div class="row">
										
										<div class="col-md-6 col-sm-6">

											<div class="input-block">
												<label>Date Of Birth <font style="font-size: 12px;">(DD/MM/YYYY)</font> </label>
												<div class="input-block">
													<input type="text" name="dob" id="datepicker1" size="8"   onchange="onDateChange();"  placeholder="DOB" />
												</div><!--/ .dropdown-->
												
											</div><!--/ .input-role-->
											
										</div>

										<div class="col-md-6 col-sm-6">

											<fieldset class="input-block">
												<label>Gender</label>
												<div class="dropdown">
													<select name="gender" id="gender" class="dropdown-select"  >
														<option value="-1">Select</option>
									<option value="Male">Male</option>
									<option value="Female">Female</option>
													</select>
												</div><!--/ .dropdown-->
												
											</fieldset><!--/ .input-role-->
											
										</div>

									</div><!--/ .row-->
									<div class="row">
										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="last-name">Qualification</label>
												<input type="text" name="qualification" id="qualification"	onkeyup="isValidName(this, 'Qualification')"  placeholder="Qualification"  />
												
											</fieldset><!--/ .input-first-name-->
										</div>
										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="email">Email <font style="color: red;">*</font></label>
												<input type="text" name="emailId" id="emailId"	onchange="validemail(this)" id="email" class="form-icon form-icon-mail" placeholder="Please enter your email ID"  />
												
											</fieldset><!--/ .input-email-->
										</div>
										
									</div><!--/ .row-->
									
									<div class="row">
										
										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="phone">Mobile No <font style="color: red;">*</font></label>
												<input type="text" name="contactno"  id="contactno" onkeyup="fmtmobile();" maxlength="10"  id="contactno" class="form-icon form-icon-phone" placeholder="Mobile No"  />
												
											</fieldset><!--/ .input-phone-->
										</div><!--/ .input-phone-->
										
										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="phone">Alternative Contact No</label>
												<input type="text"  name="phone" id="phone" onkeyup="fmtcontacno();"	maxlength="13"  id="phone" class="form-icon form-icon-phone" placeholder="Alternative Contact No"  />
												
											</fieldset><!--/ .input-phone-->
										</div><!--/ .input-phone-->
										
									</div><!--/ .row-->
<div class="row">

										<div class="col-md-12 col-sm-12">
											<fieldset class="input-block">
												<label for="address">Area Of Expertise</label>
												<textarea rows="2" cols="77" name="areofexper"></textarea>
												
											</fieldset><!--/ .input-phone-->
										</div>
										
									</div><!--/ .row-->
									
									<div class="row">

										<div class="col-md-12 col-sm-12">
											<fieldset class="input-block">
												<label for="address">Address</label>
												<input type="text" id="address" name="address" placeholder="Address"  />
												
											</fieldset><!--/ .input-phone-->
										</div>
										
									</div><!--/ .row-->

										<div class="row">

										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="address">Country*</label>
												<div class="dropdown">
												<html:select property="countryid" styleId="countryid"  styleClass="dropdown-select" >
													<html:option value="-1">--Select Country--</html:option>
													<html:options collection="countryList" property="countryid" labelProperty="countryname" />
												</html:select>
												</div>
											</fieldset><!--/ .input-phone-->
										</div>
										
									

										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="address">State*</label>
												<div class="dropdown">
												<select name="stateid" id="stateid" class="dropdown-select" >
													<option value="-1">-Select State-</option>
												</select>
												</div>
											</fieldset><!--/ .input-phone-->
										</div>
										
									</div>
									<div class="row">
										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="address">City*</label>
												<div class="dropdown">
												<select name="cityid" id="cityid"  class="dropdown-select">
													<option value="-1">-Select City-</option>
												</select>
												</div>
											</fieldset><!--/ .input-phone-->
										</div>
										
										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="address">Pincode <font style="color: red;">*</font></label>
												
												<input type="text"  name="pincode" placeholder="Pincode" />
												
											</fieldset><!--/ .input-phone-->
										</div>
										
									</div><!--/ .row-->
									 
									
									<div class="row" style="height: 100px;"> 
										<div class="col-md-6 col-sm-6">
											<div class="input-block">
												
												 
													 <label>Profile Photo:</label>
								<input name="imageFile" id="imageFile" type="file" placeholder="upload file Photo"  />
												 
												
											</div><!--/ .input-country-->
											
										
										</div>
										 	
									</div>
									
								
								<div class="col-md-10 col-sm-7" style="margin-top: 15px;">

									<fieldset class="sign">
									<legend >Sign in Details</legend>
                                    	<div class="row">
										<div class="col-md-12 col-sm-12" >
											<fieldset class="input-block">
												<label for="card-number">User Name <font style="color: red;">*</font></label><br />
												<input type="text"  name="uname"  id="uname" placeholder="User Name"  value="" size="20" style="float: left;width: 42%" /> 
												 <div id="uNameDiv"  ></div>
											</fieldset>
										</div>
										</div>
										<div class="row">
										<div class="col-md-6 col-sm-6" >
											<fieldset class="input-block">
												<label for="card-number">Password <font style="color: red;">*</font> <span style="font-size: 10px;font-style: italic;color: red;margin-left: 192px;" title="Password should contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character and min 8 characters" >Rules</span></label>
												<input type="password" name="password"  id="password" class="passWord" value="" onchange="checkPasswordStrong(this);" placeholder="Password" style="float: left;width: 90%"   />	 
           										<div id="passDiv" ></div>
											</fieldset>
										</div>
										
										
										<div class="col-md-6 col-sm-6" >
											<fieldset class="input-block">
												<label for="card-number">Confirm Password <font style="color: red;">*</font> </label>
												<input name="retypepassword" id="retypepassword" type="password" onchange="checkpassword();"   placeholder="Confirm Password"  style="float: left;width: 90%" />	 
         										<div id="confDiv" ></div>
											</fieldset>
										</div>
										
										<div class="col-md-12 col-sm-12" id="passRules" style="height: 30px;font-size: 12px;font-style: italic;color: red;" >
											 
										</div>
									 
										<div class="col-md-6 col-sm-6" style="margin-top: 5px; " >
										<fieldset class="input-block">
											 <input id="captchaText" type="text" readonly="readonly"  value="" style="float: left;width: 45%;background-image: url(img/BG1.png);color: black;border: none;text-align: center;" />
											<input id="captchabox" type="text" style="float:left;width: 45%"  />	
											<div id="captchaDiv" ></div>	
											</fieldset>
										</div>
										
										
										</div>		
									</fieldset><!--/ .input-email-->

								</div>

							</div><!--/ .row-->
							
						</div><!--/ .form-wizard-->
						
						

						
						
						
						
						</div>
						<div class="next1">
							<button class="btn btn-info btn-effect" type="submit" id="validateRegister"><span>Submit</span></button>
							<div class="button-divider"></div>
						</div>
						</html:form>
				 
							
							

						</div>

					</div><!--/ .row-->
					 
                
                
                
				</div>  <!-- span12 -->
               
            </div>   <!-- row -->
        
        
        </div>  <!-- container -->
        
      
    </div>
    <!-- end main-content -->
    
    <footer class="footer wow fadeInUp" data-wow-duration="2s">
      <div class="container">
        <div class="footer-inner">
          <div class="row">
            <div class="col-lg-3 col-sm-3">
              <section class="footer-section courses">
                <h3 class="footer-title">ABOUT US</h3>
                <a href="javascript:void(0);"><img class="footer-logo img-responsive" src="assets/img/logo_white.png"  alt="Logo"></a>
                <div class="footer-info">Fusce eleifend. Donec sapien sed pha seah lusa. Pellentesque lu amus lorem arcu sem per duiac. Vivamus porta. Sed at nisl praesnt blandit mauris vel erat.</div>
                <!--<div class="footer-contacts footer-contacts_mod-a"> <i class="icon stroke icon-Pointer"></i>
                  <address class="footer-contacts__inner">
                  370 Hill Park, Florida, USA
                  </address>
                </div>-->
                <!--<div class="footer-contacts"> <i class="icon stroke icon-Phone2"></i> <span class="footer-contacts__inner">Call us 0800 12345</span> </div>-->
                <div class="footer-contacts"> <i class="icon stroke icon-Mail"></i> <a class="footer-contacts__inner" href="mailto:Info@learnezy.com">Info@learnezy.com</a> </div>
              </section>
              <!-- end footer-section --> 
            </div>
            <!-- end col -->
            
            <div class="col-lg-2 col-sm-3">
              <section class="footer-section courses">
                <h3 class="footer-title">USEFUL LINKS</h3>
                <ul class="footer-list list-unstyled">
                  <li class="footer-list__item"><a class="footer-list__link" href="javascript:void(0);">Academies</a></li>
                  <li class="footer-list__item"><a class="footer-list__link" href="javascript:void(0);">Our Latest Courses</a></li>
                  <li class="footer-list__item"><a class="footer-list__link" href="javascript:void(0);">Who We Are</a></li>
                  <li class="footer-list__item"><a class="footer-list__link" href="javascript:void(0);">Get In Touch</a></li>
                  <li class="footer-list__item"><a class="footer-list__link" href="javascript:void(0);">Courses Categories</a></li>
                  <li class="footer-list__item"><a class="footer-list__link" href="javascript:void(0);">Support & FAQ's</a></li>
                  <li class="footer-list__item"><a class="footer-list__link" href="javascript:void(0);">Terms & Conditions</a></li>
                  <li class="footer-list__item"><a class="footer-list__link" href="javascript:void(0);">Privacy Policy</a></li>
                </ul>
              </section>
              <!-- end footer-section --> 
            </div>
            <!-- end col -->
            
            <div class="col-lg-3 col-sm-3">
              <section class="footer-section courses">
                <h3 class="footer-title">LATEST TWEETS</h3>
                <div class="tweets">
                  <div class="tweets__text">What is the enemy of #creativity?</div>
                  <div><a href="javascript:void(0);">http://enva.to/hVl5G</a></div>
                  <span class="tweets__time">9 hours ago</span> </div>
                <div class="tweets">
                  <div class="tweets__text">An agile framework can produce the type of lean marketing essential for the digital age <a href="javascript:void(0);">@aholmes360 #IMDS15</a></div>
                  <span class="tweets__time">9 hours ago</span> </div>
                <a class="tweets__link" href="javascript:void(0);">Follow @Learnezy</a> </section>
              <!-- end footer-section --> 
            </div>
            <!-- end col -->
            
            <div class="col-lg-4 col-sm-3">
              <section class="footer-section">
                <h3 class="footer-title">QUICK CONTACT</h3>
                <form class="form">
                  <div class="form-group">
                    <input class="form-control" type="text" placeholder="Your Name">
                    <input class="form-control" type="email" placeholder="Email address">
                    <textarea class="form-control" rows="7" placeholder="Message"></textarea>
                    <button class="btn btn-primary btn-effect">SEND MESSSAGE</button>
                  </div>
                </form>
              </section>
              <!-- end footer-section --> 
            </div>
            <!-- end col --> 
          </div>
          <!-- end row --> 
        </div>
        <!-- end footer-inner -->
        
        <div class="row">
          <div class="col-xs-12">
            <div class="footer-bottom">
              <div class="copyright">Copyright © 2016 <a href="javascript:void(0);">LearnEzy</a>, Online Learning  |  Designed &amp; Developed by <a href="http://www.itechsolutions.in">Itech Solutions</a></div>
              <ul class="social-links list-unstyled">
                <li><a class="icon fa fa-facebook" href="javascript:void(0);"></a></li>
                <li><a class="icon fa fa-twitter" href="javascript:void(0);"></a></li>
                <li><a class="icon fa fa-google-plus" href="javascript:void(0);"></a></li>
                <li><a class="icon fa fa-instagram" href="javascript:void(0);"></a></li>
                <li><a class="icon fa fa-linkedin" href="javascript:void(0);"></a></li>
                <li><a class="icon fa fa-youtube-play" href="javascript:void(0);"></a></li>
              </ul>
            </div>
            <!-- end footer-bottom --> 
          </div>
          <!-- end col --> 
        </div>
        <!-- end row --> 
      </div>
      <!-- end container --> 
    </footer>
  </div>
  <!-- end wrapper --> 
</div>
<!-- end layout-theme --> 

<!-- Jquery Lib -->
<script src="assets/js/jquery-1.10.1.min.js"></script>

<!-- SCRIPTS --> 
<script src="assets/js/jquery-migrate-1.2.1.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<script src="assets/js/modernizr.custom.js"></script> 
<script src="assets/js/waypoints.min.js"></script> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> 

<!--THEME--> 
<script  src="assets/plugins/sliderpro/js/jquery.sliderPro.min.js"></script> 
<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script> 
<script src="assets/plugins/isotope/jquery.isotope.min.js"></script> 
<script src="assets/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script> 
<script src="assets/plugins/datetimepicker/jquery.datetimepicker.js"></script> 
<script src="assets/plugins/jelect/jquery.jelect.js"></script> 
<script src="assets/plugins/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js"></script> 
<script src="assets/js/cssua.min.js"></script> 
<script src="assets/js/wow.min.js"></script> 
<script src="assets/js/custom.min.js"></script>

<script src="assets/js/tmm_form_wizard_custom.js"></script>
</body>
</html>
