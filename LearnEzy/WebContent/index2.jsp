<!DOCTYPE html>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui">
<title>LearnEzy.com - Home</title>
</head>

<body onload="location.hash = 'displaynew';"> 
<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->

<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200"> 
  
  <div id="wrapper"> 
    
    <jsp:include page="Commonheader.jsp"></jsp:include>
    
    <div class="main-content">
      <div id="sliderpro1" class="slider-pro main-slider">
        <div class="sp-slides">
          
          <div class="sp-slide"> <img class="sp-image" src="assets/media/main-slider/2.jpg"
					data-src="assets/media/main-slider/2.jpg"
					data-retina="assets/media/main-slider/2.jpg" alt="img"/>
            <div class="item-wrap sp-layer  sp-padding" data-horizontal="200" data-vertical="30"
					data-show-transition="left" data-hide-transition="up" data-show-delay="400" data-hide-delay="200">
              <div class="main-slider__inner">
                <div class="main-slider__title" ><h1>BEST ONLINE LEARNING</h1></div>
                <div class="main-slider__subtitle ">THE EASIER WAY</div>
                <a class="main-slider__btn btn btn-warning btn-effect" href="/">START A COURSE</a> </div>
            </div>
          </div>
          
          <div class="sp-slide"> <img class="sp-image" src="assets/media/main-slider/1.jpg"
					data-src="assets/media/main-slider/1.jpg"
					data-retina="assets/media/main-slider/1.jpg" alt="img"/>
            <div class="item-wrap sp-layer  sp-padding" data-horizontal="700" data-vertical="1"
					data-show-transition="left" data-hide-transition="up" data-show-delay="400" data-hide-delay="200">
              <div class="main-slider__inner text-center">
                <div class="main-slider__title">BEST ONLINE LEARNING</div>
                <div class="main-slider__subtitle">THE EASIER WAY</div>
                <a class="main-slider__btn btn btn-warning btn-effect" href="/">START A COURSE</a> </div>
            </div>
          </div>
          
          
        </div>
      </div>
      <!-- end main-slider -->
      
      <div class="section_find-course wow fadeInUp" data-wow-duration="2s">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <div class="find-course find-course_mod-b">
                <html:form action="DynamicData.do?action=search" styleClass="find-course__form">
                  <div class="form-group"> <i class="icon stroke icon-Search"></i>
                    <input class="form-control" type="text" placeholder="Course Keyword ..." name="courseName">
                    <div class="jelect" >
                      <input value="0" type="text" class="jelect-input">
                       <html:select property="cityid" styleClass="jelect-option jelect-option_state_active">
                      <div tabindex="0" role="button" class="jelect-current">All Locations</div>
      
                   
					<html:option value="null">--Select country--</html:option>
					<html:options collection="cityList" property="cityid" labelProperty="cityname" />
					</html:select>
                    
                    </div>
                    <!-- end jelect --> 
                  </div>
                  <!-- end form-group -->
                  <div class="find-course__wrap-btn">
                    <input type="submit" class="btn btn-info btn-effect" name="SEARCH COURSE"/>
                  </div>
                </html:form>
              </div>
              <!-- end find-course -->
              <div class="find-course__info">Search more than 50,000 online courses available from 2,000 Academies</div>
            </div>
            <!-- end col --> 
          </div>
          <!-- end row --> 
        </div>
        <!-- end container --> 
      </div>
      <!-- end section-default -->
      
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="border-decor_top">
              <section class="section-default">
                <div class="wrap-title wow zoomIn" data-wow-duration="2s">
                  <h2 class="ui-title-block ui-title-block_mod-d">We Bring You World's Best Courses From All Top Training Academies, <strong>FREE!</strong></h2>
                  <div class="ui-subtitle-block ui-subtitle-block_mod-c">Having over 9 million students worldwide and more than 50,000 online courses available.</div>
                </div>
                <!-- end wrap-title -->
                <ul class="advantages advantages_mod-b list-unstyled">
                  <li class="advantages__item wow zoomIn" data-wow-duration="2s">
                    <div class="advantages__inner">
                      <div class="advantages__info"> <span class="advantages__icon"><i class="icon stroke icon-Cup"></i></span>
                        <h3 class="ui-title-inner decor decor_mod-a">HIGHest RATED</h3>
                      </div>
                    </div>
                    <p>Fusce eleifend donec sapien phase dcua sed sa pellentesque lacus vamus lorem treb lum arcu semper duiac.</p>
                  </li>
                  <li class="advantages__item wow zoomIn" data-wow-duration="2s" data-wow-delay=".5s"> <span class="advantages__icon"><i class="icon stroke icon-Users"></i></span>
                    <div class="advantages__inner">
                      <h3 class="ui-title-inner decor decor_mod-a">SKILLED FACULTY</h3>
                      <div class="advantages__info">
                        <p>Fusce eleifend donec sapien phase dcua sed sa pellentesque lacus vamus lorem treb lum arcu semper duiac.</p>
                      </div>
                    </div>
                  </li>
                  <li class="advantages__item wow zoomIn" data-wow-duration="2s" data-wow-delay="1s"> <span class="advantages__icon"><i class="icon stroke icon-WorldGlobe"></i></span>
                    <div class="advantages__inner">
                      <h3 class="ui-title-inner decor decor_mod-a">we are GLOBAL</h3>
                      <div class="advantages__info">
                        <p>Fusce eleifend donec sapien phase dcua sed sa pellentesque lacus vamus lorem treb lum arcu semper duiac.</p>
                      </div>
                    </div>
                  </li>
                  <li class="advantages__item wow zoomIn" data-wow-duration="2s" data-wow-delay="1.5s"> <span class="advantages__icon"><i class="icon stroke icon-DesktopMonitor"></i></span>
                    <div class="advantages__inner">
                      <h3 class="ui-title-inner decor decor_mod-a">ONLINE TRAINING</h3>
                      <div class="advantages__info">
                        <p>Fusce eleifend donec sapien phase dcua sed sa pellentesque lacus vamus lorem treb lum arcu semper duiac.</p>
                      </div>
                    </div>
                  </li>
                </ul>
              </section>
              <!-- end section-advantages --> 
            </div>
            <!-- end border-decor_top --> 
          </div>
          <!-- end col --> 
        </div>
        <!-- end row --> 
      </div>
      <!-- end container -->
      
      
      
      
       
      
      <div class="section-progress wow fadeInUp section-parallax"  data-speed="25"  data-wow-duration="2s">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <ul class="list-progress list-unstyled">
                <li class="list-progress__item"> <span class="chart label-chart decor decor_mod-c" data-percent="190"><i class="icon stroke icon-WorldWide"></i><span class="percent"></span> </span> <span class="list-progress__name">ACADEMIES</span> </li>
                <li class="list-progress__item"> <span class="chart label-chart decor decor_mod-c" data-percent="250000"><i class="icon stroke icon-Book"></i><span class="percent"></span> </span> <span class="list-progress__name">COURSE CATEGORIES</span> </li>
                <li class="list-progress__item"> <span class="chart label-chart decor decor_mod-c" data-percent="824"><i class="icon stroke icon-User"></i><span class="percent"></span> </span> <span class="list-progress__name">COURSES PUBLISHED</span> </li>
                <li class="list-progress__item"> <span class="chart label-chart decor decor_mod-c" data-percent="1500"><i class="icon stroke icon-Edit"></i><span class="percent"></span> </span> <span class="list-progress__name">STUDENTS</span> </li>
              </ul>
              <!-- end list-progress --> 
            </div>
            <!-- end col  --> 
          </div>
          <!-- end row  --> 
        </div>
        <!-- end container--> 
      </div>
      <!-- end section-progress-->
      
      
      
      
      
       <!--LATEST COURSES SECTION START-->
        <section class="gray-bg">
        	<div class="container">
            	<!--SECTION HEADER START-->
            	<div class="sec-header">
                <h2 class="ui-title-block ui-title-block_mod-e">Latest <strong> Courses</strong></h2>
              <div class="wrap-subtitle">
                <div class="ui-subtitle-block ui-subtitle-block_w-line">Check Our Featured Courses</div>
              </div>
                	
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <!--SECTION HEADER END-->
                <div class="customNavigation">
                    <a class="btn prev"><i class="fa fa-angle-left"></i></a>
                    <a class="btn next"><i class="fa fa-angle-right"></i></a>
                </div>
                <div id="owl-carousel" class="owl-carousel owl-theme">
                	<!--COURSE ITEM START-->
                	<logic:notEmpty name="LatestCourseList">
                	<logic:iterate id="userrole" name="LatestCourseList" >
				
                    <div class="course">
                    	<div class="thumb">
                        	<a href="#"><img src="<%=request.getContextPath()%>/imageUpload/<bean:write name="userrole"  property="courselatestlogo" />" alt=""></a>
                            <div class="price"><span><i class="fa fa-inr"></i></span><bean:write name="userrole" property="amount" /></div>
                        </div>
                    	<div class="text">
                        	<div class="head">
                            	<h4><bean:write name="userrole" property="coursecategory" /></h4>
                                <div class="rating">
                                    <span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span>
                                </div>
                            </div>
                            <div class="course-name">
                            	<p><bean:write name="userrole" property="coursenamelatest" /></p>
                                <span><bean:write name="userrole" property="amount" /></span>
                            </div>
                            <div class="course-detail-btn">
                            	<a href="#">Subscribe</a>
                                <a href="#">Detail</a>
                            </div>
                        </div>
                    </div>
                    </logic:iterate>
				     </logic:notEmpty>
                    <!--COURSE ITEM END-->
                  
                </div>
            </div>
        </section>
        <!--LATEST COURSES SECTION END-->
      
          
      
                    
      <section class="">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <div class="section__inner">
                   <!--COURSES TOPIC SECTION START-->
        <section class="gray-bg tabs-section">
        	<div class="container">
            	<!--SECTION HEADER START-->
            	<div class="sec-header">
                 <h2 class="ui-title-block ui-title-block_mod-e">Course <strong>Categories</strong></h2>
              <div class="wrap-subtitle">
                <div class="ui-subtitle-block ui-subtitle-block_w-line">Discover courses by topic</div>
              </div>
                	
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <!--SECTION HEADER END-->
                
            </div>


            
            <div class="course-tabs">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                <logic:notEmpty name="LatestCourseCategory">
                <logic:iterate id="userrole" name="LatestCourseCategory">
                  <li><a href="DynamicData.do?action=dynamiccourse&NAME=<bean:write name="userrole" property="coursecatid"/>"><i class="fa fa-book"></i><bean:write name="userrole"  property="coursecatname"/></a></li>
                  </logic:iterate>
                  </logic:notEmpty>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content container" id="displaynew">
                  <div id="web" class="tab-pane fade active in">
                    <ul class="course-topics row">
                    	<logic:notEmpty name="courselist">
                    	<logic:iterate id="courseid" name="courselist">
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic3.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4><bean:write name="courseid" property="coursenamenew" /></h4>
                                <p><bean:write name="courseid" property="coursenamenew" /></p>
                                <span><bean:write name="courseid" property="amountnew" /></span>
                            </div>
                        </li>
                        </logic:iterate>
                         </logic:notEmpty>
                        <!--LIST ITEM START-->
                     
                       
                        <!--LIST ITEM START-->
                        
                        <!--LIST ITEM START-->
                    </ul>
                  </div>
                 
                  
                </div>
            </div>
        </section>
        <!--COURSES TOPIC SECTION END-->
      
      
                
              </div>
            </div>
            <!-- end col --> 
            
          </div>
          <!-- end row --> 
        </div>
        <!-- end of container -->
      </section>
      <!-- end section_mod-d -->
      
      
     
      <section class="section_mod-c wow bounceInUp section-categories" data-wow-duration="2s" >
        <div class="container" id="academydisplay">
          <div class="row">
            <div class="col-xs-12">
              <h2 class="ui-title-block ui-title-block_mod-e">Our <strong>Academies</strong></h2>
              <div class="wrap-subtitle">
                <div class="ui-subtitle-block ui-subtitle-block_w-line">We have very skilled and professionals for taking classes</div>
              </div>
              <div class="carousel_mod-a owl-carousel owl-theme enable-owl-carousel"
											data-min480="2"
											data-min768="3"
											data-min992="4"
											data-min1200="4"
											data-pagination="true"
											data-navigation="false"
											data-auto-play="4000"
											data-stop-on-hover="true" >
											 <logic:notEmpty name="companyList">
    
    	<logic:iterate id="userrole" name="companyList" >
											
                <div class="staff">
                  <div class="staff__media"><img class="img-responsive" src="<%=request.getContextPath()%>/imageUpload/<bean:write name="userrole"  property="logo" />" height="250" width="270" alt="foto">
                    <div class="staff__hover"></div>
                  </div>
                  <div class="staff__inner">
                    <h3 class="staff__title"><bean:write name="userrole" property="compname" /></h3>
                    <div class="staff__categories"> <a class="post-link" href="javascript:void(0);"><bean:write name="userrole" property="address"/></a></div>
                    <div class="staff__description"><bean:write name="userrole"  property="countryid"></bean:write></div>
                   
                  </div>
                </div>
           
                </logic:iterate>
                </logic:notEmpty>
                
              </div>
            </div>
            <!-- end col --> 
          </div>
          <!-- end row --> 
        </div>
        <!-- end container --> 
      </section>
      <!-- end section_mod-c -->
                    
      <div class="container">
        <div class="border-decor_bottom">
          <div class="section-default">
            <div class="row">
              <div class="col-md-4">
                <section class="section-area wow bounceInRight courses" data-wow-duration="2s">
                  <div class="title-w-icon"> <i class="icon stroke icon-Agenda"></i>
                    <h2 class="ui-title-inner">UPCOMING COURSES</h2>
                  </div>
                  <article class="post post_mod-e clearfix">
                    <div class="box-date"><span class="number">25</span>AUG</div>
                    <div class="entry-main">
                      <h3 class="entry-title entry-title_mod-a"><a href="javascript:void(0);">How To Learn Online</a></h3>
                      <div class="entry-content">
                        <p>Eleifend donec sapien sed usaer tesqed Lorem ipsum dolor sit ban.</p>
                      </div>
                      <div class="entry-footer">
                        <div class="entry-links entry-links_mod-a"><i class="icon stroke icon-Time"></i>starting <a class="post-link" href="javascript:void(0);">07:30 am</a></div>
                      </div>
                    </div>
                  </article>
                  <!-- end post -->
                  
                  <article class="post post_mod-e clearfix">
                    <div class="box-date"><span class="number">25</span>AUG</div>
                    <div class="entry-main">
                      <h3 class="entry-title entry-title_mod-a"><a href="javascript:void(0);">How To Learn Online</a></h3>
                      <div class="entry-content">
                        <p>Eleifend donec sapien sed usaer tesqed Lorem ipsum dolor sit ban.</p>
                      </div>
                      <div class="entry-footer"> <span class="entry-time"><i class="icon stroke icon-Time"></i>starting <a class="post-link" href="javascript:void(0);">07:30 am</a></span> </div>
                    </div>
                  </article>
                  <!-- end post --> 
                </section>
                <!-- end section-area --> 
              </div>
              <!-- end col -->
              
              <div class="col-md-4 wow bounceInRight courses" data-wow-duration="2s" data-wow-delay=".7s">
                <div class="title-w-icon"> <i class="icon stroke icon-User"></i>
                  <h2 class="ui-title-inner">Featured ACADEMY</h2>
                </div>
                <article class="post post_mod-f">
                  <div class="entry-media">
                    <div class="entry-thumbnail"><img class="img-responsive" src="assets/media/posts/160x125/1.jpg" width="160" height="125" alt="Foto"> </div>
                  </div>
                  <div class="entry-main">
                    <h3 class="entry-title entry-title_mod-a"><a href="javascript:void(0);">ANDREW FINCH</a></h3>
                    <div class="entry-meta"><a class="post-link" href="javascript:void(0);">Developer</a></div>
                    <div class="entry-content">
                      <p>Phasellus luctus pulvin g bienum aliquam lig sape conde lorem ipsum dolor met cons letua.</p>
                      <p>Eeniam quis nostrud exercitation ullamco labor nisut alq gxe commodo consequat duis aute re dola. Lorem ipsum dolor sit amt consectetur adipisng elit.</p>
                    </div>
                  </div>
                  <a href="javascript:void(0);" class="post-btn btn btn-primary btn-effect">FULL PROFILE</a> </article>
                <!-- end post --> 
              </div>
              <!-- end col -->
              
              <div class="col-md-4 wow bounceInRight" data-wow-duration="2s" data-wow-delay="1.4s">
                <div class="title-w-icon"> <i class="icon stroke icon-Book"></i>
                  <h2 class="ui-title-inner">RECENT COURSES</h2>
                </div>
                <article class="post post_mod-g clearfix">
                  <div class="entry-media">
                    <div class="entry-thumbnail"> <a href="javascript:void(0);" ><img class="img-responsive" src="assets/media/posts/100x90/1.jpg" width="100" height="90" alt="Foto"/></a> </div>
                  </div>
                  <div class="entry-main">
                    <h3 class="entry-title entry-title_mod-a"><a href="javascript:void(0);">Programming for Everybody</a></h3>
                    <div class="entry-meta"> <span class="entry-autor"> <span>By </span> <a class="post-link" href="javascript:void(0);">John Milton</a> </span> <span class="entry-date"><a href="javascript:void(0);">Dec 16, 2015</a></span> </div>
                    <div class="entry-footer">
                      <ul class="rating">
                        <li><i class="icon fa fa-star"></i></li>
                        <li><i class="icon fa fa-star"></i></li>
                        <li><i class="icon fa fa-star"></i></li>
                        <li><i class="icon fa fa-star"></i></li>
                        <li><i class="icon fa fa-star-o"></i></li>
                      </ul>
                    </div>
                  </div>
                  <!-- end entry-main --> 
                </article>
                <!-- end post -->
                <article class="post post_mod-g clearfix">
                  <div class="entry-media">
                    <div class="entry-thumbnail"> <a href="javascript:void(0);" ><img class="img-responsive" src="assets/media/posts/100x90/2.jpg" width="100" height="90" alt="Foto"/></a> </div>
                  </div>
                  <div class="entry-main">
                    <h3 class="entry-title entry-title_mod-a"><a href="javascript:void(0);">Programming for Everybody</a></h3>
                    <div class="entry-meta"> <span class="entry-autor"> <span>By </span> <a class="post-link" href="javascript:void(0);">John Milton</a> </span> <span class="entry-date"><a href="javascript:void(0);">Dec 16, 2015</a></span> </div>
                    <div class="entry-footer">
                      <ul class="rating">
                        <li><i class="icon fa fa-star"></i></li>
                        <li><i class="icon fa fa-star"></i></li>
                        <li><i class="icon fa fa-star"></i></li>
                        <li><i class="icon fa fa-star"></i></li>
                        <li><i class="icon fa fa-star-o"></i></li>
                      </ul>
                    </div>
                  </div>
                  <!-- end entry-main --> 
                </article>
                <!-- end post --> 
              </div>
              <!-- end col --> 
            </div>
            <!-- end row --> 
          </div>
          <!-- end section-default --> 
        </div>
        <!-- end border-decor_bottom --> 
      </div>
      <!-- end container -->
      
      <div class="section-subscribe wow fadeInUp" data-wow-duration="2s">
        <div class="subscribe">
          <div class="container">
            <div class="row">
              <div class="col-sm-6">
                <div class="subscribe__icon-wrap"> <i class="icon_bg stroke icon-Imbox"></i><i class="icon stroke icon-Imbox"></i> </div>
                <div class="subscribe__inner">
                  <h2 class="subscribe__title">STAY UPDATED WITH LEARNEZY</h2>
                  <div class="subscribe__description">Nulla feugiat nibh placerat fermentum rutrum ante risus euismod</div>
                </div>
              </div>
              <!-- end col -->
              <div class="col-sm-6">
                <form class="subscribe__form" action="get">
                  <input class="subscribe__input form-control" type="text" placeholder="Your Email address ...">
                  <button class="subscribe__btn btn btn-success btn-effect">SUBSCRIBE</button>
                </form>
              </div>
              <!-- end col --> 
            </div>
            <!-- end row --> 
          </div>
          <!-- end container --> 
        </div>
      </div>
      <!-- end section-subscribe -->
    
    
     <div class="container">
        <div class="row">
          <div class="border-decor_top">
            <div class="col-md-6">
              <section class="section-default wow bounceInLeft" data-wow-duration="2s">
                <h2 class="ui-title-block">What <strong>Students Say</strong></h2>
                <div class="slider-reviews owl-carousel owl-theme owl-theme_mod-c enable-owl-carousel"
												data-single-item="true"
												data-auto-play="7000"
												data-navigation="true"
												data-pagination="false"
												data-transition-style="fade"
												data-main-text-animation="true"
												data-after-init-delay="4000"
												data-after-move-delay="2000"
												data-stop-on-hover="true">
                  <div class="reviews">
                    <div class="reviews__text">Nulla feugiat nibh placerat fermentum rutrum ante risus euismod eros  pharetra felis justo ac tortor. Maecenas odio aco sit amet odio euismo ac donec tellus. Nullam risus turpis rhoncus vel varius consequat laort  neque. Sed ipseget lectus vitae augue zitae ipsumd do eiusmod tempor incididunt ut labore et dolore magaliqua. Ut enim ad minim veniam quis nostrud exercitation ullamco laboris aliqup.</div>
                    <div class="reviews__img"><img class="img-responsive" src="assets/media/avatar_review/1.jpg" height="60" width="60" alt="foto"></div>
                    <span class="reviews__autor">-- JOHN SMITH</span> <span class="reviews__categories">(Maths Student)</span> </div>
                  <!-- end reviews -->
                  
                  <div class="reviews">
                    <div class="reviews__text">Nulla feugiat nibh placerat fermentum rutrum ante risus euismod eros  pharetra felis justo ac tortor. Maecenas odio aco sit amet odio euismo ac donec tellus. Nullam risus turpis rhoncus vel varius consequat laort  neque. Sed ipseget lectus vitae augue zitae ipsumd do eiusmod tempor incididunt ut labore et dolore magaliqua. Ut enim ad minim veniam quis nostrud exercitation ullamco laboris aliqup.</div>
                    <div class="reviews__img"><img class="img-responsive" src="assets/media/avatar_review/2.jpg" height="60" width="60" alt="foto"></div>
                    <span class="reviews__autor">-- JOHN SMITH</span> <span class="reviews__categories">(Maths Student)</span> </div>
                  <!-- end reviews -->
                  
                  <div class="reviews">
                    <div class="reviews__text">Nulla feugiat nibh placerat fermentum rutrum ante risus euismod eros  pharetra felis justo ac tortor. Maecenas odio aco sit amet odio euismo ac donec tellus. Nullam risus turpis rhoncus vel varius consequat laort  neque. Sed ipseget lectus vitae augue zitae ipsumd do eiusmod tempor incididunt ut labore et dolore magaliqua. Ut enim ad minim veniam quis nostrud exercitation ullamco laboris aliqup.</div>
                    <div class="reviews__img"><img class="img-responsive" src="assets/media/avatar_review/3.jpg" height="60" width="60" alt="foto"></div>
                    <span class="reviews__autor">-- JOHN SMITH</span> <span class="reviews__categories">(Maths Student)</span> </div>
                  <!-- end reviews --> 
                </div>
                <!-- end slider-reviews --> 
              </section>
              <!-- end section-default --> 
            </div>
            <!-- end col -->
            
            <div class="col-md-6">
              <section class="section-default wow bounceInRight courses" data-wow-duration="2s">
                <h2 class="ui-title-block">Why <strong>Learnezy ?</strong></h2>
                <div class="panel-group accordion accordion" id="accordion-1">
                  <div class="panel panel-default">
                    <div class="panel-heading"> <a class="btn-collapse" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-1"><i class="icon"></i></a>
                      <h3 class="panel-title">Learn And Get Training From Experts</h3>
                    </div>
                    <div id="collapse-1" class="panel-collapse collapse in">
                      <div class="panel-body">
                        <p class="ui-text">Fusce eleifend. Donec sapien sed phase lusa . Pellentesque lacus.Vivamus lorem arcu semper duiac. Cras ornare arcu vamus nda leo. Etiam ind arc morbi justo mauris temd pus pharetra interdum at congue semper purus.</p>
                      </div>
                    </div>
                  </div>
                  <!-- end panel -->
                  
                  <div class="panel">
                    <div class="panel-heading"> <a class="btn-collapse collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-2"><i class="icon"></i></a>
                      <h3 class="panel-title">Enjoy Our Free Online Courses</h3>
                    </div>
                    <div id="collapse-2" class="panel-collapse collapse">
                      <div class="panel-body">
                        <p class="ui-text">Fusce eleifend. Donec sapien sed phase lusa . Pellentesque lacus.Vivamus lorem arcu semper duiac. Cras ornare arcu vamus nda leo. Etiam ind arc morbi justo mauris temd pus pharetra interdum at congue semper purus.</p>
                      </div>
                    </div>
                  </div>
                  <!-- end panel -->
                  
                  <div class="panel">
                    <div class="panel-heading"> <a class="btn-collapse collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-3"><i class="icon"></i></a>
                      <h3 class="panel-title">Learn Anytime & Anywhere</h3>
                    </div>
                    <div id="collapse-3" class="panel-collapse collapse">
                      <div class="panel-body">
                        <p class="ui-text">Fusce eleifend. Donec sapien sed phase lusa . Pellentesque lacus.Vivamus lorem arcu semper duiac. Cras ornare arcu vamus nda leo. Etiam ind arc morbi justo mauris temd pus pharetra interdum at congue semper purus.</p>
                      </div>
                    </div>
                  </div>
                  <!-- end panel -->
                  
                  <div class="panel">
                    <div class="panel-heading"> <a class="btn-collapse collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-4"><i class="icon"></i></a>
                      <h3 class="panel-title">Basic to Advance: We Teach Everything</h3>
                    </div>
                    <div id="collapse-4" class="panel-collapse collapse">
                      <div class="panel-body">
                        <p class="ui-text">Fusce eleifend. Donec sapien sed phase lusa . Pellentesque lacus.Vivamus lorem arcu semper duiac. Cras ornare arcu vamus nda leo. Etiam ind arc morbi justo mauris temd pus pharetra interdum at congue semper purus.</p>
                      </div>
                    </div>
                  </div>
                  <!-- end panel --> 
                </div>
                <!-- end accordion --> 
              </section>
            </div>
            <!-- end col --> 
          </div>
          <!-- end section-default --> 
        </div>
        <!-- end row --> 
      </div>
      <!-- end container -->
    
     <div class="tweets" style="background:#6BC868;">
            <div class="container">
            	<div class="tweet-contant">
                	<i class="fa fa-twitter"></i>
                    <h4>Weekly Updates</h4>
                    <ul class="bxslider">
                        <li>
                            <p>Are you a morning person or is the night time the right time? Interesting perspectives on the forum - <a href="#">http://t.co/tdEHlbZf</a></p>
                        </li>
                        <li>
                            <p>Dolor donec sagittis sapien. Ante aptent feugiat adipisicing. Duis int. - <a href="#">http://t.co/tdEHlbZf</a></p>
                        </li>
                        <li>
                            <p>Duis interdum olor donec sagittis sapien. Ante aptent feugiat adipisicing - <a href="#">http://t.co/tdEHlbZf</a></p>
                        </li>
                        
                    </ul>
                </div>
            </div>
        </div>
    
    
    
      
    </div>
    <!-- end main-content -->
    
   <jsp:include page="Commonfooter.jsp"></jsp:include>
  </div>
  <!-- end wrapper --> 
</div>
<!-- end layout-theme --> 


<script type="text/javascript">
window.onload = function(){
    document.getElementById('display').focus();
}
</script>

</body>
</html>
