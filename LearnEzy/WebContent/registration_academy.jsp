<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@page import="java.util.Date"%>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ page import="java.sql.*" %>
<%@page import="com.itech.elearning.forms.LoginForm"%>
<html>
<head>
<title>Itech Training :: Register</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<!-- Google Web Fonts
		================================================== -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:300,300italic,400,500,700%7cCourgette%7cRaleway:400,700,500%7cCourgette%7cLato:700' rel='stylesheet' type='text/css' />

		<!-- Basic Page Needs ================================================== -->
	
		<!-- CSS ================================================== -->

<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300' rel='stylesheet' type='text/css' />
<link href="css/bootstrap.css" rel
="stylesheet" type="text/css" media="all" />
<link href="css/cmsstyle.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>
<link rel="stylesheet" href="css/tmm_form_wizard_style_demo.css" />
<link rel="stylesheet" href="css/grid.css" />
<link rel="stylesheet" href="css/tmm_form_wizard_layout.css" />
<link rel="stylesheet" href="css/fontello.css" />
<link rel="stylesheet" type="text/css" href="date/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="src/DateTimePicker.css" />

	
<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="src/DateTimePicker.js"></script>
<script type="text/javascript" src="date/jquery-ui.js"></script>
<!-- Load jQuery and the validate plugin and masterdata js file (Make class name should calendar) -->
<link rel="stylesheet" type="text/css"	href="DateTime/jquery-calendar.css" />
<script type="text/javascript" src="DateTime/jquery-calendar.js"></script>
<script type="text/javascript" src="DateTime/calendar.js"></script>
<script type="text/javascript" src="js/jQuery.print.js"></script>


	
<!-- Load jQuery and the validate plugin and masterdata js file -->
  <script type="text/javascript" src="<%=request.getContextPath()%>/js/validation/jquery.validate.min.js"></script>
  <script type="text/javascript" src="<%=request.getContextPath()%>/js/validation/additional-methods.js"></script>
  <script type="text/javascript" src="<%=request.getContextPath()%>/js/validation/masterdata.validation.js"></script>

<!-- Dependencies -->
		 
		<script src="js/jquery.ui.draggable.js" type="text/javascript"></script>
		<!-- Core files -->
		<script src="js/jquery.alerts.js" type="text/javascript"></script>
		<link href="css/jquery.alerts.css" rel="stylesheet" type="text/css" media="screen" />
 <style>
 .row {
 	margin-top: 10px;
 }
 
 .row label{
 	margin-bottom: 5px;
 }
 .login a{  width: 67px;
  float: right;
  color: #000;
  margin-top: 22px;
  border: 1px solid #FFA500;
  padding: 6px;
  border-radius: 8px;
 }
  .courseDispTr{  
  background-color: #59bbdf;
 }
 .courseSelect{
	width:200px; 
	padding:2px;
	hight:50px;
 }
  .courseListTable{
 border: 1px;
 border-width: 12px;
 }
 .courseListTable td{
 height: 30px;
 vertical-align: middle;
 }

.sign {
border:1px solid #D0DDE1;
padding:14px;
border-radius:2px;
}
legend{width:20%;}

label{font-size:14px;}

.fa-lg{color:green;}

#captchaText{
background-image: url("..img/BG1.png);
}
input, select, textarea{
    color:#000 !important;
}
</style>
 
<script type="text/javascript" src="js/student/studentRegistration.js"></script>

	</head>
	<body>
<%
String urlre = request.getRequestURL().toString();
urlre = "Itech";
String compid = null;
Connection con = null;
String url = "jdbc:mysql://localhost:3306/";
String db = "elearningcom";
String driver = "com.mysql.jdbc.Driver";
String userName ="root";
String password="Itech123!";

int sumcount=0;
Statement st;
try{
Class.forName(driver).newInstance();
con = DriverManager.getConnection(url+db,userName,password);
String query = "select * from companymaster where CompanyName like  +'"+urlre+"' ";
System.out.println(query);
st = con.createStatement();
ResultSet rs = st.executeQuery(query);
%>
<%
while(rs.next()){
%>
<tr><td><%= compid =  rs.getString(1)%></td>
<% System.out.println(compid); %>
</tr>
<%
}
%>
<%
}
catch(Exception e){
e.printStackTrace();
}
%>

		<!-- - - - - - - - - - - - - Content - - - - - - - - - - - - -  -->
   <div class="header">	
   	 	     
             <div class="header-logo-nav">
             	  <div class="navbar navbar-inverse navbar-static-top nav-bg" role="navigation">
				      <div class="container">
				        <div class="navbar-header">
				           
				         <div class="logo"> <a class="navbar-brand" href="http://www.itechtraining.in/"><img src="images/Itech Solutions logo.png" alt="" /></a></div>
				        
				        </div>
				        <div class="login">
                    
                   <a  href="login.jsp"> LOGIN</a>
                    
                    </div><!--login-->
				      </div>
				    </div>
		         <div class="clear"></div>
                  <div class="header-bottom">
                 <div class="container">
                 <div class="time">
                  <p><div id="clockbox"></div></p>
                 <script src="js/dynamicClock.js" type="text/javascript"></script>
                 </div>
               
                  
                 </div>
                 </div>
                 <div class="clear"></div>
	        </div>
	        <div class="header-banner">
	        	
			    
	           </div>
	          
             </div>
   <!-- End Header -->

		<div id="content">

			<div class="form-container">

				<div id="tmm-form-wizard" class="container substrate">

					<div class="row" align="center">
						<div class="col-xs-12">
							<h2 class="form-login-heading"><span>Registration Form For Academy</span></h2>
						</div>

					</div><!--/ .row-->

					<div class="row stage-container" align="center">

						<div class="stage tmm-current col-md-3 col-sm-3" id="topDiv1">
							<div class="stage-header head-icon head-icon-lock"></div>
							<div class="stage-content">
								<h3 class="stage-title">Registration</h3>
								<div class="stage-info">
									Registration details
								</div> 
							</div>
						</div><!--/ .stage-->

						<div class="stage col-md-3 col-sm-3 " id="topDiv2">
							<div class="stage-header head-icon head-icon-user"></div>

							<div class="stage-content">
								<h3 class="stage-title">Courses</h3>
								<div class="stage-info">
									Select the course
								</div>
							</div>
						</div><!--/ .stage-->

						<div class="stage col-md-3 col-sm-3" id="topDiv3">
							<div class="stage-header head-icon head-icon-payment"></div>
							<div class="stage-content">
								<h3 class="stage-title">Payment Information</h3>
								<div class="stage-info">
									Make the payment
								</div>
							</div>
						</div><!--/ .stage-->

						 <div class="stage col-md-3 col-sm-3" id="topDiv4">
							<div class="stage-header head-icon head-icon-details"></div>
							<div class="stage-content">
								<h3 class="stage-title">Login</h3>
								<div class="stage-info">
									Login details
								</div> 
							</div>
						</div><!--/ .stage-->

					</div><!--/ .row-->
<html:form action="studentRegistration.do?action=userRegistration" method="post" styleClass="register" styleId="register"   enctype="multipart/form-data">
						
					<div class="row">

						<div class="col-xs-12" >

							<div class="form-header">
								<div class="form-title form-icon title-icon-lock">
									<b>Registration details</b> 
								</div>
								<div   class="form-title" style="width: 725px;text-align: center;" >
									<b style="color: red;"><logic:notEmpty name="status"><bean:write name="status"/> </logic:notEmpty></b>
								</div>
								<div class="steps">
									Steps 1 - 4
								</div>
							</div><!--/ .form-header-->

						</div>

					</div><!--/ .row-->
			 
					
						


						<div class="form-wizard" >
							
							<div class="row">

								<div class="col-md-8 col-sm-7">

									

									<div class="row">
										
										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="first-name">Academy Name <font style="color: red;">*</font></label>
												<input type="text"  placeholder="Academy Name" />
												
											</fieldset><!--/ .input-first-name-->
										</div>
										
										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="last-name">Mobile No</label>
												<input type="text"   placeholder="Mobile No"  class="form-icon form-icon-phone" />
												
											</fieldset><!--/ .input-first-name-->
										</div>
										
									</div><!--/ .row-->
									
									<div class="row">
										
										
										
										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="last-name">Alternative Contact No</label>
												<input type="text"   placeholder="Alternative Contact No"  class="form-icon form-icon-phone" />
												
											</fieldset><!--/ .input-first-name-->
										</div>
										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="email">Email <font style="color: red;">*</font></label>
												<input type="text" name="emailId"  class="form-icon form-icon-mail" placeholder="Please enter your email ID"  />
												
											</fieldset><!--/ .input-email-->
										</div>
									</div><!--/ .row-->
									
									<div class="row">

										<div class="col-md-12 col-sm-12">
											<fieldset class="input-block">
												<label for="address">Address</label>
												<input type="text"  name="address" placeholder="Address"  />
												
											</fieldset><!--/ .input-phone-->
										</div>
										
									</div><!--/ .row-->

										<div class="row">

										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="address">Country <font style="color: red;">*</font></label>
												<div class="dropdown">
												<select  class="dropdown-select">
													<option>--Select Country--</option>
													
													<option>Others</option>
												</select>
												</div>
											</fieldset><!--/ .input-phone-->
										</div>
										
									

										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="address">State <font style="color: red;">*</font></label>
												<div class="dropdown">
												<select  class="dropdown-select" >
													<option value="-1">-Select State-</option>
													<option value="0">Others</option>
												</select>
												</div>
											</fieldset><!--/ .input-phone-->
										</div>
										
									</div>
									
									
									 
									<div class="row">
										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="address">City <font style="color: red;">*</font></label>
												<div class="dropdown">
												<select  class="dropdown-select">
													<option>-Select City-</option>
													<option>Others</option>
												</select>
												</div>
											</fieldset><!--/ .input-phone-->
										</div>
										
										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="address">Pincode <font style="color: red;">*</font></label>
												
												<input type="text"  name="pincode" placeholder="Pincode" />
												
											</fieldset><!--/ .input-phone-->
										</div>
										
									</div><!--/ .row-->
									 
									
									<div class="row" style="height: 100px;"> 
										<div class="col-md-6 col-sm-6">
											<div class="input-block">
												
												 
													 <label>Academy Logo:</label>
								<input name="imageFile" id="imageFile" type="file" placeholder="upload file Photo"  />
												 
												
											</div><!--/ .input-country-->
											
										
										</div>
										 	
									</div><!--/ .row-->
									
									
									
									 
								

						 
								<div class="col-md-10 col-sm-7" style="margin-top: 15px;">

									<fieldset class="sign">
									<legend >Sign in Details</legend>
                                    	<div class="row">
										<div class="col-md-12 col-sm-12" >
											<fieldset class="input-block">
												<label for="card-number">User Name <font style="color: red;">*</font></label><br />
												<input type="text"  name="uname"  id="uname" placeholder="User Name"  value="" size="20" style="float: left;width: 42%" /> 
												 <div id="uNameDiv"  ></div>
											</fieldset>
										</div>
										</div>
										<div class="row">
										<div class="col-md-6 col-sm-6" >
											<fieldset class="input-block">
												<label for="card-number">Password <font style="color: red;">*</font> <span style="font-size: 10px;font-style: italic;color: red;margin-left: 192px;" title="Password should contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character and min 8 characters" >Rules</span></label>
												<input type="password" name="password"  id="password" class="passWord" value="" onchange="checkPasswordStrong(this);" placeholder="Password" style="float: left;width: 90%"   />	 
           										<div id="passDiv" ></div>
											</fieldset>
										</div>
										
										
										<div class="col-md-6 col-sm-6" >
											<fieldset class="input-block">
												<label for="card-number">Confirm Password <font style="color: red;">*</font> </label>
												<input name="retypepassword" id="retypepassword" type="password" onchange="checkpassword();"   placeholder="Confirm Password"  style="float: left;width: 90%" />	 
         										<div id="confDiv" ></div>
											</fieldset>
										</div>
										
										<div class="col-md-12 col-sm-12" id="passRules" style="height: 30px;font-size: 12px;font-style: italic;color: red;" >
											 
										</div>
									 
										<div class="col-md-6 col-sm-6" style="margin-top: 5px; " >
										<fieldset class="input-block">
											 <input id="captchaText" type="text" readonly="readonly"  value="" style="float: left;width: 45%;background-image: url(img/BG1.png);color: black;border: none;text-align: center;" />
											<input id="captchabox" type="text" style="float:left;width: 45%"  />	
											<div id="captchaDiv" ></div>	
											</fieldset>
										</div>
										
										
										</div>		
									</fieldset><!--/ .input-email-->

								</div>

							</div><!--/ .row-->
							
						</div><!--/ .form-wizard-->
						
						

						
						
						
						
						</div>
						<div class="next1" style="float:right; ">
							<button class="button button-control" type="submit" id="validateRegister"><span>Next Step</span></button>
							<div class="button-divider"></div>
						</div>
						</html:form>
				 
							
							

						</div>

					</div><!--/ .row-->
					 
					</div>
						 
					<!--/ .form-wizard-->
			      <jsp:include page="jsp/common/student_footer.jsp" /> 
			 


		<!-- - - - - - - - - - - - end Content - - - - - - - - - - - - - -->
 <!-- Start Footer -->
  <!-- End Footer -->
 
 
<script type="text/javascript">
$(document).ready( function() {
	$("#firstName").focus();
	$("#countryid").val("-1");
	var num = Math.floor(Math.random() * 9000) + 1000;
	//alert(num);
	$("#captchaText").val(num);
	
	 
$("#countryid").change( function() {
	var countryid=$(this).val();
	if(countryid=="0"){
		$("#otherCountryAdd").show();
	}else{
		$("#otherCountryAdd").hide();
		$("#otherCountry").val("");
	$.ajax({	
		type: "GET",		
		url:"State.do?action=ajaxActiveList",
		data:{countryId:countryid},
		success: function(r){	
			var json= JSON.parse(r);
			console.log(json);			
			$("#stateid").empty();
			var firstOption= $("<option>",{text:"--Select State--",value:"-1"});
			var otherOption= $("<option>",{text:"Others",value:"0"});
			$("#stateid").append(firstOption);
			$.each(json,function(i,obj){
				var option= $("<option>",{text:obj.state, value:obj.stateId});
				$("#stateid").append(option);
			});
			$("#stateid").append(otherOption);
		} 
		
	});
	}
});

$("#stateid").change( function() {
	var stateid=$(this).val();
	if(stateid=="0"){
	$("#otherStateAdd").show();
	}else{
	$("#otherStateAdd").hide();
	$("#otherState").val("");
	$.ajax({	
		type: "GET",		
		url:"City.do?action=ajaxActiveList",
		data:{stateId:stateid},
		success: function(r){	
			var json= JSON.parse(r);
			console.log(json);			
			$("#cityid").empty();
			var firstOption= $("<option>",{text:"--Select City--",value:"-1"});
			var otherOption= $("<option>",{text:"Others",value:"0"});
			$("#cityid").append(firstOption);
			$.each(json,function(i,obj){
				var option= $("<option>",{text:obj.city, value:obj.cityId});
				$("#cityid").append(option);
			});
			$("#cityid").append(otherOption);
		} 
		
	});
	}
});


 

$("#cityid").change( function() {
	var cityid=$(this).val();
	if(cityid=="0"){
		$("#otherCityAdd").show();
	}else{
		$("#otherCityAdd").hide();
		$("#otherCity").val("");
	}
});
});

function pincodOnlyNumbers() {
	//alert("arg");
	re = /\D/g; // remove any characters that are not numbers
	socnum = document.forms[0].pincode.value.replace(re, "");
	document.forms[0].pincode.value = socnum;
}

</script>
 
	<script src="js/commonFunction.js"  type="text/javascript"></script>	
	<script type="text/javascript">
function readURL(input) {
	if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$(document).ready(function() {
$("#imageFile").change(function(){
    readURL(this);
});
$("#otherStateAdd").hide();
$("#otherCityAdd").hide();
$("#otherCountryAdd").hide();
});
</script>
	
	</body>


</html>