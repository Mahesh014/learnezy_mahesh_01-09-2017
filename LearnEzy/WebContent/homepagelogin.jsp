
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ page import="java.sql.*" %>
<%@page import="com.itech.elearning.forms.LoginForm"%>
<html>
<head>
<title>Itech Training :: Login</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<!-- Google Web Fonts
		================================================== -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:300,300italic,400,500,700%7cCourgette%7cRaleway:400,700,500%7cCourgette%7cLato:700' rel='stylesheet' type='text/css' />

		<!-- Basic Page Needs
		================================================== -->
	
		<!-- CSS
		================================================== -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300' rel='stylesheet' type='text/css' />
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/cmsstyle.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>
<link rel="stylesheet" href="css/tmm_form_wizard_style_demo.css" />
<link rel="stylesheet" href="css/grid.css" />
<link rel="stylesheet" href="css/tmm_form_wizard_layout.css" />
<link rel="stylesheet" href="css/fontello.css" />
<link rel="stylesheet" type="text/css" href="date/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="src/DateTimePicker.css" />

<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="src/DateTimePicker.js"></script>
<script type="text/javascript" src="date/jquery-ui.js"></script>
		<!-- Dependencies -->
		<script src="js/jquery.js" type="text/javascript"></script>
		<script src="js/jquery.ui.draggable.js" type="text/javascript"></script>
		<!-- Core files -->
		<script src="js/jquery.alerts.js" type="text/javascript"></script>
		<link href="css/jquery.alerts.css" rel="stylesheet" type="text/css" media="screen" />
<style>
a:hover{color:#000;}
</style>
 <script type="text/javascript">

 window.history.forward();
 function noBack() { window.history.forward(); }
 
	$(document).ready( function() {
	$("#validateLogin").click( function() {
		if($("#userNameLogin").val()=="" && $("#passwordLogin").val()==""){
			alert('Username and Password cannot be empty');
			$("#userNameLogin").focus();
			return false;
		}else
		
		if($("#userNameLogin").val()==""){
			alert('User Name cannot be empty');
			$("#userNameLogin").focus();
			return false;
		}else
		if($("#passwordLogin").val()==""){
			alert('Password cannot be empty');
			$("#passwordLogin").focus();
			return false;
		}    
	});
	});
</script>
 <style>
 .row {
 	margin-top: 10px;
 }
 
 .row label{
 	
 	margin-bottom: 5px;
 }
 #tmm-form-wizard a:hover{color:#000;}
 
 input {
	color: black;
}
 </style>

	</head>
	<body>


		<!-- - - - - - - - - - - - - Content - - - - - - - - - - - - -  -->
   <div class="header">	
   	 	     
             <div class="header-logo-nav">
             	  <div class="navbar navbar-inverse navbar-static-top nav-bg" role="navigation">
				      <div class="container">
				        <div class="navbar-header">
				           
				         <div class="logo"> <a class="navbar-brand" href="http://www.itechtraining.in/"><img src="images/<%= img %>" alt="" /></a></div>
				          <div class="clear"></div>
				        </div>
				         
				      </div>
				    </div>
		         <div class="clear"></div>
                  <div class="header-bottom">
                 <div class="container">
                 <div class="time">
                 <p><div id="clockbox"></div></p>
                 <script src="js/dynamicClock.js" type="text/javascript"></script>
                 </div>
               
                  
                 </div>
                 </div>
                 <div class="clear"></div>
	        </div>
	        <div class="header-banner">
	        	
			    
	           </div>
	          
             </div>
   <!-- End Header -->

		<div id="content">

			<div class="form-container" >

				<div id="tmm-form-wizard" class="container substrate" style="width: 400px;">

					 

					 
					<form  action="Login.do?action=login" class="login active" method="post" id="loginform" onsubmit="return validate();" >
						<div class="row">

						<div class="col-xs-12">

							<div class="form-header">
								<div class="form-title form-icon title-icon-lock">
									<b>Login</b>
								</div>
								
								 
							</div><!--/ .form-header-->
							
											<div class="form-wizard">
							<div class="row" style="color: red;text-align: center;">
							<div class="col-md-12 col-sm-12">
											<fieldset class="input-block">
							<logic:notEmpty name="status"><bean:write name="status"/> </logic:notEmpty>
							</fieldset><!--/ .input-first-name-->
										</div>
							</div>
							<div class="row">
										<div class="col-md-12 col-sm-12">
											<fieldset class="input-block">
												<label for="last-name">User Name</label>
												<input type="text" name="userName"  id="userNameLogin" placeholder="User Name"  autofocus />
												
											</fieldset><!--/ .input-first-name-->
										</div>
										 
										
									</div><!--/ .row-->
							
							
							<div class="row">
										 
										<div class="col-md-12 col-sm-12">
											<fieldset class="input-block">
												<label for="input-block">Password</label>
												<input name="password" id="passwordLogin" type="password" placeholder="Password"  />
												
											</fieldset><!--/ .input-email-->
										</div>
										
									</div><!--/ .row-->
						</div><!--/ .form-wizard-->
							
							
							
 
						
						<div class="prev1" style="width:370px;">
						
						<p><a href="forgotpassword.jsp">Forgot Password?</a></p>
								<p style="margin-left:20px;"><button class="button button-control" type="submit"  id="validateLogin">LogIn</button></p>
						<p style="margin-left:20px;">	<a href="studentRegistration.do?action=list&amp;compid=<%=compid %>">
						                                 
						<button class="button button-control" type="button">Sign Up As Student </span></button></a></p>
						<p style="margin-left:20px;">	<a href="studentRegistration.do?action=listaca&amp;compid=<%=compid %> "><button class="button button-control" type="button">Sign Up As Academy</span></button></a></p>
						<p style="margin-left:20px;">	<a href="studentRegistration.do?action=listfree&amp;compid=<%=compid %>"><button class="button button-control" type="button">Sign Up As Freelancer</span></button></a></p>	<div class="button-divider"></div>
						</div>
									</div><!--/ .row-->
					 </form>
					</div>
						
					<!--/ .form-wizard-->
				</div><!--/ .container-->
				
		 

		</div><!--/ #content-->


		<!-- - - - - - - - - - - - end Content - - - - - - - - - - - - - -->
<!-- Start Footer -->
     <jsp:include page="jsp/common/student_footer.jsp" /> 
  <!-- End Footer -->		
	</body>
<!-- Mirrored from 0.s3.envato.com/files/94682367/form-wizard-with-icon.html by HTTrack Website Copier/3.x [XR&CO'2013], Tue, 09 Jun 2015 12:23:40 GMT -->
</html>