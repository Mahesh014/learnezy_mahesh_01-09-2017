<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="robots" content="INDEX, FOLLOW">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui">
		<meta name="description" content="Terms And Conditions At Learnezy">
		<meta name="googlebot" content="noodp">
		<title>Terms and Conditions | LearnEZY</title>
		<link rel="canonical" href="www.learnezy.com/terms-and-conditions.jsp"/>
		<link href="favicon.png" type="image/x-icon" rel="shortcut icon">
		<link href="assets/css/master.css" rel="stylesheet">

<script src="assets/plugins/jquery/jquery-1.11.3.min.js"></script>
</head>

<body>
<h1>Terms And Conditions</h1>
<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->

		<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200">



			<div id="wrapper">
<jsp:include page="Commonheader.jsp"></jsp:include>


				<div class="wrap-title-page">
					<div class="container">
						<div class="row">
							<div class="col-xs-12"><h1 class="ui-title-page">Terms &amp; Conditions</h1></div>
						</div>
					</div><!-- end container-->
				</div><!-- end wrap-title-page -->


				<div class="section-breadcrumb">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div class="wrap-breadcrumb clearfix">
									<ol class="breadcrumb">
										<li><a href="index.jsp"><i class="icon stroke icon-House"></i></a></li>
										
										<li class="active">Terms &amp; Conditions</li>
									</ol>
								</div>
							</div>
						</div><!-- end row -->
					</div><!-- end container -->
				</div><!-- end section-breadcrumb -->


				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<main class="main-content">

								<article class="post post_mod-j clearfix">
									
									<div class="post-inner decor decor_mod-a clearfix">
										
										<div class="entry-main">
											<h3 class="entry-title ui-title-inner">Learnezy - Terms &amp; Conditions</h3>
											<div class="entry-content">
											
<ul class="about-desc">

<li> The use of words, such as 'Us', or 'We' refers to the organization as a whole. It refers to Itech Training.</li>

<li> 'You' is referred to the end user or the interested, willingly using/ accessing our website.</li>
<li>Once student registered on this website and select course, directly he/she can go on payment page. From payment gateway, the user can make payment and get receipt online. Candidate can login anytime and take study materials.
By accessing this web site, you are agreeing to be bound by these web site Terms and Conditions of Use, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws.</li>

</ul>


<h2 class="left">The usage of the website is subject to the following terms and conditions</h2>

<ul><li> The information transmitted and broadcasted in the form of content on the website is strictly for your general use and in no way can be used as your own or copied.</li>

<li>The information provided is as per the prevailing conditions and laws and regulations and is likely to alter or change as per the change in legal policies.</li>

<li> The website keeps a track of the cookies to keep an eye on the browsing preferences and needs of all users. In case you do not permit the usage of the cookies then the information can be used for third party data usage namely :

<ul><li style="font-weight:bold;"> Name</li>

<li style="font-weight:bold;"> Designation / Job Title</li>

<li style="font-weight:bold;"> Contact</li>

<li style="font-weight:bold;"> Demographic Detail</li>
</ul>
</li>
</ul>
<p>Itech Training or any third party is not liable for any details mentioned. The details provided are true, correct, precise, updated, in accordance to the legal terms and conditions, as per the best of our knowledge. In case of any change in information we are not liable for the changes made. The team at Itech Training timely updates the information on the website and ensures awareness amongst all clients alike. We refrain from imparting any misleading or outdated information to our users.</p>

<p>Usage and understanding of any information or material on the website is absolutely at your own perception and understanding and Itech Training will not be held responsible for the variation in understanding of the subject matter.Itech Training at no point will be held responsible for the products being endorsed. The user agrees to take complete responsibility of his actions and decisions.</p>



<p>There is a refund policy that the organization agrees to abide by. The organization agrees to refund the entire amount or a partial amount of the fees.</p>

<p>This condition comes into existence only after the Board of Directors at any point of time find that the information imparted to the applicant was misleading or incorrect which led to the refusal or rejection of the case. It is then, that the organization becomes liable for the outcome of the result. It is also important to note that the organization will not be legally bound to pay you any amount over and above the amount payable to us. We render the services and charge the applicants for the consultancy provided. We do not guarantee the outcome. Itech Training charges you a specified sum for the services being rendered and is payable only after the completion of an assessment and consultation.</p>

<p>The material on the website is protected under Copyrights Law of India and worldwide according to the International Conventions and Treaties, and in no manner can an individual use it for personal use. All the trademarks are replicated, (which may or may not be the property of the body operating this) and publically accepted on the website. Any misuse of the website and the content will compel us to take legal action against the user. The website can also carry links to additional information regarding the subject matter, this in no way means that we, endorse the website nor can we be held responsible for the information provided on that website.</p>

<p>Usage of the website for summarizing does not by any means symbolize an association between the advisor-client or consultant-client association and the guest/reader/browser. An advisor client will be liable for any commitment only after the client and the applicant have entered into a mutual contract/ agreement; same would be after payment of consideration sums to the consultant as due. All the terms and conditions of such an agreed contract for employing the services do not comply with the terms and conditions of the website. Since the deal of engagement could vary from client to client, hence it remains the sole responsibility of the user to ask for a copy of the bond or contract or agreement, which will be relevant to his case. An individual in possession of such a written contract can in no way be used for redistribution purposes. An applicant is strictly prohibited for any such related action. Any deviation from the specified rules can lead to an immediate action against the offender.</p>

<p>A registered user can sure use the content on the website for his reference through the following means:</p>
<ul>
<li> A user can use the content for his personal use, but is refrained from doing so for monetary gains.</li>

<li> A user can copy the content for individual third parties for their reference, but should not publicly declare credit to the website.</li>

<li> The terms and conditions strictly refrains users, at any point of time to distribute, or utilize the given material, for commercial purposes. In no manner can an individual can copy the content and pass it on or store the same with another website or electronic recovery organism.</li>
</ul>

<p>Using the website automatically obliges you to act in accordance with the terms and conditions outlined by the organization.
</p>

<h2 class="left">Placement </h2>
<p>Itech solutions is a one of the leading software companies of Bangalore. If any job opening is there, we will give you the opportunity to attend interviews calls.
Please note : We will not offered guaranteed job</p>
 <h2 class="left">Comments </h2>
<p>Should you choose to register with the Site, you are required to provide accurate, current and complete information about yourself and one Identification proof.</p>
<p style="font-weight:bold;">To get detailed information, please <a style="color:#F28858;" href="contact.jsp"/> CONTACT </a>    Itech Solutions.</p>
											</div>
										</div>
										
									</div>
								</article><!-- end post -->

								
								
							</main><!-- end main-content -->

						</div><!-- end col -->


						
					</div><!-- end row -->
				</div><!-- end container -->

<jsp:include page="Commonfooter.jsp"></jsp:include>

			</div><!-- end wrapper -->
		</div><!-- end layout-theme -->

<!-- SCRIPTS --> 
<script src="assets/js/jquery-migrate-1.2.1.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<script src="assets/js/modernizr.custom.js"></script> 
<script src="assets/js/waypoints.min.js"></script> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> 

<!--THEME--> 
<script  src="assets/plugins/sliderpro/js/jquery.sliderPro.min.js"></script> 
<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script> 
<script src="assets/plugins/isotope/jquery.isotope.min.js"></script> 
<script src="assets/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script> 
<script src="assets/plugins/datetimepicker/jquery.datetimepicker.js"></script> 
<script src="assets/plugins/jelect/jquery.jelect.js"></script> 
<script src="assets/plugins/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js"></script> 
<script src="assets/js/cssua.min.js"></script> 
<script src="assets/js/wow.min.js"></script> 
<script src="assets/js/custom.min.js"></script> 


</body>
</html>
