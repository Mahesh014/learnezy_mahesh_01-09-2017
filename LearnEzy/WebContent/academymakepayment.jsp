<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ page import="java.sql.*" %>
<%@page import="com.itech.elearning.forms.LoginForm"%>
<html>
<head>
<title>Itech Training :: Register</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<!-- Google Web Fonts
		================================================== -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:300,300italic,400,500,700%7cCourgette%7cRaleway:400,700,500%7cCourgette%7cLato:700' rel='stylesheet' type='text/css' />
		<!-- Basic Page Needs ================================================== -->
		<!-- CSS ================================================== -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300' rel='stylesheet' type='text/css' />
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/cmsstyle.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>
<link rel="stylesheet" href="css/tmm_form_wizard_style_demo.css" />
<link rel="stylesheet" href="css/grid.css" />
<link rel="stylesheet" href="css/tmm_form_wizard_layout.css" />
<link rel="stylesheet" href="css/fontello.css" />
<link rel="stylesheet" type="text/css" href="date/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="src/DateTimePicker.css" />

<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="src/DateTimePicker.js"></script>
<script type="text/javascript" src="date/jquery-ui.js"></script>
<!-- Load jQuery and the validate plugin and masterdata js file (Make class name should calendar) -->
<link rel="stylesheet" type="text/css"	href="DateTime/jquery-calendar.css" />
<script type="text/javascript" src="DateTime/jquery-calendar.js"></script>
<script type="text/javascript" src="DateTime/calendar.js"></script>
<script type="text/javascript" src="js/jQuery.print.js"></script>

<!-- Load jQuery and the validate plugin and masterdata js file -->
    
 
  <script type="text/javascript" src="<%=request.getContextPath()%>/js/validation/jquery.validate.min.js"></script>
  <script type="text/javascript" src="<%=request.getContextPath()%>/js/validation/additional-methods.js"></script>
  <script type="text/javascript" src="<%=request.getContextPath()%>/js/validation/masterdata.validation.js"></script>


 <style>
 .row {
 	margin-top: 10px;
 }
 
 .row label{
 	margin-bottom: 5px;
 }
 .login a{  width: 67px;
  float: right;
  color: #000;
  margin-top: 22px;
  border: 1px solid #FFA500;
  padding: 6px;
  border-radius: 8px;
 }
  .courseDispTr{  
  background-color: #59bbdf;
 }
 .courseSelect{
	width:200px; 
	padding:2px;
	hight:50px;
 }
  .courseListTable{
 border: 1px;
 border-width: 12px;
 }
 .courseListTable td{
 height: 30px;
 vertical-align: middle;
 }

.sign {
border:1px solid #D0DDE1;
padding:14px;
border-radius:2px;
}
legend{width:20%;}

label{font-size:14px;}

.fa-lg{color:green;}

#captchaText{
background-image: url("..img/BG1.png);
}
</style>
 
<script type="text/javascript" src="js/academy/acedemyRegistration.js"></script>

	</head>
	<body>

<%
String urlre = request.getRequestURL().toString();
urlre = "Itech";
String compid = null;
Connection con = null;
String url = "jdbc:mysql://localhost:3306/";
String db = "elearningcom";
String driver = "com.mysql.jdbc.Driver";
String userName ="root";
String password="root";

int sumcount=0;
Statement st;
try{
Class.forName(driver).newInstance();
con = DriverManager.getConnection(url+db,userName,password);
String query = "select * from companymaster where CompanyName = '"+urlre+"' ";
System.out.println(query);
st = con.createStatement();
ResultSet rs = st.executeQuery(query);
%>
<%
while(rs.next()){
%>
 <% compid =  rs.getString(1);%> 
<% System.out.println(compid); %>
 
<%
}
%>
<%
}
catch(Exception e){
e.printStackTrace();
}
%>


		<!-- - - - - - - - - - - - - Content - - - - - - - - - - - - -  -->
   <div class="header">	
   	 	     
             <div class="header-logo-nav">
             	  <div class="navbar navbar-inverse navbar-static-top nav-bg" role="navigation">
				      <div class="container">
				        <div class="navbar-header">
				           
				         <div class="logo"> <a class="navbar-brand" href="http://www.itechtraining.in/"><img src="images/Itech Solutions logo.png" alt="" /></a></div>
				        
				        </div>
				        <div class="login">
                    
                   <a  href="sign_in.jsp"> LOGIN</a>
                    
                    </div><!--login-->
				      </div>
				    </div>
		         <div class="clear"></div>
                  <div class="header-bottom">
                 <div class="container">
                 <div class="time">
                  <p><div id="clockbox"></div></p>
                 <script src="js/dynamicClock.js" type="text/javascript"></script>
                 </div>
               
                  
                 </div>
                 </div>
                 <div class="clear"></div>
	        </div>
	        <div class="header-banner">
	        	
			    
	           </div>
	          
             </div>
   <!-- End Header -->

		<div id="content">

			<div class="form-container">

				<div id="tmm-form-wizard" class="container substrate">

					<div class="row" align="center">
						<div class="col-xs-12">
							<h2 class="form-login-heading"><span>Registration Process</span></h2>
						</div>

					</div><!--/ .row-->

					<div class="row stage-container" align="center">

						<div class="stage tmm-current col-md-3 col-sm-3" id="topDiv1">
							<div class="stage-header head-icon head-icon-lock"></div>
							<div class="stage-content">
								<h3 class="stage-title">Registration</h3>
								<div class="stage-info">
									Registration details
								</div> 
							</div>
						</div><!--/ .stage-->

						<div class="stage col-md-3 col-sm-3 " id="topDiv2">
							<div class="stage-header head-icon head-icon-user"></div>

							<div class="stage-content">
								<h3 class="stage-title">License</h3>
								<div class="stage-info">
									Select the License
								</div>
							</div>
						</div><!--/ .stage-->

						<div class="stage col-md-3 col-sm-3" id="topDiv3">
							<div class="stage-header head-icon head-icon-payment"></div>
							<div class="stage-content">
								<h3 class="stage-title">Payment Information</h3>
								<div class="stage-info">
									Make the payment
								</div>
							</div>
						</div><!--/ .stage-->

						 <div class="stage col-md-3 col-sm-3" id="topDiv4">
							<div class="stage-header head-icon head-icon-details"></div>
							<div class="stage-content">
								<h3 class="stage-title">Login</h3>
								<div class="stage-info">
									Login details
								</div> 
							</div>
						</div><!--/ .stage-->

					</div><!--/ .row-->
<html:form action="studentRegistration.do?action=addacadet" method="post" styleClass="register" styleId="register"   enctype="multipart/form-data">
						
						<div class="row">

						<div class="col-xs-12">

							<div class="form-header">
								<div class="form-title form-icon title-icon-lock">
									<b>Select the course</b>
								</div>
								<div   class="form-title" style="width: 725px;text-align: center;" >
									<b style="color: red;"><logic:notEmpty name="status"><bean:write name="status"/> </logic:notEmpty></b>
								</div>
								<div class="steps">
									Steps 2 - 4
								</div>
							</div><!--/ .form-header-->
							
							<div class="form-wizard">
							
							<div class="row">

								<div class="col-md-5 col-sm-5">

									<div class="row">
										
										<div class="col-md-4 col-sm-4">

											<div class="input-block">
												<label>License <font style="color: red;">*</font> :</label>
												
													
								<html:select styleClass="courseSelect" property="courseid"  multiple="true" styleId="courses">
										<html:options styleClass="courseSelectOption" collection="courselist" property="courseid"	labelProperty="coursename" />
								</html:select>
								<a href="javaScript:void(0)" id="clear" style="font-style: italic;color: blue;font-size: 12px;margin-top: 5px;">  clear </a>
												
												
											</div><!--/ .input-role-->
											
										</div>
									<input type="hidden"  name="userid"  id="userId"  value="<bean:write name="userId"/>"    />	
									<input type="hidden"  name="subAmount"  id="subAmount"  value="0"    />	
									<input type="hidden"  name="totalAmount"  id="totalAmount"  value="0"    />	 
									<input type="hidden"  name="tax"  id="tax"  value="0"    />	 
									<input type="hidden"  name="discountAmount"  id="discountAmount"  value="0"    />
									<input type="hidden"  name="serviceTax"  id="serviceTax"  value="<bean:write name="serviceTax" />"    />	 
										 
									</div><!--/ .row-->

									 

								</div>
								
								

							

								<div class="col-md-6 col-sm-6" id="courseList">

									<div class="data-container"  >
										<table border="1" cellspacing="10" cellpadding="5" width="100%" class="courseListTable"  style="text-align:center;">
                                        <tr class="courseDispTr">
                                        <td>Selected License</td>
                                        <td>Space(in GB)</td>
                                        </tr>
                                         <tr ><td colspan="2">Select the License</td></tr>
                                         <!--  <tr><td> Sub Total </td><td>800</td></tr>
                                         <tr><td>Service Tax @ 14% <span style="color:#0DD80D;font-size:16px;">+</span> </td><td>140</td></tr>
                                         <tr><td> Total Amount Discount@ <span style="color:#CB0C04;font-size:16px;">-</span></td><td> 20%</td></tr>
                                         <tr><td> Total Amount</td><td> 940</td></tr> -->
                                       </table>
									</div>

								</div>
							
							
							
						</div><!--/ .form-wizard-->
								</div>
							 
						
						<div class="next1"  style="float:right;margin-top:10px;">
							<button class="button button-control" type="submit"  id="validateSubmit"><span>Register & Make Payment </span></button>
							<div class="button-divider"></div>
						</div>
					

					</div><!--/ .row-->
					</div>
					
 
					 </html:form>
 
						 
					<!--/ .form-wizard-->
				</div><!--/ .container-->
				
			</div><!--/ .form-container-->

		</div><!--/ #content-->


		<!-- - - - - - - - - - - - end Content - - - - - - - - - - - - - -->
 <!-- Start Footer -->
     <jsp:include page="jsp/common/student_footer.jsp" /> 
  <!-- End Footer -->
 
 
	</body>

<!-- Mirrored from 0.s3.envato.com/files/94682367/form-wizard-with-icon.html by HTTrack Website Copier/3.x [XR&CO'2013], Tue, 09 Jun 2015 12:23:40 GMT -->
</html>