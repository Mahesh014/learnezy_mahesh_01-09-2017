
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>

<%@page import="com.itech.elearning.forms.LoginForm"%>
<html>
<head>
<title>Itech Training :: Login</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300' rel='stylesheet' type='text/css'> -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/cmsstyle.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/carousel.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>
<link rel="stylesheet" type="text/css" href="css/loginstyle.css" />
<script>
  function preventBack(){window.history.forward();}
  setTimeout("preventBack()", 0);
  window.onunload=function(){null};
</script>
<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script src="js/cufon-yui.js" type="text/javascript"></script>
<script src="js/ChunkFive_400.font.js" type="text/javascript"></script>
<script type="text/javascript">
	Cufon.replace('h1',{ textShadow: '1px 1px #fff'});
	Cufon.replace('h2',{ textShadow: '1px 1px #fff'});
	Cufon.replace('.back');
</script>

<link rel="stylesheet" type="text/css" href="src/DateTimePicker.css" />
<script type="text/javascript" src="src/DateTimePicker.js"></script>
        
<!-- mine -->
<style type="text/css">

#dvLoading

{
 position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('images/loader.gif') 50% 50% no-repeat rgb(249,249,249);
}

</style>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/homecalender.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/js/common.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/date/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/date/jquery-ui.css" />

 <script type="text/javascript">
 function preventBack() {
		window.history.forward(0);
	}
	setTimeout("preventBack()", 0);
	window.onunload = function() {
		null;
	};  
	
	function validateLogin(){
		if(document.getElementById("username").value.trim()=="" && document.getElementById("password").value.trim()=="" ){
			alert("Username and Password cannot be empty");
			document.getElementById("username").focus();
			document.getElementById("password").value="";
			return false;
		}else
		if(document.getElementById("username").value.trim()==""){
			alert("User Name cannot be empty");		
			document.getElementById("username").focus();
			return false;
		}else
		if(document.getElementById("password").value.trim()==""){
			alert("Password cannot be empty");		
			document.getElementById("password").focus();
			return false;
		}else{
			document.getElementById("loginform").submit();
		}
	}
	
	
	function validateForgot(){
		str=   document.getElementById("forgotmail");
		if(str.value.trim()==""){
			alert(" Please enter " +str.name+"!.");
			str.focus();
			return false;
		}
		re =/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/; 
		if(str.value.trim()!=""){
			if (str.value.search(re)== -1) {
				alert("Please Enter valid Email Id");
				str.value = "";
				str.focus();
				return false;
			}
		}	
	  document.getElementById("forgotpass").submit();
		
	}
	
	</script>
	
	
	
	
		<script type="text/javascript">
	function validateRegister(){
		if(document.forms[0].firstName.value.trim()==""){
			alert("First Name cannot be empty");
			document.forms[0].firstName.focus();
			return false;
		}else
		if(document.forms[0].lastName.value.trim()==""){
			alert("Last Name cannot be empty");
			document.forms[0].lastName.focus();
			return false;
		}else
		if(document.forms[0].gender.value.trim()== "-1"){
			alert("Please Select the Gender");
			document.forms[0].gender.focus();
			return false;
		}else
		if(document.forms[0].dob.value.trim()==""){
			alert("Date of Birth cannot be empty");
			document.forms[0].dob.focus();
			return false;
		}else
		if(document.forms[0].contactno.value.trim()=="" ){
			alert("Mobile Number cannot be empty ");
			document.forms[0].contactno.focus();
			return false;
		}else
		if(document.forms[0].contactno.value.length < 10){
			alert("Mobile Number is not Valid");
			document.forms[0].contactno.focus();
			return false;
		}else
		if(document.forms[0].qualification.value.trim()==""){
			alert("Qualification cannot be empty");
			document.forms[0].qualification.focus();
			return false;
		}else
		if(document.forms[0].phone.value.trim()!= ""  ){
			if(document.forms[0].phone.value.length < 13){
				alert("Alternative  Contact Number is not Valid");
				document.forms[0].phone.focus();
				return false;
			}
		}else
		if(document.forms[0].emailId.value.trim()==""){
			alert("Email cannot be empty");
			document.forms[0].emailId.focus();
			return false;
		}else
		if(document.forms[0].address.value.trim()==""){
			alert("Address cannot be empty");
			document.forms[0].address.focus();
			return false;
		}else
		if ($("#courses").val() ==null) {
			alert("Please select at least one course");
			return false;
		}else
		if(document.forms[0].uname.value.trim()==""){
			alert("User Name cannot be empty");
			document.forms[0].uname.focus();
			return false;
		}else
		if(document.forms[0].password.value.trim()==""){
			alert("Password cannot be empty");
			document.forms[0].password.focus();
			return false;
		}else
		if(document.forms[0].retypepassword.value.trim()==""){
			alert("Re-Type Password cannot be empty");
			document.forms[0].retypepassword.focus();
			return false;
		}else{
			return true;	
		}
	}


	function validFNameonkey(){
		re = /[^a-z,A-Z]/;
		var str=document.forms[0].firstName.value.replace(re,"");
		document.forms[0].firstName.value=str;
	}

	function onDateChange(){
		if(document.forms[0].dob.value.trim()!="" ){
			document.forms[0].contactno.focus();
		}
	}
	
	function validLNameonkey(){
		re = /[^a-z,A-Z]/;
		var str=document.forms[0].lastName.value.replace(re,"");
		document.forms[0].lastName.value=str;
	}
	function validemail(){
		re =/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/; 
		if(document.forms[0].emailId.value.trim()!=""){
			if (document.forms[0].emailId.value.search(re)== -1) {
				alert("Please Enter valid Email Id");
				document.forms[0].emailId.value = "";
				document.forms[0].emailId.focus();
			}
		}	
	}
	function fmtcontacno(){
		re = /\D/g; // remove any characters that are not numbers
		socnum=document.forms[0].phone.value.replace(re,"");
		sslen=socnum.length;
		if(sslen>3&&sslen<7){
			ssa=socnum.slice(0,3);
			ssb=socnum.slice(3,6);
			document.forms[0].phone.value="("+ssa+")"+ssb;
		}else{
			if(sslen>6){
				ssa=socnum.slice(0,3);
				ssb=socnum.slice(3,6);
				ssc=socnum.slice(6,9);
				ssd=socnum.slice(9,10);
				document.forms[0].phone.value="("+ssa+")"+ssb+"-"+ssc+ssd;
			}else{
				document.forms[0].phone.value=socnum;
			}
		}
	}

	function fmtmobile(){
		//alert("arg");
		re = /\D/g; // remove any characters that are not numbers
		socnum=document.forms[0].contactno.value.replace(re,"");
		document.forms[0].contactno.value=socnum;
		 
	}
		
	function checkpassword(){
		if(document.forms[0].password.value!=document.forms[0].retypepassword.value){
			alert("Passwords does not match");
			document.forms[0].password.value="";
			document.forms[0].retypepassword.value="";
			document.forms[0].password.focus();
		}		
	}
 
	$(function() {
	     $( "#datepicker1" ).datepicker({dateFormat: 'dd-mm-yy',changeMonth: true, changeYear: true });
	 });
	
		  
		  
		  
		  </script>
<script>
$(document).ready(function() {
	function disableBack() { window.history.forward() }
	window.onload = disableBack();
    window.onpageshow = function(evt) { if (evt.persisted) disableBack() }
	$('#imageFile').change(function () {
		var val = $(this).val().toLowerCase();
		//alert(val);
		var regex = new RegExp("(.*?)\.(jpg|png|gif|jpeg)$");
		if(!(regex.test(val))) {
		 	$(this).val('');
		 	alert('Please select .jpg .png .gif .jpeg format only');
		 } 
	});  
});
</script>
 
</head>
<body >
<div id="dvLoadingv" ></div> 

   <!-- Start Header -->
       <div class="header">	
   	 	    <div class="header-top">
   	 	      <div class="wrap"> 
   	 	    	 <div class="header-top-left">
   	 	    	 	<p>ph: +91-9611421111</p>
   	 	    	 </div>
   				  <div class="header-top-right">
				        <ul>
				            <li><a href"#">Careers</a></li>
				            <li  class="login">
				              <div id="loginContainer">
				            	   <a href="#" id="loginButton"><span>Join Us</span></a>
						                
						             </div>
				               </li>
				               <li><a href="#" >Sitemap</a></li>
				         </ul>
				    </div>
			      <div class="clear"></div>
			     </div> 
		      </div>
             <div class="header-logo-nav">
             	  <div class="navbar navbar-inverse navbar-static-top nav-bg" role="navigation">
				      <div class="container">
				        <div class="navbar-header">
				          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				            <span class="sr-only">Toggle navigation</span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				          </button>
				         <div class="logo"> <a class="navbar-brand" href="#"><img src="images/Itech Solutions logo.png" alt="" /></a></div>
				          <div class="clear"></div>
				        </div>
				       
				      </div>
				    </div>
		         <div class="clear"></div>
	        </div>
	        <div class="header-banner">
	        	
			    
	           </div>
	          
             </div>
   <!-- End Header -- >
      <!-- Start Main Content --><!--
      			<logic:notEmpty name="status"> 			   			
		<h3><font color="red" style="font-size: 20px;margin-left:525px; "><bean:write name="status"></bean:write></font></h3>
	</logic:notEmpty>
-->
	
	
   
	 <div class="main">	 
	 	<div class="wrapper">
			
			
			<div class="content" style="min-height:460px;">
				<div id="form_wrapper" class="form_wrapper">
					<html:form action="studentRegistration.do?action=addUser" method="post" styleClass="register"    enctype="multipart/form-data">
						<h3>Register</h3>
						<div class="column">
							<div>
								<label>First Name*:</label>
								<input type="text" name="firstName" id="firstName"	onkeyup="validFNameonkey(this)" autofocus />
								<span class="error">Please Enter the First Name</span>
							</div>
							
							<div>
								<label>Gender*:</label>
								<select name="gender">
									<option value="-1">Select</option>
									<option value="Male">Male</option>
									<option value="Female">Female</option>
								</select>
								
							</div>
                            <div>
								<label>Mobile*:</label>
								<input type="text"  name="contactno" onkeyup="fmtmobile();" maxlength="10" size="15"/>
								
							</div>
                             <div>
								<label>Alternative Contact No.:</label>
								<input type="text" name="phone" onkeyup="fmtcontacno();"	maxlength="13"  />
								
							</div>
                           <div style="height: 200px;">
								<label>Address*:</label>
								<textarea name="address" style="resize: none" name="address" rows="5"></textarea>
								
							</div>
							
							<div>
								<label>Course*:</label>
								<html:select property="courseid"  multiple="true" styleId="courses">
										<html:options collection="courselist" property="courseid"	labelProperty="coursename" />
								</html:select>
								
							</div>
							
							<div >
                            <label>Profile Photo*:</label>
								<input name="imageFile" id="imageFile" type="file"  />
								
							</div>
						</div>
						<div class="column">
                             <div>
								<label>Last Name*:</label>
								<input type="text" name="lastName"	onkeyup="validLNameonkey(this)"  />
								
							</div> 
							<div>
								<label>Date Of Birth*:</label>
								<input type="text" readonly name="dob" id="datepicker1" onchange="onDateChange();" />
								
                                
							</div>
                            <div>
								<label>Qualification*:</label>
								<input type="text" name="qualification"	onkeyup="isValidName(this, 'Qualification')" />
								
							</div>
							<div>
								<label>Email*:</label>
								 <input type="text" name="emailId" 	onchange="validemail(this)" />
							</div>
                            
                            <div>
								<label>User Name*:</label>
								<input type="text"  name="uname" size="20" />
							</div>
							
							<div>
                            <label>Password*:</label>
								<input type="password" name="password"/>
							</div>
							
							<div >
                            <label>Confirm Password*:</label>
								<input name="retypepassword" type="password"	onchange="checkpassword();" />
							</div>
						</div>
						<div class="bottom">
							 <input type="submit" value="Register" onclick="return validateRegister();"  />
							
                           
							<a href="#" rel="login" class="linkform">You have an account already? Log in here</a>
							<div class="clear"></div>
						</div>
					</html:form>
					
					<form  action="Login.do?action=login" class="login active" method="post" id="loginform" onsubmit="return validate();" >
						<h3>Sign In</h3>
						
				
						
                        <h4 style="text-align:center; margin:5px 0 0 10px;">Existing Users can Login here</h4>
                        <div style="color: red;text-align: center;margin-top: 5px;"><logic:notEmpty name="status"><bean:write name="status"/> </logic:notEmpty></div>
						<div>
                        
							<label>Username:</label>
							<input type="text"  name="userName" id="username" value="" autofocus />
							
						</div>
						<div>
							<label>Password:</label>
							<input type="password" type="password" name="password" id="password"	value=""/>
							 
						</div>
						<div class="bottom">
							
							
							<input type="submit" value="Submit"  id="button" name="sub" onclick="return validateLogin();"></input>
                        <a href="#" rel="forgot_password" class="forgot linkform">Forgot your password?</a>
							<a href="#" rel="register" class="linkform">You don't have an account yet? Register here</a>
							<div class="clear"></div>
						</div>
					</form>
					<form action="ForgotPassword.do?action=sendmail" method="post"  class="forgot_password"  id="forgotpass" onsubmit="return forgotmail();">
						<h3>Forgot Password</h3>
						<div>
							<label>Enter Your Email ID*:</label>
							<input type="text" name="email" id="forgotmail" autofocus/>
							
						</div>
						<div class="bottom">
							<input type="submit" value="Submit" 	onclick="return validateForgot();"></input>
							<a href="#" rel="login" class="linkform">Suddenly remebered? Log in here</a>
							
							<div class="clear"></div>
						</div>
					</form>
				</div>
				<div class="clear"></div>
			</div>
			
		</div>
		

		<!-- The JavaScript -->
        
	<!--	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>-->
		<script type="text/javascript">
			$(function() {
					//the form wrapper (includes all forms)
				var $form_wrapper	= $('#form_wrapper'),
					//the current form is the one with class active
					$currentForm	= $form_wrapper.children('form.active'),
					//the change form links
					$linkform		= $form_wrapper.find('.linkform');
						
				//get width and height of each form and store them for later						
				$form_wrapper.children('form').each(function(i){
					var $theForm	= $(this);
					//solve the inline display none problem when using fadeIn fadeOut
					if(!$theForm.hasClass('active'))
						$theForm.hide();
					$theForm.data({
						width	: $theForm.width(),
						height	: $theForm.height()
					});
				});
				
				//set width and height of wrapper (same of current form)
				setWrapperWidth();
				
				/*
				clicking a link (change form event) in the form
				makes the current form hide.
				The wrapper animates its width and height to the 
				width and height of the new current form.
				After the animation, the new form is shown
				*/
				$linkform.bind('click',function(e){
					var $link	= $(this);
					
					var target	= $link.attr('rel');
					//alert(target);
					
					
					$currentForm.fadeOut(400,function(){
						//remove class active from current form
						$currentForm.removeClass('active');
						//new current form
						$currentForm= $form_wrapper.children('form.'+target);
						//animate the wrapper
						$form_wrapper.stop()
									 .animate({
										width	: $currentForm.data('width') + 'px',
										height	: $currentForm.data('height') + 'px'
									 },500,function(){
										//new form gets class active
										$currentForm.addClass('active');
										//show the new form
										$currentForm.fadeIn(400);
									 });
					});
					
					/* if(target==="register"){
						$("#firstName").val(target);
						alert("RRR");
					}else
					if (target==="forgot_password") {
						$("#forgotmail").focus();
						alert("FF");
					}else 
					if (target==="login") {
						$("#username").focus();
						alert("LL");
					} */
					 
				});
				
				function setWrapperWidth(){
					$form_wrapper.css({
						width	: $currentForm.data('width') + 'px',
						height	: $currentForm.data('height') + 'px'
					});
				}
				
				/*
				for the demo we disabled the submit buttons
				if you submit the form, you need to check the 
				which form was submited, and give the class active 
				to the form you want to show
				*/
				 	
			});
        </script>
	      
	 </div>
    </div>
   <!-- End Main Content -->
	   
  
</body>
</html>

    	
    	
            