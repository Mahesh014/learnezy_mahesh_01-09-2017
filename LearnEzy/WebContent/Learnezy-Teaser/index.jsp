<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
	<head>
	
	<script src="<%=request.getContextPath()%>/js/jquery-2.1.1.min.js"></script>
<script src="<%=request.getContextPath()%>/js/validation/jquery.validate.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="<%=request.getContextPath()%>/js/validation/additional-methods.js"></script>

  
		<title>Learnezy - Teaser</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
		
		
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/init.js"></script>
        
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-xlarge.css" />
		</noscript>
		
		<script>
		
$(document).ready(function(){
	alert();
	$("#EnquiryForm").validate({
		
		rules:{
		
		name:{
		
		required:true,
		lettersonly: true
	},
mobile:{
		
		required:true,
		digits:true,
        minlength:10,
        maxlength:10
	},
		
	
	email:{
		required:true,
		email:true
	},
	
	textarea:{
		required:true
		
	}
	},
	
	messages:{
		
		name:{
		
		required:"Name required",
		lettersonly: "Enter Characters only"
	},
	mobile:{
		
		required:"Password required",
		digits:"Only numbers required",
        minlength:"Minimum length 10",
        maxlength:"Maxlength 10 only"
			
	},
	email:{
		required:"Email-id required",
		email:"Enter valid email-id"
	},
	
	textarea:{
		required:"Message is required"
		
	}
	
	}


	});
	
});
</script>
		
	</head>
	<body id="top">

		
		<!-- Banner -->
			<div id="banner">
				<div class="inner">
					
				</div>
			</div>
<section class="special box" >
<img src="images/LEARNEZY-LOGO.png" class="icon major" ></section>
		<!-- One -->
			
		<!-- Two -->
		
		<!-- Three -->
			<section id="three" class="wrapper style1">
				<div class="container">
					<div class="row">
						<div class="12u">
							<section>
								<h2>They Say Knowledge is Power !<br>
                                Are you ready to Aquire It?</h2>
							</section>
						</div>
						
					</div>
				</div>
			</section>			
			
		<!-- Footer -->
			<footer id="footer">
				<div class="container">
					<div class="row double">
						<div class="12u" style="text-align:center">
							
									<h2 style="color:#666f77; text-transform:none">To know more, fill up the below form and we will send you the details</h2></div></div>
                                   <div class="row double"> 
                                   <div class="3u" style="visibility:hidden">fgfdgfhfd</div>
                                   <div class="6u" class="pull-right"> 
<form action="#" id="EnquiryForm"  method="post">
  
                                    Name
                                    <input type="text" name="name" placeholder="Enter your name here"><br>
                                    
                                    Mobile
                                    <input type="text" name="mobile" placeholder="mobile"><br>
                                    
                                    Email address
                                    <input type="email" name="email"  placeholder="Email"><br>
  
                                    Message
                                    <textarea name="textarea"></textarea><br>
  
   
 
  <button type="submit" class="button big special">Submit</button>
</form>
					</div>	
                   <div class="3u" style="visibility:hidden">fgfdgfhfd</div>
                    			</div>
				
                </div>
               <div style="background-color:#000000">
               <div class="12u" style="text-align:center">© Learnezy.com. All rights reserved.</div>
               
               </div> 
                
                
			</footer>

	</body>
</html>