<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Java-Reviews</title>
<script src="<%=request.getContextPath()%>/js/jquery-2.1.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<link href="assets/css/master.css" rel="stylesheet">
<link href="favicon.png" type="image/x-icon" rel="shortcut icon">

<script src="<%=request.getContextPath()%>/js/jquery-2.1.1.min.js"></script>
<script src="<%=request.getContextPath()%>/js/validation/jquery.validate.min.js"></script>
<script src="<%=request.getContextPath()%>/js/validation/additional-methods.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>


<link rel="stylesheet" href="<%=request.getContextPath()%>/jquerypopupcss/jquery-ui.css">
<script src="<%=request.getContextPath()%>/jquerypopup/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

<style>
.stars{
width: 105px;
display:inherit
}
b{

padding-bottom: 5px;
font-size:20px;
}
.display{
display:inline;
font-size:13px;
margin-left:20px;
color: #3B99D7;
}
.view{
font-size:15px;
}
</style>
</head>
<body>
<div id="page-preloader"><span class="spinner"></span></div>

<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200"> 
  
  <div id="wrapper"> 
  
      <jsp:include page="commonindexheader.jsp" />        
                
	    <div class="wrap-title-page">
	      <div class="container">
	        <div class="row">
	          <div class="col-xs-12">                          
	            <h1 class="ui-title-page"></h1>
	          </div>
	        </div>
	      </div>
	      <!-- end container--> 
	    </div>
    
   <div class="section-breadcrumb">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="wrap-breadcrumb clearfix">
              <ol class="breadcrumb">
                <li><a href="index.html"><i class="icon stroke icon-House"></i></a></li>
                <li><a href="courses_list.html">all courses</a></li>
                <li><a href="courses-details.html">Java</a></li>
                <li class="active">Reviews</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
   </div>
    
	<div class="container">
      <div class="row">
        <div class="col-md-8">
         <h2 class="course-details__title">Customer Reviews</h2>
		 <logic:notEmpty name="reviewList" >
		 
			<logic:iterate id="review" name="reviewList" indexId="index">
	  			<b><bean:write name="review" property="emailid"/></b> 
	  	   		<p class="display" style="right">  <bean:write name="review" property="createDateTime"/></p> 
			  	<div style="height:10px"></div>	   
			    <img class="stars" src="<%=request.getContextPath() %>/<bean:write name="review" property="rating" />"  >	        
			    <div style="height:8px"></div>  		     
	     		<p><bean:write name="review" property="reviewDescription"/></p> 
	    	</logic:iterate> 
	    	 
	    	<div id=view1>
 				<a class="view" href="review.do?action=reviewlistviewmore"  style="text-decoration:none">view more</a>
 			</div>
 			
		</logic:notEmpty>
	
	<div style="height:20px"></div>
		
	<logic:notEmpty name="reviewListmore">
	<logic:iterate id="review" name="reviewListmore" indexId="index">
	  <b><bean:write name="review" property="emailid"/></b> 
	  	   <p class="display">  <bean:write name="review" property="createDateTime"/></p> 
	  	   
	  	   	<div style="height:10px"></div>
	  	   
	        <img class="stars" src="<%=request.getContextPath()%>/<bean:write name="review" property="rating"/>"></img>
	     	
	     	<div style="height:8px"></div>
	     
	     <p> <bean:write name="review" property="reviewDescription"/></p> 
	     
	   			 </logic:iterate>
	    
			</logic:notEmpty>

		</div>
	</div>
</div>
<div style="height:30px"></div>



  <jsp:include page="Commonfooter.jsp"></jsp:include>
</div>

</div>
<!-- SCRIPTS --> 
<script src="assets/js/jquery-migrate-1.2.1.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<script src="assets/js/modernizr.custom.js"></script> 
<script src="assets/js/waypoints.min.js"></script> 

<!--THEME--> 
<script  src="assets/plugins/sliderpro/js/jquery.sliderPro.min.js"></script> 
<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script> 
<script src="assets/plugins/isotope/jquery.isotope.min.js"></script> 
<script src="assets/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script> 
<script src="assets/plugins/datetimepicker/jquery.datetimepicker.js"></script> 
<script src="assets/plugins/jelect/jquery.jelect.js"></script> 
<script src="assets/plugins/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js"></script> 
<script src="assets/js/cssua.min.js"></script> 
<script src="assets/js/wow.min.js"></script> 
<script src="assets/js/custom.min.js"></script> 

</body>
</html>