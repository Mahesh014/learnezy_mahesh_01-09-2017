

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ page import="java.sql.*" %>
<%@page import="com.itech.elearning.forms.LoginForm"%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui">
<meta name="googlebot" content="noodp">
<title>LearnEzy.com - Sign in</title>
<link href="favicon.png" type="image/x-icon" rel="shortcut icon">
<link href="assets/css/master.css" rel="stylesheet">


<!-- BOOTSTRAP -->
<link href="assets/css/bootstrap-responsive.css" rel="stylesheet" media="screen">

<!-- FONT AWESOME -->
<link href="assets/css/font-awesome.min.css" rel="stylesheet" media="screen">


<style>
.submit{
margin-top:10px;}
.form-box .footer {
    padding: 20px;}
</style>
</head>
<script type="text/javascript">

 window.history.forward();
 function noBack() { window.history.forward(); }
 
	
	function validate() {

		if (document.forms[0].userName.value.trim() == "") {
			alert("Enter Your Email id");
			document.forms[0].userName.focus();
			return false;
		}else
		if (document.forms[0].password.value.trim() == "") {
			alert("Enter Your Password");
			document.forms[0].password.focus();
			return false;
		}

	
		return true;
	}
	function validemail() {
		re = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
		if (document.forms[0].userName.value.trim() != "") {
			if (document.forms[0].userName.value.search(re) == -1) {
				alert("Please Enter valid Email Id");
				document.forms[0].userName.value = "";
				document.forms[0].userName.focus();
			}
		}
	}


	
</script>
<body> 
<% String url = request.getRequestURL().toString();
String img = "";
%>
<% 

if(url.equals("http://localhost:8080/Elearning/login.jsp"))
{
	 img = "LEARNEZYLatest-LOGO.png";
}
else if(url.equals("test.elearning.in"))
{
	img="itech.png";
}

%>


<%
String urlre = request.getRequestURL().toString();
urlre = "Itech";
String compid = null;
Connection con = null;
String urln = "jdbc:mysql://localhost:3306/";
String db = "elearningcom";
String driver = "com.mysql.jdbc.Driver";
String userName ="root";
String password="Itech123!";

int sumcount=0;
Statement st;
try{
Class.forName(driver).newInstance();
con = DriverManager.getConnection(urln+db,userName,password);
String query = "select * from companymaster where CompanyName =  '"+urlre+"' ";
System.out.println(query);
st = con.createStatement();
ResultSet rs = st.executeQuery(query);
%>
<%
while(rs.next()){
%>
<!--<tr><td><%= compid =  rs.getString(1)%></td>

</tr>
--><%
}
%>
<%
}
catch(Exception e){
e.printStackTrace();
}
%>

<!-- Loader -->
<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->
		<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200">
			<div id="wrapper">
    <jsp:include page="Commonheader.jsp"></jsp:include>
    
     <div class="wrap-title-page">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h1 class="ui-title-page">Sign In</h1>
          </div>
        </div>
      </div>
      <!-- end container--> 
    </div>
    <!-- end wrap-title-page -->
    
    <div class="section-breadcrumb">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="wrap-breadcrumb clearfix">
              <ol class="breadcrumb">
                <li><a href="index.jsp"><i class="icon stroke icon-House"></i></a></li>
                <li class="active">Sign In</li>
                
              </ol>
            </div>
          </div>
        </div>
        <!-- end row --> 
      </div>
      <!-- end container --> 
    </div>
    <!-- end section-breadcrumb -->
       
    
    <div class="main-content">
          
          
    <div class="container">
    <div class="buttons">
            	<button class="btn1 login-btn"><i class="fa fa-facebook"></i>Login with Facebook</button>
                
                <button class="btn2 login-btn"><i class="fa fa-google-plus"></i>Login with Google</button>
                
                <button class="btn3 login-btn"><i class="fa fa-yahoo"></i>Login with Yahoo</button>
                
                <button class="btn4 login-btn"><i class="fa fa-linkedin"></i>Login with Linkein</button>
                
                <button class="btn5 login-btn"><i class="fa fa-windows"></i>Login with Window Live</button>
               
            </div>
            
            
          <div class="row">
           <div class="span3">
                	<div class="sidebar">
                    	
                        
                    </div>  <!-- sidebar -->
                </div>  <!-- span3 -->          
          
            	<div class="span6">
                	<div class="form-box">
                	
                	
                       					<form  action="Login.do?action=login" class="login active" method="post" id="loginform" >

                        <div class="form-body">
                        <fieldset>
                        <legend>Sign In Below:</legend>
                        <label>Email Address</label>
                        <input type="email" placeholder="Enter your E-mail ID or User Name" class="input-block-level" name="userName" onblur="validemail();" required id="userNameLogin"/>
                        <label>Password</label>
                        <input  name="password" id="passwordLogin" type="password" placeholder="Enter Password"  class="input-block-level" required>                        
                        
                        <button type="submit" onclick="return validate()" class="btn-style submit" >Submit</button>
                       <span class="forgot"> <a href="forgotpassword.jsp">Forgot Password?</a> </span>
                        </fieldset>
                        </div>
                        	 </form>
                                
                        <div class="footer">
                        
                              <p class="signup"> Don't have an account? Sign up Now <button class="btn-style reg" onclick="window.location.href='registernew.jsp'">Register</button> </p>
                               
                        
                       
                        </div>
                       
                    </div>
                </div>
                <div class="span3">
                	<div class="sidebar">
                    	
                        
                    </div>  <!-- sidebar -->
                </div>  <!-- span3 -->
            </div>   <!-- row -->
        
        
        </div>   <!-- container -->
        
      
    </div>
    <!-- end main-content -->
    
     <jsp:include page="Commonfooter.jsp"></jsp:include>
  </div>
  <!-- end wrapper --> 
</div>
<!-- end layout-theme --> 

<!-- Jquery Lib -->
<script src="assets/js/jquery-1.10.1.min.js"></script>

<!-- SCRIPTS --> 
<script src="assets/js/jquery-migrate-1.2.1.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<script src="assets/js/modernizr.custom.js"></script> 
<script src="assets/js/waypoints.min.js"></script> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> 

<!--THEME--> 
<script  src="assets/plugins/sliderpro/js/jquery.sliderPro.min.js"></script> 
<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script> 
<script src="assets/plugins/isotope/jquery.isotope.min.js"></script> 
<script src="assets/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script> 
<script src="assets/plugins/datetimepicker/jquery.datetimepicker.js"></script> 
<script src="assets/plugins/jelect/jquery.jelect.js"></script> 
<script src="assets/plugins/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js"></script> 
<script src="assets/js/cssua.min.js"></script> 
<script src="assets/js/wow.min.js"></script> 
<script src="assets/js/custom.min.js"></script>

</body>
</html>
