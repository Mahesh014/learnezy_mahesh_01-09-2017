<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="robots" content="INDEX, FOLLOW">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui">
<meta name="description" content="Enroll Into The <logic:notEmpty name="coursename"><bean:write name="coursename"/></logic:notEmpty> Course from <logic:notEmpty name="institute"><bean:write name="institute"/> Today. <bean:write name="institute"/></logic:notEmpty> Is One Of The Best Providers of <logic:notEmpty name="coursename"><bean:write name="coursename"/></logic:notEmpty> Courses In India."/>
<link rel="canonical" href="www.learnezy.com/<logic:notEmpty name="coursename"><bean:write name="coursename"/></logic:notEmpty>-course-details"/>
<title><logic:notEmpty name="coursename"><bean:write name="coursename"/></logic:notEmpty> Courses Details|Learnezy</title>
<link href="favicon.png" type="image/x-icon" rel="shortcut icon">
<link href="assets/css/master.css" rel="stylesheet">

<script src="<%=request.getContextPath()%>/js/jquery-2.1.1.min.js"></script>
<script src="<%=request.getContextPath()%>/js/validation/jquery.validate.min.js"></script>
<script src="<%=request.getContextPath()%>/js/validation/additional-methods.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>


<link rel="stylesheet" href="<%=request.getContextPath()%>/jquerypopupcss/jquery-ui.css">
<script src="<%=request.getContextPath()%>/jquerypopup/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
 <!--<style>
 
 
div.stars {
	  width: 281px;
	 display: inline-block;}
	input.star { display: none; } 
	label.star {
	  float: right;
	  padding: 10px;
	  font-size: 36px;
	  color: #444;
	  transition: all .2s;}
	input.star:checked ~ label.star:before {
	  content: '\f005';
	  color: #FD4;
	  transition: all .25s;}
	input.star-5:checked ~ label.star:before {
	  color: #FE7;
	  text-shadow: 0 0 20px #952;}
	input.star-1:checked ~ label.star:before { color: #F62; }
	label.star:hover { transform: rotate(-15deg) scale(1.3); } 
	label.star:before {
	  content: '\f006';
  font-family: FontAwesome;
	}
</style>-->
<style>
  .slideanim {visibility:hidden;}

  .slide {
      animation-name: slide;
      -webkit-animation-name: slide;
      animation-duration: 1s;
      -webkit-animation-duration: 1s;
      visibility: visible;
  }

.review{
margin:0px 0px 10px 0px;
}
.stars{
margin: 16px 0px 10px 0px;
width: 117px;

}
.reviewcontent{
margin: 8px 0px 6px 0px;

}
</style>



<script type="text/javascript">
$(document).ready(function()
		{
	$("#name").focus();
	$("#enquiryPopup").hide();
		});
$(document).ready(function()
		{

$("#btnid").click(function()
		{
	
	var id=$("#sid").val();
	var b=$("#statusValue").val();
	
	$("#tempId").val(id);
	$("#enquiryPopup").show();
	$("#enquiryDialog").dialog();
	
		});
		});

$(document).ready(function(){
    $("#statusDisp").hide();	
		var statusValue=$("#statusValue").val();
		if(statusValue=="Email-id already exist"){
			$("#statusDisp").show();
			$("#enquiryPopup").show();
			$("#enquiryDialog").dialog();
		}



		
	});

$(document).ready(function(){
	
	$("#prodForm").validate({
		
		rules:{
		
		name:{
		
		required:true
	},
	emailid:{
		required:true,
		email:true
	},
	password:{
		
		required:true
	}
		
	},
	messages:{
		
		name:{
		
		required:"Name required"
	},
	emailid:{
		required:"Email-id required",
		email:"Enter valid email-id"
	},
	
	password:{
		
		required:"Password required"
	}
	}		
	});
$("#loginform").validate({
		
		rules:{
		
		
	emailid:{
		required:true,
		email:true
	},
	password:{
		
		required:true
	}
		
	},
	messages:{
		emailid:{
		required:"Username required",
		email:"Enter valid email-id"
	},
	
	password:{
		
		required:"Password required"
	}
		
	}
		
		
	});
	
	
	
});

</script>	
 <script type="text/javascript">
 $(document).ready(function() {
	 var status=$("#status").val();
	 if(status=="Username or password is Invalid")
	 {
	        $('#signin').modal('show');


	 }

 });

 $(document).ready(function() {
	 var status=$("#status").val();
	 if(status=="Email-id already exist")
	 {
	        $('#signup').modal('show');


	 }

 });


 
	   $(document).ready(function() {

	    $('.modal-su').click(function() {
        $('#signin').modal('hide');
        $('#signup').modal('show');
    });

    $('.modal-si').click(function() {
        $('#signup').modal('hide');
        $('#signin').modal('show');
    });

	 }) ; 
	 
	 
	   </script>


<style>
.price{
float: left;
padding: 6px;
font-size:17px;
color: rgb(255, 255, 255);
}

.number{

color: rgb(255, 255, 255);}
.month{color:#fff;
font-size:12px;
padding-right:6px;}
</style>

</head>

<body>

 
<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->

<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200"> 
  

  <div id="wrapper"> 
    
                    <jsp:include page="commonindexheader.jsp" /> 
      
    
    <div class="wrap-title-page">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
                      <h1 class="ui-title-page"><logic:notEmpty name="coursename"><bean:write name="coursename"/></logic:notEmpty> Courses Details</h1>
          
          
           <logic:notEmpty name="coursedesc">
                <logic:iterate id="coursedesc" name="coursedesc">
                               
          </div>
        </div>
      </div>
      <!-- end container--> 
    </div>
    <!-- end wrap-title-page -->
    
    <div class="section-breadcrumb">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="wrap-breadcrumb clearfix">
              <ol class="breadcrumb">
                <li><a href="index.html"><i class="icon stroke icon-House"></i></a></li>
              
                <li><a href="courses_list.html">all courses</a></li>
                <li class="active"><bean:write name="coursedesc" property="coursenamelatest"></bean:write></li>
              </ol>
            </div>
          </div>
        </div>
        <!-- end row --> 
      </div>
      <!-- end container --> 
    </div>
    <!-- end section-breadcrumb -->
    
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <main class="main-content rtd">
            <article class="course-details">
           
              <h2 class="course-details__title"><bean:write name="coursedesc" property="topicname"></bean:write></h2>
              <div class="course-details__subtitle"><bean:write name="coursedesc" property="subtopicname"></bean:write></div>
              <img class="img-responsive" src="<%=request.getContextPath()%>/imageUpload/<bean:write name="coursedesc"  property="contentimage" />" height="480" width="750" alt="foto">
              <h3 class="course-details__title-inner decor">DESCRIPTION</h3>
   <bean:define id="abc"  name="coursedesc" property="description"  type="java.lang.String"/>              <p><%=abc %></p>
                    <!-- end list-collapse --> 
            </article>
            <!-- end course-details --> 
          </main>
          <!-- end main-content --> 
         
 
        </div>
        <!-- end col -->
        
        <div class="col-md-4">
          <aside class="sidebar">
            <div class="widget widget_course-description">
              <div class="block_content"> 
           <a href="#"  data-toggle="modal" data-target="#signup" class="btn btn-primary btn-effect"><span class="list-information__title price">e-Learning</span> <span class="list-information__description"><span class="list-information__number number"><bean:write name="coursedesc" property="fees"></bean:write> </span><span class="month">/ month</span></span></a>
           <div class="modal fade" id="signin" role="dialog" aria-labelledby="signinLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="signinLabel">Login to your account! </h4>
                    </div>
                    <div class="modal-body">
                        <form action="Login1.do?action=login1" class="login active" method="post" id="loginform"  role="form">
                            
                            <logic:notEmpty name="status">
                          <center>  <font style="color:red; text-align: center !important;">
                            <bean:write name="status"/></font></center>
                            <input type="hidden" name="status" id="status" value="<bean:write name="status"/>">
                            </logic:notEmpty>
                            <div class="form-group">
                                <input type="text" name="emailid"  id="userNameLogin" placeholder="Email Address" class="form-control" autofocus>
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" id="passwordLogin" placeholder="Password" class="form-control">
                            </div>
                            <logic:empty name="course1">
												<input type="hidden" name="course" value="<bean:write name="coursedesc" property="coursenamelatest"></bean:write>"></input>
												<input type="hidden" name="price" value="<bean:write name="coursedesc" property="fees"></bean:write>"></input>
											</logic:empty>
												<logic:notEmpty name="course1">
													<input type="hidden" name="course" value=<bean:write name="course1"/>></input>
												
												
												</logic:notEmpty>
												<logic:notEmpty name="price1">
													<input type="hidden" name="price" value=<bean:write name="price1"/>></input>
												
												
												</logic:notEmpty>
										
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="checkbox custom-checkbox"><label><input type="checkbox"> Remember me</label></div>
                                    </div>
                                    <div class="col-xs-6 align-right">
                                        <p class="help-block" style="text-align:right"><a href="Forgotpassword1.jsp" class="text-green isThemeText">Forgot password?</a></p>
                                    </div>
                                </div>
                            </div>
                           <div class="form-group">
                                <div class="btn-group-justified">
                                    <input type="submit" value="Sign In" class="btn btn-primary btn-effect">
                                </div>
                            </div>
                            <p class="help-block">Don't have an account? <a href="#" class="modal-su text-green isThemeText" >Sign Up</a></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        
        
<a href="review.do?action=reviewlist&amp;letter=<bean:write name="coursedesc" property="courseId"/>" id="as"  >Review&Rating</a>





        <div class="modal fade" id="signup" role="dialog" aria-labelledby="signupLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="signupLabel">Sign Up to take this Course</h4>
                    </div>
                    <div class="modal-body">
                        <form action="DynamicData.do?action=add"  method="post" id="prodForm"  role="form">
                        <logic:notEmpty name="status">
                          <center>  <font style="color:red; text-align: center !important;">
                            <bean:write name="status"/></font></center>
                            <input type="hidden" name="status" id="status" value="<bean:write name="status"/>">
                            </logic:notEmpty>
                           
                             <input type="hidden" name="courses" value="<bean:write name="coursedesc" property="coursenamelatest"></bean:write>">
	<input type="hidden" name="courseprice" value="<bean:write name="coursedesc" property="fees"></bean:write>">
					
					
	<input type="hidden" value="" id="tempId" name="tempId">
	
                           
                            
                            <div class="form-group">
                                <input type="text" name="name" id="name" placeholder="Full Name" class="form-control" autofocus>
                            </div>
                            
                            <div class="form-group">
                                <input type="text" name="emailid" id="emailid" placeholder="Email Address" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="password"  name="password" id="mobile" placeholder="Password" class="form-control">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="checkbox custom-checkbox"><label><input type="checkbox"> Be the first one to know about new courses and great offers! </label></div>
                                    </div>
                                   
                                </div>
                            </div>
                             <div class="form-group">
                                <div class="btn-group-justified">
                                    <input type="submit" value="Sign Up" class="btn btn-primary btn-effect">
                                </div>
                            </div>
                           <p class="help-block"> By signing up, you agree to our <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a>.</p>
                            <p class="help-block">Already have an account? <a href="#" class="modal-si text-green isThemeText">Login</a></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
           
           
           
           
           
           
           
           
           
           
              <a href="enquiry_form.jsp?course=<bean:write name="coursedesc" property="coursenamelatest"></bean:write>" class="btn btn-primary btn-effect"> <span class="list-information__title price">Classroom</span> <span class="list-information__description"><span class="list-information__number  number"><bean:write name="coursedesc" property="fees"></bean:write> </span><span class="month">/ month</span></span></a>
              
                <ul class="list-information list-unstyled">
                
                  <li class="list-information__item"> <span class="list-information__title"><i class="icon stroke icon-Agenda"></i>Starts</span> <span class="list-information__description"><bean:write name="coursedesc" property="coursestartdate"></bean:write></span> </li>
                  <li class="list-information__item"> <span class="list-information__title"><i class="icon stroke icon-Glasses"></i>Duration</span> <span class="list-information__description"><bean:write name="coursedesc" property="duration"></bean:write></span> </li>
                  <li class="list-information__item"> <span class="list-information__title"><i class="icon stroke icon-Edit"></i>Institution </span> <span class="list-information__description"><bean:write name="coursedesc" property="compname"></bean:write></span> </li>
                  <li class="list-information__item"> <span class="list-information__title"><i class="icon stroke icon-Book"></i>Level</span> <span class="list-information__description"><bean:write name="coursedesc" property="level"></bean:write></span> </li>
                  <li class="list-information__item"> <span class="list-information__title"><i class="icon stroke icon-Users"></i>Seats Available</span> <span class="list-information__description"><bean:write name="coursedesc" property="seatsavailable"></bean:write></span> </li>
               
               </ul>
               
              </div>
              <!-- end block_content --> 
            </div>
            <!-- end widget_course-description -->
            
            <div class="widget widget_video">
              <div class="block_content"> <a class="video-link" href="<bean:write name="coursedesc" property="yutube"></bean:write>" rel="prettyPhoto" title="YouTube"> <img class="img-responsive" src="assets/media/video-bg/1.jpg" height="250" width="350" alt="video">
                <div class="video-link__inner"> <i class="icon stroke icon-Next"></i> <span class="video-link__title">course INTRO</span> </div>
                </a> </div>
              <!-- end block_content --> 
            </div>
            <!-- end widget_video -->
            
            <section class="widget widget-default widget_instructor">
              <h3 class="widget-title ui-title-inner decor decor_mod-a">Instructor</h3>
              <div class="block_content">
                <div class="instructor__img"><img src="<%=request.getContextPath()%>/courselogos/<bean:write name="coursedesc"  property="insimage" />" height="60" width="60" alt=""></div>
                <div class="instructor__inner">
                  <div class="instructor__name"><bean:write name="coursedesc" property="instructorname"></bean:write></div>
                  <div class="instructor__categories">Senior teacher</div>
                </div>
              </div>
              <!-- end block_content --> 
            </section>
            <!-- end widget_instructor -->
            
            <section class="widget widget-default widget_social">
              <h3 class="widget-title ui-title-inner decor decor_mod-a">SHARE THIS COURSE</h3>
              <div class="block_content">
                <div class="widget_social__wrap"><img class="img-responsive" src="<%=request.getContextPath()%>/courselogos/<bean:write name="coursedesc"  property="courseimage" />" height="32" width="261" alt="Social"></div>
              </div>
              <!-- end block_content --> 
            </section>
             </logic:iterate>
                </logic:notEmpty>
            
            <!-- end widget_social -->
            
            <section class="widget widget-default widget_courses">
              <h3 class="widget-title ui-title-inner decor decor_mod-a">RELATED COURSES</h3>
              <div class="block_content">
                <ul class="list-courses list-unstyled">
                 <logic:notEmpty name="releatedcourse">
                <logic:iterate id="releatedcourse" name="releatedcourse">
                  <li class="list-courses__item">
                    <section>
                      <div class="list-courses__img"><img class="img-responsive" src="<%=request.getContextPath()%>/courselogos/<bean:write name="releatedcourse"  property="courseimage" />" height="90" width="90" alt="foto"></div>
                      <div class="list-courses__inner">
                        <h4 class="list-courses__title"><bean:write name="releatedcourse" property="coursenamelatest"></bean:write></h4>
                        <div class="list-courses__meta"><bean:write name="releatedcourse" property="instituteName"></bean:write></div>
                        <div class="list-courses__price">PRICE: <span class="list-courses__number"><bean:write name="releatedcourse" property="fees"></bean:write></span></div>
                      </div>
                     
                    </section>
                  </li>
                  </logic:iterate>
                  </logic:notEmpty>
                </ul>
                </div></div>
                
                <div class="col-md-8">
                
                <div id="spy" style="padding:100px 0px 15px 0px;">
 
                <div  id="reviewdetail"></div>
                
                <div id="viewmore"></div>
                
                </div>
                
                
                
                
              </div>
              <div class="col-md-4">
              
              <!-- end block_content --> 
            </section>
            
            <!-- end widget_courses -->
            
            <section class="widget widget-default widget_categories">
              <h3 class="widget-title ui-title-inner decor decor_mod-a">categories</h3>
              <div class="block_content">
                <ul class="list-categories list-unstyled">
                 <logic:notEmpty name="coursecategory">
                <logic:iterate id="coursecategory" name="coursecategory">
                  <li class="list-categories__item"> <a class="list-categories__link" href="DynamicData.do?action=latestcoursesdescription&amp;catid=<bean:write name="coursecategory" property="catid" />"> <span class="list-categories__name"><bean:write name="coursecategory" property="coursecategory"></bean:write></span> <span class="list-categories__number"><bean:write name="coursecategory" property="count"></bean:write></span> </a> </li>
                  </logic:iterate>
                  </logic:notEmpty>
                </ul>
              </div>
              <!-- end block_content --> 
            </section>
            <!-- end widget_categories --> 
            
          </aside>
          <!-- end sidebar --> 
        </div>
        <!-- end col --> 
      </div>
      <!-- end row --> 
    </div>
    <!-- end container -->
    
          <jsp:include page="Commonfooter.jsp"></jsp:include>
     </div>
  <!-- end wrapper --> 
</div>
<!-- end layout-theme --> 


<script>
$(document).ready(function(){
  // Add smooth scrolling to all links in navbar + footer link
  $("a[href='#spy']").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
  
  $(window).scroll(function() {
    $(".slideanim").each(function(){
      var pos = $(this).offset().top;

      var winTop = $(window).scrollTop();
        if (pos < winTop + 600) {
          $(this).addClass("slide");
        }
    });
  });
})
</script>

<!-- SCRIPTS --> 
<script src="assets/js/jquery-migrate-1.2.1.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<script src="assets/js/modernizr.custom.js"></script> 
<script src="assets/js/waypoints.min.js"></script> 

<!--THEME--> 
<script  src="assets/plugins/sliderpro/js/jquery.sliderPro.min.js"></script> 
<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script> 
<script src="assets/plugins/isotope/jquery.isotope.min.js"></script> 
<script src="assets/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script> 
<script src="assets/plugins/datetimepicker/jquery.datetimepicker.js"></script> 
<script src="assets/plugins/jelect/jquery.jelect.js"></script> 
<script src="assets/plugins/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js"></script> 
<script src="assets/js/cssua.min.js"></script> 
<script src="assets/js/wow.min.js"></script> 
<script src="assets/js/custom.min.js"></script> 


</body>
</html>
