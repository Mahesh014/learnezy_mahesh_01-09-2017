<!DOCTYPE html>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<html lang="en">
<head>
 <link href="favicon.ico" type="image/x-icon" rel="shortcut icon">
<link href="assets/css/master.css" rel="stylesheet">

<!-- BOOTSTRAP -->

<link href="assets/css/bootstrap-responsive.css" rel="stylesheet" media="screen">

<!-- FONT AWESOME -->
<link href="assets/css/font-awesome.min.css" rel="stylesheet" media="screen">

<!-- OWL CAROUSEL -->
<link href="assets/css/owl.carousel.css" rel="stylesheet">
<!-- BX SLIDER-->
<link href="assets/css/jquery.bxslider.css" rel="stylesheet" media="screen">
<link href="<%=request.getContextPath() %>/css/jquery-ui.css" rel="stylesheet" media="screen">

<meta charset="utf-8">
<meta name="robots" content="INDEX, FOLLOW">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui">
<meta name="description" content="Learnezy Is An Online Platform For Students Looking Out To Acquire New Skills As well As For Institutes Who Want To Provide Train Students On Those Skills., maximum-scale=1, minimal-ui">
<link rel="canonical" href="www.learnezy.com"/>

<title>LearnEZY | Online and Classroom Courses at one place</title>
<!-- Jquery Lib -->
<script src="<%=request.getContextPath() %>/js/jquery-2.1.1.min.js"></script>
<script src="<%=request.getContextPath() %>/js/additional-methods1.js"></script>
<script src="<%=request.getContextPath() %>/js/validation/jquery.validate.min.js"></script>





<!-- SCRIPTS --> 
<script src="assets/js/jquery-migrate-1.2.1.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<script src="assets/js/modernizr.custom.js"></script> 
<script src="assets/js/waypoints.min.js"></script> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> 
<script type="text/javascript" src="js/NewUI/DynamicIndexPage.js"></script>
<!--THEME--> 
<script  src="assets/plugins/sliderpro/js/jquery.sliderPro.min.js"></script> 
<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script> 
<script src="assets/plugins/isotope/jquery.isotope.min.js"></script> 
<script src="assets/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script> 
<script src="assets/plugins/datetimepicker/jquery.datetimepicker.js"></script> 
<script src="assets/plugins/jelect/jquery.jelect.js"></script> 
<script src="assets/plugins/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js"></script> 
<script src="assets/js/cssua.min.js"></script> 
<script src="assets/js/wow.min.js"></script> 
<script src="assets/js/custom.min.js"></script>


<!-- Bootstrap -->
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/owl.carousel.js"></script>
<script src="assets/js/functions.js"></script>
<script src="assets/js/jquery.bxslider.min.js"></script>
<script src="<%=request.getContextPath() %>/fancyBox/jquery.fancybox.pack.js"></script>
<script src="<%=request.getContextPath() %>/js/auto.js"></script>

<script> 
$(document).ready(function()
		{
$("#searchcourse").autocomplete({
	source : function(request, response) {
		$.ajax({
		
		url : "<%=request.getContextPath() %>/DynamicData.do?action=autoCompleteindexes",
		type : "POST",
		data : {
			term : request.term
			
		},
		dataType : "json",
		async:false,
		
		
		
		success : function(data) {
			
			
	        response(data);
	      }



	});
}
});
		});


$(document).ready(function()
		{
$("#subid").click(function()
		{
	
var course=$("#searchcourse").val();
var city=$("#cityids").val();
if(course!="" && city!="")
{
}
else{

	alert("Enter Course Name and City Name");
	return false;
}

	
var cities;
$.ajax({	
	type: "GET",		
	url:"DynamicData.do?action=getcityss",
	data:{city:city},
	async:false,
	success: function(r){	
		var json= JSON.parse(r);
				$.each(json,function(i,obj){
					//alert(obj.CITY);
					cities=obj.CITY;
		});
	} 
});
$("#searchformid").attr("action",""+course.toLowerCase().replace(/\s/g, "")+"-courses-in-"+cities.toLowerCase().replace(/\s/g, "")+"");






		});
		
	
		});

$(document).ready(function(){
	//the variable to be appended here
	$("#Autom").append("<ul></ul>");

	$.ajax({
	type: "GET",
	url: "sitemap.xml",
	dataType: "xml",
	success: function(xml) {
		$(xml).find('twowheeler').append($('<five>some value</five>'));
}
	});
});







</script>

</head>
<body > 
<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->

<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200"> 
  
  <div id="wrapper"> 
    
    <jsp:include page="Commonheader.jsp"></jsp:include>
    
    <div class="main-content">
      <div id="sliderpro1" class="slider-pro main-slider">
        <div class="sp-slides">
          
          <div class="sp-slide"> <img class="sp-image" src="assets/media/main-slider/3.jpg"
					data-src="assets/media/main-slider/3.jpg"
					data-retina="assets/media/main-slider/3.jpg" alt="img"/>
            <div class="item-wrap sp-layer  sp-padding" data-horizontal="200" data-vertical="30"
					data-show-transition="left" data-hide-transition="up" data-show-delay="400" data-hide-delay="200">
              <div class="main-slider__inner">
                <div class="main-slider__title" ><h1>BEST ONLINE LEARNING</h1></div>
                <div class="main-slider__subtitle ">THE EASIER WAY</div>
                <a class="main-slider__btn btn btn-warning btn-effect" href="<%=request.getContextPath() %>/student-registration.jsp">START A COURSE</a> </div>
            </div>
          </div>
          
          <div class="sp-slide"> <img class="sp-image" src="assets/media/main-slider/4.jpg"
					data-src="assets/media/main-slider/4.jpg"
					data-retina="assets/media/main-slider/4.jpg" alt="img"/>
            <div class="item-wrap sp-layer  sp-padding" data-horizontal="700" data-vertical="1"
					data-show-transition="left" data-hide-transition="up" data-show-delay="400" data-hide-delay="200">
              <div class="main-slider__inner text-center">
                <div class="main-slider__title">BEST ONLINE LEARNING</div>
                <div class="main-slider__subtitle">THE EASIER WAY</div>
                <a class="main-slider__btn btn btn-warning btn-effect" href="<%=request.getContextPath() %>/student-registration.jsp">START A COURSE</a> </div>
            </div>
          </div>
          
          
        </div>
      </div>
      <!-- end main-slider -->
      
      <div class="section_find-course wow fadeInUp" data-wow-duration="2s">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <div class="find-course find-course_mod-b">
                <html:form action="DynamicData.do?action=search" styleId="searchformid" styleClass="find-course__form">
                  <div class="form-group"> <i class="icon stroke icon-Search"></i>
                    <input class="form-control" type="text" id="searchcourse" placeholder="Course Keyword ..." name="courseName">
                    <div class="jelect" >
                      
                       <html:select property="cityid" styleId="cityids"  styleClass="jelect-option1 jelect-option_state_active1">
                      <div tabindex="0" role="button" class="jelect-current">All Locations</div>
      
                   
					<html:option value="">--Select City--</html:option>
					<html:options collection="cityList" property="cityid" labelProperty="cityname" />
					</html:select>
                    
                    </div>
                    <!-- end jelect --> 
                  </div>
                  <!-- end form-group -->
                  <div class="find-course__wrap-btn">
                    <input type="submit" id="subid" class="btn btn-info btn-effect" name="SEARCH COURSE"/>
                  </div>
                </html:form>
              </div>
              <!-- end find-course -->
              <div class="find-course__info">Search more than 50,000 online courses available from 2,000 Academies</div>
            </div>
            <!-- end col --> 
          </div>
          <!-- end row --> 
        </div>
        <!-- end container --> 
      </div>
      <!-- end section-default -->
      
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="border-decor_top">
              <section class="section-default">
                <div class="wrap-title wow zoomIn" data-wow-duration="2s">
                  <h2 class="ui-title-block ui-title-block_mod-d">We Bring You World's Best Courses From All Top Training Academies.</h2>

                  <div class="ui-subtitle-block ui-subtitle-block_mod-c">Having over 9 million students worldwide and more than 50,000 online courses available.</div>
                </div>
                <!-- end wrap-title -->
                <ul class="advantages advantages_mod-b list-unstyled">
                  <li class="advantages__item wow zoomIn" data-wow-duration="2s">
                    <div class="advantages__inner">
                      <div class="advantages__info"> <span class="advantages__icon"><i class="icon stroke icon-Cup"></i></span>
                        <h3 class="ui-title-inner decor decor_mod-a">HIGHEST RATED</h3>
                      </div>
                    </div>
                    <p style="text-align:justify;">Courses at LearnEzy are amongst the Top Rated Online Courses in various categories on the internet.</p>
                  </li>
                  <li class="advantages__item wow zoomIn" data-wow-duration="2s" data-wow-delay=".5s"> <span class="advantages__icon"><i class="icon stroke icon-Users"></i></span>
                    <div class="advantages__inner">
                      <h3 class="ui-title-inner decor decor_mod-a">SKILLED FACULTY</h3>
                      <div class="advantages__info">
                        <p style="text-align:justify;">Courses offered at LearnEzy by various Academy are created by skilled professionals and from best faculties.</p>
                      </div>
                    </div>
                  </li>
                  <li class="advantages__item wow zoomIn" data-wow-duration="2s" data-wow-delay="1s"> <span class="advantages__icon"><i class="icon stroke icon-WorldGlobe"></i></span>
                    <div class="advantages__inner">
                      <h3 class="ui-title-inner decor decor_mod-a">we are GLOBAL</h3>
                      <div class="advantages__info">
                        <p style="text-align:justify;">Our Courses are not just limited to a certain City, State or Country. We have created courses that helps student irrespective of their countries/locations. We are indeed global.</p>
                      </div>
                    </div>
                  </li>
                  <li class="advantages__item wow zoomIn" data-wow-duration="2s" data-wow-delay="1.5s"> <span class="advantages__icon"><i class="icon stroke icon-DesktopMonitor"></i></span>
                    <div class="advantages__inner">
                      <h3 class="ui-title-inner decor decor_mod-a">ONLINE TRAINING</h3>
                      <div class="advantages__info">
                        <p style="text-align:justify;">With LearnEzy you can access various courses from various academies from anywhere anytime. That is the beauty of Online Training.</p>
                      </div>
                    </div>
                  </li>
                </ul>
              </section>
              <!-- end section-advantages --> 
            </div>
            <!-- end border-decor_top --> 
          </div>
          <!-- end col --> 
        </div>
        <!-- end row --> 
      </div>
      <!-- end container -->
      
      
      
      
       
      
      <div class="section-progress wow fadeInUp section-parallax"  data-speed="25"  data-wow-duration="2s">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <ul class="list-progress list-unstyled">
              <logic:notEmpty name="dynamicnumbers">
              <logic:iterate id="dynamic" name="dynamicnumbers">
                <li class="list-progress__item"> <span class="chart label-chart decor decor_mod-c" data-percent=<bean:write name="dynamic" property="academycount" />><i class="icon stroke icon-WorldWide"></i><span class="percent"></span> </span> <span class="list-progress__name">ACADEMIES</span> </li>
                <li class="list-progress__item"> <span class="chart label-chart decor decor_mod-c" data-percent=<bean:write name="dynamic" property="coursecatcount" />><i class="icon stroke icon-Book"></i><span class="percent"></span> </span> <span class="list-progress__name">COURSE CATEGORIES</span> </li>
                <li class="list-progress__item"> <span class="chart label-chart decor decor_mod-c" data-percent=<bean:write name="dynamic" property="coursecount" />><i class="icon stroke icon-User"></i><span class="percent"></span> </span> <span class="list-progress__name">COURSES PUBLISHED</span> </li>
                <li class="list-progress__item"> <span class="chart label-chart decor decor_mod-c" data-percent=<bean:write name="dynamic" property="studentcount" />><i class="icon stroke icon-Edit"></i><span class="percent"></span> </span> <span class="list-progress__name">STUDENTS</span> </li>
             </logic:iterate>
             </logic:notEmpty>
             
              </ul>
              <!-- end list-progress --> 
            </div>
            <!-- end col  --> 
          </div>
          <!-- end row  --> 
        </div>
        <!-- end container--> 
      </div>
      <!-- end section-progress-->
      
      
      
      
      
       <!--LATEST COURSES SECTION START-->
        <section class="gray-bg">
        	<div class="container">
            	<!--SECTION HEADER START-->
            	<div class="sec-header">
                <h2 class="ui-title-block ui-title-block_mod-e">Latest <strong> Courses</strong></h2>
              <div class="wrap-subtitle">
                <div class="ui-subtitle-block ui-subtitle-block_w-line">Check Our Featured Courses</div>
              </div>
                	
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <!--SECTION HEADER END-->
                <div class="customNavigation">
                    <a class="btn prev"><i class="fa fa-angle-left"></i></a>
                    <a class="btn next"><i class="fa fa-angle-right"></i></a>
                </div>
                <div id="owl-carousel" class="owl-carousel owl-theme">
                	<!--COURSE ITEM START-->
                	<logic:notEmpty name="LatestCourseList">
                	<logic:iterate id="userrole" name="LatestCourseList" >
				
                    <div class="course">
                    	<div class="thumb">
                        	<a href="#"><img src="<%=request.getContextPath()%>/imageUpload/<bean:write name="userrole"  property="courselatestlogo" />" alt=""></a>
                            <div class="price"><span><i class="fa fa-inr"></i></span><bean:write name="userrole" property="amount" /></div>
                        </div>
                    	<div class="text">
                        	<div class="head">
                            	<h4><bean:write name="userrole" property="coursecategory" /></h4>
                                <div class="rating">
                                 <img src="<bean:write name="userrole" property="rating" />" />
                                </div>
                            </div>
                            <div class="course-name">
                            	<p><bean:write name="userrole" property="coursenamelatest" /></p>
                                <span><bean:write name="userrole" property="amount" /></span>
                            </div>
                             <div class="course-detail-btn">
                            <input type="button" class="cartclass buttonstyle" id="<bean:write name="userrole" property="courseId" />" value="Add Cart">                              
                            <input type="submit" class="courseName buttonstyle" id="<bean:write name="userrole" property="courseId" />" value="Details">
                            </div>
                        </div>
                    </div>
                    </logic:iterate>
				     </logic:notEmpty>
                    <!--COURSE ITEM END-->
                  
                </div>
            </div>
        </section>
        <!--LATEST COURSES SECTION END-->
      
         
      
                    
      <section class="">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <div class="section__inner">
                   <!--COURSES TOPIC SECTION START-->
        <section class="gray-bg tabs-section">
        	<div class="container">
            	<!--SECTION HEADER START-->
            	<div class="sec-header">
                 <h2 class="ui-title-block ui-title-block_mod-e">Course <strong>Categories</strong></h2>
              <div class="wrap-subtitle">
                <div class="ui-subtitle-block ui-subtitle-block_w-line">Discover courses by topic</div>
              </div>
                	
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <!--SECTION HEADER END-->
                
            </div>


            
            <div class="course-tabs">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                <logic:notEmpty name="LatestCourseCategory">
                <logic:iterate id="userrole" name="LatestCourseCategory">
                  <li><a href="DynamicData.do?action=dynamiccourse&NAME=<bean:write name="userrole" property="coursecatid"/>"><i class="fa fa-book"></i><bean:write name="userrole"  property="coursecatname"/></a></li>
                  </logic:iterate>
                  </logic:notEmpty>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content container">
                  <div id="web" class="tab-pane fade active in">
                    <ul class="course-topics row">
                    	<logic:notEmpty name="courselist">
                    	<logic:iterate id="courseid" name="courselist">
                    	<li class="span4">
                        	<div class="thumb" id="importantvalue">
                            	<a href="#"><img src="<%=request.getContextPath()%>/courselogos/<bean:write name="courseid"  property="imagename" />" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4><bean:write name="courseid" property="coursenamenew" /></h4>
                                <span><bean:write name="courseid" property="amountnew" /></span>
                                <input type="submit" class="courseNameAnother buttonstyle" id="<bean:write name="courseid" property="courseidnew" />" value="Details">
                                
                            </div>
                            
                        </li>
                        </logic:iterate>
                         </logic:notEmpty>
                        <!--LIST ITEM START-->
                     
                       
                        <!--LIST ITEM START-->
                        
                        <!--LIST ITEM START-->
                    </ul>
                  </div>
                 
                  
                </div>
            </div>
        </section>
        <!--COURSES TOPIC SECTION END-->
      
      
                
              </div>
            </div>
            <!-- end col --> 
            
          </div>
          <!-- end row --> 
        </div>
        <!-- end of container -->
      </section>
      <!-- end section_mod-d -->
      
      
     
      <section class="section_mod-c wow bounceInUp section-categories" data-wow-duration="2s" >
        <div class="container" id="academydisplay">
          <div class="row">
            <div class="col-xs-12">
              <h2 class="ui-title-block ui-title-block_mod-e">Our <strong>Academies</strong></h2>
              <div class="wrap-subtitle">
                <div class="ui-subtitle-block ui-subtitle-block_w-line">We have very skilled and professionals for taking classes</div>
              </div>
              <div class="carousel_mod-a owl-carousel owl-theme enable-owl-carousel"
											data-min480="2"
											data-min768="3"
											data-min992="4"
											data-min1200="4"
											data-pagination="true"
											data-navigation="false"
											data-auto-play="4000"
											data-stop-on-hover="true" >
											 <logic:notEmpty name="companyList">
    
    	<logic:iterate id="userrole" name="companyList" >
											
                <div class="staff">
                  <div class="staff__media"><img class="img-responsive" src="<%=request.getContextPath()%>/imageUpload/<bean:write name="userrole"  property="logo" />" height="250" width="270" alt="foto">
                    <div class="staff__hover"></div>
                  </div>
                  <div class="staff__inner">
                    <h3 class="staff__title"><bean:write name="userrole" property="compname" /></h3>
                    <div class="staff__categories"> <a class="post-link" href="javascript:void(0);"><bean:write name="userrole" property="address"/></a></div>
                    <div class="staff__description"><bean:write name="userrole"  property="countryid"></bean:write></div>
                   
                  </div>
                </div>
           
                </logic:iterate>
                </logic:notEmpty>
                
              </div>
            </div>
            <!-- end col --> 
          </div>
          <!-- end row --> 
        </div>
        <!-- end container --> 
      </section>
      <!-- end section_mod-c -->
                    
      <div class="container">
        <div class="border-decor_bottom">
          <div class="section-default">
            <div class="row">
              <div class="col-md-4 wow bounceInRight" data-wow-duration="2s" data-wow-delay="1.4s">
                <div class="title-w-icon"> <i class="icon stroke icon-Book"></i>
                  <h2 class="ui-title-inner">POPULAR COURSES</h2>
                </div>
                
                <logic:notEmpty name="popularlist">
                <logic:iterate id="popularlist" name="popularlist">
                <article class="post post_mod-g clearfix">
                  <div class="entry-media">
                    <div class="entry-thumbnail"> <a href="javascript:void(0);" ><img class="img-responsive" src="<%=request.getContextPath()%>/complogos/<bean:write name="popularlist"  property="popularlogo" />" width="100" height="90" alt="Foto"/></a> </div>
                  </div>
                  <div class="entry-main">
                    <h3 class="entry-title entry-title_mod-a"><a href="javascript:void(0);"><bean:write name="popularlist"  property="popularcourse" /></a></h3>
                    <div class="entry-meta"> <span class="entry-autor"> <span>By </span> <a class="post-link" href="javascript:void(0);"><bean:write name="popularlist"  property="popularcompname" /></a> </span>  </div>
                    <div class="entry-footer">
                      <ul class="rating">
                        <li><img src="<bean:write name="popularlist" property="popularrating" />" /></li>
                     
                      </ul>
                    </div>
                  </div>
                
                </article>
                </logic:iterate>
                </logic:notEmpty>
                
                <!-- end post -->
               
                <!-- end post --> 
              </div>
              <!-- end col --> 
              
              <div class="col-md-4 wow bounceInRight courses" data-wow-duration="2s" data-wow-delay=".7s">
                <div class="title-w-icon"> <i class="icon stroke icon-User"></i>
                  <h2 class="ui-title-inner">ITECH TRAINING ACADEMY</h2>
                </div>
                <article class="post post_mod-f">
                  <div class="entry-media">
                    <div class="entry-thumbnail"><img class="img-responsive" src="assets/media/posts/160x125/1.jpg" width="160" height="125" alt="Foto"> </div>
                  </div>
                  <div class="entry-main">
                    <h3 class="entry-title entry-title_mod-a"><a href="javascript:void(0);">ANDREW FINCH</a></h3>
                    <div class="entry-meta"><a class="post-link" href="javascript:void(0);">Developer</a></div>
                    <div class="entry-content">
                      <p>Phasellus luctus pulvin g bienum aliquam lig sape conde lorem ipsum dolor met cons letua.</p>
                      <p>Eeniam quis nostrud exercitation ullamco labor nisut alq gxe commodo consequat duis aute re dola. Lorem ipsum dolor sit amt consectetur adipisng elit.</p>
                    </div>
                  </div>
                  <a href="javascript:void(0);" class="post-btn btn btn-primary btn-effect">FULL PROFILE</a> </article>
                <!-- end post --> 
              </div>
              <!-- end col -->
              
              <div class="col-md-4 wow bounceInRight" data-wow-duration="2s" data-wow-delay="1.4s">
                <div class="title-w-icon"> <i class="icon stroke icon-Book"></i>
                  <h2 class="ui-title-inner">RECENT COURSES</h2>
                </div>
                 <logic:notEmpty name="recentlist">
                <logic:iterate id="popularlist" name="recentlist">
                <article class="post post_mod-g clearfix">
                   <div class="entry-media">
                    <div class="entry-thumbnail"> <a href="javascript:void(0);" ><img class="img-responsive" src="<%=request.getContextPath()%>/complogos/<bean:write name="popularlist"  property="popularlogo" />" width="100" height="90" alt="Foto"/></a> </div>
                  </div>
                  <div class="entry-main">
                    <h3 class="entry-title entry-title_mod-a"><a href="javascript:void(0);"><bean:write name="popularlist"  property="popularcourse" /></a></h3>
                    <div class="entry-meta"> <span class="entry-autor"> <span>By </span> <a class="post-link" href="javascript:void(0);"><bean:write name="popularlist"  property="popularcompname" /></a> </span>  </div>
                    <div class="entry-footer">
                      <ul class="rating">
                        <li><img src="<bean:write name="popularlist" property="popularrating" />" /></li>
                     
                      </ul>
                    </div>
                  </div>
                  <!-- end entry-main --> 
                </article>
                <!-- end post -->
                      </logic:iterate>
                </logic:notEmpty>
              
               
              </div>
              <!-- end col --> 
            </div>
            <!-- end row --> 
          </div>
          <!-- end section-default --> 
        </div>
        <!-- end border-decor_bottom --> 
      </div>
      <!-- end container -->
      
      <div class="section-subscribe wow fadeInUp" data-wow-duration="2s">
        <div class="subscribe">
          <div class="container">
            <div class="row">
              <div class="col-sm-6">
                <div class="subscribe__icon-wrap"> <i class="icon_bg stroke icon-Imbox"></i><i class="icon stroke icon-Imbox"></i> </div>
                <div class="subscribe__inner">
                  <h2 class="subscribe__title">STAY UPDATED WITH LEARNEZY</h2>
                  <div class="subscribe__description">Nulla feugiat nibh placerat fermentum rutrum ante risus euismod</div>
                </div>
              </div>
              <!-- end col -->
              <div class="col-sm-6">
                <html:form styleClass="subscribe__form" action="DynamicData.do?action=subscribe">
                 <logic:notEmpty name="status">
					<font color="red"><bean:write name="status"></bean:write></font>
				</logic:notEmpty>
                  <input class="subscribe__input form-control" type="email" placeholder="Your Email address ..." name="subscribeemail" required>
                  <button type="submit" class="subscribe__btn btn btn-success btn-effect">SUBSCRIBE</button>
               </html:form>
              </div>
              <!-- end col --> 
            </div>
            <!-- end row --> 
          </div>
          <!-- end container --> 
        </div>
      </div>
      <!-- end section-subscribe -->
    
    
     <div class="container">
        <div class="row">
          <div class="border-decor_top">
            <div class="col-md-6">
              <section class="section-default wow bounceInLeft" data-wow-duration="2s">
                <h2 class="ui-title-block">What <strong>Students Say</strong></h2>
                <div class="slider-reviews owl-carousel owl-theme owl-theme_mod-c enable-owl-carousel"
												data-single-item="true"
												data-auto-play="7000"
												data-navigation="true"
												data-pagination="false"
												data-transition-style="fade"
												data-main-text-animation="true"
												data-after-init-delay="4000"
												data-after-move-delay="2000"
									
												data-stop-on-hover="true">
												 <logic:notEmpty name="reviewList">
                  <logic:iterate id="review" name="reviewList">
                  <div class="reviews">
                 
                    <div class="reviews__text"><bean:write name="review" property="studentreview"/> .</div>
                    <div class="reviews__img"><img class="img-responsive" src="<%=request.getContextPath()%>/imageUpload/<bean:write name="review"  property="studentphoto" />" height="60" width="60" alt="foto"></div>
                    <span class="reviews__autor">-- <bean:write name="review" property="studentname"/></span> <span class="reviews__categories"><bean:write name="review" property="studentcourse"/></span> 
                  <!-- end reviews -->
                 
                  </div>
                 </logic:iterate>
                  </logic:notEmpty>
                </div>
                <!-- end slider-reviews --> 
              </section>
              <!-- end section-default --> 
            </div>
            <!-- end col -->
            
            <div class="col-md-6">
              <section class="section-default wow bounceInRight courses" data-wow-duration="2s">
                <h2 class="ui-title-block">Why <strong>Learnezy ?</strong></h2>
                <div class="panel-group accordion accordion" id="accordion-1">
                  <div class="panel panel-default">
                    <div class="panel-heading"> <a class="btn-collapse" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-1"><i class="icon"></i></a>
                      <h3 class="panel-title">Learn Anytime & Anywhere</h3>
                    </div>
                    <div id="collapse-1" class="panel-collapse collapse in">
                      <div class="panel-body">
                        <p class="ui-text"> Learn from the comfort of your home, whenever you feel like studying. No Timings, No Restrictions. You get training in the form of Videos, Audios, Text, Images and even interactive classes/courses.</p>
                      </div>
                    </div>
                  </div>
                  <!-- end panel -->
                  
                  <div class="panel">
                    <div class="panel-heading"> <a class="btn-collapse collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-2"><i class="icon"></i></a>
                      <h3 class="panel-title">Exclusive Range Of Courses</h3>
                    </div>
                    <div id="collapse-2" class="panel-collapse collapse">
                      <div class="panel-body">
                        <p class="ui-text"> Students get exclusive range of courses from several Institutions to choose from.</p>
                      </div>
                    </div>
                  </div>
                  <!-- end panel -->
                  
                  <div class="panel">
                    <div class="panel-heading"> <a class="btn-collapse collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-3"><i class="icon"></i></a>
                      <h3 class="panel-title">Practicals, Assessment & Evaluations</h3>
                    </div>
                    <div id="collapse-3" class="panel-collapse collapse">
                      <div class="panel-body">
                        <p class="ui-text"> Along with lectures, you can get to practice whatever you have learnt, For Instance, There is a compiler in website itself if you want to try out whatever you have learnt in programming.
 Assessments and Evaluation Tests to evaluate how effectively you are learning.
</p>
                      </div>
                    </div>
                  </div>
                  <!-- end panel -->
                  
                  <div class="panel">
                    <div class="panel-heading"> <a class="btn-collapse collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-4"><i class="icon"></i></a>
                      <h3 class="panel-title">Basic to Advance: We Teach Everything</h3>
                    </div>
                    <div id="collapse-4" class="panel-collapse collapse">
                      <div class="panel-body">
                        <p class="ui-text">LearnEZY offers courses for everyone, may it be beginner or an advanced level professional. We have courses for almost everyone</p>
                      </div>
                    </div>
                  </div>
                  <!-- end panel --> 
                </div>
                <!-- end accordion --> 
              </section>
            </div>
            <!-- end col --> 
          </div>
          <!-- end section-default --> 
        </div>
        <!-- end row --> 
      </div>
      <!-- end container -->
    
    <!--  <div class="tweets" style="background:#6BC868;">
            <div class="container">
            	<div class="tweet-contant">
                	<i class="fa fa-twitter"></i>
                    <h4>Weekly Updates</h4>
                    <ul class="bxslider">
                        <li>
                            <p>Are you a morning person or is the night time the right time? Interesting perspectives on the forum - <a href="#">http://t.co/tdEHlbZf</a></p>
                        </li>
                        <li>
                            <p>Dolor donec sagittis sapien. Ante aptent feugiat adipisicing. Duis int. - <a href="#">http://t.co/tdEHlbZf</a></p>
                        </li>
                        <li>
                            <p>Duis interdum olor donec sagittis sapien. Ante aptent feugiat adipisicing - <a href="#">http://t.co/tdEHlbZf</a></p>
                        </li>
                        
                    </ul>
                </div>
            </div>
        </div>--> 
    
    
    
      
    </div>
    <!-- end main-content -->
    
   <jsp:include page="Commonfooter.jsp"></jsp:include>
  </div>
  <!-- end wrapper --> 
</div>
<!-- end layout-theme --> 


</body>
</html>
