
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<html>
<head>
<title>Itech Training :: Change Password</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/cmsstyle.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/carousel.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>
<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
<script type="text/javascript">
 $(document).ready(function() {
	
 
	 $("#validateRegister").click( function() {
		 var currpass=$("#currpass");
		 var newpass=$("#newpass");
		 var confpass=$("#confpass");
		 	if(currpass.val()==""){
				alert("Current cannot be empty");
				currpass.focus();
				return false;
			}else
			if(newpass.val()==""){
				alert("New Password cannot be empty");
				newpass.focus();
				return false;
			}else{
				var decimal=  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;  
				var passwords=newpass;
				if(passwords.val().match(decimal)){  
					$("#passDiv").html("");
					$("#confDiv").html("");
				}else{
					passwords.focus();
					passwords.val("");
					$("#confpass").val("");
					$("#passDiv").html('Password should contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character and min 8 characters');
					return false;
				}  
			}
				
			if(confpass.val()=="" ){
				alert("Confirm Password cannot be empty ");
				confpass.focus();
				return false;
			}else
			if(newpass.val()!=confpass.val()){
				alert("Confirm Password Not Match");
				return false;
			}
	 });
	$("#newpass").change( function() {
		var decimal=  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;  
		var passwords=$(this);
		if(passwords.val().match(decimal)){  
			$("#confpass").val("");
			$("#passDiv").html("");
			$("#confDiv").html("");
		}else{
			passwords.focus();
			passwords.val("");
			$("#confpass").val("");
			$("#passDiv").html('Password should contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character and min 8 characters');
			return false;
		}  
	});

});
</script>
</head>
<body>
    <!-- Start Header -->
   <jsp:include page="/jsp/common/student_header.jsp"></jsp:include>
   <!-- End Header -->
   <!-- Start Main Content -->
	 <div class="main" style="min-height:450px;">	 
	 <div id="status" align="center"><logic:present name="status">
		<font color="red"> <bean:write name="status" /></font>
	</logic:present></div>
	 	<h2>Change Password</h2>
    
    <table border="0" width="40%" align="center"  class="role">
		<col style="width: 60%;" />
		<col style="width: 40%;" />
		
        <html:form action="ChangePassword.do?action=update" method="post">
		<tr >
			<td><label>Current Password:</label></td>
			<td><input type="password" id="currpass" name="currpass" value=""   /></td>
        </tr>
        <tr>
			<td><label>New Password:</label></td>
			<td align="right" ><input type="password" id="newpass" name="newpass" value="" /><span style="font-size: 10px;font-style: italic;color: red;" title="Password should contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character and min 8 characters" >Rules</span>
		 </td>
		</tr>
		<tr>
			<td><label>Confirm Password:</label></td>
			<td><input type="password" id="confpass" name="confpass" value="" />
			</td>
		</tr>
        <tr>
			<td colspan="2"><div id="passDiv" style="font-size: 12px;font-style: italic;color: red;" ></div>
       		</td>
		</tr>
		 
        <tr >
        <td colspan="2" align="center">
         <input type="submit" name="btn" value="Submit" id="validateRegister" style="padding:0.5% 1%;" />
         </td>
        </tr>
        </html:form>
						
	</table>
   
     </div>
   
   <!-- End Main Content -->
	   
   <!-- Start Footer -->
   <jsp:include page="/jsp/common/masterdata_footer.jsp"></jsp:include> 
  <!-- End Footer -->
  </body>
</html>


    	
    	
            