<!DOCTYPE HTML>
<html>
<head>
<title>Itech Training :: Admin</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300'
	rel='stylesheet' type='text/css'>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/cmsstyle.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/font-awesome.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/carousel.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css"
	media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />


<script src="js/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="js/bootstrap.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/owl.carousel.js" type="text/javascript"></script>
 
<script type="text/javascript">
	// Login Form
	$(function() {
		var button = $('#loginButton');
		var box = $('#loginBox');
		var form = $('#loginForm');
		button.removeAttr('href');
		button.mouseup(function(login) {
			box.toggle();
			button.toggleClass('active');
		});
		form.mouseup(function() {
			return false;
		});
		$(this).mouseup(function(login) {
			if (!($(login.target).parent('#loginButton').length > 0)) {
				button.removeClass('active');
				box.hide();
			}
		});
	});

	function go() {
		alert("hai");
	}
</script>
</head>
<body>
<%@page import="java.util.Date"%>
 <%@page import="com.itech.elearning.forms.LoginForm" %>
<%
	  												Object photo="";
                                                	Object userName="";
	 														 try{
	 															//photo=((LoginForm) session.getAttribute("userDetail")).getProfilePhoto();;
	 															userName=((LoginForm) session.getAttribute("userDetail")).getFirstname();;
	 															 }catch(NullPointerException e){
    	  																		e.printStackTrace();
      															}
	 														%> 
	<!-- Start Header -->
	<div class="header">
		<div class="header-top">
			<div class="wrap">
				<div class="header-top-left">
					<p>ph: +91-9611421111</p>
				</div>
				<div class="header-top-right">
					<ul>
						<li><a href="#" >Careers</a></li>
						<li class="login">
							<div id="loginContainer">
								<a href="#" id="loginButton"><span>Join Us</span></a>
								<div id="loginBox" class="login-form">
									<h3>Login into Your Account</h3>
									<form id="loginForm">
										<span> <i><img src="images/user.png" alt="" /></i> <input
											type="text" value="yourname@mail.com"
											onFocus="this.value = '';"
											onBlur="if (this.value == '') {this.value = 'yourname@mail.com';}">
										</span> <span> <i><img src="images/lock.png" alt="" /></i> <input
											type="password" value="........." onFocus="this.value = '';"
											onBlur="if (this.value == '') {this.value = '.........';}">
										</span> <input type="submit" value="Login">
									</form>
								</div>
							</div>
						</li>
						<li><a href="#">Sitemap</a></li>
					</ul>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<div class="header-logo-nav">
			<div class="navbar navbar-inverse navbar-static-top nav-bg"
				role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse"
							data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span> <span
								class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>
						</button>
						<div class="logo">
							<a class="navbar-brand" href="dynamic.jsp"><img
								src="images/LEARNEZYLatest-LOGO.png" alt="" /></a>
						</div>
						<div class="clear"></div>

					</div>

					<div class="collapse navbar-collapse">
						<ul class="menu nav navbar-nav">
							 
							<li><a href="Logout.do"><img src="images/logout.png"
									title="Logout"></a></li>
						</ul>
					</div>
					<!--/.nav-collapse -->
				</div>
			</div>
			<div class="clear"></div>
			<div class="header-bottom">
				<div class="container">
					<div class="time">
						 <p><div id="clockbox"></div></p>
                 <script src="js/dynamicClock.js" type="text/javascript"></script>
					</div>
					<div class="homepage">
						<h3>Admin Home page</h3>
					</div>
					<div class="welcome">
						  <h3 align="right">Welcome: <%=userName %></h3>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>


		<div class="clear"></div>
	</div>
	<!-- End Header -->
	<div class="clear"></div>
	<!-- Start Main Content -->
	<div class="main">

		<!--  <div class="admin">
	      <ul>
          <li class="box effect8">Role Master</li>
          <li class="box effect8">User Master</li>
          <li class="box effect8">Course Master</li>
          </ul>
          </div>
            <div class="admin">
          <ul>
          
          <li class="box effect8">Topic Master</li>
          <li class="box effect8">Question Master New</li>
          <li class="box effect8">Pass Percent Master</li>
          </ul>
	 </div>-->
		<!--<table width="960" class="admin1">
     <tr>
     <td class="box effect8">Role Master</td>
     <td class="box effect8">User Master</td>
     <td class="box effect8">Course Master</td>
     </tr>
     </table>
     <table width="960" class="admin1">
     <tr>
     <td class="box effect8">Topic Master</td>
     <td class="box effect8">Question Master</td>
     <td class="box effect8">Pass Percent Master</td>
     </tr>
     </table>-->
		<div class="adminmaster">
			<div class="admin">
				<div class="box effect8">
					<a href="Login.do?action=admhome"> Master Data</a>
				</div>
				<div class="box effect8"><a href="ReportsHome.do">Reports</a>
				</div>
				 
				<div class="box effect8">
					<a href="NotificationStudent.do">Student Notification</a>
				</div>
				 <div class="box effect8">
					<a href="Appointment.do">Appointment</a>
				</div>
				
				<div class="box effect8">
					<a href="AppointmentSchedule.do">Appointment Schedule</a>
				</div>
				
				<div class="box effect8">
					<a href="WhiteBoard.do">Appointment List</a>
				</div>
			</div>
			<div class="clear"></div>

		</div>
	</div>
	<!-- admin master -->
	<div class="clear"></div>

	<!-- End Main Content -->

	<!-- Start Footer -->


	<div class="footer-bottom" style="position: absolute;">
		<div class="wrap">
			<div class="copy-right">
				<p>
					Itech Training, Copyright 2015, All Rights Reserved. Designed by <a
						href="http://www.itechtraining.in/" target="_blank">Itech
						Solutions</a>
				</p>
			</div>
			<div class="social-icons">
				<ul>
					<li><a target="_blank" href="https://www.twitter.com"><i
							class="fa fa-twitter"></i></a></li>
					<li><a target="_blank" href="https://www.facebook.com"><i
							class="fa fa-facebook"></i></a></li>
					<li><a target="_blank" href="https://plus.google.com"><i
							class="fa fa-google-plus"></i></a></li>
					<li><a target="_blank" href="https://www.youtube.com"><i
							class="fa fa-youtube"></i></a></li>
					<li><a target="_blank" href="https://www.linkedin.com"><i
							class="fa fa-linkedin"></i></a></li>
				</ul>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<!-- End Footer -->
</body>
</html>



