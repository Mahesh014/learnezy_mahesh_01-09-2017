<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-nested.tld" prefix="nested"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Itech Training :: Notifications Stduent</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv='cache-control' content='no-cache'>
<meta http-equiv='expires' content='0'>
<meta http-equiv='pragma' content='no-cache'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300'	rel='stylesheet' type='text/css'>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"	media="all" />
<link href="css/cmsstyle.css" rel="stylesheet" type="text/css"	media="all" />
<link href="css/font-awesome.css" rel="stylesheet" type="text/css"	media="all" />
<link href="css/carousel.css" rel="stylesheet" type="text/css"	media="all" />
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css"	media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="src/DateTimePicker.css" />


<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="src/DateTimePicker.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/date/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css"	href="<%=request.getContextPath()%>/date/jquery-ui.css" />
<link href="dataTable/css/jquery.dataTables_themeroller.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.min.css"	rel="stylesheet" type="text/css" media="all" />
<script src="dataTable/js/jquery.dataTables.js"	type="text/javascript"></script>
<script src="dataTable/js/jquery.dataTables.min.js"	type="text/javascript"></script>
<script src="dataTable/js/settingDataTable.js" 	type="text/javascript"></script>

<link rel="stylesheet" type="text/css"	href="/E-Learning/date/jquery-ui.css" />
<script type="text/javascript" src="/E-Learning/date/jquery-ui.js"></script>

<!-- Text Editor -->
<script type="text/javascript">
$(function(){
    $( "#expiryDate" ).datepicker({minDate: 0,dateFormat: 'yy-mm-dd'});
	 
});
	function valueCall(){
		if(document.forms[0].notification.value == "" )	{
			alert("Please Enter the Notification ..!");
			document.forms[0].notification.focus();
			return false;
		}else
		if(document.forms[0].expiryDate.value == "") {
			alert("Please Enter the Expiry Date ..!");
			document.forms[0].expiryDate.focus();
			return false;
		}else
		if ($("#courseId").val() ==null) {
			alert("Please select at least one course");
			return false;
		}else
		if(document.getElementById("studentList").innerHTML==""){
			alert("Please Select Student!");
			return false;
		}
	}
	
	function edit(nId,uid,nuti,expDate){
		alert(nId);
		$("#notification").val(nuti);
		$("#expiryDate").val(expDate);
		document.forms[0].notificationID.value =nId;
		document.forms[0].userid.value =uid;
		document.forms[0].notification.value =nuti;
		document.forms[0].action="NotificationStudent.do?action=update";
		document.forms[0].btn.value="Update";
		document.forms[0].userid.focus();
	}

function listMetodCall()
{
	document.forms[0].action="NotificationStudent.do?action=list";
	document.forms[0].submit();
}

function deleteNotification(notifID){
	//alert(notifID);
	var conf=confirm("This will permanently delete this Notification, are you sure you want to Remove?");
	if(conf){
		document.forms[0].action="NotificationStudent.do?action=changestatus&notificationID="+notifID;
		document.forms[0].submit();
	}
	
	
}
</script>


<script type="text/javascript">
$(document).ready(function() {

	   
	
	
$("#courseId").click( function() {
	var courseId=$(this).val();
     	$.ajax({
            url: "NotificationStudent.do?action=studentListByCourse&courseId1="+courseId,
            type: "GET",
            dataType: "json",
            success: function(res) {
            	var studentList='';
            	$.each(res,function(i,obj){
            		
            		studentList=studentList+'<li class="role1li"><label class="role1label" for="chk1">&nbsp;<input type="checkbox" class="studentId" name="studentId" value="'+obj.studentId+'" /> &nbsp;&nbsp; '+obj.studentName+'</label></li>';
 				});
				if(studentList==""){
					studentList="Selected Course not have any student";
				}
 				
                $('#studentList').html(studentList);
                
				
            }
        });
    
});

$(document).on( "click", "#clearAll", function() {
	
	$(".studentId").each(function() {
		var obj=$(this);
		var checkStatus=obj.is(":checked");
		//alert(checkStatus);
		if (checkStatus==true){ 
			obj.attr('checked', false);
	    }
		 
    });
});

	/*$(document).on( "click", "#checkAll", function() {
	   alert('l');
   $(".studentId").prop('checked', $(this).prop("checked"));
   });*/
$(document).on( "click", "#checkAll", function() {
	/*$(".studentId").each(function() {
		var obj=$(this);
		var checkStatus=obj.is(":checked");
		//alert(checkStatus);
		if (checkStatus==false){ 
			//alert("checkALL False");
			obj.attr("checked", "checked");
	    }
		 
    });*/
	if(this.checked) { // check select status
        $('.studentId').each(function() { //loop through each checkbox
            this.checked = true;  //select all checkboxes with class "checkbox1"              
        });
    }else{
        $('.studentId').each(function() { //loop through each checkbox
            this.checked = true; //deselect all checkboxes with class "checkbox1"                      
        });        
    }

    
});

$(document).on( "click", "#stBtn", function() {
	if($("#notification").val()==""){
		alert("Notification cannot be empty");
		$("#notification").focus();
		return false;
	}else
	if($("#expiryDate").val()==""){
			alert("Expiry Date cannot be empty");
			$("#expiryDate").focus();
			return false;
	}else
	if ($("#courseId").val() ==null) {
		alert("Please select at least one course");
		return false;
	}else
	if($("#studentList").html()==""){
			alert("Please Select Student!");
			return false;
	}else{
		var flag=false;
		$(".studentId").each(function() {
			var obj=$(this);
			var checkStatus=obj.is(":checked");
			if (checkStatus==true){ 
				flag=true;
		    }
		});
		if(flag==false){
			alert("Please Select Student!");
			return false;
		}
	}
		
	
	
	
    
});
});
</script>
 
 
<style>
.header-bottom {
    background: none repeat scroll 0 0 #e58b30;
    color: #fff;
    float: left;
    padding: 1% 0;
    width: 100%;
}
.sub_btn{
background: #e58b30;
  color: #fff;
  width: 72px;
  border: medium none;
  padding: 1% 1%;
  -webkit-box-shadow: 0 0 10px rgba(0, 0, 0, 0.4);
  -moz-box-shadow: 0 0 10px rgba(0, 0, 0, 0.4);
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.4);
  border-radius: 5px;
  margin: 0 1%;}
</style>
<script>
    $(document).ready(function() {
        
       $("#resetBtn").click(function(){
         	$("#courseId").val("-1");
 		});
    });
    </script>
    
    
    <style type="text/css">
  
   .role1select { height: 150px; overflow: auto; width: 310px; border: 1px solid #000; }
   .role1ul { height: 150px; overflow: auto; width: 340px; border: 1px solid #000; list-style-type: none; margin-bottom: 0px; padding: 5px; overflow-x: hidden; }
   .role1li { margin: 1px; padding: 5; }
   .role1label { display: block; color: WindowText; background-color: Window; margin: 0; padding: 0; width: 100%; }
   .role1label:hover { background-color: Highlight; color: HighlightText; }
   .csDiv {color: blue;font-size: 12px;font-style: italic; width: 340px; font-weight: 500;margin-bottom: 15px; } 
 
 
  </style>
    
</head>
<body>
  <!-- Start Header -->
       
 
<%@page import="java.util.Date"%>
 <%@page import="com.itech.elearning.forms.LoginForm" %>
<%
	  												Object photo="";
                                                	Object userName="";
	 														 try{
	 															//photo=((LoginForm) session.getAttribute("userDetail")).getProfilePhoto();;
	 															userName=((LoginForm) session.getAttribute("userDetail")).getFirstname();;
	 															 }catch(NullPointerException e){
    	  																		e.printStackTrace();
      															}
	 														%> 
<div class="header">	
   	 	    <div class="header-top">
   	 	      <div class="wrap"> 
   	 	    	 <div class="header-top-left">
   	 	    	 	<p>ph: +91-9611421111</p>
   	 	    	 </div>
   				  <div class="header-top-right">
				        <ul>
				            <li><a   >Careers</a></li>
				            <li  class="login">
				              <div id="loginContainer">
				            	   <a href="#" id="loginButton"><span>Join Us</span></a>
						                <div id="loginBox" class="login-form">    
						                	<h3>Login into Your Account</h3>            
						                 
						                  </div>
						             </div>
				               </li>
				               <li><a href="#" >Sitemap</a></li>
				         </ul>
				    </div>
			      <div class="clear"></div>
			     </div> 
		      </div>
             <div class="header-logo-nav">
             	  <div class="navbar navbar-inverse navbar-static-top nav-bg" role="navigation">
				      <div class="container">
				        <div class="navbar-header">
				          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				            <span class="sr-only">Toggle navigation</span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				          </button>
				         <div class="logo"> <a class="navbar-brand" href="http://www.itechtraining.in/"><img src="images/LEARNEZYLatest-LOGO.png" alt="" /></a></div>
				          <div class="clear"></div>
				        </div>
				        <div class="collapse navbar-collapse">
				          <ul class=" menu nav navbar-nav">
				           
				            <li><a href="Adminhome.do"><img src="<%=request.getContextPath() %>/images/homeover.png" title="Home"></a></li>
				            <li><a href="Logout.do"><img src="images/logout.png"></a></li>
				          </ul>
				        </div><!--/.nav-collapse -->
				      </div>
				    </div>
		         <div class="clear"></div>
                  <div class="header-bottom">
                 <div class="container">
                 <div class="time">
                 <p><div id="clockbox"></div></p>
                 <script src="js/dynamicClock.js" type="text/javascript"></script>
                 </div>
               
                 <div class="welcome">
                 <h3 align="right">Welcome: <%=userName %></h3>
                 </div>
                 </div>
                 </div>
                 <div class="clear"></div>
	        </div>
	        <div class="header-banner">	        				    
	           </div>
             </div>
   <!-- End Header -->
   
   <!-- Start Main Content -->
	 <div class="main" style="min-height:450px;">	 
	 	<h2>Student Notification</h2>
    <div id="status" align="center"><logic:notEmpty name="status">
					<font color="blue"><bean:write name="status"></bean:write></font>
				</logic:notEmpty></div>
   <html:form action="NotificationStudent.do?action=add" >
    <input type="hidden" name="notificationID"	 />
    <table border="0" width="80%" align="center"  class="role1">
		
	
		<tr height="50">	
			<td><label>Notification *</label></td>
			<td><input type="text" name="notification"  id="notification"   maxlength="200"/></td>
			
			
			<td><label>Expiry Date* </label></td>
			<td><input type="text" name="expiryDate"     id="expiryDate" readonly="readonly"  maxlength="200"/>
	   </tr>
	<tr height="50" valign="top">
			
			<td colspan="2"><div><label>Select Course*</label> </div><html:select property="courseId" styleId="courseId" multiple="true" styleClass="role1Select" style="height: 150px; overflow: auto; width: 340px; border: 1px solid #000;">
							<html:options collection="courseList" property="courseId" labelProperty="courseName" />
						</html:select></td>
			<td colspan="2"> <label>Select Student*</label>
			<div> <ul class="role1ul" id="studentList" ></ul></div>
			<div class="csDiv"> <a href="javascript:void(0)" id="checkAll" style="float: left: ;">Select all</a> <a href="javascript:void(0)" id="clearAll" style="float: right;">Clear all</a></div>
			
			
   	  
			
			
			
			</td>
		</tr>
		<tr height="50">
			<td colspan="6" align="center" >
			<input type="submit" class="sub_btn" name="btn" value="Submit" id="stBtn"   style="padding:0.5% 1%;" />
			<input type="reset"	value="Reset" style="padding:0.5% 1%;" id="resetBtn"/> 
		</tr>

	</table>
     </html:form> 	
		
 <div class="dataGridDisplay"  >
<table id="normalTable" class="display" cellspacing="0" width="100%" align="center" >
<thead>
<tr>
    <th>SI No.</th>
    <th>Student Name</th>
    <th>Notification</th>
    <th>Expiry Date</th>
    <th>Status</th>
    <th>Action</th>
</tr>
</thead>
<tbody>
    <logic:notEmpty name="notificationList">
    <logic:iterate id="notification" name="notificationList" indexId="index">
    <tr>
    <td><%=index+1 %></td>
    <td><bean:write name="notification"	property="firstName" /></td>
    <td><bean:write name="notification"	property="notification" /></td>
    <td><bean:write name="notification"	property="expiryDate" /></td>
	<td><logic:equal name="notification" property="active"	value="true">
			<font color="green">Active</font>
		</logic:equal> 
		<logic:equal name="notification" property="active" value="false">
			<font color="red">InActive</font>
		</logic:equal>
</td>

    <td>
    
    
			<input type="button" onclick='deleteNotification("<bean:write name="notification" property="notificationID"/>")' value="Delete">
			&nbsp;&nbsp;
		 <!-- 	<a href="#" onclick='edit("<bean:write name="notification" property="notificationID"/>",
									  "<bean:write name="notification" property="userid"/>",
									  "<bean:write name="notification" property="notification"/>",
									  "<bean:write name="notification" property="expiryDate"/>")'><input type="button" value="Edit"></a>
	 -->
    
</td>
    </tr>
   
 </logic:iterate>
  </logic:notEmpty>
  </tbody>
    </table>
    
    <div align="center"><a href="NotificationStudent.do?action=excelReport"><button>Excel Export</button></a></div>
	 
     </div>
    
   <!-- End Main Content -->
	   
 <jsp:include page="/jsp/common/masterdata_footer.jsp"></jsp:include> 
 <script type="text/javascript">
 document.forms[0].notification.focus();
	</script>
  </body>
</html>




    	
    	
            