<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script src="<%=request.getContextPath() %>/js/jquery-1.10.2.min.js" type="text/javascript"></script>
<link href="dataTable/css/jquery.dataTables_themeroller.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.min.css"	rel="stylesheet" type="text/css" media="all" />
<script src="dataTable/js/jquery.dataTables.js"	type="text/javascript"></script>
<script src="dataTable/js/jquery.dataTables.min.js"	type="text/javascript"></script>
<script src="dataTable/js/settingDataTable.js" 	type="text/javascript"></script>
</head>
<body>
<logic:notEmpty name="appointmentScheduleList">
<table id="normalTable" class="display" cellspacing="0" width="100%" align="center">
				<thead>
					<tr>
						<th>SI No.</th>
						<th>Appointment Data & Time</th>
						<th>Course</th>
						<th>Appointment Type</th>
					
						<th>Action</th>
						 
					</tr>
				</thead>
				<tbody>

					
						<logic:iterate id="appointmentScheduleList" 	name="appointmentScheduleList" indexId="index">
							<tr>
								<td><%=index + 1%></td>
								<td><bean:write name="appointmentScheduleList"	property="appointmentDateTime" /></td>
								<td><bean:write name="appointmentScheduleList"	property="courseName" /></td>
								<td><bean:write name="appointmentScheduleList"  property="appointmentType" /></td>
								<td>
								<logic:equal name="appointmentScheduleList" property="appointmentType" value="White Board">
								
									<logic:equal name="appointmentScheduleList" property="sessionStatus" value="Open">
										<a href='StudentSessionClass.do?action=accessSession&amp;appointmentScheduleId=<bean:write name="appointmentScheduleList" property="appointmentScheduleId"/>'><input type="button" value="Join"></a>
									</logic:equal>
									<logic:equal name="appointmentScheduleList" property="sessionStatus" value="Close">
										<font color="red">Session Closed</font>
									</logic:equal>
								
								</logic:equal>
								<logic:equal name="appointmentScheduleList" property="appointmentType" value="Video">
								
									<logic:equal name="appointmentScheduleList" property="sessionStatus" value="Open">
										<a href='<bean:write name="appointmentScheduleList" property="meetingURLLink"/>'><input type="button" value="Join"></a>
									</logic:equal>
									
									<logic:equal name="appointmentScheduleList" property="sessionStatus" value="Close">
										<font color="red">Session Closed</font>
									</logic:equal>
								
								</logic:equal>
								
								
								
								</td>
							</tr>
						</logic:iterate>
					
				</tbody>

			</table>
</logic:notEmpty>
<logic:notEmpty name="appointmentScheduleId">

 

</logic:notEmpty>
</body>
</html>