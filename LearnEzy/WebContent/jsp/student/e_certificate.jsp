<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-nested.tld" prefix="nested"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Itech Training :: Get Certificate</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300'
	rel='stylesheet' type='text/css'>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/cmsstyle.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/font-awesome.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/carousel.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css"
	media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />


<script src="js/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="js/bootstrap.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/owl.carousel.js" type="text/javascript"></script>

  <script type="text/javascript" src="js/jQuery.print.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#print_button").click(function(){
    	 $("#certificate").print({
    		    globalStyles: true,
    		    mediaPrint: false,
    		    stylesheet: false,
    		    noPrintSelector: ".no-print",
    		    iframe: true,
    		    append: null,
    		    prepend: null,
    		    manuallyCopyFormValues: true,
    		    deferred: $.Deferred()
    		});
		   return( false );
	});
});	
</script> 
<script src="js/pdf/require.js" ></script>
 
    <script type="text/javascript" src="js/pdf/jquery-ui-1.8.17.custom.min.js"></script>
    <script type="text/javascript" src="js/pdf/jspdf.debug.js"></script>
    <script type="text/javascript" src="js/pdf/basic.js"></script>
 <script type="text/javascript">
 
 function demoLandscape123() {
	 var iii=$("#certificate").html();
		var doc = new jsPDF('landscape');
		doc.text(20, 20, iii);

		// Save the PDF
		doc.save('elerning.pdf');
	}
 </script>


</head>
<body>
	<!-- Start Header -->
		<jsp:include page="/jsp/common/student_header.jsp"></jsp:include>
	<!-- End Header -->
	<div class="clear"></div>
	<!-- Start Main Content -->
	 <div class="main" style="min-height:450px;">	 
	 <logic:notEmpty name="getCertificateDetials">
				<logic:iterate id="getCertificate" name="getCertificateDetials" 	indexId="index">
				<div id="certificate">
	 	<div style="width:800px; height:611px; padding:20px; text-align:center; border: 10px solid #787878; margin:0 auto;margin-top: 10px;
margin-bottom:10px;">
<div style="width:750px; height:558px; padding:20px; text-align:center; border: 5px solid #787878;">
      <div style="width: 703px;
height:96px;
text-align: center;
background: #E58B30;
padding: 20px;
margin-top: -21px;
color: #000;"> <span style="font-size:45px; font-weight:bold; margin-left:10px;"><img style="width:140px; float:left; margin-top:-4px; "src="images/Itech Solutions logo.png">Certificate of Training</span></div>
       <br>
       <span style="font-size:25px"><i>This is to certify that</i></span>
       <br><br>
       <span style="font-size:30px; font-style:italic;"><b><bean:write name="getCertificate" property="courseName" /></b></span><br/><br/>
       <span style="font-size:25px"><i>has completed the course</i></span> <br/>
       <span style="font-size:30px"><b><bean:write name="getCertificate" property="studentName" /></b></span> <br/><br/>
       <span style="font-size:20px;"><i>with score of </i></span> <br/>
        <span style="font-size:30px"><b><bean:write name="getCertificate" property="passpercent" />%</b></span> <br/><br/>
       <span style="font-size:25px"><i> on dated</i></span><br>
       
       <b><bean:write name="getCertificate" property="courseComplDate" /></b><br><br><br>
      <span style="font-size:12px; color:#F28858;">COMPUTER GENERATED, SIGNATURE NOT REQUIRED</span>
</div>
</div>
</div>
	</logic:iterate>
			
      
	 <div align="center">
<html:form action="GetCertificate.do?action=getCertificatePDF">
		<input type="hidden" name="courseName" value='<bean:write name="getCertificate" property="courseName" />' />
		<input type="hidden"  name="studentName" value='<bean:write name="getCertificate" property="studentName" />' />
		<input type="hidden"  name="passpercent" value='<bean:write name="getCertificate" property="passpercent" />'  />
		<input type="hidden"  name="courseComplDate" value='<bean:write name="getCertificate" property="courseComplDate" />'  />
		<input type="submit" value="Get PDF Certificate" /> <button id="print_button" >Print</button> 
</html:form>
</div>
</logic:notEmpty>






<logic:empty name="getCertificateDetials">

<div id="certificate">
	 	<div style="width:800px; height:611px; padding:20px; text-align:center; border: 10px solid #787878; margin:0 auto;margin-top: 10px;
margin-bottom:10px;">
<div style="width:750px; height:558px; padding:20px; text-align:center; border: 5px solid #787878;">
      <div style="width: 703px;
height:96px;
text-align: center;
background: #E58B30;
padding: 20px;
margin-top: -21px;
color: #000;"> <span style="font-size:45px; font-weight:bold; margin-left:10px;"><img style="width:140px; float:left; margin-top:-4px; "src="images/Itech Solutions logo.png">Certificate of Training</span></div>
       <br>
       <span style="font-size:25px"><i>This is to certify that</i></span>
       <br><br>
       <span style="font-size:30px; font-style:italic;"><b></b></span><br/><br/>
       <span style="font-size:25px"><i>has completed the course</i></span> <br/>
       <span style="font-size:30px"><b></b></span> <br/><br/>
       <span style="font-size:20px;"><i>with score of </i></span> <br/>
        <span style="font-size:30px"><b>%</b></span> <br/><br/>
       <span style="font-size:25px"><i>dated</i></span><br>
       
       <b>" "</b><br><br><br>
      <span style="font-size:12px; color:#F28858;">COMPUTER GENERATED, SIGNATURE NOT REQUIRED</span>
</div>
</div>
</div>
</logic:empty>
</div>
<script language="javascript">
document.onmousedown=disableclick;
status="Right Click Disabled";
function disableclick(event)
{
  if(event.button==2)
   {
     alert(status);
     return false;    
   }
}
</script>
	<!-- End Main Content -->
 
	 <!-- Start Footer -->
     <jsp:include page="/jsp/common/student_footer.jsp" /> 
  <!-- End Footer -->
</body>
</html>
 