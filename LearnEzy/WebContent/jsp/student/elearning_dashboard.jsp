<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.itech.elearning.forms.*"%>
<%@ page import="java.util.*"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-nested.tld" prefix="nested"%>
<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv='cache-control' content='no-cache'>
<meta http-equiv='expires' content='0'>
<meta http-equiv='pragma' content='no-cache'>
<style>
.show .heading {
	color: #02B5F5;
	width: 100%;
	text-align: left;
}

.left_menu h2 {
	text-align: left;
}
#sampleProgDiv {
  border-top: 1px dashed #F28858;
  width: 80%;
  float: right;
  margin-right: 10px;
  margin-top: 10px;
}
.assingDisplay{
margin-left: 10px;
color:black;
}
</style>
<title>Itech Training - E- Learning Screening</title>
<meta name="description"
	content="find the best job oriented training in  Web design  technology training bangalore, we also provide training in  mobile application development , JAVA J2EE course,SEO training, SAP ABAP from  experience trainers" />
<meta name="keywords"
	content="Java training in Bangalore, advanced Java training institutes Bangalore,  SAP  training, seo training,  technical writing training,  Soft skills training, mobile apps training, software testing, best Java training center in Bangalore" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300'
	rel='stylesheet' type='text/css'>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/bootstrap-theme.min.css" rel="stylesheet"
	type="text/css" media="all" />
<link href="css/cmsstyle.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/font-awesome.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/carousel.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css"
	media="all" />

<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
<script src="js/bootstrap.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/owl.carousel.js" type="text/javascript"></script>

<link href="htmlVideo/dist/skin/blue.monday/css/jplayer.blue.monday.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="htmlVideo/lib/jquery.min.js"></script>
<script type="text/javascript" src="htmlVideo/dist/jplayer/jquery.jplayer.min.js"></script>


<script type="text/javascript" src="js/student/learningDashboard.js"></script>
<style type="text/css">
.questionClass {
	color: red;
}

.answerClass {
	color: #808080;
	margin-left: 5px;
}
</style>

<script type="text/javascript">

function openJavaCompler() {
    window.open("http://www.w3schools.com");
}




$(document).ready(function()
		{
	$(document).on('click','.compileandrunclass',function(){


		
		var program=$(this).attr('id');
		var programcontent=program.substring(10); 
		
		var passingprogram=$("#sampleid"+programcontent).text();
		location.replace("http://localhost/Online-Compiler-master/Online-Compiler-master/user/code.php?program="+encodeURIComponent(passingprogram));
		
		

		});

});




</script>




</head>
<body>
	<!-- Start Header -->
		<jsp:include page="/jsp/common/student_header.jsp"></jsp:include>
	<!-- End Header -->

	<!-- Start Main Content -->

	<div class="main">
		<div class="wrap" style="margin-top: 25px;">
			<div class="col-lg-12">
				<div class="about-desc">
					<div class="left_menu">
						<h2 style="text-align: left;">
							<logic:notEmpty name="courseName">
								<bean:write name="courseName" />
							</logic:notEmpty>
							<input type="hidden" id="courseId"
								value="<bean:write name="courseId"/>">
						</h2>
						<table>
							<%
								List<TestMasterForm> topicList = (List<TestMasterForm>) request
										.getAttribute("topicList");
								Iterator<TestMasterForm> itr = topicList.listIterator();
								int count = 0;
								while (itr.hasNext()) {
									TestMasterForm r = itr.next();
									Long subTopicId = r.getTopicId();
									count = count + 1;
									Long topicId = null;
							%>

							<tr>
								<td>

									<div class="panel-group category-products" id="accordian">
										<!--category-productsr-->
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a id="<%=count%>" class="flip" href="javascript:void(0)">
														<span class="badge pull-right"><i
															class="fa fa-plus"></i></span> <%=r.getTopicName()%>
													</a>
												</h4>
											</div>
											<div id="panel<%=count%>" class="panel2">
												<div class="panel-body">
													<ul>
														<%
															List<SubTopicMasterForm> subTopicList = (List<SubTopicMasterForm>) request
																		.getAttribute("subTopicList");
																Iterator<SubTopicMasterForm> itr2 = subTopicList.listIterator();
																while (itr2.hasNext()) {
																	SubTopicMasterForm r2 = itr2.next();

																	int flag = 0;
																	if (subTopicId == r2.getTopicId()) {
																		topicId = r2.getTopicId();
														%>
															<li><a id="<%=r2.getSubTopicId()%>" class="subTopic"	href="javaScript:void()"><%=r2.getSubTopicName()%> </a></li>
															<li><a id="<%=r2.getSubTopicId()%>" class="assingSubTopic"    href="javaScript:void()"><font class="assingDisplay" >Assignment</font></a></li>

														<%
															}

																}
														%>
														<!--  <li><a id="" class="assingment"	href="javaScript:void()">Assignment </a></li> -->
													</ul>
												</div>
											</div>
										</div>
									</div>

								</td>
							</tr>
							<%
								}
							%>
							<tr>
								<td>

									<div class="panel-group category-products" id="accordian">
										<!--category-productsr-->
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a id="takeTest" class="flip" href='Test.do?action=testList&amp;courseId=<bean:write name="courseId"/>'>
													  Take Test
													</a>
												</h4>
											</div>
										</div>
									</div>



								</td>
							</tr>
							
							<tr>
								<td>

									<div class="panel-group category-products" id="accordian">
										<!--category-productsr-->
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a id="javaComplier" class="flip"  href="#">
													 Java Complier
													</a>
												</h4>
											</div>
										</div>
									</div>



								</td>
							</tr>
						</table>

					</div>
					<div class="middle_content" id="contantDiv"  style="margin-top: 5px;"></div>
					<!--middle_content -->


					<div class="right_content" style="margin-top: 15px;float: right;">
						<div id="imageUploadDiv" style="display: none;">
							<img alt="" src="" id="imageUpload" width="280" height="300" />
						</div>
						<div id="youTubeLinkDiv" style="margin-top: 15px; display: none;">
							<iframe width="280" height="300" id="youTubeLink" src="">
							</iframe>
						</div>
						<div id="videoUploadDiv" style="margin-top: 15px; display: none;">
							 
						</div>
					</div>
					<!--rightcontent-->
				</div>

			</div>

<div id="sampleProgDiv"  ></div>

			<div style="clear: both"></div>
			
		</div>
		
	</div>
	<!-- End Main Content -->

	 <!-- Start Footer -->
     <jsp:include page="/jsp/common/student_footer.jsp" /> 
  <!-- End Footer -->
</body>
</html>