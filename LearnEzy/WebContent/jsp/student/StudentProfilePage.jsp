<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="com.itech.elearning.forms.LoginForm"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-nested.tld" prefix="nested"%>
<!DOCTYPE HTML>
<html>
<head>
<title>Itech Training :: Student Profile</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW">
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300'
	rel='stylesheet' type='text/css'>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/cmsstyle.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/font-awesome.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/carousel.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css"
	media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="src/DateTimePicker.css" />

 

<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="src/DateTimePicker.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/date/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/date/jquery-ui.css" />
<script type="text/javascript">
$(function(){
    $( "#datepicker1" ).datepicker({dateFormat: 'dd/mm/yy',changeMonth: true, changeYear: true, maxDate: 0 });
	$.each($("#toCheckId" ).val().split(','), function( intIndex, objValue ){
	//alert(objValue);
		$('#courses option[value=' + objValue + ']').attr('selected', true);
	});
	
	
});

function validFNameonkey(){
	re = /[^a-z,A-Z, ]/;
	var str=document.forms[0].firstName.value.replace(re,"");
	document.forms[0].firstName.value=str;
}
function validLNameonkey(){
	re = /[^a-z,A-Z, ]/;
	var str=document.forms[0].lastName.value.replace(re,"");
	document.forms[0].lastName.value=str;
}
function isValidQlification(){
	re = /[^a-z,A-Z, ]/;
	var str=document.forms[0].qualification.value.replace(re,"");
	document.forms[0].qualification.value=str;
}
function fmtcontacno(){
	re = /\D/g; // remove any characters that are not numbers
	socnum=document.forms[0].phone.value.replace(re,"");
	sslen=socnum.length;
	if(sslen>3&&sslen<7){
		ssa=socnum.slice(0,3);
		ssb=socnum.slice(3,6);
		document.forms[0].phone.value="("+ssa+")"+ssb;
	}else{
		if(sslen>6){
			ssa=socnum.slice(0,3);
			ssb=socnum.slice(3,6);
			ssc=socnum.slice(6,9);
			ssd=socnum.slice(9,10);
			document.forms[0].phone.value="("+ssa+")"+ssb+"-"+ssc+ssd;
		}else{
			document.forms[0].phone.value=socnum;
		}
	}
}

function fmtmobile(){
	//alert("arg");
	re = /\D/g; // remove any characters that are not numbers
	socnum=document.forms[0].contactno.value.replace(re,"");
	document.forms[0].contactno.value=socnum;
	 
}
function pincodOnlyNumbers(){
	//alert("arg");
	re = /\D/g; // remove any characters that are not numbers
	socnum=document.forms[0].pincode.value.replace(re,"");
	document.forms[0].pincode.value=socnum;
}


function test1(){
	if(document.forms[0].firstName.value.trim()==""){
		alert("First Name cannot be empty");
		document.forms[0].firstName.focus();
		return false;
	}else
	if(document.forms[0].emailId.value.trim()==""){
		alert("Email cannot be empty");
		document.forms[0].emailId.focus();
		return false;
	}else
	if(document.forms[0].contactno.value.trim() == ""  ){
		alert("Mobile Number be empty");
		document.forms[0].contactno.focus();
		return false;
	}else
	if(document.forms[0].contactno.value.trim() == "0000000000"  ){
		alert("Mobile Number is not Valid");
		document.forms[0].contactno.focus();
		return false;
	}else
	if(document.forms[0].phone.value.trim() == "(000)000-0000"  ){
		alert("Contact Number is not Valid");
		document.forms[0].phone.focus();
		return false;
	}else
	if(document.forms[0].contactno.value != ""  ){
		if(document.forms[0].contactno.value.length < 10){
			alert("Mobile Number is not Valid");
			document.forms[0].contactno.focus();
			return false;
		}
	}else
	if(document.forms[0].phone.value != ""  ){
		if(document.forms[0].phone.value.length < 13){
			alert("Contact Number is not Valid");
			document.forms[0].phone.focus();
			return false;
		}
	}else
	if(document.forms[0].pincode.value.trim()=="" || document.forms[0].pincode.value.trim()=="0" || document.forms[0].pincode.value.trim()=="000000"){
		alert("Please Enter the Pin Code");
		document.forms[0].pincode.focus();
		return false;
	}else
	if(document.forms[0].countryid.value.trim()== "-1"){
		alert("Please Select the Country");
		document.forms[0].countryid.focus();
		return false;
	}else
	if(document.forms[0].stateid.value.trim()=="-1"){
		alert("Please Select the State");
		document.forms[0].stateid.focus();
		return false;
	}else
	if(document.forms[0].cityid.value.trim()=="-1"){
		alert("Please Select the City");
		document.forms[0].cityid.focus();
		return false;
	}else{
		document.forms[0].submit();
	}
}

function validemail(){
	re =/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/; 
	if(document.forms[0].emailId.value.trim()!=""){
		if (document.forms[0].emailId.value.search(re)== -1) {
			alert("Please Enter valid Email Id");
			document.forms[0].emailId.value = "";
			document.forms[0].emailId.focus();
		}
	}	
}
</script>
</head>
<body>
<input type="hidden"
	value="<bean:write name='stdentinfoList' property='courseidslist'/>"
	id="toCheckId" />
<!-- Start Header -->
<div class="header">
<div class="header-top">
<div class="wrap">
<div class="header-top-left">
<p>ph: +91-9611421111</p>
</div>
<div class="header-top-right">
<ul>
	<li><ahref"#">Careers</a></li>
	<li class="login"></li>
	<li><a href="#">Sitemap</a></li>
</ul>
</div>
<div class="clear"></div>
</div>
</div>
<div class="header-logo-nav">
<div class="navbar navbar-inverse navbar-static-top nav-bg"
	role="navigation">
<div class="container">
<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse"
	data-target=".navbar-collapse"><span class="sr-only">Toggle
navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span>
<span class="icon-bar"></span></button>
<div class="logo"><a href="http://itechtraining.in/" class="logo"><img
	src="images/Itech Solutions logo.png" alt="" /></a></div>
<div class="clear"></div>
</div>
<div class="collapse navbar-collapse">
<ul class=" menu nav navbar-nav">

	<li><a href="<%=request.getContextPath()%>/studentProfile.do?action=toHome"><img src="images/back.png"></a></li>

	<li><a href="Logout.do"><img src="images/logout.png"></a></li>
</ul>
</div>
<!--/.nav-collapse --></div>
</div>
<div class="clear"></div>
<div class="header-bottom">
<div class="container">
<div class="time">
<p id="clockbox" ></p>
<script src="js/dynamicClock.js" type="text/javascript"></script>
</div>

<div class="welcome">
<h3 align="right"></h3>
</div>
</div>
</div>
<div class="clear"></div>
</div>
<div class="header-banner"></div>

</div>
<!-- End Header -->

<!-- Start Main Content -->
<div class="main" style="min-height: 450px;">
<h2>Student Profile</h2>
<div align="center"><logic:notEmpty name="status">
	<font color="blue"><bean:write name="status"></bean:write></font>
</logic:notEmpty></div>

<html:form action="/studentProfile.do?action=updateStudentData"  enctype="multipart/form-data">
<html:hidden property="oldPhoto" name="stdentinfoList"	/>
<html:hidden property="uname" name="stdentinfoList"	/>
 <input type="hidden"  id="cityid1"  value="<bean:write name="stdentinfoList" property="cityid"/>"  />
 <input type="hidden"  id="stateid1"  value="<bean:write name="stdentinfoList" property="stateid"/>"  />
	<table border="0" width="85%" align="center"   class="role">
	<tr height="40" valign="middle">
			<td colspan="2" align="center">
			<%	Object photo = "";
				
				try {
					photo = ((LoginForm) session.getAttribute("userDetail")).getProfilePhoto();;
				} catch (NullPointerException e) {
					e.printStackTrace();
				}
				if (photo != null && !photo.equals("")) {  %> 
						<img	id="blah" src="<%=request.getContextPath()%>/imageUpload/<bean:write name="stdentinfoList" property="oldPhoto"/>"	class="img-circle" style="width: 80px; height: 80px;" alt="User Image" /> 
				<% } else { %> 
						<img src="images/empty_profile_pthoto.png" class="img-circle" id="blah"	alt="User Image" style="width: 80px; height: 80px;" /> <%
 				}
 		    %>

            </td>
			
			<td><label>Change Profile Photo :</label></td>
			<td><input name="imageFile" id="imageFile" type="file"  /></td>

		</tr>
	
		<tr height="40">
			<td><label>First Name*</label></td>
			<td><html:text property="firstName" name="stdentinfoList" styleId="firstName"
				onkeyup="validFNameonkey()" maxlength="65" /></td>
			<td><label>Last Name</label></td>
			<td><html:text property="lastName" name="stdentinfoList"
				onkeyup="validLNameonkey()" maxlength="65" /></td>

		</tr>
		<tr height="40">

			<td><label>Date of Birth</label></td>
			<td><html:text property="dob" name="stdentinfoList" 
				styleId="datepicker1"></html:text></td>
			<td><label>Gender</label></td>
			<td><html:select property="gender" name="stdentinfoList" style="width:212px;">
			<html:option value="-1">--Select Gender--</html:option>
				<html:option value="Male">Male</html:option>
				<html:option value="Female">Female</html:option>
			</html:select></td>
		</tr>

		<tr height="40">
			<td><label>Email*</label></td>
			<td><html:text property="emailId" name="stdentinfoList" styleId="emailId"
				onblur="return validemail();" /></td>
			<td><label>Mobile*</label></td>
			<td><html:text property="contactno" name="stdentinfoList" styleId="contactno"  style="width:212px;" 
				maxlength="10" size="15" onkeyup="fmtmobile(this)"
				  /></td>
		</tr>

		<tr height="40">


			<td><label>Contact No.</label></td>
			<td><html:text property="phone" name="stdentinfoList" styleId="phone"
				maxlength="13" onkeyup="fmtcontacno(this);" 
				  /></td>
			<td><label>Qualification </label></td>
			<td><html:text property="qualification" name="stdentinfoList" styleId="qualification"
				maxlength="45" onkeyup="isValidQlification(this);" /></td>

		</tr>
		<tr height="100">

			<td valign="top"><label>Address</label></td>
			<td><html:textarea property="address" name="stdentinfoList"
				rows="3" cols="10" style="resize:none;width:212px;" /></td>

			
		</tr>
		<tr height="40">
			<td><label>Country *</label></td>
			<td><html:select property="countryid" styleId="countryid"  name="stdentinfoList"  >
						<html:option value="-1">--Select Country--</html:option>
				<html:options collection="countryList" property="countryid" labelProperty="countryname" />
				</html:select></td>
			<td><label>State *</label></td>
			<td><select name="stateid" id="stateid"  >
						<option value="-1">-Select State-</option>
												</select></td>

		</tr>
		<tr height="40">
			<td><label>City *</label></td>
			<td><select name="cityid" id="cityid"  >
					<option value="-1">-Select City-</option>
			</select></td>
			<td><label>Pincode *</label></td>
			<td><html:text   styleId="pincode" property="pincode" onkeyup="pincodOnlyNumbers();"  name="stdentinfoList" maxlength="6"  /></td>
		</tr>
		<tr height="40">


		</tr>
		<html:hidden property="userid" styleId="updateid" name="stdentinfoList" />
		<logic:present name="userexist">
			<tr>
				<td colspan="4" align="left"><font color="red"><bean:write
					name="userexist" /></font></td>
			</tr>
		</logic:present>
		<tr height="30">
			<td colspan="4" align="center"><input type="submit" name="btn" id="validateRegister"
				value="Update" onclick="return test()" style="padding: 0.5% 1%;" /></td>
		</tr>
		
			 
	</table>
</html:form></div>

<!-- End Main Content -->
 

 <!-- Start Footer -->
     <jsp:include page="/jsp/common/student_footer.jsp" /> 
  <!-- End Footer -->

<script type="text/javascript">
		
			$(document).ready(function(){
				var countryid=$("#countryid").val();
				var stateid=$("#stateid1").val();
				var cityid=$("#cityid1").val();
				//alert(cityid);
				setState(countryid,stateid);
				setCity(stateid,cityid);

				$("#countryid").change( function() {
					var countryid=$(this).val();
					//alert(countryid);
					$.ajax({	
						type: "GET",		
						url:"State.do?action=ajaxActiveList",
						data:{countryId:countryid},
						success: function(r){	
							var json= JSON.parse(r);
							console.log(json);			
							$("#stateid").empty();
							var firstOption= $("<option>",{text:"--Select State--",value:"-1"});
							$("#stateid").append(firstOption);
							$.each(json,function(i,obj){
								var option= $("<option>",{text:obj.state, value:obj.stateId});
								$("#stateid").append(option);
							});
							 
						} 
						
					});
				});

				$("#stateid").change( function() {
					var stateid=$(this).val();
					//alert(stateid);
					$.ajax({	
						type: "GET",		
						url:"City.do?action=ajaxActiveList",
						data:{stateId:stateid},
						success: function(r){	
							var json= JSON.parse(r);
							console.log(json);			
							$("#cityid").empty();
							var firstOption= $("<option>",{text:"--Select City--",value:"-1"});
							$("#cityid").append(firstOption);
							$.each(json,function(i,obj){
								var option= $("<option>",{text:obj.city, value:obj.cityId});
								$("#cityid").append(option);
							});
							 
						} 
						
					});
					
				});
				
				$("#validateRegister").click( function() {
					if($("#firstName").val()==""){
						alert("First Name cannot be empty");
						$("#firstName").focus();
						return false;
					}else
					if($("#emailId").val()==""){
						alert("Email cannot be empty");
						$("#emailId").focus();
						return false;
					}else
					if($("#contactno").val()=="" ){
						alert("Mobile Number cannot be empty ");
						$("#contactno").focus();
						return false;
					}else
					if($("#contactno").val().length < 10){
						alert("Mobile Number is not Valid");
						$("#contactno").focus();
						return false;
					}else
					if($("#contactno").val()=="0000000000"){
						alert("Mobile Number is not Valid");
						$("#contactno").focus();
						return false;
					}else
					if($("#phone").val().length < 13 && $("#phone").val()!=""){
						alert("Alternative  Contact Number is not Valid");
						$("#phone").focus();
						return false;
					}else
					if($("#phone").val()=="(000)000-0000"){
						alert("Alternative  Contact Number is not Valid");
						$("#phone").focus();
						return false;
					}else			
					if($("#countryid").val()=="-1" ){
						alert("Select Country ");
						$("#countryid").focus();
						return false;
					}else
					if($("#stateid").val()=="-1" ){
						alert("Select State");
						$("#stateid").focus();
						return false;
					}else
					if($("#cityid").val()=="-1" ){
						alert("Select City");
						$("#cityid").focus();
						return false;
					}else
					if($("#pincode").val()=="" || $("#pincode").val()=="000000" || $("#pincode").val()=="0" ){
						alert("Pincode cannot be empty ");
						$("#pincode").focus();
						return false;
					}		 
				});
				
			});
		
			
			function setState(countryid,SelectedId)
			{
				
					$.ajax({	
						type: "GET",		
						url:"State.do?action=ajaxActiveList",
						data:{countryId:countryid},
						success: function(r){	
							var json= JSON.parse(r);
							console.log(json);			
							$("#stateid").empty();
							var firstOption= $("<option>",{text:"--Select State--",value:"-1"});
							$("#stateid").append(firstOption);
							$.each(json,function(i,obj){
								var option= $("<option>",{text:obj.state, value:obj.stateId});
								$("#stateid").append(option);
							});
							 
							$("#stateid").val(SelectedId);
						} 
						
					});
				
			}
				
			
			function setCity(stateid,cityIDD)
			{
				//alert(cityIDD);
					$.ajax({	
						type: "GET",		
						url:"City.do?action=ajaxActiveList",
						data:{stateId:stateid},
						success: function(r){	
							var json= JSON.parse(r);
							console.log(json);			
							$("#cityid").empty();
							var firstOption= $("<option>",{text:"--Select City--",value:"-1"});
							$("#cityid").append(firstOption);
							$.each(json,function(i,obj){
								var option= $("<option>",{text:obj.city, value:obj.cityId});
								$("#cityid").append(option);
							});
							
							$("#cityid").val(cityIDD);
						} 
				});
					
					function readURL(input) {
						if (input.files && input.files[0]) {
					        var reader = new FileReader();
					        reader.onload = function (e) {
					            $('#blah').attr('src', e.target.result);
					        }
					        reader.readAsDataURL(input.files[0]);
					    }
					}
					 
					
					 $('#imageFile').change(function () {
							var val = $(this).val().toLowerCase();
							//alert(val);
							var regex = new RegExp("(.*?)\.(jpg|png|gif|jpeg)$");
							if(!(regex.test(val))) {
							 	$(this).val('');
							 	alert('Please select .jpg .png .gif .jpeg format only');
							 }else{
								  readURL(this);
							 } 
						});  
					
					
					
					 
			}
			
			function restValues(){
				//alert("asas");
				document.forms[0].firstName.value="";
				document.forms[0].contactno.value= "";
				document.forms[0].phone.value != ""  ;
				document.forms[0].countryid.value="-1";
				document.forms[0].stateid.value="-1";
				document.forms[0].cityid.value="-1";
				document.forms[0].pincode.value="";
				document.forms[0].emailId.value=""
				document.forms[0].lastname.value="-1";
				document.forms[0].dob.value="";
				document.forms[0].gender.value="-1";
				document.forms[0].address.value="";
			}

		/*	var bIsPopup = displayPopup();
			$("#dtBox").DateTimePicker({
				isPopup: bIsPopup,
				addEventHandlers: function(){
					var dtPickerObj = this;
					$(window).resize(function(){
						bIsPopup = displayPopup();
						dtPickerObj.setIsPopup(bIsPopup);
					});
				}
			});
			function displayPopup(){
				if($(document).width() >= 768)
					return false;
				else
					return true;
			}*/
		</script>

<!-- End Footer -->
</body>
</html>




