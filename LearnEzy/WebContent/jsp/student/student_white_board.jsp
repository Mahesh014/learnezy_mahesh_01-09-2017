<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
.wbFrame {
width: 80%;
height: 100%;
padding: 10px;
}

</style>
</head>
<body>
<div class="sessionDetials">Title : <bean:write name="sessionTitle"/> &nbsp;&nbsp; Session Password : <bean:write name="sessionPassword"/></div>
<div class="wbFrame"></div>
<iframe src='<bean:write name="frameURL"/>' frameborder="0" width="800" height="600" style="border:solid 1px #555;"></iframe> 
</body>
</html>