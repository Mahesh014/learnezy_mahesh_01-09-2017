<%@page import="org.apache.struts.taglib.html.RewriteTag"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@page import="com.itech.elearning.forms.LoginForm"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="UTF-8">
<title>Itech Training :: Student Home</title>
<meta
	content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	name='viewport'>
	<meta name="robots" content="NOINDEX, NOFOLLOW">
<!-- bootstrap 3.0.2 -->
<link href="style/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- font Awesome -->
<link href="style/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="style/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- Morris chart -->
<link href="style/morris/morris.css" rel="stylesheet" type="text/css" />
<!-- jvectormap -->
<link href="style/jvectormap/jquery-jvectormap-1.2.2.css"
	rel="stylesheet" type="text/css" />
<!-- fullCalendar -->
<link href="style/fullcalendar/fullcalendar.css" rel="stylesheet"
	type="text/css" />
<!-- Daterange picker -->
<link href="style/daterangepicker/daterangepicker-bs3.css"
	rel="stylesheet" type="text/css" />
<!-- bootstrap wysihtml5 - text editor -->
<link href="style/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"
	rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery-ui.css">
	
<!-- Theme style -->
<link href="style/AdminLTE.css" rel="stylesheet" type="text/css" />

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script type="text/javascript" src="js/student/studentDashboard.js"></script>
<script src="<%=request.getContextPath() %>/js/jquery.fancybox.pack.js"></script>
<script src="<%=request.getContextPath() %>/js/auto.js"></script>
<script>
	$(document).ready(function(){
		
	$("#search2").click(function(){

var name=$("#search1").val();

		
		
		$.ajax({	
			type: "GET",
				data:{name:name},	
			url:"<%=request.getContextPath()%>/studentRegistration.do?action=viewdetails",
			async:false,
			success: function(r){	
				var json= JSON.parse(r);
				//$("#countryid").empty();
				//var firstOption= $("<option>",{text:"Select Country",value:""});
				//$("#countryid").append(firstOption);
				$.each(json,function(i,obj){


					//var d=obj.CourseName;
				

					
				      // var r= $('<input type="button" id="button" value="new button"/>');
				       

					//$("#app").append("<div>"+obj.CourseName+"<br>"+obj.Fees+"<br>"+obj.imagename+"<br>"+obj.rate+"<br>"+obj.companyname+"<br> </div>");
				    //   $("#app").append(r);
				});
			} 
		});
	
					
		});
});
	</script>
	<script>
	$(document).ready(function()
			{
				$("#notesDialog").hide();
				var b=$("#statusValue").val();
				if(b>0)
				{
				$("#tempId").val(b);
				$("#notesPopup").show();
				$("#notesDialog").dialog();
				}
				
				
			});
				
	</script>
	 <script>
$(document).ready(function()
		{
$("#compiler").click(function()
		{
	location.replace("http://localhost/Online-Compiler-master/Online-Compiler-master/user/code.php");
		});
		});
</script>

<script type="text/javascript">
$(document).ready(function()
		{
	
$("#search1").autocomplete(
		{

	source : function(request, response) {
		$.ajax({
		
		url : "<%=request.getContextPath()%>/studentRegistration.do?action=autoCompleteindexesare",
		type : "POST",
		data : {
			term : request.term
			
		},
		dataType : "json",
		success : function(data) {
	        response(data);
	       var datas=data;
	      }

	});
}
});
		});
</script>








        <script type="text/javascript">
function show(e1Id)
{
for (var i=1; i<=2; i++) {
 document.getElementById('1'+i).className = 'hide';
 }
 document.getElementById('1'+e1Id).className = 'show';
}
</script>

<style>
.box1 {
	width: 150px;
	background: #03a7e2;
	height: 38px;
	color: #fff;
	border-radius: 20px;
	padding: 5px 0px 0px 0px;
	margin-left: 10px;
	float: left;
	text-align: center;
	color: white;
	font-size: 19px;
	font-weight: bold;
	margin-bottom: 10px;
}
.sessionClass{
	width: 75%;
}
.courseRatingList, .RatingStarts{
	float: left;
}
 <style>
.show{display:block;}
.hide{display:none;}
</style>
</style>
</head>
<input type="hidden" id="serverFilePath" value="<%=request.getContextPath()%>" />
<body class="skin-blue">


<div style="color:Red;" id="notesDialog" title="My Notes" width="1000">
		<div id="bidPriceStatus"></div>
		
		
		
		<div id="notesPopup"> 
		
	<form action="Student.do?action=mynotesadd"  method="post" id="prodForm"> 
	
	<div id="bidPriceStatus"> 
		<logic:notEmpty name="courseid">
			<input type="hidden" id="statusValue"  name="statusValue"  value="<bean:write name="courseid"></bean:write>"></input>
			
					</logic:notEmpty>
							<logic:notEmpty name="status">
					<bean:write name="status"></bean:write>
					</logic:notEmpty>
					
					</div>
					
					
	<input type="hidden" value="" id="tempId" name="courseId">
			Description<textarea rows="10" cols="30" name='notes' id='description' ><logic:notEmpty name="mynotes"><logic:iterate id="mynotes" name="mynotes"><bean:write name="mynotes" property="notes"></bean:write></logic:iterate></logic:notEmpty></textarea><br>
	
			<input type='submit' value='save' id="prodSubmit" >
			
	</form>
     
</div>
</div>




	<!-- header logo: style can be found in header.less -->
	<header class="header"> <a href="dynamic.jsp"
		class="logo"> <!-- Add the class icon to your logo image or logo icon to add the margining -->
		<img src="images/LEARNEZYLatest-LOGO.png" style="margin-top: 12px;">
	</a> <!-- Header Navbar: style can be found in header.less --> <nav
		class="navbar navbar-static-top" role="navigation"> <!-- Sidebar toggle button-->
	<a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas"
		role="button"> <span class="sr-only">Toggle navigation</span> <span
		class="icon-bar"></span> <span class="icon-bar"></span> <span
		class="icon-bar"></span>
	</a>
	<div class="navbar-right">
		<ul class="nav navbar-nav">
			<!-- Messages: style can be found in dropdown.less-->
			<%
				Object photo = "";
				Object userName = "";
				try {
					photo = ((LoginForm) session.getAttribute("userDetail")).getProfilePhoto();
					;
					userName = ((LoginForm) session.getAttribute("userDetail")).getUserName();
					;
				} catch (NullPointerException e) {
					e.printStackTrace();
				}
			%>



			<!-- User Account: style can be found in dropdown.less -->
			<li class="dropdown user user-menu"><a href="#"
				class="dropdown-toggle" data-toggle="dropdown"> <i
					class="glyphicon glyphicon-user"></i> <span>My Account<i
						class="caret"></i></span>
			</a>
				<ul class="dropdown-menu">
					<!-- User image -->
					<li class="user-header bg-light-blue">
						<%
							if (photo != null && !photo.equals("")) {
						%> <img
						src="<%=request.getContextPath()%>/imageUpload/<%=photo%>"
						class="img-circle" alt="User Image" /> <%
 	} else {
 %> <img
						src="images/empty_profile_pthoto.png" class="img-circle"
						alt="User Image" /> <%
 	}
 %>

						<p>
							<%=userName%>

						</p>
					</li>
					<!-- Menu Body -->

					<!-- Menu Footer-->
					<li class="user-footer">
						<div class="pull-left">
							<a
								href="<%=request.getContextPath()%>/studentProfile.do?action=getProfileData"
								class="btn btn-default btn-flat">Profile</a>
						</div>
						<div class="pull-right">
							<a href="Logout.do" class="btn btn-default btn-flat">Sign out</a>
						</div>
					</li>
				</ul></li>
		</ul>
	</div>
	</nav> </header>
	<div class="wrapper row-offcanvas row-offcanvas-left">
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="left-side sidebar-offcanvas"> <!-- sidebar: style can be found in sidebar.less -->
		<section class="sidebar"> <!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<%
					if (photo != null && !photo.equals("")) {
				%>
				<img src="<%=request.getContextPath()%>/imageUpload/<%=photo%>"
					class="img-circle" alt="User Image" />
				<%
					} else {
				%>
				<img src="images/empty_profile_pthoto.png" class="img-circle"
					alt="User Image" />
				<%
					}
				%>
			</div>
			<div class="pull-left info">
				<p>
					Hello,
					<%=userName%></p>

				<!-- <a href="#"><i class="fa fa-circle text-success"></i>  Online  -->
				</a>
			</div>
		</div>
		<!-- search form --> <!--  <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>--> <!-- /.search form --> <!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu">
			<li class="active"><a> <i class="fa fa-dashboard"></i> <span>DASHBOARD</span>
			</a></li>
			<li><a href="javascript:void(0)" id="courseMenu"> <span>COURSES</span>
			</a></li>
			<li><a href="javascript:void(0)" id="mynotes"> <span>My Notes</span>
			</a></li>
			
			<li><a
				href="<%=request.getContextPath()%>/studentProfile.do?action=getProfileData">
					<span>PROFILE</span>

			</a></li>
			<li><a href="javascript:void(0)" id="getCertificateMenu"> <span>
						GET CERTIFICATE</span>

			</a> <!-- <ul class="treeview-menu">
                                <li><a href="pages/UI/general.html"><i class="fa fa-angle-double-right"></i> General</a></li>
                                <li><a href="pages/UI/icons.html"><i class="fa fa-angle-double-right"></i> Icons</a></li>
                                <li><a href="pages/UI/buttons.html"><i class="fa fa-angle-double-right"></i> Buttons</a></li>
                                <li><a href="pages/UI/sliders.html"><i class="fa fa-angle-double-right"></i> Sliders</a></li>
                                <li><a href="pages/UI/timeline.html"><i class="fa fa-angle-double-right"></i> Timeline</a></li>
                            </ul>--></li>
			<li><a href="javascript:void(0)" id="notificationMenu"> <span>NOTIFICATIONS</span>

			</a> <!--   <ul class="treeview-menu">
                                <li><a href="pages/forms/general.html"><i class="fa fa-angle-double-right"></i> General Elements</a></li>
                                <li><a href="pages/forms/advanced.html"><i class="fa fa-angle-double-right"></i> Advanced Elements</a></li>
                                <li><a href="pages/forms/editors.html"><i class="fa fa-angle-double-right"></i> Editors</a></li>
                            </ul>--></li>
			<li><a href="javascript:void(0)" id="assignmentMenu"> <span>ASSIGNMENTS</span>

			</a> <!-- <ul class="treeview-menu">
                                <li><a href="pages/tables/simple.html"><i class="fa fa-angle-double-right"></i> Simple tables</a></li>
                                <li><a href="pages/tables/data.html"><i class="fa fa-angle-double-right"></i> Data tables</a></li>
                            </ul>--></li>
			<li><a href="javascript:void(0)" id="knowledgeBankMenu"> <span>KNOWLEDGE
						BANK</span>

			</a></li>
			<!--  <li><a href="javascript:void(0)" id="compiler"> <span>ONLINE JAVA COMPILER</span>-->

			</a></li>
			<li><a href="javascript:void(0)"> <i class="fa fa-envelope"></i>
					<span>PAYMENT HISTORY</span>

			</a></li>
			<!--   <li >
                            <a href="AddNewCourse.do" >
                             <span>SELECT NEW COURSE</span>
                                
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="pages/examples/invoice.html"><i class="fa fa-angle-double-right"></i> Invoice</a></li>
                                <li><a href="pages/examples/login.html"><i class="fa fa-angle-double-right"></i> Login</a></li>
                                <li><a href="pages/examples/register.html"><i class="fa fa-angle-double-right"></i> Register</a></li>
                                <li><a href="pages/examples/lockscreen.html"><i class="fa fa-angle-double-right"></i> Lockscreen</a></li>
                                <li><a href="pages/examples/404.html"><i class="fa fa-angle-double-right"></i> 404 Error</a></li>
                                <li><a href="pages/examples/500.html"><i class="fa fa-angle-double-right"></i> 500 Error</a></li>
                                <li><a href="pages/examples/blank.html"><i class="fa fa-angle-double-right"></i> Blank Page</a></li>
                            </ul> 
                        </li> -->
			<li><a href="javascript:void(0)" id="resourcesMenu"> <span>RESOURCES</span>

			</a></li>
			<li><a href="javascript:void(0)" id="linksMenu"> <span>LINKS</span>

			</a></li>
			<li><a href="javascript:void(0)" id="settings"> <span>SETTINGS</span>

			</a></li>
			<li><a href="javascript:void(0)" id="classSession"> <span>SESSION
						LIVE</span>

			</a></li>
				<!-- <li><a href="javascript:void(0)" id="rtings"> <span>RATINGS
						</span>

			</a></li>-->
			
			<!--  <li><a href="<%=request.getContextPath() %>/review.do?action=list" id="review"> <span>REVIEW
						</span>

			</a></li>-->
			
			
			 <li>
                             <a href="<%=request.getContextPath() %>/Rating.do"><span>RATINGS</span>
                             
                            </a>
                           
                        </li>
                         <li>
                           <a href="<%=request.getContextPath() %>/Reviews.do">
                                
                                <span>REVIEWS</span>
                             
                            </a>
                           
                        </li>
			
			<li><a href="javascript:void(0)" id="audio"> <span>Audio
						</span>

			</a></li>
		</ul>
		</section> <!-- /.sidebar --> </aside>

		<!-- Right side column. Contains the navbar and content of the page -->
		<aside class="right-side"> <!-- Content Header (Page header) -->
		<section class="content-header">
		<h1>STUDENT DASHBOARD</h1>
		
		<html:form action="studentRegistration.do?action=viewdetails1" styleId="form">
		
		
		<input type="text" name="java" placeholder="search course" id="search1" ></input>
<input type="submit" value="search course" id="search2"></input>

</html:form>


<div id="app"></div>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
		</section> <!-- Main content --> <section class="content"> <!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-aqua">
					<div class="inner">
						<h3 id="courseCount"></h3>
						<p>
							COURSES <br />COMPLETED
						</p>
					</div>
					<div class="icon">
						<img src="images/course.png" width="80" height="79" />
					</div>


				</div>
			</div>
			<!-- ./col -->
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-green">
					<div class="inner" style="height: 125px;">
						<h3 id="testCount"></h3>
						<p>
							TEST <br /> COMPLETED
						</p>
					</div>
					<div class="icon">
						<img src="images/topic.png" width="50" height="50" />
					</div>

				</div>
			</div>
			<!-- ./col -->
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<a href="javascript:void(0);" id="assignment">
					<div class="small-box bg-yellow">
						<div class="inner">
							<h3 id="assignmentCount"></h3>
							<p>
								ASSIGNMENTS <br /> COMPLETED
							</p>
						</div>
						<div class="icon">
							<img src="images/topic.png" width="80" height="79" />
						</div>

					</div>
				</a>
			</div>
			<!-- ./col -->

			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-red">
					<div class="inner" style="height: 125px;">
						<h3 id="achievementCount"></h3>
						<p>ACHIEVEMENTS</p>
					</div>

					<div class="icon">
						<img src="images/acheivement.png" width="80" height="79" />
					</div>
				</div>
			</div>
			<!-- ./col -->
		</div>
		<!-- /.row --> <!-- top row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable"></div>
			<!-- /.col -->
		</div>
		<!-- /.row --> <!-- Main row -->
		<div class="row">
			<!-- Left col -->
			<section class="col-lg-12 connectedSortable"> <!-- Box (with bar chart) -->
			<div class="box box-danger" id="loading-example">
				<div class="box-header">

					<!--    <i class="fa fa-cloud"></i>-->

					<h3 style="margin-left: 10px;" class="box-title" id="tabTitle">ASSIGNMENTS</h3>
				</div>
				<!-- /.box-header -->


				<div class="box-body no-padding">
					<div style="margin-left: 6px" class="row">
						<div style="margin-left: 20px;" class="assign" id="courseTable">


						</div>
						<!--assign-->
 					</div>
					<!-- /.pad -->
				</div>
				
				
				
				
				
				<!-- <div class="box-body no-padding">
					<div style="margin-left: 6px" class="row">
						<div style="margin-left: 20px;" class="assign" id="ratingTable">
										
										
	<div id="jRate" style="height:50px;width: 200px;"></div>
	<div> <input type="text" id="rat"></div>
	<button id="btn-click" style="margin:0 500px;">Reset Me</button>

						</div>
						assign

					</div>
					/.pad
				</div> -->
				
				
				
				
				
			</div>
			<!-- /.row - inside box --> </section>
		</div>
		<!-- /.box-body --> </section><!-- right col -->
	</div>
	<!-- /.row (main row) -->


	</aside>
	<!-- /.right-side -->
	<aside class="right-side"> <!-- Content Header (Page header) -->

	<div class="col-lg-4 col-xs-7">
		<!-- small box -->

	</div>
	<!-- ./col -->
	<div class="col-lg-4 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-green1">
			<div class="top">
				<h4 class="course">NEWS AND EVENTS</h4>
			</div>
			<div class="inner" style="height: 129px;">

				<p>

					<marquee bahaviour="alternate" direction="up" scrollamount="4"
						height="100" id="notificationMarquee"></marquee>
				</p>
			</div>


		</div>
	</div>
	<!-- ./col -->
	<div class="col-lg-4 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-green1">
			<div class="top">
				<h4 class="course">ONLINE CHAT</h4>
			</div>
			<div class="inner" style="height: 129px;">

				<p></p>
			</div>


		</div>
	</div>
	<!-- ./col --> </section> </aside>
	 
	<!-- ./wrapper -->
	<div style="clear: both"></div>
	<div class="footer1">
		<div class="wrap">
			<div class="copy-right">
				<p>
					Itech Training, Copyright 2015, All Rights Reserved. Designed &
					Developed by <a href="http://www.itechsolutions.in/" target="_blank">Itech
						Solutions</a>
				</p>
			</div>
			<div class="social-icons">
				<ul>
					<li><a target="_blank" href="https://www.twitter.com"><i
							class="fa fa-twitter"></i></a></li>
					<li><a target="_blank" href="https://www.facebook.com"><i
							class="fa fa-facebook"></i></a></li>
					<li><a target="_blank" href="https://plus.google.com"><i
							class="fa fa-google-plus"></i></a></li>
					<li><a target="_blank" href="https://www.youtube.com"><i
							class="fa fa-youtube"></i></a></li>
					<li><a target="_blank" href="https://www.linkedin.com"><i
							class="fa fa-linkedin"></i></a></li>
				</ul>
			</div>

		</div>
	</div>


	<!-- add new calendar event modal -->

        
	<script src="<%=request.getContextPath() %>/js/jRate.js"></script>
	<!-- jQuery UI 1.10.3 -->
	<script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
	<!-- Morris.js charts  -->
	<script	src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
	 <!-- Sparkline -->
	<script src="js/plugins/sparkline/jquery.sparkline.min.js"	type="text/javascript"></script>
	<script src="js/plugins/morris/morris.js"	type="text/javascript"></script>
	<!-- jvectormap -->
	<script src="js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"	type="text/javascript"></script>
	<script src="js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"	type="text/javascript"></script>
	<!-- fullCalendar -->
	<script src="js/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
	<!-- jQuery Knob Chart -->
	<script src="js/plugins/jqueryKnob/jquery.knob.js"	type="text/javascript"></script>
	<!-- daterangepicker -->
	<script src="js/plugins/daterangepicker/daterangepicker.js"	type="text/javascript"></script>
	<!-- Bootstrap WYSIHTML5  -->
	<script	src="js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>  
	<!-- iCheck -->
	<script src="js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>

	<!-- AdminLTE App -->
	<script src="js/AdminLTE/app.js" type="text/javascript"></script>

	<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
	<script src="js/AdminLTE/dashboard.js" type="text/javascript"></script>
	<script src="mediaPlayers/html5media.min.js"></script>
	</body>
</html>