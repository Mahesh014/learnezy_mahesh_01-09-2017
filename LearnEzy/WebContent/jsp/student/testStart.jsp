<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<title>Itech Training :: Test</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300' rel='stylesheet' type='text/css'>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/cmsstyle.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/carousel.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>
   

<script src="js/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="js/bootstrap.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/owl.carousel.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv='cache-control' content='no-cache'>
<meta http-equiv='expires' content='0'>
<meta http-equiv='pragma' content='no-cache'>
<title>Take TEst</title>
<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="js/jquery.countdown.js"></script>
<script type="text/javascript" src="js/jquery.countdown.min.js"></script>
<script type="text/javascript">
var upgradeTime = <%= request.getAttribute("hour") %>;
var seconds = upgradeTime;
function timer() {
    var days        = Math.floor(seconds/24/60/60);
    var hoursLeft   = Math.floor((seconds) - (days*86400));
    var hours       = Math.floor(hoursLeft/3600);
    var minutesLeft = Math.floor((hoursLeft) - (hours*3600));
    var minutes     = Math.floor(minutesLeft/60);
    var remainingSeconds = seconds % 60;
    if (remainingSeconds < 10) {
        remainingSeconds = "0" + remainingSeconds; 
    }
    document.getElementById('countdown').innerHTML = hours + ":" + minutes + ":" + remainingSeconds;
    if (seconds == 0) {
        clearInterval(countdownTimer);
        document.getElementById('countdown').innerHTML = "Test Time is Completed!";
        $.ajax({
			url : "Test.do?action=testTimeComplete",
			type : "GET",
			dataType : "json",
			success : function(obj) {
				$("#totalQuestion").html(obj.totalQuestion);
				$("#rightAnswer").html(obj.rightAnswer);
				$("#rounghAnswer").html(obj.rounghAnswer);
				$("#tblQuestion").hide();
				$("#resultsTable").show();
			}
		});
    } else {
        seconds--;
    }
}
var countdownTimer = setInterval('timer()', 1000);
</script>

<script type="text/javascript">
	$(document).ready(function() {
		var qNo=1;
		//alert(qNo);
				$.ajax({
					url : "Test.do?action=testStarted",
					type : "GET",
					dataType : "json",
					success : function(obj) {
						//alert(obj.Question)
						$("#Question").html("Q"+qNo+". "+obj.Question);
						$("#AnswerA").html(obj.AnswerA);
						$("#AnswerB").html(obj.AnswerB);
						$("#AnswerC").html(obj.AnswerC);
						$("#AnswerD").html(obj.AnswerD);
						$("#CorrectAnswer").html(obj.CorrectAnswer);
						$("#DescriptiveAns").html(obj.DescriptiveAns);
						$("#questionId").val(obj.questionId);
						$("#resultsTable").hide();
					}
				});
				
				$("#btnNext").click(function() {
					var check=false;
					var ans="";
				 	$(".radioQuestions").each(function() {
						if (this.checked) {	ans=$(this).val(); check=true; } 
				 	});
				 	if(check){
					//alert(ans);
							$.ajax({
								url : "Test.do?action=nextQuestion",
								type : "GET",
								dataType : "json",
								data : { questionId : $("#questionId").val(), ans : ans },
								success : function(obj) {
									if (obj.qFlag==1) {
										//alert(obj.Question)
										qNo=qNo+1;
										$("#Question").html("Q"+qNo+". "+obj.Question);
										$("#AnswerA").html(obj.AnswerA);
										$("#AnswerB").html(obj.AnswerB);
										$("#AnswerC").html(obj.AnswerC);
										$("#AnswerD").html(obj.AnswerD);
										$("#CorrectAnswer").html(obj.CorrectAnswer);
										$("#DescriptiveAns").html(obj.DescriptiveAns);
										$("#questionId").val(obj.questionId); 
										$(".radioQuestions").each(function() {
											$(this).attr('checked', false);
									 	});
										$("#resultsTable").hide();
									}									
									if (obj.resultFlag==1) {
										$("#totalQuestion").html(obj.totalQuestion);
										$("#rightAnswer").html(obj.rightAnswer);
										$("#rounghAnswer").html(obj.rounghAnswer);
										$("#tblQuestion").hide();
										$("#resultsTable").show();
										$("#countdown").remove();
										
									}
								}
							});
				 	}else{
				 		
				 		alert("Please Select Answer")
				 	}
			});
	});
</script>
<style>

 
</style>
</head>
<body>
	<!-- Start Header -->
		<jsp:include page="/jsp/common/student_header.jsp"></jsp:include>
	<!-- End Header -->
   
    <div class="clear"></div>
   <!-- Start Main Content -->
	 <div class="main" id="tblQuestion">	 
	 	<h2>Questions</h2>
      
     <div class="container-fluid">
<div class="jumbotron">
	<p style="margin-bottom:30px;" id="Question" > </p><input type="hidden" id="questionId" value="">
	     
	<div class="radio"><label><input value="A" type="radio" id="radioOptionA" name="radioQuestions" class="radioQuestions" > <span id="AnswerA"></span></label></div>
	<div class="radio"><label><input value="B" type="radio" id="radioOptionB" name="radioQuestions" class="radioQuestions"> <span id="AnswerB"></span></label></div>
	<div class="radio"><label><input value="C" type="radio" id="radioOptionC" name="radioQuestions" class="radioQuestions"> <span id="AnswerC"></span></label></div>
	<div class="radio"><label><input value="D" type="radio" id="radioOptionD" name="radioQuestions" class="radioQuestions"> <span id="AnswerD"></span></label></div>
	<br>
	<input type="button" id="btnNext" value="Next" />
 
</div>
</div>
</div>
<div class="main"id="resultsTable" align="center" >	 
	 	<h2>Resultsssssss <logic:notEmpty name="courseName">
								<bean:write name="courseName" />
							</logic:notEmpty></h2>
 <table style="margin-top: 25px;">
 <tr height="35" ><td>Total :</td> <td><span id="totalQuestion"></span></td></tr>
 <tr height="35"><td>Correct Answers :</td> <td> <span id="rightAnswer"></span></td></tr>
 <tr height="35"><td> Wrong Answers :</td> <td><span id="rounghAnswer"></span></td></tr>
 
 </table>
 
 
</div>

   <!-- End Main Content -->
	   
  <!-- Start Footer -->
     <jsp:include page="/jsp/common/student_footer.jsp" /> 
  <!-- End Footer -->
  </body>
</html>