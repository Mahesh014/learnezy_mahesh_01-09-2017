<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-nested.tld" prefix="nested"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Itech Training :: Test</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300'
	rel='stylesheet' type='text/css'>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/cmsstyle.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/font-awesome.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/carousel.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css"
	media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />


<script src="js/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="js/bootstrap.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/owl.carousel.js" type="text/javascript"></script>

<script type="text/javascript">
	// Login Form
	$(function() {
		var button = $('#loginButton');
		var box = $('#loginBox');
		var form = $('#loginForm');
		button.removeAttr('href');
		button.mouseup(function(login) {
			box.toggle();
			button.toggleClass('active');
		});
		form.mouseup(function() {
			return false;
		});
		$(this).mouseup(function(login) {
			if (!($(login.target).parent('#loginButton').length > 0)) {
				button.removeClass('active');
				box.hide();
			}
		});
	});

	function go() {
		alert("hai");
	}
</script>
   
</head>
<body>
	<!-- Start Header -->
		<jsp:include page="/jsp/common/student_header.jsp"></jsp:include>
	<!-- End Header -->
	<div class="clear"></div>
	<!-- Start Main Content -->
	<div class="main">
		<h2>Test Page</h2>
		<!--  <div class="admin">
	      <ul>
          <li class="box effect8">Role Master</li>
          <li class="box effect8">User Master</li>
          <li class="box effect8">Course Master</li>
          </ul>
          </div>
            <div class="admin">
          <ul>
          
          <li class="box effect8">Topic Master</li>
          <li class="box effect8">Question Master New</li>
          <li class="box effect8">Pass Percent Master</li>
          </ul>
	 </div>-->
		<!--<table width="960" class="admin1">
     <tr>
     <td class="box effect8">Role Master</td>
     <td class="box effect8">User Master</td>
     <td class="box effect8">Course Master</td>
     </tr>
     </table>
     <table width="960" class="admin1">
     <tr>
     <td class="box effect8">Topic Master</td>
     <td class="box effect8">Question Master</td>
     <td class="box effect8">Pass Percent Master</td>
     </tr>
     </table>-->
		<div class="adminmaster">
			<div class="admin">
				<logic:notEmpty name="testList">
				<logic:iterate id="testList" name="testList" indexId="index">
					<div class="box effect8">
						<a href='Test.do?action=takeTest&amp;testId=<bean:write  property="testId"  name="testList"/>&amp;courseId=<bean:write  property="courseId"  name="testList"/>'><bean:write	name="testList" property="tname" /></a>
					</div>
					 
					</logic:iterate>
					</logic:notEmpty>
					<logic:empty name="testList">
					<h2>Test Not Available for Given Course</h2>
					</logic:empty>
			</div>
		</div>
		<!-- admin master -->
		<div class="clear"></div>
	</div>
	 

	<!-- End Main Content -->

	 <!-- Start Footer -->
     <jsp:include page="/jsp/common/student_footer.jsp" /> 
  <!-- End Footer -->
</body>
</html>
