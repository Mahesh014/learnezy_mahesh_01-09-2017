<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-nested.tld" prefix="nested"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Itech Training :: Add New Course</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300'
	rel='stylesheet' type='text/css'>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/cmsstyle.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/font-awesome.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/carousel.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css"
	media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />


<script src="js/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="js/bootstrap.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/owl.carousel.js" type="text/javascript"></script>
<script type="text/javascript" >

$(document).ready(function() {
$(".courseSelectOption").click( function() {
 	var obj=$(".courseSelect option:selected");
  	var courseId="";
  	var serviceTax=$("#serviceTax").val();
	$.each(obj, function(){ 
    	courseId= courseId +","+ $(this).val();
    });
    courseId= courseId.substring(1, courseId.length);
    //alert(courseId);
    if(courseId!=""){
    	$.ajax({
            url: "CourseMaster.do?action=courseFees",
            type: "GET",
            dataType: "json",
            data: {
                courseId: courseId
            },
            success: function(res) {
            	var courseList='<div class="data-container"  ><table border="1" cellspacing="10" class="courseListTable" cellpadding="5"   width="100%"  style="text-align:center;">'+
            		'<tr class="courseDispTr"> <td>Selected Course</td><td>Cost in Rs</td></tr>';
            	var subAmount=0;
            	var taxAmount=0;
            	var total=0;
            	var disAmount=0;
                $.each(res,function(i,obj){
					courseList=courseList+'<tr ><td>'+obj.courseName+' </td><td> Rs.'+obj.fees+'.00</td></tr>';	
					//alert(courseList);
					subAmount=parseInt(subAmount)+parseInt(obj.fees);
					if(obj.totalDiscountAmount!=0){ disAmount= parseInt(disAmount)+parseInt(parseInt(obj.fees)*(parseInt(obj.totalDiscountAmount)/parseInt(100)))  };
				});
                taxAmount=parseInt(parseInt(subAmount)*(parseInt(serviceTax)/parseInt(100)));
                total=parseInt(subAmount)+parseInt(taxAmount)-parseInt(disAmount);
                courseList=courseList+'<tr><td colspan="2"  height="10"></td></tr><tr><td> Sub Total </td><td>Rs. '+subAmount+'.00</td></tr><tr  ><td> Service Tax @ '+serviceTax+'% </td><td><span style="color:#0DD80D;font-size:16px;font-weight:bold;">+</span> Rs. '+taxAmount+'.00</td></tr><tr><td> Total Amount Discount@ </td><td> <span style="color:#CB0C04;font-size:16px;font-weight:bold;">-</span> Rs. '+disAmount+'.00</td></tr><tr  ><td> <font style="font-weight:bold;">Total Amount</font></td><td> <font style="font-weight:bold;">Rs. '+total+'.00</font></td></tr></table></div>';	
                //alert(courseList);
                
                $('#courseList').html(courseList);
                $('#subAmount').val(subAmount);
                $("#tax").val(taxAmount);
                $("#totalAmount").val(total);
                $("#discountAmount").val(disAmount);
				
            }
        });
    }
});



$("#clear").click( function() {
var courseList='<div class="data-container"  ><table border="1" cellspacing="10" class="courseListTable" cellpadding="5"   width="100%"  style="text-align:center;">'+
'<tr class="courseDispTr"> <td>Selected course</td><td>Cost in Rs</td></tr> <tr><td colspan="2">Select the course</td></tr></table>';
$(".courseSelect option:selected").removeAttr("selected");
$('#courseList').html(courseList);
$('#subAmount').val(0);
$("#totalAmount").val(0);
$("#tax").val(0);
$("#discountAmount").val(0);
});

$("#validateSubmit").click( function() {
	if ($("#courses").val() ==null) {
		alert("Please select at least one course");
		return false;
	}
});

});



</script>
 <style>
 .row {
 	margin-top: 10px;
 }
 
 .row label{
 	margin-bottom: 5px;
 }
 .login a{  width: 67px;
  float: right;
  color: #000;
  margin-top: 22px;
  border: 1px solid #FFA500;
  padding: 6px;
  border-radius: 8px;
 }
  .courseDispTr{  
  background-color: #59bbdf;
 }
 .courseSelect{
	width:200px; 
	padding:2px;
	hight:50px;
 }
  .courseListTable{
 border: 1px;
 border-width: 12px;
 }
 .courseListTable td{
 height: 30px;
 vertical-align: middle;
 }

.sign {
border:1px solid #D0DDE1;
padding:14px;
border-radius:2px;
}
legend{width:20%;}

label{font-size:14px;}

.fa-lg{color:green;}

#captchaText{
background-image: url("..img/BG1.png);
}
#leftContainer{
width: 25%;
float: left;
}
#rightContainer{
width: 65%;
float: right;
}
</style>
</head>
<body>
	<!-- Start Header -->
		<jsp:include page="/jsp/common/student_header.jsp"></jsp:include>
	<!-- End Header -->
	<div class="clear"></div>
	<!-- Start Main Content -->
	 <div class="main" style="min-height:450px;">	
	 <h2>Select Course</h2>
	<div style="padding: 10px 60px 0px 60px;">
<html:form action="AddNewCourse.do?action=addNewCourse" method="post" styleClass="register" styleId="register"   enctype="multipart/form-data">
				 <div style="width: 25%;float: left;">		
										 
								
								<div   class="form-title" style="width: 725px;text-align: center;" >
									<b style="color: red;"><logic:notEmpty name="status"><bean:write name="status"/> </logic:notEmpty></b>
								</div>
													<label>List of Course <font style="color: red;">*</font> :</label>
												
													
								<html:select styleClass="courseSelect" property="courseid"  multiple="true" styleId="courses">
										<html:options styleClass="courseSelectOption" collection="courselist" property="courseid"	labelProperty="coursename" />
								</html:select>
								<a href="javaScript:void(0)" id="clear" style="font-style: italic;color: blue;font-size: 12px;margin-top: 5px;">  clear </a>
						</div>
<div style="width: 70%;float: left;padding: 25px;">		
								<div  id="courseList">
								
								<table border="1" cellspacing="10" cellpadding="5" width="100%" class="courseListTable"  style="text-align:center;">
                                        <tr class="courseDispTr">
                                        <td>Selected course</td>
                                        <td>Cost in Rs</td>
                                        </tr>
                                         <tr ><td colspan="2">Select the course</td></tr>
                                         <!--  <tr><td> Sub Total </td><td>800</td></tr>
                                         <tr><td>Service Tax @ 14% <span style="color:#0DD80D;font-size:16px;">+</span> </td><td>140</td></tr>
                                         <tr><td> Total Amount Discount@ <span style="color:#CB0C04;font-size:16px;">-</span></td><td> 20%</td></tr>
                                         <tr><td> Total Amount</td><td> 940</td></tr> -->
                               </table>
								
							</div>
									<input type="hidden"  name="subAmount"  id="subAmount"  value="0"    />	
									<input type="hidden"  name="totalAmount"  id="totalAmount"  value="0"    />	 
									<input type="hidden"  name="tax"  id="tax"  value="0"    />	 
									<input type="hidden"  name="discountAmount"  id="discountAmount"  value="0"    />
									<input type="hidden"  name="serviceTax"  id="serviceTax"  value="<bean:write name="serviceTax" />"    />	 
								
							
						
							<button type="submit"  id="validateSubmit" style="float: right;margin-top: 25px;"><span> Select & Make Payment </span></button>
					
 </div>
					 </html:form>

</div>

</div>

	<!-- End Main Content -->
 
	 <!-- Start Footer -->
     <jsp:include page="/jsp/common/student_footer.jsp" /> 
  <!-- End Footer -->
</body>
</html>