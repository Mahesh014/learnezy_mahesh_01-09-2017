
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Itech Training :: Stduent Reports</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv='cache-control' content='no-cache'>
<meta http-equiv='expires' content='0'>
<meta http-equiv='pragma' content='no-cache'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300'	rel='stylesheet' type='text/css'>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"	media="all" />
<link href="css/cmsstyle.css" rel="stylesheet" type="text/css"	media="all" />
<link href="css/font-awesome.css" rel="stylesheet" type="text/css"	media="all" />
<link href="css/carousel.css" rel="stylesheet" type="text/css"	media="all" />
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css"	media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="src/DateTimePicker.css" />


<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="src/DateTimePicker.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/date/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css"	href="<%=request.getContextPath()%>/date/jquery-ui.css" />
<link href="dataTable/css/jquery.dataTables_themeroller.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.min.css"	rel="stylesheet" type="text/css" media="all" />
<script src="dataTable/js/jquery.dataTables.js"	type="text/javascript"></script>
<script src="dataTable/js/jquery.dataTables.min.js"	type="text/javascript"></script>
<script src="dataTable/js/settingDataTable.js" 	type="text/javascript"></script>

<link rel="stylesheet" type="text/css"	href="/E-Learning/date/jquery-ui.css" />
<script type="text/javascript" src="/E-Learning/date/jquery-ui.js"></script>

<script type="text/javascript">
$(function(){
	$("#hiddentable").hide();
    $("#fromDate").datepicker({dateFormat: 'dd/mm/yy',changeMonth: true, changeYear: true});
    $("#toDate").datepicker({dateFormat: 'dd/mm/yy',changeMonth: true, changeYear: true});
});

function DateCheck(){
  var StartDate= document.getElementById('fromDate').value;
  var EndDate= document.getElementById('toDate').value;
  var eDate = new Date(EndDate);

  var sDate = new Date(StartDate);
  if(StartDate!= '' && EndDate!= '' && sDate > eDate){
    alert("Please ensure that the End Date is greater than or equal to the Start Date.");
    document.getElementById('toDate').value="";
    return false;
  }
}


$(document).ready(function()

		{
$("#excelupload").click(function()
		{

var tab= $('#hiddentable').prop('outerHTML');
window.location.href="jsp/reports/studentexcel.jsp?table="+tab+"";


		});



		});
</script>

<script type="text/javascript">
function validate() {
	
	if(document.forms[0].userid.value == "-1" && document.forms[0].fromDate.value == "" && document.forms[0].toDate.value == "" ){
		alert("Please Select the User Name OR From Date and To Date ..!");
		document.forms[0].userid.focus();
		return false;
	}else		
	if(document.forms[0].userid.value != "-1" ){
		return true;
	}else
	if(document.forms[0].fromDate.value == "" ){
		alert("Please Select the from Date ..!");
		document.forms[0].fromDate.focus();
		return false;
	}else
	if(document.forms[0].toDate.value == "" ){
		alert("Please Select the to Date ..!");
		document.forms[0].toDate.focus();
		return false;
	} 
}
function printDiv(divName) {
	 var divToPrint = document.getElementById(divName);
	   var popupWin = window.open('', '_blank', 'width=300,height=300');
	   popupWin.document.open();
	   popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
	    popupWin.document.close();
 
}
</script>
</head>
<body>


	<!-- Start Header -->
	<jsp:include page="/jsp/common/reports_header.jsp"></jsp:include>
	<!-- End Header -->



	<div class="main" style="min-height: 450px;">
		<h2>Student Reports</h2>
		<html:form action="StudentReports.do?action=getList">
			<input type="hidden" name="assignmentId" />
			<table border="0" width="80%" align="center" class="role">
				
				<tr>
					<td colspan="6" align="center"><div id="status">
							<logic:notEmpty name="status">
								<font color="blue"><bean:write name="status"></bean:write></font>
							</logic:notEmpty>
						</div></td>
				</tr>
				<tr>
					<td><label>Student </label></td>
					<td><html:select property="userid" styleId="userid">
			<html:option value="-1">-Select Student-</html:option>
			<html:option value="0">All</html:option>
			<html:options collection="studentList" property="userid"
				labelProperty="firstName" />
		</html:select></td>


				 
					<td><label>From Date</label></td>
					<td><input type="text" name="fromDate" id="fromDate" readonly="readonly"	style="padding: 0px;width: 90px;" ></td>
					<td><label>TO Date </label></td>
					<td><input type="text" name="toDate" id="toDate" readonly="readonly" onchange="DateCheck();"	style="padding: 0px;width: 90px;">
				</tr>

				<tr>
					<td colspan="6" align="center"><input type="submit"
						value="Submit" onclick="return validate();"></td>
				</tr>
			</table>
		</html:form>

<div class="dataGridDisplay"  >
<table id="normalTable" class="display" cellspacing="0" width="100%" align="center" >
<thead>
	<tr>
		<th>SI No.</th>
		<th>Student Name</th>
		<th>Register Date</th>
		<th>Course Selected</th>
		<th>Fees Paid</th>
	</tr>
</thead>

	<logic:notEmpty name="studentDetailsList">
	<logic:iterate id="studentDetails" name="studentDetailsList" indexId="index">
		<tbody>
		<tr>
			<td><%=index + 1%></td>
			<td><bean:write name="studentDetails" property="firstName" /></td>
			<td><bean:write name="studentDetails" property="regDate" /></td>
			<td><bean:write name="studentDetails" property="courseName" /></td>
			<td><bean:write name="studentDetails" property="fees" /></td>
		</tr>
				</tbody>
		
	</logic:iterate>
	</logic:notEmpty>
	
		</table>
		
		<table id="hiddentable" border="1" >
	<tr>
		<th>SI No.</th>
		<th>Student Name</th>
		<th>Register Date</th>
		<th>Course Selected</th>
		<th>Fees Paid</th>
	</tr>


	<logic:notEmpty name="studentDetailsList">
	<logic:iterate id="studentDetails" name="studentDetailsList" indexId="index">
		
		<tr>
			<td><%=index + 1%></td>
			<td><bean:write name="studentDetails" property="firstName" /></td>
			<td><bean:write name="studentDetails" property="regDate" /></td>
			<td><bean:write name="studentDetails" property="courseName" /></td>
			<td><bean:write name="studentDetails" property="fees" /></td>
		</tr>
				
		
	</logic:iterate>
	</logic:notEmpty>
	
		</table>
		
		
		
		
		<table class="display">
		
		<tr>
		<td>
		<button type="button" id="buttonid" onclick="printDiv('hiddentable')">Print</button>
		</td>
		<td>
		<button type="button" id="excelupload" >ExporttoExcel</button>
		</td>
		
		
		
		</tr>
		</table>
			 
	</div>
	</div>
	<!-- End Main Content -->

	<!-- Start Footer -->
	<jsp:include page="/jsp/common/masterdata_footer.jsp"></jsp:include>
	<!-- End Footer -->
</body>
</html>
 