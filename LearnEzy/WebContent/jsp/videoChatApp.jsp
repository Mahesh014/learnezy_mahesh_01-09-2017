<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Itech Training :: Video Chat</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300' rel='stylesheet' type='text/css'>
<link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/cmsstyle.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/carousel.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>

<script src="<%=request.getContextPath() %>/js/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/owl.carousel.js" type="text/javascript"></script>
<style type="text/css">
.wbFrame {
width: 80%;
height: 100%;
padding: 10px;
}
.wbDiv{
 margin: 0 100px;
 text-align: center;
}
</style>
</head>
<body>
 <!-- Start Header -->
       
 
<%@page import="java.util.Date"%>
 <%@page import="com.itech.elearning.forms.LoginForm" %>
<%
	  												Object photo="";
                                                	Object userName="";
	 														 try{
	 															//photo=((LoginForm) session.getAttribute("userDetail")).getProfilePhoto();;
	 															userName=((LoginForm) session.getAttribute("userDetail")).getUserName();;
	 															 }catch(NullPointerException e){
    	  																		e.printStackTrace();
      															}
	 														%> 
<div class="header">	
   	 	    <div class="header-top">
   	 	      <div class="wrap"> 
   	 	    	 <div class="header-top-left">
   	 	    	 	<p>ph: +91-9611421111</p>
   	 	    	 </div>
   				  <div class="header-top-right">
				        <ul>
				            <li><a   >Careers</a></li>
				            <li  class="login">
				              <div id="loginContainer">
				            	   <a href="#" id="loginButton"><span>Join Us</span></a>
						                <div id="loginBox" class="login-form">    
						                	<h3>Login into Your Account</h3>            
						                 
						                  </div>
						             </div>
				               </li>
				               <li><a href="#" >Sitemap</a></li>
				         </ul>
				    </div>
			      <div class="clear"></div>
			     </div> 
		      </div>
             <div class="header-logo-nav">
             	  <div class="navbar navbar-inverse navbar-static-top nav-bg" role="navigation">
				      <div class="container">
				        <div class="navbar-header">
				          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				            <span class="sr-only">Toggle navigation</span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				          </button>
				         <div class="logo"> <a class="navbar-brand" href="http://www.itechtraining.in/"><img src="images/Itech Solutions logo.png" alt="" /></a></div>
				          <div class="clear"></div>
				        </div>
				        <div class="collapse navbar-collapse">
				          <ul class=" menu nav navbar-nav">
				           
				            <li><a href="Adminhome.do"><img src="<%=request.getContextPath() %>/images/homeover.png" title="Home"></a></li>
				            <li><a href="Logout.do"><img src="images/logout.png"></a></li>
				          </ul>
				        </div><!--/.nav-collapse -->
				      </div>
				    </div>
		         <div class="clear"></div>
                  <div class="header-bottom">
                 <div class="container">
                 <div class="time">
                 <p><div id="clockbox"></div></p>
                 <script src="js/dynamicClock.js" type="text/javascript"></script>
                 </div>
               
                 <div class="welcome">
                 <h3 align="right">Welcome <%=userName %></h3>
                 </div>
                 </div>
                 </div>
                 <div class="clear"></div>
	        </div>
	        <div class="header-banner">	        				    
	           </div>
             </div>
   <!-- End Header -->
 
   <!-- Start Main Content -->
	 <div class="main" style="min-height:450px;">	 
	  
	 	<div class="wbDiv">
<div class="wbFrame">
<iframe src="<bean:write name="frameURL"/>" width=700" height="600" frameborder="0"></iframe> 
 </div>
 </div>
    </div>
    
      
     
   <!-- End Main Content -->
	   <jsp:include page="/jsp/common/masterdata_footer.jsp"></jsp:include> 
 <script src="js/commonFunction.js"  type="text/javascript"></script>
  </body>
</html>
