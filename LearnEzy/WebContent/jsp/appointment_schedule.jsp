
<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Itech Training :: Appointment Schedule</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300'
	rel='stylesheet' type='text/css'>
<link href="<%=request.getContextPath()%>/css/bootstrap.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath()%>/css/cmsstyle.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath()%>/css/font-awesome.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath()%>/css/carousel.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath()%>/css/owl.carousel.css"
	rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />

<script src="<%=request.getContextPath()%>/js/jquery-1.10.2.min.js"
	type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap.js"
	type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"
	type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/owl.carousel.js"
	type="text/javascript"></script>

<link href="dataTable/css/jquery.dataTables_themeroller.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.css" rel="stylesheet"
	type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.min.css" rel="stylesheet"
	type="text/css" media="all" />
<script src="dataTable/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="dataTable/js/jquery.dataTables.min.js"
	type="text/javascript"></script>
<script src="dataTable/js/settingDataTable.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css"
	href="/E-Learning/date/jquery-ui.css" />
<script type="text/javascript" src="/E-Learning/date/jquery-ui.js"></script>

<script type="text/javascript">
$(document).ready(function(){
    $(".viewStudents").click(function(){
    	var studentIds=$(this).attr("tag1");
		$.ajax( {
			url : "AppointmentSchedule.do?action=getStudentName",
			type : "GET",
			dataType : "json",
			data:{studentIds:studentIds},
			success : function(res) {
				var studentList="";
				var count=0;
				$.each(res,function(i,obj){
					count=parseInt(count)+parseInt(1);
					studentList=studentList+'<span>'+count+') '+obj.studentName+'</span><br />';
			  	});
				$('#studentList').html(studentList);
				$("#studentList").dialog({
						  modal: true,
					      buttons: {
					        "close": function() {
					          $( this ).dialog( "close" );
					          $('#studentList').html("");
					        }
					      }
				});
			}
 		});
    });
 	 
});
window.history.forward();
function noBack() { window.history.forward(); }

function edit(appointmentScheduleId,appointmentId,studentId,courseid,appointmentType){
	document.forms[0].appointmentScheduleId.value=appointmentScheduleId;
	document.forms[0].appointmentId.value=appointmentId;
	document.forms[0].courseid.value=courseid;
	document.forms[0].appointmentType.value=appointmentType;
	//alert(appointmentId);
	var arr = studentId.split(',');
	for(var i = 0; i<arr.length; i++){
    	//alert(arr[i]);
    	$('#appointmentCreatedID option[value=' + arr[i] + ']').attr('selected', true);
    }
	document.getElementById("status").innerHTML="";
	document.forms[0].action="AppointmentSchedule.do?action=update";
	document.forms[0].appointmentId.focus();
	document.forms[0].btn.value="Update";
}

 function validate(){
	   
		if(document.forms[0].appointmentId.value.trim()=="-1"){
			alert("Please Select Appointment");
			document.forms[0].appointmentId.focus();
			return false;
		}else
		if(document.forms[0].appointmentType.value.trim()=="-1"){
			alert("Please Select Appointment Type");
			document.forms[0].appointmentType.focus();
			return false;
		}else
		if(document.forms[0].courseid.value.trim()=="-1"){
			alert("Please Select course");
			document.forms[0].courseid.focus();
			return false;
		}
		else
		if ($("#appointmentCreatedID").val() ==null) {
			alert("Please select at least one Student");
			return false;
		}
	    
	return true;
 }
 
 
 
</script>
<style type="text/css">
#normalTable{
font-size: 14px;

}
</style>

</head>
<body>

	<!-- Start Header -->


	<%@page import="java.util.Date"%>
	<%@page import="com.itech.elearning.forms.LoginForm"%>
	<%
		Object photo = "";
		Object userName = "";
		try {
			//photo=((LoginForm) session.getAttribute("userDetail")).getProfilePhoto();;
			userName = ((LoginForm) session.getAttribute("userDetail")).getFirstname();
			;
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	%>


	<div class="header">
		<div class="header-top">
			<div class="wrap">
				<div class="header-top-left">
					<p>ph: +91-9611421111</p>
				</div>
				<div class="header-top-right">
					<ul>
						<li><a>Careers</a></li>
						<li class="login">
							<div id="loginContainer">
								<a href="#" id="loginButton"><span>Join Us</span></a>
								<div id="loginBox" class="login-form">
									<h3>Login into Your Account</h3>

								</div>
							</div>
						</li>
						<li><a href="#">Sitemap</a></li>
					</ul>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<div class="header-logo-nav">
			<div class="navbar navbar-inverse navbar-static-top nav-bg"
				role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse"
							data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span> <span
								class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>
						</button>
						<div class="logo">
							<a class="navbar-brand" href="http://www.itechtraining.in/"><img
								src="images/LEARNEZYLatest-LOGO.png" alt="" /></a>
						</div>
						<div class="clear"></div>
					</div>
					<div class="collapse navbar-collapse">
						<ul class=" menu nav navbar-nav">

							<li><a href="Adminhome.do"><img
									src="<%=request.getContextPath()%>/images/homeover.png"
									title="Home"></a></li>
							<li><a href="Logout.do"><img src="images/logout.png"></a></li>
						</ul>
					</div>
					<!--/.nav-collapse -->
				</div>
			</div>
			<div class="clear"></div>
			<div class="header-bottom">
				<div class="container">
					<div class="time">
						<p>
						<div id="clockbox"></div>
						</p>
						<script src="js/dynamicClock.js" type="text/javascript"></script>
					</div>

					<div class="welcome">
						<h3 align="right">
							Welcome:
							<%=userName%></h3>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="header-banner"></div>
	</div>
	<!-- End Header -->

	<!-- Start Main Content -->
	<div class="main" style="min-height: 450px;">
		<h2>Appointment Schedule</h2>
		<html:form action="AppointmentSchedule.do?action=add"
			styleId="appointmentForm">
			<input type="hidden" name="appointmentScheduleId" />
			<div id="status" align="center">
				<logic:notEmpty name="status">
					<font color="blue"><bean:write name="status"></bean:write></font>
				</logic:notEmpty>
			</div>
			<table class="role" width="900">
				<tr>

					<td>Appointment Data - Time *</td>
					<td><html:select property="appointmentId"
							styleId="appointmentId" styleClass="selectWidth">
							<html:option value="-1">-Select appointment-</html:option>
							<html:options collection="appointmentList"
								property="appointmentId" labelProperty="appointmentDate" />
						</html:select></td>
					<td colspan="2" rowspan="3" valign="top"><label>Select
							Student *</label><br /> 
							<html:select property="appointmentCreatedID" multiple="multiple" styleId="appointmentCreatedID"
							styleClass="selectWidth role1Select"
							style="height: 150px; overflow: auto; width: 340px; border: 1px solid #000;">
							<html:options collection="userList"	property="appointmentCreatedID" labelProperty="firstName" />
						</html:select></td>
				</tr>
				<tr>
					<td>Appointment Type *</td>
					<td><select name="appointmentType">
							<option value="-1">--Select Appointment Type--</option>
							<option value="White Board">White Board</option>
							<option value="Video">Video Conference</option>
							<option value="Desktop Sharing">Desktop Sharing</option>
					</select></td>
				</tr>
				<tr>


					<td>Course *</td>
					<td><html:select property="courseid" styleId="courseid"
							styleClass="selectWidth">
							<html:option value="-1">-Select-</html:option>
							<html:options collection="courseList" property="courseid"
								labelProperty="courseName" />
						</html:select></td>

				</tr>
				<tr>



					<td colspan="4" align="center"><input type="submit" name="btn"
						onclick="return validate();" value="Save"> <input
						type="reset" value="Reset"></td>
				</tr>
			</table>
		</html:form>


		<div class="dataGridDisplay">
			<table id="normalTable" class="display" cellspacing="0" width="100%"
				align="center">
				<thead>
					<tr>
						<th>SI No.</th>
						<th>Appointment Date & Time</th>
						<th>Course</th>
						<th>Appointment User</th>
						<th>Students</th>
						<th>Appointment Type</th>
						<th style="width: 75px;">Status</th>
						<th style="width: 155px;">Action</th>
					</tr>
				</thead>
				<tbody>

					<logic:notEmpty name="appointmentScheduleList">

						<logic:iterate id="appointmentScheduleList"
							name="appointmentScheduleList" indexId="index">
							<tr>
								<td><%=index + 1%></td>
								<td><bean:write name="appointmentScheduleList"
										property="appointmentDateTime" /></td>
								<td><bean:write name="appointmentScheduleList"
										property="courseName" /></td>
								<td><bean:write name="appointmentScheduleList"
										property="firstName" /></td>
								<td> <a href="#" tag1="<bean:write name="appointmentScheduleList" property="studentId" />" class="viewStudents" >View Students</a> </td>
								<td><bean:write name="appointmentScheduleList"
										property="appointmentType" /></td>
								<td><logic:equal name="appointmentScheduleList"
										property="active" value="true">
										<font color="green">Active</font>
									</logic:equal> <logic:equal name="appointmentScheduleList" property="active"
										value="false">
										<font color="red">Inactive</font>
									</logic:equal></td>

								<td><logic:equal name="appointmentScheduleList"
										property="active" value="true">
										<a
											href='AppointmentSchedule.do?action=changestatus&amp;appointmentScheduleId=<bean:write name="appointmentScheduleList" property="appointmentScheduleId"/>&amp;active=false'>
											<input type="button" value="Remove">
										</a>
										<a href='#'
											onclick='edit("<bean:write name="appointmentScheduleList" property="appointmentScheduleId"/>",
									  "<bean:write name="appointmentScheduleList" property="appointmentId"/>",
									  "<bean:write name="appointmentScheduleList" property="studentId"/>",
									  "<bean:write name="appointmentScheduleList" property="courseid"/>",
									  "<bean:write name="appointmentScheduleList" property="appointmentType"/>")'><input
											type="button" value="Edit"></a>

									</logic:equal> <logic:equal name="appointmentScheduleList" property="active"
										value="false">
										<a style="text-decoration: none;"
											href='AppointmentSchedule.do?action=changestatus&amp;appointmentScheduleId=<bean:write name="appointmentScheduleList" property="appointmentScheduleId"/>&amp;active=true'>
											<input type="button" value="Active">
										</a>
									</logic:equal></td>
							</tr>


						</logic:iterate>
					</logic:notEmpty>
				</tbody>

			</table>
			<logic:notEmpty name="appointmentScheduleList">
				<!-- <div align="center"><a href="AppointmentSchedule.do?action=excelReport"><button>Excel Export</button></a></div> -->
			</logic:notEmpty>
		</div>


	</div>
<diV id="studentList" title="Student List"></diV>
	<!-- End Main Content -->
	<jsp:include page="/jsp/common/masterdata_footer.jsp"></jsp:include>
	<script src="js/commonFunction.js" type="text/javascript"></script>
</body>
</html>

