
<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Itech Training :: Appointment</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300' rel='stylesheet' type='text/css'>
<link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/cmsstyle.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/carousel.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>

<script src="<%=request.getContextPath() %>/js/jquery-2.1.1.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/owl.carousel.js" type="text/javascript"></script>
  
<link href="dataTable/css/jquery.dataTables_themeroller.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.min.css"	rel="stylesheet" type="text/css" media="all" />
<script src="dataTable/js/jquery.dataTables.js"	type="text/javascript"></script>
<script src="dataTable/js/jquery.dataTables.min.js"	type="text/javascript"></script>
<script src="dataTable/js/settingDataTable.js" 	type="text/javascript"></script>

<link rel="stylesheet" type="text/css"	href="<%=request.getContextPath() %>/date/jquery-ui.css" />
<script type="text/javascript" src="<%=request.getContextPath() %>/date/jquery-ui.js"></script>

<link href="<%=request.getContextPath() %>/timepicker/jquery.timepicker.css" 	rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="<%=request.getContextPath() %>/timepicker/jquery.timepicker.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/timepicker/jquery.timepicker.min.js"></script>

<script type="text/javascript">
$(function(){
    $("#appointmentDate1").datepicker();
    $("#appointmentTime1").timepicker();
});
window.history.forward();
function noBack() { window.history.forward(); }

 function edit(id,appointmentDate,appointmentTime){
	 	document.getElementById("status").innerHTML="";
		document.forms[0].action="Appointment.do?action=update";
		document.forms[0].appointmentId.value=id;
		document.forms[0].appointmentDate.value=appointmentDate;
		document.forms[0].appointmentTime.value=appointmentTime;
		document.forms[0].appointmentDate.focus();
		document.forms[0].btn.value="Update";
	}
  

 function validate(){
	   
		if(document.forms[0].appointmentDate.value.trim()==""){
			alert("Please Select Date");
			document.forms[0].appointmentDate.focus();
			return false;
		}

		if (document.forms[0].appointmentTime.value.trim()=="")
	    {
	 	   document.forms[0].appointmentTime.focus(); 
	       alert("Please Enter Time");
	       return false;   
	    }
	    
	return true;
 }

 
 
</script>
</head>
<body>
 
 <!-- Start Header -->
       
 
<%@page import="java.util.Date"%>
 <%@page import="com.itech.elearning.forms.LoginForm" %>
<%
	  												Object photo="";
                                                	Object userName="";
	 														 try{
	 															//photo=((LoginForm) session.getAttribute("userDetail")).getProfilePhoto();;
	 															userName=((LoginForm) session.getAttribute("userDetail")).getFirstname();;
	 															 }catch(NullPointerException e){
    	  																		e.printStackTrace();
      															}
	 														%> 
<div class="header">	
   	 	    <div class="header-top">
   	 	      <div class="wrap"> 
   	 	    	 <div class="header-top-left">
   	 	    	 	<p>ph: +91-9611421111</p>
   	 	    	 </div>
   				  <div class="header-top-right">
				        <ul>
				            <li><a   >Careers</a></li>
				            <li  class="login">
				              <div id="loginContainer">
				            	   <a href="#" id="loginButton"><span>Join Us</span></a>
						                <div id="loginBox" class="login-form">    
						                	<h3>Login into Your Account</h3>            
						                 
						                  </div>
						             </div>
				               </li>
				               <li><a href="#" >Sitemap</a></li>
				         </ul>
				    </div>
			      <div class="clear"></div>
			     </div> 
		      </div>
             <div class="header-logo-nav">
             	  <div class="navbar navbar-inverse navbar-static-top nav-bg" role="navigation">
				      <div class="container">
				        <div class="navbar-header">
				          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				            <span class="sr-only">Toggle navigation</span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				          </button>
				         <div class="logo"> <a class="navbar-brand" href="http://www.itechtraining.in/"><img src="images/LEARNEZYLatest-LOGO.png" alt="" /></a></div>
				          <div class="clear"></div>
				        </div>
				        <div class="collapse navbar-collapse">
				          <ul class=" menu nav navbar-nav">
				           
				            <li><a href="Adminhome.do"><img src="<%=request.getContextPath() %>/images/homeover.png" title="Home"></a></li>
				            <li><a href="Logout.do"><img src="images/logout.png"></a></li>
				          </ul>
				        </div><!--/.nav-collapse -->
				      </div>
				    </div>
		         <div class="clear"></div>
                  <div class="header-bottom">
                 <div class="container">
                 <div class="time">
                 <p><div id="clockbox"></div></p>
                 <script src="js/dynamicClock.js" type="text/javascript"></script>
                 </div>
               
                 <div class="welcome">
                 <h3 align="right">Welcome: <%=userName %></h3>
                 </div>
                 </div>
                 </div>
                 <div class="clear"></div>
	        </div>
	        <div class="header-banner">	        				    
	           </div>
             </div>
   <!-- End Header -->
 
   <!-- Start Main Content -->
	 <div class="main" style="min-height:450px;">	 
	 	<h2>Appointment </h2>
    <html:form action="/Appointment.do?action=add" styleId="appointmentForm">
    <input type="hidden" name="appointmentId" />
    <div id="status" align="center"><logic:notEmpty name="status">
					<font color="blue"><bean:write name="status"></bean:write></font>
				</logic:notEmpty></div>
     <table class="role" width="750">
     <tr>
     
     <td>Appointment Data & Time:<font style="color: red;">*</font></td>
     <td><input type="text"  name="appointmentDate" placeholder="Date" id="appointmentDate1" size="8"    maxlength="50" autofocus> &nbsp;
         <input type="text"  name="appointmentTime" placeholder="Time" id="appointmentTime1" size="8"   maxlength="50" ></td>
    
     <td><input type="submit" name="btn" onclick="return validate();" value="Save"> <input type="reset" value="Reset" ></td>
     </tr>
     </table>
     </html:form>
     
     	
     <div class="dataGridDisplay"  >
   	<table id="normalTable" class="display" cellspacing="0" width="100%" align="center" >
				<thead>
    <tr>
    <th>SI No.</th>
    <th>Appointment Data</th>
    <th>Appointment Time</th>
    <th style="width:75px;">Status</th>
    <th style="width:155px;">Action</th>
    </tr>
    </thead>
    <tbody>
    
    <logic:notEmpty name="appointmentList">
    
    	<logic:iterate id="appointmentList" name="appointmentList" indexId="index">
    <tr>
    <td><%=index+1 %></td>
    <td><bean:write name="appointmentList" property="appointmentDate" /></td>
     <td><bean:write name="appointmentList" property="appointmentTime" /></td>
    <td>
    
    <logic:equal name="appointmentList"
							property="active" value="true">
							<font color="green">Active</font>
						</logic:equal> <logic:equal name="appointmentList" property="active" value="false">
							<font color="red">Inactive</font>
						</logic:equal>
    
    </td>
    
    <td>
    <logic:equal name="appointmentList"
							property="active" value="true">
							<a style="text-decoration: none;"
								href='Appointment.do?action=changestatus&amp;appointmentId=<bean:write name="appointmentList" property="appointmentId"/>&amp;active=false'> <input type="button" value="Remove"></a>
								&nbsp; <a href='#'
								onclick='edit("<bean:write name="appointmentList" property="appointmentId"/>","<bean:write name="appointmentList" property="appointmentDate"/>","<bean:write name="appointmentList" property="appointmentTime"/>")'><input type="button" value="Edit"></a>
						</logic:equal> <logic:equal name="appointmentList" property="active" value="false">
							<a style="text-decoration: none;"
								href='Appointment.do?action=changestatus&amp;appointmentId=<bean:write name="appointmentList" property="appointmentId"/>&amp;active=true'> <input type="button" value="Active"></a>
						</logic:equal>
    
</td>
    </tr>
  
    
    </logic:iterate>
     </logic:notEmpty>
    </tbody>
    
    </table>
      <logic:notEmpty name="appointmentList">
    <!-- <div align="center"><a href="Appointment.do?action=excelReport"><button>Excel Export</button></a></div> -->
    </logic:notEmpty>
    </div>
    
    
     </div>
     
   <!-- End Main Content -->
	   <jsp:include page="/jsp/common/masterdata_footer.jsp"></jsp:include> 
 <script src="js/commonFunction.js"  type="text/javascript"></script>
  </body>
</html>

