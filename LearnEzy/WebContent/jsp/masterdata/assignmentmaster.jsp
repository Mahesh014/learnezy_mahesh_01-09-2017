<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Itech Training :: Assignment Master</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300' rel='stylesheet' type='text/css'>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/cmsstyle.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/carousel.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>
       
<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
<script src="js/bootstrap.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/owl.carousel.js" type="text/javascript"></script>



<script src="js/commonFunction.js"  type="text/javascript"></script>
<link href="dataTable/css/jquery.dataTables_themeroller.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.min.css"	rel="stylesheet" type="text/css" media="all" />
<script src="dataTable/js/jquery.dataTables.js"	type="text/javascript"></script>
<script src="dataTable/js/jquery.dataTables.min.js"	type="text/javascript"></script>
<script src="dataTable/js/settingDataTable.js" 	type="text/javascript"></script>

<script type="text/javascript">
window.history.forward();
function noBack() { window.history.forward(); }
function validate(){
	if(document.forms[0].courseId.value == -1){
		alert("Please Select the Course");
		document.forms[0].courseId.focus();
		return false;
	}
		
	if(document.forms[0].topicId.value == -1){
		alert("Please Select the Topic");
		document.forms[0].topicId.focus();
		return false;
	}
	if(document.forms[0].subTopicId.value == -1){
		alert("Please Select the Sub Topic");
		document.forms[0].topicId.focus();
		return false;
	}
	if(document.forms[0].assignmentQuestion.value.trim()==""){
		alert("Assignment Question cannot be empty");
		document.forms[0].assignmentQuestion.focus();
		return false;
	}
	
	if(document.forms[0].assignmentAnswer.value.trim()==""){
		alert("Assignment Answer cannot be empty");
		document.forms[0].assignmentAnswer.focus();
		return false;
	}
	
}
function subTopicList(){

	var topicId=$("#tid").val();
	document.forms[0].action="AssignmentMaster.do?action=list&topicId="+topicId+"";
	document.forms[0].submit();
}

function exclValidate(){
	if(document.forms[1].excelFile.value == ""){
		alert("Please Select  xl or xls extension file");
		
		return false;
	}
}
function callList()
{
	var courseId=$("#cid").val();
	document.forms[0].action="AssignmentMaster.do?action=list&courseId="+courseId+"";
	document.forms[0].submit();
}
function edit(aId,cId,tId,question,ans,stid)
{
	document.forms[0].action="AssignmentMaster.do?action=update";
	document.forms[0].assignmentId.value= aId;
	document.forms[0].courseId.value= cId;
	//getTopic(courseId,tId);
	document.forms[0].topicId.value= tId;
	document.forms[0].subTopicId.value= stid;
	document.forms[0].assignmentQuestion.value= question;	
	document.forms[0].assignmentAnswer.value= ans;
	document.forms[0].btn.value="Update";
	document.forms[0].assignmentQuestion.focus();
	
}

function getTopic(courseId,selectedTopicId){
	$.ajax({	
		type: "GET",		
		url:"ContantMaster.do?action=getTopic",
		data:{courseId:courseId},
		success: function(r){	
			var json= JSON.parse(r);
			$("#tid").empty();
			var firstOption= $("<option>",{text:"--Select Topic--",value:"-1"});
			$("#tid").append(firstOption);
			$.each(json,function(i,obj){
				var option= $("<option>",{text:obj.topicName, value:obj.tid});
				$("#tid").append(option);
				alert(obj.topicName+ " "+ obj.tid);
			});
			if(selectedTopicId!="" && selectedTopicId!=null){
				$("#tid").val(selectedTopicId);
    		}
		} 
		
	});
	
}




$(document).ready(function() {
	
	 $("#resetBtn").click(function(){		 	
	    	$("#assignmentForm").attr("action","AssignmentMaster.do?action=reset")
	    	$("#assignmentForm").submit();
	    	
	 });

	 $('#excelFile').change(function () {
			var val = $(this).val().toLowerCase();
			var regex = new RegExp("(.*?)\.(xls|xlsx)$");
			if(!(regex.test(val))) {
			 	$(this).val('');
			 	alert('Please select xls xlsx format only');
			 	return false;
			 } 
		}); 

	$("#tid").change(function(event,topicId){

		var topicId=$("#tid").val();
		var courseId=$("#cid").val();
		$.ajax({
				type: "GET",		
				url:"AssignmentMaster.do?action=getSubTpics",
				data:{topicId:topicId,courseId:courseId},
				success: function(r){
					var json= JSON.parse(r);
					console.log(json);			
					$("#sid").empty();
					var firstOption= $("<option>",{text:"--Select SubTopic--",value:"-1"});
					$("#sid").append(firstOption);
					$.each(json,function(i,obj){
						var option= $("<option>",{text:obj.stname, value:obj.stid});
						$("#sid").append(option);
				//		alert(obj.stname+ " "+ obj.stid);
					});
					
			}	
		});
		
			
		});});
$(document).ready(function() {
	 
	 $(".editJson").click(function(){
		 var tag=$(this).attr("tag");
		 var jsonObj=JSON.parse(tag);
		 	$("#cid").val(jsonObj.courseId);
			$("#tid").val(jsonObj.topicId);
			$("#sid").val(jsonObj.subTopicId);
			$("#assignmentId").val(jsonObj.assignmentId);
			$("#assignmentQuestion").val(jsonObj.assignmentQuestion);
			$("#assignmentAnswer").val(jsonObj.assignmentAnswer);
			document.forms[0].action="AssignmentMaster.do?action=update";
			document.forms[0].btn.value="Update";
			document.forms[0].assignmentQuestion.focus();
	 });
});
</script>
</head>

<body>
  <!-- Start Header -->
     <jsp:include page="/jsp/common/masterdata_header.jsp"></jsp:include>
   <!-- End Header -->
   
 

 <div class="main" style="min-height:450px;">	 
	 	<h2>Assignment Master</h2>
    <html:form action="AssignmentMaster.do?action=add" styleId="assignmentForm">
		<input type="hidden" name="assignmentId" id="assignmentId" />
    <table border="0" width="80%" align="center"  class="role">
		 
		<tr><td colspan="3" align="center"><div id="status"><logic:notEmpty name="status">
	<font color="blue"><bean:write name="status"></bean:write></font>
</logic:notEmpty></div></td></tr>
		<tr>
					<td><label>Course *</label>
						<html:select style="width:216px;" property="courseId" styleId="cid" onchange="callList();" >
						<html:option value="-1">-Select-</html:option>
						<html:options collection="courseList" property="courseId"
							labelProperty="courseName" />
					</html:select></td>

					<td><label>Topic*</label>
						<html:select style="width:216px;" property="topicId" styleId="tid" onchange="">
						<html:option value="-1">-Select-</html:option>
						<html:options collection="topicList" property="topicId"
							labelProperty="topicName" />
					</html:select></td>
					
					<td><label>Sub Topic*</label>
						<html:select style="width:216px;" property="subTopicId" styleId="sid">
						<html:option value="-1">-Select-</html:option>
						<html:options collection="subTopicList" property="subTopicId"
							labelProperty="subTopicName" />
					</html:select></td>
				</tr>
				<tr>
				
				<td  colspan="3"><label>Assignment Question *</label>
				               <textarea  name="assignmentQuestion"  id="assignmentQuestion" maxlength="2500" style="width: 740px;height: 60px;resize: none;" ></textarea> 
					</td>
				</tr>
				 
				<tr>	
					<td colspan="3"><label>Assignment Answer *</label>
					<textarea  name="assignmentAnswer" id="assignmentAnswer" maxlength="2500" style="width: 740px;height: 60px;resize: none;" ></textarea> 
					
				</tr>
				 
				<tr>
 					<td colspan="3" align="center"><input type="submit" name="btn"
						onclick="return validate();" value="Save" class="but1" />
					<input type="reset" value="Reset" id="resetBtn"   /></td>
				</tr>
			</table>
		</html:form>
		<html:form action="AssignmentMaster.do?action=excelUpload" enctype="multipart/form-data" onsubmit="return exclValidate();">
		
		<table border="0" width="90%" align="center" class="role">
		<tr height="40">
            <td colspan=   align="center">Excel Upload</td>
       				 <td><input type="file" name="excelFile" id="excelFile" /></td>
       				  <td colspan=   align="left" ><input type="submit" value="Submit"></input></td>
     <td><a class="submit_btn" style="float:left" href="<%=request.getContextPath() %>/studentRegistration.do?action=downloadexcel&amp;filename=AssignmentMasterExcel.xls"/>Download Excel File Format</a>
       				  
		</tr>
		</table>
		</html:form>
 
	<div class="dataGridDisplay"  >
<table id="normalTable" class="display" cellspacing="0" width="100%"     >
	<thead>
   <tr>
    <th>Sl No.</th>
    <th>Assignment Question</th>
   	<th>Assignment Answer</th>
    <th>Status</th>
    <th style="min-width: 155px;">Action</th>
    </tr>
    </thead>
    <tbody>
   
    <logic:notEmpty name="assignmentList">   
    <logic:iterate id="assignmentList" name="assignmentList" indexId="index">
    <tr>
    <td><%=index+1 %></td>
    <td><textarea rows="5" cols="30" readonly="readonly" style="resize:none;"><bean:write name="assignmentList"property="assignmentQuestion" /></textarea></td>
    <td><textarea rows="5" cols="30" readonly="readonly" style="resize:none;"><bean:write name="assignmentList"property="assignmentAnswer" /></textarea></td>
    <td>
    	<logic:equal name="assignmentList" property="active" value="true"><font color="green">Active</font></logic:equal> 
   	 	<logic:equal name="assignmentList" property="active" value="false">	<font color="red">InActive</font></logic:equal>
    </td>

    <td>
    	<logic:equal name="assignmentList" property="active"	value="true">
			<a href='AssignmentMaster.do?action=changestatus&amp;assignmentId=<bean:write name="assignmentList" property="assignmentId"/>&amp;active=false'><input type="button" value="Remove"></a>
			&nbsp;&nbsp;
			<a href='#' tag="<bean:write name="assignmentList" property="jEdit"/>"  class="editJson"><input type="button" value="Edit"></a>
		</logic:equal> 
		<logic:equal name="assignmentList" property="active" value="false">
			<a href='AssignmentMaster.do?action=changestatus&amp;assignmentId=<bean:write name="assignmentList" property="assignmentId"/>&amp;active=true'><input type="button" value="Active"></a>
		</logic:equal>      
	</td>
    </tr>
    
 </logic:iterate>
    </logic:notEmpty>
	 </tbody>
</table>
 <logic:notEmpty name="assignmentList">  
<div align="center"><a href="AssignmentMaster.do?action=excelReport"><button>Excel Export</button></a></div>
</logic:notEmpty>
 </div>
    </div>
   <!-- End Main Content -->
	   
  <!-- Start Footer -->
   <jsp:include page="/jsp/common/masterdata_footer.jsp"></jsp:include> 
  <!-- End Footer -->
<!--   <script type="text/javascript">-->
<!--   -->
<!--   document.forms[0].courseId.focus();-->
<!--	</script>-->
  </body>
</html>






