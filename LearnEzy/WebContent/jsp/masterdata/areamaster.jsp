
<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Itech Training :: Area Master</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300' rel='stylesheet' type='text/css'>
<link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/cmsstyle.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/carousel.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>

<script src="<%=request.getContextPath() %>/js/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/owl.carousel.js" type="text/javascript"></script>

<link href="dataTable/css/jquery.dataTables_themeroller.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.min.css"	rel="stylesheet" type="text/css" media="all" />
<script src="dataTable/js/jquery.dataTables.js"	type="text/javascript"></script>
<script src="dataTable/js/jquery.dataTables.min.js"	type="text/javascript"></script>
<script src="dataTable/js/settingDataTable.js" 	type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function() {
	
    $("#countryid").val('-1');

    $("#countryid").change( function() {

    	var countryid=$(this).val();
    	if(countryid!="0"){
    	
    		getState(countryid,""); 
    	}
    });

    $("#stateid").change( function() {
    	var stateid=$(this).val();
    	if(stateid!="0"){
    		getCity(stateid,"");
    	}
    });

});	

function edit(areaid,countryid,areaname,stateid,cityid){
	document.getElementById('status').innerHTML="";
	document.forms[0].action="Area.do?action=update";
	document.forms[0].countryid.value=countryid;
	document.forms[0].areaname.value=areaname;	
	getState(countryid,stateid); 
	getCity(stateid,cityid);
	document.forms[0].areaid.value=areaid;
	document.forms[0].btn.value="Update";
	document.forms[0].countryid.focus();
}

function getState(countryid,selectedId){
	$.ajax({	
		type: "GET",		
		url:"State.do?action=ajaxActiveList",
		data:{countryId:countryid},
		success: function(r){	
			var json= JSON.parse(r);
			console.log(json);			
			$("#stateid").empty();
			var firstOption= $("<option>",{text:"--Select State--",value:"-1"});
			var otherOption= $("<option>",{text:"Others",value:"0"});
			$("#stateid").append(firstOption);
			$.each(json,function(i,obj){
				var option= $("<option>",{text:obj.state, value:obj.stateId});
				$("#stateid").append(option);
			});
			$("#stateid").append(otherOption);
			if(selectedId!="" && selectedId!=null){
				$("#stateid").val(selectedId);
    		}
			
		} 
	});
}
function getCity(stateid,selectedId){
	$.ajax({	
		type: "GET",		
		url:"City.do?action=ajaxActiveList",
		data:{stateId:stateid},
		success: function(r){	
			var json= JSON.parse(r);
			console.log(json);			
			$("#cityid").empty();
			var firstOption= $("<option>",{text:"--Select City--",value:"-1"});
			var otherOption= $("<option>",{text:"Others",value:"0"});
			$("#cityid").append(firstOption);
			$.each(json,function(i,obj){
				var option= $("<option>",{text:obj.city, value:obj.cityId});
				$("#cityid").append(option);
			});
			$("#cityid").append(otherOption);
			if(selectedId!="" && selectedId!=null){
				$("#cityid").val(selectedId);
			}
			
		} 
	});
}







function changeStatus(areaid,val) {
	
	//var areaid=document.forms[0].areaid.value;
	
	document.forms[0].action="Area.do?action=changestatus"+"&areaid="+areaid+"&Active="+val;
	document.forms[0].submit();
}


function resetBtn(){
	document.getElementById('status').innerHTML="";
	document.forms[0].cityname.value="";
	document.forms[0].stateid.value="-1";
	document.forms[0].countryid.value="-1";
	document.forms[0].countryid.focus();
	return false;
}
 
function onload(){
	document.forms[0].countryid.focus();
}

function validate(){
	   
	if(document.forms[0].countryid.value.trim()=="-1"){
		alert("Please Select Country");
		document.forms[0].countryid.focus();
		return false;
	}else
	if(document.forms[0].stateid.value.trim()=="-1"){
		alert("Please Select State");
		document.forms[0].stateid.focus();
		return false;
	}else
		if(document.forms[0].cityid.value.trim()=="-1"){
			alert("Please Select City");
			document.forms[0].cityid.focus();
			return false;
		}else
	if(document.forms[0].areaname.value.trim()==""){
		alert("Please enter Area Name");
		document.forms[0].areaname.focus();
		return false;
	}else
	if (!document.forms[0].areaname.value.match(/^[a-zA-Z ]*$/) && document.forms[0].areaname.value !=""){
 	   document.forms[0].areaname.value="";
 	   document.forms[0].areaname.focus(); 
       alert("Please Enter only Alphabets");
       return false;   
    }

}
</script>


<script type="text/javascript">

   function preventBack(){window.history.forward();}
    setTimeout("preventBack()", 0);
    window.onunload=function(){null};
</script>

</head>
<body onload="onload()">
 
 <jsp:include page="/jsp/common/masterdata_header.jsp"></jsp:include>
 
   <!-- Start Main Content -->
	 <div class="main" style="min-height:450px;">	 
	 	<h2>Area Master</h2>
   <html:form action="Area.do?action=add" styleId="cityForm">
  <table class="role" width="900">
 <tr>
		<td colspan="6" align="center">
		<div align="center" id="status"><logic:notEmpty name="status">
			<font color="red"><bean:write name="status"></bean:write></font>
		</logic:notEmpty></div>
		</td>
		</tr>
				<tr >
					<td >Country Name*: 
						<html:select property="countryid" styleClass="select1" styleId="countryid">
						<html:option value="-1">--Select country--</html:option>
						<html:options collection="countryList" property="countryid" labelProperty="countryname" />
					</html:select>
					</td>
					
					<td>State Name*: 
						<select name="stateid" id="stateid" class="dropdown-select" >
	 <option value="-1">-Select State-</option>
	<option value="0">Others</option>
	</select></td>
					
					<td>City Name*:
					<select name="cityid" id="cityid"  class="dropdown-select">
													<option value="-1">-Select City-</option>
													<option value="0">Others</option>
												</select>
					 	</td>
					 	
					 	<td>Area Name*:<input type="hidden" name="areaid" id="areaid" /> 
					 	<input type="text" name="areaname" id="areaname" maxlength="80" /></td>
					
					</tr>
					<tr >
					<td colspan="3" align="center" >
						<input type="submit" name="btn" value="Save" onclick="return validate();" />&nbsp; &nbsp; 
						<input type="Reset"  value="Reset" onclick="resetBtn();" />
					</td>
				</tr>
</table>
  </html:form>
     
     	
     <div class="dataGridDisplay"  >
   <table id="normalTable" class="display" cellspacing="0" width="100%" align="center" >
				<thead>
  <tr height="30"
			style="background: #FFFFFF; color: #000; font-weight: bold;">
			<td>SI.NO</td>
			<td>Area Name</td>
			<td class="no-print" style="width:100px;">Status</td>
			<td class="no-print" style="width:155px;">Action</td>
		</tr>
		</thead>
		<tbody>
		<logic:notEmpty name="arealist">
		<logic:iterate id="city" name="arealist" indexId="index">
			<tr height="30">

				<td><%=index + 1%></td>
				<td><bean:write name="city" property="areaname" /></td>
				<td class="no-print"><logic:equal name="city" property="active" value="true">
					<font color="green">Active</font>
				</logic:equal> <logic:equal name="city" property="active" value="false">
					<font color="red">Inactive</font>
				</logic:equal></td>
				<td class="no-print">
				  
				<logic:equal name="city" property="active" value="true">
					<a href="#"
						onclick='changeStatus("<bean:write name="city" property="areaid"/>",false)'><input
						type="button" value="Remove" /></a>
					<a href='#'
						onclick='edit("<bean:write name="city" property="areaid"/>","<bean:write name="city" property="cityid"/>","<bean:write name="city" property="areaname"/>","<bean:write name="city" property="stateid"/>","<bean:write name="city" property="countryid"/>")'><input
						type="button" value="Edit" /></a>
				</logic:equal> <logic:equal name="city" property="active" value="false">
					<a href="#"
						onclick='changeStatus("<bean:write name="city" property="areaid"/>",true)'><input
						type="button" value="Activate"
						  /></a>
				</logic:equal>
				 
				</td>
			</tr>
		</logic:iterate>
	</logic:notEmpty>
	</tbody>
	</table>
	<logic:notEmpty name="arealist">
	<div align="center"><a href="Area.do?action=exportexcel"><button>Excel Export</button></a></div>
	</logic:notEmpty>
    </div>
   
    
    
     </div>
     
   <!-- End Main Content -->
	   <jsp:include page="/jsp/common/masterdata_footer.jsp"></jsp:include> 
 <script src="js/commonFunction.js"  type="text/javascript"></script>

 <script type="text/javascript">
 document.forms[0].countryid.focus();
 </script>
  </body>
</html>

