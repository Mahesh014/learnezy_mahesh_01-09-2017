
<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Itech Training ::  Course Offered Master</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300' rel='stylesheet' type='text/css'>
<link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/cmsstyle.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/carousel.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>

<script src="<%=request.getContextPath()%>/js/jquery-2.1.1.min.js"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/owl.carousel.js" type="text/javascript"></script>

<link href="dataTable/css/jquery.dataTables_themeroller.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.min.css"	rel="stylesheet" type="text/css" media="all" />
<script src="dataTable/js/jquery.dataTables.js"	type="text/javascript"></script>
<script src="dataTable/js/jquery.dataTables.min.js"	type="text/javascript"></script>
<script src="dataTable/js/settingDataTable.js" 	type="text/javascript"></script>
<script type="text/javascript">
window.history.forward();
function noBack() { window.history.forward(); }

 function edit(id,coursecategoryId,desc){
	 	document.getElementById("status").innerHTML="";
		document.courseofferedForm.action="courseoffered.do?action=update";
		document.courseofferedForm.courseofferedid.value=id;
		document.courseofferedForm.coursecategoryId.value=coursecategoryId;
		
		
		document.courseofferedForm.courseofferedname.value=desc;
		 document.courseofferedForm.courseofferedname.focus();
		document.courseofferedForm.btn.value="Update";
	}
 function onfocus2()
 {

 document.getElementById('status').innerHTML="";
 document.courseofferedForm.courseofferedid.value="";
 document.courseofferedForm.action="courseoffered.do?action=add";
 document.courseofferedForm.btn.value="Save";
 document.courseofferedForm.courseofferedname.focus();
 document.getElementById('status').innerHTML="";
 }

 function validate(){
	   
		if(document.forms[0].courseofferedname.value.trim()==""){
			alert("Please enter Course Offered");
			document.forms[0].courseofferedname.focus();
			return false;
		}

		if (!document.forms[0].courseofferedname.value.courseofferedname.value !="")
	    {
	 	   document.forms[0].courseofferedname.value="";
	 	   document.forms[0].courseofferedname.focus(); 
	       //alert("Please Enter only Alphabets");
	       return false;   
	    }
	    
	return true;
 }

 $(window).load(function(){

	  $('input[name=courseofferedname]').focus();

	});
	$(document).ready(function()
			{


$("#resetid").click(function()
		{
	$.ajax({
		 type: "GET",
	    url: "studentRegistration.do?action=clear",
	    success : function (response){
	    	window.location.href=window.location.href;
	    	$("#coursecategoryId").val("-1");
	    	

	}
	  });




	});




		});


			
 
</script>
</head>
<body>
 
 <jsp:include page="/jsp/common/masterdata_header.jsp"></jsp:include>
 
   <!-- Start Main Content -->
	 <div class="main" style="min-height:450px;">	 
	 	<h2>Course Offered Master</h2>
    <html:form action="/courseoffered.do?action=add">
    <input type="hidden"	name="courseofferedid" />
    <div id="status" align="center"><logic:notEmpty name="status">
					<font color="blue"><bean:write name="status"></bean:write></font>
				</logic:notEmpty></div>
     <table class="role" width="600">
     <tr>
        <td>Select Course Category :
     <html:select property="coursecategoryId"  style="width:150px;" styleId="coursecategoryId" >
 <html:option value="-1" >--Select Course Category--</html:option>
 <html:option value="-2" >All</html:option>
 <html:options collection="coursecatlist" property="coursecategoryId" labelProperty="coursecategoryname" /> 
 </html:select></td>
     
     
     <td>Course Offered Name:
     <input type="text"  name="courseofferedname" size="25" maxlength="50"></td></tr>
  <tr>   <td><input type="submit" name="btn" onclick="return validate();" value="Save"> 
     <input type="reset" value="Reset" onclick="onfocus2()"></td>
     </tr>
     </table>
     </html:form>
     
     	
<div class="dataGridDisplay"  >
   <table id="normalTable" class="display" cellspacing="0" width="100%" align="center" >
				<thead>
    <tr>
    <th>SI No.</th>
    <th>Course Offered Name</th>
    <th style="width:100px;" >Status</th>
    <th style="width:155px;">Action</th>
    </tr>
    </thead>
    <tbody>
    <logic:notEmpty name="roleList">
    
    	<logic:iterate id="userrole" name="roleList" indexId="index">
    <tr>
    <td><%=index+1 %></td>
    <td><bean:write name="userrole" property="courseofferedname" /></td>
    <td>
    
    <logic:equal name="userrole"
							property="active" value="true">
							<font color="green">Active</font>
						</logic:equal> <logic:equal name="userrole" property="active" value="false">
							<font color="red">Inactive</font>
						</logic:equal>
    
    </td>
    
    <td>
    <logic:equal name="userrole"
							property="active" value="true">
							<a style="text-decoration: none;"
								href='courseoffered.do?action=changestatus&amp;courseofferedid=<bean:write name="userrole" property="courseofferedid"/>&amp;active=false'> <input type="button" value="Remove"></a>
								&nbsp; <a href='#'
								onclick='edit("<bean:write name="userrole" property="courseofferedid"/>","<bean:write name="userrole" property="coursecategoryId"/>","<bean:write name="userrole" property="courseofferedname"/>")'><input type="button" value="Edit"></a>
						</logic:equal> <logic:equal name="userrole" property="active" value="false">
							<a style="text-decoration: none;"
								href='courseoffered.do?action=changestatus&amp;courseofferedid=<bean:write name="userrole" property="courseofferedid"/>&amp;active=true'> <input type="button" value="Active"></a>
						</logic:equal>
    
</td>
    </tr>
  
    
    </logic:iterate>
     </logic:notEmpty>
  </tbody>
    
    </table>
     <logic:notEmpty name="roleList">
    <div align="center"><a href="courseoffered.do?action=excelReport"><button>Excel Export</button></a></div>
    </logic:notEmpty>
   </div>
    
    
     </div>
     
   <!-- End Main Content -->
	   <jsp:include page="/jsp/common/masterdata_footer.jsp"></jsp:include> 
 <script src="js/commonFunction.js"  type="text/javascript"></script>
  </body>
</html>

