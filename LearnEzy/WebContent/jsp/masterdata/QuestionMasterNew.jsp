	<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-nested.tld" prefix="nested"%>

<html>
<head>
<title>Itech Training :: Questions Master</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300' rel='stylesheet' type='text/css'>
<link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/cmsstyle.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/carousel.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>
       
<script src="<%=request.getContextPath() %>/js/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/owl.carousel.js" type="text/javascript"></script>
<script src="js/commonFunction.js"  type="text/javascript"></script>

<link href="dataTable/css/jquery.dataTables_themeroller.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.min.css"	rel="stylesheet" type="text/css" media="all" />
<script src="dataTable/js/jquery.dataTables.js"	type="text/javascript"></script>
<script src="dataTable/js/jquery.dataTables.min.js"	type="text/javascript"></script>
<script src="dataTable/js/settingDataTable.js" 	type="text/javascript"></script>  
 
<script type="text/javascript">


$(document).ready(function(){

$("#excelFile").click(function(){
	
	
$("#exceluploadspan").text("Please Download Excel File Format & Upload");

});


$("#excelFile").change(function(){
	
	
	$("#exceluploadspan").text("");

	});
$(".editJson").click(function(){
	var tag=$(this).attr("tag");
	var jsonObj=JSON.parse(tag);
	$("#questionId").val(jsonObj.questionId);
	$("#question").val(jsonObj.question);
	$("#ansA").val(jsonObj.ansA);
	$("#ansB").val(jsonObj.ansB);
	$("#ansC").val(jsonObj.ansC);
	$("#ansD").val(jsonObj.ansD);
	$("#crctAns").val(jsonObj.crctAns);
	$("#descriptiveAns").val(jsonObj.descriptiveAns);
	$("#course").val(jsonObj.courseId);
	document.forms[0].action="QuestionMasterNew.do?action=update";
	document.forms[0].btn.value="Update";
	document.forms[0].question.focus();
});

$('#excelFile').change(function () {
	var val = $(this).val().toLowerCase();
	//alert(val);
	var regex = new RegExp("(.*?)\.(xls|xl)$");
	if(!(regex.test(val))) {
	 	$(this).val('');
	 	alert('Please select .xls .xl format only');
	 } 
}); 
$("#resetBtn").click(function()
		{
$('select option:selected').removeAttr('selected');

		});


});


</script>

<script type="text/javascript">


function resetBtn(){
	
	document.forms[0].action="TopicMaster.do?action=list&courseId="+0+"";
	document.forms[0].submit();

}

function callList()
{
	document.forms[0].action="QuestionMasterNew.do?action=list";
	document.forms[0].submit();
}
function edit(questionId,question,courseId,ansA,ansB,ansC,ansD,crctAns,descAns)
{
		document.forms[0].action="QuestionMasterNew.do?action=update";
		document.forms[0].questionId.value = questionId;
		document.forms[0].question.value = question;
		document.forms[0].courseId.value = courseId;
		document.forms[0].ansA.value = ansA;
		document.forms[0].ansB.value = ansB;
		document.forms[0].ansC.value = ansC;
		document.forms[0].ansD.value = ansD;
		document.forms[0].crctAns.value = crctAns;
		document.forms[0].descriptiveAns.value = descAns;
		document.forms[0].btn.value="Update";
		document.forms[0].question.focus();

		}



function validate()
{

		if(document.forms[0].courseId.value == -1)
		{
			alert("Please select the Course");
			document.forms[0].courseId.focus();
			return false;
		}
	 


		
		if(document.forms[0].question.value == "")
		{
			alert("Please Enter the questions");
			document.forms[0].question.focus();
			return false;
		}
		
		
		if(document.forms[0].ansA.value == "" ) 
		{
			alert("Option A Can't be Empty ");
			document.forms[0].ansA.focus();
			return false;
		}
		
		if(document.forms[0].ansB.value == "")
		{
			alert("Option B Can't be Empty ");
			document.forms[0].ansB.focus();
			return false;
		}
		
		if(document.forms[0].ansC.value == "")
		{
			alert("Option C Can't be Empty ");
			document.forms[0].ansC.focus();
			return false;
		}
		
		if(document.forms[0].ansD.value == "")
		{
			alert("Option D Can't be Empty ");
			document.forms[0].ansD.focus();
			return false;
		}
		
		if(document.forms[0].crctAns.value ==  -1)
		{
			alert("Please select the Correct Option");
			document.forms[0].crctAns.focus();
			return false;
		}

		if(document.forms[0].ansA.value==document.forms[0].ansB.value || document.forms[0].ansA.value==document.forms[0].ansC.value || document.forms[0].ansA.value==document.forms[0].ansD.value){
			alert("Answer A is Duplicate");
			document.forms[0].ansA.focus();
			return false;
		}
		if(document.forms[0].ansB.value==document.forms[0].ansA.value || document.forms[0].ansB.value==document.forms[0].ansC.value || document.forms[0].ansB.value==document.forms[0].ansD.value){
			alert("Answer B is Duplicate");
			document.forms[0].ansB.focus();
			return false;
		}
		if(document.forms[0].ansC.value==document.forms[0].ansA.value || document.forms[0].ansC.value==document.forms[0].ansB.value || document.forms[0].ansC.value==document.forms[0].ansD.value){
			alert("Answer C is Duplicate");
			document.forms[0].ansC.focus();
			return false;
		}
		if(document.forms[0].ansD.value==document.forms[0].ansA.value || document.forms[0].ansD.value==document.forms[0].ansB.value || document.forms[0].ansD.value==document.forms[0].ansC.value){
			alert("Answer D is Duplicate");
			document.forms[0].ansD.focus();
			return false;
		}
		
}

function focus2()
{
	document.forms[0].action="QuestionMasterNew.do?action=list1";
	document.forms[0].submit();
	
}


function exclValidate(){
	if(document.forms[1].excelFile.value == ""){
		alert("Please Select  xl or xls extension file");
		
		return false;
	}
}
</script>
</head>
<body>
   <!-- Start Header -->
     <jsp:include page="/jsp/common/masterdata_header.jsp"></jsp:include>
   <!-- End Header -->
   
   <!-- Start Main Content -->
	 <div class="main" style="min-height:450px;">	 
	 	<h2>Question Master</h2>
    <html:form action="QuestionMasterNew.do?action=add" styleId="questionForm">
    <input type="hidden" name="questionId" id="questionId" />
    	<div id="status" align="center"><logic:notEmpty name="status">
					<font color="blue"><bean:write name="status"></bean:write></font>
				</logic:notEmpty></div>
				
    <table border="0" width="80%" align="center"  class="role">
		<col style="width: 14%;" />
		<col style="width: 36%;" />
		<col style="width: 10%;" />
		<col style="width: 34%;" />
		<tr height="50">
			
			<td><label>Course *</label></td>
					<td><html:select style="width:216px;" property="courseId" name="testForm"
						onchange="callList();" styleId="course">
						<html:option value="-1">-Select-</html:option>
						<html:options collection="courseList" property="courseId"
							labelProperty="courseName" />
					</html:select>
					
					
					
					</td>
			 <td>Questions*</td>
        <td><input type="text" name="question"  id="question"  maxlength="4000"/> </td>
		</tr>
		
		<tr height="50">
        <td><label>Option A *</label></td>
			<td><input type="text" name="ansA" id="ansA"  maxlength="4000" /></td>
        <td><label>Option B *</label></td>
			<td><input type="text" name="ansB"  id="ansB" maxlength="4000" /></td>
        </tr>
        <tr height="50">
        <td><label>Option C *</label></td>
			<td><input type="text" name="ansC"   id="ansC"  maxlength="4000" /></td>
        <td><label>Option D *</label></td>
			<td><input type="text" name="ansD" id="ansD" maxlength="4000" /></td>
        </tr>
        <tr>
        <td>Correct Option *</td>
     <td><select style="width:216px;" name="crctAns" id="crctAns">
						<option value="-1">Select</option>
						<option value="A">Option A</option>
						<option value="B">Option B</option>
						<option value="C">Option C</option>
						<option value="D">Option D</option>
					</select></td>
					<td><label>Answer Description </label></td>
			<td><input type="text" name="descriptiveAns" id="descriptiveAns" maxlength="4000" /></td>
                    </tr>

		<tr height="50">
			<td colspan="2" align="center" ><input type="submit" name="btn"
						onclick="return validate();" value="Save" style="padding:0.5% 1%;" />
						 <input type="reset" id="resetBtn"
				value="Reset"  style="padding:0.5% 1%;"/></td>
				     <td colspan="2"><a class="submit_btn" style="float:left" href="<%=request.getContextPath() %>/studentRegistration.do?action=downloadexcel&amp;filename=QuestionMaster.xls"/>Download Excel File Format</a>
				</td>
		</tr>

	</table>
		</html:form>
			<html:form action="QuestionMasterNew.do?action=excelUpload" enctype="multipart/form-data" onsubmit="return exclValidate();">
		
		<table border="0" align="center" class="role">
		<tr height="40">
            <td align="center">Excel Upload</td>
            <td style="width: 7%;"></td>
       				 <td><input type="file" name="excelFile" id="excelFile" /></td>
       				  <td><input type="submit" value="Submit"></input></td>
       				 <td> <span id="exceluploadspan"></span></td>
		</tr>
		</table>
		</html:form>
		
		
		<div class="dataGridDisplay"  >
    <table id="normalTable" class="display" cellspacing="0" width="100%" align="center" >
				<thead>
    <tr>
    <th>SI No.</th>
    <th>Question Name</th>
    <th style="width:100px;">Status</th>
    <th style="width:215px;">Action</th>
    </tr>
    </thead>
    <tbody>
    <logic:notEmpty name="questionList">
    <logic:iterate id="test" name="questionList" indexId="index">
    <tr>
    <td><%=index + 1%></td>
    <td><bean:write name="test"	property="question" /></td>

    <td>

<logic:equal name="test" property="active"
							value="true">
							<font color="green">Active</font>
						</logic:equal> <logic:equal name="test" property="active" value="false">
							<font color="red">Inactive</font>
						</logic:equal>
</td>
    <td>
    
    
    
    <logic:equal name="test" property="active"
							value="true">
							<a
								href='QuestionMasterNew.do?action=changestatus&amp;questionId=<bean:write name="test" property="questionId"/>&amp;active=false'>  <input type="button" value="Remove"></a>
								  <a href='#' tag="<bean:write name="test"	property="jEdit" />"  class="editJson" ><input type="button" value="Edit"></a>
								
								<a href='QuestionMasterNew.do?action=delete&amp;questionId=<bean:write name="test" property="questionId"/>' >  <input type="button" value="Delete"></a>
						</logic:equal> <logic:equal name="test" property="active" value="false">
							<a href='QuestionMasterNew.do?action=changestatus&amp;questionId=<bean:write name="test" property="questionId"/>&amp;active=true'>  <input type="button" value="Active"></a>
						</logic:equal>
    
</td>
    </tr>
    
  </logic:iterate>
   </logic:notEmpty>
   </tbody>
    </table>
    <logic:notEmpty name="questionList">
    <div align="center"><a href="QuestionMasterNew.do?action=excelReport"><button>Excel Export</button></a></div>
     </logic:notEmpty>
      </div>
     </div>
 
   <!-- End Main Content -->
	   
  <!-- Start Footer -->
   <jsp:include page="/jsp/common/masterdata_footer.jsp"></jsp:include> 
  <!-- End Footer -->
  <script type="text/javascript">
	document.forms[0].courseId.focus();
	
	//

  </script>
	 
  </body>
</html>

     