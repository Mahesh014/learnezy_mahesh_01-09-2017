<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>Itech Training :: Test Master</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv='cache-control' content='no-cache'>
<meta http-equiv='expires' content='0'>
<meta http-equiv='pragma' content='no-cache'>
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300'
	rel='stylesheet' type='text/css'>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/cmsstyle.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/font-awesome.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/carousel.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css"
	media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="src/DateTimePicker.css" />


<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/date/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/date/jquery-ui.css" />

<link href="dataTable/css/jquery.dataTables_themeroller.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.css" rel="stylesheet"
	type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.min.css" rel="stylesheet"
	type="text/css" media="all" />
<script src="dataTable/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="dataTable/js/jquery.dataTables.min.js"
	type="text/javascript"></script>
<script src="dataTable/js/settingDataTable.js" type="text/javascript"></script>

<script type="text/javascript" src="js/VideoAcceptOnly.js"></script>

<script type="text/javascript">
	$(document)
			.ready(
					function() {
						$("#testTypeId").val(-1);
						$("#questionDiv").hide();
						$("#courseId").val("-1");
						$("#addQuestions")
								.click(
										function() {
											if ($("#courseId").val() != "-1") {
												$("#dialog-message")
														.dialog(
																{
																	width : 940,
																	modal : true,
																	buttons : {
																		Ok : function() {
																			var qIds = "";
																			$(
																					".qCkBx")
																					.each(
																							function() {
																								var objClass = $(this);
																								if (objClass
																										.is(':checked')) {
																									trueFlag = true;
																									qIds = qIds
																											+ objClass
																													.val()
																											+ ",";
																								}
																							});
																			qIds = qIds
																					.substring(
																							0,
																							parseInt(qIds.length)
																									- parseInt(1));
																			//alert(qIds);
																			$(
																					"#qIds")
																					.val(
																							qIds);
																			$(
																					this)
																					.dialog(
																							"close");
																		},
																		Cancel : function() {
																			$(
																					this)
																					.dialog(
																							"close");
																		}
																	}
																});
											} else {
												alert("Please Select the Course");
											}
										});
						$("#courseId")
								.focusout(
										function() {
											var courseId = $("#courseId").val();
											//alert(courseId);	 
											var trTable = "";
											$
													.ajax({
														url : "QuestionMasterNew.do?action=questionByCourseid&courseId="
																+ courseId,
														success : function(r) {
															var json = JSON
																	.parse(r);
															$
																	.each(
																			json,
																			function(
																					i,
																					obj) {
																				trTable = trTable
																						+ '<tr><td width="5%"><input type="checkbox" name="qIds" class="qCkBx" value="'+obj.QuestionId+'"></input><td>'
																						+ '<td>'
																						+ obj.Question
																						+ '</td></tr>';

																			});
															$("#displayTable")
																	.html(
																			trTable);

														}
													});
										});

						$(".editBtn")
								.click(
										function() {
											var datarow = $(this).attr(
													'data-row');
											var trTable = "";
											var json = JSON.parse(datarow);
											//alert(datarow);
											$
													.each(
															json,
															function(i, obj) {
																trTable = trTable
																		+ '<tr><td width="5%"><input type="checkbox" name="qIds" checked="checked" class="qCkBx" value="'+obj.Qid+'"></input><td>'
																		+ '<td>'
																		+ obj.Question
																		+ '</td></tr>';
															});
											$("#displayTable").html(trTable);

											var tag = $(this).attr('tag');
											var resObj = JSON.parse(tag);
											$("#courseId").val(resObj.courseId);
											$("#testTypeId").val(
													resObj.testTypeID);
											$("#testId").val(resObj.testId);
											$("#tname").val(resObj.testname);
											$("#datepicker1").val(
													resObj.testDate);
											$("#testTime").val(resObj.testTime);
											$("#minScore").val(resObj.minScore);
											$("#achivementScore").val(
													resObj.achivementScore);
											$("#qIds").val(resObj.questionIds);
											$("#testForm")
													.attr("action",
															"TestMaster.do?action=update");
											$("#btnSave").prop('value',
													'Update');

											$("#dialog-message")
													.dialog(
															{
																width : 940,
																modal : true,
																buttons : {
																	Ok : function() {
																		var qIds = "";
																		$(
																				".qCkBx")
																				.each(
																						function() {
																							var objClass = $(this);
																							if (objClass
																									.is(':checked')) {
																								trueFlag = true;
																								qIds = qIds
																										+ objClass
																												.val()
																										+ ",";
																							}
																						});
																		qIds = qIds
																				.substring(
																						0,
																						parseInt(qIds.length)
																								- parseInt(1));
																		//alert(qIds);
																		$(
																				"#qIds")
																				.val(
																						qIds);
																		$(this)
																				.dialog(
																						"close");
																	},
																	Cancel : function() {
																		$(this)
																				.dialog(
																						"close");
																	}
																}
															});
										})

						$("#clearAll").click(function() {
							$(".qCkBx").each(function() {
								$(this).prop('checked', false);
								;
							});
						});

						$("#checkAll").click(function() {
							$(".qCkBx").each(function() {
								$(this).prop('checked', true);
								;
							});
						});

						$("#resetBtn").click(function() {
							$("#displayTable").html("");
							$("#testTypeId").val(-1);
							$("#tname").val("");
							$("#datepicker1").val("");
							$("#testTime").val("");
							$("#minScore").val("");
							$("#achivementScore").val("");
							$("#courseId").val("-1");
						});
					});
	$(function() {
		$("#datepicker1").datepicker({
			minDate : 1,
			dateFormat : 'dd/mm/yy'
		});

	});
</script>
<script type="text/javascript">
	function exclValidate() {
		if (document.forms[1].filename1.value == "") {
			alert("Please Select  xl or xls extension file");

			return false;
		}
	}
</script>
<script type="text/javascript">
	window.history.forward();
	function noBack() {
		window.history.forward();
	}
	function validate() {
		if (document.forms[0].tname.value.trim() == "") {
			alert("Test Name cannot be empty");
			document.forms[0].tname.focus();
			return false;
		} else if (document.forms[0].testTypeId.value == -1) {
			alert("Please Select the Test Type");
			document.forms[0].testTypeId.focus();
			return false;
		} else if (document.forms[0].minScore.value.trim() == "") {
			alert("Minimum score of test connot be empty");
			document.forms[0].minScore.focus();
			return false;
		} else if (document.forms[0].minScore.value.trim() == 0) {
			alert("Minimum score of test should be greater than 0");
			document.forms[0].minScore.focus();
			return false;
		}else if (document.forms[0].minScore.value.trim()>100) {
			alert("Minimum score of test should not be greater than 100");
			document.forms[0].minScore.focus();
			return false;
		}else if (document.forms[0].minScore.value != "" && document.forms[0].minScore.value > 100) {
			alert("Test Min Score should be lessthen 100");
			document.forms[0].minScore.focus();
			return false;
		} else if (document.forms[0].testTime.value.trim() == "") {
			alert("Test time connot be empty");
			document.forms[0].testTime.focus();
			return false;//achivementScore
		}else if (document.forms[0].testTime.value.trim() == 0) {
			alert("Test time atleast 1 mintue");
			document.forms[0].testTime.focus();
			return false;//achivementScore
		} else if (document.forms[0].tdate.value.trim() == "") {
			alert("Test Date cannot be empty");
			document.forms[0].tdate.focus();
			return false;
		}  else if (document.forms[0].courseId.value == -1) {
			alert("Please Select the Course");
			document.forms[0].courseId.focus();
			return false;
		} else if (document.forms[0].achivementScore.value.trim() =="") {
			alert("Achievement score connot be empty");
			document.forms[0].achivementScore.focus();
			return false;//achivementScore
		} else if (document.forms[0].achivementScore.value.trim() == 0) {
			alert("Achievement score should be greater than 1");
			document.forms[0].achivementScore.focus();
			return false;//achivementScore
		} else if (document.forms[0].achivementScore.value>100) {
			alert("Achievement score should not be greater than 100");
			document.forms[0].achivementScore.focus();
			return false;//achivementScore
		} else if (document.forms[0].achivementScore.value != ""
				&& document.forms[0].achivementScore.value > 100) {
			alert("AchivementScore should be lessthen 100");
			document.forms[0].achivementScore.focus();
			return false;
		} else if (document.getElementById("displayTable").innerHTML.trim() == "") {
			alert("Selected Course not having any Question");
			document.forms[0].courseId.focus();
			return false;
		} else {
			var trueFlag = false;
			$(".qCkBx").each(function() {
				if ($(this).is(':checked')) {
					trueFlag = true;
					//alert(trueFlag);
				}
			});

			if (trueFlag == false) {
				alert("Please Allocate Questions");
				return false;
			}
		}
	}
	function checknumber(inputtxt) {
		//alert("arg");
		re = /\D/g; // remove any characters that are not numbers
		socnum = inputtxt.value.replace(re, "");
		inputtxt.value = socnum;
	}

	function callList() {
		document.forms[0].action = "TestMaster.do?action=list";
		document.forms[0].submit();
	}

	function onDateChange() {
		if (document.forms[0].tdate.value.trim() != "") {
			document.forms[0].courseId.focus();
		}
	}

	function isValidName(uname) {
		re = /[^a-z,A-Z,0-9, ]/;
		var str = uname.value.replace(re, "");
		uname.value = str;
	}
</script>
<script type="text/javascript">
    function checkchacter(inputtxt) {
		//alert("arg");
			re = /[^a-z,A-Z, ]/; // remove any characters that are not numbers
		socnum = inputtxt.value.replace(re, "");
		inputtxt.value = socnum;
	}
    </script>
<style type="text/css">
.csDiv1 {
	color: blue;
	font-size: 12px;
	font-style: italic;
	font-weight: 500;
}
</style>
</head>
<body>

	<!-- Start Header -->
	<jsp:include page="/jsp/common/masterdata_header.jsp"></jsp:include>
	<!-- End Header -->
	<!-- Start Main Content -->
	<div class="main" style="min-height: 450px;">
		<h2>Test Master</h2>

		<html:form action="TestMaster.do?action=add" styleId="testForm">
			<table border="0" width="90%" align="center" class="role">

				<tr>
					<td colspan="4" align="center"><div id="status">
							<logic:notEmpty name="status">
								<font color="blue"><bean:write name="status"></bean:write></font>
							</logic:notEmpty>
						</div></td>
				</tr>
				<tr>
					<td><label>Test Name *</label></td>
					<td><input type="text" name="tname" id="tname" maxlength="250"
					 onkeyup="return checkchacter(this);" /> <input type="hidden"
						name="testId" id="testId" /> <input type="hidden" name="qIds"
						id="qIds"></input></td>

					<td><label>&nbsp;&nbsp;Test Type *</label></td>
					<td><html:select style="width:216px;" property="testTypeId"
							styleId="testTypeId">
							<html:option value="-1">-Select-</html:option>
							<html:options collection="testTypeList" property="testTypeId"
								labelProperty="testTypeName" />
						</html:select>
				</tr>

				<tr>
					<td><label>Test Min Score (%)*</label></td>
					<td><input type="text" name="minScore"
						onkeyup="return checknumber(this);" maxlength="2" id="minScore" />
						&nbsp;</td>

					<td><label>&nbsp;&nbsp;Test Timings (Minutes)*</label></td>
					<td><input type="text" name="testTime"
						onkeyup="return checknumber(this);" maxlength="3" id="testTime" /></td>


				</tr>
				<tr>
					<td><label>Test Date *</label></td>
					<td><input type="text" name="tdate" id="datepicker1"
						readonly="readonly" onchange="onDateChange();" /></td>
					<td><label>&nbsp;&nbsp;Course *</label></td>
					<td>
					<html:select style="width:216px;" property="courseId"
							styleId="courseId">
					<html:option value="-1">-Select-</html:option>
				    <html:options collection="courseList" property="courseId" labelProperty="courseName" />
					</html:select>
					</td>

				</tr>
				<tr>
					<td><label>Achivement Score (%)*</label></td>
					<td><input type="text" name="achivementScore"
						onkeyup="return checknumber(this);" maxlength="2"
						id="achivementScore" /></td>
					<td colspan="2" align="center"><a href="javascript:void(0);"
						id="addQuestions">Allocate Questions</a></td>
				</tr>

				<tr>

					<td></td>
					<td width="20%" align="right"><input type="submit" name="btn"
						id="btnSave" onclick="return validate();" value="Save"
						class="but1" /></td>
					<td><input type="button" value="Reset" id="resetBtn" /></td>
				</tr>
			</table>
		</html:form>

		<center>
			<html:form action="TestMaster.do?action=testExcelUpload"
				styleId="testForm" enctype="multipart/form-data"
				onsubmit="return exclValidate();">

				<tr>
					<td>Excel file<input type="file" name="filename1" id="testId"
						onchange="PreviewImage(9);" style="width: 210px;" />
					</td>
					<input type="submit" value="submit" id="submitid"></input>
								<td><a class="submit_btn" style="float:left" href="<%=request.getContextPath() %>/studentRegistration.do?action=downloadexcel&amp;filename=TestmasterExcel.xls"/>Download Excel File Format</a>
					
				</tr>
				<tr>
			</html:form>
		</center>

		<div id="questionDiv">
			<div id="dialog-message" title="Select The Question">

				<table border="0" width="100%" align="center" id="displayTable"
					cellpadding="5  "></table>
				<br></br> <a href="javascript:void(0)" id="checkAll"
					style="float: left; color: blue;" class="csDiv1">Select ALL</a> <a
					href="javascript:void(0)" id="clearAll" class="csDiv1"
					style="float: right; color: blue;">Clear ALL</a>

			</div>
		</div>
		<div class="dataGridDisplay">
			<table id="normalTable" class="display" cellspacing="0" width="100%"
				align="center">
				<thead>
					<tr>
						<th>SI No.</th>
						<th>Test Name</th>
						<th>Test Min Score</th>
						<th>Test Timing</th>
						<th>Test Type</th>
						<th>Course</th>
						<th>Achivement Score</th>
						<th>Status</th>
						<th style="width: 175px;">Action</th>
					</tr>
				</thead>
				<tbody>
					<logic:notEmpty name="testList">

						<logic:iterate id="testList" name="testList" indexId="index">
							<tr>
								<td><%=index + 1%></td>
								<td><bean:write name="testList" property="tname" /></td>
								<td><bean:write name="testList" property="minScore" /></td>
								<td><bean:write name="testList" property="testTime" /></td>
								<td><bean:write name="testList" property="testTypeName" /></td>
								<td><bean:write name="testList" property="courseName" /></td>
								<td><bean:write name="testList" property="achivementScore" /></td>
								<td><logic:equal name="testList" property="active"
										value="true">
										<font color="green">Active</font>
									</logic:equal> <logic:equal name="testList" property="active" value="false">
										<font color="red">InActive</font>
									</logic:equal></td>
								<td><logic:equal name="testList" property="active"
										value="true">
										<a
											href='TestMaster.do?action=changestatus&amp;testID=<bean:write name="testList" property="testId"/>&amp;active=false'><input
											type="button" value="Remove"></a>
					&nbsp;&nbsp;
				<input type="button" class="editBtn" value="Edit"
											data-row="<bean:write name="testList" property="jsonQIds" />"
											tag="<bean:write name="testList" property="jsonEdit" />">
									</logic:equal> <logic:equal name="testList" property="active" value="false">
										<a
											href='TestMaster.do?action=changestatus&amp;testID=<bean:write name="testList" property="testId"/>&amp;active=true'><input
											type="button" value="Active"></a>
									</logic:equal></td>


							</tr>
						</logic:iterate>
					</logic:notEmpty>
				</tbody>
			</table>
			<logic:notEmpty name="testList">
				<div align="center">
					<a href="TestMaster.do?action=excelReport"><button>Excel
							Export</button></a>
				</div>
			</logic:notEmpty>
		</div>
	</div>

	<!-- End Main Content -->

	<!-- Start Footer -->
	<jsp:include page="/jsp/common/masterdata_footer.jsp"></jsp:include>
	<!-- End Footer -->
	<script type="text/javascript">
		document.forms[0].tname.focus();
	</script>
	<script src="js/commonFunction.js" type="text/javascript"></script>
</body>
</html>
