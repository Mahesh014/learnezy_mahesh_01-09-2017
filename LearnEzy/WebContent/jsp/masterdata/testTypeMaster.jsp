<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Itech Training :: Test Type Master</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300' rel='stylesheet' type='text/css'>
<link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/cmsstyle.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/carousel.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>

<script src="<%=request.getContextPath() %>/js/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/owl.carousel.js" type="text/javascript"></script>

<link href="dataTable/css/jquery.dataTables_themeroller.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.min.css"	rel="stylesheet" type="text/css" media="all" />
<script src="dataTable/js/jquery.dataTables.js"	type="text/javascript"></script>
<script src="dataTable/js/jquery.dataTables.min.js"	type="text/javascript"></script>
<script src="dataTable/js/settingDataTable.js" 	type="text/javascript"></script>

<script type="text/javascript" src="js/VideoAcceptOnly.js"></script>

<script type="text/javascript">
window.history.forward();
function noBack() { window.history.forward(); }

 function edit(id,desc){
	 	document.getElementById("status").innerHTML="";
		document.forms[0].action="TestType.do?action=update";
		document.forms[0].testTypeId.value=id;
		document.forms[0].testType.value=desc;
		document.forms[0].btn.value="Update";
		document.forms[0].testType.focus();
	}
 function focus2(){
	document.getElementById('status').innerHTML="";
 	document.forms[0].roleid.value="";
 	alert(document.forms[0].testTypeId.value);
 	document.forms[0].action="testType.do?action=add";
 	document.forms[0].btn.value="Save";
 	document.forms[0].testType.focus();
 	document.getElementById('status').innerHTML="";
 }

 function validate(){
	  
		if(document.forms[0].testType.value.trim()==""){
			alert("Please enter Test Type Name ..!");
			document.forms[0].testType.focus();
			return false;
		}

		if (!document.forms[0].testType.value.match(/^[a-zA-Z ]*$/) && document.forms[0].testType.value !="")
	    {
	 	   document.forms[0].testType.value="";
	 	   document.forms[0].testType.focus(); 
	       alert("Please Enter only Alphabets");
	       return false;   
	    }
	    
	return true;
 }
 $(document).ready(function() {
 $('#excelFile').change(function () {
		var val = $(this).val().toLowerCase();
		var regex = new RegExp("(.*?)\.(xls|xlsx)$");
		if(!(regex.test(val))) {
		 	$(this).val('');
		 	alert('Please select .xls .xl format only');
		 } 
		});
	});
</script>
<script type="text/javascript">

function exclValidate(){
	if(document.forms[1].fileName.value == ""){
		alert("Please Select  xl or xls extension file");
		
		return false;
	}
}
</script>
<script type="text/javascript">
    function checkchacter(inputtxt) {
		//alert("arg");
			re = /[^a-z,A-Z, ]/; // remove any characters that are not numbers
		socnum = inputtxt.value.replace(re, "");
		inputtxt.value = socnum;
	}
    </script>
</head>

<body>
 
 
 <jsp:include page="/jsp/common/masterdata_header.jsp"></jsp:include>
 
   <!-- Start Main Content -->
	 <div class="main" style="min-height:450px;">	 
	 	<h2>Test Type Master</h2>
	 	
	 	 <html:form action="TestType.do?action=add">
	 	 <input type="hidden" name="testTypeId" />
	 	 <div id="status" align="center"><logic:notEmpty name="status">
					<font color="blue"><bean:write name="status"></bean:write></font>
				</logic:notEmpty></div>
     <table class="role" width="60%">
			 

				 

				<tr>
					
					<td><label>Test Type Name</label></td>
					<td><input type="text" name="testType" size="25" maxlength="500"	class="textfield1" onkeyup="return checkchacter(this);"/></td>
					<td><input type="submit" name="btn"	onclick="return validate();" value="Save" class="but1" /> &nbsp;<input type="reset" value="Reset" onclick="focus2()" /></td>
					 
				</tr>
			</table>
		</html:form>
		
		
		<center>
		<html:form action="TestType.do?action=testTypeExcelUpload" enctype="multipart/form-data" onsubmit="return exclValidate();">
		
		
		<tr>
				<td>
				Excel file:<input  type="file" name="fileName" id="excelFile"    style="width: 210px;" />
				<input type="submit" value="submit" id="submitid"></input></td>
			<td><a class="submit_btn" style="float:left" href="<%=request.getContextPath() %>/studentRegistration.do?action=downloadexcel&amp;filename=Testtypemaster.xls"/>Download Excel File Format</a>
				
				</tr>
		<hr/>
		</html:form>
		</center>
		<hr />

		<div class="dataGridDisplay"  >
				<table id="normalTable" class="display" cellspacing="0" width="100%" align="center" >
				<thead>

				<tr>
					<th  >Sl No.</th>
					<th  >Test Type</th>
					<th style="width:100px;" >Status</th>
					<th  style="width:155px;">Action</th>
				</tr>
				</thead>
				<tbody><logic:notEmpty name="testTypeList">
				<logic:iterate id="testtype" name="testTypeList" indexId="index">
					<tr class="tdContent2">
						<td align="center" style="color: black"><%=index+1 %></td>
						<td align="left" style="color: black"><bean:write
							name="testtype" property="testType" /></td>
													<td align="center"><logic:equal name="testtype"
							property="active" value="true">
							<font color="green">Active</font>
						</logic:equal> <logic:equal name="testtype" property="active" value="false">
							<font color="red">Inactive</font>
						</logic:equal></td>
						<td align="center"><logic:equal name="testtype"
							property="active" value="true">
							<a
								href='TestType.do?action=changestatus&amp;testTypeID=<bean:write name="testtype" property="testTypeId"/>&amp;active=false'> <input type="button" value="Remove"></a>
								&nbsp; <a href='#'
								onclick='edit("<bean:write name="testtype" property="testTypeId"/>","<bean:write name="testtype" property="testType"/>")'> <input type="button" value="Edit"></a>
						</logic:equal> <logic:equal name="testtype" property="active" value="false">
							<a
								href='TestType.do?action=changestatus&amp;testTypeID=<bean:write name="testtype" property="testTypeId"/>&amp;active=true'> <input type="button" value="Active"></a>
						</logic:equal></td>
						
						
						
						
					</tr>
				</logic:iterate>
				
		</logic:notEmpty>
		</tbody>
			</table>
			<logic:notEmpty name="testTypeList">
			<div align="center"><a href="TestType.do?action=excelReport"><button>Excel Export</button></a></div>
			</logic:notEmpty>
		</div>

</div>
 
 
     
   <!-- End Main Content -->
	   <jsp:include page="/jsp/common/masterdata_footer.jsp"></jsp:include> 
   <script type="text/javascript">
  document.forms[0].testType.focus();
  </script>
  <script src="js/commonFunction.js"  type="text/javascript"></script>
  </body>
</html>