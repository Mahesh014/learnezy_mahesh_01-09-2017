  <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  <%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html" %>
  <%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean" %> 
  <%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<script type="text/javascript" src="jquery-1.11.0.min.js"></script>

<link href="<%=request.getContextPath() %>/JQGrid/Css/themes/base/minified/jquery-ui.min.css" rel="stylesheet"/>
<link href="<%=request.getContextPath() %>/JQGrid/Css/jquery.jqGrid/ui.jqgrid.css" rel="stylesheet" />
<script src="<%=request.getContextPath() %>/JQGrid/Scripts/i18n/grid.locale-en.js"></script>
<script src="<%=request.getContextPath() %>/JQGrid/Scripts/jquery.jqGrid.min.js"></script>

<script type="text/javascript" src="<%= request.getContextPath() %>/js/questionsGrid.js"></script>

<title>Itech Training :: Login</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300' rel='stylesheet' type='text/css'> -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/cmsstyle.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/carousel.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>
        <link rel="stylesheet" type="text/css" href="css/loginstyle.css" />
		<script src="js/cufon-yui.js" type="text/javascript"></script>
		<script src="js/ChunkFive_400.font.js" type="text/javascript"></script>
		<script type="text/javascript">
			Cufon.replace('h1',{ textShadow: '1px 1px #fff'});
			Cufon.replace('h2',{ textShadow: '1px 1px #fff'});
			Cufon.replace('.back');
		</script>

<link rel="stylesheet" type="text/css" href="src/DateTimePicker.css" />	
<script type="text/javascript" src="src/DateTimePicker.js"></script>
        
<!-- mine -->
<style type="text/css">
.field_set{
 border-color:#;
}
#dvLoading

{
 position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('images/loader.gif') 50% 50% no-repeat rgb(249,249,249);
}
</style>

<script type="text/javascript" src="js/validation.js"></script>


<script type="text/javascript"
	src="<%=request.getContextPath() %>/js/homecalender.js"></script>
<script language="javascript"
	src="<%=request.getContextPath()%>/js/common.js"></script>

	<script type="text/javascript" src="<%=request.getContextPath() %>/date/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/date/jquery-ui.css" />
<script type="text/javascript">



$(window).load(function(){

  $('#dvLoading').delay(500).fadeOut(2000);

});


$(document).ready(function(){

var index=0;

	$('#addButton1').click(function(){
		
		var trPoFieldSetId= "trPoFieldSet";
		
		index = index+1;
		console.log(index);
		var trPoFieldSet =$(this).closest("table").closest("tr");
		var trPoFieldSetClone= $(trPoFieldSet).clone(true);

		GenerateNewIdsForClonedTR(trPoFieldSetClone,index);
		var trPoFieldSetCloneNewId= trPoFieldSetId+index;
		$(trPoFieldSetClone).attr("id",trPoFieldSetCloneNewId);
		$(trPoFieldSetClone).insertAfter(trPoFieldSet);

		var removeBtnIdStatrsWith = "input[id^='removeButton1']";
		var element= $(trPoFieldSet).find(removeBtnIdStatrsWith);
		$(element).hide();
		$(this).hide();
		
	});



$("#removeButton1").click(function(){
		
		var trPoFieldSet=	$(this).closest("table").closest("tr");
		var trPoFieldSetPrevious=$(trPoFieldSet).prev();	//$(this).closest("table").closest("tr").closest("tr");
		//console.log(trPoFieldSet1.length);
		//console.log(trPoFieldSet1.attr('id'));
		var addBtnIdStatrsWith= "input[id^='addButton1']";
		var elementBtnAdd= $(trPoFieldSetPrevious).find(addBtnIdStatrsWith);
		$(elementBtnAdd).show();

		var removeBtnIdStatrsWith= "input[id^='removeButton1']";
		var elementBtnRemove= $(trPoFieldSetPrevious).find(removeBtnIdStatrsWith);

		var tblTop = $("#room");
		var noOfFieldSets= $("#room fieldset").length;
		
		//console.log(noOfFieldSets);
		if(noOfFieldSets>2){
			$(elementBtnRemove).show();
		}
		else{
			$(elementBtnRemove).hide();
		}
		$(trPoFieldSet).remove();
		
	});


function GenerateNewIdsForClonedTR(trPoFieldSet,i){
	var arrIds=[{Tag:"input",Id:"question"},{Tag:"select",Id:"crctAns"},
	{Tag:"input",Id:"ansB"},{Tag:"input",Id:"ansA"},{Tag:"input",Id:"ansC"},{Tag:"input",Id:"ansD"},
	{Tag:"input",Id:"addButton1"},{Tag:"input",Id:"removeButton1"}];
	$.each(arrIds,function(index, obj){
				
		var idStartsWith= obj.Tag+"[id^='"+obj.Id+"']";
		var element= $(trPoFieldSet).find(idStartsWith);
		var newId=obj.Id+i;
	   $(element).attr("id",newId);
	   if(obj.Id=="removeButton1"){
		   $(element).show();
	   }
	   if(obj.Id=="question"){
		   $(element).val("");
		}
	   if(obj.Id=="ansA"){
		   $(element).val("");
		}
	   if(obj.Id=="ansB"){
		   $(element).val("");
		}
	   if(obj.Id=="ansC"){
		   $(element).val("");
		}
	   if(obj.Id=="ansD"){
		   $(element).val("");
		}
	   if(obj.Id=="crctAns"){
		   $(element).val("-1");
		}
				});
}


	

	
    $("#courseID").change(function() {

    var courseID=$('#courseID :selected').val();

     if(courseID!="-1")
    {
    $.ajax({

    	url:"Testquestion.do?action=topicsList&courseID="+courseID,
    	success:function(r){
    		var json= JSON.parse(r);
    		
    	     $("#topicId").empty();
    	     
    	     var firstoption=$("<option>",{text:"----SELECT---",value:"-1"});
    	     
    	     $("#topicId").append(firstoption);

     		 $.each(json,function(index,obj){
         		 
             var Option=$("<option>",{text:obj.TopicName,value:obj.TopicID});
             
             $("#topicId").append(Option);
        		});
	}
    });
    }
	
	});

    $("#topicId").change(function(){
        var topicID=$("#topicId option:selected").val();
        if(topicID!="-1")
        {
            $.ajax({
            	url:"Testquestion.do?action=subTopicList&TopicID="+topicID,
            	success:function(r){
            		var json=JSON.parse(r);
                    $("#subTopicId").empty();   
            		 var firstOption=$("<option>",{text:"---SELECT---",value:"-1"});
            		 $("#subTopicId").append(firstOption);
            		   
            		$.each(json,function(index,obj){
                        var option=$("<option>",{text:obj.SubTopicName,value:obj.SubTopicID});
                        $("#subTopicId").append(option);
            		});
        	}
            });
        }	
        });
});

</script>

</head>
<body>

 <jsp:include page="/jsp/common/masterdata_header.jsp"></jsp:include>

<br></br>
<div align="center" style="margin-left:55px;margin-right:55px;padding-top:10px;padding-bottom:10px;background-color: #ccc" class="field_set">
<html:form action="Testquestion.do?action=addQuestions">
<div align="center"><logic:notEmpty name="addQuery"> 
<font color="red"> <bean:write name="addQuery"/></font>
</logic:notEmpty></div>

<table id="room" style="padding-top: 5px;" cellpadding="5"  cellspacing="5">
<tr height="30">
			
			<td><label>Course *</label></td>
			<td><html:select property="courseId" styleId="courseID">
				<html:option value="-1">---SELECT---</html:option>
				<html:options collection="usersList" property="courseId" labelProperty="courseName" />
			</html:select></td>

      <td><label>Topic Name *</label></td>
       <td>
			<select name="topicId" id="topicId">
			<option value="-1">---SELECT---</option>
		</select></td>
		<td><label>Sub Topic Name</label></td>
		<td>
		<select name="subTopicId" id="subTopicId">
			<option value="-1">---SELECT---</option>
		</select>
		</td>
		      <td><label>Test Name *</label></td>
		      <td><input type="text" name="testName"></input></td>
</tr>


<tr id="trPoFieldSet">
<td colspan="4">
<fieldset id="poFieldSet">
<legend>
Add Questions
</legend>
<table>
<tr height="30">

        <td>Question</td>
        <td><input type="text" name="question" id="question"/> <input type="hidden" name="questionId" /></td>
        </tr>
        
	<tr height="30">
        <td><label>Option A </label></td>
			<td><input type="text" name="ansA" id="ansA"/></td>
        <td><label>Option B </label></td>
			<td><input type="text" name="ansB" id="ansB"/></td>
        </tr>
        
      <tr height="30">
        <td><label>Option C</label></td>
			<td><input type="text" name="ansC" id="ansC"/></td>
        <td><label>Option D </label></td>
			<td><input type="text" name="ansD" id="ansD"/></td>
        </tr>
        
        <tr height="30">
        <td>Correct Option</td>
        <td>
        <select name="crctAns" id="crctAns">
						<option value="-1">--SELECT--</option>
						<option value="A">Option A</option>
						<option value="B">Option B</option>
						<option value="C">Option C</option>
						<option value="D">Option D</option>
	    </select>
	    </td>
	     <td class="clsTd"><br/> <input type="button"  id="addButton1" value="Add More Items" /></td>
    <td class="clsTd"><br/> <input type="button"  id="removeButton1" style="display:none;" value="Remove item" /></td>
	    </tr>	
</table>
</fieldset>
</td>
   </tr>

		<tr>
<td colspan="4" align="center"><input type="submit" name="btn"
	onclick="return validate();" value="Save" style="padding:0.5% 1%;" /> 
	<input type="reset" value="Reset" onclick="focus2()" style="padding:0.5% 1%;"/></td>
		</tr>

</table>
</html:form>

</div>
<br></br>  

<div align="center">

<table id="tbl" align="center" width="500" style="padding-top: 10px;border: 1px solid;">
</table>

<div id="divPager">
        </div></div>
<br></br>       
        
   <div align="center" id="status" style="color: red;"> </div>     
    <br></br>  
       
        <div align="center" id="divHide" style="display: none;">
<table id="tbl1" align="center" width="500" style="padding-top: 10px;border: 1px solid;">
</table>

     <br></br>  
   <label>Allocate Questions to &nbsp;&nbsp;&nbsp;&nbsp;</label><select id="UserName"></select>&nbsp;&nbsp;&nbsp;<input type="button" value="Allocate" id="allocateID" ></input> 
  
</div>

<br></br>
               
<div class="footer-bottom" >
			 	<div class="wrap">
			 	<div class="copy-r ight">
			 		<p>Itech Training, Copyright 2015, All Rights Reserved. Designed by  <a href="http://www.itechtraining.in/" target="_blank">Itech Solutions</a></p>
			 	</div>
			 	
			 	<div class="social-icons">
			 		<ul>
			 			<li><a href="#"><i class="fa fa-twitter"></i></a></li>
			 			<li><a href="#"><i class="fa fa-facebook"></i></a></li>
			 			<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
			 			<li><a href="#"><i class="fa fa-youtube"></i></a></li>
			 			<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
			 		</ul>
			 	</div>
			 	
			 	<div class="clear"></div>
	          </div>
</div>
</body>
</html>