
<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Itech Training :: City Master</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300' rel='stylesheet' type='text/css'>
<link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/cmsstyle.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/carousel.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>

<script src="<%=request.getContextPath() %>/js/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/owl.carousel.js" type="text/javascript"></script>

<link href="dataTable/css/jquery.dataTables_themeroller.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.min.css"	rel="stylesheet" type="text/css" media="all" />
<script src="dataTable/js/jquery.dataTables.js"	type="text/javascript"></script>
<script src="dataTable/js/jquery.dataTables.min.js"	type="text/javascript"></script>
<script src="dataTable/js/settingDataTable.js" 	type="text/javascript"></script>

<script type="text/javascript">
function fetch(){
	if(document.forms[0].countryid.value!="-1"){
		document.forms[0].action="City.do?action=list";
		document.forms[0].submit();
	}
}
function fetch2(){
	if(document.forms[0].stateid.value!="-1"){
	  	document.forms[0].action="City.do?action=list";
		document.forms[0].submit();
	}
}

function edit(cityid,cityname,stateid,countryid){
	document.getElementById('status').innerHTML="";
	document.forms[0].action="City.do?action=update";
	document.forms[0].cityid.value=cityid;
	document.forms[0].cityname.value=cityname;
	document.forms[0].stateid.value=stateid;
	document.forms[0].countryid.value=countryid;
	document.forms[0].btn.value="Update";
	document.forms[0].countryid.focus();
}
function checkchacter(inputtxt) {
	//alert("arg");
		re = /[^a-z,A-Z, ]/; // remove any characters that are not numbers
	socnum = inputtxt.value.replace(re, "");
	inputtxt.value = socnum;
}

function changeStatus(cityid,stateid,val) {
	var countryid=document.forms[0].countryid.value;
	document.forms[0].action="City.do?action=changestatus"+"&cityid="+cityid+"&stateid="+stateid+"&countryid="+countryid+"&Active="+ val;
	document.forms[0].submit();
}
function resetBtn(){
	document.getElementById('status').innerHTML="";
	document.forms[0].cityname.value="";
	document.forms[0].stateid.value="-1";
	document.forms[0].countryid.value="-1";
	document.forms[0].countryid.focus();
	return false;
}
 
function onload(){
	document.forms[0].countryid.focus();
}

function validate(){
	   
	if(document.forms[0].countryid.value.trim()=="-1"){
		alert("Please Select Country");
		document.forms[0].countryid.focus();
		return false;
	}else
	if(document.forms[0].stateid.value.trim()=="-1"){
		alert("Please Select State");
		document.forms[0].stateid.focus();
		return false;
	}else
	if(document.forms[0].cityname.value.trim()==""){
		alert("Please enter City Name");
		document.forms[0].cityname.focus();
		return false;
	}else
	if (!document.forms[0].cityname.value.match(/^[a-zA-Z ]*$/) && document.forms[0].cityname.value !=""){
 	   document.forms[0].cityname.value="";
 	   document.forms[0].cityname.focus(); 
       alert("Please Enter only Alphabets");
       return false;   
    }

}
</script>

<script type="text/javascript">

   function preventBack(){window.history.forward();}
    setTimeout("preventBack()", 0);
    window.onunload=function(){null};
</script>

</head>
<body onload="onload()">
 
 <jsp:include page="/jsp/common/masterdata_header.jsp"></jsp:include>
 
   <!-- Start Main Content -->
	 <div class="main" style="min-height:450px;">	 
	 	<h2>City Master</h2>
   <html:form action="City.do?action=add" styleId="cityForm">
  <table class="role" width="900">
 <tr>
		<td colspan="6" align="center">
		<div align="center" id="status"><logic:notEmpty name="status">
			<font color="red"><bean:write name="status"></bean:write></font>
		</logic:notEmpty></div>
		</td>
		</tr>
				<tr >
					<td >Country Name*: 
						<html:select property="countryid" styleClass="select1" onchange="fetch();">
						<html:option value="-1">--Select country--</html:option>
						
						
						<html:options collection="countryList" property="countryid" labelProperty="countryname" />
						
						
						
						
					</html:select></td>
					
					<td >State Name*: 
						<html:select property="stateid" styleClass="select1" onchange="fetch2()">
						<html:option value="-1">--Select State--</html:option>
						<html:options collection="stateList" property="stateid"
							labelProperty="statename" />
					</html:select></td>
					
					<td>City Name*:<input type="hidden" name="cityid" /> 
					 	<input type="text" name="cityname" id="cityname" maxlength="80" onkeyup="return checkchacter(this);" /></td>
					
					</tr>
					<tr >
					<td colspan="3" align="center" >
						<input type="submit" name="btn" value="Save" onclick="return validate();" />&nbsp; &nbsp; 
						<input type="button"  value="Reset" onclick="resetBtn();" />
					</td>
				</tr>
</table>
  </html:form>
     
     	
     <div class="dataGridDisplay"  >
   <table id="normalTable" class="display" cellspacing="0" width="100%" align="center" >
				<thead>
  <tr height="30"
			style="background: #FFFFFF; color: #000; font-weight: bold;">
			<td>SI.NO</td>
			<td>City Name</td>
			<td class="no-print" style="width:100px;">Status</td>
			<td class="no-print" style="width:155px;">Action</td>
		</tr>
		</thead>
		<tbody>
		<logic:notEmpty name="cityList">
		<logic:iterate id="city" name="cityList" indexId="index">
			<tr height="30">

				<td><%=index + 1%></td>
				<td><bean:write name="city" property="cityname" /></td>
				<td class="no-print"><logic:equal name="city" property="active" value="true">
					<font color="green">Active</font>
				</logic:equal> <logic:equal name="city" property="active" value="false">
					<font color="red">Inactive</font>
				</logic:equal></td>
				<td class="no-print">
				  
				<logic:equal name="city" property="active" value="true">
					<a href="#"
						onclick='changeStatus("<bean:write name="city" property="cityid"/>","<bean:write name="city" property="stateid"/>",false)'><input
						type="button" value="Remove" /></a>
					<a href='#'
						onclick='edit("<bean:write name="city" property="cityid"/>","<bean:write name="city" property="cityname"/>","<bean:write name="city" property="stateid"/>","<bean:write name="city" property="countryid"/>")'><input
						type="button" value="Edit" /></a>
				</logic:equal> <logic:equal name="city" property="active" value="false">
					<a href="#"
						onclick='changeStatus("<bean:write name="city" property="cityid"/>","<bean:write name="city" property="stateid"/>",true)'><input
						type="button" value="Activate"
						  /></a>
				</logic:equal>
				 
				</td>
			</tr>
		</logic:iterate>
	</logic:notEmpty>
	</tbody>
	</table>
	<logic:notEmpty name="cityList">
	<div align="center"><a href="City.do?action=excelReport"><button>Excel Export</button></a></div>
	</logic:notEmpty>
    </div>
   
    
    
     </div>
     
   <!-- End Main Content -->
	   <jsp:include page="/jsp/common/masterdata_footer.jsp"></jsp:include> 
 <script src="js/commonFunction.js"  type="text/javascript"></script>
 <script type="text/javascript">
 document.forms[0].countryid.focus();
 </script>
  </body>
</html>

