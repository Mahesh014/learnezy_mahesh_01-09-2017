<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-nested.tld" prefix="nested"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Itech Training :: Notification Master</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv='cache-control' content='no-cache'>
<meta http-equiv='expires' content='0'>
<meta http-equiv='pragma' content='no-cache'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300'	rel='stylesheet' type='text/css'>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"	media="all" />
<link href="css/cmsstyle.css" rel="stylesheet" type="text/css"	media="all" />
<link href="css/font-awesome.css" rel="stylesheet" type="text/css"	media="all" />
<link href="css/carousel.css" rel="stylesheet" type="text/css"	media="all" />
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css"	media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="src/DateTimePicker.css" />


<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="src/DateTimePicker.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/date/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css"	href="<%=request.getContextPath()%>/date/jquery-ui.css" />

<link href="dataTable/css/jquery.dataTables_themeroller.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.min.css"	rel="stylesheet" type="text/css" media="all" />
<script src="dataTable/js/jquery.dataTables.js"	type="text/javascript"></script>
<script src="dataTable/js/jquery.dataTables.min.js"	type="text/javascript"></script>
<script src="dataTable/js/settingDataTable.js" 	type="text/javascript"></script>

<!-- Text Editor -->
<script type="text/javascript">
	function valueCall(){
		if(document.forms[0].courseId.value == -1 ){
			alert("Please Select the Course Name ..!");
			document.forms[0].courseId.focus();
			return false;
		}
		if(document.forms[0].notification.value == "" )	{
			alert("Please Enter the Notification ..!");
			document.forms[0].notification.focus();
			return false;
		}
		if(document.forms[0].expiryDate.value == "") {
			alert("Please Enter the Expiry Date ..!");
			document.forms[0].expiryDate.focus();
			return false;
		}
	}
	
	function edit(nId,cid,nuti,expDate){
		document.forms[0].notificationID.value =nId;
		document.forms[0].courseId.value =cid;
		document.forms[0].notification.value =nuti;
		document.forms[0].expiryDate.value =expDate;
		document.forms[0].action="NotificationMaster.do?action=update";
		document.forms[0].btn.value="Update";
		document.forms[0].notification.focus();
	}

function listMetodCall()
{
	document.forms[0].action="NotificationMaster.do?action=list";
	document.forms[0].submit();
}
</script>

<style>
.header-bottom {
    background: none repeat scroll 0 0 #e58b30;
    color: #fff;
    float: left;
    padding: 1% 0;
    width: 100%;
}
.sub_btn{
  background: #e58b30;
  color: #fff;
  width: 72px;
  border: medium none;
  padding: 0.5%;
  -webkit-box-shadow: 0 0 10px rgba(0, 0, 0, 0.4);
  -moz-box-shadow: 0 0 10px rgba(0, 0, 0, 0.4);
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.4);
  border-radius: 5px;
  margin: 0 1%;
  font-size:15px;
}
</style>
<script>
    $(document).ready(function() {
        
       	$("#resetBtn").click(function(){
         	$("#courseId").val("-1");
 		});
       
       	$(function(){
    	   $( "#expiryDate" ).datepicker({dateFormat: 'yy-mm-dd'});
   		 
   		});
        $(".editJson").click(function(){
   		 	var tag=$(this).attr("tag");
   			var jsonObj=JSON.parse(tag);
   			$("#courseId").val(jsonObj.courseId);
   			$("#notificationID").val(jsonObj.notificationID);
   			$("#notification").val(jsonObj.notification);
   			$("#expiryDate").val(jsonObj.expiryDate);
   			document.forms[0].action="NotificationMaster.do?action=update";
   			document.forms[0].btn.value="Update";
   			document.forms[0].notification.focus();
   	 });
    });
    </script>
</head>
<body>
  <jsp:include page="/jsp/common/masterdata_header.jsp"></jsp:include>
   <!-- Start Main Content -->
	 <div class="main" style="min-height:450px;">	 
	 	<h2>Notification Master</h2>
    <div id="status" align="center"><logic:notEmpty name="status">
					<font color="blue"><bean:write name="status"></bean:write></font>
				</logic:notEmpty></div>
   <html:form action="/NotificationMaster.do?action=add" >
    <input type="hidden" name="notificationID" id="notificationID"	 />
    <table border="0" width="80%" align="center"  class="role1">
		<tr height="50">
			
			<td><label>Course *</label></td>
			<td><html:select property="courseId" styleId="courseId" onchange="listMetodCall()">
				<html:option value="-1">-Select-</html:option>
				<html:options collection="courseList" property="courseId" labelProperty="courseName" />
			</html:select></td>
			
			
			<td><label>Notification *</label></td>
			<td><input type="text" name="notification"  id="notification"   maxlength="4000"/></td>
			
			
			<td><label>Expiry Date * </label></td>
			<td><input type="text" name="expiryDate" id="expiryDate" readonly="readonly" onchange="onDateChange();" /></td>
			  
	   </tr>
	
		<tr height="50">
			<td colspan="6" align="center" >
			<input class="sub_btn" type="submit" name="btn" value="Submit" onclick="return valueCall();" style="padding:0.5% 1%;" />
			<input type="reset"	value="Reset" style="padding:0.5% 1%;" id="resetBtn"/> 
		</tr>

	</table>
     </html:form> 	
		
 
    <table id="normalTable" class="display" cellspacing="0" width="100%" align="center" >
	<thead>
    <tr>
    <th>SI No.</th>
    <th>Course Name</th>
    <th>Notification</th>
    <th>Expiry Date</th>
    <th>Status</th>
    <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <logic:notEmpty name="notificationList">
    <logic:iterate id="notification" name="notificationList" indexId="index">
    <tr>
    <td><%=index+1 %></td>
    <td><bean:write name="notification"	property="courseName" /></td>
    <td><bean:write name="notification"	property="notification" /></td>
    <td><bean:write name="notification"	property="expiryDate" /></td>
	<td><logic:equal name="notification" property="active"	value="true">
			<font color="green">Active</font>
		</logic:equal> 
		<logic:equal name="notification" property="active" value="false">
			<font color="red">InActive</font>
		</logic:equal>
</td>

    <td>
    
    <logic:equal name="notification" property="active"	value="true">
			<a href='NotificationMaster.do?action=changestatus&amp;notificationID=<bean:write name="notification" property="notificationID"/>&amp;active=false'><input type="button" value="Remove"></a>
			&nbsp;&nbsp;
			<a href="#" tag="<bean:write name="notification" property="jEdit"/>" class="editJson" ><input type="button" value="Edit"></a>
	</logic:equal> 
	<logic:equal name="notification" property="active" value="false">
			<a href='NotificationMaster.do?action=changestatus&amp;notificationID=<bean:write name="notification" property="notificationID"/>&amp;active=true'><input type="button" value="Active"></a>
	</logic:equal>
   
    
</td>
    </tr>
   
 </logic:iterate>
  </logic:notEmpty>
  </tbody>
  </table>
     <logic:notEmpty name="notificationList">
    <div align="center"><a href="NotificationMaster.do?action=excelReport"><button>Excel Export</button></a></div>
    </logic:notEmpty>
	 
     </div>
    
   <!-- End Main Content -->
	   
 <jsp:include page="/jsp/common/masterdata_footer.jsp"></jsp:include> 
  <script type="text/javascript">
	document.forms[0].courseId.focus();
	</script>
	<script src="js/commonFunction.js"  type="text/javascript"></script>
  </body>
</html>




    	
    	
            