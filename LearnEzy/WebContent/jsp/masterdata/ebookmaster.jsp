<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-nested.tld" prefix="nested"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Itech Training :: eBook Master</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="icon" type="image/png" href="<%=request.getContextPath() %>/images/logo.png"></link>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="UI_style.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath() %>/css/stylesheet_tms.css"	rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath() %>/css/search_data.css"	rel="stylesheet" type="text/css" /> 
<link href="<%=request.getContextPath() %>/css/style1.css"	rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/cmsstyle.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />

<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>
<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>

<!-- Include jQuery form & jQuery script file. -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js" ></script>
<script src="http://malsup.github.com/jquery.form.js" ></script>
<script src="js/fileUploadScript.js" ></script>
<!-- Include css styles here -->


<link href="dataTable/css/jquery.dataTables_themeroller.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.min.css"	rel="stylesheet" type="text/css" media="all" />
<script src="dataTable/js/jquery.dataTables.js"	type="text/javascript"></script>
<script src="dataTable/js/jquery.dataTables.min.js"	type="text/javascript"></script>
<script src="dataTable/js/settingDataTable.js" 	type="text/javascript"></script>


<!-- Text Editor -->
<script type="text/javascript">
	function valueCall(){
		if(document.forms[0].courseId.value == -1 ){
			alert("Please Select the Course Name ..!");
			document.forms[0].courseId.focus();
			return false;
		}
		if(document.forms[0].eBookName.value == "" )	{
			alert("Please Enter the eBook Name ..!");
			document.forms[0].eBookName.focus();
			return false;
		}

		if(document.forms[0].hiddenpdffile.value == "" )	{
			alert("Please Upload The Ebook..!");
			document.forms[0].eBookName.focus();
			return false;
		}
		
	}
	

function listMetodCall(){
	document.forms[0].action="EBookDetailsMaster.do?action=list";
	document.forms[0].submit();
}
</script>



 
 
<style>
.header-bottom {
    background: none repeat scroll 0 0 #e58b30;
    color: #fff;
    float: left;
    padding: 1% 0;
    width: 100%;
}
</style>
<script>
    $(document).ready(function() {
        $("#hiddenpdfid").attr('readonly',true)
       $("#resetBtn").click(function(){
         	$("#courseId").val("-1");
 		});

     
       	$('#imageFile').change(function () {
       		var val = $(this).val().toLowerCase();
       		//alert(val);
       		var regex = new RegExp("(.*?)\.(pdf)$");
       		if(!(regex.test(val))) {
       		 	$(this).val('');
       		 	alert('Please select PDF format only');
       		 } 
       		else
       		{
       			var filenames=document.getElementById("imageFile").files[0].name; 
           		
   				$("#hiddenpdfid").val(filenames);

       		}
       	});  
       	
       	$(".editJson").click(function(){
   		 		var tag=$(this).attr("tag");
   		 		var jsonObj=JSON.parse(tag);
   				$("#courseId").val(jsonObj.courseId);
   				$("#courseName").val(jsonObj.courseName);
   				$("#eBookID").val(jsonObj.eBookID);
   				$("#eBookName").val(jsonObj.eBookName);
   				$("#fileName").val(jsonObj.fileName);
   				$("#hiddenpdfid").val(jsonObj.fileName);
   				document.forms[0].action="EBookDetailsMaster.do?action=update";
   				document.forms[0].btn.value="Update";
   				document.forms[0].eBookName.focus();
   	 	});
       
    });
    </script>
</head>
<body>
  <jsp:include page="/jsp/common/masterdata_header.jsp"></jsp:include>
   
   <!-- Start Main Content -->
	 <div class="main" style="min-height:450px;">	 
	 	<h2>E-Book Master</h2>
    <div id="status" align="center"><logic:notEmpty name="status">
					<font color="blue"><bean:write name="status"></bean:write></font>
				</logic:notEmpty></div>
   <html:form action="/EBookDetailsMaster.do?action=add" enctype="multipart/form-data" style="margin-left:80px;" >
    <input type="hidden" name="eBookID"	id="eBookID" />
    <input type="hidden" name="fileName"	 />
    <table border="0" width="100%" align="center"  class="role">
		<tr height="50">
			
			<td><label>Course *</label></td>
			<td><html:select property="courseId" styleId="courseId" >
				<html:option value="-1">-Select-</html:option>
				<html:options collection="courseList" property="courseId" labelProperty="courseName" />
			</html:select></td>
			
			
			<td><label>eBook Name *</label></td>
			<td><input type="text" name="eBookName"  id="eBookName"   maxlength=50/></td>
			
			
			
	   </tr>
	
		<tr height="50">
		<td><label>Upload PDF E-Book * </label></td>
			<td><input type="file" name="imageFile"  id="imageFile"   maxlength="750" /><input type="text" name="hiddenpdffile" id="hiddenpdfid">			 
			</td>
			<td  align="center" >
			<input type="submit" name="btn" value="Submit" onclick="return valueCall();" style="padding:0.5% 1%;" />
			<input type="reset"	value="Reset" style="padding:0.5% 1%;" id="resetBtn"/> 
		</tr>

	</table>
     </html:form> 	
		
 <div class="dataGridDisplay"  >
    <table id="normalTable" class="display" cellspacing="0" width="100%" align="center" >
	<thead>
    <tr>
    <th>SI No.</th>
     <th>Course Name</th>
    <th>eBook Name </th>
    <th>File Name</th>
   	<th>Status</th>
    <th style="width:250px;" >Action</th>
    </tr>
    </thead>
    <tbody>
    <logic:notEmpty name="eBookList">
    <logic:iterate id="eBookName" name="eBookList" indexId="index">
    <tr>
    <td><%=index+1 %></td>
    <td><bean:write name="eBookName"	property="courseName" /></td>
    <td><bean:write name="eBookName"	property="eBookName" /></td>
    <td><a href="<%=request.getContextPath() %>/ebookPDF/<bean:write name="eBookName"	property="fileName" />"><bean:write name="eBookName"	property="fileName" /></a> </td>
	 
	
	<td><logic:equal name="eBookName" property="active"	value="true">
			<font color="green">Active</font>
		</logic:equal> 
		<logic:equal name="eBookName" property="active" value="false">
			<font color="red">InActive</font>
		</logic:equal>
</td>

    <td>
    
    <logic:equal name="eBookName" property="active"	value="true">
			<a href='EBookDetailsMaster.do?action=changestatus&amp;eBookID=<bean:write name="eBookName" property="eBookID"/>&amp;active=false'><input type="button" value="Remove"></a>
			&nbsp;&nbsp;
			<a href="#" tag="<bean:write name="eBookName" property="jEdit"/>" class="editJson"><input type="button" value="Edit"></a>
			<a href="<%=request.getContextPath() %>/ebookPDF/<bean:write name="eBookName"	property="fileName" />"><input type="button" value="View"></a> 
	</logic:equal> 
	<logic:equal name="eBookName" property="active" value="false">
			<a href='EBookDetailsMaster.do?action=changestatus&amp;eBookID=<bean:write name="eBookName" property="eBookID"/>&amp;active=true'><input type="button" value="Active"></a>
			<a href="<%=request.getContextPath() %>/ebookPDF/<bean:write name="eBookName"	property="fileName" />"><input type="button" value="View"></a> 
	</logic:equal>
   
    
</td>
    </tr>
   
 </logic:iterate>
  </logic:notEmpty>
  </tbody>
   
    </table>
        <logic:notEmpty name="eBookList">
   <div align="center"><a href="EBookDetailsMaster.do?action=excelReport"><button>Excel Export</button></a></div>
    </logic:notEmpty>
	 </div>
     </div>
    
   <!-- End Main Content -->
	   
 <jsp:include page="/jsp/common/masterdata_footer.jsp"></jsp:include> 
  <script type="text/javascript">
	document.forms[0].courseId.focus();
	</script>
	<script src="js/commonFunction.js"  type="text/javascript"></script>
  </body>
</html>




    	
    	
            