<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-nested.tld" prefix="nested"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.cete.dynamicpdf.text.r"%><html>
<head>
<%
response.setHeader("Cache-Control", "no-cache");

//Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control", "no-store");

//Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0);

//Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma", "no-cache");
//HTTP 1.0 backward enter code here


%>
<title>Itech Training :: Course Master</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300'	rel='stylesheet' type='text/css'>
<link href="<%=request.getContextPath()%>/css/bootstrap.css"	rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath()%>/css/cmsstyle.css"	rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath()%>/css/font-awesome.css"	rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath()%>/css/carousel.css"	rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath()%>/css/owl.carousel.css"	rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath()%>/css/cmsstyle.css"	rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-2.1.1.min.js"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap.js"	type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"	type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/owl.carousel.js"	type="text/javascript"></script>
 

<script language="javaScript" type="text/javascript"	src="<%=request.getContextPath()%>/js/calendar.js"></script>
<link href="<%=request.getContextPath()%>/css/calendar.css"	rel="stylesheet" type="text/css">

<link rel="icon" type="image/png"	href="<%=request.getContextPath()%>/images/logo.png" />

 
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="UI_style.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath()%>/css/stylesheet_tms.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath()%>/css/search_data.css"	rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath()%>/css/style1.css"	rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=request.getContextPath()%>/date/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css"	href="<%=request.getContextPath()%>/date/jquery-ui.css" />

<link href="dataTable/css/jquery.dataTables_themeroller.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.min.css"	rel="stylesheet" type="text/css" media="all" />
<script src="dataTable/js/jquery.dataTables.js"	type="text/javascript"></script>
<script src="dataTable/js/jquery.dataTables.min.js"	type="text/javascript"></script>
<script src="dataTable/js/settingDataTable.js" 	type="text/javascript"></script>

<link rel="stylesheet" type="text/css"	href="/E-Learning/date/jquery-ui.css" />
<script type="text/javascript" src="/E-Learning/date/jquery-ui.js"></script>

<script type="text/javascript">



$(document).ready(function(){
	$("#coursecategoryId").change(function(event,courseofferedId){

		var coursecatId=$(this).val();
		$.ajax({	
			type: "GET",		
			url:"CourseMaster.do?action=getcourse",
			data:{catId:coursecatId},
			async:false,
			success: function(r){	
				var json= JSON.parse(r);
				$("#courseofferedId").empty();
				var firstOption= $("<option>",{text:"----Select----",value:"-1"});
				$("#courseofferedId").append(firstOption);
				$.each(json,function(i,obj){
					
					var option= $("<option>",{text:obj.CourseofferedName,value:obj.CourseofferedName});
					$("#courseofferedId").append(option);
				});
			} 
		});
		if(courseofferedId!=undefined){
			//alert('model name'+model);
		$("#courseofferedId").val(courseofferedId);
	}
	});
			});

</script>


<script type="text/javascript">
	function validFNameonkey() {
		re = /[^a-z,A-Z,0-9 ]/;
		var str = document.forms[0].courseName.value.replace(re, "");
		document.forms[0].courseName.value = str;

	}
	function validCourseNames() {
		re = /[^a-z,A-Z,+#& ]/;
		var str = document.forms[0].courseName.value.replace(re, "");
		document.forms[0].courseName.value = str;

	}
	function validate() {

		if(document.forms[0].coursecategoryId.value =="" && document.forms[0].coursecategoryId.value ==null){

			alert("Course Name cannot be empty");
			document.forms[0].courseName.focus();
			return false;
		} else if (document.forms[0].fees.value.trim() == "" && document.forms[0].fees.value.trim() == 0) {
			alert("Fees cannot be empty");
			document.forms[0].fees.focus();
			return false;
		} else if (document.forms[0].maxStd.value.trim() == "") {
			alert("Maximum Students cannot be empty");
			document.forms[0].maxStd.focus();
			return false;
		} else if (document.forms[0].duration.value.trim() == "") {
			alert("Course  Duration cannot be empty");
			document.forms[0].duration.focus();
			return false;
		} else if (document.forms[0].passpercent.value.trim() == "") {
			alert("Pass Percentage cannot be empty");
			document.forms[0].passpercent.focus();
			return false;
		} else if (document.forms[0].totalDiscountAmount.value.trim() == "") {
			alert("Discount Percentage cannot be empty");
			document.forms[0].totalDiscountAmount.focus();
			return false;
		} else if (document.forms[0].discountValidUpTo.value.trim() == "") {
			alert("Discount Valid UpTo cannot be empty");
			document.forms[0].discountValidUpTo.focus();
			return false;
		}else if (document.forms[0].fees.value.trim() == 0) {
			alert("Fees amount should be more than 0");
			document.forms[0].fees.focus();
			return false;
		}else if (document.forms[0].duration.value.trim() == 0) {
			alert("Course duration should not be zero days");
			document.forms[0].duration.focus();
			return false;
		}else if (document.forms[0].passpercent.value.trim() == 0) {
			alert("Pass Percentage should be greater than 0");
			document.forms[0].passpercent.focus();
			return false;
		}else if (document.forms[0].passpercent.value>=100) {
			alert("Pass Percentage should not be greater than 100");
			document.forms[0].passpercent.focus();
			return false;
		}else 
		if(document.forms[0].totalDiscountAmount.value>=100) {
			alert("Discount percentage should not be greater than 100");
			document.forms[0].totalDiscountAmount.focus();
			return false;
		}else 			
		if (document.forms[0].maxStd.value.trim() == 0) {
			alert("Atleast one student must be there");
			document.forms[0].maxStd.focus();
			return false;
		}else if (document.forms[0].duration.value.trim() == 0) {
			alert("Course duration must not be zero days");
			document.forms[0].duration.focus();
			return false;
		}
		else
			if (document.forms[0].level.value =="")
		    {
		 	   document.forms[0].level.value="";
		 	   document.forms[0].level.focus(); 
		       alert("Please Select level");
		       return false;   
		    }





		else
			if (document.forms[0].instructorname.value =="")
		    {
		 	   document.forms[0].instructorname.value="";
		 	   document.forms[0].instructorname.focus(); 
		       alert("Please Enter Instructor Name");
		       return false;   
		    }

			else
			if (!$('#coursespan').text().length) {
			  alert("Please Select the course image");
		       return false;   
		}
			
	
	}

	function exclValidate(){
		if(document.forms[1].excelFile.value == ""){
			alert("Please Select  xl or xls extension file");
			
			return false;
		}
	}
	
	function checknumber(inputtxt) {
		//alert("arg");
		re = /\D/g; // remove any characters that are not numbers
		socnum = inputtxt.value.replace(re, "");
		inputtxt.value = socnum;
	}
	function checkchacter(inputtxt) {
		//alert("arg");
			re = /[^a-z,A-Z, ]/; // remove any characters that are not numbers
		socnum = inputtxt.value.replace(re, "");//this is used for only number
		inputtxt.value = socnum;
	}
	
	function resetit() {
		document.forms[0].action = "CourseMaster.do";
		document.forms[0].submit();
	}
	
	function edit(courseId,courseName,fees,maxStd,duration,passpercent,discountValidUpTo,totalDiscountAmount,coursecategoryId,instructorname,availableseats,coursestartdate,level,courseimage,insimage,elearning,free) {
		document.getElementById('status').innerHTML="";
		document.getElementById("elearnid1").checked =false;
		document.getElementById("elearnid").checked = false;
		alert(elearning);
		if(elearning=='elearn')
		{
		document.getElementById("elearnid").checked = true;


		$("#hiddenelearningid").val(1);
		}


		if(elearning=='classroom')
		{
		document.getElementById("elearnid1").checked = true;
		$("#hiddenclassid").val(1);
		
		}


		
		$("#coursecategoryId").val(coursecategoryId);
		 $("#coursecategoryId").trigger( "change", [courseName] );
		document.forms[0].courseId.value=courseId;
		document.forms[0].level.value=level;
		
		document.forms[0].fees.value=fees;
		document.forms[0].maxStd.value=maxStd;
		document.forms[0].duration.value=duration;
		document.forms[0].passpercent.value=passpercent;
		document.forms[0].totalDiscountAmount.value=totalDiscountAmount;
		document.forms[0].discountValidUpTo.value=discountValidUpTo;
		document.forms[0].instructorname.value=instructorname;
		document.forms[0].availableseats.value=availableseats;
		document.forms[0].coursestartdate.value=coursestartdate;
		$("#coursespan").text(courseimage);
		$("#insspan").text(insimage);

		$("#hiddencoursename").val(courseimage);
		$("#hiddeninstructorname").val(insimage);
		
		

		

        $("#btn").val('update');
		$("#formId").attr('action','CourseMaster.do?action=updateCourse');
		
		
		
		//document.forms[0].action="CourseMaster.do?action=updateCourse";
		//document.forms[0].btn.value="Update";
		
		document.forms[0].courseName.focus();
		
	}

	function onload(){
		document.forms[0].coursecategoryId.focus();
		document.forms[0].coursecategoryId.value()==-1;
	}


	function fetch(){
		var f=document.forms[0];
		if(f.coursecategoryId.value!=-1){
			f.action="CourseMaster.do?action=list";
			f.submit();
		}
	}

	 
</script>
<script type="text/javascript">
$(document).ready(function(){
	$('#csdate').attr('readonly', true);
	$( "#csdate" ).datepicker({ dateFormat: 'dd-mm-yy' });
	$("#free").click(function()
			{
	if($("#free").prop('checked') == true){

			$("#fees").hide();
			$("#labelfees").hide();
	$("#fees").val("null");
		
			
		}
		else
		{
			$("#fees").val("");
			$("#labelfees").show();
			$("#fees").show();

		}
		
				
			});

$("#courseimeageid").change(function()
		{



var fileExtension = ['jpeg', 'jpg'];
if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
    // alert("Only '.jpeg','.jpg' formats are allowed.");
    alert("Only '.jpeg','.jpg' formats are allowed.");
    $("#coursespan").text("");
}
else
{
$("#coursespan").text($(this).val());
}
		});



$("#instructorimageid").change(function()
		{



var fileExtension = ['jpeg', 'jpg'];
if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
    // alert("Only '.jpeg','.jpg' formats are allowed.");
    alert("Only '.jpeg','.jpg' formats are allowed.");
    $("#coursespan").text("");
}
else
{
$("#insspan").text($(this).val());
}
		});






	});
$(document).ready(function()
		{
$("#resetid").click(function()
		{


$.ajax({
	 type: "GET",
    url: "studentRegistration.do?action=clear",
    success : function (response){
    	$("#coursespan").val("");
    	$("#insspan").text("");
    	$("#coursecategoryId").val("");
    	$("#courseofferedId").val("");

}
  });

});
});

</script>
<!-- if(document.getElementById("elearnid").checked) {
    document.getElementById('hiddenid').disabled = true;
}

if(document.getElementById("elearnid1").checked) {
    document.getElementById('classroom').disabled = true;
} -->
<script type="text/javascript">



if(document.getElementById("free").checked) {
    document.getElementById('freehiddenid').disabled = true;
}





</script>


 <script type="text/javascript">

function selectOnlyThis(id){
	  var myCheckbox = document.getElementsByName("elearnings");
	  Array.prototype.forEach.call(myCheckbox,function(el){
	    el.checked = false;
	  });
	  id.checked = true;
	}
</script>


<script>
$(document).ready(function()
		{
	
$("#elearnid").click(function()
		{
	$("#hiddenelearningid").val(1);
	$("#hiddenclassid").val(0);
	
});

$(".elearnid1").click(function()
		{
	$("#hiddenelearningid").val(0);
	$("#hiddenclassid").val(1);
	
		});

$( "#avilseats" ).keyup(function() {
	
	 var id1 = $('#maximstud').val(); //if #id1 is input element change from .text() to .val() 

	
	    var id2 = $('#avilseats').val(); //if #id2 is input element change from .text() to .val()
	   //50>40
	    if (parseInt(id1) > parseInt(id2)) {
	    //    alert('Error,');
	    	$("#avilablesetspan").text("");
	    }
	    //50=50
	    else if(parseInt(id1)==parseInt(id2))
	    {
	        
	      //  alert('Error, cant do that');
	    	$("#avilablesetspan").text(""); 
	    }

	    else if(id2=="")
	    {
	      	$("#avilablesetspan").text(""); 
		    
	    }
	    //50<60
	    else 
	    {
	//	var alerts=  alert("less"); 
		 $("#avilablesetspan").text("Avilable seats should not be grater than max students");  
		 $("#avilablesetspan").css('color', 'red');
	    }
	    
	});
});



</script>

<style type="text/css">
input[type='file'] {
  color: transparent;
}
 
</style>
</head>
<body onload="onload()">
	
	<!-- Start Header -->
	
	<jsp:include page="/jsp/common/masterdata_header.jsp"></jsp:include>
	<!-- End Header -->

	<!-- Start Main Content -->

	<div class="main" style="min-height: 450px;">
		<h2 style="text-align: center;">Course Master</h2>

		<div align="center" id="status">
				<logic:notEmpty name="status">
					<font color="blue"><bean:write name="status"></bean:write></font>
				</logic:notEmpty>
			</div>
		
		<html:form action="/CourseMaster.do?action=addCourse"  enctype="multipart/form-data" styleId="formId" styleClass="formId">
		<input type="hidden" name="courseId" />
			

			<logic:present name="duplicateUser">
				<center>
					<font color="red"><bean:write name="duplicateUser" /></font>
				</center>
			</logic:present>
			<table border="0" width="90%" align="center" class="role">
				
				<tr height="40">
				
				<td colspan="2" >Select Course Category : 
						<html:select property="coursecategoryId" styleId="coursecategoryId" styleClass="select1" >
						<html:option value="">--Select Course Category--</html:option>
						<html:options collection="coursecatList"  property="coursecategoryId"  labelProperty="coursecategoryname" />
					</html:select></td>
					
					
						<td colspan="2" >Select Course offered : 
						<select name="courseofferedname" id="courseofferedId" class="select1">
						</select>
						
						
						</td>
					
				
					
				</tr>
				<tr height="60">
					<td colspan="2"><label>Maximum Students:*</label>
					<input type="text" name="maxStd"
						onkeyup="return checknumber(this);" maxlength="5" id="maximstud"></td>
					<td colspan="2"><label>Course Duration(In
							Days):*</label>
					<input type="text" name="duration"
						onkeyup="return checknumber(this);" maxlength="3"></td>

				</tr>

				<tr height="60">
					<td colspan="2"><label>Pass Percentage:*</label>
					<input type="text" name="passpercent"
						onkeyup="return checknumber(this);" maxlength="2"></td>
						<td colspan="2"> <label>Discount Percentage:*</label>
					<input type="text" name="totalDiscountAmount"
						onkeyup="return checknumber(this);" maxlength="2"></td>
				</tr>
				
				<tr height="60">
					<td colspan="2"><label>Discount Valid UpTo:*</label>
					<input type="text" name="discountValidUpTo"	  id="discountValidUpTo" class="calendar" readonly="readonly" size="20"></td>
						 
					<td colspan="2"><label>if free course </label>
					<input type="checkbox" name="free" id="free" value="1">
					<input type="hidden" id="freehiddenid" name="free" value="0">
					
					
					</td>
				 
					
					
					
					<td colspan="2"><label id="labelfees">Fees (In Rupees):* </label>
					<input type="text" name="fees" id="fees"
						onkeyup="return checknumber(this);" maxlength="5"></td>
				 
				</tr>
				
				
						<tr height="60">
					<td colspan="2"><label>Upload Course Image:*</label>
      <input  name="imageFile" type="file" id="courseimeageid"></input><span id="coursespan"></span>
      <input type="hidden" name="hiddencoursename" id="hiddencoursename">	</td>					 
      
      
</td>					 
					 
					 
					 <td colspan="2"><label>Select Course Type:*</label>
					 		<!-- <input type="checkbox"  class="subject-list" name="elearning" id="elearnid" value="0"  onclick="call(0,this)" >E-Learning<br>
			<input type="hidden" id="hiddenid" name="elearning" value="0">
					 		 -->			 				  
				<input type="radio" name="elearnings" id="elearnid" value="0" onclick="selectOnlyThis(this)" />E-Learning<br>
<input type="radio" name="elearnings" id="elearnid1" class="elearnid1" value="0" onclick="selectOnlyThis(this)"/>Class room Training
			
			<input type="hidden" name="classroom" id="hiddenclassid">	
						<input type="hidden" name="elearning" id="hiddenelearningid">		
				
					<!-- 
<input type="checkbox" id="elearnid1"  class="subject-list" name="classroom" value="1"  onclick="call(1,this)">Class room Training
 	<input type="hidden" id="classroom" name="classroom" value="0"> 
 	-->   </td>
					 
					 

					 
					  <td colspan="2"><label>Select Level Type:*</label>
					  <select name="level" id="level">
					  <option value="">select levels</option>
					  <option value="Beginner">Beginner</option>
					  <option value="Intermediate">Intermediate</option>
					  	<option value="Advanced">Advanced</option>
					  
					  
					  </select>
					
					 </td>
					 
					 
					 
					 
				</tr>
				<tr height="60">
					<td colspan="2"><label>Instructor Image:</label>
<input type="file" name="instructorimage" id="instructorimageid" > <span id="insspan"></span><input type="hidden" name="hiddeninstructorname" id="hiddeninstructorname">	</td>					 
					 
					 
					 <td colspan="2"><label>Instructor Name:</label>
					<input type="text" name="instructorname" 
						 maxlength="5" onkeyup="return checkchacter(this);" >
					 </td>
					 
					 
					  <td colspan="2"><label>Available seats:</label>
					  <input type="text" name="availableseats" 
						onkeyup="return checknumber(this);" maxlength="5" id="avilseats">
						
						<span id="avilablesetspan" ></span>
						
						</td>
					 
					 
					 
					 
				</tr>
								<tr height="60">
					<td colspan="2"><label>Course Starting date:</label>
<input type="text" name="coursestartdate" id="csdate"> 	</td>
</tr>
				
				
				<logic:present name="userexist">
					<tr>
						<td colspan="4" align="left"><font color="red"><bean:write	name="userexist" /></font></td>
					</tr>
				</logic:present>

				<tr height="30">
					<td colspan="4" align="center"><input type="submit" id="btn" value="Save"  onclick="return validate()"
						style="padding: 0.5% 0%;" /> <input type="reset" value="Reset" id="resetid"
						style="padding: 0.5% 0%;" /></td>
				</tr>




			</table>

		</html:form>
		
			<html:form action="CourseMaster.do?action=excelUpload" enctype="multipart/form-data" onsubmit="return exclValidate();">
		
		<table border="0" width="90%" align="center" class="role">
		<tr height="40">
            <td colspan=   align="center">Excel Upload</td>
       				 <td><input type="file" name="excelFile" id="excelFile" /></td>
       				  <td colspan=   align="left" ><input type="submit" value="Submit"></input></td>
       				  
       				       <td><a class="submit_btn" style="float:left" href="<%=request.getContextPath() %>/studentRegistration.do?action=downloadexcel&amp;filename=coursemaster.xls"/>Download Excel File Format</a>
       				  
       				  
       				  
       				  
       				  
       				  
       				  
       				  
		</tr>
		</table>
		</html:form>
		
		
		 	<div class="dataGridDisplay" >
			<table id="normalTable" class="display" cellspacing="0" width="100%"     >
				<thead>
		 
				<tr>
					<th>SI No.</th>
					<th>Course Name</th>
					<th>Fees</th>
					<!-- <th>Maximum Students</th> -->
					<!--  <th>Duration</th> -->
					<th>Pass Percentage</th>
					<th>Discount  Percentage</th>
					<th>Discount Valid UpTo</th>
					<th>Status</th>
					<th style="width:155px;" >Action</th>
				</tr>
</thead>
<tbody>
<logic:notEmpty name="courseList">
				<logic:iterate id="course" name="courseList" indexId="index">
					<tr>
						<td><%=index + 1%></td>
						<td><bean:write name="course" property="courseName" /></td>
						<td><bean:write name="course" property="fees" /></td>
						<!--<td><bean:write name="course" property="maxStd" /></td> -->
						<!--<td><bean:write name="course" property="duration" /></td> -->
						<td><bean:write name="course" property="passpercent" /></td>
						<td><bean:write name="course" property="totalDiscountAmount" /></td>
						<td><bean:write name="course" property="discountValidUpToDisplay" /></td>
						<td><logic:equal name="course" property="active" value="true">
								<font color="green">Active</font>
							</logic:equal> <logic:equal name="course" property="active" value="false">
								<font color="red">InActive</font>
							</logic:equal></td>
						<td><logic:equal name="course" property="active" value="true">
								<a
									href='CourseMaster.do?action=changestatus&amp;courseid=<bean:write name="course" property="courseId"/>&amp;active=false'><input
									type="button" value="Remove"></a>
			&nbsp;&nbsp;
			<a href="#" id="Save" onclick='edit("<bean:write name="course" property="courseId" />",
									  "<bean:write name="course" property="courseName" />",
									  "<bean:write name="course" property="fees" />",
									  "<bean:write name="course" property="maxStd" />",
									  "<bean:write name="course" property="duration" />",
									  "<bean:write name="course" property="passpercent" />",
									  "<bean:write name="course" property="discountValidUpTo" />",
									  "<bean:write name="course" property="totalDiscountAmount" />",
									   "<bean:write name="course" property="coursecategoryId" />",
									    "<bean:write name="course" property="instructorname" />",
									     "<bean:write name="course" property="availableseats" />",
									     	"<bean:write name="course" property="coursestartdate" />",
									     		"<bean:write name="course" property="level" />",
									     			"<bean:write name="course" property="courseimage" />",
									     			"<bean:write name="course" property="instructorimages" />",
									     	"<bean:write name="course" property="elearning" />",
									     		"<bean:write name="course" property="free" />"
									     			
									     			
									     		
									     	
									     
									  )' ><input	type="button" value="Edit"></a>
							</logic:equal> <logic:equal name="course" property="active" value="false">
								<a
									href='CourseMaster.do?action=changestatus&amp;courseid=<bean:write name="course" property="courseId"/>&amp;active=true'><input
									type="button" value="Active"></a>
							</logic:equal></td>


					</tr>
				</logic:iterate>
				 </logic:notEmpty>
				 </tbody>
				 
   
			</table>
			<logic:notEmpty name="courseList">
			 <div align="center"><a href="CourseMaster.do?action=excelReport"><button>Excel Export</button></a></div>
			</logic:notEmpty>
</div>

		 
		 
	</div>

	<!-- End Main Content -->

	<!-- Start Footer -->
	<jsp:include page="/jsp/common/masterdata_footer.jsp"></jsp:include>
	<!-- End Footer -->
</body>
 
<script type="text/javascript">
$(document).ready(function() {
	
	$(function(){
	    $( "#discountValidUpTo" ).datepicker({minDate:1,dateFormat: 'dd/mm/yy'});
		 
	});
	$('#excelFile').change(function () {
		var val = $(this).val().toLowerCase();
		//alert(val);
		var regex = new RegExp("(.*?)\.(xls|xlsx)$");
		if(!(regex.test(val))) {
		 	$(this).val('');
		 	alert('Please select .xls .xl format only');
		 } 
	}); 
});
</script>

</html>




