<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-nested.tld" prefix="nested"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Itech Training :: Sample Program Master</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"	href="<%=request.getContextPath() %>/htmlEditor/tinyeditor.css">
<script	src="<%=request.getContextPath() %>/htmlEditor/tiny.editor.packed.js"></script>
<link rel="icon" type="image/png" href="<%=request.getContextPath()%>/images/logo.png"></link>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="UI_style.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath()%>/css/stylesheet_tms.css"	rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath()%>/css/search_data.css"	rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath()%>/css/style1.css"	rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath()%>/css/bootstrap.css"	rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath()%>/css/cmsstyle.css"	rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath()%>/css/font-awesome.css"	rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>


<link href="dataTable/css/jquery.dataTables_themeroller.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.min.css"	rel="stylesheet" type="text/css" media="all" />
<script src="dataTable/js/jquery.dataTables.js"	type="text/javascript"></script>
<script src="dataTable/js/jquery.dataTables.min.js"	type="text/javascript"></script>
<script src="dataTable/js/settingDataTable.js" 	type="text/javascript"></script>


<script type="text/javascript" src="js/VideoAcceptOnly.js"></script>
<!-- Text Editor -->
<script type="text/javascript">
	function valueCall(){
		if(document.forms[0].courseId.value == -1 ){
			alert("Please Select the Course Name ..!");
			document.forms[0].courseId.focus();
			return false;
		}
		if(document.forms[0].topicId.value == -1 )	{
			alert("Please Select the Topic Name ..!");
			document.forms[0].topicId.focus();
			return false;
		}
		if(document.forms[0].subTopicId.value == "-1") {
			alert("Please Select the Sub Topic Name ..!");
			document.forms[0].subTopicId.focus();
			return false;
		}
		
		
		else{
			document.forms[0].sampleProgramContent.value = window.frames[0].document.body.innerHTML;
		}
	}
	
	function edit(sampleProgramID){
		$.ajax({	
    		type: "GET",		
    		url:"SampleProgram.do?action=edit",
    		data:{sampleProgramID:sampleProgramID},
    		success: function(r){	
        		 
    			var json= JSON.parse(r);
    			var courseId=json.courseId;
    			var topicId=json.topicId;
        		var subTopicId=json.subTopicId;
				var sampleProgramID=json.sampleProgramID;
				var name =json.name;
				var sampleProgramContent=json.sampleProgramContent;
        		
				getTopic(courseId,topicId);
    			getSubTopic(topicId,subTopicId);
				
				$("#cid").val(courseId);
    		 	$("#sampleProgramID").val(sampleProgramID);
				$("#name").val(name);
				//$("#sampleProgramContent").val(sampleProgramContent);
    			 
				window.frames[0].document.body.innerHTML=sampleProgramContent;
    			$('#spForm').attr('action', 'SampleProgram.do?action=update');
    			$('#btn').attr('value', 'Update');
    		} 
    	});
	}	
	

	/*function edit(sampleProgramID,name,sampleProgramContent,courseId,topicId,subTopicId){
		
		document.forms[0].sampleProgramID.value =sampleProgramID;
		document.forms[0].name.value =name;
		document.forms[0].sampleProgramContent.value =sampleProgramContent;
		
		getTopic(courseId,topicId);
		getSubTopic(topicId,subTopicId);
		
		document.forms[0].courseId.value =courseId;
		document.forms[0].action="c.do?action=update";
		document.forms[0].btn.value="Update";
		document.forms[0].courseId.focus();
	}*/

	 

	function getTopic(courseId,selectedTopicId){
		$.ajax({	
    		type: "GET",		
    		url:"ContantMaster.do?action=getTopic",
    		data:{courseId:courseId},
    		success: function(r){	
    			var json= JSON.parse(r);
    			console.log(json);			
    			$("#tid").empty();
    			var firstOption= $("<option>",{text:"--Select Topic--",value:"-1"});
    			$("#tid").append(firstOption);
    			$.each(json,function(i,obj){
    				var option= $("<option>",{text:obj.topicName, value:obj.tid});
    				$("#tid").append(option);
    			});
    			if(selectedTopicId!="" && selectedTopicId!=null){
    				$("#tid").val(selectedTopicId);
        		}
    		} 
    		
    	});
    	
	}	

	
	 

	function getSubTopic(topicId,selectedSubTopicId){
		$.ajax({	
    		type: "GET",		
    		url:"ContantMaster.do?action=getSubTopic",
    		data:{topicId:topicId},
    		success: function(r){	
    			var json= JSON.parse(r);
    			console.log(json);			
    			$("#sid").empty();
    			var firstOption= $("<option>",{text:"--Select SubTopic--",value:"-1"});
    			$("#sid").append(firstOption);
    			$.each(json,function(i,obj){
    				var option= $("<option>",{text:obj.subTopicName, value:obj.sid});
    				$("#sid").append(option);
    			});
    			if(selectedSubTopicId!="" && selectedSubTopicId!=null){
    				$("#sid").val(selectedSubTopicId);
        		}
    		} 
    	});
    	
	}	
	
</script>
<script type="text/javascript">

function exclValidate(){
	if(document.forms[1].fileName.value == ""){
		alert("Please Select  xl or xls extension file");
		
		return false;
	}
}
</script>
<script>
 $(document).ready(function() {
	 $("#cid").val("-1");
	 
	 $("#cid").change(function(){
	    	var cid=$(this).val();
	    	getTopic(cid);
	    	
	 });	

	 $("#tid").change(function(){
	    	var tid=$(this).val(); 
	    	getSubTopic(tid);
	    	
	 });	

	 $("#resetBtn").click(function(){
			 
		$.ajax({
			 type: "GET",
		     url: "studentRegistration.do?action=clear",
		     success : function (response){
		    	 window.frames[0].document.body.innerHTML="";
		 		
		 		$("#cid").val("-1");
		 		$("#tid").val("-1");
		 		$("#sid").val("-1");

}
		   });
	


		
	});

 });
 



 
</script>




<style>
.header-bottom {
	background: none repeat scroll 0 0 #e58b30;
	color: #fff;
	float: left;
	padding: 1% 0;
	width: 100%;
}
</style>
 
</head>
<body>
	<jsp:include page="/jsp/common/masterdata_header.jsp"></jsp:include>

	<!-- Start Main Content -->
	<div class="main" style="min-height: 450px;">
		<h2>Sample Program Master</h2>
		<div id="status" align="center" style="margin-top: 20px;">
			<logic:notEmpty name="status">
				<font color="blue"><bean:write name="status"></bean:write></font>
			</logic:notEmpty>
		</div>
		<html:form action="SampleProgram.do?action=add" styleId="spForm">
			<input type="hidden" name="sampleProgramID" id="sampleProgramID" />
			<table border="0" width="85%" align="center" class="role">
				 
			<tr height="50" >
			<td  width="18%">Course *</td>
			<td><html:select property="courseId" styleId="cid">
				<html:option value="-1">-Select-</html:option>
				<html:options collection="courseList" property="courseId" labelProperty="courseName" />
			</html:select></td>
			
			<td >Topic list *</td>
			<td><select  name="topicId" id="tid">
				<option value="-1">-Select-</option>
				</select>
			</td>
			</tr>
			<tr height="50" >
			<td >Sub Topic Name* </td>
			<td><select name="subTopicId" id="sid">
				<option value="-1">-Select-</option>
				</select>
			</td>
		
			<td><label>Sample Program Name </label></td>
			<td><input type="text"  name="name" id="name" maxlength="1000"  onkeyup="return checkchacter(this);"/></td>
			</tr>
			
			<tr height="50">	 
			 
			<td colspan="4" align="center" ><label> Sample program content* </label></td>
			</tr>
			 
			<tr height="50" valign="top" >	 
			
			<td colspan="4" align="center">
			
			<input type="hidden" name="sampleProgramContent" value="">
		<textarea id="tinyeditor" style="width:auto; height: 200px"></textarea>
		<script>
			var editor = new TINY.editor.edit('editor', {
				id: 'tinyeditor',
				width: 584,
				height: 175,
				cssclass: 'tinyeditor',
				controlclass: 'tinyeditor-control',
				rowclass: 'tinyeditor-header',
				dividerclass: 'tinyeditor-divider',
				controls: ['bold', 'italic', 'underline', 'strikethrough', '|', 'subscript', 'superscript', '|',
						   'orderedlist', 'unorderedlist', '|', 'outdent', 'indent', '|', 'leftalign',
						   'centeralign', 'rightalign', 'blockjustify', '|', 'unformat', '|', 'undo', 'redo', 'n',
					   	   'font', 'size', 'style', '|', 'image', 'hr', 'link', 'unlink', '|', 'print'],
				footer: true,
				fonts: ['Verdana','Arial','Georgia','Trebuchet MS'],
				xhtml: true,
				cssfile: 'custom.css',
				bodyid: 'editor',
				footerclass: 'tinyeditor-footer',
				toggle: {text: 'source', activetext: 'Design', cssclass: 'toggle'},
				resize: {cssclass: 'resize'}
			});
</script>
			<script type="text/javascript">
    function checkchacter(inputtxt) {
		//alert("arg");
			re = /[^a-z,A-Z,0-9 ]/; // remove any characters that are not numbers
		socnum = inputtxt.value.replace(re, "");
		inputtxt.value = socnum;
	}
    </script>
			
			</td>
				   
			 </tr>

				<tr height="50">
					<td colspan="6" align="center">
					    <input type="submit"		id="btn" value="Submit" onclick="return valueCall();"	style="padding: 0.5% 1%;margin-top: 10px;" /> 
					    <input type="reset" value="Reset"	style="padding: 0.5% 1%;" id="resetBtn" /></td>
				</tr>

			</table>
		</html:form>
		
		<center>
		
		<html:form action="SampleProgram.do?action=sampleExcelUpload" styleId="spForm" enctype="multipart/form-data" onsubmit="return exclValidate();" >
		
		<tr><td>	
		Excel Upload:<input  type="file" name="fileName" id="fileNameid"  onchange="PreviewImage(9);" style="width: 300px;" />
		</td><td> <input type="submit"  value="Submit" id="submitid"  style="padding: 0.5% 1%;margin-top: 10px;"/></td>
		<td><a class="submit_btn" style="float:left" href="<%=request.getContextPath() %>/studentRegistration.do?action=downloadexcel&amp;filename=sampleprogrammaster.xls"/>Download Excel File Format</a>
		</td>
		
		</tr>
		
		<tr>
		
		</html:form>
		
		</center>

		<div class="dataGridDisplay"  >
			<table id="normalTable" class="display" cellspacing="0" width="100%" align="center" >
				<thead>
				
				<tr>
					<th>SI No.</th>
					<th>Sub Topic Name</th>
					<th>Sample Program Name</th>
					 
					<th>Status</th>
					<th  style="width:155px;">Action</th>
				</tr>
</thead>
<tbody><logic:notEmpty name="sampleProgramList">
				<logic:iterate id="sampleProgram" name="sampleProgramList" indexId="index">
					<tr>
						<td><%=index + 1%></td>
						<td><bean:write name="sampleProgram" property="subTopicName" /></td>
						<td><bean:write name="sampleProgram" property="name" /></td>
						 
						
						<td><logic:equal name="sampleProgram" property="active"	value="true">
								<font color="green">Active</font>
							</logic:equal> <logic:equal name="sampleProgram" property="active"	value="false">
								<font color="red">InActive</font>
							</logic:equal>
						</td>

	 
	
						
						<td><logic:equal name="sampleProgram" property="active"
								value="true">
								<a
									href='SampleProgram.do?action=changestatus&amp;sampleProgramID=<bean:write name="sampleProgram" property="sampleProgramID"/>&amp;active=false'><input
									type="button" value="Remove"></a>
			&nbsp;&nbsp;
			<a href="#"
									onclick='edit("<bean:write name="sampleProgram" property="sampleProgramID"/>")'><input
									type="button" value="Edit"></a>
							</logic:equal> <logic:equal name="sampleProgram" property="active" value="false">
								<a
									href='SampleProgram.do?action=changestatus&amp;sampleProgramID=<bean:write name="sampleProgram" property="sampleProgramID"/>&amp;active=true'><input
									type="button" value="Active"></a>
							</logic:equal></td>
					</tr>

				</logic:iterate>
			

		</logic:notEmpty>
		 
		 </tbody>
		 </table>
		 <logic:notEmpty name="sampleProgramList">
		 <div align="center"><a href="SampleProgram.do?action=excelReport"><button>Excel Export</button></a></div>
	</logic:notEmpty>
	</div>
	 	</div>
	<!-- End Main Content -->

	<jsp:include page="/jsp/common/masterdata_footer.jsp"></jsp:include>
	<script type="text/javascript">
	document.forms[0].courseId.focus();
	</script>
	<script src="js/commonFunction.js"  type="text/javascript"></script>
</body>
</html>






