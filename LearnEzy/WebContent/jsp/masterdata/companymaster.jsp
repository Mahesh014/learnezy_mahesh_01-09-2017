
<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Itech Training ::  Company Master</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300' rel='stylesheet' type='text/css'>
<link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/cmsstyle.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/carousel.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>

<script src="<%=request.getContextPath() %>/js/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/owl.carousel.js" type="text/javascript"></script>

<link href="dataTable/css/jquery.dataTables_themeroller.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.min.css"	rel="stylesheet" type="text/css" media="all" />
<script src="dataTable/js/jquery.dataTables.js"	type="text/javascript"></script>
<script src="dataTable/js/jquery.dataTables.min.js"	type="text/javascript"></script>
<script src="dataTable/js/settingDataTable.js" 	type="text/javascript"></script>

  <script type="text/javascript">

window.history.forward();
function noBack() { window.history.forward(); }

 function edit(id,desc,complogo,compsubdomain,countryid,stateid,cityid,pcompid){
	 	document.getElementById("status").innerHTML="";
		document.CompanyForm.action="Company.do?action=update";
		document.CompanyForm.companyID.value=id;
		document.CompanyForm.companyName.value=desc;
		document.CompanyForm.compsubdomain.value=compsubdomain;
		document.CompanyForm.complogo.value=complogo;
		document.CompanyForm.countryid.value=countryid;
		//document.CompanyForm.stateid.value=stateid;
		//$("#countryid").trigger("change",[stateid]);
		//$("#stateid").val("stateid");
		//document.CompanyForm.cityid.value=cityid;
		//$("#stateid").trigger("change",[cityid]);
		//$("#stateid").val("cityid");
		getState(countryid,stateid); 
		getCity(stateid,cityid);
		document.CompanyForm.pcompid.value=pcompid;
		
		document.CompanyForm.companyName.focus();
		document.CompanyForm.btn.value="Update";
	}
 function getState(countryid,selectedId){
		$.ajax({	
			type: "GET",		
			url:"State.do?action=ajaxActiveList",
			data:{countryId:countryid},
			success: function(r){	
				var json= JSON.parse(r);
				$("#stateid").empty();
				var firstOption= $("<option>",{text:"--Select State--",value:"-1"});
				var otherOption= $("<option>",{text:"Others",value:"0"});
				$("#stateid").append(firstOption);
				$.each(json,function(i,obj){
					var option= $("<option>",{text:obj.state, value:obj.stateId});
					$("#stateid").append(option);
				});
				$("#stateid").append(otherOption);
				if(selectedId!="" && selectedId!=null){
					$("#stateid").val(selectedId);
	    		}
				
			} 
		});
	}
 function getCity(stateid,selectedId){
		$.ajax({	
			type: "GET",		
			url:"City.do?action=ajaxActiveList",
			data:{stateId:stateid},
			success: function(r){	
				var json= JSON.parse(r);
				$("#cityid").empty();
				var firstOption= $("<option>",{text:"--Select City--",value:"-1"});
				var otherOption= $("<option>",{text:"Others",value:"0"});
				$("#cityid").append(firstOption);
				$.each(json,function(i,obj){
					var option= $("<option>",{text:obj.city, value:obj.cityId});
					$("#cityid").append(option);
				});
				$("#cityid").append(otherOption);
				if(selectedId!="" && selectedId!=null){
					$("#cityid").val(selectedId);
				}
			} 
		});
	}

 function focus2()
 {

 document.getElementById('status').innerHTML="";
 document.CompanyForm.companyID.value="";
 document.CompanyForm.action="Company.do?action=add";
 document.CompanyForm.btn.value="Save";
 document.CompanyForm.companyName.focus();
 document.getElementById('status').innerHTML="";
 }

 function validate(){
	   
		if(document.forms[0].companyName.value.trim()==""){
			alert("Company name connot be empty");
			document.forms[0].companyName.focus();
			return false;
		}

		if (!document.forms[0].companyName.value.match(/^[a-zA-Z ]*$/) && document.forms[0].companyName.value !="")
	    {
	 	   document.forms[0].companyName.value="";
	 	   document.forms[0].companyName.focus(); 
	       alert("Please Enter only Alphabets");
	       return false;   
	    }


		if(document.forms[0].countryid.value=="-1"){
			alert("Select Country Name");
			document.forms[0].countryid.focus();
			return false;
		}
		if(document.forms[0].stateid.value=="-1"){
			alert("Select State Name");
			document.forms[0].stateid.focus();
			return false;
		}
		if(document.forms[0].cityid.value=="-1"){
			alert("Select City Name");
			document.forms[0].cityid.focus();
			return false;
		}
	return true;
 }

 $(window).load(function(){

	  $('input[name=companyName]').focus();

	});
</script>
<script type="text/javascript">
    function checkchacter(inputtxt) {
		//alert("arg");
			re = /[^a-z,A-Z, ]/; // remove any characters that are not numbers
		socnum = inputtxt.value.replace(re, "");
		inputtxt.value = socnum;
	}
$(document).ready(function()

		{

	$('#companylogo').on("change",function () {

        var fileExtension = ['jpeg', 'jpg','png'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            // alert("Only '.jpeg','.jpg' formats are allowed.");
           
            alert("Only '.jpeg','.jpg' formats are allowed.");
            $("#companylogo").val("");
        }
        else {
            
        } 
});





		});

	
    </script>
</head>
<body>
 
 <jsp:include page="/jsp/common/masterdata_header.jsp"></jsp:include>
 
   <!-- Start Main Content -->
	 <div class="main" style="min-height:450px;">	 
	 	<h2>Company Master</h2>
    <html:form action="/Company.do?action=add" enctype="multipart/form-data" >
    <input type="hidden"	name="companyID" />
    <input type="hidden"	name="complogo" />
    <div id="status" align="center"><logic:notEmpty name="status">
					<font color="blue"><bean:write name="status"></bean:write></font>
				</logic:notEmpty></div>
     <table class="role" width="600">
     <tr>
     
     <td>Company Name *:</td>
     <td><input type="text"  name="companyName" size="25" maxlength="50"  ></td>
     
     
         <td>Company Sub Domain : </td>
     <td><input type="text" name="compsubdomain"  onkeyup="return checkchacter(this);"> </td>
     
    
 </tr>
     <tr>
     

     <td>Company Logo :</td>
     <td><input type="file" name="imageFile" id="companylogo" >  
     </td>
     
     <td>Country Name *:</td>
     <td> 
     <html:select property="countryid" styleId="countryid"  styleClass="dropdown-select" >
	<html:option value="-1">--Select Country--</html:option>
    <html:options collection="countryList" property="countryid" labelProperty="countryname" />
  
	</html:select>
      </td>
     </tr>
     
     <tr>
     <td>State Name *:</td>
     <td>
     <select name="stateid"  property="stateid" id="stateid" class="dropdown-select" >
	 <option value="-1">-Select State-</option>
	  <option value="0">-others-</option>

	</select>
     </td>
     
     <td><label>City * <font style="color: red;"></font></label></td>
					<td>	<select name="cityid" id="cityid"  class="dropdown-select">
													<option value="-1">-Select City-</option>
													
												</select>
					</td>
     </tr>
     
     <tr>
     
      <td>Parent Company Name :</td>
     <td> 
     <html:select property="pcompid" styleId="pcompid"  styleClass="dropdown-select" >
	<html:option value="-1">--Select Parent Company Name--</html:option>
    <html:options collection="ParentCompanyList" property="pcompid" labelProperty="pcompanyname" />
  
	</html:select>
      </td>
     </tr>
     
     
     <tr>
    
      <td colspan="6" style="text-align: center;">
      <input type="submit" name="btn"  id="Save" onclick="return validate();" value="Save"> &nbsp; &nbsp; 
      <input type="reset" value="Reset" onclick="focus2()"></td>
      </tr>
     </table>
     </html:form>
     
     	
<div class="dataGridDisplay"  >
   <table id="normalTable" class="display" cellspacing="0" width="100%" align="center" >
				<thead>
    <tr>
    <th>SI No.</th>
    <th>Company Name</th>
    <th>Sub domain Name</th>
    <th>Company Logo</th>
    <th style="width:100px;" >Status</th>
    <th style="width:155px;">Action</th>
    </tr>
    </thead>
    <tbody>
    <logic:notEmpty name="CompanyList">
    	<logic:iterate id="userrole" name="CompanyList" indexId="index">
    <tr>
    <td><%=index+1 %></td>
    <td><bean:write name="userrole" property="companyName" /></td>
    <td><bean:write name="userrole" property="compsubdomain"/> </td>
    <td><bean:write name="userrole" property="complogo"/> </td>
    <td>
    
    <logic:equal name="userrole"
							property="active" value="true">
							<font color="green">Active</font>
						</logic:equal> <logic:equal name="userrole" property="active" value="false">
							<font color="red">Inactive</font>
						</logic:equal>
    
    </td>
    
    <td>
    <logic:equal name="userrole"
							property="active" value="true">
							<a style="text-decoration: none;"
								href='Company.do?action=changestatus&amp;companyID=<bean:write name="userrole" property="companyID"/>&amp;active=false'> <input type="button" value="Remove"></a>
								&nbsp; <a href='#'
								onclick='edit("<bean:write name="userrole" property="companyID"/>",
								"<bean:write name="userrole" property="companyName"/>",
								"<bean:write name="userrole" property="complogo"/>",
								
								"<bean:write name="userrole" property="compsubdomain"/>",
							
								"<bean:write name="userrole" property="countryid"/>",
									"<bean:write name="userrole" property="stateid"/>",
										"<bean:write name="userrole" property="cityid"/>",
										"<bean:write name="userrole" property="pcompid"/>")'>
								<input type="button" value="Edit"></a>
						</logic:equal> <logic:equal name="userrole" property="active" value="false">
							<a style="text-decoration: none;"
								href='Company.do?action=changestatus&amp;companyID=<bean:write name="userrole" property="companyID"/>&amp;active=true'> <input type="button" value="Active"></a>
						</logic:equal>
    
</td>
    </tr>
  
    </logic:iterate>
     </logic:notEmpty>
  </tbody>
    
    </table>
     <logic:notEmpty name="CompanyList">
    <div align="center"><a href="Company.do?action=excelReport"><button>Excel Export</button></a></div>
    </logic:notEmpty>
   </div>
  </div>
     
   <!-- End Main Content -->
	   <jsp:include page="/jsp/common/masterdata_footer.jsp"></jsp:include> 
 <script src="js/commonFunction.js"  type="text/javascript"></script>
 
  <script src="js/masterdata/companymaster.js"  type="text/javascript"></script>
 
  </body>
</html>

