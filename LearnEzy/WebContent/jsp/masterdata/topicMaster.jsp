<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-nested.tld" prefix="nested"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Itech Training :: Topic Master</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="icon" type="image/png" href="<%=request.getContextPath() %>/images/logo.png"></link>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="UI_style.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath() %>/css/stylesheet_tms.css"	rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath() %>/css/search_data.css"	rel="stylesheet" type="text/css" /> 
<link href="<%=request.getContextPath() %>/css/style1.css"	rel="stylesheet" type="text/css" />

<style type="text/css" media="all">
@import "<%=request.getContextPath() %>/jsp/masterdata/css/info.css";
@import "<%=request.getContextPath() %>/jsp/masterdata/css/main.css";
@import	"<%=request.getContextPath() %>/jsp/masterdata/css/widgEditor.css";
</style>

<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.11.0.min.js"></script>
<script type="text/javascript"	src="<%=request.getContextPath() %>/jsp/masterdata/scripts/widgEditor.js"> </script>
<script type="text/javascript"	src="<%=request.getContextPath() %>/js/homecalender.js"> </script>
<script language="javascript"	src="<%=request.getContextPath()%>/js/common.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/additional-methods1.js"></script>

<!-- Text Editor -->
<script>
        $(document).ready(function() {courseId
                $('#topicform').validate( { // initialize the plugin
                                        rules : {
                	topicName : {
                                                        required : true,
                                                        lettersWithSpaceonly : true
                                                },
                                                courseName : {
                                                    required : true
                                            }
                                        },
                                        messages : {
                                        	topicName : {
                                                        required : "Please enter topicname",
                                                        lettersWithSpaceonly : "Enter only characters"
                                                },
                                                courseName : {
                                                    required : "PLEASE ENTER TITLE"
                                            }
                                        }
                                });
        });
</script>

<script type="text/javascript">

	

function resetBtn(){
	$('select option:selected').removeAttr('selected');

}

function listMetodCall(){
	document.forms[0].action="TopicMaster.do?action=list";
	document.forms[0].submit();
}

function edit(topicId,courseId,topicName){
		document.forms[0].action="TopicMaster.do?action=updateCourse";	
		document.forms[0].courseId.value = courseId;
		document.forms[0].topicId.value = topicId; 
		document.forms[0].topicName.value = topicName;		
		document.forms[0].btn.value="Update";
		document.forms[0].topicName.focus();
}


function callList(){
	document.forms[0].action="TopicMaster.do?action=list";
	document.forms[0].submit();
}

$(window).load(function(){
	  $('#courseId').focus();
	});


</script>
<script type="text/javascript">

function exclValidate(){
	if(document.forms[1].filename.value == ""){
		alert("Please Select  xl or xls extension file");
		
		return false;
	}
}
</script>


<link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/cmsstyle.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>

<link href="dataTable/css/jquery.dataTables_themeroller.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.min.css"	rel="stylesheet" type="text/css" media="all" />
<script src="dataTable/js/jquery.dataTables.js"	type="text/javascript"></script>
<script src="dataTable/js/jquery.dataTables.min.js"	type="text/javascript"></script>
<script src="dataTable/js/settingDataTable.js" 	type="text/javascript"></script>
	

<script>
    $(document).ready(function() {
       $("#resetBtn").click(function(){
         	 alert("asas");
 			$("#courseId").val("-1");
 			$("#topicId").val("-1");
 			$("#subTopicName").val("");
 			
 	 	});
       $('#excelFile').change(function () {
   		var val = $(this).val().toLowerCase();
   		var regex = new RegExp("(.*?)\.(xls|xlsx)$");
   		if(!(regex.test(val))) {
   		 	$(this).val('');
   		 	alert('Please select .xls .xl format only');
   		 } 
   		});
    });
    </script>
   
</head>
<body>

 <jsp:include page="/jsp/common/masterdata_header.jsp"></jsp:include>
   
   <!-- Start Main Content -->
	 <div class="main" style="min-height:450px;">	 
	 	<h2 style="text-align:center;">Topic Master</h2>
    
 
   <html:form action="/TopicMaster.do?action=addCourse" styleId="topicform">
    <input type="hidden" name="topicId" />
    	<div id="status" align="center"><logic:notEmpty name="status">
					<font color="blue"><bean:write name="status"></bean:write></font>
				</logic:notEmpty></div>
				
    <table border="0" width="65%" align="center"  class="role">	
		<tr height="50">			
			<td style="text-align:right;"><label>Course *</label>&nbsp;&nbsp;</td>
					<td><html:select property="courseId"  styleId="courseId" onchange="listMetodCall();">
						<html:option value="-1">-Select-</html:option>
						<html:options collection="courseList" property="courseId" labelProperty="courseName" />
					</html:select></td>
			<td style="text-align:right;"><label>Topic Name * </label>&nbsp;&nbsp;</td>
			
			<td><input type="text" name="topicName" maxlength="250"  onkeyup="return checkchacter(this);"/></td>
		</tr>
		<tr height="50">
			<td colspan="4" align="center" >
			<input type="submit" name="btn"	onclick="return validate();" value="Save" style="padding:0.5% 1%;" /> 
			<input type="reset"	 onclick="resetBtn()" value="Reset"   style="padding:0.5% 1%;"/></td>
		</tr>

	</table>
		</html:form>
		
		<center>
		<html:form action="/TopicMaster.do?action=topicExcelUpload" enctype="multipart/form-data"  onsubmit="return exclValidate();" >
		
		<tr height="50"><td>	
		Excel file:<input  type="file" name="filename" id="excelFile"   style="width: 110px;" /> <input type="submit" value="submit" id="submitid" ></input>
		</td>
		     <td><a class="submit_btn" style="float:left" href="<%=request.getContextPath() %>/studentRegistration.do?action=downloadexcel&amp;filename=Topicmasters.xls"/>Download Excel File Format</a>
		</td>
		
		
		
		
		</tr>
		
		</html:form>
 		</center>
 		
 
<div class="dataGridDisplay"  >
  	<table id="normalTable" class="display" cellspacing="0" width="100%" >
				<thead>
    <tr>
    <th>SI No.</th>
    <th>Topic Name</th>
    <th style="width:100px;">Status</th>
    <th style="width:155px;">Action</th>
    </tr>
    </thead>
    <tbody>
    <logic:notEmpty name="topicList">
    <logic:iterate id="topic" name="topicList" indexId="index">
    <tr>
    <td><%=index+1 %></td>
    <td><bean:write name="topic"
					property="topicName" /></td>
    <td>
<logic:equal name="topic" property="active"
					value="true">
					<font color="green">Active</font>
				</logic:equal> <logic:equal name="topic" property="active" value="false">
					<font color="red">InActive</font>
				</logic:equal>
</td>

    <td>
    
    <logic:equal name="topic" property="active"
					value="true">
					<a href='TopicMaster.do?action=changestatus&amp;topicid=<bean:write name="topic" property="topicId"/>&amp;active=false'><input type="button" value="Remove"></a>
			&nbsp;&nbsp;
		<a href='#' onclick='edit("<bean:write name="topic" property="topicId"/>","<bean:write name="topic" property="courseId"/>","<bean:write name="topic" property="topicName"/>")'><input type="button" value="Edit"></a>
				</logic:equal> <logic:equal name="topic" property="active" value="false">
					<a href='TopicMaster.do?action=changestatus&amp;topicid=<bean:write name="topic" property="topicId"/>&amp;active=true'><input type="button" value="Active"></a>
				</logic:equal>      
</td>
    </tr>
    
 </logic:iterate>
  </logic:notEmpty>
  </tbody>
 
    </table>
     <logic:notEmpty name="topicList">
    <div align="center"><a href="TopicMaster.do?action=excelReport"><button>Excel Export</button></a></div>
      </logic:notEmpty>
      </div>
	
     </div>
  
   <!-- End Main Content -->
	   
 <jsp:include page="/jsp/common/masterdata_footer.jsp"></jsp:include> 
 <script src="js/commonFunction.js"  type="text/javascript"></script>
  </body>
</html>


    	
    	
            