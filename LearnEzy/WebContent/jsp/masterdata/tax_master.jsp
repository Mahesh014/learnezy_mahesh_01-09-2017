
<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Itech Training :: Parameters Master</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300' rel='stylesheet' type='text/css'>
<link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/cmsstyle.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/carousel.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>

<script src="<%=request.getContextPath() %>/js/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/owl.carousel.js" type="text/javascript"></script>

<link href="dataTable/css/jquery.dataTables_themeroller.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.min.css"	rel="stylesheet" type="text/css" media="all" />
<script src="dataTable/js/jquery.dataTables.js"	type="text/javascript"></script>
<script src="dataTable/js/jquery.dataTables.min.js"	type="text/javascript"></script>
<script src="dataTable/js/settingDataTable.js" 	type="text/javascript"></script>
  
<script type="text/javascript">
window.history.forward();
function noBack() { window.history.forward(); }

 function validate(){
	   
		if(document.forms[0].taxName.value.trim()==""){
			alert("Please enter Parameters Type");
			document.forms[0].taxName.focus();
			return false;
		}

		if (document.forms[0].taxValue.value =="")
	    {
	 	   document.forms[0].taxValue.value="";
	 	   document.forms[0].taxValue.focus(); 
	       alert("Please Enter Parameters Value");
	       return false;   
	    }
	    
	return true;
 }
 
 function validCharecters(){
		re = /[^a-z,A-Z, ]/;
		var str=$("#taxName").val().replace(re,"");
		 $("#taxName").val(str);
	}
 
 function validNumbers(){
		//alert("arg");
		re = /\D/g; // remove any characters that are not numbers
		socnum=$("#taxValue").val().replace(re,"");
		$("#taxValue").val(socnum);		 
	}

 
 
</script>
<script type="text/javascript">
$(document).ready(function(){
 	$(document).on("click",".clsJsEdit", function(){
 		var tag = $(this).attr('tag');
		var jsonObj= JSON.parse(tag);
	 	$("#taxName").val(jsonObj.tName);
	 	$("#taxValue").val(jsonObj.tValue);
	 	$("#taxId").val(jsonObj.tId);
	 	$("#taxsettingform").attr("action","parameter.do?action=update");
	 	$("#btnSave").prop('value', 'Update');
 	});
 
});

</script>
</head>
<body>
 
 <jsp:include page="/jsp/common/masterdata_header.jsp"></jsp:include>
 
   <!-- Start Main Content -->
	 <div class="main" style="min-height:450px;">	 
	 	<h2>Parameters</h2>
    <html:form action="parameter.do?action=add" styleId="taxsettingform">
 <input type="hidden" name="taxId"  id="taxId" />  
 <table width="800" cellpadding="0" cellspacing="0" align="center" style="margin-top: 10px;" class="role">
  <tr  align="center">
<td colspan="4" > 
	<div id="status">
			<logic:notEmpty name="status">
			<font color="red"><bean:write name="status"></bean:write></font>
			</logic:notEmpty>
			</div>
			</td>
	  </tr>  
<tr height="50" valign="top">
<td>Parameters Type: </td> <td><input type="text" name="taxName" value=""  id="taxName" autofocus="autofocus" onkeyup="validCharecters(this)"/></td> 
<td>Parameters Value:</td> <td> <input type="text" name="taxValue" value="" id="taxValue" onkeyup="validNumbers(this);" /> </td>
  </tr>  
<tr height="50" valign="top">
<td colspan="4" align="center">
 <input type="submit" value="Submit"  id="btnSave" name="btn" onclick="return validate();" />&nbsp; &nbsp;
 <input type="reset" value="Reset">
</td>
 </tr>
</table>
</html:form>
     
     	
     <div class="dataGridDisplay"  >
   	<table id="normalTable" class="display" cellspacing="0" width="100%"  >
				<thead>
    <tr  >
 		<td>Sl No </td>
 		<td> Parameters Type</td>
 		<td> Parameters Value </td> 
		<td class="no-print" style="width:100px;"> Status </td>
 		<td class="no-print"  style="width:155px;"> Action </td>
 	</tr>
 </thead>
 <tbody>
 <logic:notEmpty name="taxMasterList" >
 <logic:iterate id="taxmaster" name="taxMasterList" indexId="index">
 <tr >
 <td> <%=index + 1%></td>
 <td> <bean:write name="taxmaster" property="taxName"/> </td>
 <td>  <bean:write name="taxmaster" property="taxValue"/> </td> 
 <td class="no-print"> <logic:equal name="taxmaster" property="active" value="true"> <font color="green">Active</font> </logic:equal> 
 	  <logic:equal  name="taxmaster" property="active" value="false"> <font color="red">Inactive</font> </logic:equal>
 </td>
 <td class="no-print">
  
 	<logic:equal name="taxmaster" property="active" value="true">
		<a href='parameter.do?action=changestatus&amp;taxId=<bean:write name="taxmaster" property="taxId"/>&amp;active=false' ><input type="button"  value="Remove"/></a>
				        <a href='javascript:void(0);' class="clsJsEdit"  tag="<bean:write name="taxmaster" property="taxJson"/>" ><input type="button"  value="Edit" /></a>
	</logic:equal> 
							
	<logic:equal name="taxmaster" property="active" value="false">
						<a href='parameter.do?action=changestatus&amp;taxId=<bean:write name="taxmaster" property="taxId"/>&amp;active=true'><input type="button"  value="Activate" /></a>	
	</logic:equal>&nbsp;
 
 </td>
  </tr>
 </logic:iterate>
 </logic:notEmpty>
 
  </tbody>
 
 
 </table>
     <logic:notEmpty name="taxMasterList" >
   <div align="center"><a href="parameter.do?action=excelReport"><button>Excel Export</button></a></div>
   </logic:notEmpty>
    </div>
    
     </div>
     
   <!-- End Main Content -->
	   <jsp:include page="/jsp/common/masterdata_footer.jsp"></jsp:include> 
 <script src="js/commonFunction.js"  type="text/javascript"></script>
  </body>
</html>

