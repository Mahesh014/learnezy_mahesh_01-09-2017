
<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Itech Training :: State Master</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300' rel='stylesheet' type='text/css'>
<link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/cmsstyle.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/carousel.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>

<script src="<%=request.getContextPath() %>/js/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/owl.carousel.js" type="text/javascript"></script>

<link href="dataTable/css/jquery.dataTables_themeroller.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.min.css"	rel="stylesheet" type="text/css" media="all" />
<script src="dataTable/js/jquery.dataTables.js"	type="text/javascript"></script>
<script src="dataTable/js/jquery.dataTables.min.js"	type="text/javascript"></script>
<script src="dataTable/js/settingDataTable.js" 	type="text/javascript"></script>
<script type="text/javascript">
function onload(){
	document.forms[0].countryid.focus();
}

function fetch(){
	var f=document.forms[0];
	if(f.countryid.value!=-1){
		f.action="State.do?action=list";
		f.submit();
	}
}


function edit(id,name,id1){ 	
	document.forms[0].stateid.value=id;
	document.forms[0].statename.value=name;
	document.forms[0].countryid.value=id1;
	document.forms[0].action="State.do?action=update";
	document.forms[0].btn1.value="Update";	
	document.forms[0].statename.focus();
	document.getElementById("status").innerHTML="";		
}

function resetBtn(){
	document.forms[0].statename.value="";
	document.forms[0].countryid.value="-1";
	document.getElementById("status").innerHTML=""
	return false;
}

function validate(){
	   
	if(document.forms[0].countryid.value.trim()=="-1" || document.forms[0].countryid.value.trim()=="-2"){
		alert("Please Select Country");
		document.forms[0].countryid.focus();
		return false;
	}
	
	if(document.forms[0].statename.value.trim()==""){
		alert("Please enter State Name");
		document.forms[0].statename.focus();
		return false;
	}

	if (!document.forms[0].statename.value.match(/^[a-zA-Z ]*$/) && document.forms[0].statename.value !=""){
 	   document.forms[0].statename.value="";
 	   document.forms[0].statename.focus(); 
       alert("Please Enter only Alphabets");
       return false;   
    }

}
</script>
<script type="text/javascript">
    function checkchacter(inputtxt) {
		//alert("arg");
			re = /[^a-z,A-Z, ]/; // remove any characters that are not numbers
		socnum = inputtxt.value.replace(re, "");
		inputtxt.value = socnum;
	}
    </script>
</head>
<body>
 
 <jsp:include page="/jsp/common/masterdata_header.jsp"></jsp:include>
 
   <!-- Start Main Content -->
	 <div class="main" style="min-height:450px;">	 
	 	<h2>State Master</h2>
   <html:form action="State.do?action=add" styleId="stateForm" >
  <table class="role" width="800">
 <tr >
 <td colspan="4" align="center">
  <div align="center"  id="status">    											  
 <logic:notEmpty name="status">
 <font color="red"><bean:write name="status"></bean:write></font>
  </logic:notEmpty></div>
  </td>
  </tr>
<tr >
<td > 
 Country Name:
  <html:select property="countryid" onchange="fetch()" style="width:150px;" styleId="countryid" >
 <html:option value="-1" >--Select Country--</html:option>
 <html:option value="-2" >All</html:option>
 <html:options collection="countryList" property="countryid" labelProperty="countryname"  /> 
 </html:select>
 </td>
 <td > 
 State Name :  <input type="hidden" name="stateid"></input>
 			   <input type ="text" name="statename" id="statename" maxlength="70"  onkeyup="return checkchacter(this);"></input>
   </td>
   <td >
   
 <input type="submit" name="btn" value="Save" onclick="return validate()" />
   <input type="button" value="Reset" onclick="resetBtn();"   />
 </td>
 </tr>
</table>
  </html:form>
     
     <div class="dataGridDisplay"  >	
  	<table id="normalTable" class="display" cellspacing="0" width="100%"   >
				 
				<thead>
    <tr height="30" style="background:#FFFFFF;color:#000;font-weight:bold;">
 <td> SI.NO </td>
 <td> State Name </td> 
 <td class="no-print" style="width:100px;"> Status </td>
 <td class="no-print" style="width:155px;"> Action </td>
 </tr>
 </thead>
 <tbody>
 <logic:notEmpty name="stateList">
  <logic:iterate id="state" name="stateList" indexId="index">
 <tr height="30">
 <td> <%=index+1 %> </td>
 <td> <bean:write name="state" property="statename"/></td> 
 <td class="no-print"> 
 
 <logic:equal name="state" property="active" value="true">
 <font color="green">Active</font>
 </logic:equal>
  <logic:equal name="state" property="active" value="false">
 <font color="red">Inactive</font>
 </logic:equal>
  </td>
 <td class="no-print"> 

  <logic:equal name="state" property="active" value="true">
 <a href='State.do?action=changestatus&amp;stateid=<bean:write name="state" property="stateid"/>&amp;countryid=<bean:write name="state" property="countryid"/>&amp;active=false' style="text-decoration:none;"><input type="button"  value="Remove"/></a>
 <a href='#' onclick='edit("<bean:write name="state" property="stateid"/>","<bean:write name="state" property="statename"/>","<bean:write name="state" property="countryid"/>")'><input type="button"  value="Edit" /></a>
  </logic:equal>
<logic:equal name="state" property="active" value="false">
<a href='State.do?action=changestatus&amp;stateid=<bean:write name="state" property="stateid"/>&amp;countryid=<bean:write name="state" property="countryid"/>&amp;active=true' style="text-decoration:none;"><input type="button"  value="Activate" /></a>

</logic:equal>
  </td>
  </tr>
  </logic:iterate>
  </logic:notEmpty>
  
   <logic:present name="stateList">
         <logic:empty name="stateList">
             <tr height="30"> <td align="center" colspan="4"><font color="black"> No State are added yet.</font></td></tr>
		 </logic:empty>
	 </logic:present>

    </tbody>
    </table>
     <logic:notEmpty name="stateList">
    <div align="center"><a href="State.do?action=excelReport"><button>Excel Export</button></a></div>
    </logic:notEmpty>
    </div>
    </div>
   
    
    
     
     
   <!-- End Main Content -->
	   <jsp:include page="/jsp/common/masterdata_footer.jsp"></jsp:include> 
 <script src="js/commonFunction.js"  type="text/javascript"></script>
  </body>
</html>

