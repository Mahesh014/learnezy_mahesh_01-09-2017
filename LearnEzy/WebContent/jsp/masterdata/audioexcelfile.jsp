<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%

response.setContentType("application/vnd.ms-excel");
response.setHeader("Content-Disposition", "inline; filename="+ "audiomaster.xls");
%>

<table width="930" cellpadding="0" cellspacing="0" border="1" class="table1">
<tr class="height">

<th>SL No</th>
<th>course Name</th>
	<td> Audio File </td> 
	<td > Status </td>
</tr>

<logic:notEmpty name="audioList" >
 <logic:iterate id="audioList" name="audioList" indexId="index">
<tr>
<td> <%=index + 1%></td>
 <td> <bean:write name="audioList" property="courseName"/> </td>
 <td><bean:write name="audioList" property="fileName"/>Download</a> </td> 
   
 
 <td class="no-print"> <logic:equal name="audioList" property="active" value="true"> <font color="green">Active</font> </logic:equal> 
 	  <logic:equal  name="audioList" property="active" value="false"> <font color="red">Inactive</font> </logic:equal>
 </td>
  
 </tr>
		</logic:iterate>
	</logic:notEmpty>
	</table>
</body>
</html>