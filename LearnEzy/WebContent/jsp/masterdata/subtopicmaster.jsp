<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-nested.tld" prefix="nested"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Itech Training :: Sub Topic Master</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="icon" type="image/png"	href="<%=request.getContextPath()%>/images/logo.png"></link>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="UI_style.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath()%>/css/stylesheet_tms.css"	rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath()%>/css/search_data.css"	rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath()%>/css/style1.css"	rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath()%>/css/bootstrap.css"	rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath()%>/css/cmsstyle.css"	rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath()%>/css/font-awesome.css"	rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />

<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>

<link href="dataTable/css/jquery.dataTables_themeroller.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.min.css"	rel="stylesheet" type="text/css" media="all" />
<script src="dataTable/js/jquery.dataTables.js"	type="text/javascript"></script>
<script src="dataTable/js/jquery.dataTables.min.js"	type="text/javascript"></script>
<script src="dataTable/js/settingDataTable.js" 	type="text/javascript"></script>


<!-- Text Editor -->
<script type="text/javascript">
	function valueCall(){
		if(document.forms[0].courseId.value == -1 ){
			alert("Please Select the Course Name ..!");
			document.forms[0].courseId.focus();
			return false;
		}
		if(document.forms[0].topicId.value == -1 )	{
			alert("Please Select the Topic Name ..!");
			document.forms[0].topicId.focus();
			return false;
		}
		if(document.forms[0].subTopicName.value == "") {
			alert("Please Enter the Sub Topic Name ..!");
			document.forms[0].subTopicName.focus();
			return false;
		}
	}
	
	function edit(stip,cid,tid,stname){
		document.forms[0].subTopicId.value =stip;
		document.forms[0].courseId.value =cid;
		document.forms[0].topicId.value =tid;
		document.forms[0].subTopicName.value =stname;
		document.forms[0].action="SubTopicMaster.do?action=updateCourse";
		document.forms[0].btn.value="Update";
		document.forms[0].courseId.focus();
	}

function listMetodCall()
{
	document.forms[0].action="SubTopicMaster.do?action=list";
	document.forms[0].submit();
}

function exclValidate(){
	if(document.forms[1].excelFile.value == ""){
		alert("Please Select  xl or xls extension file");
		
		return false;
	}
}
</script>



<script>
    $(document).ready(function() {
        $("#resetBtn").click(function(){
           	$("#courseId").val(-1);
 			$("#topicId").val(-1);
 			$("#subTopicName").val("");
 			return false;
 			
 	 	});
        	$("#topicId").val(-1);
			$("#subTopicName").val("");



			$('#excelFile').change(function () {
				var val = $(this).val().toLowerCase();
				//alert(val);
				var regex = new RegExp("(.*?)\.(xls|xl)$");
				if(!(regex.test(val))) {
				 	$(this).val('');
				 	alert('Please select .xls .xl format only');
				 } 
			}); 		
    });
    </script>
    
</head>
<body>
	<jsp:include page="/jsp/common/masterdata_header.jsp"></jsp:include>

	<!-- Start Main Content -->
	<div class="main" style="min-height: 450px;">
		<h2>Sub Topic Master</h2>
		<div id="status" align="center">
			<logic:notEmpty name="status">
				<font color="blue"><bean:write name="status"></bean:write></font>
			</logic:notEmpty>
		</div>
		<html:form action="/SubTopicMaster.do?action=addCourse">
			<input type="hidden" name="subTopicId" />
			<table border="0" width="95%" align="center" class="role">
				 
				<tr height="50">

					<td><label>Course *</label></td>
					<td><html:select property="courseId" styleId="courseId"
							onchange="listMetodCall()">
							<html:option value="-1">-Select-</html:option>
							<html:options collection="courseList" property="courseId"
								labelProperty="courseName" />
						</html:select></td>


					<td><label>Topic Name *</label></td>
					<td><html:select property="topicId" styleId="topicId">
							<html:option value="-1">-Select-</html:option>
							<html:options collection="topicList" property="topicId"
								labelProperty="topicName" />
						</html:select></td>


					
				</tr>

				<tr height="50">
				<td><label>Sub Topic Name* </label></td>
					<td><input type="text" name="subTopicName" id="subTopicName"
						maxlength="250"  onkeyup="return checkchacter(this);"  /></td>
					<td colspan="2" align="left">
					<input type="submit" name="btn" value="Submit" onclick="return valueCall();"	/> 
					<input type="button" value="Reset"	 id="resetBtn" /></td>
				</tr>

			</table>
		</html:form>
		
		<html:form action="SubTopicMaster.do?action=excelUpload" enctype="multipart/form-data" onsubmit="return exclValidate();">
		
		<table border="0" width="90%" align="center" class="role">
		<tr height="40">
            <td colspan=   align="center">Excel Upload</td>
       				 <td><input type="file" name="excelFile" id="excelFile" /></td>
       				  <td colspan=   align="left" ><input type="submit" value="Submit"></input></td>
       				  
       				  		     <td><a class="submit_btn" style="float:left" href="<%=request.getContextPath() %>/studentRegistration.do?action=downloadexcel&amp;filename=subtopicmaster.xlsx"/>Download Excel File Format</a>
       				  </td>
       				  
       				  
		</tr>
		</table>
		</html:form>
<div class="dataGridDisplay"  >
		
			<table id="normalTable" class="display" cellspacing="0" width="100%"  >
				<thead>
				<tr>
					<th>SI No.</th>
					<th>Sub Topic Name</th>
					<th style="width:100px;">Status</th>
					<th style="width:155px;">Action</th>
				</tr>
				</thead>
				<tbody>
<logic:notEmpty name="subTopicList">
				<logic:iterate id="subtopic" name="subTopicList" indexId="index">
					<tr>
						<td><%=index + 1%></td>
						<td><bean:write name="subtopic" property="subTopicName" /></td>
						<td><logic:equal name="subtopic" property="active"
								value="true">
								<font color="green">Active</font>
							</logic:equal> <logic:equal name="subtopic" property="active" value="false">
								<font color="red">InActive</font>
							</logic:equal></td>

						<td><logic:equal name="subtopic" property="active"
								value="true">
								<a
									href='SubTopicMaster.do?action=changestatus&amp;subtopicid=<bean:write name="subtopic" property="subTopicId"/>&amp;active=false'><input
									type="button" value="Remove"></a>
			&nbsp;&nbsp;
			<a href="#"
									onclick='edit("<bean:write name="subtopic" property="subTopicId"/>",
									  "<bean:write name="subtopic" property="courseId"/>",
									  "<bean:write name="subtopic" property="topicId"/>",
									  "<bean:write name="subtopic" property="subTopicName"/>")'><input
									type="button" value="Edit"></a>
							</logic:equal> <logic:equal name="subtopic" property="active" value="false">
								<a
									href='SubTopicMaster.do?action=changestatus&amp;subtopicid=<bean:write name="subtopic" property="subTopicId"/>&amp;active=true'><input
									type="button" value="Active"></a>
							</logic:equal></td>
					</tr>

				</logic:iterate>
				 </logic:notEmpty>
    </tbody>
			</table>
<logic:notEmpty name="subTopicList">
		 <div align="center"><a href="SubTopicMaster.do?action=excelReport"><button>Excel Export</button></a></div>
		</logic:notEmpty> 	
		 	</div>
	 
	</div>
	 
	<!-- End Main Content -->

	<jsp:include page="/jsp/common/masterdata_footer.jsp"></jsp:include>
	<script type="text/javascript">
	document.forms[0].courseId.focus();
	</script>
	<script src="js/commonFunction.js"  type="text/javascript"></script>
</body>
</html>






