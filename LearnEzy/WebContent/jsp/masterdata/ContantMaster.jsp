<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-nested.tld" prefix="nested"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Itech Training :: Contant Master</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script language="javascript" src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
<!--
 Rich Text Box Plug in
-->
<link rel="stylesheet"	href="<%=request.getContextPath() %>/htmlEditor/tinyeditor.css">
<script	src="<%=request.getContextPath() %>/htmlEditor/tiny.editor.packed.js"></script>

<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="UI_style.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath() %>/css/stylesheet_tms.css"	rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath() %>/css/search_data.css"	rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath() %>/css/style1.css"	rel="stylesheet" type="text/css" />

<link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/cmsstyle.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>

<script type="text/javascript"	src="<%=request.getContextPath() %>/js/homecalender.js"></script>
<script language="javascript"	src="<%=request.getContextPath()%>/js/common.js"></script>

<script type="text/javascript"	src="<%=request.getContextPath() %>/htmlEditor/scripts/widgEditor.js"></script>

<link href="dataTable/css/jquery.dataTables_themeroller.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.min.css"	rel="stylesheet" type="text/css" media="all" />
<script src="dataTable/js/jquery.dataTables.js"	type="text/javascript"></script>
<script src="dataTable/js/jquery.dataTables.min.js"	type="text/javascript"></script>
<script src="dataTable/js/settingDataTable.js" 	type="text/javascript"></script>

<!-- Text Editor -->
<script type="text/javascript">
	function validate() {
		if(document.forms[0].courseId.value == -1 ){
			alert("Please Select the Course Name ..!");
			document.forms[0].courseId.focus();
			return false;
		}else
		if(document.forms[0].topicId.value == -1 ){
			alert("Please Select the Topic Name ..!");
			document.forms[0].topicId.focus();
			return false;
		}else
		if(document.forms[0].subTopicId.value == -1 ){
			alert("Please Select the Sub Topic Name ..!");
			document.forms[0].subTopicId.focus();
			return false;
		}else
		if(window.frames[0].document.body.innerHTML==""){
			alert("Please Enter the Content Text ..!");
			return false;
		}else{
			document.forms[0].contentText.value = window.frames[0].document.body.innerHTML;
		}
		
	}

	function listMetodCall(){
		document.forms[0].action="ContantMaster.do?action=list";    
		document.forms[0].submit();
	}


	function exclValidate(){
		if(document.forms[1].excelFile.value == ""){
			alert("Please Select  xl or xls extension file");
			
			return false;
		}
	}

	function edit(contentID){
		$.ajax({	
    		type: "GET",		
    		url:"ContantMaster.do?action=edit",
    		data:{contentID:contentID},
    		success: function(r){	
        		 
    			var json= JSON.parse(r);
    			var courseId=json.cid;
    			var topicId=json.tid;
        		var subTopicId=json.sid;

        		getTopic(courseId,topicId);
    			getSubTopic(topicId,subTopicId);

    			$("#hiddenimageFileid").val(json.imageFile);
    			$("#hiddenvideoFileid").val(json.videoFile);


    			$("#imagespanid").text(json.imageFile);
    			$("#videospanid").text(json.videoFile);
    			$("#youTubeLink").val(json.youTubeLink);
    			$("#cid").val(courseId);
    			$("#tid").val(topicId);
    			$("#sid").val(subTopicId);
    			
    			window.frames[0].document.body.innerHTML=json.conttxt;
    			$("#contID").val(contentID);
    			$('#contentForm').attr('action', 'ContantMaster.do?action=updateContent');
    			$('#btn').attr('value', 'Update');
    		} 
    	});
	}	

	function getTopic(courseId,selectedTopicId){
		$.ajax({	
    		type: "GET",		
    		url:"ContantMaster.do?action=getTopic",
    		data:{courseId:courseId},
    		success: function(r){	
    			var json= JSON.parse(r);
    			console.log(json);			
    			$("#tid").empty();
    			var firstOption= $("<option>",{text:"--Select Topic--",value:"-1"});
    			$("#tid").append(firstOption);
    			$.each(json,function(i,obj){
    				var option= $("<option>",{text:obj.topicName, value:obj.tid});
    				$("#tid").append(option);
    			});
    			if(selectedTopicId!="" && selectedTopicId!=null){
    				$("#tid").val(selectedTopicId);
        		}
    		} 
    		
    	});
    	
	}	

	
	 

	function getSubTopic(topicId,selectedSubTopicId){
		$.ajax({	
    		type: "GET",		
    		url:"ContantMaster.do?action=getSubTopic",
    		data:{topicId:topicId},
    		success: function(r){	
    			var json= JSON.parse(r);
    			console.log(json);			
    			$("#sid").empty();
    			var firstOption= $("<option>",{text:"--Select SubTopic--",value:"-1"});
    			$("#sid").append(firstOption);
    			$.each(json,function(i,obj){
    				var option= $("<option>",{text:obj.subTopicName, value:obj.sid});
    				$("#sid").append(option);
    			});
    			if(selectedSubTopicId!="" && selectedSubTopicId!=null){
    				$("#sid").val(selectedSubTopicId);
        		}
    		} 
    	});
    	
	}	
	
</script>

<script>
 $(document).ready(function() {
	 $("#cid").val("-1");
	 $("#cid").change(function(){
	    	var cid=$(this).val();
	    	getTopic(cid);
	    	
	 });	

	 $("#tid").change(function(){
	    	var tid=$(this).val();
	    	getSubTopic(tid);
	    	
	 });	

	 
	$('#imageFile').change(function () {
		var val = $(this).val().toLowerCase();
		//alert(val);
		var regex = new RegExp("(.*?)\.(jpg|png|gif|jpeg)$");
		if(!(regex.test(val))) {
		 	$(this).val('');
		 	alert('Please select .jpg .png .gif .jpeg format only');
		 } 
		else
		{

			
$("#imagespanid").text($(this).val().replace(/^.*\\/, ""));
$("#hiddenimageFileid").val($(this).val().replace(/^.*\\/, ""));


		}
	});  
	$('#videoFile').change(function () {
		var val = $(this).val().toLowerCase();
		//alert(val);
		var regex = new RegExp("(.*?)\.(mp4)$");
		if(!(regex.test(val))) {
		 	$(this).val('');
		 	alert('Please select MP4 format only');
		 } 
		else
		{
			$("#videospanid").text($(this).val().replace(/^.*\\/, ""));
			$("#hiddenvideoFileid").val($(this).val().replace(/^.*\\/, ""));

		}
	});  


	$('#excelFile').change(function () {
		var val = $(this).val().toLowerCase();
		//alert(val);
		var regex = new RegExp("(.*?)\.(xls|xl)$");
		if(!(regex.test(val))) {
		 	$(this).val('');
		 	alert('Please select .xls .xl format only');
		 } 
	}); 

	 $("#resetBtn").click(function(){
		$("#youTubeLink").val("");
		$("#cid").val("-1");
		$("#tid").val("-1");
		$("#sid").val("-1");
		$("#imageFile").val("");
		$("#videoFile").val("");
		window.frames[0].document.body.innerHTML="";
		return false;
	    	
	 });

 });


$(document).ready(function()

		{
$("#resetBtn").click(function()
		{

	$("#videospanid").text("");
	$("#imagespanid").text("");
	


		});



		});
 
</script>
</head>
<body>
  <jsp:include page="/jsp/common/masterdata_header.jsp"></jsp:include>
   
   <!-- Start Main Content -->
	 <div class="main" style="min-height:450px;">	 
	 	<h2>Content Master</h2>
   
	<div align="center">
	<logic:notEmpty name="status">
		<font color="blue"><bean:write name="status"></bean:write></font>
	</logic:notEmpty></div>
     <html:form action="/ContantMaster.do?action=addCourse" enctype="multipart/form-data" styleId="contentForm">
     <input type="hidden" name="contentId" value="" id="contID">
    <table  width="85%" align="center" class="role"   >
	
		<tr height="50">
			<td  >Course *</td>
			<td ><html:select property="courseId" styleId="cid" styleClass="selectWidth">
				<html:option value="-1">-Select-</html:option>
				<html:options collection="courseList" property="courseId" labelProperty="courseName" />
			</html:select></td>
			
			<td>Topic list *</td>
			<td><select  name="topicId" id="tid" class="selectWidth">
				<option value="-1">-Select-</option>
				</select>
			</td>
			</tr>
				<tr height="50">
			<td  width="80">Sub Topic Name* </td>
			<td width="100"><select name="subTopicId" id="sid" class="selectWidth">
				<option value="-1">-Select-</option>
				</select>
			</td>
		
	
			 
			<td >Image Upload</td>
			<td><input type="file"  name="imageFile" id="imageFile" />
			</td>
			
			<td ><span id="imagespanid"></span><input type="hidden"  name="hiddenimageFile" id="hiddenimageFileid"  />
			</td>
		</tr>
			<tr height="50">
			<td >Video Upload</td>
			<td ><input type="file"  name="videoFile" id="videoFile" />
			</td>
			
			<td ><span id="videospanid"></span><input type="hidden"  name="hiddenvideoFile" id="hiddenvideoFileid"/>
			</td>
			
			<td >YouTube Link</td>
			<td ><input type="text"  name="youTubeLink" id="youTubeLink" maxlength="500" />
			</td>
			
			
		</tr>
		<tr height="50">
        <td colspan="4"   align="center">Enter Topic Text</td>
        </tr>
        <tr height="50">
        <td colspan="6" align="center"><input type="hidden" name="contentText" value="">
		<textarea id="tinyeditor" style="width:auto; height: 200px"></textarea>
		<script>
			var editor = new TINY.editor.edit('editor', {
				id: 'tinyeditor',
				width: 584,
				height: 175,
				cssclass: 'tinyeditor',
				controlclass: 'tinyeditor-control',
				rowclass: 'tinyeditor-header',
				dividerclass: 'tinyeditor-divider',
				controls: ['bold', 'italic', 'underline', 'strikethrough', '|', 'subscript', 'superscript', '|',
						   'orderedlist', 'unorderedlist', '|', 'outdent', 'indent', '|', 'leftalign',
						   'centeralign', 'rightalign', 'blockjustify', '|', 'unformat', '|', 'undo', 'redo', 'n',
					   	   'font', 'size', 'style', '|', 'image', 'hr', 'link', 'unlink', '|', 'print'],
				footer: true,
				fonts: ['Verdana','Arial','Georgia','Trebuchet MS'],
				xhtml: true,
				cssfile: 'custom.css',
				bodyid: 'editor',
				footerclass: 'tinyeditor-footer',
				toggle: {text: 'source', activetext: 'Design', cssclass: 'toggle'},
				resize: {cssclass: 'resize'}
			});
</script></td>
        </tr>
		
	
		<tr height="50">
			<td colspan="6" align="center" >
				<input type="submit"  id="btn" value="Save" onclick="return validate();" style="padding:0.5% 1%;margin-top: 15px;" /> 
				<input type="button"	value="Reset" id="resetBtn" style="padding:0.5% 1%;margin-top: 15px;"/>
			</td>
		</tr>

	</table>
	
	</html:form>
		<html:form action="ContantMaster.do?action=excelUpload" enctype="multipart/form-data" onsubmit="return exclValidate();">
		
		<table border="0" width="90%" align="center" class="role">
		<tr height="40">
            <td colspan=   align="center">Excel Upload</td>
       				 <td><input type="file" name="excelFile" id="excelFile" /></td>
       				  <td colspan=   align="left" ><input type="submit" value="Submit"></input></td>
       <td><a class="submit_btn" style="float:left" href="<%=request.getContextPath() %>/studentRegistration.do?action=downloadexcel&amp;filename=newcontentmaster.xls"/>Download Excel File Format</a>
       				  
       				  </td>
       				  
       				  
		</tr>
		</table>
		</html:form>
<div class="dataGridDisplay"  >
	<table id="normalTable" class="display" cellspacing="0" width="100%" >
	<thead>
    <tr>
    <th>SI No.</th>
    <th>Course Name</th>
    <th>Topic Name</th>
    <th>Sub Topic Name</th>
    <th  >Status</th>
    <th style="width:155px;">Action</th>
    </tr>
    </thead>
     
    <tbody>
   <logic:notEmpty name="contentList">
    <logic:iterate id="contentList" name="contentList" indexId="index">
    <tr>
    <td><%=index+1 %></td>
    <td><bean:write name="contentList"	property="courseName" /></td>
    <td><bean:write name="contentList"	property="topicName" /></td>
    <td><bean:write name="contentList"	property="subTopicName" /></td>
    <td><logic:equal name="contentList" property="active"	value="true">
			<font color="green">Active</font>
		</logic:equal> 
		<logic:equal name="contentList" property="active" value="false">
			<font color="red">InActive</font>
		</logic:equal>
	</td>
    <td>
    <logic:equal name="contentList" property="active"	value="true">
		<a href='ContantMaster.do?action=changestatus&amp;contentId=<bean:write name="contentList" property="contentId"/>&amp;active=false'><input type="button" value="Remove"></a>
		&nbsp;&nbsp;
		<a href="#" onclick='edit("<bean:write name="contentList" property="contentId"/>")'><input type="button" value="Edit"></a>
	</logic:equal> 
	<logic:equal name="contentList" property="active" value="false">
		<a href='ContantMaster.do?action=changestatus&amp;contentId=<bean:write name="contentList" property="contentId"/>&amp;active=true'><input type="button" value="Active"></a>
	</logic:equal>
    </td>
    </tr>
    </logic:iterate>
    </logic:notEmpty>
    
    </tbody>
    </table>
     <logic:notEmpty name="contentList">
    <div align="center"><a href="ContantMaster.do?action=excelReport"><button>Excel Export</button></a></div>
    </logic:notEmpty>
    </div>
   </div>
   <!-- End Main Content -->
	   
 <jsp:include page="/jsp/common/masterdata_footer.jsp"></jsp:include> 
  </body>
  <script type="text/javascript">
	document.forms[0].courseId.focus();
	</script>
	<script src="js/commonFunction.js"  type="text/javascript"></script>
</html>