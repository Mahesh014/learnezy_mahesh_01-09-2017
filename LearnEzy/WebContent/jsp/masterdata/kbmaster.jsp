<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-nested.tld" prefix="nested"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Itech Training :: Knowledge Bank</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="icon" type="image/png" href="images/logo.png"></link>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="UI_style.css" rel="stylesheet" type="text/css" />
<link href="css/stylesheet_tms.css"	rel="stylesheet" type="text/css" />
<link href="css/search_data.css"	rel="stylesheet" type="text/css" /> 
<link href="css/style1.css"	rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
<script src="js/commonFunction.js"  type="text/javascript"></script>

<!-- Text Editor -->
<script type="text/javascript">
	function valueCall(){
		if(document.forms[0].courseId.value == -1 ){
			alert("Please Select the Course Name ..!");
			document.forms[0].courseId.focus();
			return false;
		}
		if(document.forms[0].question.value == "" )	{
			alert("Please Enter the Question ..!");
			document.forms[0].question.focus();
			return false;
		}
		if(document.forms[0].answer.value == "") {
			alert("Please Enter the Answer ..!");
			document.forms[0].answer.focus();
			return false;
		}
	}
	

function listMetodCall(){
	document.forms[0].action="KBMaster.do?action=list";
	document.forms[0].submit();
}
</script>

<link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/cmsstyle.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>

<link href="dataTable/css/jquery.dataTables_themeroller.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.min.css"	rel="stylesheet" type="text/css" media="all" />
<script src="dataTable/js/jquery.dataTables.js"	type="text/javascript"></script>
<script src="dataTable/js/jquery.dataTables.min.js"	type="text/javascript"></script>
<script src="dataTable/js/settingDataTable.js" 	type="text/javascript"></script>
 
 
<script>
    $(document).ready(function() {
      // $("#courseId").val("-1");
       $("#resetBtn").click(function(){
         	$("#courseId").val("-1");
         	$("#question").val("");
  			$("#answer").val("");
         	return false;
 		});
       
       $(".editJson").click(function(){
  		 var tag=$(this).attr("tag");
  		 var jsonObj=JSON.parse(tag);
  		 	$("#kbId").val(jsonObj.kbId);
  			$("#question").val(jsonObj.question);
  			$("#answer").val(jsonObj.answer);
  			$("#courseId").val(jsonObj.courseId);
  			document.forms[0].action="KBMaster.do?action=update";
  			document.forms[0].btn.value="Update";
  	 });
       
    });
    </script>
</head>
<body>
  <jsp:include page="/jsp/common/masterdata_header.jsp"></jsp:include>
   
   <!-- Start Main Content -->
	 <div class="main" style="min-height:450px;">	 
	 	<h2>Knowledge Bank</h2>
    <div id="status" align="center"><logic:notEmpty name="status">
					<font color="blue"><bean:write name="status"></bean:write></font>
				</logic:notEmpty></div>
   <html:form action="/KBMaster.do?action=add" >
    <input type="hidden" name="kbId" id="kbId"	 />
    <table border="0" width="80%" align="center"  class="role" >
		
		<tr  >
			
			<td><label>Course *</label> &nbsp;<html:select property="courseId" styleId="courseId" onchange="listMetodCall()">
				<html:option value="-1">-Select-</html:option>
				<html:options collection="courseList" property="courseId" labelProperty="courseName" />
			</html:select></td>
			</tr>
		<tr >
			<td style="height:20px;"><label>Question *</label></td>
		</tr>
		<tr >
			
			<td> <textarea  name="question"  id="question"   maxlength="200" style="width: 735px;height: 60px;resize: none;"></textarea></td>
		</tr>
		
		<tr >
		<td style="height:20px;"><label>Answer * </label></td>
		</tr>
		<tr >
		<td><textarea  name="answer"  id="answer"   maxlength="2000" style="width: 735px;height: 60px;resize: none;"></textarea>
		</td>
		</tr>
	
		<tr >
	<td align="center" colspan="2">
			<input type="submit" name="btn" value="Submit" onclick="return valueCall();" style="padding:0.5% 1%;" />
			<input type="button"  value="Reset" style="padding:0.5% 1%;" id="resetBtn"/> 
		</tr>

	</table>
     </html:form> 	
		
 <div class="dataGridDisplay"  >
   <table id="normalTable" class="display" cellspacing="0" width="100%" align="center" >
	<thead>
    <tr>
    <th>SI No.</th>
    <th>Course Name</th>
    <th>Question</th>
    <th>Answer</th>
    <th style="width:100px;">Status</th>
    <th style="width:155px;" >Action</th>
    </tr>
    </thead>
    <tbody>
    <logic:notEmpty name="KBlist">
    <logic:iterate id="KBlist" name="KBlist" indexId="index">
    <tr>
    <td><%=index+1 %></td>
    <td><bean:write name="KBlist"	property="courseName" /></td>
    <td><textarea rows="5" cols="28" readonly="readonly" style="resize:none;"><bean:write name="KBlist"	property="question" /></textarea></td>
    <td><textarea rows="5" cols="28" readonly="readonly" style="resize:none;"><bean:write name="KBlist"	property="answer" /></textarea></td>
	<td><logic:equal name="KBlist" property="active"	value="true">
			<font color="green">Active</font>
		</logic:equal> 
		<logic:equal name="KBlist" property="active" value="false">
			<font color="red">InActive</font>
		</logic:equal>
</td>

    <td>
    
    <logic:equal name="KBlist" property="active"	value="true">
			<a href='KBMaster.do?action=changestatus&amp;kbId=<bean:write name="KBlist" property="kbId"/>&amp;active=false'><input type="button" value="Remove"></a>
			&nbsp;&nbsp;
			<a href="#" tag="<bean:write name="KBlist" property="jEdit"/>" class="editJson"><input type="button" value="Edit"></a>
	</logic:equal> 
	<logic:equal name="KBlist" property="active" value="false">
			<a href='KBMaster.do?action=changestatus&amp;kbId=<bean:write name="KBlist" property="kbId"/>&amp;active=true'><input type="button" value="Active"></a>
	</logic:equal>
   
    
</td>
    </tr>
   
 </logic:iterate>
  </logic:notEmpty>
   </tbody>
     
    </table>
    <logic:notEmpty name="KBlist">
    <div align="center"><a href="KBMaster.do?action=excelReport"><button>Excel Export</button></a></div>
    </logic:notEmpty>
     </div>
	 
     </div>
    
   <!-- End Main Content -->
	   
 <jsp:include page="/jsp/common/masterdata_footer.jsp"></jsp:include> 
 <script src="js/commonFunction.js"  type="text/javascript"></script>
  </body>
</html>




    	
    	
            