<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Itech Training :: Parent Company Master</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300' rel='stylesheet' type='text/css'>
<link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/cmsstyle.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/carousel.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>

<script src="<%=request.getContextPath() %>/js/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/owl.carousel.js" type="text/javascript"></script>
  
<link href="dataTable/css/jquery.dataTables_themeroller.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.min.css"	rel="stylesheet" type="text/css" media="all" />
<script src="dataTable/js/jquery.dataTables.js"	type="text/javascript"></script>
<script src="dataTable/js/jquery.dataTables.min.js"	type="text/javascript"></script>
<script src="dataTable/js/settingDataTable.js" 	type="text/javascript"></script>

<script type="text/javascript">
window.history.forward();
function noBack() { window.history.forward(); }

 function edit(id,desc){
	 	document.getElementById("status").innerHTML="";
		document.forms[0].action="Parentcomp.do?action=update";
		document.forms[0].pcompid.value=id;
		document.forms[0].pcompanyname.value=desc;
		document.forms[0].pcompanyname.focus();
		document.forms[0].btn.value="Update";
	}
  

 function validate(){
	   
		if(document.forms[0].pcompanyname.value.trim()==""){
			alert("Please enter Parent Company Name");
			document.forms[0].pcompanyname.focus();
			return false;
		}

		if (!document.forms[0].pcompanyname.value.match(/^[a-zA-Z ]*$/) && document.forms[0].pcompanyname.value !="")
	    {
	 	   document.forms[0].pcompanyname.value="";
	 	   document.forms[0].pcompanyname.focus(); 
	       alert("Please Enter only Alphabets");
	       return false;   
	    }
	    
	return true;
 }

 
 
</script>
<script type="text/javascript">
    function checkchacter(inputtxt) {
		//alert("arg");
			re = /[^a-z,A-Z, ]/; // remove any characters that are not numbers
		socnum = inputtxt.value.replace(re, "");
		inputtxt.value = socnum;
	}
    </script>
</head>
<body>
 
 <jsp:include page="/jsp/common/masterdata_header.jsp"></jsp:include>
 
   <!-- Start Main Content -->
	 <div class="main" style="min-height:450px;">	 
	 	<h2>Parent Company Master</h2>
    <html:form action="/Parentcomp.do?action=add" styleId="countryForm">
    <input type="hidden" name="pcompid" />
    <div id="status" align="center"><logic:notEmpty name="status">
					<font color="blue"><bean:write name="status"></bean:write></font>
				</logic:notEmpty></div>
     <table class="role" width="600">
     <tr>
     
     <td>Parent Company Name:</td>
     <td><input type="text"  name="pcompanyname" size="25" maxlength="50" autofocus onkeyup="return checkchacter(this);"></td></tr>
  <tr>  <td><input type="submit" name="btn" onclick="return validate();" value="Save"> <input type="reset" value="Reset" ></td>
     </tr>
     </table>
     </html:form>
     
     	
     <div class="dataGridDisplay"  >
   	<table id="normalTable" class="display" cellspacing="0" width="100%" align="center" >
				<thead>
    <tr>
    <th>SI No.</th>
    <th>Parent Company Name</th>
    <th style="width:75px;">Status</th>
    <th style="width:155px;">Action</th>
    </tr>
    </thead>
    <tbody>
    
    <logic:notEmpty name="countryList">
    
    	<logic:iterate id="country" name="countryList" indexId="index">
    <tr>
    <td><%=index+1 %></td>
    <td><bean:write name="country" property="pcompanyname" /></td>
    <td>
    
    <logic:equal name="country"
							property="active" value="true">
							<font color="green">Active</font>
						</logic:equal> <logic:equal name="country" property="active" value="false">
							<font color="red">Inactive</font>
						</logic:equal>
    
    </td>
    
    <td>
    <logic:equal name="country"
							property="active" value="true">
							<a style="text-decoration: none;"
								href='Parentcomp.do?action=changestatus&amp;pcompid=<bean:write name="country" property="pcompid"/>&amp;active=false'> <input type="button" value="Remove"></a>
								&nbsp; <a href='#'
								onclick='edit("<bean:write name="country" property="pcompid"/>","<bean:write name="country" property="pcompanyname"/>")'><input type="button" value="Edit"></a>
						</logic:equal> <logic:equal name="country" property="active" value="false">
							<a style="text-decoration: none;"
								href='Parentcomp.do?action=changestatus&amp;pcompid=<bean:write name="country" property="pcompid"/>&amp;active=true'> <input type="button" value="Active"></a>
						</logic:equal>
    
</td>
    </tr>
  
    
    </logic:iterate>
     </logic:notEmpty>
    </tbody>
    
    </table>
      <logic:notEmpty name="countryList">
    <div align="center"><a href="Parentcomp.do?action=parentexcelReport"><button>Excel Export</button></a></div>
    </logic:notEmpty>
    </div>
    
    
     </div>
     
   <!-- End Main Content -->
	   <jsp:include page="/jsp/common/masterdata_footer.jsp"></jsp:include> 
 <script src="js/commonFunction.js"  type="text/javascript"></script>
  </body>
</html>

