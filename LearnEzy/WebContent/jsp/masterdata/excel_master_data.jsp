<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-nested.tld" prefix="nested"%>
<%@ page contentType="application/vnd.ms-excel"%>
<html:html>

<!-- ------------------------------------------------AGENCY MASTER--------------------------------- -->
<logic:present name="assignmentList">
	<table border="1" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Sl No.</th>
				<th>Assignment Question</th>
				<th>Assignment Answer</th>
				<th>Status</th>

			</tr>
		</thead>
		<tbody>

			<logic:notEmpty name="assignmentList">
				<logic:iterate id="assignmentList" name="assignmentList"
					indexId="index">
					<tr>
						<td><%=index + 1%></td>
						<td><bean:write name="assignmentList"
								property="assignmentQuestion" /></td>
						<td><bean:write name="assignmentList"
								property="assignmentAnswer" /></td>
						<td><logic:equal name="assignmentList" property="active"
								value="true">
								<font color="green">Active</font>
							</logic:equal> <logic:equal name="assignmentList" property="active"
								value="false">
								<font color="red">InActive</font>
							</logic:equal></td>


					</tr>

				</logic:iterate>
			</logic:notEmpty>
		</tbody>
	</table>
</logic:present>


<logic:notEmpty name="courseList">
	<table border="1" class="display" cellspacing="0" width="100%">
		<thead>

			<tr>
				<th>SI No.</th>
				<th>Course Name</th>
				<th>Fees</th>
				<!-- <th>Maximum Students</th> -->
				<!--  <th>Duration</th> -->
				<th>Pass Percentage</th>
				<th>Discount Percentage</th>
				<th>Discount Valid UpTo</th>
				<th>Status</th>

			</tr>
		</thead>
		<tbody>

			<logic:iterate id="course" name="courseList" indexId="index">
				<tr>
					<td><%=index + 1%></td>
					<td><bean:write name="course" property="courseName" /></td>
					<td><bean:write name="course" property="fees" /></td>
					<!--<td><bean:write name="course" property="maxStd" /></td> -->
					<!--<td><bean:write name="course" property="duration" /></td> -->
					<td><bean:write name="course" property="passpercent" /></td>
					<td><bean:write name="course" property="totalDiscountAmount" /></td>
					<td><bean:write name="course"
							property="discountValidUpToDisplay" /></td>
					<td><logic:equal name="course" property="active" value="true">
							<font color="green">Active</font>
						</logic:equal> <logic:equal name="course" property="active" value="false">
							<font color="red">InActive</font>
						</logic:equal></td>



				</tr>
			</logic:iterate>

		</tbody>


	</table>

</logic:notEmpty>

<logic:notEmpty name="contentList">
	<table border="1" class="display" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>SI No.</th>
				<th>Course Name</th>
				<th>Topic Name</th>
				<th>Sub Topic Name</th>
				<th>Status</th>

			</tr>
		</thead>
		<tbody>

			<logic:iterate id="contentList" name="contentList" indexId="index">
				<tr>
					<td><%=index + 1%></td>
					<td><bean:write name="contentList" property="courseName" /></td>
					<td><bean:write name="contentList" property="topicName" /></td>
					<td><bean:write name="contentList" property="subTopicName" /></td>
					<td><logic:equal name="contentList" property="active"
							value="true">
							<font color="green">Active</font>
						</logic:equal> <logic:equal name="contentList" property="active" value="false">
							<font color="red">InActive</font>
						</logic:equal></td>

				</tr>
			</logic:iterate>


		</tbody>
	</table>
</logic:notEmpty>

<logic:notEmpty name="countryList">
	<table border="1" class="display" cellspacing="0" width="100%"
		align="center">
		<thead>
			<tr>
				<th>SI No.</th>
				<th>Country Name</th>
				<th style="width: 75px;">Status</th>

			</tr>
		</thead>
		<tbody>



			<logic:iterate id="country" name="countryList" indexId="index">
				<tr>
					<td><%=index + 1%></td>
					<td><bean:write name="country" property="countryname" /></td>
					<td><logic:equal name="country" property="active" value="true">
							<font color="green">Active</font>
						</logic:equal> <logic:equal name="country" property="active" value="false">
							<font color="red">Inactive</font>
						</logic:equal></td>


				</tr>


			</logic:iterate>

		</tbody>

	</table>
</logic:notEmpty>
<logic:notEmpty name="cityList">

	<table border="1" class="display" cellspacing="0" width="100%"
		align="center">
		<thead>
			<tr height="30"
				style="background: #FFFFFF; color: #000; font-weight: bold;">
				<td>SI.NO</td>
				<td>City Name</td>
				<td class="no-print" style="width: 100px;">Status</td>

			</tr>
		</thead>
		<tbody>

			<logic:iterate id="city" name="cityList" indexId="index">
				<tr height="30">

					<td><%=index + 1%></td>
					<td><bean:write name="city" property="cityname" /></td>
					<td class="no-print"><logic:equal name="city"
							property="active" value="true">
							<font color="green">Active</font>
						</logic:equal> <logic:equal name="city" property="active" value="false">
							<font color="red">Inactive</font>
						</logic:equal></td>

				</tr>
			</logic:iterate>

		</tbody>
	</table>
</logic:notEmpty>

<logic:notEmpty name="eBookList">
	<table border="1" class="display" cellspacing="0" width="100%"
		align="center">
		<thead>
			<tr>
				<th>SI No.</th>
				<th>Course Name</th>
				<th>eBook Name</th>
				<th>File Name</th>
				<th>Status</th>

			</tr>
		</thead>
		<tbody>

			<logic:iterate id="eBookName" name="eBookList" indexId="index">
				<tr>
					<td><%=index + 1%></td>
					<td><bean:write name="eBookName" property="courseName" /></td>
					<td><bean:write name="eBookName" property="eBookName" /></td>
					<td><a
						href="<%=request.getContextPath()%>/ebookPDF/<bean:write name="eBookName"	property="fileName" />"><bean:write
								name="eBookName" property="fileName" /></a></td>


					<td><logic:equal name="eBookName" property="active"
							value="true">
							<font color="green">Active</font>
						</logic:equal> <logic:equal name="eBookName" property="active" value="false">
							<font color="red">InActive</font>
						</logic:equal></td>

				</tr>

			</logic:iterate>

		</tbody>

	</table>
</logic:notEmpty>
<logic:notEmpty name="linkList">
	<table border="1" class="display" cellspacing="0" width="100%"
		align="center">
		<thead>
			<tr>
				<th>SI No.</th>
				<th>Link Name</th>
				<th>URL</th>
				<th>Status</th>

			</tr>
		</thead>
		<tbody>

			<logic:iterate id="link" name="linkList" indexId="index">
				<tr>
					<td><%=index + 1%></td>
					<td><bean:write name="link" property="linkName" /></td>
					<td><a href="<bean:write name="link"	property="URL" />"><bean:write
								name="link" property="URL" /></a></td>
					<td><logic:equal name="link" property="active" value="true">
							<font color="green">Active</font>
						</logic:equal> <logic:equal name="link" property="active" value="false">
							<font color="red">InActive</font>
						</logic:equal></td>


				</tr>

			</logic:iterate>

		</tbody>
	</table>

</logic:notEmpty>

<logic:notEmpty name="taxMasterList">
	<table border="1" class="display" cellspacing="0" width="100%">
		<thead>
			<tr>
				<td>Sl No</td>
				<td>Parameters Type</td>
				<td>Parameters Value</td>
				<td class="no-print" style="width: 100px;">Status</td>

			</tr>
		</thead>
		<tbody>

			<logic:iterate id="taxmaster" name="taxMasterList" indexId="index">
				<tr>
					<td><%=index + 1%></td>
					<td><bean:write name="taxmaster" property="taxName" /></td>
					<td><bean:write name="taxmaster" property="taxValue" /></td>
					<td class="no-print"><logic:equal name="taxmaster"
							property="active" value="true">
							<font color="green">Active</font>
						</logic:equal> <logic:equal name="taxmaster" property="active" value="false">
							<font color="red">Inactive</font>
						</logic:equal></td>

				</tr>
			</logic:iterate>


		</tbody>


	</table>
</logic:notEmpty>
<logic:notEmpty name="KBlist">

	<table border="1" class="display" cellspacing="0" width="100%"
		align="center">
		<thead>
			<tr>
				<th>SI No.</th>
				<th>Course Name</th>
				<th>Question</th>
				<th>Answer</th>
				<th style="width: 100px;">Status</th>

			</tr>
		</thead>
		<tbody>

			<logic:iterate id="KBlist" name="KBlist" indexId="index">
				<tr>
					<td><%=index + 1%></td>
					<td><bean:write name="KBlist" property="courseName" /></td>
					<td><bean:write name="KBlist" property="question" /></td>
					<td><bean:write name="KBlist" property="answer" /></td>
					<td><logic:equal name="KBlist" property="active" value="true">
							<font color="green">Active</font>
						</logic:equal> <logic:equal name="KBlist" property="active" value="false">
							<font color="red">InActive</font>
						</logic:equal></td>


				</tr>

			</logic:iterate>

		</tbody>

	</table>
</logic:notEmpty>
<logic:notEmpty name="roleList">
	<table border="1" class="display" cellspacing="0" width="100%"
		align="center">
		<thead>
			<tr>
				<th>SI No.</th>
				<th>Role Name</th>
				<th style="width: 100px;">Status</th>

			</tr>
		</thead>
		<tbody>


			<logic:iterate id="userrole" name="roleList" indexId="index">
				<tr>
					<td><%=index + 1%></td>
					<td><bean:write name="userrole" property="rolename" /></td>
					<td><logic:equal name="userrole" property="active"
							value="true">
							<font color="green">Active</font>
						</logic:equal> <logic:equal name="userrole" property="active" value="false">
							<font color="red">Inactive</font>
						</logic:equal></td>


				</tr>


			</logic:iterate>

		</tbody>

	</table>
</logic:notEmpty>
<logic:notEmpty name="stateList">
	<table border="1" class="display" cellspacing="0" width="100%">

		<thead>
			<tr height="30"
				style="background: #FFFFFF; color: #000; font-weight: bold;">
				<td>SI.NO</td>
				<td>State Name</td>
				<td class="no-print" style="width: 100px;">Status</td>

			</tr>
		</thead>
		<tbody>

			<logic:iterate id="state" name="stateList" indexId="index">
				<tr height="30">
					<td><%=index + 1%></td>
					<td><bean:write name="state" property="statename" /></td>
					<td class="no-print"><logic:equal name="state"
							property="active" value="true">
							<font color="green">Active</font>
						</logic:equal> <logic:equal name="state" property="active" value="false">
							<font color="red">Inactive</font>
						</logic:equal></td>

				</tr>
			</logic:iterate>


			<logic:present name="stateList">
				<logic:empty name="stateList">
					<tr height="30">
						<td align="center" colspan="4"><font color="black"> No
								State are added yet.</font></td>
					</tr>
				</logic:empty>
			</logic:present>

		</tbody>
	</table>
</logic:notEmpty>
<logic:notEmpty name="subTopicList">
	<table border="1" class="display" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>SI No.</th>
				<th>Sub Topic Name</th>
				<th style="width: 100px;">Status</th>

			</tr>
		</thead>
		<tbody>

			<logic:iterate id="subtopic" name="subTopicList" indexId="index">
				<tr>
					<td><%=index + 1%></td>
					<td><bean:write name="subtopic" property="subTopicName" /></td>
					<td><logic:equal name="subtopic" property="active"
							value="true">
							<font color="green">Active</font>
						</logic:equal> <logic:equal name="subtopic" property="active" value="false">
							<font color="red">InActive</font>
						</logic:equal></td>


				</tr>

			</logic:iterate>

		</tbody>
	</table>
</logic:notEmpty>
<logic:notEmpty name="sampleProgramList">
	<table border="1" class="display" cellspacing="0" width="100%"
		align="center">
		<thead>

			<tr>
				<th>SI No.</th>
				<th>Sub Topic Name</th>
				<th>Sample Program Name</th>
				<th>Status</th>

			</tr>
		</thead>
		<tbody>
			<logic:iterate id="sampleProgram" name="sampleProgramList"
				indexId="index">
				<tr>
					<td><%=index + 1%></td>
					<td><bean:write name="sampleProgram" property="subTopicName" /></td>
					<td><bean:write name="sampleProgram" property="name" /></td>
					<td><logic:equal name="sampleProgram" property="active"
							value="true">
							<font color="green">Active</font>
						</logic:equal> <logic:equal name="sampleProgram" property="active" value="false">
							<font color="red">InActive</font>
						</logic:equal></td>
				</tr>

			</logic:iterate>




		</tbody>
	</table>
</logic:notEmpty>

<logic:notEmpty name="topicList">
	<table border="1" class="display" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>SI No.</th>
				<th>Topic Name</th>
				<th style="width: 100px;">Status</th>

			</tr>
		</thead>
		<tbody>

			<logic:iterate id="topic" name="topicList" indexId="index">
				<tr>
					<td><%=index + 1%></td>
					<td><bean:write name="topic" property="topicName" /></td>
					<td><logic:equal name="topic" property="active" value="true">
							<font color="green">Active</font>
						</logic:equal> <logic:equal name="topic" property="active" value="false">
							<font color="red">InActive</font>
						</logic:equal></td>


				</tr>

			</logic:iterate>

		</tbody>

	</table>
</logic:notEmpty>

<logic:notEmpty name="testTypeList">
	<table border="1" class="display" cellspacing="0" width="100%"
		align="center">
		<thead>

			<tr>
				<th>Sl No.</th>
				<th>Test Type</th>
				<th style="width: 100px;">Status</th>

			</tr>
		</thead>
		<tbody>
			<logic:iterate id="testtype" name="testTypeList" indexId="index">
				<tr class="tdContent2">
					<td align="center" style="color: black"><%=index + 1%></td>
					<td align="left" style="color: black"><bean:write
							name="testtype" property="testType" /></td>
					<td align="center"><logic:equal name="testtype"
							property="active" value="true">
							<font color="green">Active</font>
						</logic:equal> <logic:equal name="testtype" property="active" value="false">
							<font color="red">Inactive</font>
						</logic:equal></td>





				</tr>
			</logic:iterate>


		</tbody>
	</table>
</logic:notEmpty>

<logic:notEmpty name="testList">
	<table border="1" class="display" cellspacing="0" width="100%"
		align="center">
		<thead>
			<tr>
				<th>SI No.</th>
				<th>Test Name</th>
				<th>Test Min Score</th>
				<th>Test Timing</th>
				<th>Test Type</th>
				<th>Course</th>
				<th>Achivement Score</th>
				<th>Status</th>

			</tr>
		</thead>
		<tbody>

			<logic:iterate id="testList" name="testList" indexId="index">
				<tr>
					<td><%=index + 1%></td>
					<td><bean:write name="testList" property="tname" /></td>
					<td><bean:write name="testList" property="minScore" /></td>
					<td><bean:write name="testList" property="testTime" /></td>
					<td><bean:write name="testList" property="testTypeName" /></td>
					<td><bean:write name="testList" property="courseName" /></td>
					<td><bean:write name="testList" property="achivementScore" /></td>
					<td><logic:equal name="testList" property="active"
							value="true">
							<font color="green">Active</font>
						</logic:equal> <logic:equal name="testList" property="active" value="false">
							<font color="red">InActive</font>
						</logic:equal></td>



				</tr>
			</logic:iterate>

		</tbody>
	</table>
</logic:notEmpty>
<logic:notEmpty name="usersList">

	<table border="1" class="display" cellspacing="0" width="100%"
		align="center">
		<thead>
			<tr>
				<th>SI No.</th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Mobile</th>
				<th>Role</th>
				<th>Status</th>

			</tr>
		</thead>
		<tbody>

			<logic:iterate id="user" name="usersList" indexId="index">
				<tr>
					<td><%=index + 1%></td>
					<td><bean:write name="user" property="firstName" /></td>
					<td><bean:write name="user" property="lastName" /></td>
					<td><bean:write name="user" property="contactno" /></td>

					<td><bean:write name="user" property="rolename" /></td>
					<td><logic:equal name="user" property="active" value="true">
							<font color="green">active</font>
						</logic:equal> <logic:equal name="user" property="active" value="false">
							<font color="red">inactive</font>
						</logic:equal></td>

				</tr>
			</logic:iterate>

		</tbody>

	</table>
</logic:notEmpty>


<logic:notEmpty name="questionList">
	<table border="1" class="display" cellspacing="0" width="100%"
		align="center">
		<thead>
			<tr>
				<th>SI No.</th>
				<th>Question Name</th>
				<th style="width: 100px;">Status</th>

			</tr>
		</thead>
		<tbody>

			<logic:iterate id="test" name="questionList" indexId="index">
				<tr>
					<td><%=index + 1%></td>
					<td><bean:write name="test" property="question" /></td>

					<td><logic:equal name="test" property="active" value="true">
							<font color="green">Active</font>
						</logic:equal> <logic:equal name="test" property="active" value="false">
							<font color="red">Inactive</font>
						</logic:equal></td>

				</tr>

			</logic:iterate>

		</tbody>
	</table>
</logic:notEmpty>

<logic:notEmpty name="notificationList">
	<table border="1" class="display" cellspacing="0" width="100%"
		align="center">
		<thead>
			<tr>
				<th>SI No.</th>
				<th>Student Name</th>
				<th>Notification</th>
				<th>Expiry Date</th>
				<th>Status</th>

			</tr>
		</thead>
		<tbody>

			<logic:iterate id="notification" name="notificationList"
				indexId="index">
				<tr>
					<td><%=index + 1%></td>
					<td><bean:write name="notification" property="firstName" /></td>
					<td><bean:write name="notification" property="notification" /></td>
					<td><bean:write name="notification" property="expiryDate" /></td>
					<td><logic:equal name="notification" property="active"
							value="true">
							<font color="green">Active</font>
						</logic:equal> <logic:equal name="notification" property="active" value="false">
							<font color="red">InActive</font>
						</logic:equal></td>


				</tr>

			</logic:iterate>

		</tbody>
	</table>
</logic:notEmpty>


<logic:notEmpty name="CompanyList">

	<table cellspacing="0" border="1" width="100%" align="center">
		<thead>
			<tr>
				<th>SI No.</th>
				<th>Company Name</th>
				<th>Sub domain Name</th>
				<th>Company Logo</th>
				<th style="width: 100px;">Status</th>
			</tr>
		</thead>
		<tbody>
			<logic:notEmpty name="CompanyList">
				<logic:iterate id="userrole" name="CompanyList" indexId="index">
					<tr>
						<td><%=index + 1%></td>
						<td><bean:write name="userrole" property="companyName" /></td>
						<td><bean:write name="userrole" property="compsubdomain" /></td>
						<td><bean:write name="userrole" property="complogo" /></td>
						<td><logic:equal name="userrole" property="active"
								value="true">
								<font color="green">Active</font>
							</logic:equal> <logic:equal name="userrole" property="active" value="false">
								<font color="red">Inactive</font>
							</logic:equal>
						</td>


					</tr>


				</logic:iterate>
			</logic:notEmpty>
		</tbody>

	</table>
</logic:notEmpty>
<logic:notEmpty name="coursecategory">

	<table cellspacing="0" border="1" width="100%" align="center">
		<thead>
			<tr>
				<th>SI No.</th>
				<th>Course category Name</th>
				
				<th style="width: 100px;">Status</th>
			</tr>
		</thead>
		<tbody>
			<logic:notEmpty name="coursecategory">
				<logic:iterate id="userrole" name="coursecategory" indexId="index">
					<tr>
						<td><%=index + 1%></td>
						<td><bean:write name="userrole" property="coursecategoryname" /></td>
						<td><logic:equal name="userrole" property="active" value="true">
								<font color="green">Active</font>
							</logic:equal> <logic:equal name="userrole" property="active" value="false">
								<font color="red">Inactive</font>
							</logic:equal>
						</td>


					</tr>


				</logic:iterate>
			</logic:notEmpty>
		</tbody>

	</table>
</logic:notEmpty>
<logic:notEmpty name="languagelist">

	<table cellspacing="0" border="1" width="100%" align="center">
		<thead>
			<tr>
				<th>SI No.</th>
				<th>Course category Name</th>
				
				<th style="width: 100px;">Status</th>
			</tr>
		</thead>
		<tbody>
			<logic:notEmpty name="languagelist">
				<logic:iterate id="languagelist" name="languagelist" indexId="index">
					<tr>
						<td><%=index + 1%></td>
						<td><bean:write name="languagelist" property="languagename" /></td>
						<td><logic:equal name="languagelist" property="active" value="true">
								<font color="green">Active</font>
							</logic:equal> <logic:equal name="languagelist" property="active" value="false">
								<font color="red">Inactive</font>
							</logic:equal>
						</td>


					</tr>


				</logic:iterate>
			</logic:notEmpty>
		</tbody>

	</table>
</logic:notEmpty>

</html:html>

