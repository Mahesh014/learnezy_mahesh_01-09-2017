
<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
 
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Itech Training :: Audio Master</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300' rel='stylesheet' type='text/css'>
<link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/cmsstyle.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/carousel.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>

<script src="<%=request.getContextPath() %>/js/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/owl.carousel.js" type="text/javascript"></script>

<link href="dataTable/css/jquery.dataTables_themeroller.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.min.css"	rel="stylesheet" type="text/css" media="all" />
<script src="dataTable/js/jquery.dataTables.js"	type="text/javascript"></script>
<script src="dataTable/js/jquery.dataTables.min.js"	type="text/javascript"></script>
<script src="dataTable/js/settingDataTable.js" 	type="text/javascript"></script>
<script src="mediaPlayers/html5media.min.js"></script>
  <!-- <video src="video.mp4" width="320" height="200" controls preload></video>
    <audio src="audio.mp3" controls preload></audio> -->
<script type="text/javascript">
window.history.forward();
function noBack() { window.history.forward(); }

 function validate(){
	   
		if(document.forms[0].courseId.value.trim()==""){
			alert("Please select course name");
			document.forms[0].courseId.focus();
			return false;
		}

		if (document.forms[0].audioFile.value =="" && document.forms[0].fileName.value==""  ){
	 	   document.forms[0].audioFile.focus(); 
	       alert("Please Select Audio File");
	       return false;   
	    }
		 
	    
	return true;
 }
 
 
$(document).ready(function(){
 	$(document).on("click",".clsJsEdit", function(){
 		var tag = $(this).attr('tag');
		var jsonObj= JSON.parse(tag);
		$("#audioId").val(jsonObj.audioId);
		$("#courseId").val(jsonObj.courseId);
		$("#audio").text(jsonObj.fileName);
		$("#fileName").val(jsonObj.fileName);
	 	$("#audioForm").attr("action","Audio.do?action=update");
	 	$("#btnSave").prop('value', 'Update');
 	});
 	
 	$(".validateCharecters").keyup(function(){
 		re = /[^a-z,A-Z, ]/;
		var str=$(this).val().replace(re,"");
		 $(this).val(str);
 	});
 	
 	$(".validateNumbers").keyup(function(){
 		re = /\D/g; 
		var str=$(this).val().replace(re,"");
		 $(this).val(str);
 	});
 	
 	$("#audioFile").change(function () {
 		var fileExtension = ['mp3', 'wav'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Only formats are allowed : "+fileExtension.join(', '));
            $(this).val("");
        }
        else
        {
    		$("#audio").text("");


        }
    });
});

$(document).ready(function()
		{
$("#resetid").click(function()

		{
	$.ajax({
		 type: "GET",
	    url: "studentRegistration.do?action=clear",
	    success : function (response){
	    	$("#courseId").val("");
	    	$("#audio").text("");
	    	

	}
	  });



	

		});



		});



</script>
</head>
<body>
 
 <jsp:include page="/jsp/common/masterdata_header.jsp"></jsp:include>
 
   <!-- Start Main Content -->
	 <div class="main" style="min-height:450px;">	 
	 	<h2>Audio Upload</h2>
    <html:form action="Audio.do?action=add" enctype="multipart/form-data" styleId="audioForm">
 <input type="hidden" name="audioId"  id="audioId" />  
  <input type="hidden" name="fileName"  id="fileName" />  
 <table width="800" cellpadding="0" cellspacing="0" align="center" style="margin-top: 10px;" class="role">
  <tr  align="center">
<td colspan="4" > 
	<div id="status">
			<logic:notEmpty name="status">
			<font color="red"><bean:write name="status"></bean:write></font>
			</logic:notEmpty>
			</div>
			</td>
	  </tr>  
<tr height="50" valign="top">
<td> Course*: </td> <td><html:select property="courseId" styleId="courseId" styleClass="selectWidth">
				<html:option value="">-Select-</html:option>
				<html:options collection="courseList" property="courseId" labelProperty="courseName" />
			</html:select></td> 
<td>Upload Audio File*:</td> <td> <input type="file" name="audioFile" value="" id="audioFile" /><span id="audio"></span> </td>
 </tr> 
  <tr>
<td colspan="4" align="center">
 <input type="submit" value="Submit"  id="btnSave" name="btn" onclick="return validate();" />&nbsp; &nbsp;
 <input type="reset" value="Reset" id="resetid">
</td>
 </tr>
</table>
</html:form>
     
     	
     <div class="dataGridDisplay"  >
   	<table id="normalTable" class="display" cellspacing="0" width="100%"  >
				<thead>
    <tr  >
 		<td>Sl No </td>
 		<td> Course</td>
 		<td> Audio File </td> 
		<td class="no-print" style="width:100px;"> Status </td>
 		<td class="no-print"  style="width:155px;"> Action </td>
 	</tr>
 </thead>
 <tbody>
 <logic:notEmpty name="audioList" >
 <logic:iterate id="audioList" name="audioList" indexId="index">
 <tr >
 <td> <%=index + 1%></td>
 <td> <bean:write name="audioList" property="courseName"/> </td>
 <td><audio src="<%= request.getContextPath() %>/audioFiles/<bean:write name="audioList" property="fileName"/>" controls preload></audio><a href="<%= request.getContextPath() %>/audioFiles/<bean:write name="audioList" property="fileName"/>">Download</a> </td> 
   
 
 <td class="no-print"> <logic:equal name="audioList" property="active" value="true"> <font color="green">Active</font> </logic:equal> 
 	  <logic:equal  name="audioList" property="active" value="false"> <font color="red">Inactive</font> </logic:equal>
 </td>
 <td class="no-print">
  
 	<logic:equal name="audioList" property="active" value="true">
		<a href='Audio.do?action=changestatus&amp;audioId=<bean:write name="audioList" property="audioId"/>&amp;active=false' ><input type="button"  value="Remove"/></a>
				        <a href='javascript:void(0);' class="clsJsEdit"  tag="<bean:write name="audioList" property="editJson"/>" ><input type="button"  value="Edit" /></a>
	</logic:equal> 
							
	<logic:equal name="audioList" property="active" value="false">
						<a href='Audio.do?action=changestatus&amp;audioId=<bean:write name="audioList" property="audioId"/>&amp;active=true'><input type="button"  value="Activate" /></a>	
	</logic:equal>&nbsp;
 
 </td>
  </tr>
 </logic:iterate>
 </logic:notEmpty>
 
  </tbody>
 
 
 </table>
     <logic:notEmpty name="audioList" >
   <div align="center"><a href="Audio.do?action=excelReport"><button>Excel Export</button></a></div>
   </logic:notEmpty>
    </div>
    
     </div>
     
   <!-- End Main Content -->
	   <jsp:include page="/jsp/common/masterdata_footer.jsp"></jsp:include> 
 <script src="js/commonFunction.js"  type="text/javascript"></script>
  </body>
</html>

