<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-nested.tld" prefix="nested"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Itech Training :: User Master</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="<%=request.getContextPath()%>/css/bootstrap.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath()%>/css/cmsstyle.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath()%>/css/font-awesome.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath()%>/css/carousel.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath()%>/css/owl.carousel.css"	rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />

<script src="<%=request.getContextPath()%>/js/jquery-1.10.2.min.js"	type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap.js"	type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"	type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/owl.carousel.js"	type="text/javascript"></script>

<link href="dataTable/css/jquery.dataTables_themeroller.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.min.css"	rel="stylesheet" type="text/css" media="all" />
<script src="dataTable/js/jquery.dataTables.js"	type="text/javascript"></script>
<script src="dataTable/js/jquery.dataTables.min.js"	type="text/javascript"></script>
<script src="dataTable/js/settingDataTable.js" 	type="text/javascript"></script>

<script type="text/javascript">
	function validate() {

		if (document.forms[0].firstName.value.trim() == "") {
			alert("First Name cannot be empty");
			document.forms[0].firstName.focus();
			return false;
		}else
		if (document.forms[0].lastName.value.trim() == "") {
			alert("Last Name cannot be empty");
			document.forms[0].lastName.focus();
			return false;
		}

		else
		if (document.forms[0].emailId.value.trim() == "") {
			alert("Enter Email ID");
			document.forms[0].emailId.focus();
			return false;
		}

		else
			if (document.forms[0].companyID.value.trim() == "-1") {
				alert("Select Company");
				document.forms[0].companyID.focus();
				return false;
			}


		else
		if (document.forms[0].emailId.value.trim() != "") {
			re = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
			if (document.forms[0].emailId.value.search(re) == -1) {
				alert("Please Enter valid Email Id");
				document.forms[0].emailId.value = "";
				document.forms[0].emailId.focus();
				return false;
			}
		} 
		if (document.forms[0].contactno.value.trim() == "") {
			alert("Mobile Number cannot be empty ");
			document.forms[0].contactno.focus();
			return false;
		}else
		if (document.forms[0].roleId.value == "-1") {
			alert("Please select the role");
			document.forms[0].roleId.focus();
			return false;
		}else
		if (document.forms[0].countryid.value == "-1") {
			alert("Please select the Country");
			document.forms[0].countryid.focus();
			return false;
		}else
		if (document.forms[0].countryid.value == "0" && document.forms[0].othercountryname.value == "") {
			alert("other country cannot be empty");
			document.forms[0].othercountryname.focus();
			return false;
		}else		
		if (document.forms[0].stateid.value == "-1") {
			alert("Please select the State");
			document.forms[0].stateid.focus();
			return false;
		}else
		if (document.forms[0].stateid.value == "0" && document.forms[0].otherstatename.value == "") {
			alert("other state cannot be empty");
			document.forms[0].otherstatename.focus();
			return false;
		}else
		if (document.forms[0].cityid.value == "-1") {
			alert("Please select the City");
			document.forms[0].cityid.focus();
			return false;
		}else
		if (document.forms[0].cityid.value == "0" && document.forms[0].othercityname.value == "") {
			alert("other city cannot be empty");
			document.forms[0].othercityname.focus();
			return false;
		}else	
		if (document.forms[0].pincode.value == "") {
			alert("Pincode cannot be empty ");
			document.forms[0].pincode.focus();
			return false;
		}
		if (document.forms[0].password.value.trim() == "") {
			alert("Password cannot be empty");
			document.forms[0].password.focus();
			return false;
		}else
		if (document.forms[0].retypepassword.value.trim() == "") {
			alert("Confirm password cannot be empty");
			document.forms[0].retypepassword.focus();
			return false;
		}else
		if (document.forms[0].retypepassword.value.trim() != document.forms[0].password.value.trim()) {
			alert("Passwords does not match");
			document.forms[0].password.value="";
			document.forms[0].retypepassword.value="";
			document.forms[0].password.focus();
			return false;
		}else
		if (document.forms[0].btn.value == "Save") {
			document.forms[0].action = "UserMaster.do?action=addUser";
			document.forms[0].submit();
		}else if (document.forms[0].btn.value == "Save Changes") {
					document.forms[0].action = "UserMaster.do?action=updateUser&id="+ document.getElementById("updateid");
					document.forms[0].submit();
		}
		return true;
	}

	function validFNameonkey() {
		re = /[^a-z,A-Z]/;
		var str = document.forms[0].firstName.value.replace(re, "");
		document.forms[0].firstName.value = str;
	}
	function validQulification() {
		re = /[^A-Za-z\s]/;
		var str = document.forms[0].qualification.value.replace(re, "");
		document.forms[0].qualification.value = str;
	}
	function validLNameonkey() {
		re = /[^a-z,A-Z]/;
		var str = document.forms[0].lastName.value.replace(re, "");
		document.forms[0].lastName.value = str;
	}
	function validemail() {
		re = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
		if (document.forms[0].emailId.value.trim() != "") {
			if (document.forms[0].emailId.value.search(re) == -1) {
				alert("Please Enter valid Email Id");
				document.forms[0].emailId.value = "";
				document.forms[0].emailId.focus();
			}
		}
	}
	function fmtcontacno() {
		re = /\D/g; // remove any characters that are not numbers
		socnum = document.forms[0].phone.value.replace(re, "");
		sslen = socnum.length;
		if (sslen > 3 && sslen < 7) {
			ssa = socnum.slice(0, 3);
			ssb = socnum.slice(3, 6);
			document.forms[0].phone.value = "(" + ssa + ")" + ssb;
		} else {
			if (sslen > 6) {
				ssa = socnum.slice(0, 3);
				ssb = socnum.slice(3, 6);
				ssc = socnum.slice(6, 9);
				ssd = socnum.slice(9, 10);
				document.forms[0].phone.value = "(" + ssa + ")" + ssb + "-"
						+ ssc + ssd;
			} else {
				document.forms[0].phone.value = socnum;
			}
		}
	}

	function fmtmobile() {
		//alert("arg");
		re = /\D/g; // remove any characters that are not numbers
		socnum = document.forms[0].contactno.value.replace(re, "");
		document.forms[0].contactno.value = socnum;
	}
	function pincodOnlyNumbers() {
		//alert("arg");
		re = /\D/g; // remove any characters that are not numbers
		socnum = document.forms[0].pincode.value.replace(re, "");
		document.forms[0].pincode.value = socnum;
	}

	function checkpassword() {
		if (document.forms[0].retypepassword.value.trim() != "") {
			if (document.forms[0].password.value != document.forms[0].retypepassword.value) {
				alert("Passwords does not match");
				document.forms[0].password.value = "";
				document.forms[0].retypepassword.value = "";
				document.forms[0].password.focus();
		
			}
		}
	}

	function resetit() {
		document.forms[0].action = "UserMaster.do";
		document.forms[0].submit();
	}

	function isValidName(uname, name) {
		var letters = /^[A-Za-z ]+$/;
		if (uname.value != null && uname.value != ""
				&& !uname.value.match(letters)) {
			alert('Please enter only alphabets');
			uname.value = "";
			uname.focus();
			return false;
		} else {
			return true;
		}
	}

	function checklengthAsk() {
		if (document.forms[0].contactno.value.trim() != "") {
			if (document.forms[0].contactno.value.length!=10) {
				alert("Mobile Number must contains 10 Numbers !..");
				document.forms[0].contactno.value = "";
				document.forms[0].contactno.focus();
				return false;
			}
		}
	}

	function checklengthAsk1() {
		if (document.forms[0].phone.value.trim()!="") {
			if (document.forms[0].phone.value.length > 15) {
				alert("Contact Number Not Be More Than 15 Numbers !..");
				document.forms[0].phone.value = "";
				document.forms[0].phone.focus();
				return false;
			}
		}
	}

	 function edit(userid,firstName,lastName,gender,emailId,contactno,phone,address,countryid,stateid,
			 cityid,pincode,roleId,qualification,uname,password,otherCountry,otherState,companyID,otherCity) {
		 	document.getElementById('status').innerHTML="";
		 	document.forms[0].userid.value=userid;
			document.forms[0].firstName.value=firstName;
			document.forms[0].lastName.value=lastName;
			document.forms[0].gender.value=gender;
			document.forms[0].emailId.value=emailId;
			document.forms[0].contactno.value=contactno;
			document.forms[0].phone.value=phone;
			document.forms[0].address.value=address;
			document.forms[0].countryid.value=countryid;
			
			getState(countryid,stateid); 
			getCity(stateid,cityid);
			
			//document.forms[0].stateid.value=stateid;
			//document.forms[0].cityid.value=cityid;
			document.forms[0].pincode.value=pincode;
			document.forms[0].roleId.value=roleId;
			document.forms[0].qualification.value=qualification;
			document.forms[0].uname.value=uname;
			document.forms[0].password.value=password;
			document.forms[0].retypepassword.value=password;
			
			document.forms[0].otherCountry.value=otherCountry;
			document.forms[0].otherState.value=otherState;
			document.forms[0].companyID.value=companyID;
			document.forms[0].otherCity.value=otherCity;
			document.forms[0].btn.value="Update";
			document.forms[0].action="UserMaster.do?action=updateUser";

			document.forms[0].firstName.focus();
	}
	 
	 function getState(countryid,selectedId){
			$.ajax({	
				type: "GET",		
				url:"State.do?action=ajaxActiveList",
				data:{countryId:countryid},
				success: function(r){	
					var json= JSON.parse(r);
					//console.log(json);			
					$("#stateid").empty();
					var firstOption= $("<option>",{text:"--Select State--",value:"-1"});
					var otherOption= $("<option>",{text:"Others",value:"0"});
					$("#stateid").append(firstOption);
					$.each(json,function(i,obj){
						var option= $("<option>",{text:obj.state, value:obj.stateId});
						$("#stateid").append(option);
					});
					$("#stateid").append(otherOption);
					if(selectedId!="" && selectedId!=null){
	    				$("#stateid").val(selectedId);
	        		}
					
				} 
			});
	}

	function getCity(stateid,selectedId){
			$.ajax({	
				type: "GET",		
				url:"City.do?action=ajaxActiveList",
				data:{stateId:stateid},
				success: function(r){	
					var json= JSON.parse(r);
					//console.log(json);			
					$("#cityid").empty();
					var firstOption= $("<option>",{text:"--Select City--",value:"-1"});
					var otherOption= $("<option>",{text:"Others",value:"0"});
					$("#cityid").append(firstOption);
					$.each(json,function(i,obj){
						var option= $("<option>",{text:obj.city, value:obj.cityId});
						$("#cityid").append(option);
					});
					$("#cityid").append(otherOption);
					if(selectedId!="" && selectedId!=null){
						$("#cityid").val(selectedId);
					}
					
				} 
			});
	}
	
	function checkUserName(userName) {
		//alert(userName);
		var flag=true;
		$.ajax( {
			url : "studentRegistration.do?action=checkUserName",
			type : "GET",
			dataType : "json",
			data:{userName:userName},
			success : function(res) {
				if(res.flag==1){
					$("#uNameDiv").html(yes);
				  }else if(res.flag==0){
					var notExists='<i style="color:red;" class="fa fa-times fa-lg pr-10"><font style="font-size:12px;color:red;margin-top:2px;">&nbsp;&nbsp;This username already exists, try another username!</font></i>';
					$("#uNameDiv").html(notExists);
					$("#uname").val("");
					$("#password").val("");
					$("#retypepassword").val("");
					$("#passRules").html("");
					$("#passDiv").html("");
					$("#confDiv").html("");
					$("#uname").focus();
					flag=false;
				}
			}
	 	});
		//alert(flag);
		return flag;
	}
</script>
	<script type="text/javascript">
$(document).ready( function() {
	 
$("#countryid").change( function() {
	var countryid=$(this).val();
	if(countryid=="0"){
	}else{
		$("#otherCountry").val("");
		getState(countryid,""); 
	}
});

$("#stateid").change( function() {
	var stateid=$(this).val();
	if(stateid=="0"){
	}else{
		$("#otherState").val("");
		getCity(stateid,"");
	}
});

$("#password").change( function() {
	var decimal=  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;  
	var passwords=$(this);
	if(passwords.val().match(decimal)){
		$("#passRules").html("");
	}else{
		//alert('Password should contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character and min 8 characters');  
		$("#passRules").html('Password should contain at least one lowercase letter, one uppercase letter, one numeric digit and one special character and min 8 characters');
		$("#password").val("");
		passwords.focus();
	}  
});
$("#uname").focusout(function(){
	var userName=$(this).val();
	if($.trim(userName)!=""){
	$.ajax( {
		url : "studentRegistration.do?action=checkUserName",
		type : "GET",
		dataType : "json",
		data:{userName:userName},
		success : function(res) {
			if(res.flag==1){
				 var yes='<font style="font-size:12px;color:green;margin-top:2px;">&nbsp;&nbsp;This username available !</font>';
			  $("#uNameDiv").html(yes);
			  }else if(res.flag==0){
				  var notExists='<font style="font-size:12px;color:red;margin-top:2px;">&nbsp;&nbsp;This username already exists, try another username!</font>';
				$("#uNameDiv").html(notExists);
				$("#uname").val("");
				$("#uname").focus();
			  }
		}
 	});
	}
});
$("#cityid").change( function() {
	var cityid=$(this).val();
	if(cityid=="0"){
	}else{
		$("#otherCity").val("");
	}
});

$("#resetBtn").click( function() {
	$("#countryid").val("-1");
	$("#roleId").val("-1");
	$("#companyID").val("-1")
});

$("#otherCountryAdd").hide();
$("#otherStateAdd").hide();
$("#otherCityAdd").hide();
$("#countryid").val("-1");
$("#companyID").val("-1");
});

</script>
<script type="text/javascript">
(document).ready(function(){
	$("#companyID").val("-1");
	
	
$("#resetBtn").click(function(){ 

			 $.ajax({
				 type: "GET",
			     url: "docSubCat.do?action=clear",
			     success : function (response){
			    		$("#docTypeId").val("");
			    		$("#docCatId").val("");
			    
			    		

			      }
			   });
});
		
	});
</script>
 <style type="text/css">
 input[ type="text"]{
  width: 227px;
 }
 
 input[ type="password"]{
  width: 227px;
 }
 textarea{
  width: 227px;
 }
 </style>
</head>
<body>
	<!-- Start Header -->
	<jsp:include page="/jsp/common/masterdata_header.jsp"></jsp:include>
	<!-- End Header -->

	<!-- Start Main Content -->
	<div class="main" style="min-height: 450px;">
		<h2>User Master</h2>

		<html:form action="/UserMaster.do?action=addUser"	enctype="multipart/form-data">
		<input type="hidden" name="userid" />
			<div align="center" id="status">
				<logic:notEmpty name="status">
					<font color="blue"><bean:write name="status"></bean:write></font>
				</logic:notEmpty>
			</div>

			<logic:present name="duplicateUser">
				<center>
					<font color="red"><bean:write name="duplicateUser" /></font>
				</center>
			</logic:present>

			<table border="0" width="70%" align="center" class="role">
				 
				<tr height="40">

					<td><label>First Name <font style="color: red;">*</font></label></td>
					<td><input type="text" name="firstName"
						onkeyup="isValidName(this)" maxlength="30" autofocus/></td>
					<td><label>Last Name</label></td>
					<td><input type="text" name="lastName"
						onkeyup="isValidName(this)" maxlength="30" /></td>

				</tr>
				<tr height="40">


					<td><label>Gender</label></td>
					<td><select name="gender">
							<option value="-1">Select</option>
							<option value="Male">Male</option>
							<option value="Female">Female</option>
					</select></td>
					
				</tr>
				<tr height="40">
					
					<td><label>Mobile No <font style="color: red;">*</font></label></td>
					<td><input type="text" name="contactno" maxlength="10"	 onBlur="checklengthAsk()"
						onkeyup="fmtmobile();" /></td>
						<td><label>Telephone No.</label></td>
					<td><input type="text" name="phone"	onkeyup="fmtcontacno();" onBlur="checklengthAsk1()" 	maxlength="15" /></td>
						
				</tr>
				<tr height="40">


					
				<td rowspan="2"><label>Address</label></td>
					<td  rowspan="2" ><textarea rows="2" cols="17" style="resize: none" maxlength="250"
							name="address"></textarea></td>
							<td><label>Role <font style="color: red;">*</font></label></td>
					<td><html:select property="roleId" styleId="roleId">
							<html:option value="-1">-Select-</html:option>
							<html:options collection="roleList" property="roleId"
								labelProperty="rolename" />
						</html:select></td>
				</tr>
				
				<tr height="2">
					
				
				</tr>
				
				
				<tr height="40">
					<td><label>Country <font style="color: red;">*</font></label></td>
					<td><html:select property="countryid" styleId="countryid"  styleClass="dropdown-select" >
													<html:option value="-1">--Select Country--</html:option>
													<html:options collection="countryList" property="countryid" labelProperty="countryname" />
													<html:option value="0">others</html:option>
												</html:select>
					</td>
					<td valign="top"><label>State <font style="color: red;">*</font></label></td>
					<td><select name="stateid" id="stateid" class="dropdown-select" >
													<option value="-1">-Select State-</option>
													<option value="0">Others</option>
												</select></td>
				</tr>
				
				<tr>
				
					<td><label>Other Country </label></td>
					<td><input type="text" id="otherCountry" name="othercountryname"   onkeyup="isValidName(this)" maxlength="100"  />
					</td>
			   
					<td valign="top"><label>Other State </label></td>
					<td><input type="text" id="otherState" name="otherstatename"  onkeyup="isValidName(this)" maxlength="100"  /></td>
				</tr>
				
				<tr height="40">
					<td><label>City <font style="color: red;">*</font></label></td>
					<td>	<select name="cityid" id="cityid"  class="dropdown-select">
													<option value="-1">-Select City-</option>
													<option value="0">Others</option>
												</select>
					</td>
					<td valign="top"><label>Pincode <font style="color: red;">*</font></label></td>
					<td><input type="text" id="pincode" name="pincode"  onkeyup="pincodOnlyNumbers();" maxlength="6"  /></td>
				</tr>
				 
				 <tr height="40">
					<td><label>Other City </label></td>
					<td><input type="text" id="otherCity" name="othercityname"  onkeyup="isValidName(this)" maxlength="100"  />
					</td>
					
						<td><label>Company <font style="color: red;">*</font></label></td>
					<td><html:select property="companyID">
							<html:option value="-1">-Select-</html:option>
							<html:options collection="companyList" property="companyID"
								labelProperty="companyName" />
						</html:select></td>
					
				</tr>
				<tr height="40">
					<td><label>Qualification </label></td>
					<td><input type="text" name="qualification" maxlength="50" onkeyup="validQulification()"  /></td>
					<td><label>Email-id <font style="color: red;">*</font></label></td>
					<td><input type="text" name="emailId" onblur="validemail();" maxlength="50" /></td>

				</tr>
				<tr height="40">
					<td><label>Password <font style="color: red;">*</font> </label></td>
					<td><input name="password" type="password" id="password"	maxlength="25"   /><a style="font-size: 10px;font-style: italic;color: red;margin-left: 135px;" title="Password should contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character and min 8 characters" >Rules for Password</a></td>
					<td><label>Confirm Password <font style="color: red;">*</font></label></td>
					<td><input name="retypepassword" type="password" maxlength="25" onblur="checkpassword()" /></td>

				</tr>
				
				<tr height="10" >
					<td colspan="4"><div class="col-md-12 col-sm-12" id="passRules" style="height: 30px;font-size: 12px;font-style: italic;color: red;" ></div></td>
				</tr>
				<logic:present name="userexist">
					<tr>
						<td colspan="4" align="left"><font color="red"><bean:write
									name="userexist" /></font></td>
					</tr>
				</logic:present>
				<tr height="30">
					<td colspan="4" align="center"><input type="submit"
						name="btn" value="Save" onclick="return validate()"
						style="padding: 0.5% 1%;" /> <input type="reset" value="Reset" id="resetBtn"
						style="padding: 0.5% 1%;" /></td>
				</tr>

			</table>

		</html:form>
		

		
		<div class="dataGridDisplay"  >
			<table id="normalTable" class="display"  cellspacing="0" width="100%" align="center" >
				<thead>
				<tr>
					<th>SI No.</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Mobile</th>
					<th>Role</th>
					<th>Status</th>
					<th style="width: 155px;">Action</th>
				</tr>
				</thead>
 				<tbody>
 				<logic:notEmpty name="usersList">
				<logic:iterate id="user" name="usersList" indexId="index">
					<tr>
						<td><%=index + 1%></td>
						<td><bean:write name="user" property="firstName" /></td>
						<td><bean:write name="user" property="lastName" /></td>
						<td><bean:write name="user" property="contactno" /></td>

						<td><bean:write name="user" property="rolename" /></td>
						<td><logic:equal name="user" property="active" value="true">
								<font color="green">active</font>
							</logic:equal> <logic:equal name="user" property="active" value="false">
								<font color="red">inactive</font>
							</logic:equal></td>
						<td><logic:equal name="user" property="active" value="true">
								<a
									href='UserMaster.do?action=changestatus&amp;userid=<bean:write name="user" property="userid"/>&amp;active=false'><input
									type="button" value="Remove"></a>
			&nbsp;&nbsp;
			<logic:equal value="2" property="roleId" name="user">
									<!-- 	<a
						href="CouchCourse.do?action=list&stdId=<bean:write name="user" property="userid"/> "><input
						type="button" value="Course Allotment" /></a> -->
								</logic:equal>
								<logic:equal value="3" property="roleId" name="user">

								</logic:equal>
								<a	href="#" onclick='edit("<bean:write name="user" property="userid" />",
								"<bean:write name="user" property="firstName" />",
								"<bean:write name="user" property="lastName" />",
								"<bean:write name="user" property="gender" />",
								"<bean:write name="user" property="emailId" />",
								"<bean:write name="user" property="contactno" />",
								"<bean:write name="user" property="phone" />",
								"<bean:write name="user" property="address" />",
								"<bean:write name="user" property="countryid" />",
								"<bean:write name="user" property="stateid" />",
								"<bean:write name="user" property="cityid" />",
								"<bean:write name="user" property="pincode" />",
								"<bean:write name="user" property="roleId" />",
								"<bean:write name="user" property="qualification" />",
								"<bean:write name="user" property="uname" />",
								"<bean:write name="user" property="password" />",
								"<bean:write name="user" property="othercountryname" />",
								"<bean:write name="user" property="otherstatename" />",
								"<bean:write name="user" property="companyID" />",
								"<bean:write name="user" property="othercityname" />")'>	<input type="button" value="Edit"></a>
							
							</logic:equal> <logic:equal name="user" property="active" value="false">
								<a  href='UserMaster.do?action=changestatus&amp;userid=<bean:write name="user" property="userid"/>&amp;active=true'><input	type="button" value="Active"></a>
							</logic:equal></td>
					</tr>
				</logic:iterate>
				</logic:notEmpty>
				</tbody>
				
			</table>
			<logic:notEmpty name="usersList">
			<div align="center"><a href="UserMaster.do?action=excelReport"><button>Excel Export</button></a></div>
			</logic:notEmpty>
</div>
</div>
		
		
	
	 
	<!-- End Main Content -->

	<!-- Start Footer -->
	<jsp:include page="/jsp/common/masterdata_footer.jsp"></jsp:include>
	<!-- End Footer -->

	<script type="text/javascript">
	
	</script>

	<!-- End Footer -->
	
	 <script src="js/commonFunction.js"  type="text/javascript"></script>

</body>
</html>




