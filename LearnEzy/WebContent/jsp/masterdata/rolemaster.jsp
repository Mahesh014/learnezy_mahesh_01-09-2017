
<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Itech Training ::  Role Master</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300' rel='stylesheet' type='text/css'>
<link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/cmsstyle.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/carousel.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>

<script src="<%=request.getContextPath() %>/js/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/owl.carousel.js" type="text/javascript"></script>

<link href="dataTable/css/jquery.dataTables_themeroller.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.min.css"	rel="stylesheet" type="text/css" media="all" />
<script src="dataTable/js/jquery.dataTables.js"	type="text/javascript"></script>
<script src="dataTable/js/jquery.dataTables.min.js"	type="text/javascript"></script>
<script src="dataTable/js/settingDataTable.js" 	type="text/javascript"></script>
<script type="text/javascript">
window.history.forward();
function noBack() { window.history.forward(); }

 function edit(id,desc){
	 	document.getElementById("status").innerHTML="";
		document.roleForm.action="Role.do?action=update";
		document.roleForm.roleid.value=id;
		document.roleForm.rolename.value=desc;
		 document.roleForm.rolename.focus();
		document.roleForm.btn.value="Update";
	}
 function focus2()
 {

 document.getElementById('status').innerHTML="";
 document.roleForm.roleid.value="";
 document.roleForm.action="Role.do?action=add";
 document.roleForm.btn.value="Save";
 document.roleForm.rolename.focus();
 document.getElementById('status').innerHTML="";
 }

 function validate(){
	   
		if(document.forms[0].rolename.value.trim()==""){
			alert("Please enter Role");
			document.forms[0].rolename.focus();
			return false;
		}

		if (!document.forms[0].rolename.value.match(/^[a-zA-Z ]*$/) && document.forms[0].rolename.value !="")
	    {
	 	   document.forms[0].rolename.value="";
	 	   document.forms[0].rolename.focus(); 
	       alert("Please Enter only Alphabets");
	       return false;   
	    }
	    
	return true;
 }

 $(window).load(function(){

	  $('input[name=rolename]').focus();

	});
 
</script>
<script type="text/javascript">
    function checkchacter(inputtxt) {
		//alert("arg");
			re = /[^a-z,A-Z, ]/; // remove any characters that are not numbers
		socnum = inputtxt.value.replace(re, "");
		inputtxt.value = socnum;
	}
    </script>
</head>
<body>
 
 <jsp:include page="/jsp/common/masterdata_header.jsp"></jsp:include>
 
   <!-- Start Main Content -->
	 <div class="main" style="min-height:450px;">	 
	 	<h2>Role Master</h2>
    <html:form action="/Role.do?action=add">
    <input type="hidden"	name="roleid" />
    <div id="status" align="center"><logic:notEmpty name="status">
					<font color="blue"><bean:write name="status"></bean:write></font>
				</logic:notEmpty></div>
     <table class="role" width="600">
     <tr>
     
     <td>Role Name:</td>
     <td><input type="text"  name="rolename" size="25" maxlength="50" onkeyup="return checkchacter(this);"></td>
     <td><input type="submit" name="btn" onclick="return validate();" value="Save"> 
     <input type="reset" value="Reset" onclick="focus2()"></td>
     </tr>
     </table>
     </html:form>
     
     	
<div class="dataGridDisplay"  >
   <table id="normalTable" class="display" cellspacing="0" width="100%" align="center" >
				<thead>
    <tr>
    <th>SI No.</th>
    <th>Role Name</th>
    <th style="width:100px;" >Status</th>
    <th style="width:155px;">Action</th>
    </tr>
    </thead>
    <tbody>
    <logic:notEmpty name="roleList">
    
    	<logic:iterate id="userrole" name="roleList" indexId="index">
    <tr>
    <td><%=index+1 %></td>
    <td><bean:write name="userrole" property="rolename" /></td>
    <td>
    
    <logic:equal name="userrole"
							property="active" value="true">
							<font color="green">Active</font>
						</logic:equal> <logic:equal name="userrole" property="active" value="false">
							<font color="red">Inactive</font>
						</logic:equal>
    
    </td>
    
    <td>
    <logic:equal name="userrole"
							property="active" value="true">
							<a style="text-decoration: none;"
								href='Role.do?action=changestatus&amp;roleid=<bean:write name="userrole" property="roleid"/>&amp;active=false'> <input type="button" value="Remove"></a>
								&nbsp; <a href='#'
								onclick='edit("<bean:write name="userrole" property="roleid"/>","<bean:write name="userrole" property="rolename"/>")'><input type="button" value="Edit"></a>
						</logic:equal> <logic:equal name="userrole" property="active" value="false">
							<a style="text-decoration: none;"
								href='Role.do?action=changestatus&amp;roleid=<bean:write name="userrole" property="roleid"/>&amp;active=true'> <input type="button" value="Active"></a>
						</logic:equal>
    
</td>
    </tr>
  
    
    </logic:iterate>
     </logic:notEmpty>
  </tbody>
    
    </table>
     <logic:notEmpty name="roleList">
    <div align="center"><a href="Role.do?action=excelReport"><button>Excel Export</button></a></div>
    </logic:notEmpty>
   </div>
    
    
     </div>
     
   <!-- End Main Content -->
	   <jsp:include page="/jsp/common/masterdata_footer.jsp"></jsp:include> 
 <script src="js/commonFunction.js"  type="text/javascript"></script>
  </body>
</html>

