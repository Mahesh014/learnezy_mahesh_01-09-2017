<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-nested.tld" prefix="nested"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Itech Training :: Link Master</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="icon" type="image/png" href="<%=request.getContextPath() %>/images/logo.png"></link>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="UI_style.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath() %>/css/stylesheet_tms.css"	rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath() %>/css/search_data.css"	rel="stylesheet" type="text/css" /> 
<link href="<%=request.getContextPath() %>/css/style1.css"	rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/cmsstyle.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />

<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>
<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>

<link href="dataTable/css/jquery.dataTables_themeroller.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.min.css"	rel="stylesheet" type="text/css" media="all" />
<script src="dataTable/js/jquery.dataTables.js"	type="text/javascript"></script>
<script src="dataTable/js/jquery.dataTables.min.js"	type="text/javascript"></script>
<script src="dataTable/js/settingDataTable.js" 	type="text/javascript"></script>

<!-- Text Editor -->
<script type="text/javascript">
	function valueCall(){
		if(document.forms[0].linkName.value == "" )	{
			alert("Please Enter the Link NameL ..!");
			document.forms[0].linkName.focus();
			return false;
		}
        var pattern = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
		
		if(document.forms[0].URL.value == "" )	{
			alert("Please Enter the URL ..!");
			document.forms[0].URL.focus();
			return false;
		}
		else if (pattern.test(document.forms[0].URL.value)) {
		           
		            return true;
		        } 
		else
		{
		            alert("Url is not valid!");
		            document.forms[0].URL.value ="";
					document.forms[0].URL.focus();
		            
		            return false;

		    }


			
		
	}
	
	function edit(linkID,URL,linkName){
		document.forms[0].linkID.value =linkID;
		document.forms[0].linkName.value =linkName;
		document.forms[0].URL.value =URL;
		document.forms[0].action="LinkMaster.do?action=update";
		document.forms[0].btn.value="Update";
		document.forms[0].URL.focus();
	}
	
function listMetodCall()
{
	document.forms[0].action="LinkMaster.do?action=list";
	document.forms[0].submit();
}
</script>

<script>
    $(document).ready(function() {
        
       $("#resetBtn").click(function(){
         	$("#courseId").val("-1");
 		});
    });
    </script>
    
    <script type="text/javascript">
    function checkchacters(inputtxt){

    	re=/[^a-z,A-z, ]/;
    	socnum = inputtxt.value.replace(re,"");
    	inputtxt.value = socnum;
    }
    
    </script>
</head>
<body>
  <jsp:include page="/jsp/common/masterdata_header.jsp"></jsp:include>
   
   <!-- Start Main Content -->
	 <div class="main" style="min-height:450px;">	 
	 	<h2>Links Master</h2>
    <div id="status" align="center"><logic:notEmpty name="status">
					<font color="blue"><bean:write name="status"></bean:write></font>
				</logic:notEmpty></div>
   <html:form action="/LinkMaster.do?action=add" >
    <input type="hidden" name="linkID"	 />
    <table border="0" width="60%" align="center"  class="role"  >
			 
		<tr height="30" >
			<td ><label>Enter Link Name *</label></td><td>
			<input type="text" name="linkName"  id="linkName"   maxlength="50" onkeyup="return checkchacters(this);"/></td>
			
			</tr>
			<tr height="50">
			<td ><label>Enter URL *</label></td>
			<td><input type="text" name="URL"  id="URL"   maxlength="50" style="width: 400px;"/></td>
			
			
		
	   </tr>
	
		<tr height="50">
			<td colspan="6" align="center" >
			<input type="submit" name="btn" value="Submit" onclick="return valueCall();" style="padding:0.5% 1%;" />
			<input type="reset"	value="Reset" style="padding:0.5% 1%;" id="resetBtn"/> 
		</tr>

	</table>
     </html:form> 	
		
 	<div class="dataGridDisplay"  >
   <table id="normalTable" class="display" cellspacing="0" width="100%" align="center" >
				<thead>
    <tr>
    <th>SI No.</th>
    <th>Link Name</th>
    <th>URL</th>
    <th>Status</th>
    <th style="width:155px;">Action</th>
    </tr>
    </thead>
    <tbody>
    <logic:notEmpty name="linkList">
    <logic:iterate id="link" name="linkList" indexId="index">
    <tr>
    <td><%=index+1 %></td>
    <td><bean:write name="link"	property="linkName" /></td>
   	<td><a href="<bean:write name="link"	property="URL" />"><bean:write name="link"	property="URL" /></a></td>
   	<td><logic:equal name="link" property="active"	value="true">
			<font color="green">Active</font>
		</logic:equal> 
		<logic:equal name="link" property="active" value="false">
			<font color="red">InActive</font>
		</logic:equal>
</td>

    <td>
    
    <logic:equal name="link" property="active"	value="true">
			<a href='LinkMaster.do?action=changestatus&amp;linkID=<bean:write name="link" property="linkID"/>&amp;active=false'><input type="button" value="Remove"></a>
			&nbsp;&nbsp;
			<a href="#" onclick='edit("<bean:write name="link" property="linkID"/>",
									  "<bean:write name="link" property="URL"/>",
									  "<bean:write name="link" property="linkName"/>")'><input type="button" value="Edit"></a>
	</logic:equal> 
	<logic:equal name="link" property="active" value="false">
			<a href='LinkMaster.do?action=changestatus&amp;linkID=<bean:write name="link" property="linkID"/>&amp;active=true'><input type="button" value="Active"></a>
	</logic:equal>
   
    
</td>
    </tr>
   
 </logic:iterate>
  </logic:notEmpty>
  </tbody>
    </table><logic:notEmpty name="linkList">
    <div align="center"><a href="LinkMaster.do?action=excelReport"><button>Excel Export</button></a></div>
    </logic:notEmpty>
    </div>
	 
     </div>
    
   <!-- End Main Content -->
	   
 <jsp:include page="/jsp/common/masterdata_footer.jsp"></jsp:include>
  <script type="text/javascript">
	document.forms[0].linkName.focus();
	</script> 
	<script src="js/commonFunction.js"  type="text/javascript"></script>
  </body>
</html>




    	
    	
            