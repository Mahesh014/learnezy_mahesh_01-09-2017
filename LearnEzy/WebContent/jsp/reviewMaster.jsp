<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld"	prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%> 

    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript"  src="<%=request.getContextPath()%>/js/validation/jquery-1.9.1.js"></script>
<script type="text/javascript"  src="<%=request.getContextPath()%>/js/validation/jquery.validate.min.js"></script>
<script type="text/javascript"  src="<%=request.getContextPath()%>/js/validation/additional-methods.js"></script>
<script type="text/javascript">

$(document).ready(function(){
	//alert();
	
		$.ajax( {
		url : "Student.do?action=courseList",
		type : "GET",
		dataType : "json",
		async:false,
		success : function(res){
			//alert("2");
		var firstOption= $("<option>",{text:"--------------------------------SELECT--------------------------------",value:""});
		$("#courseList").append(firstOption);
		$.each(res,function(i,obj){
			//alert("2");
				  var option=$("<option>",{text:obj.courseName,value:obj.courseId});
				$("#courseList").append(option);
				//alert("3");
			});
		 	
	 	}
 	});
	
	
});

</script>
<script type="text/javascript">
$(document).ready(function(){

$("#review-from").validate({

	
	rules:{

	courseList:{required:true}
	},
	messages:{
		courseList:{required:"Please select course"}
		}
});
	
});

</script>
</head>
<body>

<h1>Review Master</h1>

<logic:notEmpty name="status">
						<font color="red"><bean:write name="status"></bean:write></font>
					</logic:notEmpty>
<html:form action="review.do?action=add"  styleId="review-from">
<table border="1">

<tr>

<td> Course:<select name="courseList"id="courseList" ></select></td>
<td> Description:<textarea  name="reviewDescription" id="reviewDescription" ></textarea></td>
</tr>



<tr>
<td>
<input type="submit"  value="Submit"></input>
<input type="reset"  value="Reset"></input>
</td>
</tr>
</table>




</html:form>


</body>
</html>