 <!-- Start Footer -->
     
      
			 <div class="footer-bottom">
			 	<div class="wrap">
			 	<div class="copy-right">
			 		<p>Itech Training, Copyright 2017, All Rights Reserved. Design & Developed by   <a href="http://www.itechsolutions.in/" target="_blank">Itech Solutions</a></p>
			 	</div>
			 	<div class="social-icons">
			 		<ul>
			 			<li><a target="_blank" href="https://www.twitter.com"><i
							class="fa fa-twitter"></i></a></li>
					<li><a target="_blank" href="https://www.facebook.com"><i
							class="fa fa-facebook"></i></a></li>
					<li><a target="_blank" href="https://plus.google.com"><i
							class="fa fa-google-plus"></i></a></li>
					<li><a target="_blank" href="https://www.youtube.com"><i
							class="fa fa-youtube"></i></a></li>
					<li><a target="_blank" href="https://www.linkedin.com"><i
							class="fa fa-linkedin"></i></a></li>
			 		</ul>
			 	</div>
			 	<div class="clear"></div>
			 </div>
	       </div>
  <!-- End Footer -->