<!-- HEADER -->
    <header class="header">
      <div class="top-header">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <!--<div class="top-header__contacts"><i class="icon stroke icon-Phone2"></i>(123) 0800 12345</div>-->
              <div class="top-header__contacts"><i class="icon stroke icon-Mail"></i><a href="mailto:Info@learnezy.com">Info@learnezy.com</a></div>
              <div class="top-header__link">
                <button class="btn-header btn-effect">LATEST</button>
                <span>We have added new courses today ...</span> </div>
              <div class="header-login"> <a class="header-login__item" href="sign_in.jsp"><i class="icon stroke icon-User"></i>Sign In</a> <a class="header-login__item" href="registernew.jsp">Register</a> </div>
            </div>
            <!-- end col  --> 
          </div>
          <!-- end row  --> 
        </div>
        <!-- end container  --> 
      </div>
      <!-- end top-header  -->
      
      <div class="container">
        <div class="row">
          <div class="col-xs-12"> <a class="header-logo" href="dynamic.jsp"><img class="header-logo__img" src="assets/img/logo.png" alt="Logo"></a>
            <div class="header-inner">
              <div class="header-search">
                <div class="navbar-search">
                  <form id="search-global-form">
                    <div class="input-group">
                      <input type="text" placeholder="Type your search..." class="form-control" autocomplete="off" name="s" id="search" value="">
                      <span class="input-group-btn">
                      <button type="reset" class="btn search-close" id="search-close"><i class="fa fa-times"></i></button>
                      </span> </div>
                  </form>
                </div>
              </div>
              <a id="search-open" href="#fakelink"><i class="icon stroke icon-Search"></i></a> <a class="header-cart" href="/"> <i class="icon stroke icon-ShoppingCart"></i></a>
               <nav class="navbar yamm">
                <div class="navbar-header hidden-md  hidden-lg  hidden-sm ">
                  <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                </div>
                <div id="navbar-collapse-1" class="navbar-collapse collapse">
                  <ul class="nav navbar-nav">
                    <li class="dropdown"><a href="dynamic.jsp">Home<span class="nav-subtitle">OUR World</span></a>
                     
                    </li>
                   
                    <li><a href="contact-us.jsp">CONTACT<span class="nav-subtitle">say us hi</span></a></li>
                  </ul>
                </div>
              </nav>
              <!--end navbar --> 
              
            </div>
            <!-- end header-inner --> 
          </div>
          <!-- end col  --> 
        </div>
        <!-- end row  --> 
      </div>
      <!-- end container--> 
    </header>
    <!-- end header -->