<!-- Start Footer -->

 <footer class="footer wow fadeInUp" data-wow-duration="2s">
      <div class="container">
        <div class="footer-inner">
          <div class="row">
            <div class="col-lg-3 col-sm-3">
              <section class="footer-section courses">
                <h3 class="footer-title">ABOUT US</h3>
                <a href="javascript:void(0);"><img class="footer-logo img-responsive" src="assets/img/logo_white.png"  alt="Logo"></a>
                <div class="footer-info">Fusce eleifend. Donec sapien sed pha seah lusa. Pellentesque lu amus lorem arcu sem per duiac. Vivamus porta. Sed at nisl praesnt blandit mauris vel erat.</div>
                <!--<div class="footer-contacts footer-contacts_mod-a"> <i class="icon stroke icon-Pointer"></i>
                  <address class="footer-contacts__inner">
                  370 Hill Park, Florida, USA
                  </address>
                </div>-->
                <!--<div class="footer-contacts"> <i class="icon stroke icon-Phone2"></i> <span class="footer-contacts__inner">Call us 0800 12345</span> </div>-->
                <div class="footer-contacts"> <i class="icon stroke icon-Mail"></i> <a class="footer-contacts__inner" href="mailto:Info@learnezy.com">Info@learnezy.com</a> </div>
              </section>
              <!-- end footer-section --> 
            </div>
            <!-- end col -->
            
            <div class="col-lg-2 col-sm-3">
              <section class="footer-section courses">
                <h3 class="footer-title">USEFUL LINKS</h3>
                <ul class="footer-list list-unstyled">
                  <li class="footer-list__item"><a class="footer-list__link" href="javascript:void(0);">Academies</a></li>
                  <li class="footer-list__item"><a class="footer-list__link" href="javascript:void(0);">Our Latest Courses</a></li>
                  <li class="footer-list__item"><a class="footer-list__link" href="javascript:void(0);">Who We Are</a></li>
                  <li class="footer-list__item"><a class="footer-list__link" href="javascript:void(0);">Get In Touch</a></li>
                  <li class="footer-list__item"><a class="footer-list__link" href="javascript:void(0);">Courses Categories</a></li>
                  <li class="footer-list__item"><a class="footer-list__link" href="javascript:void(0);">Support & FAQ's</a></li>
                  <li class="footer-list__item"><a class="footer-list__link" href="javascript:void(0);">Terms & Conditions</a></li>
                  <li class="footer-list__item"><a class="footer-list__link" href="javascript:void(0);">Privacy Policy</a></li>
                </ul>
              </section>
              <!-- end footer-section --> 
            </div>
            <!-- end col -->
            
            <div class="col-lg-3 col-sm-3">
              <section class="footer-section courses">
                <h3 class="footer-title">LATEST TWEETS</h3>
                <div class="tweets">
                  <div class="tweets__text">What is the enemy of #creativity?</div>
                  <div><a href="javascript:void(0);">http://enva.to/hVl5G</a></div>
                  <span class="tweets__time">9 hours ago</span> </div>
                <div class="tweets">
                  <div class="tweets__text">An agile framework can produce the type of lean marketing essential for the digital age <a href="javascript:void(0);">@aholmes360 #IMDS15</a></div>
                  <span class="tweets__time">9 hours ago</span> </div>
                <a class="tweets__link" href="javascript:void(0);">Follow @Learnezy</a> </section>
              <!-- end footer-section --> 
            </div>
            <!-- end col -->
            
            <div class="col-lg-4 col-sm-3">
              <section class="footer-section">
                <h3 class="footer-title">QUICK CONTACT</h3>
                <form  action="DynamicData.do?action=enquiry1" method="post" class="form">
                  <div class="form-group">
                  
                  <input >
                    <input class="form-control" type="text"   minlength="3"  maxlength="30"  placeholder="Your Name" pattern="[a-zA-Z\s]+" title="Enter only letters" name="txtname" required>
                    <input class="form-control" type="email"  maxlength="50" placeholder="Email address" name="txtemail" required>
                  <input class="form-control" type="text"  maxlength="12" minlength="10" placeholder="Mobile Number" name="contactnumber"  pattern="[0-9]{10}" title="Enter only Numbers"  required>
                    
                    <textarea class="form-control" rows="7"  maxlength="100" placeholder="Message" name="textarea-message"></textarea>
                    <button type="submit" class="btn btn-primary btn-effect">SEND MESSSAGE</button>
                  </div>
                </form>
              </section>
              <!-- end footer-section --> 
            </div>
            <!-- end col --> 
          </div>
          <!-- end row --> 
        </div>
        <!-- end footer-inner -->
        
        <div class="row">
          <div class="col-xs-12">
            <div class="footer-bottom">
              <div class="copyright">Copyright � 2016 <a href="javascript:void(0);">LearnEzy</a>, Online Learning  |  Designed &amp; Developed by <a href="http://www.itechsolutions.in">Itech Solutions</a></div>
              <ul class="social-links list-unstyled">
                <li><a class="icon fa fa-facebook" href="javascript:void(0);"></a></li>
                <li><a class="icon fa fa-twitter" href="javascript:void(0);"></a></li>
                <li><a class="icon fa fa-google-plus" href="javascript:void(0);"></a></li>
                <li><a class="icon fa fa-instagram" href="javascript:void(0);"></a></li>
                <li><a class="icon fa fa-linkedin" href="javascript:void(0);"></a></li>
                <li><a class="icon fa fa-youtube-play" href="javascript:void(0);"></a></li>
              </ul>
            </div>
            <!-- end footer-bottom --> 
          </div>
          <!-- end col --> 
        </div>
        <!-- end row --> 
      </div>
      <!-- end container --> 
    </footer>
     
      
		
		 
	
<!-- End Footer -->