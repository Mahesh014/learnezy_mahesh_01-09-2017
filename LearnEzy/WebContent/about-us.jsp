<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="robots" content="INDEX, FOLLOW">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui">
	<meta name="description" content="An Online Study And Teaching Platform For Students As Well As Institutes, initial-scale=1, maximum-scale=1, minimal-ui">
		<title>About Us | LearnEZY</title>
<link rel="canonical" href="www.learnezy.com/about-us.jsp"/>		
		<link href="favicon.png" type="image/x-icon" rel="shortcut icon">
		<link href="assets/css/master.css" rel="stylesheet">

<script src="assets/plugins/jquery/jquery-1.11.3.min.js"></script>

</head>

<body>

<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->
		<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200">


			<div id="wrapper">

				<jsp:include page="Commonheader.jsp"></jsp:include>

				<div class="wrap-title-page">
					<div class="container">
						<div class="row">
							<div class="col-xs-12"><h1 class="ui-title-page">About Learnezy</h1></div>
						</div>
					</div><!-- end container-->
				</div><!-- end wrap-title-page -->


				<div class="section-breadcrumb">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div class="wrap-breadcrumb clearfix">
									<ol class="breadcrumb">
										<li><a href="index.jsp"><i class="icon stroke icon-House"></i></a></li>
										<li class="active">About Us</li>
									</ol>
								</div>
							</div>
						</div><!-- end row -->
					</div><!-- end container -->
				</div><!-- end section-breadcrumb -->


				<main class="main-content">

					<section class="about rtd">
						<div class="container">
							<div class="row">
								<div class="col-md-5">
									<h2 class="about__title">welcome to <strong>Learnezy</strong></h2>
									<p>Reprehenderit in voluptate velit esse cillum dolo reu fugiat nulla pariatu excp teur sint occa ecated cupidatat non pried ent sunla in asculpa quiah officias deserunt mollit anim. Est laborum sedu perspiciatis unde omnis iste natuser asor lorem ipsum dolor sit amet.</p>
									<h3 class="about__title-inner">We Offer Everyone <strong>FREE Online Courses !</strong></h3>
									<ul class="list-mark">
										<li>Pellentesque lacus vamus lorem arcu semper duiac</li>
										<li>Cras ornare arcu avamus nda leo etiam ind arcu morjusto mauris</li>
										<li>Pellentesque lacus vamus lorem arcu semper duiac ras ornare arcu</li>
										<li>Bamus nda leo etiam ind arcu morbi justo mauris tempus</li>
										<li>Aare arcu avamus nda leo etiam ind arcu lorem ipsum dolor sit</li>
									</ul>
									<p>Reprehenderit in voluptate velit esse cillum dolo reu fugiat nulla pariatu excp teur sint occa ecated cupidatat non pried ent sunla.</p>
								</div><!-- end col -->
								<div class="col-md-7">
									<img class="img-responsive" src="assets/media/about/670x350/1.jpg" height="350" width="670" alt="Foto">
									<div class="row">
										<div class="col-xs-6">
											<img class="img-responsive" src="assets/media/about/320x200/1.jpg" height="200" width="320" alt="Foto">
										</div>
										<div class="col-xs-6">
											<img class="img-responsive" src="assets/media/about/320x200/2.jpg" height="200" width="320" alt="Foto">
										</div><!-- end col -->
									</div><!-- end row -->

								</div><!-- end col -->
							</div><!-- end row -->
						</div><!-- end container -->
					</section><!-- end about -->


					<div class="section-advantages_mod-a">
						<div class="container">
							<div class="row">
								<div class="col-xs-12">
									<ul class="advantages advantages_mod-c list-unstyled">
										<li class="advantages__item">
											<span class="advantages__icon decor decor_mod-a"><i class="icon stroke icon-Users"></i></span>
											<span class="advantages__title ui-title-inner">SKILLED FACULTY</span>
										</li>
										<li class="advantages__item">
											<span class="advantages__icon decor decor_mod-a"><i class="icon stroke icon-Cup"></i></span>
											<span class="advantages__title ui-title-inner">HIGHest RATED</span>
										</li>
										<li class="advantages__item">
											<span class="advantages__icon decor decor_mod-a"><i class="icon stroke icon-WorldGlobe"></i></span>
											<span class="advantages__title ui-title-inner">GLOBALLY RECOGNIZED</span>
										</li>
										<li class="advantages__item">
											<span class="advantages__icon decor decor_mod-a"><i class="icon stroke icon-DesktopMonitor"></i></span>
											<span class="advantages__title ui-title-inner">ONLINE TRAINING</span>
										</li>
									</ul>
								</div><!-- end col -->
							</div><!-- end row -->
						</div><!-- end container -->
					</div><!-- end section-advantages -->


					<div class="container">
						<div class="row">
							<div class="col-md-6">
								<section class="section-default">
									<h2 class="ui-title-block">Why <strong>Learnezy ?</strong></h2>
									<div class="panel-group accordion accordion" id="accordion-1">
										<div class="panel panel-default">
											<div class="panel-heading">
												<a class="btn-collapse" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-1"><i class="icon"></i></a>
												<h3 class="panel-title">Learn And Get Training From Experts</h3>
											</div>
											<div id="collapse-1" class="panel-collapse collapse in">
												<div class="panel-body">
													<p class="ui-text">Fusce eleifend. Donec sapien sed phase lusa . Pellentesque lacus.Vivamus lorem arcu semper duiac. Cras ornare arcu vamus nda leo. Etiam ind arc morbi justo mauris temd pus pharetra interdum at congue semper purus.</p>
												</div>
											</div>
										</div><!-- end panel -->

										<div class="panel">
											<div class="panel-heading">
												<a class="btn-collapse collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-2"><i class="icon"></i></a>
												<h3 class="panel-title">Enjoy Our Free Online Courses</h3>
											</div>
											<div id="collapse-2" class="panel-collapse collapse">
												<div class="panel-body">
													<p class="ui-text">Fusce eleifend. Donec sapien sed phase lusa . Pellentesque lacus.Vivamus lorem arcu semper duiac. Cras ornare arcu vamus nda leo. Etiam ind arc morbi justo mauris temd pus pharetra interdum at congue semper purus.</p>
												</div>
											</div>
										</div><!-- end panel -->

										<div class="panel">
											<div class="panel-heading">
												<a class="btn-collapse collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-3"><i class="icon"></i></a>
												<h3 class="panel-title">Learn Anytime & Anywhere</h3>
											</div>
											<div id="collapse-3" class="panel-collapse collapse">
												<div class="panel-body">
													<p class="ui-text">Fusce eleifend. Donec sapien sed phase lusa . Pellentesque lacus.Vivamus lorem arcu semper duiac. Cras ornare arcu vamus nda leo. Etiam ind arc morbi justo mauris temd pus pharetra interdum at congue semper purus.</p>
												</div>
											</div>
										</div><!-- end panel -->

										<div class="panel">
											<div class="panel-heading">
												<a class="btn-collapse collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-4"><i class="icon"></i></a>
												<h3 class="panel-title">Basic to Advance: We Teach Everything</h3>
											</div>
											<div id="collapse-4" class="panel-collapse collapse">
												<div class="panel-body">
													<p class="ui-text">Fusce eleifend. Donec sapien sed phase lusa . Pellentesque lacus.Vivamus lorem arcu semper duiac. Cras ornare arcu vamus nda leo. Etiam ind arc morbi justo mauris temd pus pharetra interdum at congue semper purus.</p>
												</div>
											</div>
										</div><!-- end panel -->
									</div><!-- end accordion -->
								</section>
							</div><!-- end col -->

							<div class="col-md-6">
								<section class="section-default">
									<h2 class="ui-title-block">Why <strong>It Works</strong></h2>
									<ul class="nav nav-tabs nav-tabs_mod-a">
										<li class="active"><a class="ui-title-inner decor decor_mod-d" href="#tab-1" data-toggle="tab">Learning Community</a></li>
										<li><a class="ui-title-inner decor decor_mod-d" href="#tab-2" data-toggle="tab">Skills & Knowledge</a></li>
									</ul>

									<div class="tab-content">
										<div class="tab-pane active" id="tab-1">
											<img class="pull-left img-responsive" src="assets/media/posts/113x113/1.jpg" height="113" width="113" alt="foto">
											<p>Phasellus luctus nibhay pulvinar bibend um aliquam ligula sapien condimen tum magna eu adipiscing risu Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam quis nostrud.</p>
											<p>Bibend um aliquam ligula sapien condimen tum magna eu adipiscing risu Lorem ips um dolor sit amet consectetur adipisicing elit sed do eiusmod.</p>
										</div>
										<div class="tab-pane" id="tab-2">
											<img class="pull-right img-responsive" src="assets/media/posts/113x113/1.jpg" height="113" width="113" alt="foto">
											<p>Phasellus luctus nibhay pulvinar bibend um aliquam ligula sapien condimen tum magna eu adipiscing risu Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam quis nostrud.</p>
											<p>Bibend um aliquam ligula sapien condimen tum magna eu adipiscing risu Lorem ips um dolor sit amet consectetur adipisicing elit sed do eiusmod.</p></div>
									</div>
								</section>
							</div><!-- end col -->
						</div><!-- end row -->
					</div><!-- end container -->


				

				</main><!-- end main-content -->


			<jsp:include page="Commonfooter.jsp"></jsp:include>

			</div><!-- end wrapper -->
		</div><!-- end layout-theme -->


		<!-- SCRIPTS --> 
<script src="assets/js/jquery-migrate-1.2.1.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<script src="assets/js/modernizr.custom.js"></script> 
<script src="assets/js/waypoints.min.js"></script> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> 

<!--THEME--> 
<script  src="assets/plugins/sliderpro/js/jquery.sliderPro.min.js"></script> 
<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script> 
<script src="assets/plugins/isotope/jquery.isotope.min.js"></script> 
<script src="assets/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script> 
<script src="assets/plugins/datetimepicker/jquery.datetimepicker.js"></script> 
<script src="assets/plugins/jelect/jquery.jelect.js"></script> 
<script src="assets/plugins/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js"></script> 
<script src="assets/js/cssua.min.js"></script> 
<script src="assets/js/wow.min.js"></script> 
<script src="assets/js/custom.min.js"></script> 


</body>
</html>
