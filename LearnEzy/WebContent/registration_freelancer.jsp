<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ page import="java.sql.*" %>
<%@page import="com.itech.elearning.forms.LoginForm"%>
<html>
<head>
<title>Itech Training :: Register</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui">
<title>LearnEzy.com - Registration</title>
<link href="favicon.png" type="image/x-icon" rel="shortcut icon">
<link href="assets/css/master.css" rel="stylesheet">
<!-- BOOTSTRAP -->
<link href="assets/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="assets/css/bootstrap-responsive.css" rel="stylesheet" media="screen">

<!-- FONT AWESOME -->
<link href="assets/css/font-awesome.min.css" rel="stylesheet" media="screen">


<link rel="stylesheet" href="assets/css/tmm_form_wizard_style_demo.css" />
		<link rel="stylesheet" href="assets/css/grid.css" />
		<link rel="stylesheet" href="assets/css/tmm_form_wizard_layout.css" />
		 <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-2.1.4.js"></script>
	
<script type="text/javascript" src="src/DateTimePicker.js"></script>
<script type="text/javascript" src="date/jquery-ui.js"></script>


	
<!-- Load jQuery and the validate plugin and masterdata js file -->
   <script type="text/javascript" src="<%=request.getContextPath()%>/js/validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/additional-methods1.js"></script>
  <script type="text/javascript" src="<%=request.getContextPath()%>/js/validation/common.validation.js"></script>
  

<!-- Dependencies -->
<script src="js/jquery.ui.draggable.js" type="text/javascript"></script>
		<!-- Core files -->
		<script src="js/jquery.alerts.js" type="text/javascript"></script>
<script type="text/javascript" src="js/student/studentRegistration.js"></script>

 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script>
  $(function() {
    $( "#datepicker" ).datepicker();
  });
  </script>

<script type="text/javascript">
    function checkchacter(inputtxt) {
		//alert("arg");
			re = /[^a-z,A-Z, ]/; // remove any characters that are not numbers
		socnum = inputtxt.value.replace(re, "");
		inputtxt.value = socnum;
	}
    </script>


<script type="text/javascript">
function checknumber(inputtxt) {
		//alert("arg");
		re = /\D/g; // remove any characters that are not numbers
		socnum = inputtxt.value.replace(re, "");
		inputtxt.value = socnum;
	}
    </script>
    
    <script>
     $(document).ready(function()
    		{
    
    $("#datepickers").datepicker({
    changeMonth: true, 
    changeYear: true, 
    dateFormat: "dd/mm/yy",
    yearRange: "-90:+00",
    maxDate:"today"
   
});
    		});
    
    </script>
    
    
    
    
</head>
	<body>

		<!-- - - - - - - - - - - - - Content - - - - - - - - - - - - -  -->
   <div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->

<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200"> 
  
  <div id="wrapper"> 
    
       <jsp:include page="jsp/common/commonregisterheader.jsp"></jsp:include>
    
    
     <div class="wrap-title-page">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h1 class="ui-title-page">Register to become a member of Learnezy</h1>
          </div>
        </div>
      </div>
      <!-- end container--> 
    </div>
    <!-- end wrap-title-page -->
    
    <div class="section-breadcrumb">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="wrap-breadcrumb clearfix">
              <ol class="breadcrumb">
                <li><a href="dynamic.jsp"><i class="icon stroke icon-House"></i></a></li>
                <li><a href="registernew.jsp">Registration</a></li>
                <li class="active">Freelancer Registration</li>
                
              </ol>
            </div>
          </div>
        </div>
        <!-- end row --> 
      </div>
      <!-- end container --> 
    </div>
    <!-- end section-breadcrumb -->
       
    
    <div class="main-content">
          
              <div class="container">
    <div class="buttons">
            	
       	  <button class="btn1 login-btn"><i class="fa fa-facebook"></i>Login with Facebook</button>
                
                <button class="btn2 login-btn"><i class="fa fa-google-plus"></i>Login with Google</button>
                
                <button class="btn3 login-btn"><i class="fa fa-yahoo"></i>Login with Yahoo</button>
                
                <button class="btn4 login-btn"><i class="fa fa-linkedin"></i>Login with Linkein</button>
                
                <button class="btn5 login-btn"><i class="fa fa-windows"></i>Login with Window Live</button>
               
            </div>
            
            
          <div class="row">
          
          
            	<div class="span12">
                
                <div class="form-container">

				<div id="tmm-form-wizard" class="container substrate">

					<div class="row" align="center">
						<div class="col-xs-12">
							<h2 class="form-login-heading"><span>Registration Form For Freelancer</span></h2>
						</div>

					</div><!--/ .row-->

					<div class="row stage-container" align="center">

						<div class="stage tmm-current col-md-3 col-sm-3" id="topDiv1">
							<div class="stage-header head-icon head-icon-lock"></div>
							<div class="stage-content">
								<h3 class="stage-title">Registration</h3>
								<div class="stage-info">
									Registration details
								</div> 
							</div>
						</div><!--/ .stage-->

						<div class="stage col-md-3 col-sm-3 " id="topDiv2">
							<div class="stage-header head-icon head-icon-user"></div>

							<div class="stage-content">
								<h3 class="stage-title">License</h3>
								<div class="stage-info">
									Select the Licenses
								</div>
							</div>
						</div><!--/ .stage-->

						<div class="stage col-md-3 col-sm-3" id="topDiv3">
							<div class="stage-header head-icon head-icon-payment"></div>
							<div class="stage-content">
								<h3 class="stage-title">Payment Information</h3>
								<div class="stage-info">
									Make the payment
								</div>
							</div>
						</div><!--/ .stage-->

						 <div class="stage col-md-3 col-sm-3" id="topDiv4">
							<div class="stage-header head-icon head-icon-details"></div>
							<div class="stage-content">
								<h3 class="stage-title">Login</h3>
								<div class="stage-info">
									Login details
								</div> 
							</div>
						</div><!--/ .stage-->

					</div><!--/ .row-->
<html:form action="studentRegistration.do?action=FreelancerRegistration" method="post" styleClass="register" styleId="freelancer"   enctype="multipart/form-data">
						
					<div class="row">

						<div class="col-xs-12" >

							<div class="form-header">
								<div class="form-title form-icon title-icon-lock">
									<b>Registration details</b> 
								</div>
								<div   class="form-title" style="width: 725px;text-align: center;" >
									<b style="color: red;"><logic:notEmpty name="status"><bean:write name="status"/> </logic:notEmpty></b>
								</div>
								<div class="steps">
									Steps 1 - 4
								</div>
							</div><!--/ .form-header-->

						</div>

					</div><!--/ .row-->
			 
					
						


						<div class="form-wizard" >
							
							<div class="row">

								<div class="col-md-8 col-sm-7">

									
<div class="row">
										
										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="first-name">First Name <font style="color: red;">*</font></label>
												<input type="text" id="firstName" placeholder="First Name" name="firstName" maxlength="30"   autofocus  />
												
											</fieldset><!--/ .input-first-name-->
										</div>
										
										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="last-name">Last Name</label>
												<input type="text" id="lastName"  name="lastName"  maxlength="20"	  placeholder="Last Name"   />
												
											</fieldset><!--/ .input-first-name-->
										</div>
										
									</div><!--/ .row-->
									<div class="row">
										
										<div class="col-md-6 col-sm-6">

											<div class="input-block">
												<label>Date Of Birth <font style="font-size: 12px;">(DD/MM/YYYY)</font> </label>
												<div class="input-block">
													<input type="text" name="dob"  placeholder="DOB" id="datepickers" size="8" readonly   />
												</div><!--/ .dropdown-->
												
											</div><!--/ .input-role-->
											
										</div>

										<div class="col-md-6 col-sm-6">

											<fieldset class="input-block">
												<label>Gender</label>
												<div class="dropdown">
													<select name="gender" id="gender" class="dropdown-select"  >
														<option value="-1">Select</option>
									<option value="Male">Male</option>
									<option value="Female">Female</option>
													</select>
												</div><!--/ .dropdown-->
												
											</fieldset><!--/ .input-role-->
											
										</div>

									</div><!--/ .row-->
									<div class="row">
										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="last-name">Qualification</label>
												<input type="text" name="qualification" id="qualification"  maxlength="50"	  placeholder="Qualification"  />
												
											</fieldset><!--/ .input-first-name-->
										</div>
										
									</div><!--/ .row-->
									
									<div class="row">
										
										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="phone">Mobile No <font style="color: red;">*</font></label>
												<input type="text" name="contactno"  id="contactno"  maxlength="12"  id="contactno" class="form-icon form-icon-phone" placeholder="Mobile No"  />
												
											</fieldset><!--/ .input-phone-->
										</div><!--/ .input-phone-->
										
										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="phone">Alternative Contact No</label>
												<input type="text"  name="phone" id="phone" 	maxlength="13"  id="phone" class="form-icon form-icon-phone" placeholder="Alternative Contact No"  />
												
											</fieldset><!--/ .input-phone-->
										</div><!--/ .input-phone-->
										
									</div><!--/ .row-->
<div class="row">

										<div class="col-md-12 col-sm-12">
											<fieldset class="input-block">
												<label for="address">Area Of Expertise</label>
												<textarea rows="2" cols="77" maxlength="100" name="areofexper"></textarea>
												
											</fieldset><!--/ .input-phone-->
										</div>
										
									</div><!--/ .row-->
									
									<div class="row">

										<div class="col-md-12 col-sm-12">
											<fieldset class="input-block">
												<label for="address">Address</label>
												<input type="text" id="address" name="address"  maxlength="150" placeholder="Address"  />
												
											</fieldset><!--/ .input-phone-->
										</div>
										
									</div><!--/ .row-->

										<div class="row">

										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="address">Country*</label>
												<div class="dropdown">
												<html:select property="countryid" styleId="countryid"  styleClass="dropdown-select" >
													<html:option value="">--Select Country--</html:option>
													<html:options collection="countryList" property="countryid" labelProperty="countryname" />
												</html:select>
												</div>
											</fieldset><!--/ .input-phone-->
										</div>
										
									

										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="address">State*</label>
												<div class="dropdown">
												<select name="stateid" id="stateid" class="dropdown-select" >
													<option value="">-Select State-</option>
												</select>
												</div>
											</fieldset><!--/ .input-phone-->
										</div>
										
									</div>
									<div class="row">
										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="address">City*</label>
												<div class="dropdown">
												<select name="cityid" id="cityid"  class="dropdown-select">
													<option value="">-Select City-</option>
												</select>
												</div>
											</fieldset><!--/ .input-phone-->
										</div>
										
												<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="address">Area*</label>
												<div class="dropdown">
												<select name="areaid" id="areaid" class="dropdown-select">
													<option value="">-Select Area-</option>
												</select>
												</div>
											</fieldset><!--/ .input-phone-->
										</div>
										
										
										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="address">Pincode <font style="color: red;">*</font></label>
												
												<input type="text"  name="pincode" placeholder="Pincode"  maxlength="6"/>
												
											</fieldset><!--/ .input-phone-->
										</div>
										
									</div><!--/ .row-->
									 
									
									<div class="row" style="height: 100px;"> 
										<div class="col-md-6 col-sm-6">
											<div class="input-block">
												
												 
													 <label>Profile Photo:</label>
								<input name="imageFile" id="imageFile" type="file" placeholder="upload file Photo"  />
												 
												
											</div><!--/ .input-country-->
											
										
										</div>
										 	
									</div>
									

						 
								<div class="col-md-10 col-sm-7" style="margin-top: 15px;">

									<fieldset class="sign">
									<legend >Sign in Details</legend>
                                    	<div class="row">
										<div class="col-md-12 col-sm-12" >
											<fieldset class="input-block">
												<label for="card-number">Email-id<font style="color: red;">*</font></label><br />
												<input type="email"  name="emailId" id="emailId"	  placeholder="User Name"  value="" maxlength="25" style="float: left;width: 42%" /> 
												 <div id="uNameDiv"  ></div>
											</fieldset>
										</div>
										</div>
										<div class="row">
										<div class="col-md-6 col-sm-6" >
											<fieldset class="input-block">
												<label for="card-number">Password <font style="color: red;">*</font> <span style="font-size: 10px;font-style: italic;color: red;margin-left: 192px;" title="Password should contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character and min 8 characters" >Rules</span></label>
												<input type="password" name="password" maxlength="30"  id="password" class="passWord" value="" onchange="checkPasswordStrong(this);" placeholder="Password" style="float: left;width: 90%"   />	 
           										<div id="passDiv" ></div>
											</fieldset>
										</div>
										
										
										<div class="col-md-6 col-sm-6" >
											<fieldset class="input-block">
												<label for="card-number">Confirm Password <font style="color: red;">*</font> </label>
												<input name="retypepassword"  maxlength="30"  id="retypepassword" type="password" onchange="checkpassword();"   placeholder="Confirm Password"  style="float: left;width: 90%" />	 
         										<div id="confDiv" ></div>
											</fieldset>
										</div>
										
										<div class="col-md-12 col-sm-12" id="passRules" style="height: 30px;font-size: 12px;font-style: italic;color: red;" >
											 
										</div>
									 
										<div class="col-md-6 col-sm-6" style="margin-top: 5px; " >
										<fieldset class="input-block">
											 <input id="captchaText" type="text" readonly="readonly"  value="" style="float: left;width: 45%;background-image: url(img/BG1.png);color: black;border: none;text-align: center;" />
											<input id="captchabox" type="text" style="float:left;width: 45%"  />	
											<div id="captchaDiv" ></div>	
											</fieldset>
										</div>
										
										
										</div>		
									</fieldset><!--/ .input-email-->

								</div>

							</div><!--/ .row-->
							
						</div><!--/ .form-wizard-->
						
						

						
						
						
						
						</div>
						<div class="next1" style="float:right; ">
							<button class="button button-control" type="submit" id="validateRegister"><span>Submit</span></button>
							<div class="button-divider"></div>
						</div>
						</html:form>
				 
							
							
</div>

					</div><!--/ .row-->
					 
                
                
                
				</div>  <!-- span12 -->
               
            </div>   <!-- row -->
        
        
        </div>  <!-- container -->
        
      
    </div>
    <!-- end main-content -->
    
        <jsp:include page="jsp/common/student_footer.jsp" /> 
   
  </div>
  <!-- end wrapper --> 
</div>
<!-- end layout-theme --> 

<!-- Jquery Lib -->


<!-- SCRIPTS --> 
<script src="assets/js/jquery-migrate-1.2.1.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<script src="assets/js/modernizr.custom.js"></script> 
<script src="assets/js/waypoints.min.js"></script> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> 

<!--THEME--> 
<script  src="assets/plugins/sliderpro/js/jquery.sliderPro.min.js"></script> 
<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script> 
<script src="assets/plugins/isotope/jquery.isotope.min.js"></script> 
<script src="assets/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script> 
<script src="assets/plugins/datetimepicker/jquery.datetimepicker.js"></script> 
<script src="assets/plugins/jelect/jquery.jelect.js"></script> 
<script src="assets/plugins/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js"></script> 
<script src="assets/js/cssua.min.js"></script> 
<script src="assets/js/wow.min.js"></script> 
<script src="assets/js/custom.min.js"></script>

<script src="assets/js/tmm_form_wizard_custom.js"></script>
 
 
 
 
<script type="text/javascript">
$(document).ready( function() {


	$(function() {
	      $('#myVariable').datepicker({dateFormat: 'dd/mm/yy'});
	});
	
	$("#firstName").focus();
	$("#countryid").val("-1");
	var num = Math.floor(Math.random() * 9000) + 1000;
	//alert(num);
	$("#captchaText").val(num);
	
	 
$("#countryid").change( function() {
	var countryid=$(this).val();
	if(countryid=="0"){
		$("#otherCountryAdd").show();
	}else{
		$("#otherCountryAdd").hide();
		$("#otherCountry").val("");
	$.ajax({	
		type: "GET",		
		url:"State.do?action=ajaxActiveList",
		data:{countryId:countryid},
		success: function(r){	
			var json= JSON.parse(r);
			console.log(json);			
			$("#stateid").empty();
			var firstOption= $("<option>",{text:"--Select State--",value:"-1"});
			var otherOption= $("<option>",{text:"Others",value:"0"});
			$("#stateid").append(firstOption);
			$.each(json,function(i,obj){
				var option= $("<option>",{text:obj.state, value:obj.stateId});
				$("#stateid").append(option);
			});
			$("#stateid").append(otherOption);
		} 
		
	});
	}
});

$("#stateid").change( function() {
	var stateid=$(this).val();
	if(stateid=="0"){
	$("#otherStateAdd").show();
	}else{
	$("#otherStateAdd").hide();
	$("#otherState").val("");
	$.ajax({	
		type: "GET",		
		url:"City.do?action=ajaxActiveList",
		data:{stateId:stateid},
		success: function(r){	
			var json= JSON.parse(r);
			console.log(json);			
			$("#cityid").empty();
			var firstOption= $("<option>",{text:"--Select City--",value:"-1"});
			var otherOption= $("<option>",{text:"Others",value:"0"});
			$("#cityid").append(firstOption);
			$.each(json,function(i,obj){
				var option= $("<option>",{text:obj.city, value:obj.cityId});
				$("#cityid").append(option);
			});
			$("#cityid").append(otherOption);
		} 
		
	});
	}
});

$("#cityid").change( function() {
	var cityid=$(this).val();
	if(cityid=="0"){
	$("#otherStateAdd").show();
	}else{
	$("#otherStateAdd").hide();
	$("#otherState").val("");
	$.ajax({	
		type: "GET",		
		url:"Area.do?action=ajaxActiveList",
		data:{cityId:cityid},
		success: function(r){	
			var json= JSON.parse(r);
			console.log(json);			
			$("#areaid").empty();
			var firstOption= $("<option>",{text:"--Select Area--",value:"-1"});
			var otherOption= $("<option>",{text:"Others",value:"0"});
			$("#areaid").append(firstOption);
			$.each(json,function(i,obj){
				var option= $("<option>",{text:obj.AREANAME, value:obj.AREAID});
				$("#areaid").append(option);
			});
			$("#areaid").append(otherOption);
		} 
		
	});
	}
});

 

$("#cityid").change( function() {
	var cityid=$(this).val();
	if(cityid=="0"){
		$("#otherCityAdd").show();
	}else{
		$("#otherCityAdd").hide();
		$("#otherCity").val("");
	}
});
});

function pincodOnlyNumbers() {
	//alert("arg");
	re = /\D/g; // remove any characters that are not numbers
	socnum = document.forms[0].pincode.value.replace(re, "");
	document.forms[0].pincode.value = socnum;
}

</script>
 
	<script src="js/commonFunction.js"  type="text/javascript"></script>	
	<script type="text/javascript">
function readURL(input) {
	if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$(document).ready(function() {
$("#imageFile").change(function(){
    readURL(this);
});
$("#otherStateAdd").hide();
$("#otherCityAdd").hide();
$("#otherCountryAdd").hide();
});
$(document).ready(function()
		{

$("#emailId").change(function()
		{
	var emailid=$(this).val();
	$.ajax({	
		type: "GET",		
		url:"<%=request.getContextPath()%>/studentRegistration.do?action=verifyemailid",
		data:{emailid:emailid},
		success: function(r){	
			var json= JSON.parse(r);
			
			$.each(json,function(i,obj){
				$("#emailId").val("");
				alert(obj.email);
				$("#emailId").focus();
				
			
				
				
			});
		} 
		
	});

	





	
		});


		});
$(document).ready(function()
{
$("#validateRegister").click(function()
		{

	if($("#freelancer").valid())
	{
var checkstr=$("#captchaText").val();
var captchabox=$("#captchabox").val();
if(checkstr==captchabox)
{
return true;

}
else
{
alert("Enter correct verify Number");
return false;
}
	}


		});


		});






</script>
	
	</body>


</html>