<!-- HEADER -->
  
 <header class="header">
      <div class="top-header">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <!--<div class="top-header__contacts"><i class="icon stroke icon-Phone2"></i>(123) 0800 12345</div>-->
              <div class="top-header__contacts"><i class="icon stroke icon-Mail"></i><a href="mailto:Info@learnezy.com">Info@learnezy.com</a></div>
              <div class="top-header__link">
                <a href="DynamicData.do?action=latestcourses"><button class="btn-header btn-effect" >LATEST</button></a>
                <span>We have added new courses today ...</span> </div>
              <div class="header-login"> <a class="header-login__item" href="sign_in.jsp"><i class="icon stroke icon-User"></i>Sign In</a> <a class="header-login__item" href="registernew.jsp">Register</a> </div>
            </div>
            <!-- end col  --> 
          </div>
          <!-- end row  --> 
        </div>
        <!-- end container  --> 
      </div>
      <!-- end top-header  -->
      
      <div class="container">
        <div class="row">
          <div class="col-xs-12"> <a class="header-logo" href="dynamic.jsp"><img class="header-logo__img" src="assets/img/LEARNEZY-LOGO.png" alt="Logo"></a>
            <div class="header-inner">
              <div class="header-search">
                <div class="navbar-search">
                  <form id="search-global-form">
                    <div class="input-group">
                      <input type="text" placeholder="Type your search..." class="form-control" autocomplete="off" name="s" id="search" value="">
                      <span class="input-group-btn">
                      <button type="reset" class="btn search-close" id="search-close"><i class="fa fa-times"></i></button>
                      </span> </div>
                  </form>
                </div>
              </div>
              <!--
              <a id="search-open" href="#fakelink" ><i class="icon stroke icon-Search"></i></a> <a class="header-cart" href="#"> <i class="icon stroke icon-ShoppingCart"></i></a>
              -->
    <a id="search-open" href="#fakelink" ><i class="icon stroke icon-Search"></i></a> <a class="header-cart" href="#"  data-toggle="modal" data-target="#signup"> <i class="icon stroke icon-ShoppingCart"></i></a>
              
              
              
              
              
              <nav class="navbar yamm">
                <div class="navbar-header hidden-md  hidden-lg  hidden-sm ">
                  <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                </div>
                <div id="navbar-collapse-1" class="navbar-collapse collapse">
                  <ul class="nav navbar-nav">
                    <li class="dropdown"><a href="DynamicData.do">Home<span class="nav-subtitle">OUR World</span></a>
                     
                    </li>
                    <!-- <li class="dropdown"> <a href="DynamicData.do?action=latestcourses">COURSES<span class="nav-subtitle">What We Offers</span></a> -->
                      
                    </li>
                   
                    <li><a href="contact-us.jsp">CONTACT<span class="nav-subtitle">say us hi</span></a></li>
                  </ul>
                </div>
              </nav>
              <!--end navbar --> 
              
            </div>
            
            <!-- Add To cart-->
            
             <div class="modal fade" id="signup" role="dialog" aria-labelledby="signupLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                <!--
                    <div class="modal-header">
                        <h4 class="modal-title" id="signupLabel">Sign Up to take this Course</h4>
                    </div>
                    -->
                    <div class="modal-body">
                        <form action="#"  method="post" id="prodForm"  role="form">
                        <logic:notEmpty name="status">
                          <center>  <font style="color:red; text-align: center !important;">
                            <bean:write name="status"/></font></center>
                            <input type="hidden" name="status" id="status" value="<bean:write name="status"/>">
                            </logic:notEmpty>
                           
                             <input type="hidden" name="courses" value="<bean:write name="coursedesc" property="coursenamelatest"></bean:write>">
	<input type="hidden" name="courseprice" value="<bean:write name="coursedesc" property="fees"></bean:write>">
					
					
	<input type="hidden" value="" id="tempId" name="tempId">
	
                           
                            
                            <div class="form-group">
                                <img src="pic_mountain.jpg" alt="Image" style="width:304px;height:228px;"></img>
                            </div>
                            
                            <div class="form-group">
                                <input type="text" name="emailid" id="name" placeholder="aaaa" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="text"  name="text" id="mobile" placeholder="price" class="form-control">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="checkbox custom-checkbox"><label><input type="checkbox"> Be the first one to know about new courses and great offers! </label></div>
                                    </div>
                                   
                                </div>
                            </div>
                             <div class="form-group">
                                <div class="btn-group-justified">
                                    <input type="submit" value="Sign Up" class="btn btn-primary btn-effect">
                                </div>
                            </div>
                           <p class="help-block"> By signing up, you agree to our <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a>.</p>
                            <p class="help-block">Already have an account? <a href="#" class="modal-si text-green isThemeText">Login</a></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
            
         !-- Add To cart-->   
         
         
         
         
         
         
         
         
            
            
            <!-- end header-inner --> 
          </div>
          <!-- end col  --> 
        </div>
        <!-- end row  --> 
      </div>
      <!-- end container--> 
    </header>
    <!-- end header -->
    
    
