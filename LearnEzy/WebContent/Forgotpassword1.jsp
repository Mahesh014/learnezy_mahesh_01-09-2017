<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui">
<title>LearnEzy.com - Forgot Password</title>
<link href="favicon.png" type="image/x-icon" rel="shortcut icon">
<link href="assets/css/master.css" rel="stylesheet">

<!-- BOOTSTRAP -->
<link href="assets/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="assets/css/bootstrap-responsive.css" rel="stylesheet" media="screen">

<!-- FONT AWESOME -->
<link href="assets/css/font-awesome.min.css" rel="stylesheet" media="screen">

<!-- OWL CAROUSEL -->
<link href="assets/css/owl.carousel.css" rel="stylesheet">
<!-- BX SLIDER-->
<link href="assets/css/jquery.bxslider.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
</head>
 <script type="text/javascript">
  
	$(document).ready( function() {
		
	$("#validateForgot").click( function() {
		var re =/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/; 
		var forgotMail=$("#forgotmail");
		if(forgotMail.val()==""){
			alert("Email ID cannot be empty");
			forgotMail.focus();
			return false;
		}     
		if (forgotMail.val().search(re)== -1) {
			alert("Please Enter valid Email Id");
			forgotMail.val("")
			forgotMail.focus();
			return false;
		}
	});
	
	 
	
	
});
</script>
<body> 

<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->

<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200"> 
  
  <div id="wrapper"> 
    
   <jsp:include page="Commonheader.jsp"></jsp:include>
    
     <div class="wrap-title-page">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h1 class="ui-title-page">Forgot Password?</h1>
          </div>
        </div>
      </div>
      <!-- end container--> 
    </div>
    <!-- end wrap-title-page -->
    
    <div class="section-breadcrumb">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="wrap-breadcrumb clearfix">
              <ol class="breadcrumb">
                <li><a href="index.html"><i class="icon stroke icon-House"></i></a></li>
                <li><a href="sign_in.html">Login</a></li>
                <li class="active">Forgot Password</li>
              </ol>
            </div>
          </div>
        </div>
        <!-- end row --> 
      </div>
      <!-- end container --> 
    </div>
    <!-- end section-breadcrumb -->
       
    
    <div class="main-content">
          
          
    <div class="container">
    <div class="buttons">
            	<button class="btn1 login-btn"><i class="fa fa-facebook"></i>Login with Facebook</button>
                
                <button class="btn2 login-btn"><i class="fa fa-google-plus"></i>Login with Google</button>
                
                <button class="btn3 login-btn"><i class="fa fa-yahoo"></i>Login with Yahoo</button>
                
                <button class="btn4 login-btn"><i class="fa fa-linkedin"></i>Login with Linkein</button>
                
                <button class="btn5 login-btn"><i class="fa fa-windows"></i>Login with Window Live</button>
               
            </div>
            
            
          <div class="row">
           <div class="span3">
                	<div class="sidebar">
                    	
                        
                    </div>  <!-- sidebar -->
                </div>  <!-- span3 -->          
          
            	<div class="span6">
                	<div class="form-box">
                         <form action="ForgotPassword1.do?action=sendmail1" method="post"  class="forgot_password"  id="forgotpass" >
                        <div class="form-body">
                        <fieldset>
                        <legend>Forgot Password</legend>
                        <legend>Please enter your registered Email ID<br>
Password will be send to your Email ID</legend>
                        <label>Email Address</label>
                        <input type="text" name="email" id="forgotmail" onchange="return validateForgot();"  placeholder="Enter your E-mail ID" class="input-block-level">
                                       
                        
                        <button type="submit" type="submit" class="btn-style" id="validateForgot" >Submit</button>
                      
                        </fieldset>
                        </div>
                        
                        </form>
                    </div>
                </div>
                <div class="span3">
                	<div class="sidebar">
                    	
                        
                    </div>  <!-- sidebar -->
                </div>  <!-- span3 -->
            </div>   <!-- row -->
        
        
        </div>   <!-- container -->
        
      
    </div>
    <!-- end main-content -->
    
   <jsp:include page="Commonfooter.jsp"></jsp:include>
  </div>
  <!-- end wrapper --> 
</div>
<!-- end layout-theme --> 

<!-- Jquery Lib -->
<script src="assets/js/jquery-1.10.1.min.js"></script>

<!-- SCRIPTS --> 
<script src="assets/js/jquery-migrate-1.2.1.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<script src="assets/js/modernizr.custom.js"></script> 
<script src="assets/js/waypoints.min.js"></script> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> 

<!--THEME--> 
<script  src="assets/plugins/sliderpro/js/jquery.sliderPro.min.js"></script> 
<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script> 
<script src="assets/plugins/isotope/jquery.isotope.min.js"></script> 
<script src="assets/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script> 
<script src="assets/plugins/datetimepicker/jquery.datetimepicker.js"></script> 
<script src="assets/plugins/jelect/jquery.jelect.js"></script> 
<script src="assets/plugins/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js"></script> 
<script src="assets/js/cssua.min.js"></script> 
<script src="assets/js/wow.min.js"></script> 
<script src="assets/js/custom.min.js"></script>



</body>
</html>
