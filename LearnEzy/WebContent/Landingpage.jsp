<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Creative - Start Bootstrap Theme</title>

<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/jquery-2.1.1.min.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/validation/jquery.validate.min.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/validation/common.validation.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/validation/additional-methods.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/additional-methods1.js"></script>

<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/creative.min.js"></script>
    

<script type="text/javascript">

$(document).ready(function()
		{
	alert();
$("#form11").validate( {
	rules : {
	txtname : {	
			required : true,
			lettersonly : true,
			maxlength : 25
		},
		txtemail  : {
			required : true,
			checkemailnew : true
		},
		contactnumber  : {
			required : true,
			number:true,
			maxlength:10
		}
},
	messages : {
	txtname : {
			required : "Customer Name required!",
			lettersonly : "Enter only characters",
			maxlength:"Maximum length 25"
				 
		},
		txtemail  : {
			required :"Email Id is required!",
			checkemailnew : "Please Enter Valid email Id"
		},
		contactnumber  : {
			required : "Mobile Number required!",
			number : "Please enter numbers",
			maxlength:"Please Only 10 Numbers"
		}
		}
	
});
});

$(document).ready(function()
		{
			var val=$("#hiddenid").val();
			if(val!=='')
			{
			alert("Thanks for Registering");
			}
				
		});

</script>
    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Open+Sans'>
 <link rel="stylesheet" href="blueberry.css" />
      <link rel="stylesheet" href="cssl/style.css">
    <!-- Theme CSS -->
    <link href="css/creative.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
 
 <![endif]-->


 <link rel="stylesheet" type="text/css" href="css1/demo.css" />
        <link rel="stylesheet" type="text/css" href="css1/style.css" />
<style>
.social{
   
    border-radius: 50px;
    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.15), 0 0 50px rgba(0, 0, 0, 0.1) inset;
    float: left;
    height: 49px;
    margin: 0 10px 10px 0;
    padding: 9px;
    text-align: center;
    width: 56px;
}
body{
margin:0;
}
.slides {
    height: 100px !important;
}.solt{
margin-left:-43%;
}
	.fimg{
	margin-left: 4%;
	height:100px;
	
	}
	.form{
	 margin-left: 2%;
    width: 30%;
	}
	.progress-bar {
    color: #333;
}

.section {
    padding: 0px 0 !important;
}


{
    -webkit-box-sizing: border-box;
	   -moz-box-sizing: border-box;
	        box-sizing: border-box;
	outline: none;
}

.form-control {

	  position: relative;
	  font-size: 16px;
	  height: auto;
	  padding: 10px;
		@include box-sizing(border-box);

		&:focus {
		  z-index: 2;
		}
	}


header .header-content .header-content-inner h1 {
    font-weight: 700;
    text-transform: uppercase;
    margin-top: 25% !important;
    margin-bottom: 0;
    font-weight:  40px;
}
.login-form {
	margin-top: 206px;
  
}
.regis
{
    color: #fff;

border-color: #204d74;
padding: 10px 16px;
font-size: 18px;
line-height: 1.3333333;

display: inline;
width: 100%;
text-align: center;

}

form[role=login] {
	color: #5d5d5d;
	background: #6f6565;
	padding: 26px;
	border-radius: px;
	
      filter: opacity(63%);

}
	form[role=login] img {
		display: block;
		margin: 0 auto;
		margin-bottom: 35px;
	}
	form[role=login] input,
	form[role=login] button {
		font-size: 18px;
		margin: 16px 0;
	}
	form[role=login] > div {
		text-align: center;
   

	}
	
.form-links {
	text-align: center;
	margin-top: 1em;
	margin-bottom: 50px;
}
	.form-links a {
		color: #fff;
	}
  .register
  {

    color: white;
  }
  
    #form11
  {

    color: yellow;
  }

  header .header-content .header-content-inner h1 {
    font-weight: 700;
    text-transform: uppercase;
   
    margin-bottom: 0;
    font-size: 30px;
}
	@media screen and (max-width: 480px) 
   {
   
   .social {
    height: 39px;
    margin: 0 4px 10px 0;
    width: 49px;
}
   .sp-slideshow {
    height: 67px;
}
.solt {
    margin-left: 2%;
}
.sp-content {
    
    height: 49%;
    }

     .img-responsive
     {
     display: inline;
max-width: 87%;
height: 109px;
}
    header .header-content .header-content-inner h1 {
    font-weight: 700;
    text-transform: uppercase;
    margin-top: -77%;
    margin-bottom: 0;
    font-size: 30px;
}
   
   .form{
	width:100%;
	margin-left: 0%;
	}
	.login-form {
    margin-top: 0px;
}
   .header-content {
    margin-top: -37%;
    padding: 67px 28px;
    width: 100%;
}
  .demo {
    position: absolute;
   top: 55% !important;
    left: 45%;
    margin-left: -15rem;
    margin-top: -28.5rem;
    width: 28rem;
    height: 28rem;
    overflow: hidden;
} 


 a.sign
   {
   border:1px solid blue;
   padding: 7px 32px 6px 29px;
   top:54%;
   left:25%!important;
   position:absolute;
   color:white;
   text-decoration:none;
   }
   a.sign1
   {
   border:1px solid blue;
   padding: 7px 32px 6px 29px;
   top:54%;
   left:59%  !important;
   position:absolute;
   text-decoration:none;
   }
   }
   .foot
   {
   padding:2px;
  height: auto;
   text-align:center;
   background-color:#4a423f;
   color:white;
   }
   .imm{
   height:80px;
   width:100%;
   position:relative;
   }
   a.sign
   {
   border:1px solid blue;
   padding: 7px 32px 6px 29px;
   top:54%;
   left:80%;
   position:absolute;
   color:white;
   text-decoration:none;
   }
   a.sign1
   {
   border:1px solid blue;
   padding: 7px 32px 6px 29px;
   top:54%;
   left:70%;
   position:absolute;
   text-decoration:none;
   }
   #homeHeading {
    font-size: 28px;
}
   section {
    padding: 48px 0 !important;
  }

  .service-box {
    max-width: 250px !important;
    margin: 50px auto 0;
}
@media (min-width:700px) and (max-width:900px)
{
header {
    min-height: 115%;
}

header .header-content {
    
    top:48%;
   
}
.form {
    margin-left:12%;
    width: 78%;
}
#homeHeading {
    font-size: 48px;
    margin-top: 284px;
}
.login-form {
    margin-top: 2px;
}
}

@media (min-width:901px) and (max-width:1100px)
{
.form {
    margin-left:12%;
    width: 78%;
}
#homeHeading {
    font-size: 48px;
    margin-top: 284px;
}
.login-form {
    margin-top: 2px;
}
}
.discnt
{
color:yellow;
}
.input-lg {
    border-radius: 0px !important;
    
}
.btn {
    border: medium none;
    border-radius: 0px;
    font-weight: 700;
    text-transform: uppercase;
}


.myn {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: rgb(74,66,63);
	 padding: 15px 14% 0 0;
	
	
}

.myna  {
    float: right;
	
}

.myn  a {
    display: block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

.myna  a:hover {
    background-color: red;
}
.hdr{
position:relative; 

}



@import url(//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css);

.google-pluse:hover{background:#DD4B39;color:#FFF;}
.facebook:hover{background:#3b5998;color:#FFF;}
.twitter:hover{background:#00acee;color:#FFF;}
.pinterest:hover{background:#c8232c;color:#FFF;}
.linkedin:hover{background:#0e76a8;color:#FFF;}
.skype:hover{background:#00aff0;color:#FFF;}
.youtube:hover{background:#c4302b;color:#FFF;}
.tumblr:hover{background:#34526f;color:#FFF;}

.fa-5x {
    font-size: 2em;
}

.hdr
{
text-align:center;
color:black;
}
.lern
{
float:center;
margin-left:45%;
}
.title-block {
  display: block;
    text-align:center;
}
.title-block h1 {
  margin: 0px;
  font-size: 35px;
 position:relative;
  display: inline-block;
  color:black;

}
.title-block h1:after{
content:'';
width:170px; height:1px; background:rgb(221,64,21); position:absolute; bottom:-5px;
left:0px; right:auto; margin-left:10px;
}
.text-faded1
{
color:black;
}
.row{
margin-right:0;
margin-left:0;
}
   </style>
   


<script>
$(window).load(function() {
	$('.blueberry').blueberry();
});
</script>

</head>

<body id="page-top">

	
    <header>
	
<div class="row">
<div class="col-sm-12">                                                                                                                                                                                                                                                                                                                                                                     
     <img src="log.png" class="img-responsive fimg">
	 </img></div>
	

	 
	<div class="row">
	<div class="header-content">
	<div class="col-lg-8">
         
            <div class="header-content-inner">

               <br><br> <h1 id="homeHeading" class="abto">Flat <b class="discnt">40% discount </b>on all<br>
               E-commerce courses</h1>
                <hr>
               <h4>Covering two with an industry based live  project, You will be awarded Benchmark
certification and 100% job placements in one of the top IT companies. Do not miss the
opportunity.</h4>
               <img src="img/m10.jpg" class="img-responsive ">
            </div>
        </div>
		
		
		
		  <div class="col-sm-4 form" id="pwd-container">
      <section class="login-form" id="reg">
<!--        <form method="post" action="#" role="login" id="form1">-->

        <form action="DynamicData.do?action=enquiry1" role="login" method="post" name="frm_contact" id="form11">
       
          <h1 class="regis">Register Now !	</h1>
		  <hr class="lern">
<%
   				 Object msg=request.getAttribute("varName");
   				 if(msg==null)
  				  {
      			  msg="";
   				  }
   				 
   			 %>
   			 
   			 <p><font color="white"><%= msg %></font></p>
   			   <input type="hidden" id="hiddenid" value=<%= msg %>>
           <input type="text" name="txtname" class="form-control input-lg" id="name" placeholder="Name"  />
            <input type="mobile" name="contactnumber" class="form-control input-lg" id="mobile" placeholder="Mobile" />
          <input type="email" name="txtemail" placeholder="Email"  class="form-control input-lg"  placeholder="Email" />
            <textarea rows="4" name="textarea-message" cols="50"  placeholder="Message"  class="form-control"   autofocus style="color:#555;"> </textarea>
            <div class="pwstrength_viewport_progress"></div>

          <button type="submit" id="myButton" name="go" class="btn btn-lg btn-primary btn-block">Submit</button>
<!--          <div>-->
<!--           -->
<!--          </div>-->
          
        </form>
        
        <div class="form-links">
          
        </div>
      </section>  
      </div>
		<!------login ends---->
    </header>

    <section class="bg-primary" id="about">
     <div class="title-block text-xs-center text-lg-left">
  <h1>Testimonial</h1>

</div>
	 <div class="container">
		
			<!-- Codrops top bar -->
           
			
			  
			
			<div class="sp-slideshow">
			
				<input id="button-1" type="radio" name="radio-set" class="sp-selector-1" checked="checked" />
				<label for="button-1" class="button-label-1"></label>
				
				<input id="button-2" type="radio" name="radio-set" class="sp-selector-2" />
				<label for="button-2" class="button-label-2"></label>
				
				<input id="button-3" type="radio" name="radio-set" class="sp-selector-3" />
				<label for="button-3" class="button-label-3"></label>
				
				<input id="button-4" type="radio" name="radio-set" class="sp-selector-4" />
				<label for="button-4" class="button-label-4"></label>
				
				<input id="button-5" type="radio" name="radio-set" class="sp-selector-5" />
				<label for="button-5" class="button-label-5"></label>
				
				<label for="button-1" class="sp-arrow sp-a1"></label>
				<label for="button-2" class="sp-arrow sp-a2"></label>
				<label for="button-3" class="sp-arrow sp-a3"></label>
				<label for="button-4" class="sp-arrow sp-a4"></label>
				<label for="button-5" class="sp-arrow sp-a5"></label>
				
				<div class="sp-content">
					<div class="sp-parallax-bg"></div>
					<ul class="sp-slider clearfix">
						<li><img src="images/m7.png" alt="image01" /></li>
						<li><img src="images/m11.jpg" alt="image02" /></li>
						<li><img src="images/m12.jpg" alt="image03" /></li>
						<li><img src="images/m13.jpg" alt="image04" /></li>
						<li><img src="images/image5.png" alt="image05" /></li>
					</ul>
				</div><!-- sp-content -->
				
			</div><!-- sp-slideshow -->
			
			
		</div>
	
	
    </section>

     <section class="bg-primary1" id="about">
        <div class="container">
            <div class="row">
			    
			
                <div class="col-lg-8  text-center">
                    <h2 class="section-heading">About Learnz</h2>
                    
                    <p class="text-faded1">LearnEZY is like an Online Platform for Institutions, Home Based Tutors, Freelancers and everyone else who wants to teach and, yes obviously a Study Portal for Students.

LearnEZY provides students with almost everything they would need to learn a new subject, which includes various formats such as Text, Video, Audio, Image as well as interactive classes.</p>
                    <a href="#reg" class="page-scroll btn btn-default btn-xl sr-button">Get In touch with us today !</a>
                </div>
				<div class="col-lg-4 ">
                    <h2 class="section-heading"></h2>
                    <img src="img/m6.png" class="img-responsive">
                </div>
            </div>
        </div>
    </section>

   

    

    <section id="contact" class="foot">
        <div class="container foot">
            <div class="row">
              <div class="col-lg-8">
			  
<h3 class="solt">
Copyright © LearnEzy 2017. All Rights Reserved<br></h3>
			  </div>
			  
			   <div class="col-lg-4">
			  

<div class="social google-pluse">
           <a href="#"> <i class="fa fa-google-plus fa-5x"></i></a>
        </div>
        <div class="social facebook">
            <a href="#"> <i class="fa fa-facebook fa-5x"></i></a>       
        </div>
        <div class="social twitter">
           <a href="#">  <i class="fa fa-twitter fa-5x"></i>   </a>
        </div>
        <div class="social pinterest">
           <a href="#">  <i class="fa fa-pinterest fa-5x"></i>  </a>
        </div> 
		 <div class="social pinterest">
           <a href="#">  <i class="fa fa-youtube fa-5x"></i>  </a>
        </div> 
    </div>
   
            </div>
        </div>
    </section>

<!--     jQuery -->
<!--    <script src="vendor/jquery/jquery.min.js"></script>-->

    <!-- Bootstrap Core JavaScript -->


</body>

</html>
