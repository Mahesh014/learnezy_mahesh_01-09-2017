<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui">
<title>LearnEzy.com - Course List</title>
<link href="favicon.png" type="image/x-icon" rel="shortcut icon">
<link href="assets/css/master.css" rel="stylesheet">
<link href="css/fSelect.css" rel="stylesheet" type="text/css">
<script src="assets/plugins/jquery/jquery-1.11.3.min.js"></script>

		<link rel="stylesheet" href="gotop/css/ap-scroll-top.css" type="text/css" media="all" />

        
		<script src="gotop/js/ap-scroll-top.js"></script>
<style>
label{display:block;}
.equal{font-size:13px;}
.jelect-options{width:130px;}
.btn {
    display: inline-block;
    margin-bottom: 7px;
    font-weight: normal;
    text-align: center;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    background-image: none;
    white-space: nowrap;
    padding: 8px 8px;
    font-size: 12px;
    line-height: 1;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    font-family: Montserrat , arial;
    border: 1px solid transparent;
    margin-left: 9px;
}
</style>
</head>
<body>
<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->

<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200"> 
  

  
  <div id="wrapper"> 
      <!-- HEADER -->
    <header class="header">
      <div class="top-header">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <!--<div class="top-header__contacts"><i class="icon stroke icon-Phone2"></i>(123) 0800 12345</div>-->
              <div class="top-header__contacts"><i class="icon stroke icon-Mail"></i><a href="mailto:Info@learnezy.com">Info@learnezy.com</a></div>
              <div class="top-header__link">
                <button class="btn-header btn-effect">LATEST</button>
                <span>We have added new courses today ...</span> </div>
              <div class="header-login"> <a class="header-login__item" href="sign_in.html"><i class="icon stroke icon-User"></i>Sign In</a> <a class="header-login__item" href="register.html">Register</a> </div>
            </div>
            <!-- end col  --> 
          </div>
          <!-- end row  --> 
        </div>
        <!-- end container  --> 
      </div>
      <!-- end top-header  -->
      
      <div class="container">
        <div class="row">
          <div class="col-xs-12"> <a class="header-logo" href="index.html"><img class="header-logo__img" src="assets/img/logo.png" alt="Logo"></a>
            <div class="header-inner">
              <div class="header-search">
                <div class="navbar-search">
                  <form id="search-global-form">
                    <div class="input-group">
                      <input type="text" placeholder="Type your search..." class="form-control" autocomplete="off" name="s" id="search" value="">
                      <span class="input-group-btn">
                      <button type="reset" class="btn search-close" id="search-close"><i class="fa fa-times"></i></button>
                      </span> </div>
                  </form>
                </div>
              </div>
             
              <nav class="navbar yamm">
                <div class="navbar-header hidden-md  hidden-lg  hidden-sm ">
                  <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                </div>
                <div id="navbar-collapse-1" class="navbar-collapse collapse">
                  <ul class="nav navbar-nav">
                    <li class="dropdown"><a href="index.html">Home<span class="nav-subtitle">OUR World</span></a>                    </li>
                    <li class="dropdown"> <a href="courses_list.html">COURSES<span class="nav-subtitle">What We Offers</span></a>                    </li>
                   
                    <li><a href="contact.html">CONTACT<span class="nav-subtitle">say us hi</span></a></li>
                  </ul>
                </div>
              </nav>
              <!--end navbar --> 
              
            </div>
            <!-- end header-inner --> 
          </div>
          <!-- end col  --> 
        </div>
        <!-- end row  --> 
      </div>
      <!-- end container--> 
    </header>
    <!-- end header -->
    
   
    <div class="wrap-title-page">
      <div class="container">
        <div class="row">
          <div class=" col-xs-12">
            <h1 class="ui-title-page">All Courses</h1>
            
          </div>
          
          
           
        </div>
      </div>
      <!-- end container--> 
    </div>
    <!-- end wrap-title-page -->
   
    
    <div class="section-breadcrumb">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="wrap-breadcrumb clearfix">
              <ol class="breadcrumb">
                <li><a href="index.html"><i class="icon stroke icon-House"></i></a></li>
                <li><a href="javascript:void(0);">courses categories</a></li>
                <li class="active">all courses</li>
              </ol>
              <div class="sorting">
                
                 <!--search starts here-->
   
                             
                    
            
             <div class=" find-course_mod-b">
                                            <form method="post">
                                                <div class="form-group">
                                                    <label>Course Name</label>
                                                    <input type="text" class="input-text full-width" placeholder="Java"  />
                                                </div>
                                                <div class="form-group">
                                                    <label>Locality</label>
                                                   <input type="text" class="input-text full-width" placeholder="Rajajinagar"  />
                                                </div>
                                                <div class="form-group">
                                                    <label>City</label>
                                                      <input type="text" class="input-text full-width" placeholder="Rajajinagar"  />
                                                </div>
                                                
                                                <button  class="btn btn-primary btn-effect">SEARCH AGAIN</button>
                                            </form>
                                        </div>
                                        </div>
                                          <!--search ends here--> 
              </div>
              <!-- end sorting --> 
              
            </div>
              
          </div>
        </div>
        <!-- end row--> 
      </div>
      <!-- end container--> 
    </div>
    <!-- end section-breadcrumb-->
              
                                        
                                        
    <main class="main-content">
      <div class="container">
        <div class="row">
         <div class="col-md-2 filter-content">
          <div class="toggle-container filters-container">
                                       
                            
                                <div class="panel style1 arrow-right">
                                    <h4 class="panel-title collapsed">Mode
                                    </h4>
                                    <div id="accomodation-type-filter" class="panel-collapse collapse in">
                                        <div class="panel-content">
                                            <div class="checkbox">
							<label>
							  <input type="checkbox">eLearning<small>72</small>
							</label>
						</div>
                          <div class="checkbox">
							<label>
							  <input type="checkbox">Classroom<small>722</small>
							</label>
						</div> 
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="panel style1 arrow-right">
                                   <h4 class="panel-title collapsed">Cost
                                    </h4>
                                    <div id="amenities-filter" class="panel-collapse collapse in ">
                                        <div class="panel-content">
                                             <div class="checkbox">
							<label>
							  <input type="checkbox">Paid<small>722</small>
							</label>
						</div>
                          <div class="checkbox">
							<label>
							  <input type="checkbox">Free<small>722</small>
							</label>
						</div>
                          
                                        </div>
                                        
                                    </div>
                                </div>
                                
                                <div class="panel style1 arrow-right">
                                    <h4 class="panel-title collapsed">
                                         Level
                                    </h4>
                                    <div id="language-filter" class="panel-collapse collapse in">
                                        <div class="panel-content">
                                           <div class="checkbox">
							<label>
							  <input type="checkbox">Beginner<small>722</small>
							</label>
						</div>
						<div class="checkbox">
							<label>
							  <input type="checkbox">Intermediate<small>722</small>
							</label>
						</div>
						<div class="checkbox">
							<label>
							  <input type="checkbox">Advanced <small>722</small>
							</label>
						</div>
						
                                        </div>
                                        
                                    </div>
                                    
                                    
                                </div>
                                
                                
                                  <div class="panel style1 arrow-right">
                                     <h4 class="panel-title collapsed">Price
                                    </h4>
                                    <div id="language-filter1" class="panel-collapse collapse in ">
                                        <div class="panel-content">
                                           <div class="checkbox">
							<label>
							  <input type="checkbox"><i  class="fa fa-angle-left equal"></i><span class="equal">= </span>
<i class="fa fa-inr"></i> 500

							</label>
						</div>
						<div class="checkbox">
							<label>
							  <input type="checkbox"><i  class="fa fa-angle-right equal"></i>
<i class="fa fa-inr"></i> 500 &amp; <i  class="fa fa-angle-left equal"></i><span class="equal">= </span>
<i class="fa fa-inr"></i> 2000
							</label>
						</div>
						<div class="checkbox">
							<label>
							  <input type="checkbox"><i  class="fa fa-angle-right equal"></i>
<i class="fa fa-inr"></i> 2000 &amp; <i  class="fa fa-angle-left equal"></i><span class="equal">= </span>
<i class="fa fa-inr"></i> 5000
							</label>
						</div>
                        <div class="checkbox">
							<label>
							  <input type="checkbox"><i  class="fa fa-angle-right equal"></i>
<i class="fa fa-inr"></i> 5000 
							</label>
						</div>
						
                                        </div>
                                        
                                    </div>
                                    
                                    
                                </div>
                                
                                 <div class="panel style1 arrow-right">
                                   <h4 class="panel-title collapsed">Select Categories
                                    </h4>
                                    <div id="amenities-filter" class="panel-collapse collapse in ">
                                        <div class="panel-content">
                                          <select class="demo1" multiple="multiple">
    <optgroup label="Languages">
        <option value="cp">C++</option>
        <option value="cs">C#</option>
        <option value="oc">Object C</option>
        <option value="c">C</option>
    </optgroup>
    <optgroup label="Scripts">
        <option value="js">JavaScript</option>
        <option value="php">PHP</option>
        <option value="asp">ASP</option>
        <option value="jsp">JSP</option>
    </optgroup>
</select>
                          
                                        </div>
                                        
                                    </div>
                                </div>
                          </div>      
                          
                           <a href="javascript:void(0);" class="btn btn-primary btn-effect">Submit</a> <a href="javascript:void(0);" class="btn btn-primary btn-effect">Reset</a>          
         </div>
          <div class="col-md-10 col-sm-12">
        
           <div id="paginationdemo" class="demo">
           
            <div id="p1" class="pagedemo _current" style="">           
           
            <div class="wrap-title wrap-title_mod-b">
              <div class="title-list">Showing <span class="title-list__number">1 - 9</span> of  total <span class="title-list__number">50</span> Courses</div>  
            </div>
            <div class="sorting1"> <div  class="select jelect">
                  <input id="jelect" name="sorting" value="0" data-text="imagemin" type="text" class="jelect-input">
                  <div tabindex="0" role="button" class="jelect-current">Sort By</div>
                  <ul class="jelect-options">
                    <li class="jelect-option jelect-option_state_active">Ratings</li>
                    <li  class="jelect-option">Reviews</li>
                     <li  class="jelect-option">Price - Low to High</li>
                      <li  class="jelect-option">Price - High to Low</li>
                  </ul>
                </div>
                </div>
            <!-- end wrap-title -->
            <div style="clear:both;"></div>
            <div class="posts-wrap">
              <logic:notEmpty name="latestcourses">
             <logic:iterate id="latestcourses" name="latestcourses">
              <article class="post post_mod-c clearfix advantages__item wow zoomIn" data-wow-duration="2s">
                <div class="entry-media">
                  <div class="entry-thumbnail"> <a href="course-details.html" ><img class="img-responsive" src="<%=request.getContextPath()%>/complogos/<bean:write name="latestcourses"  property="courselatestlogo" />" width="370" height="250" alt="Foto"/></a> </div>
                </div>
                <div class="entry-main entry-main_mod-a">
                  <h3 class="entry-title ui-title-inner"><a href="course-details.html"> <bean:write name="latestcourses" property="coursenamelatest"/> </a></h3>
                  <div class="entry-meta decor decor_mod-b"> <span class="entry-autor"> <span>By </span> <a class="post-link" href="javascript:void(0);"><bean:write name="latestcourses" property="compname"/></a> </span> <span class="entry-date"><a href="javascript:void(0);"><bean:write name="latestcourses" property="cityname"/></a></span> </div>
                  <div class="stars"><ul class="rating">
                      <li><img src="<bean:write name="latestcourses" property="rating" />" /></li>
                  
                    </ul>
                   
                            
                            <span class="color"><i class="fa fa-inr"></i>
800</span>
                            </div>
                 
                  <div class="entry-footer">
                    <a href="javascript:void(0);" class="btn btn-primary btn-effect">View Details</a>                  </div>
                </div>
              </article>
              <!-- end post -->
                </logic:iterate>
              </logic:notEmpty>
              
             
             
          
           
              
            
            
         
         
            
              <!-- end post -->
            </div>
            </div>
            <!-- end posts-wrap -->
            </div> <!-- page 1 -->
            
            
            <div id="p2" class="pagedemo" style="display:none;">Page 2</div>
				<div id="p3" class="pagedemo" style="display:none;">Page 3</div>
				<div id="p4" class="pagedemo" style="display:none;">Page 4</div>
				<div id="p5" class="pagedemo" style="display:none;">Page 5</div>
				<div id="p6" class="pagedemo" style="display:none;">Page 6</div>
				<div id="p7" class="pagedemo" style="display:none;">Page 7</div>
				<div id="p8" class="pagedemo" style="display:none;">Page 8</div>
				<div id="p9" class="pagedemo" style="display:none;">Page 9</div>
				<div id="p10" class="pagedemo" style="display:none;">Page 10</div>
				<div id="demo5">                   
                </div>
            
            </div> <!-- pagination --> 
          </div>
          <!-- end col --> 
           </div>
        <!-- end row --> 
      </div>
      <!-- end container --> 
      
      
     
      
      
    </main>
    <!-- end main-content -->
    
    <footer class="footer">
      <div class="container">
        <div class="footer-inner border-decor_top">
          <div class="row">
            <div class="col-lg-3 col-sm-3">
              <section class="footer-section">
                <h3 class="footer-title">ABOUT US</h3>
                <a href="javascript:void(0);"><img class="footer-logo img-responsive" src="assets/img/logo_white.png" height="50" width="195" alt="Logo"></a>
                <div class="footer-info">Fusce eleifend. Donec sapien sed pha seah lusa. Pellentesque lu amus lorem arcu sem per duiac. Vivamus porta. Sed at nisl praesnt blandit mauris vel erat.</div>
                <div class="footer-contacts footer-contacts_mod-a"> <i class="icon stroke icon-Pointer"></i>
                  <address class="footer-contacts__inner">
                  370 Hill Park, Florida, USA
                  </address>
                </div>
                <div class="footer-contacts"> <i class="icon stroke icon-Phone2"></i> <span class="footer-contacts__inner">Call us 0800 12345</span> </div>
                <div class="footer-contacts"> <i class="icon stroke icon-Mail"></i> <a class="footer-contacts__inner" href="mailto:Info@academica.com">Info@academica.com</a> </div>
              </section>
              <!-- end footer-section --> 
            </div>
            <!-- end col -->
            
            <div class="col-lg-3 col-sm-3">
              <section class="footer-section">
                <h3 class="footer-title">LATEST TWEETS</h3>
                <div class="tweets">
                  <div class="tweets__text">What is the enemy of #creativity?</div>
                  <div><a href="javascript:void(0);">http://enva.to/hVl5G</a></div>
                  <span class="tweets__time">9 hours ago</span> </div>
                <div class="tweets">
                  <div class="tweets__text">An agile framework can produce the type of lean marketing essential for the digital age <a href="javascript:void(0);">@aholmes360 #IMDS15</a></div>
                  <span class="tweets__time">9 hours ago</span> </div>
                <a class="tweets__link" href="javascript:void(0);">Follow @Academica</a> </section>
              <!-- end footer-section --> 
            </div>
            <!-- end col -->
            
            <div class="col-lg-4 col-sm-3">
              <section class="footer-section">
                <h3 class="footer-title">QUICK CONTACT</h3>
                <form class="form">
                  <div class="form-group">
                    <input class="form-control" type="text" placeholder="Your Name">
                    <input class="form-control" type="email" placeholder="Email address">
                    <textarea class="form-control" rows="7" placeholder="Message"></textarea>
                    <button class="btn btn-primary btn-effect">SEND MESSSAGE</button>
                  </div>
                </form>
              </section>
              <!-- end footer-section --> 
            </div>
            <!-- end col --> 
          </div>
          <!-- end row --> 
        </div>
        <!-- end footer-inner -->
        
         <div class="row">
          <div class="col-xs-12">
            <div class="footer-bottom">
              <div class="copyright">Copyright � 2015 <a href="javascript:void(0);">Academica</a>, Online Learning  |  Created by <a href="javascript:void(0);">Templines</a></div>
              <ul class="social-links list-unstyled">
                <li><a class="icon fa fa-facebook" href="javascript:void(0);"></a></li>
                <li><a class="icon fa fa-twitter" href="javascript:void(0);"></a></li>
                <li><a class="icon fa fa-google-plus" href="javascript:void(0);"></a></li>
                <li><a class="icon fa fa-instagram" href="javascript:void(0);"></a></li>
                <li><a class="icon fa fa-linkedin" href="javascript:void(0);"></a></li>
                <li><a class="icon fa fa-youtube-play" href="javascript:void(0);"></a></li>
              </ul>
              
            </div>
             <!-- end footer-bottom --> 
          </div>
          <!-- end col --> 
        </div>
        <!-- end row --> 
      </div>
      <!-- end container --> 
    </footer>
  </div>
  <!-- end wrapper --> 
</div>
<!-- end layout-theme --> 
            <!-- SCRIPTS -->
 
<script src="assets/js/jquery-migrate-1.2.1.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<script src="assets/js/modernizr.custom.js"></script> 
<script src="assets/js/waypoints.min.js"></script> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> 

<!--THEME--> 
<script  src="assets/plugins/sliderpro/js/jquery.sliderPro.min.js"></script> 
<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script> 
<script src="assets/plugins/isotope/jquery.isotope.min.js"></script> 
<script src="assets/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script> 
<script src="assets/plugins/datetimepicker/jquery.datetimepicker.js"></script> 
<script src="assets/plugins/jelect/jquery.jelect.js"></script> 
<script src="assets/plugins/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js"></script> 
<script src="assets/js/cssua.min.js"></script> 
<script src="assets/js/wow.min.js"></script> 
<script src="assets/js/custom.min.js"></script>

<script src="js/fSelect.js"></script>
<script>
$(function() {
        $('.demo1').fSelect();
    });
</script>

		<script src="assets/js/jquery.paginate.js" type="text/javascript"></script>
		<script type="text/javascript">
		$(function() {
			
			$("#demo5").paginate({
				count 		: 10,
				start 		: 1,
				display     : 5,
				border					: true,
				border_color			: '#E84C3D',
				text_color  			: '#fff',
				background_color    	: '#E84C3D',	
				border_hover_color		: '#ccc',
				text_hover_color  		: '#000',
				background_hover_color	: '#fff', 
				images					: false,
				mouse					: 'press',
				onChange     			: function(page){
											$('._current','#paginationdemo').removeClass('_current').hide();
											$('#p'+page).addClass('_current').show();
										  }
			});
		});
		</script>
        <script type="text/javascript">
            // Setup plugin with default settings
            $(document).ready(function() {

                $.apScrollTop({
                    'onInit': function(evt) {
                        console.log('apScrollTop: init');
                    }
                });

                // Add event listeners
                $.apScrollTop().on('apstInit', function(evt) {
                    console.log('apScrollTop: init');
                });

                $.apScrollTop().on('apstToggle', function(evt, details) {
                    console.log('apScrollTop: toggle / is visible: ' + details.visible);
                });

                $.apScrollTop().on('apstCssClassesUpdated', function(evt) {
                    console.log('apScrollTop: cssClassesUpdated');
                });

                $.apScrollTop().on('apstPositionUpdated', function(evt) {
                    console.log('apScrollTop: positionUpdated');
                });

                $.apScrollTop().on('apstEnabled', function(evt) {
                    console.log('apScrollTop: enabled');
                });

                $.apScrollTop().on('apstDisabled', function(evt) {
                    console.log('apScrollTop: disabled');
                });

                $.apScrollTop().on('apstBeforeScrollTo', function(evt, details) {
                    console.log('apScrollTop: beforeScrollTo / position: ' + details.position + ', speed: ' + details.speed);

                    // You can return a single number here, which means that to this position
                    // browser window scolls to
                    /*
                    return 100;
                    */

                    // .. or you can return an object, containing position and speed:
                    /*
                    return {
                        position: 100,
                        speed: 100
                    };
                    */

                    // .. or do not return anything, so the default values are used to scroll
                });

                $.apScrollTop().on('apstScrolledTo', function(evt, details) {
                    console.log('apScrollTop: scrolledTo / position: ' + details.position);
                });

                $.apScrollTop().on('apstDestroy', function(evt, details) {
                    console.log('apScrollTop: destroy');
                });

            });


            // Add change events for options
            $('#option-enabled').on('change', function() {
                var enabled = $(this).is(':checked');
                $.apScrollTop('option', 'enabled', enabled);
            });

            $('#option-visibility-trigger').on('change', function() {
                var value = $(this).val();
                if (value == 'custom-function') {
                    $.apScrollTop('option', 'visibilityTrigger', function(currentYPos) {
                        var imagePosition = $('#image-for-custom-function').offset();
                        return (currentYPos > imagePosition.top);
                    });
                }
                else {
                    $.apScrollTop('option', 'visibilityTrigger', parseInt(value));
                }
            });

            $('#option-visibility-fade-speed').on('change', function() {
                var value = parseInt($(this).val());
                $.apScrollTop('option', 'visibilityFadeSpeed', value);
            });

            $('#option-scroll-speed').on('change', function() {
                var value = parseInt($(this).val());
                $.apScrollTop('option', 'scrollSpeed', value);
            });

            $('#option-position').on('change', function() {
                var value = $(this).val();
                $.apScrollTop('option', 'position', value);
            });
		</script><script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
            
              
</body>
</html>