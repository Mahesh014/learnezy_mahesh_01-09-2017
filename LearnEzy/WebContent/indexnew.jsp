<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body onload="location.hash = 'importantvalue';">
<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->

<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200"> 
  
  <div id="wrapper"> 
    
    <!-- HEADER -->
    <header class="header">
      <div class="top-header">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <!--<div class="top-header__contacts"><i class="icon stroke icon-Phone2"></i>(123) 0800 12345</div>-->
              <div class="top-header__contacts"><i class="icon stroke icon-Mail"></i><a href="mailto:Info@learnezy.com">Info@learnezy.com</a></div>
              <div class="top-header__link">
                <button class="btn-header btn-effect">LATEST</button>
                <span>We have added new courses today ...</span> </div>
              <div class="header-login"> <a class="header-login__item" href="sign_in.html"><i class="icon stroke icon-User"></i>Sign In</a> <a class="header-login__item" href="register.html">Register</a> </div>
            </div>
            <!-- end col  --> 
          </div>
          <!-- end row  --> 
        </div>
        <!-- end container  --> 
      </div>
      <!-- end top-header  -->
      
      <div class="container">
        <div class="row">
          <div class="col-xs-12"> <a class="header-logo" href="index.html"><img class="header-logo__img" src="assets/img/logo.png" alt="Logo"></a>
            <div class="header-inner">
              <div class="header-search">
                <div class="navbar-search">
                  <form id="search-global-form">
                    <div class="input-group">
                      <input type="text" placeholder="Type your search..." class="form-control" autocomplete="off" name="s" id="search" value="">
                      <span class="input-group-btn">
                      <button type="reset" class="btn search-close" id="search-close"><i class="fa fa-times"></i></button>
                      </span> </div>
                  </form>
                </div>
              </div>
              <a id="search-open" href="#fakelink"><i class="icon stroke icon-Search"></i></a> <a class="header-cart" href="/"> <i class="icon stroke icon-ShoppingCart"></i></a>
              <nav class="navbar yamm">
                <div class="navbar-header hidden-md  hidden-lg  hidden-sm ">
                  <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                </div>
                <div id="navbar-collapse-1" class="navbar-collapse collapse">
                  <ul class="nav navbar-nav">
                    <li class="dropdown"><a href="index.html">Home<span class="nav-subtitle">OUR World</span></a>
                     
                    </li>
                    <li class="dropdown"> <a href="courses_list.html">COURSES<span class="nav-subtitle">What We Offers</span></a>
                      
                    </li>
                   
                    <li><a href="contact.html">CONTACT<span class="nav-subtitle">say us hi</span></a></li>
                  </ul>
                </div>
              </nav>
              <!--end navbar --> 
              
            </div>
            <!-- end header-inner --> 
          </div>
          <!-- end col  --> 
        </div>
        <!-- end row  --> 
      </div>
      <!-- end container--> 
    </header>
    <!-- end header -->
<div class="main-content">
      <div id="sliderpro1" class="slider-pro main-slider">
        <div class="sp-slides">
          
          <div class="sp-slide"> <img class="sp-image" src="assets/media/main-slider/2.jpg"
					data-src="assets/media/main-slider/2.jpg"
					data-retina="assets/media/main-slider/2.jpg" alt="img"/>
            <div class="item-wrap sp-layer  sp-padding" data-horizontal="200" data-vertical="30"
					data-show-transition="left" data-hide-transition="up" data-show-delay="400" data-hide-delay="200">
              <div class="main-slider__inner">
                <div class="main-slider__title" ><h1>BEST ONLINE LEARNING</h1></div>
                <div class="main-slider__subtitle ">THE EASIER WAY</div>
                <a class="main-slider__btn btn btn-warning btn-effect" href="/">START A COURSE</a> </div>
            </div>
          </div>
          
          <div class="sp-slide"> <img class="sp-image" src="assets/media/main-slider/1.jpg"
					data-src="assets/media/main-slider/1.jpg"
					data-retina="assets/media/main-slider/1.jpg" alt="img"/>
            <div class="item-wrap sp-layer  sp-padding" data-horizontal="700" data-vertical="1"
					data-show-transition="left" data-hide-transition="up" data-show-delay="400" data-hide-delay="200">
              <div class="main-slider__inner text-center">
                <div class="main-slider__title" >BEST ONLINE LEARNING</div>
                <div class="main-slider__subtitle ">THE EASIER WAY</div>
                <a class="main-slider__btn btn btn-warning btn-effect" href="/">START A COURSE</a> </div>
            </div>
          </div>
          
          
        </div>
      </div>
      <!-- end main-slider -->
 <div class="section_find-course wow fadeInUp" data-wow-duration="2s">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <div class="find-course find-course_mod-b">
                <form class="find-course__form" action="get">
                  <div class="form-group"> <i class="icon stroke icon-Search"></i>
                    <input class="form-control" type="text" placeholder="Course Keyword ...">
                    <div class="jelect" >
                      <input value="0" type="text" class="jelect-input">
                      <div tabindex="0" role="button" class="jelect-current">All Categories</div>
                      <ul class="jelect-options">
                        <li  class="jelect-option jelect-option_state_active">Categorie 1</li>
                        <li class="jelect-option">Categorie 2</li>
                        <li  class="jelect-option">Categorie 3</li>
                      </ul>
                    </div>
                    <!-- end jelect --> 
                  </div>
                  <!-- end form-group -->
                  <div class="find-course__wrap-btn">
                    <button class="btn btn-info btn-effect">SEARCH COURSE</button>
                  </div>
                </form>
              </div>
              <!-- end find-course -->
              <div class="find-course__info">Search more than 50,000 online courses available from 2,000 Academies</div>
            </div>
            <!-- end col --> 
          </div>
          <!-- end row --> 
        </div>
        <!-- end container --> 
      </div>
      <!-- end section-default -->
      
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="border-decor_top">
              <section class="section-default">
                <div class="wrap-title wow zoomIn" data-wow-duration="2s">
                  <h2 class="ui-title-block ui-title-block_mod-d">We Bring You World's Best Courses From All Top Training Academies, <strong>FREE!</strong></h2>
                  <div class="ui-subtitle-block ui-subtitle-block_mod-c">Having over 9 million students worldwide and more than 50,000 online courses available.</div>
                </div>
                <!-- end wrap-title -->
                <ul class="advantages advantages_mod-b list-unstyled">
                  <li class="advantages__item wow zoomIn" data-wow-duration="2s">
                    <div class="advantages__inner">
                      <div class="advantages__info"> <span class="advantages__icon"><i class="icon stroke icon-Cup"></i></span>
                        <h3 class="ui-title-inner decor decor_mod-a">HIGHest RATED</h3>
                      </div>
                    </div>
                    <p>Fusce eleifend donec sapien phase dcua sed sa pellentesque lacus vamus lorem treb lum arcu semper duiac.</p>
                  </li>
                  <li class="advantages__item wow zoomIn" data-wow-duration="2s" data-wow-delay=".5s"> <span class="advantages__icon"><i class="icon stroke icon-Users"></i></span>
                    <div class="advantages__inner">
                      <h3 class="ui-title-inner decor decor_mod-a">SKILLED FACULTY</h3>
                      <div class="advantages__info">
                        <p>Fusce eleifend donec sapien phase dcua sed sa pellentesque lacus vamus lorem treb lum arcu semper duiac.</p>
                      </div>
                    </div>
                  </li>
                  <li class="advantages__item wow zoomIn" data-wow-duration="2s" data-wow-delay="1s"> <span class="advantages__icon"><i class="icon stroke icon-WorldGlobe"></i></span>
                    <div class="advantages__inner">
                      <h3 class="ui-title-inner decor decor_mod-a">we are GLOBAL</h3>
                      <div class="advantages__info">
                        <p>Fusce eleifend donec sapien phase dcua sed sa pellentesque lacus vamus lorem treb lum arcu semper duiac.</p>
                      </div>
                    </div>
                  </li>
                  <li class="advantages__item wow zoomIn" data-wow-duration="2s" data-wow-delay="1.5s"> <span class="advantages__icon"><i class="icon stroke icon-DesktopMonitor"></i></span>
                    <div class="advantages__inner">
                      <h3 class="ui-title-inner decor decor_mod-a">ONLINE TRAINING</h3>
                      <div class="advantages__info">
                        <p>Fusce eleifend donec sapien phase dcua sed sa pellentesque lacus vamus lorem treb lum arcu semper duiac.</p>
                      </div>
                    </div>
                  </li>
                </ul>
              </section>
              <!-- end section-advantages --> 
            </div>
            <!-- end border-decor_top --> 
          </div>
          <!-- end col --> 
        </div>
        <!-- end row --> 
      </div>
      <!-- end container -->
      
      

</body>
</html>