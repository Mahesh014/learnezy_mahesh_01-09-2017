<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui">
<title>LearnEzy.com - Home</title>
<link href="favicon.png" type="image/x-icon" rel="shortcut icon">
<link href="assets/css/master.css" rel="stylesheet">

<!-- BOOTSTRAP -->

<link href="assets/css/bootstrap-responsive.css" rel="stylesheet" media="screen">

<!-- FONT AWESOME -->
<link href="assets/css/font-awesome.min.css" rel="stylesheet" media="screen">

<!-- OWL CAROUSEL -->
<link href="assets/css/owl.carousel.css" rel="stylesheet">
<!-- BX SLIDER-->
<link href="assets/css/jquery.bxslider.css" rel="stylesheet" media="screen">
</head>
<body>
<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->

<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200"> 
  
  <div id="wrapper"> 
    
    <!-- HEADER -->
    <header class="header">
      <div class="top-header">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <!--<div class="top-header__contacts"><i class="icon stroke icon-Phone2"></i>(123) 0800 12345</div>-->
              <div class="top-header__contacts"><i class="icon stroke icon-Mail"></i><a href="mailto:Info@learnezy.com">Info@learnezy.com</a></div>
              <div class="top-header__link">
                <button class="btn-header btn-effect">LATEST</button>
                <span>We have added new courses today ...</span> </div>
              <div class="header-login"> <a class="header-login__item" href="sign_in.html"><i class="icon stroke icon-User"></i>Sign In</a> <a class="header-login__item" href="register.html">Register</a> </div>
            </div>
            <!-- end col  --> 
          </div>
          <!-- end row  --> 
        </div>
        <!-- end container  --> 
      </div>
      <!-- end top-header  -->

  
      <div class="container">
        <div class="row">
          <div class="col-xs-12"> <a class="header-logo" href="index.html"><img class="header-logo__img" src="assets/img/logo.png" alt="Logo"></a>
            <div class="header-inner">
              <div class="header-search">
                <div class="navbar-search">
                  <form id="search-global-form">
                    <div class="input-group">
                      <input type="text" placeholder="Type your search..." class="form-control" autocomplete="off" name="s" id="search" value="">
                      <span class="input-group-btn">
                      <button type="reset" class="btn search-close" id="search-close"><i class="fa fa-times"></i></button>
                      </span> </div>
                  </form>
                </div>
              </div>
 <a id="search-open" href="#fakelink"><i class="icon stroke icon-Search"></i></a> <a class="header-cart" href="/"> <i class="icon stroke icon-ShoppingCart"></i></a>
              <nav class="navbar yamm">
                <div class="navbar-header hidden-md  hidden-lg  hidden-sm ">
                  <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                </div>
                <div id="navbar-collapse-1" class="navbar-collapse collapse">
                  <ul class="nav navbar-nav">
                    <li class="dropdown"><a href="index.html">Home<span class="nav-subtitle">OUR World</span></a>
                     
                    </li>
                    <li class="dropdown"> <a href="courses_list.html">COURSES<span class="nav-subtitle">What We Offers</span></a>
                      
                    </li>
                   
                    <li><a href="contact.html">CONTACT<span class="nav-subtitle">say us hi</span></a></li>
                  </ul>
                </div>
              </nav>
              <!--end navbar --> 
              
            </div>
            <!-- end header-inner --> 
 </div>
          <!-- end col  --> 
        </div>
        <!-- end row  --> 
      </div>
      <!-- end container--> 
    </header>
    <!-- end header -->
 <div class="main-content">
      <div id="sliderpro1" class="slider-pro main-slider">
        <div class="sp-slides">
          
          <div class="sp-slide"> <img class="sp-image" src="assets/media/main-slider/2.jpg"
					data-src="assets/media/main-slider/2.jpg"
					data-retina="assets/media/main-slider/2.jpg" alt="img"/>
            <div class="item-wrap sp-layer  sp-padding" data-horizontal="200" data-vertical="30"
					data-show-transition="left" data-hide-transition="up" data-show-delay="400" data-hide-delay="200">
              <div class="main-slider__inner">
                <div class="main-slider__title" ><h1>BEST ONLINE LEARNING</h1></div>
                <div class="main-slider__subtitle ">THE EASIER WAY</div>
                <a class="main-slider__btn btn btn-warning btn-effect" href="/">START A COURSE</a> </div>
            </div>
          </div>
          
          <div class="sp-slide"> <img class="sp-image" src="assets/media/main-slider/1.jpg"
					data-src="assets/media/main-slider/1.jpg"
					data-retina="assets/media/main-slider/1.jpg" alt="img"/>
            <div class="item-wrap sp-layer  sp-padding" data-horizontal="700" data-vertical="1"
					data-show-transition="left" data-hide-transition="up" data-show-delay="400" data-hide-delay="200">
              <div class="main-slider__inner text-center">
                <div class="main-slider__title" >BEST ONLINE LEARNING</div>
                <div class="main-slider__subtitle ">THE EASIER WAY</div>
                <a class="main-slider__btn btn btn-warning btn-effect" href="/">START A COURSE</a> </div>
            </div>
          </div>
          
          
        </div>
      </div>
      <!-- end main-slider -->
<div class="section_find-course wow fadeInUp" data-wow-duration="2s">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <div class="find-course find-course_mod-b">
                <form class="find-course__form" action="get">
                  <div class="form-group"> <i class="icon stroke icon-Search"></i>
                    <input class="form-control" type="text" placeholder="Course Keyword ...">
                    <div class="jelect" >
                      <input value="0" type="text" class="jelect-input">
                      <div tabindex="0" role="button" class="jelect-current">All Categories</div>
                      <ul class="jelect-options">
                        <li  class="jelect-option jelect-option_state_active">Categorie 1</li>
                        <li class="jelect-option">Categorie 2</li>
                        <li  class="jelect-option">Categorie 3</li>
                      </ul>
                    </div>
                    <!-- end jelect --> 
                  </div>
                  <!-- end form-group -->
                  <div class="find-course__wrap-btn">
                    <button class="btn btn-info btn-effect">SEARCH COURSE</button>
                  </div>
                </form>
              </div>
              <!-- end find-course -->
              <div class="find-course__info">Search more than 50,000 online courses available from 2,000 Academies</div>
            </div>
            <!-- end col --> 
          </div>
          <!-- end row --> 
        </div>
        <!-- end container --> 
      </div>
      <!-- end section-default -->
 <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="border-decor_top">
              <section class="section-default">
                <div class="wrap-title wow zoomIn" data-wow-duration="2s">
                  <h2 class="ui-title-block ui-title-block_mod-d">We Bring You World's Best Courses From All Top Training Academies, <strong>FREE!</strong></h2>
                  <div class="ui-subtitle-block ui-subtitle-block_mod-c">Having over 9 million students worldwide and more than 50,000 online courses available.</div>
                </div>
                <!-- end wrap-title -->
 <ul class="advantages advantages_mod-b list-unstyled">
                  <li class="advantages__item wow zoomIn" data-wow-duration="2s">
                    <div class="advantages__inner">
                      <div class="advantages__info"> <span class="advantages__icon"><i class="icon stroke icon-Cup"></i></span>
                        <h3 class="ui-title-inner decor decor_mod-a">HIGHest RATED</h3>
                      </div>
                    </div>
                    <p>Fusce eleifend donec sapien phase dcua sed sa pellentesque lacus vamus lorem treb lum arcu semper duiac.</p>
                  </li>
                  <li class="advantages__item wow zoomIn" data-wow-duration="2s" data-wow-delay=".5s"> <span class="advantages__icon"><i class="icon stroke icon-Users"></i></span>
                    <div class="advantages__inner">
                      <h3 class="ui-title-inner decor decor_mod-a">SKILLED FACULTY</h3>
                      <div class="advantages__info">
                        <p>Fusce eleifend donec sapien phase dcua sed sa pellentesque lacus vamus lorem treb lum arcu semper duiac.</p>
                      </div>
                    </div>
                  </li>
 <li class="advantages__item wow zoomIn" data-wow-duration="2s" data-wow-delay="1s"> <span class="advantages__icon"><i class="icon stroke icon-WorldGlobe"></i></span>
                    <div class="advantages__inner">
                      <h3 class="ui-title-inner decor decor_mod-a">we are GLOBAL</h3>
                      <div class="advantages__info">
                        <p>Fusce eleifend donec sapien phase dcua sed sa pellentesque lacus vamus lorem treb lum arcu semper duiac.</p>
                      </div>
                    </div>
                  </li>
 <li class="advantages__item wow zoomIn" data-wow-duration="2s" data-wow-delay="1.5s"> <span class="advantages__icon"><i class="icon stroke icon-DesktopMonitor"></i></span>
                    <div class="advantages__inner">
                      <h3 class="ui-title-inner decor decor_mod-a">ONLINE TRAINING</h3>
                      <div class="advantages__info">
                        <p>Fusce eleifend donec sapien phase dcua sed sa pellentesque lacus vamus lorem treb lum arcu semper duiac.</p>
                      </div>
                    </div>
                  </li>
                </ul>
              </section>
<!-- end section-advantages --> 
            </div>
            <!-- end border-decor_top --> 
          </div>
          <!-- end col --> 
        </div>
        <!-- end row --> 
      </div>
      <!-- end container -->
      
 <div class="section-progress wow fadeInUp section-parallax"  data-speed="25"  data-wow-duration="2s">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <ul class="list-progress list-unstyled">
                <li class="list-progress__item"> <span class="chart label-chart decor decor_mod-c" data-percent="190"><i class="icon stroke icon-WorldWide"></i><span class="percent"></span> </span> <span class="list-progress__name">ACADEMIES</span> </li>
                <li class="list-progress__item"> <span class="chart label-chart decor decor_mod-c" data-percent="250000"><i class="icon stroke icon-Book"></i><span class="percent"></span> </span> <span class="list-progress__name">COURSE CATEGORIES</span> </li>
                <li class="list-progress__item"> <span class="chart label-chart decor decor_mod-c" data-percent="824"><i class="icon stroke icon-User"></i><span class="percent"></span> </span> <span class="list-progress__name">COURSES PUBLISHED</span> </li>
                <li class="list-progress__item"> <span class="chart label-chart decor decor_mod-c" data-percent="1500"><i class="icon stroke icon-Edit"></i><span class="percent"></span> </span> <span class="list-progress__name">STUDENTS</span> </li>
              </ul>
              <!-- end list-progress --> 
            </div>
            <!-- end col  --> 
          </div>
          <!-- end row  --> 
        </div>
        <!-- end container--> 
      </div>
      <!-- end section-progress-->

       <!--LATEST COURSES SECTION START-->
        <section class="gray-bg">
        	<div class="container">
            	<!--SECTION HEADER START-->
            	<div class="sec-header">
                <h2 class="ui-title-block ui-title-block_mod-e">Latest <strong> Courses</strong></h2>
              <div class="wrap-subtitle">
                <div class="ui-subtitle-block ui-subtitle-block_w-line">Check Our Featured Courses</div>
              </div>
                	
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
 <!--SECTION HEADER END-->
                <div class="customNavigation">
                    <a class="btn prev"><i class="fa fa-angle-left"></i></a>
                    <a class="btn next"><i class="fa fa-angle-right"></i></a>
                </div>
 <div id="owl-carousel" class="owl-carousel owl-theme">
                	<!--COURSE ITEM START-->
                    <div class="course">
                    	<div class="thumb">
                        	<a href="#"><img src="images/course1.jpg" alt=""></a>
                            <div class="price"><span>$</span>9</div>
                        </div>
<div class="text">
                        	<div class="head">
                            	<h4>Management</h4>
                                <div class="rating">
                                </div>
                                 </div>
                            <div class="course-name">
                            	<p>Build a Business Plan</p>
                                <span>$800</span>
                            </div>
                            <div class="course-detail-btn">
                            	<a href="#">Subscribe</a>
                                <a href="#">Detail</a>
                            </div>
                        </div>
                    </div>
                    <!--COURSE ITEM END-->
                    <!--COURSE ITEM START-->
                                 <div class="course">
                    	<div class="thumb">
                        	<a href="#"><img src="images/course2.jpg" alt=""></a>
                            <div class="price"><span>$</span>51</div>
                        </div>
                                
                                <div class="text">
                        	<div class="head">
                            	<h4>Music</h4>
                                <div class="rating">
                                </div>
                                </div>
                            <div class="course-name">
                            	<p>How to play the guitar</p>
                                <span>$800</span>
                            </div>
                            <div class="course-detail-btn">
                            	<a href="#">Subscribe</a>
                                <a href="#">Detail</a>
                            </div>
                        </div>
                    </div>
                    <!--COURSE ITEM END-->
                    <!--COURSE ITEM START-->
                                
 <div class="course">
                    	<div class="thumb">
                        	<a href="#"><img src="images/course3.jpg" alt=""></a>
                            <div class="price"><span>$</span>20</div>
                        </div>
                    	<div class="text">
                        	<div class="head">
                            	<h4>Marketing</h4>
                                <div class="rating">
                                </div>
                            </div>
                            <div class="course-name">
                            	<p>Marketing Management</p>
                                <span>$1200</span>
                            </div>
                            <div class="course-detail-btn">
                            	<a href="#">Subscribe</a>
                                <a href="#">Detail</a>
                            </div>
                        </div>
                    </div>
                     <!--COURSE ITEM END-->
                    <!--COURSE ITEM START-->
                    <div class="course">
                    	<div class="thumb">
                        	<a href="#"><img src="images/course4.jpg" alt=""></a>
                            <div class="price"><span>$</span>8</div>
                        </div>
                    	<div class="text">
                        	<div class="head">
                            	<h4>Holidays Education</h4>
                                <div class="rating">
                                </div>
                            </div>
                            <div class="course-name">
                            	<p>Holidays Education Course</p>
                                <span>$800</span>
                            </div>
                            <div class="course-detail-btn">
                            	<a href="#">Subscribe</a>
                                <a href="#">Detail</a>
                            </div>
                        </div>
                    </div>
                    
 <!--COURSE ITEM END-->
                    <!--COURSE ITEM START-->
                    <div class="course">
                    	<div class="thumb">
                        	<a href="#"><img src="images/course1.jpg" alt=""></a>
                            <div class="price"><span>$</span>15</div>
                        </div>
                    	<div class="text">
                        	<div class="head">
                            	<h4>Management</h4>
                            	<div class="rating">
                                </div>
                            </div>
                            <div class="course-name">
                            	<p>Build a Business Plan</p>
                                <span>$800</span>
                            </div>
                            <div class="course-detail-btn">
                            	<a href="#">Subscribe</a>
                                <a href="#">Detail</a>
                            </div>
                        </div>
                    </div>
                    <!--COURSE ITEM END-->
                    <!--COURSE ITEM START-->
                     <div class="course">
                    	<div class="thumb">
                        	<a href="#"><img src="images/course2.jpg" alt=""></a>
                            <div class="price"><span>$</span>63</div>
                        </div>
                    	<div class="text">
                        	<div class="head">
                            	<h4>Music</h4>
                                <div class="rating">
                                </div>
                           	</div>
                            <div class="course-name">
                            	<p>How to play the guitar</p>
                                <span>$800</span>
                            </div>
                            <div class="course-detail-btn">
                            	<a href="#">Subscribe</a>
                                <a href="#">Detail</a>
                            </div>
                        </div>
                    </div>
                     <!--COURSE ITEM END-->
                    <!--COURSE ITEM START-->
                    <div class="course">
                    	<div class="thumb">
                        	<a href="#"><img src="images/course3.jpg" alt=""></a>
                            <div class="price"><span>$</span>19</div>
                        </div>
                    	<div class="text">
                        	<div class="head">
                            	<h4>Marketing</h4>
                                <div class="rating">
                                </div>
                            </div>
                            <div class="course-name">
                            	<p>Marketing Management</p>
                                <span>$1200</span>
                            </div>
                            <div class="course-detail-btn">
                            	<a href="#">Subscribe</a>
                                <a href="#">Detail</a>
                            </div>
                        </div>
                    </div>
                    <!--COURSE ITEM END-->
                    <!--COURSE ITEM START-->
                     <div class="course">
                    	<div class="thumb">
                        	<a href="#"><img src="images/course4.jpg" alt=""></a>
                            <div class="price"><span>$</span>25</div>
                        </div>
                    	<div class="text">
                        	<div class="head">
                            	<h4>Holidays Education</h4>
                                <div class="rating">
                                </div>
                            </div>
                            <div class="course-name">
                            	<p>Holidays Education Course</p>
                                <span>$800</span>
                            </div>
                            <div class="course-detail-btn">
                            	<a href="#">Subscribe</a>
                                <a href="#">Detail</a>
                            </div>
                        </div>
                    </div>
                    <!--COURSE ITEM END-->
                    <!--COURSE ITEM START-->
                    <div class="course">
                    	<div class="thumb">
                        	<a href="#"><img src="images/course1.jpg" alt=""></a>
                            <div class="price"><span>$</span>13</div>
                        </div>
                    	<div class="text">
                        	<div class="head">
                            	<h4>Management</h4>
                                <div class="rating">
                                </div>
                            </div>
                            <div class="course-name">
                            	<p>Build a Business Plan</p>
                                <span>$800</span>
                            </div>
                            <div class="course-detail-btn">
                            	<a href="#">Subscribe</a>
                                <a href="#">Detail</a>
                            </div>
                        </div>
                    </div>
                     <!--COURSE ITEM END-->
                    <!--COURSE ITEM START-->
                    <div class="course">
                    	<div class="thumb">
                        	<a href="#"><img src="images/course2.jpg" alt=""></a>
                            <div class="price"><span>$</span>17</div>
                        </div>
                    	<div class="text">
                        	<div class="head">
                            	<h4>Music</h4>
                                <div class="rating">
                                </div>
                            </div>
                            <div class="course-name">
                            	<p>How to play the guitar</p>
                                <span>$800</span>
                            </div>
                            <div class="course-detail-btn">
                            	<a href="#">Subscribe</a>
                                <a href="#">Detail</a>
                            </div>
                        </div>
                    </div>
                     <!--COURSE ITEM END-->
                    <!--COURSE ITEM START-->
                    <div class="course">
                    	<div class="thumb">
                        	<a href="#"><img src="images/course3.jpg" alt=""></a>
                            <div class="price"><span>$</span>31</div>
                        </div>
                    	<div class="text">
                        	<div class="head">
                            	<h4>Marketing</h4>
                            	<div class="rating">
                                </div>
                            </div>
                            <div class="course-name">
                            	<p>Marketing Management</p>
                                <span>$1200</span>
                            </div>
                            <div class="course-detail-btn">
                            	<a href="#">Subscribe</a>
                                <a href="#">Detail</a>
                            </div>
                        </div>
                    </div>
                    <!--COURSE ITEM END-->
                    <!--COURSE ITEM START-->
                      <div class="course">
                    	<div class="thumb">
                        	<a href="#"><img src="images/course4.jpg" alt=""></a>
                            <div class="price"><span>$</span>36</div>
                        </div>
                    	<div class="text">
                        	<div class="head">
                            	<h4>Holidays Education</h4>
                                <div class="rating">
                                </div>
                            </div>
                            <div class="course-name">
                            	<p>Holidays Education Course</p>
                                <span>$800</span>
                            </div>
                            <div class="course-detail-btn">
                            	<a href="#">Subscribe</a>
                                <a href="#">Detail</a>
                            </div>
                        </div>
                    </div>
                    <!--COURSE ITEM END-->
                </div>
            </div>
        </section>
        <!--LATEST COURSES SECTION END-->
       <section class="">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <div class="section__inner">
                   <!--COURSES TOPIC SECTION START-->
        <section class="gray-bg tabs-section">
        	<div class="container">
            	<!--SECTION HEADER START-->
            	<div class="sec-header">
                 <h2 class="ui-title-block ui-title-block_mod-e">Course <strong>Categories</strong></h2>
              <div class="wrap-subtitle">
                <div class="ui-subtitle-block ui-subtitle-block_w-line">Discover courses by topic</div>
              </div>
                	
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <!--SECTION HEADER END-->
                
            </div>
            <div class="course-tabs">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                  <li class="active"><a data-toggle="tab" href="#web"><i class="fa fa-book"></i>Web Design</a></li>
                  <li><a data-toggle="tab" href="#music"><i class="fa fa-bell-o"></i>Web Development</a></li>
                  <li><a data-toggle="tab" href="#math"><i class="fa fa-bolt"></i>Mobile App Development</a></li>
                  <li><a data-toggle="tab" href="#Production"><i class="fa fa-bullhorn"></i>SEO</a></li>
                  <li><a data-toggle="tab" href="#Photography"><i class="fa fa-camera"></i>Software Testing</a></li>
                  <li><a data-toggle="tab" href="#Geography"><i class="fa fa-cubes"></i>Soft Skills</a></li>
                  <li><a data-toggle="tab" href="#Arts"><i class="fa fa-link"></i>Arts &amp; Crafts </a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content container">
                  <div id="web" class="tab-pane fade active in">
                    <ul class="course-topics row">
                    	<!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic1.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>HTML5 Programming</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                        <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic2.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>CSS3</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                        <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic3.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>Adobe Dreamweaver</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                        <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic4.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>Adobe Photoshop</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                        <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic5.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>Javascript</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                        <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic6.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>Jquery</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                    </ul>
                  </div>
      <div id="music" class="tab-pane fade">
                    <ul class="course-topics row">
                    	<!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic4.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>Core Java</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                        <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic5.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>J2EE</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                        <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic6.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>Struts Framework</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                    	<!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic1.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>HTML5 Programming</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                        <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic2.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>Javascript</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                        <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic3.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>Sping / Hybernet</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                    </ul>
                  </div>
                  <div id="math" class="tab-pane fade">
                    <ul class="course-topics row">
                    	 <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic2.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>Android</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                        <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic3.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>iPhone</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                        <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic4.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>Windows</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                    	<!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic1.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>Phonegap</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                        <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic5.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>Web Services - SOAP</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                        <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic6.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>Web Services </h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                    </ul>
                  </div>
                  <div id="Production" class="tab-pane fade">
                    <ul class="course-topics row">
                    	<!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic5.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>On-Page Optimization</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                        <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic6.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>Off-Page Optimization</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                    	<!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic1.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>Digital Marketing</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                        <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic2.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>HTML5 Programming</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                        <!--LIST ITEM START-->
      <li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic3.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>HTML5 Programming</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                        <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic4.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>HTML5 Programming</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                    </ul>
                  </div>
                  <div id="Photography" class="tab-pane fade">
                    <ul class="course-topics row">
                    	<!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic2.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>Manual Testing</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                    	<!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic1.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>Automation Testing</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                         <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic4.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>HTML5 Programming</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                        <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic3.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>HTML5 Programming</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                        <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic6.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>HTML5 Programming</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                        <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic5.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>HTML5 Programming</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                    </ul>
                  </div>
       <div id="Geography" class="tab-pane fade">
                    <ul class="course-topics row">
                        <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic3.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>Personality Development</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                        <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic4.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>Communication</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                        <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic1.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>HTML5 Programming</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                        <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic2.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>HTML5 Programming</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                        <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic5.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>HTML5 Programming</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                        <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic6.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>HTML5 Programming</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                    </ul>
                  </div>
      <div id="Arts" class="tab-pane fade">
                    <ul class="course-topics row">
                    	<!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic1.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>HTML5 Programming</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                        <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic2.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>HTML5 Programming</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                        <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic3.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>HTML5 Programming</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                        <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic4.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>HTML5 Programming</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                        <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic5.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>HTML5 Programming</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                        <!--LIST ITEM START-->
                    	<li class="span4">
                        	<div class="thumb">
                            	<a href="#"><img src="images/topic6.jpg" alt=""></a>
                            </div>
                            <div class="text">
                            	<h4>HTML5 Programming</h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                                <span>$ 199</span>
                            </div>
                        </li>
                        <!--LIST ITEM START-->
                    </ul>
                  </div>
                </div>
            </div>
        </section>
        <!--COURSES TOPIC SECTION END-->
      
      
                
              </div>
            </div>
            <!-- end col --> 
          </div>
          <!-- end row --> 
        </div>
        <!-- end of container -->
      </section>
      <!-- end section_mod-d -->
      
      
      
      <section class="section_mod-c wow bounceInUp section-categories" data-wow-duration="2s">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <h2 class="ui-title-block ui-title-block_mod-e">Our <strong>Academies</strong></h2>
              <div class="wrap-subtitle">
                <div class="ui-subtitle-block ui-subtitle-block_w-line">We have very skilled and professionals for taking classes</div>
              </div>
              <div class="carousel_mod-a owl-carousel owl-theme enable-owl-carousel"
											data-min480="2"
											data-min768="3"
											data-min992="4"
											data-min1200="4"
											data-pagination="true"
											data-navigation="false"
											data-auto-play="4000"
											data-stop-on-hover="true">
                <div class="staff">
                  <div class="staff__media"><img class="img-responsive" src="assets/media/institutes/1.jpg" height="250" width="270" alt="foto">
                    <div class="staff__hover"></div>
                  </div>
                  <div class="staff__inner">
                    <h3 class="staff__title">Itech Solutions</h3>
                    <div class="staff__categories"> <a class="post-link" href="javascript:void(0);">Rajajinagar</a></div>
                    <div class="staff__description">Easellus luctus nib pulvinar bien quam ligula sapien condime</div>
                   
                  </div>
                </div>
                <div class="staff">
                  <div class="staff__media"><img class="img-responsive" src="assets/media/institutes/1.jpg" height="250" width="270" alt="foto">
                    <div class="staff__hover"></div>
                  </div>
                  <div class="staff__inner">
                    <h3 class="staff__title">Itech Solutions</h3>
                    <div class="staff__categories"> <a class="post-link" href="javascript:void(0);">Rajajinagar</a></div>
                    <div class="staff__description">Easellus luctus nib pulvinar bien quam ligula sapien condime</div>
                
                  </div>
                </div>
                <div class="staff">
                  <div class="staff__media"><img class="img-responsive" src="assets/media/institutes/1.jpg" height="250" width="270" alt="foto">
                    <div class="staff__hover"></div>
                  </div>
                  <div class="staff__inner">
                    <h3 class="staff__title">Itech Solutions</h3>
                    <div class="staff__categories"> <a class="post-link" href="javascript:void(0);">Rajajinagar</a></div>
                    <div class="staff__description">Easellus luctus nib pulvinar bien quam ligula sapien condime</div>
                    
                  </div>
                </div>
                <div class="staff">
                  <div class="staff__media"><img class="img-responsive" src="assets/media/institutes/1.jpg" height="250" width="270" alt="foto">
                    <div class="staff__hover"></div>
                  </div>
                  <div class="staff__inner">
                   <h3 class="staff__title">Itech Solutions</h3>
                    <div class="staff__categories"> <a class="post-link" href="javascript:void(0);">Rajajinagar</a></div>
                    <div class="staff__description">Easellus luctus nib pulvinar bien quam ligula sapien condime</div>
                    
                  </div>
                </div>
                <div class="staff">
                  <div class="staff__media"><img class="img-responsive" src="assets/media/institutes/1.jpg" height="250" width="270" alt="foto">
                    <div class="staff__hover"></div>
                  </div>
                  <div class="staff__inner">
                    <h3 class="staff__title">Itech Solutions</h3>
                    <div class="staff__categories"> <a class="post-link" href="javascript:void(0);">Rajajinagar</a></div>
                    <div class="staff__description">Easellus luctus nib pulvinar bien quam ligula sapien condime</div>
                   
                  </div>
                </div>
                <div class="staff">
                  <div class="staff__media"><img class="img-responsive" src="assets/media/institutes/1.jpg" height="250" width="270" alt="foto">
                    <div class="staff__hover"></div>
                  </div>
                  <div class="staff__inner">
                  <h3 class="staff__title">Itech Solutions</h3>
                    <div class="staff__categories"> <a class="post-link" href="javascript:void(0);">Rajajinagar</a></div>
                    <div class="staff__description">Easellus luctus nib pulvinar bien quam ligula sapien condime</div>
                   
                  </div>
                </div>
                <div class="staff">
                  <div class="staff__media"><img class="img-responsive" src="assets/media/institutes/1.jpg" height="250" width="270" alt="foto">
                    <div class="staff__hover"></div>
                  </div>
                  <div class="staff__inner">
                    <h3 class="staff__title">Itech Solutions</h3>
                    <div class="staff__categories"> <a class="post-link" href="javascript:void(0);">Rajajinagar</a></div>
                    <div class="staff__description">Easellus luctus nib pulvinar bien quam ligula sapien condime</div>
                    
                  </div>
                </div>
                <div class="staff">
                  <div class="staff__media"><img class="img-responsive" src="assets/media/institutes/1.jpg" height="250" width="270" alt="foto">
                    <div class="staff__hover"></div>
                  </div>
                  <div class="staff__inner">
                    <h3 class="staff__title">Itech Solutions</h3>
                    <div class="staff__categories"> <a class="post-link" href="javascript:void(0);">Rajajinagar</a></div>
                    <div class="staff__description">Easellus luctus nib pulvinar bien quam ligula sapien condime</div>
                    
                  </div>
                </div>
              </div>
            </div>
            <!-- end col --> 
          </div>
          <!-- end row --> 
        </div>
        <!-- end container --> 
      </section>
      <!-- end section_mod-c -->
                    
      <div class="container">
        <div class="border-decor_bottom">
          <div class="section-default">
            <div class="row">
              <div class="col-md-4">
                <section class="section-area wow bounceInRight courses" data-wow-duration="2s">
                  <div class="title-w-icon"> <i class="icon stroke icon-Agenda"></i>
                    <h2 class="ui-title-inner">UPCOMING COURSES</h2>
                  </div>
                  <article class="post post_mod-e clearfix">
                    <div class="box-date"><span class="number">25</span>AUG</div>
                    <div class="entry-main">
                      <h3 class="entry-title entry-title_mod-a"><a href="javascript:void(0);">How To Learn Online</a></h3>
                      <div class="entry-content">
                        <p>Eleifend donec sapien sed usaer tesqed Lorem ipsum dolor sit ban.</p>
                      </div>
                      <div class="entry-footer">
                        <div class="entry-links entry-links_mod-a"><i class="icon stroke icon-Time"></i>starting <a class="post-link" href="javascript:void(0);">07:30 am</a></div>
                      </div>
                    </div>
                  </article>
                  <!-- end post -->
                  
                  <article class="post post_mod-e clearfix">
                    <div class="box-date"><span class="number">25</span>AUG</div>
                    <div class="entry-main">
                      <h3 class="entry-title entry-title_mod-a"><a href="javascript:void(0);">How To Learn Online</a></h3>
                      <div class="entry-content">
                        <p>Eleifend donec sapien sed usaer tesqed Lorem ipsum dolor sit ban.</p>
                      </div>
                      <div class="entry-footer"> <span class="entry-time"><i class="icon stroke icon-Time"></i>starting <a class="post-link" href="javascript:void(0);">07:30 am</a></span> </div>
                    </div>
                  </article>
                  <!-- end post --> 
                </section>
                <!-- end section-area --> 
              </div>
              <!-- end col -->
              
              <div class="col-md-4 wow bounceInRight courses" data-wow-duration="2s" data-wow-delay=".7s">
                <div class="title-w-icon"> <i class="icon stroke icon-User"></i>
                  <h2 class="ui-title-inner">Featured ACADEMY</h2>
                </div>
                <article class="post post_mod-f">
                  <div class="entry-media">
                    <div class="entry-thumbnail"><img class="img-responsive" src="assets/media/posts/160x125/1.jpg" width="160" height="125" alt="Foto"> </div>
                  </div>
                  <div class="entry-main">
                    <h3 class="entry-title entry-title_mod-a"><a href="javascript:void(0);">ANDREW FINCH</a></h3>
                    <div class="entry-meta"><a class="post-link" href="javascript:void(0);">Developer</a></div>
                    <div class="entry-content">
                      <p>Phasellus luctus pulvin g bienum aliquam lig sape conde lorem ipsum dolor met cons letua.</p>
                      <p>Eeniam quis nostrud exercitation ullamco labor nisut alq gxe commodo consequat duis aute re dola. Lorem ipsum dolor sit amt consectetur adipisng elit.</p>
                    </div>
                  </div>
                  <a href="javascript:void(0);" class="post-btn btn btn-primary btn-effect">FULL PROFILE</a> </article>
                <!-- end post --> 
              </div>
              <!-- end col -->
              
              <div class="col-md-4 wow bounceInRight" data-wow-duration="2s" data-wow-delay="1.4s">
                <div class="title-w-icon"> <i class="icon stroke icon-Book"></i>
                  <h2 class="ui-title-inner">RECENT COURSES</h2>
                </div>
                <article class="post post_mod-g clearfix">
                  <div class="entry-media">
                    <div class="entry-thumbnail"> <a href="javascript:void(0);" ><img class="img-responsive" src="assets/media/posts/100x90/1.jpg" width="100" height="90" alt="Foto"/></a> </div>
                  </div>
                  <div class="entry-main">
                    <h3 class="entry-title entry-title_mod-a"><a href="javascript:void(0);">Programming for Everybody</a></h3>
                    <div class="entry-meta"> <span class="entry-autor"> <span>By </span> <a class="post-link" href="javascript:void(0);">John Milton</a> </span> <span class="entry-date"><a href="javascript:void(0);">Dec 16, 2015</a></span> </div>
                    <div class="entry-footer">
                      <ul class="rating">
                        <li><i class="icon fa fa-star"></i></li>
                        <li><i class="icon fa fa-star"></i></li>
                        <li><i class="icon fa fa-star"></i></li>
                        <li><i class="icon fa fa-star"></i></li>
                        <li><i class="icon fa fa-star-o"></i></li>
                      </ul>
                    </div>
                  </div>
                  <!-- end entry-main --> 
                </article>
                <!-- end post -->
                <article class="post post_mod-g clearfix">
                  <div class="entry-media">
                    <div class="entry-thumbnail"> <a href="javascript:void(0);" ><img class="img-responsive" src="assets/media/posts/100x90/2.jpg" width="100" height="90" alt="Foto"/></a> </div>
                  </div>
                  <div class="entry-main">
                    <h3 class="entry-title entry-title_mod-a"><a href="javascript:void(0);">Programming for Everybody</a></h3>
                    <div class="entry-meta"> <span class="entry-autor"> <span>By </span> <a class="post-link" href="javascript:void(0);">John Milton</a> </span> <span class="entry-date"><a href="javascript:void(0);">Dec 16, 2015</a></span> </div>
                    <div class="entry-footer">
                      <ul class="rating">
                        <li><i class="icon fa fa-star"></i></li>
                        <li><i class="icon fa fa-star"></i></li>
                        <li><i class="icon fa fa-star"></i></li>
                        <li><i class="icon fa fa-star"></i></li>
                        <li><i class="icon fa fa-star-o"></i></li>
                      </ul>
                    </div>
                  </div>
                  <!-- end entry-main --> 
                </article>
                <!-- end post --> 
              </div>
              <!-- end col --> 
            </div>
            <!-- end row --> 
          </div>
          <!-- end section-default --> 
        </div>
        <!-- end border-decor_bottom --> 
      </div>
      <!-- end container -->
      
      <div class="section-subscribe wow fadeInUp" data-wow-duration="2s">
        <div class="subscribe">
          <div class="container">
            <div class="row">
              <div class="col-sm-6">
                <div class="subscribe__icon-wrap"> <i class="icon_bg stroke icon-Imbox"></i><i class="icon stroke icon-Imbox"></i> </div>
                <div class="subscribe__inner">
                  <h2 class="subscribe__title">STAY UPDATED WITH LEARNEZY</h2>
                  <div class="subscribe__description">Nulla feugiat nibh placerat fermentum rutrum ante risus euismod</div>
                </div>
              </div>
              <!-- end col -->
              <div class="col-sm-6">
                <form class="subscribe__form" action="get">
                  <input class="subscribe__input form-control" type="text" placeholder="Your Email address ...">
                  <button class="subscribe__btn btn btn-success btn-effect">SUBSCRIBE</button>
                </form>
              </div>
              <!-- end col --> 
            </div>
            <!-- end row --> 
          </div>
          <!-- end container --> 
        </div>
      </div>
      <!-- end section-subscribe -->
    
      <div class="container">
        <div class="row">
          <div class="border-decor_top">
            <div class="col-md-6">
              <section class="section-default wow bounceInLeft" data-wow-duration="2s">
                <h2 class="ui-title-block">What <strong>Students Say</strong></h2>
                <div class="slider-reviews owl-carousel owl-theme owl-theme_mod-c enable-owl-carousel"
												data-single-item="true"
												data-auto-play="7000"
												data-navigation="true"
												data-pagination="false"
												data-transition-style="fade"
												data-main-text-animation="true"
												data-after-init-delay="4000"
												data-after-move-delay="2000"
												data-stop-on-hover="true">
                  <div class="reviews">
                    <div class="reviews__text">Nulla feugiat nibh placerat fermentum rutrum ante risus euismod eros  pharetra felis justo ac tortor. Maecenas odio aco sit amet odio euismo ac donec tellus. Nullam risus turpis rhoncus vel varius consequat laort  neque. Sed ipseget lectus vitae augue zitae ipsumd do eiusmod tempor incididunt ut labore et dolore magaliqua. Ut enim ad minim veniam quis nostrud exercitation ullamco laboris aliqup.</div>
                    <div class="reviews__img"><img class="img-responsive" src="assets/media/avatar_review/1.jpg" height="60" width="60" alt="foto"></div>
                    <span class="reviews__autor">-- JOHN SMITH</span> <span class="reviews__categories">(Maths Student)</span> </div>
                  <!-- end reviews -->
                  
                  <div class="reviews">
                    <div class="reviews__text">Nulla feugiat nibh placerat fermentum rutrum ante risus euismod eros  pharetra felis justo ac tortor. Maecenas odio aco sit amet odio euismo ac donec tellus. Nullam risus turpis rhoncus vel varius consequat laort  neque. Sed ipseget lectus vitae augue zitae ipsumd do eiusmod tempor incididunt ut labore et dolore magaliqua. Ut enim ad minim veniam quis nostrud exercitation ullamco laboris aliqup.</div>
                    <div class="reviews__img"><img class="img-responsive" src="assets/media/avatar_review/2.jpg" height="60" width="60" alt="foto"></div>
                    <span class="reviews__autor">-- JOHN SMITH</span> <span class="reviews__categories">(Maths Student)</span> </div>
                  <!-- end reviews -->
                  
                  <div class="reviews">
                    <div class="reviews__text">Nulla feugiat nibh placerat fermentum rutrum ante risus euismod eros  pharetra felis justo ac tortor. Maecenas odio aco sit amet odio euismo ac donec tellus. Nullam risus turpis rhoncus vel varius consequat laort  neque. Sed ipseget lectus vitae augue zitae ipsumd do eiusmod tempor incididunt ut labore et dolore magaliqua. Ut enim ad minim veniam quis nostrud exercitation ullamco laboris aliqup.</div>
                    <div class="reviews__img"><img class="img-responsive" src="assets/media/avatar_review/3.jpg" height="60" width="60" alt="foto"></div>
                    <span class="reviews__autor">-- JOHN SMITH</span> <span class="reviews__categories">(Maths Student)</span> </div>
                  <!-- end reviews --> 
                </div>
                <!-- end slider-reviews --> 
              </section>
              <!-- end section-default --> 
            </div>
            <!-- end col -->
            
            <div class="col-md-6">
              <section class="section-default wow bounceInRight courses" data-wow-duration="2s">
                <h2 class="ui-title-block">Why <strong>Learnezy ?</strong></h2>
                <div class="panel-group accordion accordion" id="accordion-1">
                  <div class="panel panel-default">
                    <div class="panel-heading"> <a class="btn-collapse" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-1"><i class="icon"></i></a>
                      <h3 class="panel-title">Learn And Get Training From Experts</h3>
                    </div>
                    <div id="collapse-1" class="panel-collapse collapse in">
                      <div class="panel-body">
                        <p class="ui-text">Fusce eleifend. Donec sapien sed phase lusa . Pellentesque lacus.Vivamus lorem arcu semper duiac. Cras ornare arcu vamus nda leo. Etiam ind arc morbi justo mauris temd pus pharetra interdum at congue semper purus.</p>
                      </div>
                    </div>
                  </div>
                  <!-- end panel -->
                  
                  <div class="panel">
                    <div class="panel-heading"> <a class="btn-collapse collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-2"><i class="icon"></i></a>
                      <h3 class="panel-title">Enjoy Our Free Online Courses</h3>
                    </div>
                    <div id="collapse-2" class="panel-collapse collapse">
                      <div class="panel-body">
                        <p class="ui-text">Fusce eleifend. Donec sapien sed phase lusa . Pellentesque lacus.Vivamus lorem arcu semper duiac. Cras ornare arcu vamus nda leo. Etiam ind arc morbi justo mauris temd pus pharetra interdum at congue semper purus.</p>
                      </div>
                    </div>
                  </div>
                  <!-- end panel -->
                  
                  <div class="panel">
                    <div class="panel-heading"> <a class="btn-collapse collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-3"><i class="icon"></i></a>
                      <h3 class="panel-title">Learn Anytime & Anywhere</h3>
                    </div>
                    <div id="collapse-3" class="panel-collapse collapse">
                      <div class="panel-body">
                        <p class="ui-text">Fusce eleifend. Donec sapien sed phase lusa . Pellentesque lacus.Vivamus lorem arcu semper duiac. Cras ornare arcu vamus nda leo. Etiam ind arc morbi justo mauris temd pus pharetra interdum at congue semper purus.</p>
                      </div>
                    </div>
                  </div>
                  <!-- end panel -->
                  
                  <div class="panel">
                    <div class="panel-heading"> <a class="btn-collapse collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-4"><i class="icon"></i></a>
                      <h3 class="panel-title">Basic to Advance: We Teach Everything</h3>
                    </div>
                    <div id="collapse-4" class="panel-collapse collapse">
                      <div class="panel-body">
                        <p class="ui-text">Fusce eleifend. Donec sapien sed phase lusa . Pellentesque lacus.Vivamus lorem arcu semper duiac. Cras ornare arcu vamus nda leo. Etiam ind arc morbi justo mauris temd pus pharetra interdum at congue semper purus.</p>
                      </div>
                    </div>
                  </div>
                  <!-- end panel --> 
                </div>
                <!-- end accordion --> 
              </section>
            </div>
            <!-- end col --> 
          </div>
          <!-- end section-default --> 
        </div>
        <!-- end row --> 
      </div>
      <!-- end container -->
    
     <div class="tweets" style="background:#6BC868;">
            <div class="container">
            	<div class="tweet-contant">
                	<i class="fa fa-twitter"></i>
                    <h4>Weekly Updates</h4>
                    <ul class="bxslider">
                        <li>
                            <p>Are you a morning person or is the night time the right time? Interesting perspectives on the forum - <a href="#">http://t.co/tdEHlbZf</a></p>
                        </li>
                        <li>
                            <p>Dolor donec sagittis sapien. Ante aptent feugiat adipisicing. Duis int. - <a href="#">http://t.co/tdEHlbZf</a></p>
                        </li>
                        <li>
                            <p>Duis interdum olor donec sagittis sapien. Ante aptent feugiat adipisicing - <a href="#">http://t.co/tdEHlbZf</a></p>
                        </li>
                        
                    </ul>
                </div>
            </div>
        </div>
    
    
    
      
    </div>
    <!-- end main-content -->
    
    <footer class="footer wow fadeInUp" data-wow-duration="2s">
      <div class="container">
        <div class="footer-inner">
          <div class="row">
            <div class="col-lg-3 col-sm-3">
              <section class="footer-section courses">
                <h3 class="footer-title">ABOUT US</h3>
               
                <div class="footer-info">Fusce eleifend. Donec sapien sed pha seah lusa. Pellentesque lu amus lorem arcu sem per duiac. Vivamus porta. Sed at nisl praesnt blandit mauris vel erat.</div>
                <!--<div class="footer-contacts footer-contacts_mod-a"> <i class="icon stroke icon-Pointer"></i>
                  <address class="footer-contacts__inner">
                  370 Hill Park, Florida, USA
                  </address>
                </div>-->
                <!--<div class="footer-contacts"> <i class="icon stroke icon-Phone2"></i> <span class="footer-contacts__inner">Call us 0800 12345</span> </div>-->
                <div class="footer-contacts"> <i class="icon stroke icon-Mail"></i> <a class="footer-contacts__inner" href="mailto:Info@learnezy.com">Info@learnezy.com</a> </div>
              </section>
              <!-- end footer-section --> 
            </div>
            <!-- end col -->
            
            <div class="col-lg-2 col-sm-3">
              <section class="footer-section courses">
                <h3 class="footer-title">USEFUL LINKS</h3>
                <ul class="footer-list list-unstyled">
                 <li class="footer-list__item"><a class="footer-list__link" href="academies.html">Our Academies</a></li>
                  <li class="footer-list__item"><a class="footer-list__link" href="courses_list.html">Our Latest Courses</a></li>
                  <li class="footer-list__item"><a class="footer-list__link" href="about.html">Who We Are</a></li>
                  <li class="footer-list__item"><a class="footer-list__link" href="contact.html">Get In Touch</a></li>
                  
                  <li class="footer-list__item"><a class="footer-list__link" href="faqs.html">Support & FAQ's</a></li>
                  <li class="footer-list__item"><a class="footer-list__link" href="terms.html">Terms & Conditions</a></li>
                  <li class="footer-list__item"><a class="footer-list__link" href="privacy_policy.html">Privacy Policy</a></li>
                </ul>
              </section>
              <!-- end footer-section --> 
            </div>
            <!-- end col -->
            
            <div class="col-lg-3 col-sm-3">
              <section class="footer-section courses">
                <h3 class="footer-title">LATEST TWEETS</h3>
                <div class="tweets">
                  <div class="tweets__text">What is the enemy of #creativity?</div>
                  <div><a href="javascript:void(0);">http://enva.to/hVl5G</a></div>
                  <span class="tweets__time">9 hours ago</span> </div>
                <div class="tweets">
                  <div class="tweets__text">An agile framework can produce the type of lean marketing essential for the digital age <a href="javascript:void(0);">@aholmes360 #IMDS15</a></div>
                  <span class="tweets__time">9 hours ago</span> </div>
                <a class="tweets__link" href="javascript:void(0);">Follow @Learnezy</a> </section>
              <!-- end footer-section --> 
            </div>
            <!-- end col -->
            
            <div class="col-lg-4 col-sm-3">
              <section class="footer-section">
                <h3 class="footer-title">QUICK CONTACT</h3>
                <form class="form">
                  <div class="form-group">
                    <input class="form-control" type="text" placeholder="Your Name">
                    <input class="form-control" type="email" placeholder="Email address">
                    <textarea class="form-control" rows="7" placeholder="Message"></textarea>
                    <button class="btn btn-primary btn-effect">SEND MESSSAGE</button>
                  </div>
                </form>
              </section>
              <!-- end footer-section --> 
            </div>
            <!-- end col --> 
          </div>
          <!-- end row --> 
        </div>
        <!-- end footer-inner -->
        
        <div class="row">
          <div class="col-xs-12">
            <div class="footer-bottom">
              <div class="copyright">Copyright © 2016 <a href="javascript:void(0);">LearnEzy</a>, Online Learning  |  Designed &amp; Developed by <a href="http://www.itechsolutions.in">Itech Solutions</a></div>
              <ul class="social-links list-unstyled">
                <li><a class="icon fa fa-facebook" href="https://www.facebook.com/learneazy/" target="_blank"></a></li>
                <li><a class="icon fa fa-twitter" href="javascript:void(0);" target="_blank"></a></li>
                <li><a class="icon fa fa-google-plus" href="https://plus.google.com/u/0/107536091050129859190/" target="_blank"></a></li>
               
                <li><a class="icon fa fa-linkedin" href="javascript:void(0);" target="_blank"></a></li>
                <li><a class="icon fa fa-youtube-play" href="https://www.youtube.com/channel/UC-WxseNjJRYJHlJQNX7U9Jw/videos" target="_blank"></a></li>
                 <li><a class="icon fa fa-pinterest-p" href="https://in.pinterest.com/learnezy/" target="_blank"></a></li>
                  <li><a class="icon fa fa-stumbleupon" href="http://www.stumbleupon.com/stumbler/learnezymail" target="_blank"></a></li>
                  <li><a class="icon fa fa-tumblr" href="http://www.tagged.com/profile.html?dataSource=Profile&ll=nav" target="_blank"></a></li>
                 
                 
              </ul>
            </div>
            <!-- end footer-bottom --> 
          </div>
          <!-- end col --> 
        </div>
        <!-- end row --> 
      </div>
      <!-- end container --> 
    </footer>
      </div>
  <!-- end wrapper --> 
</div>
<!-- end layout-theme --> 

<!-- Jquery Lib -->
<script src="assets/js/jquery-1.10.1.min.js"></script>

<!-- SCRIPTS --> 
<script src="assets/js/jquery-migrate-1.2.1.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<script src="assets/js/modernizr.custom.js"></script> 
<script src="assets/js/waypoints.min.js"></script> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> 

<!--THEME--> 
<script  src="../assets/plugins/sliderpro/js/jquery.sliderPro.min.js"></script> 
<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script> 
<script src="assets/plugins/isotope/jquery.isotope.min.js"></script> 
<script src="assets/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script> 
<script src="assets/plugins/datetimepicker/jquery.datetimepicker.js"></script> 
<script src="assets/plugins/jelect/jquery.jelect.js"></script> 
<script src="assets/plugins/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js"></script> 
<script src="assets/js/cssua.min.js"></script> 
<script src="assets/js/wow.min.js"></script> 
<script src="assets/js/custom.min.js"></script>


<!-- Bootstrap -->
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/owl.carousel.js"></script>
<script src="assets/js/functions.js"></script>
<script src="assets/js/jquery.bxslider.min.js"></script>

      
                    

</body>
</html>