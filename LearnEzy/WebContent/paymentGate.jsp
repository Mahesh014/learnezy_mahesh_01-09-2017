<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Advanced Java Training, dot net c# training, SAP training, Soft skills training for freshers - itech training</title>
<meta name="description" content="itech is one of the best Java, SAP training institute in bangalore, advanced Java training, SAP  training, Soft skills training, real-time working professionals with handful years of experience" />

<meta name="keywords" content="Java training in Bangalore, advanced Java training institutes Bangalore,  SAP  training, seo training,  technical writing training,  Soft skills training best Java training center in Bangalore" />

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300' rel='stylesheet' type='text/css'>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/carousel.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/cmsstyle.css" rel="stylesheet" type="text/css" media="all" />
<script src="js/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="js/bootstrap.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/owl.carousel.js" type="text/javascript"></script>
  <script>
    $(document).ready(function() {

      var owl = $("#owl-demo");

      owl.owlCarousel({

      items :4, //10 items above 1000px browser width
      itemsDesktop : [1000,4], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
      itemsTablet: [600,2], //2 items between 600 and 0;
      itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
      
      });

      // Custom Navigation Events
      $(".next").click(function(){
        owl.trigger('owl.next');
      })
      $(".prev").click(function(){
        owl.trigger('owl.prev');
      })

    });
    </script>
  <script type="text/javascript">
  // Login Form
	$(function() {
	    var button = $('#loginButton');
	    var box = $('#loginBox');
	    var form = $('#loginForm');
	    button.removeAttr('href');
	    button.mouseup(function(login) {
	        box.toggle();
	        button.toggleClass('active');
	    });
	    form.mouseup(function() { 
	        return false;
	    });
	    $(this).mouseup(function(login) {
	        if(!($(login.target).parent('#loginButton').length > 0)) {
	            button.removeClass('active');
	            box.hide();
	        }
	    });
	});
  </script>
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57957829-1', 'auto');
  ga('send', 'pageview');

</script>
  <style>
  .details{
  width:60%;
  margin:0 auto;
  border:1px solid #f90;
 text-align:center;
  }
.details tr td{padding:10px;
 border:1px solid #f90;}
 
 
 
 h2 {
    text-align: center;}
    .about-desc h2{text-align:center;}

.details1{ width:60%;
  margin:10px auto;
   text-align:center;
  }
   .login a{  width: 67px;
  float: right;
  color: #000;
  margin-top: 22px;
  border: 1px solid #FFA500;
  padding: 6px;
  border-radius: 8px;
 }
  </style>
  
  
</head>
<body>


 <!-- Start Header -->
         <div class="header">	
   	 	     
             <div class="header-logo-nav">
             	  <div class="navbar navbar-inverse navbar-static-top nav-bg" role="navigation">
				      <div class="container">
				        <div class="navbar-header">
				           
				         <div class="logo"> <a class="navbar-brand" href="http://www.itechtraining.in/"><img src="images/Itech Solutions logo.png" alt="" /></a></div>
				        
				        </div>
				        <div class="login">
                    
                  <!-- <a  href="login.jsp"> LOGIN</a>-->
                    
                    </div><!--login-->
				      </div>
				    </div>
		         <div class="clear"></div>
                  <div class="header-bottom">
                 <div class="container">
                 <div class="time">
                  <p><div id="clockbox"></div></p>
                 <script src="js/dynamicClock.js" type="text/javascript"></script>
                 </div>
               
                  
                 </div>
                 </div>
                 <div class="clear"></div>
	        </div>
	        <div class="header-banner">
	        	
			    
	           </div>
	          
             </div>
   <!-- End Header -->
   <!-- End Header -->
    <!-- Start Main Content -->
	 <div class="main" >
	 	 <div class="wrap">
	 	 	 <div class="col-lg-12">
	 	  <div class="about-desc">
	 		 <h2> 
	 		Payment Details</h2>
	 		<table  class="details" cellpadding="10" cellspacing="10" border="1"  width="60%" >
				<thead>
  <tr>
			<td>SI.NO</td>
			<td>Selected Course</td>
			<td>Fees in Rupees</td>
			 
		</tr>
		</thead>
		<tbody>
		<logic:notEmpty name="selectedcourselist">
		<logic:iterate id="selectedcourselist" name="selectedcourselist" indexId="index">
			<tr height="30">

				<td><%=index + 1%></td>
				<td><bean:write name="selectedcourselist" property="coursename" /></td>
	 <td><bean:write name="selectedcourselist" property="courseAmount" /></td>
			</tr>
		</logic:iterate>
	</logic:notEmpty>
	</tbody>
	<tr height="30">
				<td colspan="2"  >Sub Total</td>
	 <td><%=session.getAttribute("subAmount") %></td>
			</tr>
			
				<tr height="30" >
				<td colspan="2" >Service Tax @ <%=session.getAttribute("serviceTax") %> % </td>
	 <td><%=session.getAttribute("tax") %></td>
			</tr>
			
			
				<tr height="30">
				<td colspan="2" >Discount Amount</td>
	 <td><%=session.getAttribute("discountAmount") %></td>
			</tr>
			
			<tr height="30">
				<td colspan="2" >Grand Total</td>
	 <td><%=session.getAttribute("totalAmount") %></td>
			</tr>
			
	</table><br><br>
	 		 <form action="pgi.jsp">
	 		 
	 	   <table class="details" cellpadding="10" cellspacing="10" border="1"  width="60%">
                   <p style="color:red; text-align:center;"><logic:notEmpty name="status"><bean:write name="status"/> </logic:notEmpty></p><br></br>
                    <tr>
                             <td> Email-id </td> <td><%=session.getAttribute("userName") %></td>
                            </tr>
 <tr>
                             <td>Course List </td> <td><%=session.getAttribute("courseList") %></td>
                            </tr>
 <tr>
                             <td>Course Name  </td><td><%=session.getAttribute("courseNames") %></td>
                            </tr>
 <tr>
                             <td>Discount in % </td><td> <%=session.getAttribute("discountPercentage") %></td>
                            </tr>
                    
                    
                    <tr>
                             <td> Course ID </td>
                             <input type="hidden" name = "couid" value=" <%=request.getAttribute("course") %>"></input>
                             <td > <%=(String)request.getAttribute("course") %></td>
                            </tr>
                            <tr>
                             <td >Userid </td>
                             <input type="hidden" name="userid" value = "<%=session.getAttribute("userId") %>"></input>
                             <td class="text"> <%=session.getAttribute("userId") %> </td>
                            </tr>
                            
                             <tr>
                             <td > Total Amount </td>
                             <input type="hidden" name="totamt" value="<%=request.getAttribute("tamt") %>">
                             <td ><%=request.getAttribute("tamt") %></td>
                             
                            </tr>
                            <tr>
                             <td >transactionID</td>
                             <td ><%=session.getAttribute("transactionID") %></td>
                            </tr>
                            <tr>
                             <td> subAmount </td>
                             <td > <%=session.getAttribute("subAmount") %></td>
                            </tr>
                            <tr>
                             <td > discountAmount </td>
                             <td><input type="hidden" value="disamt" value="<%=session.getAttribute("discountAmount") %>" ></input></td>
                             <td> <%=session.getAttribute("discountAmount") %></td>
                            </tr>
                            <tr>
                             <td >  serviceTax </td>
                             <td><input type="hidden" name="stax" value="<%=session.getAttribute("serviceTax") %>"></input> </td>
                             <td ><%=session.getAttribute("serviceTax") %></td>
                            </tr>
                            
                            
                    
                          
                         
                          </table>
                          <table align="center" class="details1" cellpadding="10" cellspacing="10" border="0"  width="60%">
                          <tr><td>
                                <input style="background:#f90; border-radius:5px; border:1px solid #f90;"  type="submit" value="Confirm And Proceed" align="middle">
                              </td></tr>
                               </table>
                            
      </form>
		   
          </div>
		  </div> 	   
		       </div>
		
		  
             
	 </div>
   
    
   <!-- End Main Content -->
	     <div style="clear:both;"></div>
   <!-- Start Footer -->
     <jsp:include page="jsp/common/student_footer.jsp" /> 
  <!-- End Footer -->
</body>
</html>