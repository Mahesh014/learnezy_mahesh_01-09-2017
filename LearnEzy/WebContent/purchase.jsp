<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>


<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui">
		<title>Learnezy, Confirm Purchase</title>
		<link href="favicon.png" type="image/x-icon" rel="shortcut icon">
		<link href="assets/css/master.css" rel="stylesheet">
        <link href="css/purchase.css" rel="stylesheet">

	 <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-2.1.4.js"></script>

<script type="text/javascript">

$(document).ready(function()

		{
$("#submitid").click(function()
		{

	var userid;
	var statutsid=$("#statutsid").val();
	
	
	
		var emailid=$("#emailid").val();
		var courseid=$("#courseid").val();

		
		
		$.ajax({	
			type: "GET",		
			url:"<%=request.getContextPath()%>/studentRegistration.do?action=addcourses",
			data:{emailid:emailid,courseid:courseid},
			success: function(r){	
				var json= JSON.parse(r);
				
				$.each(json,function(i,obj){
					alert(obj.status);
					$("#statutsid").val(obj.status);
					$("#userid").val(obj.userid);
					userid=obj.userid;	
					alert("userid"+userid);			
					
				
					
					
				});
			} 
			
		});
var status=$("#statutsid").val();
if(status=="")
{
	var userid=$("#userid").val();
	
	
	var courseid=$("#courseid").val();
	var totalidamount=$("#totalidamount").val();
	var tax=$("#taxes").val();

	$.ajax({	
		type: "GET",		
		url:"<%=request.getContextPath()%>/studentRegistration.do?action=addtrasnsactiondetails",
		data:{userid:userid,courseid:courseid,totalidamount:totalidamount,tax:tax},
		success: function(r){	
			var json= JSON.parse(r);
			
			$.each(json,function(i,obj){
				alert(obj.transactionstatus);
				return true;
			
				
				
			});
		} 
		
	});

}
		





		
			});




		});


</script>
</head>

<body>

<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->
		<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200">


			<div id="wrapper">

			                    <jsp:include page="commonindexheader.jsp" /> 
			

				<div class="wrap-title-page">
					<div class="container">
						<div class="row">
							<div class="col-xs-12"><h1 class="ui-title-page">Confirm Purchase</h1></div>
						</div>
					</div><!-- end container-->
				</div><!-- end wrap-title-page -->


				<div class="section-breadcrumb">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div class="wrap-breadcrumb clearfix">
									<ol class="breadcrumb">
										<li><a href="index.html"><i class="icon stroke icon-House"></i></a></li>
										<li class="active">Confirm Purchase</li>
									</ol>
								</div>
							</div>
						</div><!-- end row -->
					</div><!-- end container -->
				</div><!-- end section-breadcrumb -->


				<main class="main-content">

					<section class="about rtd">
						<div class="container">
								<form action="pg1.jsp"  method="post" id="prodForm"> 
							
                            <div class="row">
                            
                            
                            <div class="payment-v4" data-purpose="payment-screen"> 
                            
                            <div class="inline-loader-holder none" style="height:100%" ng-class="{none: !loadingIndicator.open}"> 
                            
                            <div style="background-color:rgba(0,0,0,.2);margin:0;padding:0;width:100%;height:100%;position:absolute;z-index:100"> 
                            
                            <span class="ajax-loader-small" style="top:50%"></span> </div> </div> <h3 class="general-heading" translate=""><span>Selected Course</span></h3>
                            
                       <div class="payment-top">	<logic:notEmpty name="course"> <div class="fwx fxac original-price-wrapper top-row"> 
                       
                       
                       
                       <div class="original-price-left top-left fx" data-purpose="buyable-title"> Course Name </div>
                       
                        <div class="original-price-right top-right"> <!-- ngIf: viewModel.buyable.license.id --><bean:write name="course"></bean:write> </div> </div> 
                        
                        		</logic:notEmpty>
                   	<logic:notEmpty name="price">
                        
              <div class="redeem-wrapper top-row" data-purpose="discount-info" ng-if="(viewModel.price_info.has_total_amount || viewModel.discount_info) &amp;&amp; viewModel.buyable.coupons_available"> <coupon-code-input discount-for="viewModel.buyable" discount-info="viewModel.discount_info">
              <div class="fxw fxac"> <div class="redeem-left fx"> 
                       
                       <div class="redeem-submitted fxax" ng-if="hasValidDiscount()"> <div> <span translate=""><span>Price</span></span> <b class="applied-code"></b></div> </div>
                       
                       
                  </div> <div class="redeem-right" ng-if="hasValidDiscount()"> <bean:write name="price"></bean:write> </div> </div> 
                  
                  </coupon-code-input> </div>
                  <div class="redeem-wrapper top-row">
                  
                  <div class="fxw fxac"> <div class="redeem-left fx"> 
                       
                       <div class="redeem-submitted fxax"> <div> <span translate=""><span>Sub Total</span></span> <b class="applied-code"></b></div> </div>
                       
                       
                  </div> <div class="redeem-right" id="subtotal" ></div> </div> 
                  </div>
                                    <div class="redeem-wrapper top-row">
                  
                  <div class="fxw fxac"> <div class="redeem-left fx"> 
                       
                       <div class="redeem-submitted fxax"> <div> <span translate=""><span>Service Tax @ 6%</span></span> <b class="applied-code"></b></div> </div>
                       
                       
                  </div> <div class="redeem-right" id="servicetax" ></div> </div> 
                  </div>
                  
                  
                       <div class="redeem-wrapper top-row">
                  
                  <div class="fxw fxac"> <div class="redeem-left fx"> 
                       
                       <div class="redeem-submitted fxax"> <div> <span translate=""><span>Total Amount Discount@ </span></span> <b class="applied-code"></b></div> </div>
                       
                       
                  </div> <div class="redeem-right" id="totalamountdiscount" >  </div> </div> 
                  </div>
                  
                       <div class="redeem-wrapper top-row">
                  
                  <div class="fxw fxac"> <div class="redeem-left fx"> 
                       
                       <div class="redeem-submitted fxax"> <div> <span translate=""><span>Total Amount	</span></span> <b class="applied-code"></b></div> </div>
                       
                       
                  </div> <div class="redeem-right" id="totalamount" >  </div> </div> 
                  </div>
                  
                  
                      
                  
                  
                  
                  <input type="hidden" id="statutsid">
                  
                   <input type="hidden" id="taxes">
                  
                  
                   <input type="hidden" name="totamt" id="totalidamount" > </logic:notEmpty><logic:notEmpty name="emailid">
		<input type="hidden" name="userid"  value="10">
	
	<input type="hidden" name="email" id="emailid" value=<bean:write name="emailid"></bean:write>></input>
		<input type="hidden" id="courseid" name="courseid"  value=<bean:write name="couseid"></bean:write>></input>
		<input type="hidden"  name="serviceTax"  id="serviceTax"  value="<bean:write name="serviceTax" />"    />
	<div id="appendid"></div>
	</logic:notEmpty>     
    
                   </div> 
                                               
                            <div class="main-row tac fxac fxjc"> <button type="submit" id="submitid"  class="btn btn-lg btn-primary btn-block bold border-radius-5 paypal-button" rel="nofollow"> <span translate=""><span>Go to Paypal</span></span> </button> </div>
                            
                            
                            </div>
                            
                            
						</div><!-- end container -->
						</form>
					</section><!-- end about -->


				</main><!-- end main-content -->


				          <jsp:include page="Commonfooter.jsp"></jsp:include>
				
			</div><!-- end wrapper -->
		</div><!-- end layout-theme -->


		<!-- SCRIPTS --> 
<script src="assets/js/jquery-migrate-1.2.1.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<script src="assets/js/modernizr.custom.js"></script> 
<script src="assets/js/waypoints.min.js"></script> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> 

<!--THEME--> 
<script  src="assets/plugins/sliderpro/js/jquery.sliderPro.min.js"></script> 
<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script> 
<script src="assets/plugins/isotope/jquery.isotope.min.js"></script> 
<script src="assets/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script> 
<script src="assets/plugins/datetimepicker/jquery.datetimepicker.js"></script> 
<script src="assets/plugins/jelect/jquery.jelect.js"></script> 
<script src="assets/plugins/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js"></script> 
<script src="assets/js/cssua.min.js"></script> 
<script src="assets/js/wow.min.js"></script> 
<script src="assets/js/custom.min.js"></script> 
<script>
$(document).ready(function()
		{

			var courseId=$("#courseid").val();
			var serviceTax=$("#serviceTax").val();
			alert(serviceTax);
			$.ajax({
                url: "CourseMaster.do?action=courseFees",
                type: "GET",
                dataType: "json",
                data: {courseId: courseId},
                success: function(res) {
                	
                	var subAmount=0;
                	var taxAmount=0;
                	var total=0;
                	var disAmount=0;
	                $.each(res,function(i,obj){
		               
		               
		                subAmount=parseInt(subAmount)+parseInt(obj.fees);


						$("#subtotal").append(subAmount);
						
						if(obj.totalDiscountAmount!=0){ disAmount= parseInt(disAmount)+parseInt(parseInt(obj.fees)*(parseInt(obj.totalDiscountAmount)/parseInt(100)));  };
						alert("disamount"+disAmount);
						alert("servicetax"+disAmount);
						  taxAmount=parseInt(parseInt(subAmount)*(parseInt(serviceTax)/parseInt(100)));
			                total=parseInt(subAmount)+parseInt(taxAmount)-parseInt(disAmount);
		                     $("#servicetax").append(taxAmount);
		                     $("#taxes").val(taxAmount);
			                $("#totalamountdiscount").append(disAmount);
			                $("#totalamount").append(total);
			                $("#totalidamount").val(total);
			               
			                


						});
	              
	             
	                
					
                }
            });
		
});

</script>

</body>
</html>
