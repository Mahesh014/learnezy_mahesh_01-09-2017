<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui">
<title>LearnEzy.com - Sign in</title>
<link href="favicon.png" type="image/x-icon" rel="shortcut icon">
<link href="assets/css/master.css" rel="stylesheet">

<!-- BOOTSTRAP -->
<link href="assets/css/bootstrap-responsive.css" rel="stylesheet" media="screen">

<!-- FONT AWESOME -->
<link href="assets/css/font-awesome.min.css" rel="stylesheet" media="screen">

<!-- OWL CAROUSEL -->
<link href="assets/css/owl.carousel.css" rel="stylesheet">
<!-- BX SLIDER-->
<link href="assets/css/jquery.bxslider.css" rel="stylesheet" media="screen">
<style>
select {
    width: 220px;
    background-color: #ffffff;
    border: 1px solid #cccccc;
}
select, input[type="file"] {
    height: 30px;
    *margin-top: 4px;
    line-height: 30px;
}
textarea {
    height: auto;
        border: 1px solid #cccccc;
        margin-bottom:10px;
    
}

</style>
<script src="<%=request.getContextPath()%>/js/jquery-2.1.1.min.js"></script>
<script src="<%=request.getContextPath()%>/js/additional-methods1.js"></script>

<script src="<%=request.getContextPath()%>/js/validation/jquery.validate.min.js"></script>


<script>
$(document).ready(function()
		{
    $.ajax({	
		type: "GET",		
		url:"DynamicData.do?action=getcity",
		async:false,
		success: function(r){	
			
			var json= JSON.parse(r);
			var firstOption= $("<option>",{text:"----Select----",value:""});
			$("#countryid").append(firstOption);
			
			
			$.each(json,function(i,obj){
				var option= $("<option>",{text:obj.city,value:obj.city});
				$("#countryid").append(option);
		
			
			});
		} 
	});
	


    
		
	});	


$(document).ready(function()
		{
	
	$("#enquiryform").validate( {
		rules : {
		firstname : {
				required : true
			},
			
			email  : {
				required : true,
				email: true
			},
			mobilenumber:{
				required : true,
				digits : true,
				minlength :10
				
			},
			city:{
				required :true
				
				
			},
			gender:{
				required :true
				
				
			},
			level : {
				required : true,
				minlength : 3
			},
			age : {
				required : true,
				digits : true,
				minlength : 1,
				maxlength : 3
	}
			
	},
		messages : {
		firstname : {
				required : "First Name required!"
			},
			
			email  : {
				required :"Email Id is required!",
				email : "Please Enter Valid email Id"
			},
			mobilenumber:{
				required : "Mobile Number required!",
				digits : "Please enter numbers",
				minlength : "10 number Minimum",
				
			},
			city:{
				required :"Please Select City "
			},
			gender:{
				required :"Please Select Gender "
			},
			
			level : {
				required : "Education level required!",
				minlength : "3 Letters Minimum"
			},
			age : {
				required : "age is required!",
				digits : "Please enter numbers",
				minlength : "1 number Minimum",
				maxlength : "3 numbers Maximum"
			}
			
		}
	});


		});


</script>
</head>

<body> 

<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->

<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200"> 
  
  <div id="wrapper"> 
                        <jsp:include page="commonindexheader.jsp" /> 
    
        <%
    String course=request.getParameter("course");

    %>
     <div class="wrap-title-page">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h1 class="ui-title-page">Enquiry Form</h1>
          </div>
        </div>
      </div>
      <!-- end container--> 
    </div>
    <!-- end wrap-title-page -->
    
    <div class="section-breadcrumb">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="wrap-breadcrumb clearfix">
              <ol class="breadcrumb">
                <li><a href="index.html"><i class="icon stroke icon-House"></i></a></li>
                <li class="active">Enquiry Form</li>
                
              </ol>
            </div>
          </div>
        </div>
        <!-- end row --> 
      </div>
      <!-- end container --> 
    </div>
    <!-- end section-breadcrumb -->
       
    
    <div class="main-content">
          
          
    <div class="container">
   
            
            
        <div class="row">
            	<div class="span12">
                	<div class="form-box">
                	<%
   				 Object msg=request.getAttribute("varName");
   				 if(msg==null)
  				  {
      			  msg="";
   				  }
   			 %>
   			 <p><font color="red"><%= msg %></font></p>
                        <form action="DynamicData.do?action=enquiry" method="post" id="enquiryform">
                        <div class="form-body">
							<fieldset>
								
								<div class="row-fluid">
									<div class="span6">
										<label>First Name*</label>
										
										<input type="hidden" name="course" value=<%=course%>></input>
										<input type="text" name="firstname" placeholder="Enter your First Name" class="input-block-level">
									</div>
									<div class="span6">
										<label>Last Name</label>
										<input type="text" name="lastname" placeholder="Enter your Last Name" class="input-block-level">
									</div>
								</div>
								<div class="row-fluid">
									<div class="span6">
										<label>Enter Email*</label>
										<input type="text" name="email" placeholder="Enter your E-mail ID" class="input-block-level">
									</div>
									<div class="span6">
										<label>Enter Phone number*</label>
										<input type="text" name="mobilenumber" maxlength="12" placeholder="Enter your Phone Number" class="input-block-level">
									</div>
								</div>
								<div class="row-fluid">
									<div class="span6">
									
										<label>Select Your City</label>
										<select name="city" class="input-block-level" id="countryid"></select>
									</div>
									<div class="span6">
										<label>Gender</label>
										<select name="gender" class="input-block-level">
											<option value="">----select-----</option>
										    <option value="Male">Male</option>
											<option value="Female">Female</option>
										</select>
									</div>
								</div>
								<div class="row-fluid">
									<div class="span6">
										<label>Education Level</label>
										<input type="text" name="level" placeholder="Enter your Education Level" class="input-block-level">
									</div>
									<div class="span6">
										<label>Age</label>
										<input type="text" name="age" placeholder="Enter your age" class="input-block-level">
									</div>
								</div>
							</fieldset>
                        </div>
                        <div class="footer">
                        	<h2>Your Preferences</h2>
                           
                            <div class="row-fluid">
                                <div class="span12">
                                    <label>Additional Message</label>
                                    <textarea name="message" placeholder="Your Message" class="input-block-level" rows="8"></textarea>
                                </div>
                                <p class="pull-left">By registering, You accept Terms &amp; Conditions</p>
                                <button type="submit" class="btn-style pull-right">Submit</button>
                            </div>
                        </div>
                        
                        </form>
                    </div>
                </div>
                        	</div>
        
        </div>   <!-- container -->
        
      
    </div>
    <!-- end main-content -->
              <jsp:include page="Commonfooter.jsp"></jsp:include>
    
  </div>
  <!-- end wrapper --> 
</div>
<!-- end layout-theme --> 

<!-- Jquery Lib -->

<!-- SCRIPTS --> 
<script src="assets/js/jquery-migrate-1.2.1.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<script src="assets/js/modernizr.custom.js"></script> 
<script src="assets/js/waypoints.min.js"></script> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> 

<!--THEME--> 
<script  src="assets/plugins/sliderpro/js/jquery.sliderPro.min.js"></script> 
<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script> 
<script src="assets/plugins/isotope/jquery.isotope.min.js"></script> 
<script src="assets/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script> 
<script src="assets/plugins/datetimepicker/jquery.datetimepicker.js"></script> 
<script src="assets/plugins/jelect/jquery.jelect.js"></script> 
<script src="assets/plugins/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js"></script> 
<script src="assets/js/cssua.min.js"></script> 
<script src="assets/js/wow.min.js"></script> 
<script src="assets/js/custom.min.js"></script>



</body>
</html>
