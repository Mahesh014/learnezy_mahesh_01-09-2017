 <footer class="footer wow fadeInUp" data-wow-duration="2s">
      <div class="container">
        <div class="footer-inner">
          <div class="row">
            <div class="col-lg-3 col-sm-3">
              <section class="footer-section courses">
                <h3 class="footer-title">ABOUT US</h3>
               
                <div class="footer-info">Fusce eleifend. Donec sapien sed pha seah lusa. Pellentesque lu amus lorem arcu sem per duiac. Vivamus porta. Sed at nisl praesnt blandit mauris vel erat.</div>
                <!--<div class="footer-contacts footer-contacts_mod-a"> <i class="icon stroke icon-Pointer"></i>
                  <address class="footer-contacts__inner">
                  370 Hill Park, Florida, USA
                  </address>
                </div>-->
                <!--<div class="footer-contacts"> <i class="icon stroke icon-Phone2"></i> <span class="footer-contacts__inner">Call us 0800 12345</span> </div>-->
                <div class="footer-contacts"> <i class="icon stroke icon-Mail"></i> <a class="footer-contacts__inner" href="mailto:Info@learnezy.com">Info@learnezy.com</a> </div>
              </section>
              <!-- end footer-section --> 
            </div>
            <!-- end col -->
            
            <div class="col-lg-2 col-sm-3">
              <section class="footer-section courses">
                <h3 class="footer-title">USEFUL LINKS</h3>
                <ul class="footer-list list-unstyled">
                 <li class="footer-list__item"><a class="footer-list__link" href="academies.jsp">Our Academies</a></li>
                  <li class="footer-list__item"><a class="footer-list__link" href="courses_list.jsp">Our Latest Courses</a></li>
                  <li class="footer-list__item"><a class="footer-list__link" href="about.jsp">Who We Are</a></li>
                  <li class="footer-list__item"><a class="footer-list__link" href="contact.jsp">Get In Touch</a></li>
                  
                  <li class="footer-list__item"><a class="footer-list__link" href="frequently-asked-questions.jsp">Support & FAQ's</a></li>
                  <li class="footer-list__item"><a class="footer-list__link" href="terms-and-conditions.jsp">Terms & Conditions</a></li>
                  <li class="footer-list__item"><a class="footer-list__link" href="privacy_policy.jsp">Privacy Policy</a></li>
                </ul>
              </section>
              <!-- end footer-section --> 
            </div>
            <!-- end col -->
            
            <div class="col-lg-3 col-sm-3">
              <section class="footer-section courses">
                <h3 class="footer-title">LATEST TWEETS</h3>
                <div class="tweets">
                  <div class="tweets__text">What is the enemy of #creativity?</div>
                  <div><a href="javascript:void(0);">http://enva.to/hVl5G</a></div>
                  <span class="tweets__time">9 hours ago</span> </div>
                <div class="tweets">
                  <div class="tweets__text">An agile framework can produce the type of lean marketing essential for the digital age <a href="javascript:void(0);">@aholmes360 #IMDS15</a></div>
                  <span class="tweets__time">9 hours ago</span> </div>
                <a class="tweets__link" href="javascript:void(0);">Follow @Learnezy</a> </section>
              <!-- end footer-section --> 
            </div>
            <!-- end col -->
            
            <div class="col-lg-4 col-sm-3">
              <section class="footer-section">
                <h3 class="footer-title">QUICK CONTACT</h3>
                			<%
					   				 Object msg=request.getAttribute("varName");
					   				 if(msg==null)
					  				  {
					      			  msg="";
					   				  }
					   			 %>

									<%= msg %>
									
                <form action="DynamicData.do?action=enquiry1" method="post" class="form-contact ui-form" name="frm_contact">
                  <div class="form-group">
                    <input class="form-control" type="text" placeholder="Your Name" name="txtname" required>
                    <input class="form-control" type="email" placeholder="Email address" name="txtemail" required>
                     <input class="form-control" type="text" placeholder="Mobile Number" name="contactnumber" required>
                    <textarea class="form-control" rows="7" placeholder="Message" name="textarea-message"></textarea>
                    <button type="submit" class="btn btn-primary btn-effect">SEND MESSSAGE</button>
                  </div>
                </form>
              </section>
              
              <!-- end footer-section --> 
            </div>
            <!-- end col --> 
          </div>
          <!-- end row --> 
        </div>
        <!-- end footer-inner -->
        
        <div class="row">
          <div class="col-xs-12">
            <div class="footer-bottom">
              <div class="copyright">Copyright © 2016 <a href="javascript:void(0);">LearnEzy</a>, Online Learning  |  Designed &amp; Developed by <a href="http://www.itechsolutions.in">Itech Solutions</a></div>
              <ul class="social-links list-unstyled">
                <li><a class="icon fa fa-facebook" href="https://www.facebook.com/learneazy/" target="_blank"></a></li>
                <li><a class="icon fa fa-twitter" href="javascript:void(0);" target="_blank"></a></li>
                <li><a class="icon fa fa-google-plus" href="https://plus.google.com/u/0/107536091050129859190/" target="_blank"></a></li>
               
                <li><a class="icon fa fa-linkedin" href="javascript:void(0);" target="_blank"></a></li>
                <li><a class="icon fa fa-youtube-play" href="https://www.youtube.com/channel/UC-WxseNjJRYJHlJQNX7U9Jw/videos" target="_blank"></a></li>
                 <li><a class="icon fa fa-pinterest-p" href="https://in.pinterest.com/learnezy/" target="_blank"></a></li>
                  <li><a class="icon fa fa-stumbleupon" href="http://www.stumbleupon.com/stumbler/learnezymail" target="_blank"></a></li>
                  <li><a class="icon fa fa-tumblr" href="http://www.tagged.com/profile.jsp?dataSource=Profile&ll=nav" target="_blank"></a></li>
                 
                 
              </ul>
            </div>
            <!-- end footer-bottom --> 
          </div>
          <!-- end col --> 
        </div>
        <!-- end row --> 
      </div>
      <!-- end container --> 
    </footer>