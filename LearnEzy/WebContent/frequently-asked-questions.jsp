<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="utf-8">
	<meta name="robots" content="INDEX, FOLLOW">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui">
			<meta name="description" content="Have Questions About How Learnezy Works? Here, Check Out Our Faq'S For You Answers.">
			<meta name="googlebot" content="noodp">
		<link rel="canonical" href="www.learnezy.com/frequently-asked-questions"/>
		<link href="favicon.png" type="image/x-icon" rel="shortcut icon">
		<link href="assets/css/master.css" rel="stylesheet">
		<title>Frequently Asked Questions | LearnEZY</title>

<script src="assets/plugins/jquery/jquery-1.11.3.min.js"></script>
</head>
<body>
<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->
		<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200">


			<div id="wrapper">

	<!-- HEADER -->
    <header class="header">
      <div class="top-header">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <!--<div class="top-header__contacts"><i class="icon stroke icon-Phone2"></i>(123) 0800 12345</div>-->
              <div class="top-header__contacts"><i class="icon stroke icon-Mail"></i><a href="mailto:Info@learnezy.com">Info@learnezy.com</a></div>
              <div class="top-header__link">
                <button class="btn-header btn-effect">LATEST</button>
                <span>We have added new courses today.</span> </div>
              <div class="header-login"> <a class="header-login__item" href="sign_in.html"><i class="icon stroke icon-User"></i>Sign In</a> <a class="header-login__item" href="register.html">Register</a> </div>
            </div>
            <!-- end col  --> 
          </div>
          <!-- end row  --> 
        </div>
        <!-- end container  --> 
      </div>
      <!-- end top-header  -->
      
      <div class="container">
        <div class="row">
          <div class="col-xs-12"> <a class="header-logo" href="index.html"><img class="header-logo__img" src="assets/img/logo.png" alt="Logo"></a>
            <div class="header-inner">
              <div class="header-search">
                <div class="navbar-search">
                  <form id="search-global-form">
                    <div class="input-group">
                      <input type="text" placeholder="Type your search..." class="form-control" autocomplete="off" name="s" id="search" value="">
                      <span class="input-group-btn">
                      <button type="reset" class="btn search-close" id="search-close"><i class="fa fa-times"></i></button>
                      </span> </div>
                  </form>
                </div>
              </div>
              <a id="search-open" href="#fakelink"><i class="icon stroke icon-Search"></i></a> <a class="header-cart" href="/"> <i class="icon stroke icon-ShoppingCart"></i></a>
              <nav class="navbar yamm">
                <div class="navbar-header hidden-md  hidden-lg  hidden-sm ">
                  <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                </div>
                <div id="navbar-collapse-1" class="navbar-collapse collapse">
                  <ul class="nav navbar-nav">
                    <li class="dropdown"><a href="index.html">Home<span class="nav-subtitle">OUR World</span></a>
                     
                    </li>
                    <li class="dropdown"> <a href="courses_list.html">COURSES<span class="nav-subtitle">What We Offers</span></a>
                      
                    </li>
                   
                    <li><a href="contact.html">CONTACT<span class="nav-subtitle">say us hi</span></a></li>
                  </ul>
                </div>
              </nav>
              <!--end navbar --> 
              
            </div>
            <!-- end header-inner --> 
          </div>
          <!-- end col  --> 
        </div>
        <!-- end row  --> 
      </div>
      <!-- end container--> 
    </header>
    <!-- end header -->


				<div class="wrap-title-page">
					<div class="container">
						<div class="row">
							<div class="col-xs-12"><h1 class="ui-title-page">Frequently Asked Questions (Faq'S)</h1></div>
						</div>
					</div><!-- end container-->
				</div><!-- end wrap-title-page -->


				<div class="section-breadcrumb">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div class="wrap-breadcrumb clearfix">
									<ol class="breadcrumb">
										<li><a href="index.html"><i class="icon stroke icon-House"></i></a></li>
										<li class="active">FAQ's</li>
									</ol>
								</div>
							</div>
						</div><!-- end row -->
					</div><!-- end container -->
				</div><!-- end section-breadcrumb -->


				<main class="main-content">
 <div class="container">
						<div class="row">
						
							<div class="col-md-12">
								<section class="section-default">
									
									<div class="panel-group accordion accordion" id="accordion-1">
										<div class="panel panel-default">
											<div class="panel-heading">
												<a class="btn-collapse" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-1"><i class="icon"></i></a>
												<h3 class="panel-title">How candidate will get original receipt for the payment </h3>
											</div>
											<div id="collapse-1" class="panel-collapse collapse in">
												<div class="panel-body">
													<p class="ui-text">The Invoice for payment received will be emailed to the candidate as a PDF document. </p>
												</div>
											</div>
										</div><!-- end panel -->

										<div class="panel">
											<div class="panel-heading">
												<a class="btn-collapse collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-2"><i class="icon"></i></a>
												<h3 class="panel-title">Fees (not available) which is displaying on website it is final no hidden charges applicable</h3>
											</div>
											<div id="collapse-2" class="panel-collapse collapse">
												<div class="panel-body">
													<p class="ui-text">Fees details for respective course (with other charges if applicable) to be updated on Website by 20th March 2015. </p>
												</div>
											</div>
										</div><!-- end panel -->

										<div class="panel">
											<div class="panel-heading">
												<a class="btn-collapse collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-3"><i class="icon"></i></a>
												<h3 class="panel-title">If candidate does not attend the scheduled class due to illness / network failure than merchant will rescheduled the same or student will not be eligible to attend the same </h3>
											</div>
											<div id="collapse-3" class="panel-collapse collapse">
												<div class="panel-body">
													<p class="ui-text">This situation will not arise since the classes and courses are all online which the student can take as per his convenience. </p>
												</div>
											</div>
										</div><!-- end panel -->


										<div class="panel">
											<div class="panel-heading">
												<a class="btn-collapse collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-4"><i class="icon"></i></a>
												<h3 class="panel-title">If the student does not attend the scheduled class due to cancelled by merchant than how merchant will treat with student</h3>
											</div>
											<div id="collapse-4" class="panel-collapse collapse">
												<div class="panel-body">
													<p class="ui-text">This situation will not arise since the classes and courses are all online which the student can take as per his convenience. </p>
												</div>
											</div>
										</div><!-- end panel -->
                                        
                                        	<div class="panel">
											<div class="panel-heading">
												<a class="btn-collapse collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-5"><i class="icon"></i></a>
												<h3 class="panel-title">Cancellation Clause Not mentioned</h3>
											</div>
											<div id="collapse-5" class="panel-collapse collapse">
												<div class="panel-body">
													<p class="ui-text">Cancellation & Refund policy to be updated on Website by 24th March 2015.  </p>
												</div>
											</div>
										</div><!-- end panel -->
                                        
                                        <div class="panel">
											<div class="panel-heading">
												<a class="btn-collapse collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-6"><i class="icon"></i></a>
												<h3 class="panel-title">Once candidate made the payment then within how many days merchant will provide course materials to student </h3>
											</div>
											<div id="collapse-6" class="panel-collapse collapse">
												<div class="panel-body">
													<p class="ui-text">The classes and courses are all online which the student can take as per his convenience. This will be activated as soon as payment is made.   </p>
												</div>
											</div>
										</div><!-- end panel --> 
                                        
                                        <div class="panel">
											<div class="panel-heading">
												<a class="btn-collapse collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-7"><i class="icon"></i></a>
												<h3 class="panel-title">If student will paid twice for one transaction, the one transaction amount will be refunded via same source within 15 to 20 working days </h3>
											</div>
											<div id="collapse-7" class="panel-collapse collapse">
												<div class="panel-body">
													<p class="ui-text">our Refund policy will explain the details.   </p>
												</div>
											</div>
										</div><!-- end panel -->
                                        
                                        
                                        <div class="panel">
											<div class="panel-heading">
												<a class="btn-collapse collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-8"><i class="icon"></i></a>
												<h3 class="panel-title">Please note : We will not offered guaranteed job </h3>
											</div>
											<div id="collapse-8" class="panel-collapse collapse">
												<div class="panel-body">
													<p class="ui-text">This training course is nothing to do with Placements. We can offer placement assistance to deserving candidates.   </p>
												</div>
											</div>
										</div><!-- end panel -->
                                        <div class="panel">
											<div class="panel-heading">
												<a class="btn-collapse collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-9"><i class="icon"></i></a>
												<h3 class="panel-title">Another address is reflecting on the website </h3>
											</div>
											<div id="collapse-9" class="panel-collapse collapse">
												<div class="panel-body">
													<p class="ui-text">we will check this; what address are you referring to?  </p>
												</div>
											</div>
										</div><!-- end panel -->
                                        
                                         <div class="panel">
											<div class="panel-heading">
												<a class="btn-collapse collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-10"><i class="icon"></i></a>
												<h3 class="panel-title">Check box agrees for terms & condition</h3>
											</div>
											<div id="collapse-10" class="panel-collapse collapse">
												<div class="panel-body">
													<p class="ui-text">Ok will be included by 24th March 2015 </p>
												</div>
											</div>
										</div><!-- end panel -->
                                        
                                         <div class="panel">
											<div class="panel-heading">
												<a class="btn-collapse collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-11"><i class="icon"></i></a>
												<h3 class="panel-title">What documents he will provide in case of disputes</h3>
											</div>
											<div id="collapse-11" class="panel-collapse collapse">
												<div class="panel-body">
													<p class="ui-text">Not clear, please explain.  </p>
												</div>
											</div>
										</div><!-- end panel -->
                                           <div class="panel">
											<div class="panel-heading">
												<a class="btn-collapse collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-12"><i class="icon"></i></a>
												<h3 class="panel-title">Point of contact in chargeback related cases</h3>
											</div>
											<div id="collapse-12" class="panel-collapse collapse">
												<div class="panel-body">
													<p class="ui-text">He can send an email to hr@itechsolutions.in which we will mention in the Refund & Cancellation Policy.  </p>
												</div>
											</div>
										</div><!-- end panel -->
                                        
									</div><!-- end accordion -->
								</section>
							</div><!-- end col -->

							
						</div><!-- end row -->
					</div><!-- end container -->


				

				</main><!-- end main-content -->


			  <footer class="footer wow fadeInUp" data-wow-duration="2s">
      <div class="container">
        <div class="footer-inner">
          <div class="row">
            <div class="col-lg-3 col-sm-3">
              <section class="footer-section courses">
                <h3 class="footer-title">ABOUT US</h3>
               
                <div class="footer-info">Fusce eleifend. Donec sapien sed pha seah lusa. Pellentesque lu amus lorem arcu sem per duiac. Vivamus porta. Sed at nisl praesnt blandit mauris vel erat.</div>
                <!--<div class="footer-contacts footer-contacts_mod-a"> <i class="icon stroke icon-Pointer"></i>
                  <address class="footer-contacts__inner">
                  370 Hill Park, Florida, USA
                  </address>
                </div>-->
                <!--<div class="footer-contacts"> <i class="icon stroke icon-Phone2"></i> <span class="footer-contacts__inner">Call us 0800 12345</span> </div>-->
                <div class="footer-contacts"> <i class="icon stroke icon-Mail"></i> <a class="footer-contacts__inner" href="mailto:Info@learnezy.com">Info@learnezy.com</a> </div>
              </section>
              <!-- end footer-section --> 
            </div>
            <!-- end col -->
            
            <div class="col-lg-2 col-sm-3">
              <section class="footer-section courses">
                <h3 class="footer-title">USEFUL LINKS</h3>
                <ul class="footer-list list-unstyled">
                 <li class="footer-list__item"><a class="footer-list__link" href="academies.html">Our Academies</a></li>
                  <li class="footer-list__item"><a class="footer-list__link" href="courses_list.html">Our Latest Courses</a></li>
                  <li class="footer-list__item"><a class="footer-list__link" href="about.html">Who We Are</a></li>
                  <li class="footer-list__item"><a class="footer-list__link" href="contact.html">Get In Touch</a></li>
                  
                  <li class="footer-list__item"><a class="footer-list__link" href="faqs.html">Support & FAQ's</a></li>
                  <li class="footer-list__item"><a class="footer-list__link" href="terms.html">Terms & Conditions</a></li>
                  <li class="footer-list__item"><a class="footer-list__link" href="privacy_policy.html">Privacy Policy</a></li>
                </ul>
              </section>
              <!-- end footer-section --> 
            </div>
            <!-- end col -->
            
            <div class="col-lg-3 col-sm-3">
              <section class="footer-section courses">
                <h3 class="footer-title">LATEST TWEETS</h3>
                <div class="tweets">
                  <div class="tweets__text">What is the enemy of #creativity?</div>
                  <div><a href="javascript:void(0);">http://enva.to/hVl5G</a></div>
                  <span class="tweets__time">9 hours ago</span> </div>
                <div class="tweets">
                  <div class="tweets__text">An agile framework can produce the type of lean marketing essential for the digital age <a href="javascript:void(0);">@aholmes360 #IMDS15</a></div>
                  <span class="tweets__time">9 hours ago</span> </div>
                <a class="tweets__link" href="javascript:void(0);">Follow @Learnezy</a> </section>
              <!-- end footer-section --> 
            </div>
            <!-- end col -->
            
            <div class="col-lg-4 col-sm-3">
              <section class="footer-section">
                <h3 class="footer-title">QUICK CONTACT</h3>
                <form class="form">
                  <div class="form-group">
                    <input class="form-control" type="text" placeholder="Your Name">
                    <input class="form-control" type="email" placeholder="Email address">
                    <textarea class="form-control" rows="7" placeholder="Message"></textarea>
                    <button class="btn btn-primary btn-effect">SEND MESSSAGE</button>
                  </div>
                </form>
              </section>
              <!-- end footer-section --> 
            </div>
            <!-- end col --> 
          </div>
          <!-- end row --> 
        </div>
        <!-- end footer-inner -->
        
        <div class="row">
          <div class="col-xs-12">
            <div class="footer-bottom">
              <div class="copyright">Copyright � 2016 <a href="javascript:void(0);">LearnEzy</a>, Online Learning  |  Designed &amp; Developed by <a href="http://www.itechsolutions.in">Itech Solutions</a></div>
              <ul class="social-links list-unstyled">
                <li><a class="icon fa fa-facebook" href="https://www.facebook.com/learneazy/" target="_blank"></a></li>
                <li><a class="icon fa fa-twitter" href="javascript:void(0);" target="_blank"></a></li>
                <li><a class="icon fa fa-google-plus" href="https://plus.google.com/u/0/107536091050129859190/" target="_blank"></a></li>
               
                <li><a class="icon fa fa-linkedin" href="javascript:void(0);" target="_blank"></a></li>
                <li><a class="icon fa fa-youtube-play" href="https://www.youtube.com/channel/UC-WxseNjJRYJHlJQNX7U9Jw/videos" target="_blank"></a></li>
                 <li><a class="icon fa fa-pinterest-p" href="https://in.pinterest.com/learnezy/" target="_blank"></a></li>
                  <li><a class="icon fa fa-stumbleupon" href="http://www.stumbleupon.com/stumbler/learnezymail" target="_blank"></a></li>
                  <li><a class="icon fa fa-tumblr" href="http://www.tagged.com/profile.html?dataSource=Profile&ll=nav" target="_blank"></a></li>
                 
                 
              </ul>
            </div>
            <!-- end footer-bottom --> 
          </div>
          <!-- end col --> 
        </div>
        <!-- end row --> 
      </div>
      <!-- end container --> 
    </footer>

			</div><!-- end wrapper -->
		</div><!-- end layout-theme -->


		<!-- SCRIPTS --> 
<script src="assets/js/jquery-migrate-1.2.1.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<script src="assets/js/modernizr.custom.js"></script> 
<script src="assets/js/waypoints.min.js"></script> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> 

<!--THEME--> 
<script  src="assets/plugins/sliderpro/js/jquery.sliderPro.min.js"></script> 
<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script> 
<script src="assets/plugins/isotope/jquery.isotope.min.js"></script> 
<script src="assets/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script> 
<script src="assets/plugins/datetimepicker/jquery.datetimepicker.js"></script> 
<script src="assets/plugins/jelect/jquery.jelect.js"></script> 
<script src="assets/plugins/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js"></script> 
<script src="assets/js/cssua.min.js"></script> 
<script src="assets/js/wow.min.js"></script> 
<script src="assets/js/custom.min.js"></script> 
</body>
</html>