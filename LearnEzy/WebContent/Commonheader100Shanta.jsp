<!-- HEADER -->
  <style>
  

 .cart-items::before {
    border-bottom: 10px solid #e9e9e9;
    border-left: 10px solid transparent;
    border-right: 10px solid transparent;
    content: "";
    height: 0;
    position: absolute;
    right: 50px;
    top: -9px;
    width: 0;
}
.cartEx{width:500px !important;}
.cart-items::after {
    border-bottom: 10px solid #ffffff;
    border-left: 10px solid transparent;
    border-right: 10px solid transparent;
    content: "";
    height: 0;
    position: absolute;
    right: 49px;
    top: -8px;
    width: 0;
}
.cart-items .cart-items-inner {
    background-color: #ffffff;
    border: 3px solid #e9e9e9;
}
.media-right, .media > .pull-right {
    padding-left: 10px;
}

.cart-items .cart-items-inner {
    background-color: #ffffff;
    border: 3px solid #e9e9e9;
    
}
.media:first-child {
    margin-top: 0;
}
.media-right, .media > .pull-right {
    padding-left: 10px;
}
.pull-right {
    float: right;
}
.pull-right {
    float: right;
}
p {
    margin-bottom: 20px;
}
.cart-items .item-title {
    color: #232323;
    font-size: 17px;
    font-weight: 300;
    margin-bottom: 0;
    text-transform: uppercase;
}




.modal-dialog

{
position:fixed;
}

.modal-backdrop.fade {
    opacity: 0 !important;
    
}
.modal-dialog {
    width: 600px;
    margin: 149px;
        margin-left: 149px;
    margin-left: 45%;
}

.modal-backdrop {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 0 !important;
    background-color: #000000;
}
  </style>
  
 <header class="header">

      <div class="top-header">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <!--<div class="top-header__contacts"><i class="icon stroke icon-Phone2"></i>(123) 0800 12345</div>-->
              <div class="top-header__contacts"><i class="icon stroke icon-Mail"></i><a href="mailto:Info@learnezy.com">Info@learnezy.com</a></div>
              <div class="top-header__link">
                <a href="DynamicData.do?action=latestcourses"><button class="btn-header btn-effect" >LATEST</button></a>
                <span>We have added new courses today ...</span> </div>
              <div class="header-login"> <a class="header-login__item" href="sign_in.jsp"><i class="icon stroke icon-User"></i>Sign In</a> <a class="header-login__item" href="registernew.jsp">Register</a> </div>
            </div>
            <!-- end col  --> 
          </div>
          <!-- end row  --> 
        </div>
        <!-- end container  --> 
      </div>
      <!-- end top-header  -->
      
    
      
      <div class="container">
        <div class="row">
          <div class="col-xs-12"> <a class="header-logo" href="dynamic.jsp"><img class="header-logo__img" src="assets/img/LEARNEZY-LOGO.png" alt="Logo"></a>
            <div class="header-inner">
              <div class="header-search">
                <div class="navbar-search">
                  <form id="search-global-form">
                    <div class="input-group">
                      <input type="text" placeholder="Type your search..." class="form-control" autocomplete="off" name="s" id="search" value="">
                      <span class="input-group-btn">
                      <button type="reset" class="btn search-close" id="search-close"><i class="fa fa-times"></i></button>
                      </span> </div>
                  </form>
                </div>
              </div>
              <!--
              <a id="search-open" href="#fakelink"><i class="icon stroke icon-Search"></i></a> <a class="header-cart" href="#"> <i class="icon stroke icon-ShoppingCart"></i></a>
               -->
               
               
  <a id="search-open" href="#fakelink"><i class="icon stroke icon-Search"></i></a> 
  <a class="header-cart" href="#" data-toggle="modal" data-target="#popup-cart"> <i class="icon stroke icon-ShoppingCart"></i></a>
                
             
              <nav class="navbar yamm">
                <div class="navbar-header hidden-md  hidden-lg  hidden-sm ">
                  <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                </div>
                <div id="navbar-collapse-1" class="navbar-collapse collapse">
                  <ul class="nav navbar-nav">
                    <li class="dropdown"><a href="DynamicData.do">Home<span class="nav-subtitle">OURr World</span></a>
                     
                    </li>
                    <!-- <li class="dropdown"> <a href="DynamicData.do?action=latestcourses">COURSES<span class="nav-subtitle">What We Offers</span></a> -->
                      
                    </li>
                   
                    <li><a href="contact-us.jsp">CONTACT<span class="nav-subtitle">say us hi</span></a></li>
                  </ul>
                </div>
              </nav>
              <!--end navbar --> 
              
            </div>
            <!-- end header-inner --> 
          </div>
          <!-- end col  --> 
        </div>
        <!-- end row  --> 
      </div>
      <!-- end container--> 
      
      
        <!-- Popup: LearnEzy cart  -->
        
                 <div class="modal  popup-cart" id="popup-cart"  >
                <div class="modal-dialog">
                    <div class="container cartEx">
                        <div class="cart-items">
                            <div class="cart-items-inner">
                                <div class="media">
	                              <table id="tbUser">
		                              <tr>
			                              <td>                          
				                              <a class="pull-left">
				                              		<img src="images/Desert.jpg" width="100" height="100" style="margin-right: 20px"></img>
				                              </a>
				                              
				                                <p>java course    Rs:17000.00</p>
				                                            
				                              <p class="pull-right ">
				                                 	<input type="button" name="remove" value="Removed" id="#" class="glyphicon glyphicon-trash form-control row-remover removeid" >                                    
				                              </p>                                   
				                              <div class="media-body" ><p id="pid2"></p>
				                              <input type="hidden" name="orderid" id="orderhiddenid"/>
				                                    
				                              </div>
			                             </td>
		                              </tr>     
	                               </table> 
                                 </div>
                                 
                                 <div class="media">
                                    <p class="pull-right " id="price">2000.00</p>
                                    <div class="media-body">
                                        <h4 class="media-heading item-title summary">Subtotal</h4>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-body">
                                        <div>
                                            <a href="#" class="btn btn-theme btn-theme-dark" data-dismiss="modal">Close</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Popup: LearnEzy cart  -->
      
      
      
      
      
      
      
    </header>
    <!-- end header -->
    
    
