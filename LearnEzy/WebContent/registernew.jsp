<!DOCTYPE html>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ page import="java.sql.*" %>
<%@page import="com.itech.elearning.forms.LoginForm"%>
<html lang="en">
	<head>
		<meta charset="utf-8">
		
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui">
		<meta name="robots" content="NOINDEX, NOFOLLOW">
		<meta name="googlebot" content="noodp">
		<title>ACADEMICA, Courses Category</title>
		<link href="favicon.png" type="image/x-icon" rel="shortcut icon">
		<link href="assets/css/master.css" rel="stylesheet">

<script src="assets/plugins/jquery/jquery-1.11.3.min.js"></script>

</head>

<body>
<% String url = request.getRequestURL().toString();
String img = "";
%>
<% 

if(url.equals("http://localhost:8080/elearningcom/login.jsp"))
{
	 img = "Itech Solutions logo.png";
}
else if(url.equals("test.elearning.in"))
{
	img="itech.png";
}

%>


<%
String urlre = request.getRequestURL().toString();
urlre = "Itech";
String compid = null;
Connection con = null;
String urln = "jdbc:mysql://localhost:3306/";
String db = "elearningcom";
String driver = "com.mysql.jdbc.Driver";
String userName ="root";
String password="Itech123!";

int sumcount=0;
Statement st;
try{
Class.forName(driver).newInstance();
con = DriverManager.getConnection(urln+db,userName,password);
String query = "select * from companymaster where CompanyName =  '"+urlre+"' ";
System.out.println(query);
st = con.createStatement();
ResultSet rs = st.executeQuery(query);
%>
<%
while(rs.next()){
%>
<!--<tr><td><%= compid =  rs.getString(1)%></td>

</tr>
--><%
}
%>
<%
}
catch(Exception e){
e.printStackTrace();
}
%>

<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->
		<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200">

	

			<div id="wrapper">

				                     <jsp:include page="commonindexheader.jsp" /> 
				     <!-- end header -->



				<div class="wrap-title-page">
					<div class="container">
						<div class="row">
							<div class="col-xs-12"><h1 class="ui-title-page">Registration</h1></div>
						</div>
					</div><!-- end container-->
				</div><!-- end wrap-title-page -->


				<div class="section-breadcrumb">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div class="wrap-breadcrumb clearfix">
									<ol class="breadcrumb">
										<li><a href="dynamic.jsp"><i class="icon stroke icon-House"></i></a></li>
										<li class="active">Registration</li>
									</ol>
								</div>
							</div>
						</div><!-- end row -->
					</div><!-- end container -->
				</div><!-- end section-breadcrumb -->


				<main class="main-content">

					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<div class="wrap-title wrap-title_mod-b">
									<div class="ui-subtitle-block">Register to LearnEzy as</div>
								</div><!-- end wrap-title -->


								<div class="posts-wrap">
									<article class="post post_mod-h clearfix">
										<div class="entry-media">
											<div class="entry-thumbnail">
												<a href="javascript:void(0);" ><img class="img-responsive" src="images/student.jpg" width="370" height="200" alt="Foto"/></a>
											</div>
										</div><!-- end entry-media -->

										<div class="entry-main">
											<h3 class="entry-title ui-title-inner"><a href="javascript:void(0);">Student</a></h3><br>

											<div class="entry-content decor decor_mod-b">
												<p>Esasellus luctus nibhay pulvinar bibena laum aliqua ligula sapien ipsun diua lorem ipsum dolor.</p>
											</div><!-- end entry-content -->

											
											<div class="entry-footer">
												<a href="studentRegistration.do?action=list&amp;roleid=3" class="post-btn btn btn-primary btn-effect">Register</a>
											</div><!-- end entry-footer -->
										</div><!-- end entry-main -->
									</article><!-- end post -->

									<article class="post post_mod-h clearfix">
										<div class="entry-media">
											<div class="entry-thumbnail">
												<a href="javascript:void(0);" ><img class="img-responsive" src="images/academy.jpg" width="370" height="250" alt="Foto"/></a>
											</div>
										</div><!-- end entry-media -->

										<div class="entry-main">
											<h3 class="entry-title ui-title-inner"><a href="javascript:void(0);">Academy/Training Institute</a></h3>

											<div class="entry-content decor decor_mod-b">
												<p>Esasellus luctus nibhay pulvinar bibena laum aliqua ligula sapien ipsun diua lorem ipsum dolor.</p>
											</div><!-- end entry-content -->

										
											<div class="entry-footer">
												<a href="studentRegistration.do?action=listaca&amp;roleid=5" class="post-btn btn btn-primary btn-effect">Register</a>
											</div><!-- end entry-footer -->
										</div><!-- end entry-main -->
									</article><!-- end post -->

									<article class="post post_mod-h clearfix">
										<div class="entry-media">
											<div class="entry-thumbnail">
												<a href="javascript:void(0);" ><img class="img-responsive" src="images/freelancer.jpg" width="370" height="200" alt="Foto"/></a>
											</div>
										</div><!-- end entry-media -->

										<div class="entry-main">
											<h3 class="entry-title ui-title-inner"><a href="javascript:void(0);">Freelancer</a></h3><br>

											<div class="entry-content decor decor_mod-b">
												<p>Esasellus luctus nibhay pulvinar bibena laum aliqua ligula sapien ipsun diua lorem ipsum dolor.</p>
											</div><!-- end entry-content -->

											
											<div class="entry-footer">
												<a href="studentRegistration.do?action=listfree&amp;roleid=6" class="post-btn btn btn-primary btn-effect">Register</a>
											</div><!-- end entry-footer -->
										</div><!-- end entry-main -->
									</article><!-- end post -->

									<article class="post post_mod-h clearfix">
										<div class="entry-media">
											<div class="entry-thumbnail">
												<a href="javascript:void(0);" ><img class="img-responsive" src="images/consultant.jpg" width="370" height="200" alt="Foto"/></a>
											</div>
										</div><!-- end entry-media -->

										<div class="entry-main">
											<h3 class="entry-title ui-title-inner"><a href="javascript:void(0);">Consultant</a></h3><br>

											<div class="entry-content decor decor_mod-b">
												<p>Esasellus luctus nibhay pulvinar bibena laum aliqua ligula sapien ipsun diua lorem ipsum dolor.</p>
											</div><!-- end entry-content -->

										
											<div class="entry-footer">
												<a href="javascript:void(0);" class="post-btn btn btn-primary btn-effect">Register</a>
											</div><!-- end entry-footer -->
										</div><!-- end entry-main -->
									</article><!-- end post -->

									<article class="post post_mod-h clearfix">
										<div class="entry-media">
											<div class="entry-thumbnail">
												<a href="javascript:void(0);" ><img class="img-responsive" src="images/hbt.jpg" width="370" height="200" alt="Foto"/></a>
											</div>
										</div><!-- end entry-media -->

										<div class="entry-main">
											<h3 class="entry-title ui-title-inner"><a href="javascript:void(0);">Home Based Trainer </a></h3>

											<div class="entry-content decor decor_mod-b">
												<p>Esasellus luctus nibhay pulvinar bibena laum aliqua ligula sapien ipsun diua lorem ipsum dolor.</p>
											</div><!-- end entry-content -->

											
											<div class="entry-footer">
												<a href="javascript:void(0);" class="post-btn btn btn-primary btn-effect">Register</a>
											</div><!-- end entry-footer -->
										</div><!-- end entry-main -->
									</article><!-- end post -->

									
								</div><!-- end posts-wrap -->
							</div><!-- end col -->
						</div><!-- end row -->
					</div><!-- end container -->

				</main><!-- end main-content -->


				          <jsp:include page="Commonfooter.jsp"></jsp:include>
				
			</div><!-- end wrapper -->
		</div><!-- end layout-theme -->


	<!-- SCRIPTS --> 
<script src="assets/js/jquery-migrate-1.2.1.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<script src="assets/js/modernizr.custom.js"></script> 
<script src="assets/js/waypoints.min.js"></script> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> 

<!--THEME--> 
<script  src="assets/plugins/sliderpro/js/jquery.sliderPro.min.js"></script> 
<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script> 
<script src="assets/plugins/isotope/jquery.isotope.min.js"></script> 
<script src="assets/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script> 
<script src="assets/plugins/datetimepicker/jquery.datetimepicker.js"></script> 
<script src="assets/plugins/jelect/jquery.jelect.js"></script> 
<script src="assets/plugins/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js"></script> 
<script src="assets/js/cssua.min.js"></script> 
<script src="assets/js/wow.min.js"></script> 
<script src="assets/js/custom.min.js"></script>
</body>
</html>

