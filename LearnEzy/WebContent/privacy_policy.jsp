<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="robots" content="INDEX, FOLLOW">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui">
	<meta name="description" content="Learnezy Privacy Policy">
	<meta name="googlebot" content="noodp">
		
		<title>Privacy Policy | LearnEZY</title>
		<link rel="canonical" href="www.learnezy.com/privacy-policy.jsp"/>
		<link href="favicon.png" type="image/x-icon" rel="shortcut icon">
		<link href="assets/css/master.css" rel="stylesheet">

<script src="assets/plugins/jquery/jquery-1.11.3.min.js"></script>
</head>

<body>

<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->

		<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200">



			<div id="wrapper">

			<jsp:include page="Commonheader.jsp"></jsp:include>


				<div class="wrap-title-page">
					<div class="container">
						<div class="row">
							<div class="col-xs-12"><h1 class="ui-title-page">Privacy Policy</h1></div>
						</div>
					</div><!-- end container-->
				</div><!-- end wrap-title-page -->


				<div class="section-breadcrumb">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div class="wrap-breadcrumb clearfix">
									<ol class="breadcrumb">
										<li><a href="index.html"><i class="icon stroke icon-House"></i></a></li>
										
										<li class="active">Privacy Policy</li>
									</ol>
								</div>
							</div>
						</div><!-- end row -->
					</div><!-- end container -->
				</div><!-- end section-breadcrumb -->


				<div class="container">
					<div class="row">
						<div class="col-md-8">
							<main class="main-content">

								<article class="post post_mod-j clearfix">
									
									<div class="post-inner decor decor_mod-a clearfix">
										
										<div class="entry-main">
											<h3 class="entry-title ui-title-inner">Learnezy - Privacy Policies</h3>
											<div class="entry-content">
												 <p>The Itech Training website has been carefully drafted and designed both in content and technical aspects to provide and deliver a comprehensive, understanding and user interface for all our users alike. The terms and conditions have been drafted carefully encompassing all areas of interest and concerns for an applicant. Applicants usually like to explore the method of operations of an organization and determine whether the working norms suit their requirements. Itech Training makes sure that all applicants are aware of our operational norms and are aware of our policies. We also require the applicants to respect and acknowledge our privacy policies. For a better understanding of the policies, read below the terms and conditions that we abide by.</p>

												

											<h2 class="left">Security </h2>
<p>The security of your personal information is important to us. Itech solutions employs procedural and technological measures to protect your personally identifiable information. These measures are reasonably designed to help protect your personally identifiable information from loss, unauthorized access, disclosure, alteration or destruction. We may use software, secure socket layer technology (SSL) encryption, password protection, firewalls, internal restrictions and other security measures to help prevent unauthorized access to your personally identifiable information. However, Itech solutions cannot guarantee that unauthorized third parties will never be able to defeat these measures or use your personally identifiable information for improper purposes. Therefore, you should be aware that when you voluntarily display or distribute personally identifiable information, that information could be collected and used by others. Itech solutions is not responsible for the unauthorized use by third parties of information you post or otherwise make available publicly.</p>

<h2 class="left">Service Delivery Policy </h2>
<p>Once candidate made the payment then immediately merchant will provide course materials to student. Anytime candidate can login and study according to his convenience.<br/>
Itech solutions is committed to providing high-quality online educational experiences to our students.
</p>


<h2 class="left">Cancellation Policy </h2>
<p>Itech solutions offers Students a course changing facility. If you, as a Student, are unhappy with such a Course, You can choose another course , we will provide you all deatails and material of that course. To request for changing a course, please contact us via support@itechsolutions.in.
Itech solutions does not provide refunds for Courses which you have paid but course changing facility is there. We work very hard to try to keep our training program more effective</p>

<h2 class="left">Refund Policy </h2>
<p>If student will paid twice for one transaction, the one transaction amount will be refunded via same source within 15 to 20 working days.</p>
											</div>
										</div>
										
									</div>
								</article><!-- end post -->

								
								
							</main><!-- end main-content -->

						</div><!-- end col -->


						<div class="col-md-4">
							<aside class="sidebar sidebar_mod-a">

								

								<section class="widget widget-default widget_courses">
									<h3 class="widget-title ui-title-inner decor decor_mod-a">POPULAR COURSES</h3>
									<div class="block_content">
										<ul class="list-courses list-unstyled">
											<li class="list-courses__item">
												<section>
													<div class="list-courses__img"><img class="img-responsive" src="assets/media/posts/90x90/1.jpg" height="90" width="90" alt="foto"></div>
													<div class="list-courses__inner">
														<h4 class="list-courses__title"><a href="javascript:void(0);">lifestyle: childhood in the digita age</a></h4>
														<div class="list-courses__meta">John Milton</div>
														<div class="list-courses__price">PRICE: <span class="list-courses__number">Free</span></div>
													</div>
												</section>
											</li>
											<li class="list-courses__item">
												<section>
													<div class="list-courses__img"><img class="img-responsive" src="assets/media/posts/90x90/2.jpg" height="90" width="90" alt="foto"></div>
													<div class="list-courses__inner">
														<h4 class="list-courses__title"><a href="javascript:void(0);">projects finance & management</a></h4>
														<div class="list-courses__meta">University of LIMS</div>
														<div class="list-courses__price">PRICE: <span class="list-courses__number">$650</span></div>
													</div>
												</section>
											</li>
											<li class="list-courses__item">
												<section>
													<div class="list-courses__img"><img class="img-responsive" src="assets/media/posts/90x90/3.jpg" height="90" width="90" alt="foto"></div>
													<div class="list-courses__inner">
														<h4 class="list-courses__title"><a href="javascript:void(0);">Clinical supervision with confidence</a></h4>
														<div class="list-courses__meta">University of LIMS</div>
														<div class="list-courses__price">PRICE: <span class="list-courses__number">$300</span></div>
													</div>
												</section>
											</li>
										</ul>
									</div><!-- end block_content -->
								</section><!-- end widget -->


							
								<section class="widget widget-default widget_categories">
									<h3 class="widget-title ui-title-inner decor decor_mod-a">categories</h3>
									<div class="block_content">
										<ul class="list-categories list-unstyled">
											<li class="list-categories__item">
												<a class="list-categories__link" href="javascript:void(0);">
													<span class="list-categories__name">computer sciences</span>
													<span class="list-categories__number">28</span>
												</a>
											</li>
											<li class="list-categories__item">
												<a class="list-categories__link" href="javascript:void(0);">
													<span class="list-categories__name">business & management</span>
													<span class="list-categories__number">106</span>
												</a>
											</li>
											<li class="list-categories__item">
												<a class="list-categories__link" href="javascript:void(0);">
													<span class="list-categories__name">biology & life sciences</span>
													<span class="list-categories__number">62</span>
												</a>
											</li>
											<li class="list-categories__item">
												<a class="list-categories__link" href="javascript:void(0);">
													<span class="list-categories__name">software engineering</span>
													<span class="list-categories__number">74</span>
												</a>
											</li>
											<li class="list-categories__item">
												<a class="list-categories__link" href="javascript:void(0);">
													<span class="list-categories__name">Music, Film, and Audio</span>
													<span class="list-categories__number">207</span>
												</a>
											</li>
											<li class="list-categories__item">
												<a class="list-categories__link" href="javascript:void(0);">
													<span class="list-categories__name">Economics and Finance</span>
													<span class="list-categories__number">69</span>
												</a>
											</li>
										</ul>
									</div><!-- end block_content -->
								</section><!-- end widget -->



								
							</aside><!-- end sidebar -->
						</div><!-- end col -->
					</div><!-- end row -->
				</div><!-- end container -->

<jsp:include page="Commonfooter.jsp"></jsp:include>

			</div><!-- end wrapper -->
		</div><!-- end layout-theme -->

<!-- SCRIPTS --> 
<script src="assets/js/jquery-migrate-1.2.1.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<script src="assets/js/modernizr.custom.js"></script> 
<script src="assets/js/waypoints.min.js"></script> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> 

<!--THEME--> 
<script  src="assets/plugins/sliderpro/js/jquery.sliderPro.min.js"></script> 
<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script> 
<script src="assets/plugins/isotope/jquery.isotope.min.js"></script> 
<script src="assets/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script> 
<script src="assets/plugins/datetimepicker/jquery.datetimepicker.js"></script> 
<script src="assets/plugins/jelect/jquery.jelect.js"></script> 
<script src="assets/plugins/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js"></script> 
<script src="assets/js/cssua.min.js"></script> 
<script src="assets/js/wow.min.js"></script> 
<script src="assets/js/custom.min.js"></script> 


</body>
</html>
