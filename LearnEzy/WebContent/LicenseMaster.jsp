
<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Itech Training ::  License Master</title>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300' rel='stylesheet' type='text/css'>
<link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/cmsstyle.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/carousel.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=request.getContextPath() %>/css/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>

<script src="<%=request.getContextPath() %>/js/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/owl.carousel.js" type="text/javascript"></script>

<link href="dataTable/css/jquery.dataTables_themeroller.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.css"	rel="stylesheet" type="text/css" media="all" />
<link href="dataTable/css/jquery.dataTables.min.css"	rel="stylesheet" type="text/css" media="all" />
<script src="dataTable/js/jquery.dataTables.js"	type="text/javascript"></script>
<script src="dataTable/js/jquery.dataTables.min.js"	type="text/javascript"></script>
<script src="dataTable/js/settingDataTable.js" 	type="text/javascript"></script>
<script>
$(document).ready(function()


		{
$("#resetid").click(function()
		{


$("#licennameid").val("");
$("#spaceid").val("");
$("#clientperid").val("");



		});




		});



</script>

</head>
<body ng-app="myApp" ng-controller="myCtrl">
 
 <jsp:include page="/jsp/common/masterdata_header.jsp"></jsp:include>
 
   <!-- Start Main Content -->
	 <div class="main" style="min-height:450px;">	 
	 	<h2>License Master</h2>
   
    <div id="status" ng-repeat="x in status " align="center">
					<font color="blue">{{x.status}}</font>
				</div>
				<form name ="myform" id="licenseid" >  
     <table class="role" width="600">
     <tr>
     
     <td >License Id:</td>
     <td><input type="text" ng-model="nglicense"  name="licenseid" class="licenseid" size="25" maxlength="20" readonly>
    
     
     </td>
     <td>License Name*:</td>
    
     <td><input type="text" ng-model="licenname" pattern="[a-zA-Z\s]+" id="licennameid" title="Enter Only Letters"    size="25" maxlength="20" required ></td>
     </tr>
     
      <tr>
     
     <td>Space*:</td>
     <td><input type="text"  ng-model="space"  pattern="[a-zA-Z0-9\s]+" id="spaceid"  title="Enter Alpha Numeric Only"  name="space" size="25" maxlength="20" required ></td>
     <td>Client Percentage*:</td>
    
     <td><input type="text"  ng-model="clientper" id="clientperid"  pattern="[0-9]+"  title="Enter Valid Percentage"  name="clientper" size="25" maxlength="20" required ></td>
     </tr>
     <tr>
     <td><input type="submit" ng-click="add()" id="addid"  value="Save"><input type="submit" ng-show="showMe" ng-click="update()"  value="update"> </td>
     <td>
     <input type="reset" value="Reset" id="resetid"></td>
    

     </tr>
     </table>
     </form>
     
     
     	
<div class="dataGridDisplay"  >
   <table id="normalTable" class="display" cellspacing="0" width="100%" align="center" >
				<thead>
    <tr>
        <th>SLNO</th>
    
    <th>License ID</th>
    <th>License Name</th>
     <th>Space</th>
       <th>Client Percentage</th>
     
    <th style="width:100px;" >Status</th>
    <th style="width:155px;">Action</th>
    </tr>
    </thead>
    <tbody>
   
    
    
    <tr ng-repeat="x in listvalue">
        <td>{{ $index + 1 }}</td>
    <td>{{x.licenseautoid}}</td>
    <td>{{x.licenseName}}</td>
     <td>{{x.space}}</td>
      <td>{{x.clientpercentage}}</td>
      <td ng-if="x.active==true"><font color="green">Active</font></td>
            <td ng-if="x.active==false"><font color="red">InActive</font></td>
      
       <td ng-if="x.active==true"><input type="button" ng-click="edit(x.licenseautoid,x.licenseName,x.space,x.clientpercentage)" value="Edit"><input type="button"  ng-click="changestatus(0,x.licenseautoid)" value="inactive"></td>
         <td ng-if="x.active==false"><input type="button" ng-click="changestatus(1,x.licenseautoid)" value="activate"></td>
    
    
   
    
    
    
    <td>
   
    
</td>
    </tr>
  
    
  
  </tbody>
    
    </table>
     <logic:notEmpty name="roleList">
    <div align="center"><a href="Role.do?action=excelReport"><button>Excel Export</button></a></div>
    </logic:notEmpty>
   </div>
    
    
     </div>
     
   <!-- End Main Content -->
	   <jsp:include page="/jsp/common/masterdata_footer.jsp"></jsp:include> 
 <script src="js/commonFunction.js"  type="text/javascript"></script>
 
 
 <script>
var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope, $http) {
	$scope.showMe =false;
	
	 $http.get("Role.do?action=licenselist").then(function (response) {
		
	        $scope.listvalue=response.data;
	    });

	
        
	 
	    
	$scope.add=function()
	{
		 if($scope.myform.$valid) {
			 
	var licenseid=$(".licenseid").val();
	

		 $http.get("Role.do?action=licenseadd&licensid="+licenseid+"&licensename="+$scope.licenname+"&space="+$scope.space+"&clientper="+$scope.clientper+"").then(function (response) {
		      
			  $scope.status=response.data;

		        
		    });

		
    
		 $http.get("Role.do?action=licenselist").then(function (response) {
			
		        $scope.listvalue=response.data;
		    });
		 }
	};
	  

	$scope.changestatus=function(a,id)
	{
	
    $http.get("Role.do?action=chanstatus&status="+a+"&id="+id+"").then(function (response) {
    	  $scope.status=response.data;
		        
		    });
		 $http.get("Role.do?action=licenselist").then(function (response) {
			
		        $scope.listvalue=response.data;
		    });
	
	};


	$scope.edit=function(licenseid,licensename,space,clientper)
	{
		   $scope.nglicense=licenseid;
		   $scope.licenname=licensename;
		   $scope.space=space;
		   $scope.clientper=clientper;
		   $scope.showMe =true;
		   $("#addid").hide();
		  $http.get("Role.do?action=licenselist").then(function (response) {
			
		        $scope.listvalue=response.data;
		    });
	
	};
	$scope.update=function()
	{
		 if($scope.myform.$valid) {
	var licenseid=$(".licenseid").val();
	
	

		 $http.get("Role.do?action=licenseupdate&licensid="+licenseid+"&licensename="+$scope.licenname+"&space="+$scope.space+"&clientper="+$scope.clientper+"").then(function (response) {
		        $scope.status=response.data;
			       
		    });
		 $http.get("Role.do?action=licenselist").then(function (response) {
			
		        $scope.listvalue=response.data;
		    });
		 }
	};





	
    
});
</script>
 
 <script>
$(document).ready(function()
		{
	 $.ajax({	
 		type: "GET",		
 		url:"Role.do?action=getlicenseid",
 	
 		async:false,
 		success: function(r){	
 			var json= JSON.parse(r);
 					$.each(json,function(i,obj){
     					$(".licenseid").val(obj.Autoid);
     					
 						
 			});
 		} 
 	});



		});


</script>
 
 
 
 
  </body>
</html>

