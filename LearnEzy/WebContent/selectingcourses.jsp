<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="robots" content="INDEX, FOLLOW">
<logic:notEmpty name="institutenames"><link rel="canonical" href="www.learnezy.com/<bean:write name="institutenames"/>-<logic:notEmpty name="location"><bean:write name="location"/></logic:notEmpty>-<logic:notEmpty name="citys"><bean:write name="citys"/></logic:notEmpty>"/></logic:notEmpty>
<logic:notEmpty name="institutenamess"><link rel="canonical" href="www.learnezy.com/<logic:notEmpty name="categoryname"><bean:write name="categoryname"/></logic:notEmpty>-courses-in-<bean:write name="institutenamess"/>"/></logic:notEmpty>
<logic:notEmpty name="institutenamesss"><link rel="canonical" href="www.learnezy.com/<logic:notEmpty name="categoryname"><bean:write name="categoryname"/></logic:notEmpty>-courses-in-<bean:write name="institutenamesss"/>-<logic:notEmpty name="citiess"><bean:write name="citiess"/></logic:notEmpty>"/></logic:notEmpty>
<logic:notEmpty name="institutenamessss"><link rel="canonical" href="www.learnezy.com/<logic:notEmpty name="categoryname"><bean:write name="categoryname"/></logic:notEmpty>-courses-in-<bean:write name="institutenamessss"/>-<logic:notEmpty name="areaname"><bean:write name="areaname"/></logic:notEmpty>-<logic:notEmpty name="citiess"><bean:write name="citiess"/></logic:notEmpty>"/></logic:notEmpty>
<logic:notEmpty name="institutenamesssss"><link rel="canonical" href="www.learnezy.com/<logic:notEmpty name="coursenames"><bean:write name="coursenames"/></logic:notEmpty>-in-<bean:write name="institutenamesssss"/>"/></logic:notEmpty>
<logic:notEmpty name="coursescityname"><link rel="canonical" href="www.learnezy.com/<logic:notEmpty name="coursenames"><bean:write name="coursenames"/></logic:notEmpty>-in-<bean:write name="coursescityname"/>"/></logic:notEmpty>
<logic:notEmpty name="citynamecoursearea"><link rel="canonical" href="www.learnezy.com/<logic:notEmpty name="coursenames"><bean:write name="coursenames"/></logic:notEmpty>-in-<logic:notEmpty name="areaname"><bean:write name="areaname"/></logic:notEmpty>-<bean:write name="citynamecoursearea"/>"/></logic:notEmpty>
<logic:notEmpty name="elearningcity"><link rel="canonical" href="www.learnezy.com/<logic:notEmpty name="elearning"><bean:write name="elearning"/></logic:notEmpty><logic:notEmpty name="classroom"><bean:write name="classroom"/> </logic:notEmpty>-<logic:notEmpty name="coursenames"><bean:write name="coursenames"/></logic:notEmpty>-in-<bean:write name="elearningcity"/>"/></logic:notEmpty>
<logic:notEmpty name="elearningcitylocation"><link rel="canonical" href="www.learnezy.com/<logic:notEmpty name="elearninglocation"><bean:write name="elearninglocation"/></logic:notEmpty><logic:notEmpty name="classroomlocation"><bean:write name="classroomlocation"/></logic:notEmpty>-<logic:notEmpty name="coursenames"><bean:write name="coursenames"/></logic:notEmpty>-In--<logic:notEmpty name="areaname"><bean:write name="areaname"/></logic:notEmpty>-<bean:write name="elearningcitylocation"/>"/></logic:notEmpty>

<logic:notEmpty name="institutenames">
<meta name="description" content="Get The Best Courses Offered By
 <bean:write name="institutenames"/> <logic:notEmpty name="location"><bean:write name="location"/></logic:notEmpty> <logic:notEmpty name="citys"><bean:write name="citys"/>
 Under Learnezy. <bean:write name="institutenames"/> Is One Of The Best Places For Learning In <bean:write name="citys"/></logic:notEmpty>"></logic:notEmpty>
 <logic:notEmpty name="institutenamess">
<meta name="description" content="<bean:write name="institutenamess"/> Is The Best Provider Of  <logic:notEmpty name="categoryname"><bean:write name="categoryname"/>
 Courses . Learn <bean:write name="categoryname"/> From The Best Trainers And Become An Expert <bean:write name="categoryname"/></logic:notEmpty>"></logic:notEmpty>
 <logic:notEmpty name="institutenamesss">
<meta name="description" content="<bean:write name="institutenamesss"/>Is The Best Provider Of <logic:notEmpty name="categoryname"><bean:write name="categoryname"/> </logic:notEmpty> Courses In <logic:notEmpty name="citiess"><bean:write name="citiess"/> </logic:notEmpty> .Learn <logic:notEmpty name="categoryname"><bean:write name="categoryname"/> </logic:notEmpty> From The Best Trainers And Become An Expert In The Field"></logic:notEmpty>
 <logic:notEmpty name="institutenamessss">
<meta name="description" content="Looking For Top <logic:notEmpty name="categoryname"><bean:write name="categoryname"/> </logic:notEmpty> Courses In <logic:notEmpty name="areaname"><bean:write name="areaname"/></logic:notEmpty> <logic:notEmpty name="citiess"><bean:write name="citiess"/></logic:notEmpty>, <bean:write name="institutenamessss"/> Has Been Providing The Best <logic:notEmpty name="categoryname"><bean:write name="categoryname"/> </logic:notEmpty> Courses. Join And Start Learning Today."></logic:notEmpty>
 <logic:notEmpty name="institutenamesssss">
<meta name="description" content="Planning To Acquire A New Skill? <bean:write name="institutenamesssss"/> Is Offering The Best Possible Training For <logic:notEmpty name="coursenames"><bean:write name="coursenames"/> . Don't Just Acquire The Skill, Start Today To Become An Expert In <bean:write name="coursenames"/> .</logic:notEmpty>"></logic:notEmpty>
  <logic:notEmpty name="coursescityname">
<meta name="description" content="<logic:notEmpty name="coursenames"><bean:write name="coursenames"/> </logic:notEmpty> Has Been In A Lots Of Demand In Recent Times. Learnezy Is Here To Provide You The Best <logic:notEmpty name="coursenames"><bean:write name="coursenames"/> </logic:notEmpty> In <bean:write name="coursescityname"/> ."></logic:notEmpty>
 <logic:notEmpty name="citynamecoursearea">
<meta name="description" content="<logic:notEmpty name="coursenames"><bean:write name="coursenames"/> </logic:notEmpty> Has Been In A Lots Of Demand In Recent Times. Learnezy Is Here To Provide You The Best <logic:notEmpty name="coursenames"><bean:write name="coursenames"/> </logic:notEmpty> In<logic:notEmpty name="coursenames"><bean:write name="coursenames"/> </logic:notEmpty> <bean:write name="citynamecoursearea"/>."></logic:notEmpty>
 <logic:notEmpty name="elearningcity">
<meta name="description" content="<logic:notEmpty name="coursenames"><bean:write name="coursenames"/> </logic:notEmpty> Has Been In A Lots Of Demand In Recent Times. Learnezy Is Here To Provide You The Best <logic:notEmpty name="elearning"><bean:write name="elearning"/> </logic:notEmpty><logic:notEmpty name="classroom"><bean:write name="classroom"/> </logic:notEmpty><logic:notEmpty name="coursenames"><bean:write name="coursenames"/> </logic:notEmpty>In <bean:write name="elearningcity"/>."></logic:notEmpty>
 <logic:notEmpty name="elearningcitylocation">
<meta name="description" content="<logic:notEmpty name="coursenames"><bean:write name="coursenames"/> </logic:notEmpty> Has Been In A Lots Of Demand In Recent Times. Learnezy Is Here To Provide You The Best <logic:notEmpty name="elearninglocation"><bean:write name="elearninglocation"/> </logic:notEmpty><logic:notEmpty name="classroomlocation"><bean:write name="classroomlocation"/> </logic:notEmpty>  <logic:notEmpty name="coursenames"><bean:write name="coursenames"/> </logic:notEmpty>In <logic:notEmpty name="areaname"><bean:write name="areaname"/> </logic:notEmpty> <bean:write name="elearningcitylocation"/>."></logic:notEmpty>
 
<meta name="googlebot" content="noodp">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui">
<logic:notEmpty name="institutenames"><title>Courses in <bean:write name="institutenames"/> |LearnEZY</title></logic:notEmpty>

<logic:notEmpty name="institutenamesss"><title><logic:notEmpty name="categoryname"><bean:write name="categoryname"/> </logic:notEmpty> Course in <bean:write name="institutenamesss"/> <logic:notEmpty name="citiess"><bean:write name="citiess"/></logic:notEmpty> | LearnEZY</title></logic:notEmpty>
<logic:notEmpty name="institutenamessss"><title><logic:notEmpty name="categoryname"><bean:write name="categoryname"/> </logic:notEmpty> Course in <bean:write name="institutenamessss"/> <logic:notEmpty name="areaname"><bean:write name="areaname"/></logic:notEmpty><logic:notEmpty name="citiess"><bean:write name="citiess"/></logic:notEmpty> | LearnEZY</title></logic:notEmpty>
<logic:notEmpty name="institutenamesssss"><title><logic:notEmpty name="coursenames"><bean:write name="coursenames"/> </logic:notEmpty> Course in <bean:write name="institutenamesssss"/> | LearnEZY</title></logic:notEmpty>



<logic:notEmpty name="categoryname"><title><bean:write name="categoryname"/> Course in <logic:notEmpty name="institutenamess"><bean:write name="institutenamess"/></logic:notEmpty> | LearnEZY</title></logic:notEmpty>

<logic:notEmpty name="coursescityname"><title><logic:notEmpty name="coursenames"><bean:write name="coursenames"/> </logic:notEmpty> Course in <bean:write name="coursescityname"/>| LearnEZY</title></logic:notEmpty>

<logic:notEmpty name="citynamecoursearea"><title><logic:notEmpty name="coursenames"><bean:write name="coursenames"/> </logic:notEmpty>-in-<logic:notEmpty name="areaname"><bean:write name="areaname"/> </logic:notEmpty>-<bean:write name="citynamecoursearea"/> | LearnEZY</title></logic:notEmpty>
<logic:notEmpty name="elearningcity"><title><logic:notEmpty name="elearning"><bean:write name="elearning"/> </logic:notEmpty><logic:notEmpty name="classroom"><bean:write name="classroom"/> </logic:notEmpty>-<logic:notEmpty name="coursenames"><bean:write name="coursenames"/> </logic:notEmpty>-in-<bean:write name="elearningcity"/>| LearnEZY</title></logic:notEmpty>
<logic:notEmpty name="elearningcitylocation"><title><logic:notEmpty name="elearninglocation"><bean:write name="elearninglocation"/> </logic:notEmpty><logic:notEmpty name="classroomlocation"><bean:write name="classroomlocation"/> </logic:notEmpty>-<logic:notEmpty name="coursenames"><bean:write name="coursenames"/> </logic:notEmpty>-In-<logic:notEmpty name="areaname"><bean:write name="areaname"/> </logic:notEmpty>-<bean:write name="elearningcitylocation"/>| LearnEZY</title></logic:notEmpty>

<link href="favicon.png" type="image/x-icon" rel="shortcut icon">
<link href="assets/css/master.css" rel="stylesheet">
<link href="assets/css/fSelect.css" rel="stylesheet" type="text/css">
<script src="<%=request.getContextPath()%>/js/jquery-2.1.1.min.js"></script>
<script src="<%=request.getContextPath()%>/js/validation/jquery.validate.min.js"></script>
<script src="<%=request.getContextPath()%>/js/validation/additional-methods.js"></script>

<script>
$(document).ready(function() {

    $("#submitid").click(function(){
    	if($('[type="checkbox"]').is(":checked")){
        
    		
    	}
    	
    	else
    	{
        	alert("Atleast Select one option");
        	return false;
        	

    	}
    	
    	});
    });





$(document).ready(function() {
    var favorite = [];
    var favorite1 = [];
    var favorite2 = [];
    var favorite3 = [];
    var favorite4 = [];
    
    var institute;
    var city;
    var location;
    var concatinstitute;
    var category=0;
    var coursename;
    var eleanrning;
    var classroom;
    var mode;
    
    
$("#submitid").click(function()
	{
    $.each($("input[name='institute']:checked"), function(){ 
    	favorite.push($(this).val());
    	
        
        $("#appdendinstituteid").val(favorite);
                                
        
        return true;

    });
    $.each($("input[name='locality']:checked"), function(){ 
        favorite1.push($(this).val());
        $("#appdendlocalityid").val(favorite1);
        return true;

    });
    $.each($("input[name='cityname']:checked"), function(){ 
        favorite2.push($(this).val());
        $("#appdendcityid").val(favorite2);
        return true;

    });
    $.each($("input[name='coursesnames']:checked"), function(){ 
        favorite3.push($(this).val());
        $("#appendcourses").val(favorite3);
        return true;

    });
    $.each($("input[name='elearning']:checked"), function(){ 
    	eleanrning= $(this).val();
        return true;

    });
    $.each($("input[name='classroom']:checked"), function(){ 
    	classroom= $(this).val();
        $("#appendclassroonid").val(classroom);
    	
        return true;

    });
category=$(".demo1").val();
$("#coursecatidss").val(category);
    
    if((favorite.length>0)&&(favorite1.length>0)&&(favorite2.length>0)&&(!favorite3.length>0)&&(category==null)&&(eleanrning!=1)&&(classroom!=1))
    {
       
        var insid=$("#appdendinstituteid").val();
        var cityid=$("#appdendcityid").val();
        var locality=$("#appdendlocalityid").val();
        var catname=$("#coursecatidss").val();
        
        //alert(insid);
        
    	$.ajax({	
    		type: "GET",		
    		url:"DynamicData.do?action=getinstitute",
    		data:{institute:insid},
    		async:false,
    		success: function(r){	
    			var json= JSON.parse(r);
    					$.each(json,function(i,obj){
        					institute=obj.CompanyName;
    						
    			});
    		} 
    	});

    	$.ajax({	
    		type: "GET",		
    		url:"DynamicData.do?action=getcityurl",
    		data:{cityid:cityid},
    		async:false,
    		success: function(r){	
    			var json= JSON.parse(r);
    					$.each(json,function(i,obj){
        					//alert(obj.CITY);
        					city=obj.CITY;
        					
    						
    			});
    		} 
    	});
    	$.ajax({	
    		type: "GET",		
    		url:"DynamicData.do?action=getlocalityurl",
    		data:{locality:locality},
    		async:false,
    		success: function(r){	
    			var json= JSON.parse(r);
    					$.each(json,function(i,obj){
    						location=obj.AREANAME;
    						
    			});
    		} 
    	});
    	$("#selectcourseid").attr("action","courses-in-"+institute.toLowerCase()+"-"+city.toLowerCase()+"-"+location.toLowerCase()+"");
    	
    	            
    }
    if((favorite.length>0)&&(!favorite1.length>0)&&(!favorite2.length>0)&&(!favorite3.length>0)&&(category!=null)&&(eleanrning!=1)&&(classroom!=1))
    {
        var catname=$("#coursecatidss").val();
        var insid=$("#appdendinstituteid").val();
        $.ajax({	
    		type: "GET",		
    		url:"DynamicData.do?action=getinstitute",
    		data:{institute:insid},
    		async:false,
    		success: function(r){	
    			var json= JSON.parse(r);
    					$.each(json,function(i,obj){
        					institute=obj.CompanyName;
    						
    			});
    		} 
    	});
        $.ajax({	
    		type: "GET",		
    		url:"DynamicData.do?action=getcoursecategory",
    		data:{catname:catname},
    		async:false,
    		success: function(r){	
    			var json= JSON.parse(r);
    					$.each(json,function(i,obj){
    						category=obj.coursecatname;
    						
    			});
    		} 
    	});
    	$("#selectcourseid").attr("action",""+institute.toLowerCase()+"-courses-in-"+category.toLowerCase()+"");
        
        

    }
    if((favorite.length>0)&&(!favorite1.length>0)&&(favorite2.length>0)&&(!favorite3.length>0)&&(category!=null)&&(eleanrning!=1)&&(classroom!=1))
    {
        var insid=$("#appdendinstituteid").val();
        var cityid=$("#appdendcityid").val();
        var catname=$("#coursecatidss").val();
        
       // alert(insid);
        
    	$.ajax({	
    		type: "GET",		
    		url:"DynamicData.do?action=getinstitute",
    		data:{institute:insid},
    		async:false,
    		success: function(r){	
    			var json= JSON.parse(r);
    					$.each(json,function(i,obj){
        					institute=obj.CompanyName;
    						
    			});
    		} 
    	});

    	$.ajax({	
    		type: "GET",		
    		url:"DynamicData.do?action=getcityurl",
    		data:{cityid:cityid},
    		async:false,
    		success: function(r){	
    			var json= JSON.parse(r);
    					$.each(json,function(i,obj){
        					city=obj.CITY;
        					
    						
    			});
    		} 
    	});
    	 $.ajax({	
     		type: "GET",		
     		url:"DynamicData.do?action=getcoursecategory",
     		data:{catname:catname},
     		async:false,
     		success: function(r){	
     			var json= JSON.parse(r);
     					$.each(json,function(i,obj){
     						category=obj.coursecatname;
     						
     			});
     		} 
     	});
       	$("#selectcourseid").attr("action",""+category.toLowerCase()+"_courses_in_"+institute.toLowerCase()+"_"+city.toLowerCase()+"");
    	
    	            
    }
    
    if((favorite.length>0)&&(favorite1.length>0)&&(favorite2.length>0)&&(!favorite3.length>0)&&(category!=null)&&(eleanrning!=1)&&(classroom!=1))
    {
       // alert("only for ins category and city");
        var insid=$("#appdendinstituteid").val();
        var cityid=$("#appdendcityid").val();
        var catname=$("#coursecatidss").val();
        var locality=$("#appdendlocalityid").val();
        
        
       // alert(insid);
        
    	$.ajax({	
    		type: "GET",		
    		url:"DynamicData.do?action=getinstitute",
    		data:{institute:insid},
    		async:false,
    		success: function(r){	
    			var json= JSON.parse(r);
    					$.each(json,function(i,obj){
        					institute=obj.CompanyName;
    						
    			});
    		} 
    	});

    	$.ajax({	
    		type: "GET",		
    		url:"DynamicData.do?action=getcityurl",
    		data:{cityid:cityid},
    		async:false,
    		success: function(r){	
    			var json= JSON.parse(r);
    					$.each(json,function(i,obj){
        					city=obj.CITY;
        					
    						
    			});
    		} 
    	});
    	 $.ajax({	
     		type: "GET",		
     		url:"DynamicData.do?action=getcoursecategory",
     		data:{catname:catname},
     		async:false,
     		success: function(r){	
     			var json= JSON.parse(r);
     					$.each(json,function(i,obj){
     						category=obj.coursecatname;
     						
     			});
     		} 
     	});
    	 $.ajax({	
     		type: "GET",		
     		url:"DynamicData.do?action=getlocalityurl",
     		data:{locality:locality},
     		async:false,
     		success: function(r){	
     			var json= JSON.parse(r);
     					$.each(json,function(i,obj){
     						location=obj.AREANAME;
     						
     			});
     		} 
     	});
    

        	
       	$("#selectcourseid").attr("action",""+category.toLowerCase()+"_courses_in_"+institute.toLowerCase()+"_"+location.toLowerCase()+"_"+city.toLowerCase()+"");
    	
    	            
    }
    if((favorite.length>0)&&(!favorite1.length>0)&&(!favorite2.length>0)&&(favorite3.length>0)&&(category==null)&&(eleanrning!=1)&&(classroom!=1))
    {
        var insid=$("#appdendinstituteid").val();
       var coursename=$("#appendcourses").val();
        
       // alert(coursename);
        
    	$.ajax({	
    		type: "GET",		
    		url:"DynamicData.do?action=getinstitute",
    		data:{institute:insid},
    		async:false,
    		success: function(r){	
    			var json= JSON.parse(r);
    					$.each(json,function(i,obj){
        					institute=obj.CompanyName;
    						
    			});
    		} 
    	});

    	$.ajax({	
    		type: "GET",		
    		url:"DynamicData.do?action=getcourse",
    		data:{coursename:coursename},
    		async:false,
    		success: function(r){	
    			var json= JSON.parse(r);
    					$.each(json,function(i,obj){
    						coursename=obj.CourseName;
    						
    			});
    		} 
    	});
    	 

        	
      	$("#selectcourseid").attr("action",""+coursename.toLowerCase()+"-courses-in-"+institute.toLowerCase()+"");
    	
    	            
    }

    if((!favorite.length>0)&&(!favorite1.length>0)&&(favorite2.length>0)&&(favorite3.length>0)&&(category==null)&&(eleanrning!=1)&&(classroom!=1))
    {
      //  alert("only course and city");
        var cityid=$("#appdendcityid").val();
       var coursename=$("#appendcourses").val();
        
     //   alert(coursename);
        
        $.ajax({	
    		type: "GET",		
    		url:"DynamicData.do?action=getcityurl",
    		data:{cityid:cityid},
    		async:false,
    		success: function(r){	
    			var json= JSON.parse(r);
    					$.each(json,function(i,obj){
        					city=obj.CITY;
        					
    						
    			});
    		} 
    	});
    	$.ajax({	
    		type: "GET",		
    		url:"DynamicData.do?action=getcourse",
    		data:{coursename:coursename},
    		async:false,
    		success: function(r){	
    			var json= JSON.parse(r);
    					$.each(json,function(i,obj){
    						coursename=obj.CourseName;
    						
    			});
    		} 
    	});
    	 

        	
      	$("#selectcourseid").attr("action",""+coursename.toLowerCase()+"-courses-in-"+city.toLowerCase()+"");
    	
    	            
    }
    
    if((!favorite.length>0)&&(favorite1.length>0)&&(favorite2.length>0)&&(favorite3.length>0)&&(category==null)&&(eleanrning!=1)&&(classroom!=1))
    {
        var cityid=$("#appdendcityid").val();
       var coursename=$("#appendcourses").val();
       var locality=$("#appdendlocalityid").val();
       
       // alert(coursename);
        
        $.ajax({	
    		type: "GET",		
    		url:"DynamicData.do?action=getcityurl",
    		data:{cityid:cityid},
    		async:false,
    		success: function(r){	
    			var json= JSON.parse(r);
    					$.each(json,function(i,obj){
        					city=obj.CITY;
        					
    						
    			});
    		} 
    	});
    	$.ajax({	
    		type: "GET",		
    		url:"DynamicData.do?action=getcourse",
    		data:{coursename:coursename},
    		async:false,
    		success: function(r){	
    			var json= JSON.parse(r);
    					$.each(json,function(i,obj){
    						coursename=obj.CourseName;
    						
    			});
    		} 
    	});
    	 $.ajax({	
      		type: "GET",		
      		url:"DynamicData.do?action=getlocalityurl",
      		data:{locality:locality},
      		async:false,
      		success: function(r){	
      			var json= JSON.parse(r);
      					$.each(json,function(i,obj){
      						location=obj.AREANAME;
      						
      			});
      		} 
      	});
     

        	
       	$("#selectcourseid").attr("action",""+coursename.toLowerCase()+"-courses-in-"+location.toLowerCase()+"-"+city.toLowerCase()+"");
    	
    	            
    }


    if((!favorite.length>0)&&(!favorite1.length>0)&&(favorite2.length>0)&&(favorite3.length>0)&&(category==null)&&(eleanrning==1)&&(classroom!=1))
    {
        var cityid=$("#appdendcityid").val();
       var coursename=$("#appendcourses").val();
       
        
        $.ajax({	
    		type: "GET",		
    		url:"DynamicData.do?action=getcityurl",
    		data:{cityid:cityid},
    		async:false,
    		success: function(r){	
    			var json= JSON.parse(r);
    					$.each(json,function(i,obj){
        					city=obj.CITY;
        					
    						
    			});
    		} 
    	});
    	$.ajax({	
    		type: "GET",		
    		url:"DynamicData.do?action=getcourse",
    		data:{coursename:coursename},
    		async:false,
    		success: function(r){	
    			var json= JSON.parse(r);
    					$.each(json,function(i,obj){
    						coursename=obj.CourseName;
    						mode="online";
    						
    			});
    		} 
    	});
    	 

        	
      	$("#selectcourseid").attr("action",""+mode+"-"+coursename.toLowerCase()+"-courses-in-"+city.toLowerCase()+"");
    	
    	            
    }
    if((!favorite.length>0)&&(!favorite1.length>0)&&(favorite2.length>0)&&(favorite3.length>0)&&(category==null)&&(eleanrning!=1)&&(classroom==1))
    {
        var cityid=$("#appdendcityid").val();
       var coursename=$("#appendcourses").val();
       
        
        $.ajax({	
    		type: "GET",		
    		url:"DynamicData.do?action=getcityurl",
    		data:{cityid:cityid},
    		async:false,
    		success: function(r){	
    			var json= JSON.parse(r);
    					$.each(json,function(i,obj){
        					city=obj.CITY;
        					
    						
    			});
    		} 
    	});
    	$.ajax({	
    		type: "GET",		
    		url:"DynamicData.do?action=getcourse",
    		data:{coursename:coursename},
    		async:false,
    		success: function(r){	
    			var json= JSON.parse(r);
    					$.each(json,function(i,obj){
    						coursename=obj.CourseName;
    						mode="classroom";
    						
    			});
    		} 
    	});
    	 

        	
      	$("#selectcourseid").attr("action",""+mode+"-"+coursename.toLowerCase()+"-courses-in-"+city.toLowerCase()+"");
    	
    	            
    }

    if((!favorite.length>0)&&(favorite1.length>0)&&(favorite2.length>0)&&(favorite3.length>0)&&(category==null)&&(eleanrning==1)&&(classroom!=1))
    {
        var cityid=$("#appdendcityid").val();
       var coursename=$("#appendcourses").val();
       var locality=$("#appdendlocalityid").val();
       
       
        
        $.ajax({	
    		type: "GET",		
    		url:"DynamicData.do?action=getcityurl",
    		data:{cityid:cityid},
    		async:false,
    		success: function(r){	
    			var json= JSON.parse(r);
    					$.each(json,function(i,obj){
        					city=obj.CITY;
        					
    						
    			});
    		} 
    	});
    	$.ajax({	
    		type: "GET",		
    		url:"DynamicData.do?action=getcourse",
    		data:{coursename:coursename},
    		async:false,
    		success: function(r){	
    			var json= JSON.parse(r);
    					$.each(json,function(i,obj){
    						coursename=obj.CourseName;
    						mode="online";
    						
    			});
    		} 
    	});
    	$.ajax({	
      		type: "GET",		
      		url:"DynamicData.do?action=getlocalityurl",
      		data:{locality:locality},
      		async:false,
      		success: function(r){	
      			var json= JSON.parse(r);
      					$.each(json,function(i,obj){
      						location=obj.AREANAME;
      						
      			});
      		} 
      	});
     

    	 

        	
      	$("#selectcourseid").attr("action",""+mode+"-"+coursename.toLowerCase()+"-courses-in-"+location.toLowerCase()+"-"+city.toLowerCase()+"");
    	
    	            
    }
    

    if((!favorite.length>0)&&(favorite1.length>0)&&(favorite2.length>0)&&(favorite3.length>0)&&(category==null)&&(eleanrning!=1)&&(classroom==1))
    {
        var cityid=$("#appdendcityid").val();
       var coursename=$("#appendcourses").val();
       var locality=$("#appdendlocalityid").val();
       
       
        
        $.ajax({	
    		type: "GET",		
    		url:"DynamicData.do?action=getcityurl",
    		data:{cityid:cityid},
    		async:false,
    		success: function(r){	
    			var json= JSON.parse(r);
    					$.each(json,function(i,obj){
        					city=obj.CITY;
        					
    						
    			});
    		} 
    	});
    	$.ajax({	
    		type: "GET",		
    		url:"DynamicData.do?action=getcourse",
    		data:{coursename:coursename},
    		async:false,
    		success: function(r){	
    			var json= JSON.parse(r);
    					$.each(json,function(i,obj){
    						coursename=obj.CourseName;
    						mode="classroom";
    						
    			});
    		} 
    	});
    	$.ajax({	
      		type: "GET",		
      		url:"DynamicData.do?action=getlocalityurl",
      		data:{locality:locality},
      		async:false,
      		success: function(r){	
      			var json= JSON.parse(r);
      					$.each(json,function(i,obj){
      						location=obj.AREANAME;
      						
      			});
      		} 
      	});
     

    	 

        	
      	$("#selectcourseid").attr("action",""+mode+"-"+coursename.toLowerCase()+"-courses-in-"+location.toLowerCase()+"-"+city.toLowerCase()+"");
    	
    	            
    }



});
	
});





</script>


		<link rel="stylesheet" href="gotop/css/ap-scroll-top.css" type="text/css" media="all" />

        
		<script src="gotop/js/ap-scroll-top.js"></script>
<style>
label{display:block;}
.equal{font-size:13px;}
.jelect-options{width:130px;}
.btn {
    display: inline-block;
    margin-bottom: 7px;
    font-weight: normal;
    text-align: center;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    background-image: none;
    white-space: nowrap;
    padding: 8px 8px;
    font-size: 12px;
    line-height: 1;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    font-family: Montserrat , arial;
    border: 1px solid transparent;
    margin-left: 9px;
}
</style>
<script type="text/javascript">

if(document.getElementById("elearnid").checked) {
    document.getElementById('hiddenid').disabled = true;
}
if(document.getElementById("elearnid1").checked) {
    document.getElementById('classroom').disabled = true;
}
if(document.getElementById("paid").checked) {
    document.getElementById('paidid').disabled = true;
}
if(document.getElementById("free").checked) {
    document.getElementById('freeid').disabled = true;
}
if(document.getElementById("beginnerid").checked) {
    document.getElementById('beginnerhidden').disabled = true;
}
if(document.getElementById("intid").checked) {
    document.getElementById('intermedhidden').disabled = true;
}
if(document.getElementById("advancedid").checked) {
    document.getElementById('advancehidden').disabled = true;
}
if(document.getElementById("lessthanfiveid").checked) {
    document.getElementById('lessidhidden').disabled = true;
}
if(document.getElementById("greaterfiveid").checked) {
    document.getElementById('graterfidhidden').disabled = true;
}
if(document.getElementById("greatertwotid").checked) {
    document.getElementById('gratertidhidden').disabled = true;
}
if(document.getElementById("greaterfivethid").checked) {
    document.getElementById('graterfiveidhidden').disabled = true;
}



</script>
<script type="text/javascript">

$(document).ready(function()
		{
    $.ajax({	
		type: "GET",		
		url:"<%=request.getContextPath() %>/DynamicData.do?action=selectcoursecategory",
		async:false,
		success: function(r){	
			var json= JSON.parse(r);
			var firstOption= $("<option>",{text:"----Select----",value:""});
			$("#coursecategory").append(firstOption);
			$.each(json,function(i,obj){
				var option= $("<option>",{text:obj.coursecatname,value:obj.catid});
				$("#coursecategory").append(option);
			});
		} 
	});
	


    
		
	});
			
$(document).ready(function()
		{
    $.ajax({	
		type: "GET",		
		url:"DynamicData.do?action=elearningcountss",
		async:false,
		success: function(r){	
			var json= JSON.parse(r);
			$.each(json,function(i,obj){
				$("#elearnidcount").append(obj.elearningcount);
			
			});
		} 
	});
	


    
		
	});	
$(document).ready(function()
		{
    $.ajax({	
		type: "GET",		
		url:"DynamicData.do?action=classroomcounts",
		async:false,
		success: function(r){	
			var json= JSON.parse(r);
			$.each(json,function(i,obj){
				$("#classroomid").append(obj.classroom);
			
			});
		} 
	});
	


    
		
	});	
$(document).ready(function()
		{
    $.ajax({	
		type: "GET",		
		url:"DynamicData.do?action=freecounts",
		async:false,
		success: function(r){	
			var json= JSON.parse(r);
			$.each(json,function(i,obj){
				$("#freeid").append(obj.freecount);
			
			});
		} 
	});
	


    
		
	});	
$(document).ready(function()
		{
    $.ajax({	
		type: "GET",		
		url:"DynamicData.do?action=beginnercounts",
		async:false,
		success: function(r){	
			var json= JSON.parse(r);
			$.each(json,function(i,obj){
				$("#beginid").append(obj.beginnercount);
			
			});
		} 
	});
	


    
		
	});	
$(document).ready(function()
		{
    $.ajax({	
		type: "GET",		
		url:"DynamicData.do?action=advancedcounts",
		async:false,
		success: function(r){	
			var json= JSON.parse(r);
			$.each(json,function(i,obj){
				$("#advid").append(obj.Advanced);
			
			});
		} 
	});
	


    
		
	});	
$(document).ready(function()
		{
    $.ajax({	
		type: "GET",		
		url:"DynamicData.do?action=intermediatecounts",
		async:false,
		success: function(r){	
			var json= JSON.parse(r);
			$.each(json,function(i,obj){
				$("#interid").append(obj.intermediatecount);
			
			});
		} 
	});
	


    
		
	});	
$(document).ready(function()
		{
    $.ajax({	
		type: "GET",		
		url:"DynamicData.do?action=paidcounts",
		async:false,
		success: function(r){	
			var json= JSON.parse(r);
			$.each(json,function(i,obj){
				$("#paidcount").append(obj.paidcount);
			
			});
		} 
	});
	


    
		
	});	


$(document).ready(function()
		{
$("#submitids").click(function()
		{
	
var course=$(".courseName").val();
var locality=$(".locality").val();
var cities=$(".city").val();

if(course=="")
{

alert("Enter Course Name");
$(".courseName").focus();
return false;
}
else if(locality=="")
{

alert("Enter Locality");
$(".locality").focus();
return false;
}

else if(cities=="")
{

alert("Enter City");
$(".city").focus();
return false;
}
$("#searchform").attr("action",""+course.toLowerCase()+"-courses-in-"+locality.toLowerCase()+"-"+cities.toLowerCase()+"");
		});
	});
	
		
$(document).ready(function()
		{
$(".anchorclass").click(function(){
	
    var courseid=$(this).attr('id');
    alert(courseid);
  var coursename=$("#corsesid"+courseid).val();//alert(coursename);
  alert(coursename);
  var catid=$("#catidss"+courseid).val();
  alert(catid);

$(".forms").attr("action",""+coursename.toLowerCase().replace(/\s/g, "")+"-course-details");  
    
});
});


</script>
</head>

<body>

<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->

<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200"> 
  

  
  <div id="wrapper"> 
                <jsp:include page="commonindexheader.jsp" /> 
      
   
    <div class="wrap-title-page">
      <div class="container">
        <div class="row">
          <div class=" col-xs-12">
          <logic:notEmpty name="institutenames">
            <h1 class="ui-title-page">Courses in <bean:write name="institutenames"/></h1></logic:notEmpty>
            <logic:notEmpty name="institutenamess">
            <h1 class="ui-title-page"><bean:write name="categoryname"/> Course In <logic:notEmpty name="institutenamess"><bean:write name="institutenamess"/></logic:notEmpty></h1></logic:notEmpty>
          <logic:notEmpty name="institutenamesss">
            <h1 class="ui-title-page"><logic:notEmpty name="categoryname"><bean:write name="categoryname"/> </logic:notEmpty> Course In <bean:write name="institutenamesss"/> <logic:notEmpty name="citiess"><bean:write name="citiess"/></logic:notEmpty> </h1></logic:notEmpty>
          <logic:notEmpty name="institutenamessss">
            <h1 class="ui-title-page"><logic:notEmpty name="categoryname"><bean:write name="categoryname"/> </logic:notEmpty> Course In <bean:write name="institutenamessss"/><logic:notEmpty name="areaname"><bean:write name="areaname"/></logic:notEmpty> <logic:notEmpty name="citiess"><bean:write name="citiess"/> </logic:notEmpty></h1></logic:notEmpty>
          <logic:notEmpty name="institutenamesssss">
            <h1 class="ui-title-page"><logic:notEmpty name="coursenames"><bean:write name="coursenames"/> </logic:notEmpty> Course  In <bean:write name="institutenamesssss"/></h1></logic:notEmpty>
        <logic:notEmpty name="coursescityname">
            <h1 class="ui-title-page"><logic:notEmpty name="coursenames"><bean:write name="coursenames"/> </logic:notEmpty> Course  In <bean:write name="coursescityname"/></h1></logic:notEmpty>
       <logic:notEmpty name="citynamecoursearea">
            <h1 class="ui-title-page"> <logic:notEmpty name="coursenames"><bean:write name="coursenames"/></logic:notEmpty>-In-<logic:notEmpty name="areaname"><bean:write name="areaname"/></logic:notEmpty>-<bean:write name="citynamecoursearea"/></h1></logic:notEmpty>
       <logic:notEmpty name="elearningcity">
            <h1 class="ui-title-page"> <logic:notEmpty name="elearning"><bean:write name="elearning"/> </logic:notEmpty><logic:notEmpty name="classroom"><bean:write name="classroom"/> </logic:notEmpty>-<logic:notEmpty name="coursenames"><bean:write name="coursenames"/> </logic:notEmpty>-in-<bean:write name="elearningcity"/></h1></logic:notEmpty>
         <logic:notEmpty name="elearningcitylocation">
            <h1 class="ui-title-page"><logic:notEmpty name="elearninglocation"><bean:write name="elearninglocation"/> </logic:notEmpty><logic:notEmpty name="classroomlocation"><bean:write name="classroomlocation"/> </logic:notEmpty>-<logic:notEmpty name="coursenames"><bean:write name="coursenames"/> </logic:notEmpty>-In-<logic:notEmpty name="areaname"><bean:write name="areaname"/> </logic:notEmpty>-<bean:write name="elearningcitylocation"/> </h1></logic:notEmpty>
          </div>
          
          
           
        </div>
      </div>
      <!-- end container--> 
    </div>
    <!-- end wrap-title-page -->
   
    
    <div class="section-breadcrumb">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="wrap-breadcrumb clearfix">
              <ol class="breadcrumb">
                <li><a href="index.html"><i class="icon stroke icon-House"></i></a></li>
                <li><a href="javascript:void(0);">courses categories</a></li>
                <li class="active">all courses</li>
              </ol>
              <div class="sorting">
                
                 <!--search starts here-->
   
                             
                    
            
             <div class=" find-course_mod-b">
             <logic:notEmpty name="results">
             <bean:write name="results"></bean:write>
             </logic:notEmpty>
             
             
                                            <form action="DynamicData.do?action=coursesearch" id="searchform" method="post">
                                                <div class="form-group">
                                                    <label>Course Name*</label>
                                                    <input type="text" name="courseName" class="input-text full-width courseName" placeholder="Java"  />
                                                </div>
                                                <div class="form-group">
                                                    <label>Locality</label>
                                                   <input type="text" name="locality" class="input-text full-width locality" placeholder="Rajajinagar"  />
                                                </div>
                                                <div class="form-group">
                                                    <label>City</label>
                                                      <input type="text" name="city" class="input-text full-width city" placeholder="Rajajinagar"  />
                                                </div>
                                                
                                                <button type="submit" id="submitids"  class="btn btn-primary btn-effect">SEARCH AGAIN</button>
                                            </form>
                                        </div>
                                        </div>
                                          <!--search ends here--> 
              </div>
              <!-- end sorting --> 
              
            </div>
              
          </div>
        </div>
        <!-- end row--> 
      </div>
      <!-- end container--> 
    </div>
    <!-- end section-breadcrumb-->
              
                                        
                                        
    <main class="main-content">
      <div class="container">
        <div class="row">
        <html:form action="DynamicData.do?action=selectingcourses" styleId="selectcourseid" method="post">
        
        
         <div class="col-md-2 filter-content">
          <div class="toggle-container filters-container">
                                       
                            
                                <div class="panel style1 arrow-right">
                                    <h4 class="panel-title collapsed">Mode
                                    </h4>
                                    <div id="accomodation-type-filter" class="panel-collapse collapse in">
                                        <div class="panel-content">
                                            <div class="checkbox">
							<label>
							  <input type="checkbox" id="elearnid" name="elearning" value="1">eLearning<small id="elearnidcount"></small>
							<input type="hidden" id="hiddenid" name="elearning" value="null">
						<input type="hidden" name="appendinstitutevalue" id="appdendinstituteid">
							<input type="hidden" name="appendlocalityname" id="appdendlocalityid">
							<input type="hidden" name="appendcityname" id="appdendcityid">
							<input type="hidden" name="hiddenappendcoursesname" id="appendcourses">
							<input type="hidden" name="appendcoursecatnames" id="coursecatidss">
									<input type="hidden" name="appendclassroomname" id="appendclassroonid">
							
							
								
							</label>
						</div>
                          <div class="checkbox">
							<label>
							  <input type="checkbox" id="elearnid1" name="classroom" value="1">Classroom<small id="classroomid"></small>
								<input type="hidden" id="classroom" name="classroom" value="null">
							
							</label>
						</div> 
						    
						
						
						
						
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="panel style1 arrow-right">
                                   <h4 class="panel-title collapsed">Cost
                                    </h4>
                                    <div id="amenities-filter" class="panel-collapse collapse in ">
                                        <div class="panel-content">
                                             <div class="checkbox">
							<label>
							  <input type="checkbox" name="paid" id="paid" value="0">Paid<small id="paidcount"></small>
							  <input type="hidden" id="paidid" name="paid" value="null">
							  
							</label>
						</div>
                          <div class="checkbox">
							<label>
							  <input type="checkbox" name="free" id="free" value="1">Free<small  id="freeid"></small>
							  <input type="hidden" id="freeid" name="free" value="null">
							  
							</label>
						</div>
                          
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="panel style1 arrow-right">
                                   <h4 class="panel-title collapsed">Course
                                    </h4>
                                    <logic:notEmpty name="courselist">
                                    <logic:iterate id="courselist" name="courselist">                                    
                                    <div id="amenities-filter" class="panel-collapse collapse in">
                                        <div class="panel-content">
                                             <div class="checkbox">
							<label>
							  <input type="checkbox" name="coursesnames" id="coursesid" value="<bean:write name="courselist" property="coursesid" />"><bean:write name="courselist" property="courseName" /><small id="paidcount"></small>
							  
							</label>
						</div>
                          </div>
                                        
                                    </div></logic:iterate>
                                    </logic:notEmpty>
                                </div>
                             
                                <div class="panel style1 arrow-right">
                                   <h4 class="panel-title collapsed">Institute
                                    </h4>
                                    <logic:notEmpty name="institutelist">
                                    <logic:iterate id="institutelist" name="institutelist">                                    
                                    <div id="amenities-filter" class="panel-collapse collapse in ">
                                        <div class="panel-content">
                                             <div class="checkbox">
							<label>
							  <input type="checkbox" name="institute" id="institute" value="<bean:write name="institutelist" property="companyid" />"><bean:write name="institutelist" property="compname" /><input type="hidden" name="companyname" value="<bean:write name="institutelist" property="compname" />"><small id="paidcount"></small>
							  
							</label>
						</div>
                          </div>
                                        
                                    </div></logic:iterate>
                                    </logic:notEmpty>
                                </div>
                          
                                 <div class="panel style1 arrow-right">
                                   <h4 class="panel-title collapsed">Locality
                                    </h4>
                                    <logic:notEmpty name="localitylist">
                                    <logic:iterate id="localitylist" name="localitylist">                                    
                                    <div id="amenities-filter" class="panel-collapse collapse in ">
                                        <div class="panel-content">
                                             <div class="checkbox">
							<label>
							  <input type="checkbox" name="locality" id="locality" value="<bean:write name="localitylist" property="areaids" />"><bean:write name="localitylist" property="areaname" /><small id="paidcount"></small>
							  
							</label>
						</div>
                          </div>
                                        
                                    </div></logic:iterate>
                                    </logic:notEmpty>
                                </div>
                          <div class="panel style1 arrow-right">
                                   <h4 class="panel-title collapsed">City
                                    </h4>
                                    <logic:notEmpty name="citylist">
                                    <logic:iterate id="citylist" name="citylist">                                    
                                    <div id="amenities-filter" class="panel-collapse collapse in ">
                                        <div class="panel-content">
                                             <div class="checkbox">
							<label>
							  <input type="checkbox" name="cityname" id="cityid" value="<bean:write name="citylist" property="cityids" />"><bean:write name="citylist" property="cityname" /><small id="paidcount"></small>
							  
							</label>
						</div>
                          </div>
                                        
                                    </div></logic:iterate>
                                    </logic:notEmpty>
                                </div>
                          
                                
                                
                                
                                
                                
                                <div class="panel style1 arrow-right">
                                    <h4 class="panel-title collapsed">
                                         Level
                                    </h4>
                                    <div id="language-filter" class="panel-collapse collapse in">
                                        <div class="panel-content">
                                           <div class="checkbox">
							<label>
							  <input type="checkbox" name="beginner" id="beginnerid" value="Beginner">Beginner<small id="beginid"></small>
							  <input type="hidden" id="beginnerhidden" name="beginner" value="null">
							  
							</label>
						</div>
						<div class="checkbox">
							<label>
							  <input type="checkbox" name="intermediate" id="intid" value="Intermediate">Intermediate<small id="interid"></small>
							  <input type="hidden" id="intermedhidden" name="intermediate" value="null">
							  
							</label>
						</div>
						<div class="checkbox">
							<label>
							  <input type="checkbox" name="advanced" id="advancedid" value="Advanced">Advanced <small id="advid"></small>
								<input type="hidden" id="advancehidden" name="advanced" value="null">
							
							
							</label>
						</div>
						
                                        </div>
                                        
                                    </div>
                                    
                                    
                                </div>
                                
                                
                                  <div class="panel style1 arrow-right">
                                     <h4 class="panel-title collapsed">Price
                                    </h4>
                                    <div id="language-filter1" class="panel-collapse collapse in ">
                                        <div class="panel-content">
                                           <div class="checkbox">
							<label>
							  <input type="checkbox" name="lessthanfive" id="lessthanfiveid" value="500"><i  class="fa fa-angle-left equal"></i><span class="equal">= </span>
							  	<input type="hidden" id="lessidhidden" name="lessthanfive" value="-1">
							  
<i class="fa fa-inr"></i> 500

							</label>
						</div>
						<div class="checkbox">
							<label>
							  <input type="checkbox" name="greaterfive" id="greaterfiveid" value="5002000"><i  class="fa fa-angle-right equal"></i>
							  <input type="hidden" id="graterfidhidden" name="greaterfive" value="0000000">
							  
<i class="fa fa-inr"></i> 500 &amp; <i  class="fa fa-angle-left equal"></i><span class="equal">= </span>
<i class="fa fa-inr"></i> 2000
							</label>
						</div>
						<div class="checkbox">
							<label>
							  <input type="checkbox" name="greatertwot" id="greatertwotid" value="20005000"><i  class="fa fa-angle-right equal"></i>
							   <input type="hidden" id="gratertidhidden" name="greatertwot" value="00000000">
							  
<i class="fa fa-inr"></i> 2000 &amp; <i  class="fa fa-angle-left equal"></i><span class="equal">= </span>
<i class="fa fa-inr"></i> 5000
							</label>
						</div>
                        <div class="checkbox">
							<label>
							 <input type="checkbox" name="greaterthanfiveth" id="greaterfivethid" value="5000"><i  class="fa fa-angle-right equal"></i>
							   <input type="hidden" id="graterfiveidhidden" name="greaterthanfiveth" value="500000000">


<i class="fa fa-inr"></i> 5000 
							</label>
						</div>
						
                                        </div>
                                        
                                    </div>
                                    
                                    
                                </div>
                                
                                 <div class="panel style1 arrow-right">
                                   <h4 class="panel-title collapsed">Select Categories
                                    </h4>
                                    <div id="amenities-filter" class="panel-collapse collapse in ">
                                        <div class="panel-content">
                                          <select class="demo1" name="coursecategory" id="coursecategory" multiple="multiple">
                                 </select>
                          
                                        </div>
                                        
                                    </div>
                                </div>
                          </div>      
                      
                    <input type="submit" id="submitid" class="btn btn-primary btn-effect" value="Submit" onClick="checkForm('Form')"><input type="reset" class="btn btn-primary btn-effect" value="Reset">          
         </div>
         </html:form>
          <div class="col-md-10 col-sm-12">
         
           <div id="paginationdemo" class="demo">
           
            <div id="p1" class="pagedemo _current" style="">           
           
            <div class="wrap-title wrap-title_mod-b">
              <div class="title-list">Showing <span class="title-list__number">1 - 9</span> of  total <span class="title-list__number">50</span> Courses</div>  
            </div>
            <div class="sorting1"> <div  class="select jelect">
                  <input id="jelect" name="sorting" value="0" data-text="imagemin" type="text" class="jelect-input">
                  <div tabindex="0" role="button" class="jelect-current">Sort By</div>
                  <ul class="jelect-options">
                    <a href="DynamicData.do?action=ratewiseselectingcourses">   <li class="jelect-option jelect-option_state_active">Ratings</li></a>
                    <a href="DynamicData.do?action=pricelowtohighselectingcourses"> <li  class="jelect-option">Price - Low to High</li></a>
                     <a href="DynamicData.do?action=pricehightolowselectingcourses">  <li  class="jelect-option">Price - High to Low</li></a>
                  </ul>
                </div>
                </div>
            <!-- end wrap-title -->
            <div style="clear:both;"></div>
           
            <div class="posts-wrap">
             <logic:notEmpty name="latestcourses">
            <logic:iterate id="latestcourses" name="latestcourses">
              <article class="post post_mod-c clearfix advantages__item wow zoomIn" data-wow-duration="2s">
                <div class="entry-media">
                  <div class="entry-thumbnail"> <a href="DynamicData.do?action=coursesdescription&amp;id=<bean:write name="latestcourses" property="coursesid" />&amp;catid=<bean:write name="latestcourses" property="catid" />" ><img class="img-responsive" src="<%=request.getContextPath()%>/courselogos/<bean:write name="latestcourses"  property="courselatestlogo" />" width="370" height="250" alt="Foto"/></a> </div>
                </div>
                                <form action="DynamicData.do?action=coursesdescription" method="post" class="forms">
                
                <div class="entry-main entry-main_mod-a">
                  <h3 class="entry-title ui-title-inner"><a href="DynamicData.do?action=coursesdescription&amp;id=<bean:write name="latestcourses" property="coursesid" />&amp;catid=<bean:write name="latestcourses" property="catid" />"><bean:write name="latestcourses" property="coursenamelatest"></bean:write></a></h3>
                  <div class="entry-meta decor decor_mod-b"> <span class="entry-autor"> <span>By </span> <a class="post-link" href="javascript:void(0);"><bean:write name="latestcourses" property="compname"></bean:write></a> </span> <span class="entry-date"><a href="javascript:void(0);"><bean:write name="latestcourses" property="cityname"></bean:write></a></span> </div>
                  <div class="stars"><ul class="rating">
                      <li><img src="<bean:write name="latestcourses" property="rating" />" /></li>
                     
                    </ul>
                    <input type="hidden"  id="corsesid<bean:write name="latestcourses" property="coursesid" />" value="<bean:write name="latestcourses" property="coursenamelatest" />">
                    <input type="hidden" name="id" id="corsesid<bean:write name="latestcourses" property="coursesid" />" value="<bean:write name="latestcourses" property="coursesid" />">
                  
                  
                  <input type="hidden" name="catid" id="catidss<bean:write name="latestcourses" property="coursesid" />" value="<bean:write name="latestcourses" property="catid" />">
                   <input type="hidden" name="institute"  value="<bean:write name="latestcourses" property="compname" />">
                            
                            <span class="color"><i class="fa fa-inr"></i>
					<bean:write name="latestcourses" property="fees" /></span>
                            </div>
                 
                  <div class="entry-footer">
                                      <button type="submit" class="btn btn-primary btn-effect  anchorclass " id="<bean:write name="latestcourses" property="coursesid" />">View Details</button></div>
                  
                </div>
                </form>
              </article>
               </logic:iterate>
            </logic:notEmpty>
              <!-- end post -->
              <!-- end post -->
            </div>
           
            </div>
            <!-- end posts-wrap -->
            </div> <!-- page 1 -->
            
            
          <!--  <div id="p2" class="pagedemo" style="display:none;">Page 2</div>
				<div id="p3" class="pagedemo" style="display:none;">Page 3</div>
				<div id="p4" class="pagedemo" style="display:none;">Page 4</div>
				<div id="p5" class="pagedemo" style="display:none;">Page 5</div>
				<div id="p6" class="pagedemo" style="display:none;">Page 6</div>
				<div id="p7" class="pagedemo" style="display:none;">Page 7</div>
				<div id="p8" class="pagedemo" style="display:none;">Page 8</div>
				<div id="p9" class="pagedemo" style="display:none;">Page 9</div>
				<div id="p10" class="pagedemo" style="display:none;">Page 10</div>
				<div id="demo5">                   
                </div>-->
            
            </div> <!-- pagination --> 
          </div>
          <!-- end col --> 
          
          
                
          
        </div>
        <!-- end row --> 
      </div>
      <!-- end container --> 
      
      
     
      
      
    </main>
    <!-- end main-content -->
    
       <jsp:include page="Commonfooter.jsp"></jsp:include>
      </div>
  <!-- end wrapper --> 
</div>
<!-- end layout-theme --> 

<!-- SCRIPTS -->
 
<script src="assets/js/jquery-migrate-1.2.1.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<script src="assets/js/modernizr.custom.js"></script> 
<script src="assets/js/waypoints.min.js"></script> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> 

<!--THEME--> 
<script  src="assets/plugins/sliderpro/js/jquery.sliderPro.min.js"></script> 
<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script> 
<script src="assets/plugins/isotope/jquery.isotope.min.js"></script> 
<script src="assets/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script> 
<script src="assets/plugins/datetimepicker/jquery.datetimepicker.js"></script> 
<script src="assets/plugins/jelect/jquery.jelect.js"></script> 
<script src="assets/plugins/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js"></script> 
<script src="assets/js/cssua.min.js"></script> 
<script src="assets/js/wow.min.js"></script> 
<script src="assets/js/custom.min.js"></script>

<script src="js/fSelect.js"></script>
<script>
$(function() {
        $('.demo1').fSelect();
    });
</script>

		<script src="assets/js/jquery.paginate.js" type="text/javascript"></script>
		<script type="text/javascript">
		$(function() {
			
			$("#demo5").paginate({
				count 		: 10,
				start 		: 1,
				display     : 5,
				border					: true,
				border_color			: '#E84C3D',
				text_color  			: '#fff',
				background_color    	: '#E84C3D',	
				border_hover_color		: '#ccc',
				text_hover_color  		: '#000',
				background_hover_color	: '#fff', 
				images					: false,
				mouse					: 'press',
				onChange     			: function(page){
											$('._current','#paginationdemo').removeClass('_current').hide();
											$('#p'+page).addClass('_current').show();
										  }
			});
		});
		</script>
        <script type="text/javascript">
            // Setup plugin with default settings
            $(document).ready(function() {

                $.apScrollTop({
                    'onInit': function(evt) {
                        console.log('apScrollTop: init');
                    }
                });

                // Add event listeners
                $.apScrollTop().on('apstInit', function(evt) {
                    console.log('apScrollTop: init');
                });

                $.apScrollTop().on('apstToggle', function(evt, details) {
                    console.log('apScrollTop: toggle / is visible: ' + details.visible);
                });

                $.apScrollTop().on('apstCssClassesUpdated', function(evt) {
                    console.log('apScrollTop: cssClassesUpdated');
                });

                $.apScrollTop().on('apstPositionUpdated', function(evt) {
                    console.log('apScrollTop: positionUpdated');
                });

                $.apScrollTop().on('apstEnabled', function(evt) {
                    console.log('apScrollTop: enabled');
                });

                $.apScrollTop().on('apstDisabled', function(evt) {
                    console.log('apScrollTop: disabled');
                });

                $.apScrollTop().on('apstBeforeScrollTo', function(evt, details) {
                    console.log('apScrollTop: beforeScrollTo / position: ' + details.position + ', speed: ' + details.speed);

                    // You can return a single number here, which means that to this position
                    // browser window scolls to
                    /*
                    return 100;
                    */

                    // .. or you can return an object, containing position and speed:
                    /*
                    return {
                        position: 100,
                        speed: 100
                    };
                    */

                    // .. or do not return anything, so the default values are used to scroll
                });

                $.apScrollTop().on('apstScrolledTo', function(evt, details) {
                    console.log('apScrollTop: scrolledTo / position: ' + details.position);
                });

                $.apScrollTop().on('apstDestroy', function(evt, details) {
                    console.log('apScrollTop: destroy');
                });

            });


            // Add change events for options
            $('#option-enabled').on('change', function() {
                var enabled = $(this).is(':checked');
                $.apScrollTop('option', 'enabled', enabled);
            });

            $('#option-visibility-trigger').on('change', function() {
                var value = $(this).val();
                if (value == 'custom-function') {
                    $.apScrollTop('option', 'visibilityTrigger', function(currentYPos) {
                        var imagePosition = $('#image-for-custom-function').offset();
                        return (currentYPos > imagePosition.top);
                    });
                }
                else {
                    $.apScrollTop('option', 'visibilityTrigger', parseInt(value));
                }
            });

            $('#option-visibility-fade-speed').on('change', function() {
                var value = parseInt($(this).val());
                $.apScrollTop('option', 'visibilityFadeSpeed', value);
            });

            $('#option-scroll-speed').on('change', function() {
                var value = parseInt($(this).val());
                $.apScrollTop('option', 'scrollSpeed', value);
            });

            $('#option-position').on('change', function() {
                var value = $(this).val();
                $.apScrollTop('option', 'position', value);
            });
		</script><script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>


</body>
</html>
