<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ page import="java.sql.*" %>
<%@page import="com.itech.elearning.forms.LoginForm"%>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui">
<title>LearnEzy.com - Registration</title>
<link href="favicon.png" type="image/x-icon" rel="shortcut icon">
<link href="assets/css/master.css" rel="stylesheet">

<!-- BOOTSTRAP -->
<link href="assets/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="assets/css/bootstrap-responsive.css" rel="stylesheet" media="screen">

<!-- FONT AWESOME -->
<link href="assets/css/font-awesome.min.css" rel="stylesheet" media="screen">


<link rel="stylesheet" href="assets/css/tmm_form_wizard_style_demo.css" />
		<link rel="stylesheet" href="assets/css/grid.css" />
		<link rel="stylesheet" href="assets/css/tmm_form_wizard_layout.css" />
		

<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="src/DateTimePicker.js"></script>
<script type="text/javascript" src="date/jquery-ui.js"></script>
<script type="text/javascript" src="DateTime/jquery-calendar.js"></script>
<script type="text/javascript" src="DateTime/calendar.js"></script>
<script type="text/javascript" src="js/jQuery.print.js"></script>


	
<!-- Load jQuery and the validate plugin and masterdata js file -->
  <script type="text/javascript" src="<%=request.getContextPath()%>/js/validation/jquery.validate.min.js"></script>
  <script type="text/javascript" src="<%=request.getContextPath()%>/js/validation/additional-methods.js"></script>
  <script type="text/javascript" src="<%=request.getContextPath()%>/js/validation/masterdata.validation.js"></script>

<!-- Dependencies -->
<script src="js/jquery.ui.draggable.js" type="text/javascript"></script>
		<!-- Core files -->
		<script src="js/jquery.alerts.js" type="text/javascript"></script>
<script type="text/javascript" src="js/student/studentRegistration.js"></script>

	</head>
</body>
</html>