<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui">
		<meta name="robots" content="INDEX, FOLLOW">
		<meta name="googlebot" content="noodp">
				<meta name="description" content="<Course-Name> Has Been In A Lots Of Demand In Recent Times. Learnezy Is Here To Provide You The Best <Course-Name> In <City>.">
		
		<title>Contact Us | LearnEZY</title>
		<link href="favicon.png" type="image/x-icon" rel="shortcut icon">
		<link href="assets/css/master.css" rel="stylesheet">

<script src="//code.jquery.com/jquery-1.9.1.js"></script>
  <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
  
  <!-- jQuery Form Validation code -->
  <script>
  
  // When the browser is ready...
  $(function() {
  
    // Setup form validation on the #register-form element
    $("#register-form").validate({
    
        // Specify the validation rules
        rules: {
    	name: "required",
            
    	Email: {
                required: true,
                email: true
            },
            contct: {
                required: true,
                digits:true,
                minlength:10
            }
            
        },
        
        // Specify the validation error messages
        messages: {
        	name: "Please enter your first name",
            Email: "Please enter a valid email address",
            	contct:
            	{
        	required:"Enter MObile Number!",
        	digits:"Enter only numbers!",
        	minlength:"Minimum 10 digits!"
        	
        	

            	}
        },
        
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
  </script>



</head>

<body>
<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->
		<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200">
			<div id="wrapper">

			<jsp:include page="Commonheader.jsp"></jsp:include>

				<div class="wrap-title-page">
					<div class="container">
						<div class="row">
							<div class="col-xs-12"><h1 class="ui-title-page">Contact Us</h1></div>
						</div>
					</div><!-- end container-->
				</div><!-- end wrap-title-page -->


				<div class="section-breadcrumb">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div class="wrap-breadcrumb clearfix">
									<ol class="breadcrumb">
										<li><a href="dynamic.jsp"><i class="icon stroke icon-House"></i></a></li>
										<li class="active">CONTACT Us</li>
									</ol>
								</div>
							</div>
						</div><!-- end row -->
					</div><!-- end container -->
				</div><!-- end section-breadcrumb -->


				<main class="main-content">

					<div class="container">
						<div class="row">
							<div class="col-md-5">
								<section class="section_contacts">
									<h2 class="ui-title-inner decor decor_mod-a">Get in Touch with us</h2>
									<p>If You Have Any Queries, Feel Free To Contact Us Using The Form or At Below Given Details</p>
									<!--  <ul class="list-social list-inline">
										<li>
											<a href="javascript:void(0);"><i class="icon fa fa-facebook"></i></a>
										</li>
										<li>
											<a href="javascript:void(0);"><i class="icon fa fa-twitter"></i></a>
										</li>
										<li>
											<a href="javascript:void(0);"><i class="icon fa fa-google-plus"></i></a>
										</li>
										<li>
											<a href="javascript:void(0);"><i class="icon fa fa-linkedin"></i></a>
										</li>
										<li>
											<a href="javascript:void(0);"><i class="icon fa fa-behance"></i></a>
										</li>
										<li>
											<a href="javascript:void(0);"><i class="icon fa fa-vimeo"></i></a>
										</li>
										<li>
											<a href="javascript:void(0);"><i class="icon fa fa-whatsapp"></i></a>
										</li>
										<li>
											<a href="javascript:void(0);"><i class="icon fa fa-youtube-play"></i></a>
										</li>
									</ul>-->
									<ul class="list-contacts list-unstyled">
										<li class="list-contacts__item">
											<i class="icon stroke icon-Phone2"></i>
											<div class="list-contacts__inner">
												<div class="list-contacts__title">PHONE</div>
												<div class="list-contacts__info">+91-9611400344  |  +91-9611421111</div>
											</div>
										</li>
										<li class="list-contacts__item">
											<i class="icon stroke icon-Mail"></i>
											<div class="list-contacts__inner">
												<div class="list-contacts__title">EMAIL</div>
												<div class="list-contacts__info">info@learnezy.com</div>
											</div>
										</li>
										<li class="list-contacts__item">
											<i class="icon stroke icon-WorldWide"></i>
											<div class="list-contacts__inner">
												<div class="list-contacts__title">WEB</div>
												<div class="list-contacts__info">http://www.learnezy.com</div>
											</div>
										</li>
										
									</ul>
								</section><!-- end section_contacts -->
							</div><!-- end col -->

							<div class="col-md-7">
								<div class="section_map">
									<h2 class="ui-title-block">Send <strong>Us Message</strong></h2>
									<div class="wrap-subtitle">
										<div class="ui-subtitle-block ui-subtitle-block_w-line">If you have some feedback or want to ask any questions</div>
									</div><!-- end wrap-title -->
										
						<%
					   				 Object msg=request.getAttribute("varName");
					   				 if(msg==null)
					  				  {
					      			  msg="";
					   				  }
					   			 %>

									<%= msg %>
									<form action="DynamicData.do?action=contact" method="post" class="form-contact ui-form" id="register-form" name="frm_contact">
										<div class="row">
											<div class="col-md-6">
												<input class="form-control" 	type="text" name="name" placeholder="Full Name" required >
											</div>
											<div class="col-md-6">
												<input class="form-control" type="email" onblur="validemail();" name="Email" placeholder="Emailid" required>
											</div>
											<div class="col-md-6">
												<input class="form-control"  onBlur="checklengthAsk()" onkeyup="fmtmobile();"  type="text" name="contct" placeholder="ContactNo"  minlength="10" title="Phone number" required >
											</div>
											<div class="col-md-6">
												<input class="form-control" type="text" name="subject" placeholder="Subject" name="txtemail">
											</div>
											<div class="col-xs-12">
												<textarea class="form-control" required rows="11" name="textareamessage"></textarea>
												<button type="submit" class="btn btn-primary btn-effect">SEND NOW</button>
											</div>
										</div>
									</form>
								</div>
							</div><!-- end col -->
						</div><!-- end row -->
					</div><!-- end container -->

                  
				</main><!-- end main-content -->


				<jsp:include page="Commonfooter.jsp"></jsp:include>

			</div><!-- end wrapper -->
		</div><!-- end layout-theme -->
	<!-- SCRIPTS --> 
<script src="assets/js/jquery-migrate-1.2.1.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<script src="assets/js/modernizr.custom.js"></script> 
<script src="assets/js/waypoints.min.js"></script> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> 

<!--THEME--> 
<script  src="assets/plugins/sliderpro/js/jquery.sliderPro.min.js"></script> 
<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script> 
<script src="assets/plugins/isotope/jquery.isotope.min.js"></script> 
<script src="assets/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script> 
<script src="assets/plugins/datetimepicker/jquery.datetimepicker.js"></script> 
<script src="assets/plugins/jelect/jquery.jelect.js"></script> 
<script src="assets/plugins/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js"></script> 
<script src="assets/js/cssua.min.js"></script> 
<script src="assets/js/wow.min.js"></script> 
<script src="assets/js/custom.min.js"></script>


</body>
</html>
