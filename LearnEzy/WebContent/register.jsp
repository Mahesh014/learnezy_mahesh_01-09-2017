<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>

<%@page import="com.itech.elearning.forms.LoginForm"%>
<html>
<head>
<title>Itech Training :: Register</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta Charset="UTF-8">
<meta name="robots" content="NOINDEX, NOFOLLOW">
	<!-- Google Web Fonts
		================================================== -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:300,300italic,400,500,700%7cCourgette%7cRaleway:400,700,500%7cCourgette%7cLato:700' rel='stylesheet' type='text/css' />

		<!-- Basic Page Needs ================================================== -->
	
		<!-- CSS ================================================== -->

<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300' rel='stylesheet' type='text/css' />
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/cmsstyle.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>
<link rel="stylesheet" href="css/tmm_form_wizard_style_demo.css" />
<link rel="stylesheet" href="css/grid.css" />
<link rel="stylesheet" href="css/tmm_form_wizard_layout.css" />
<link rel="stylesheet" href="css/fontello.css" />
<link rel="stylesheet" type="text/css" href="date/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="src/DateTimePicker.css" />

<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="src/DateTimePicker.js"></script>
<script type="text/javascript" src="date/jquery-ui.js"></script>
<!-- Load jQuery and the validate plugin and masterdata js file (Make class name should calendar) -->
<link rel="stylesheet" type="text/css"	href="DateTime/jquery-calendar.css" />
<script type="text/javascript" src="DateTime/jquery-calendar.js"></script>
<script type="text/javascript" src="DateTime/calendar.js"></script>
<script type="text/javascript" src="js/jQuery.print.js"></script>

<!-- Load jQuery and the validate plugin and masterdata js file -->
    
 
  <script type="text/javascript" src="<%=request.getContextPath()%>/js/validation/jquery.validate.min.js"></script>
  <script type="text/javascript" src="<%=request.getContextPath()%>/js/validation/additional-methods.js"></script>
  <script type="text/javascript" src="<%=request.getContextPath()%>/js/validation/masterdata.validation.js"></script>


 <style>
 .row {
 	margin-top: 10px;
 }
 
 .row label{
 	margin-bottom: 5px;
 }
 .login a{  width: 67px;
  float: right;
  color: #000;
  margin-top: 22px;
  border: 1px solid #FFA500;
  padding: 6px;
  border-radius: 8px;
 }
  .courseDispTr{  
  background-color: #59bbdf;
 }
 .courseSelect{
	width:200px; 
	padding:2px;
	hight:50px;
 }
  .courseListTable{
 border: 1px;
 border-width: 12px;
 }
 .courseListTable td{
 height: 30px;
 vertical-align: middle;
 }

.sign {
border:1px solid #D0DDE1;
padding:14px;
border-radius:2px;
}
legend{width:20%;}

label{font-size:14px;}

.fa-lg{color:green;}

#captchaText{
background-image: url("..img/BG1.png);
}
</style>
 
<script type="text/javascript" src="js/student/studentRegistration.js"></script>

	</head>
	<body>


		<!-- - - - - - - - - - - - - Content - - - - - - - - - - - - -  -->
   <div class="header">	
   	 	     
             <div class="header-logo-nav">
             	  <div class="navbar navbar-inverse navbar-static-top nav-bg" role="navigation">
				      <div class="container">
				        <div class="navbar-header">
				           
				         <div class="logo"> <a class="navbar-brand" href="index.html"><img src="images/Itech Solutions logo.png" alt="" /></a></div>
				        
				        </div>
				        <div class="login">
                    
                   <a  href="login.jsp"> LOGIN</a>
                    
                    </div><!--login-->
				      </div>
				    </div>
		         <div class="clear"></div>
                  <div class="header-bottom">
                 <div class="container">
                 <div class="time">
                  <p><div id="clockbox"></div></p>
                 <script src="js/dynamicClock.js" type="text/javascript"></script>
                 </div>
               
                  
                 </div>
                 </div>
                 <div class="clear"></div>
	        </div>
	        <div class="header-banner">
	        	
			    
	           </div>
	          
             </div>
   <!-- End Header -->

		<div id="content">

			<div class="form-container">

				<div id="tmm-form-wizard" class="container substrate">

					<div class="row" align="center">
						<div class="col-xs-12">
							<h2 class="form-login-heading"><span>Registration Process</span></h2>
						</div>

					</div><!--/ .row-->

					<div class="row stage-container" align="center">

						<div class="stage tmm-current col-md-3 col-sm-3" id="topDiv1">
							<div class="stage-header head-icon head-icon-lock"></div>
							<div class="stage-content">
								<h3 class="stage-title">Registration</h3>
								<div class="stage-info">
									Registration details
								</div> 
							</div>
						</div><!--/ .stage-->

						<div class="stage col-md-3 col-sm-3 " id="topDiv2">
							<div class="stage-header head-icon head-icon-user"></div>

							<div class="stage-content">
								<h3 class="stage-title">Courses</h3>
								<div class="stage-info">
									Select the course
								</div>
							</div>
						</div><!--/ .stage-->

						<div class="stage col-md-3 col-sm-3" id="topDiv3">
							<div class="stage-header head-icon head-icon-payment"></div>
							<div class="stage-content">
								<h3 class="stage-title">Payment Information</h3>
								<div class="stage-info">
									Make the payment
								</div>
							</div>
						</div><!--/ .stage-->

						 <div class="stage col-md-3 col-sm-3" id="topDiv4">
							<div class="stage-header head-icon head-icon-details"></div>
							<div class="stage-content">
								<h3 class="stage-title">Login</h3>
								<div class="stage-info">
									Login details
								</div> 
							</div>
						</div><!--/ .stage-->

					</div><!--/ .row-->
<html:form action="studentRegistration.do?action=addUser" method="post" styleClass="register" styleId="register"   enctype="multipart/form-data">
						<div id="div1"   > 
					<div class="row">

						<div class="col-xs-12" >

							<div class="form-header">
								<div class="form-title form-icon title-icon-lock">
									<b>Registration details</b> 
								</div>
								<div   class="form-title" style="width: 725px;text-align: center;" >
									<b style="color: red;"><logic:notEmpty name="status"><bean:write name="status"/> </logic:notEmpty></b>
								</div>
								<div class="steps">
									Steps 1 - 4
								</div>
							</div><!--/ .form-header-->

						</div>

					</div><!--/ .row-->
			 
					
						


						<div class="form-wizard" >
							
							<div class="row">

								<div class="col-md-8 col-sm-7">

									

									<div class="row">
										
										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="first-name">First Name*</label>
												<input type="text" id="firstName" placeholder="First Name" name="firstName"  	onkeyup="validFNameonkey(this)" autofocus  />
												
											</fieldset><!--/ .input-first-name-->
										</div>
										
										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="last-name">Last Name</label>
												<input type="text" id="lastName"  name="lastName"	onkeyup="validLNameonkey(this)"  placeholder="Last Name"   />
												
											</fieldset><!--/ .input-first-name-->
										</div>
										
									</div><!--/ .row-->
									<div class="row">
										
										<div class="col-md-6 col-sm-6">

											<div class="input-block">
												<label>Date Of Birth <font style="font-size: 12px;">(DD/MM/YYYY)</font> </label>
												<div class="input-block">
													<input type="text" readonly name="dob" id="datepicker1" size="8"   onchange="onDateChange();"  placeholder="DOB" />
												</div><!--/ .dropdown-->
												
											</div><!--/ .input-role-->
											
										</div>

										<div class="col-md-6 col-sm-6">

											<fieldset class="input-block">
												<label>Gender</label>
												<div class="dropdown">
													<select name="gender" id="gender" class="dropdown-select"  >
														<option value="-1">Select</option>
									<option value="Male">Male</option>
									<option value="Female">Female</option>
													</select>
												</div><!--/ .dropdown-->
												
											</fieldset><!--/ .input-role-->
											
										</div>

									</div><!--/ .row-->
									<div class="row">
										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="last-name">Qualification</label>
												<input type="text" name="qualification" id="qualification"	onkeyup="isValidQlification(this)"  placeholder="Qualification"  />
												
											</fieldset><!--/ .input-first-name-->
										</div>
										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="email">Email*</label>
												<input type="text" name="emailId" id="emailId"	onchange="validemail(this)" id="email" class="form-icon form-icon-mail" placeholder="Please enter your email ID"  />
												
											</fieldset><!--/ .input-email-->
										</div>
										
									</div><!--/ .row-->
									
									<div class="row">
										
										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="phone">Mobile No*</label>
												<input type="text" name="contactno"  id="contactno" onkeyup="fmtmobile();" maxlength="10"  id="contactno" class="form-icon form-icon-phone" placeholder="Mobile No"  />
												
											</fieldset><!--/ .input-phone-->
										</div><!--/ .input-phone-->
										
										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="phone">Alternative Contact No</label>
												<input type="text"  name="phone" id="phone" onkeyup="fmtcontacno();"	maxlength="13"  id="phone" class="form-icon form-icon-phone" placeholder="Alternative Contact No"  />
												
											</fieldset><!--/ .input-phone-->
										</div><!--/ .input-phone-->
										
									</div><!--/ .row-->

									
									<div class="row">

										<div class="col-md-12 col-sm-12">
											<fieldset class="input-block">
												<label for="address">Address</label>
												<input type="text" id="address" name="address" placeholder="Address"  />
												
											</fieldset><!--/ .input-phone-->
										</div>
										
									</div><!--/ .row-->
									
									<div class="row">

										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="address">Country*</label>
												<div class="dropdown">
												<html:select property="countryid" styleId="countryid"  styleClass="dropdown-select" >
													<html:option value="-1">--Select Country--</html:option>
													<html:options collection="countryList" property="countryid" labelProperty="countryname" />
												</html:select>
												</div>
											</fieldset><!--/ .input-phone-->
										</div>
										
									

										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="address">State*</label>
												<div class="dropdown">
												<select name="stateid" id="stateid" class="dropdown-select" >
													<option value="-1">-Select State-</option>
												</select>
												</div>
											</fieldset><!--/ .input-phone-->
										</div>
										
									</div>
									<div class="row">
										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="address">City*</label>
												<div class="dropdown">
												<select name="cityid" id="cityid"  class="dropdown-select">
													<option value="-1">-Select City-</option>
												</select>
												</div>
											</fieldset><!--/ .input-phone-->
										</div>
										
										<div class="col-md-6 col-sm-6">
											<fieldset class="input-block">
												<label for="address">Pincode*</label>
												
												<input type="text" id="pincode" name="pincode" placeholder="Pincode" onkeyup="pincodOnlyNumbers();" maxlength="6"  />
												
											</fieldset><!--/ .input-phone-->
										</div>
										
									</div><!--/ .row-->
									

									<div class="row" style="height: 100px;"> 
										<div class="col-md-6 col-sm-6">
											<div class="input-block">
												
												 
													 <label>Profile Photo:</label>
								<input name="imageFile" id="imageFile" type="file" placeholder="upload file Photo"  />
												 
												
											</div><!--/ .input-country-->
											
										
										</div>
										 	<div class="col-md-6 col-sm-6">
											<img id="blah" src="" alt="  "  width="200" height="100"/>
										
										</div>
									</div><!--/ .row-->
									
									
									 
								</div>

						 
								<div class="col-md-8 col-sm-7" style="margin-top: 15px;">

									<fieldset class="sign">
									<legend >Sign in Details</legend>
                                    	<div class="row">
										<div class="col-md-12 col-sm-12" >
											<fieldset class="input-block">
												<label for="card-number">User Name*</label><br />
												<input type="text"  name="uname"  id="uname" placeholder="User Name"  value="" size="20" style="float: left;width: 42%" /> 
												 <div id="uNameDiv"  ></div>
											</fieldset>
										</div>
										</div>
										<div class="row">
										<div class="col-md-6 col-sm-6" >
											<fieldset class="input-block">
												<label for="card-number">Password* <span style="font-size: 10px;font-style: italic;color: red;margin-left: 192px;" title="Password should contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character and min 8 characters" >Rules</span></label>
												<input type="password" name="password"  id="password" class="passWord" value="" onchange="checkPasswordStrong(this);" placeholder="Password" style="float: left;width: 90%"   />	 
           										<div id="passDiv" ></div>
											</fieldset>
										</div>
										
										
										<div class="col-md-6 col-sm-6" >
											<fieldset class="input-block">
												<label for="card-number">Confirm Password* </label>
												<input name="retypepassword" id="retypepassword" type="password" onchange="checkpassword();"   placeholder="Confirm Password"  style="float: left;width: 90%" />	 
         										<div id="confDiv" ></div>
											</fieldset>
										</div>
										
										<div class="col-md-12 col-sm-12" id="passRules" style="height: 30px;font-size: 12px;font-style: italic;color: red;" >
											 
										</div>
									 
										<div class="col-md-6 col-sm-6" style="margin-top: 5px; " >
										<fieldset class="input-block">
											 <input id="captchaText" type="text" readonly="readonly"  value="" style="float: left;width: 45%;background-image: url(img/BG1.png);color: black;border: none;text-align: center;" />
											<input id="captchabox" type="text" style="float:left;width: 45%"  />	
											<div id="captchaDiv" ></div>	
											</fieldset>
										</div>
										
										
										</div>		
									</fieldset><!--/ .input-email-->

								</div>

							</div><!--/ .row-->
							
						</div><!--/ .form-wizard-->
						
						

						
						
						<div class="next1" style="float:right; ">
							<button class="button button-control" type="button" id="validateRegister"><span>Next Step </span></button>
							<div class="button-divider"></div>
						</div>
						
						</div>
						
						<div id="div2">
						<div class="row">

						<div class="col-xs-12">

							<div class="form-header">
								<div class="form-title form-icon title-icon-lock">
									<b>Select the course</b>
								</div>
								<div class="steps">
									Steps 2 - 4
								</div>
							</div><!--/ .form-header-->
							
											<div class="form-wizard">
							
							<div class="row">

								<div class="col-md-5 col-sm-5">

									<div class="row">
										
										<div class="col-md-4 col-sm-4">

											<div class="input-block">
												<label>Course*:</label>
												
													
								<html:select styleClass="courseSelect" property="courseid"  multiple="true" styleId="courses">
										<html:options styleClass="courseSelectOption" collection="courselist" property="courseid"	labelProperty="coursename" />
								</html:select>
								<a href="javaScript:void(0)" id="clear" style="font-style: italic;color: blue;font-size: 12px;margin-top: 5px;">  clear </a>
												
												
											</div><!--/ .input-role-->
											
										</div>
									<input type="hidden"  name="subAmount"  id="subAmount"  value="0"    />	
									<input type="hidden"  name="totalAmount"  id="totalAmount"  value="0"    />	 
									<input type="hidden"  name="tax"  id="tax"  value="0"    />	 
									<input type="hidden"  name="discountAmount"  id="discountAmount"  value="0"    />
									<input type="hidden"  name="serviceTax"  id="serviceTax"  value="<bean:write name="serviceTax" />"    />	 
										 
									</div><!--/ .row-->

									 

								</div>
								
								

							

								<div class="col-md-6 col-sm-6" id="courseList">

									<div class="data-container"  >
										<table border="1" cellspacing="10" cellpadding="5" width="100%" class="courseListTable"  style="text-align:center;">
                                        <tr class="courseDispTr">
                                        <td>Selected course</td>
                                        <td>Cost in Rs</td>
                                        </tr>
                                         <tr ><td colspan="2">Select the course</td></tr>
                                         <!--  <tr><td> Sub Total </td><td>800</td></tr>
                                         <tr><td>Service Tax @ 14% <span style="color:#0DD80D;font-size:16px;">+</span> </td><td>140</td></tr>
                                         <tr><td> Total Amount Discount@ <span style="color:#CB0C04;font-size:16px;">-</span></td><td> 20%</td></tr>
                                         <tr><td> Total Amount</td><td> 940</td></tr> -->
                                       </table>
									</div>

								</div>
							
							
							
						</div><!--/ .form-wizard-->
							
							
							
							
							
							
							
							
							
							
							
							
<div class="prev1" style="width:300px;">
							<button class="button button-control" type="button"   onclick="goNext('1')" ><span>Prev Step </span></button>
							<div class="button-divider"></div>
						</div>
						
						<div class="next1"  style="float:right;margin-top:-65px;">
							<button class="button button-control" type="submit"  id="validateSubmit"><span>Register & Make Payment </span></button>
							<div class="button-divider"></div>
						</div>
						</div>

					</div><!--/ .row-->
					</div>
					</div>
						<div id="div3">
							<div class="row">

						<div class="col-xs-12">

							<div class="form-header">
								<div class="form-title form-icon title-icon-lock">
									<b>Make the Payment</b>
								</div>
								<div class="steps">
									Steps 3 - 4
								</div>
							</div><!--/ .form-header-->

						</div>

					</div><!--/ .row-->
						<div class="form-wizard">
							
							<div class="row">

								<div class="col-md-8 col-sm-7">

									<div class="row">
										
										<div class="col-md-12 col-sm-12">
											<fieldset class="input-block">
												<label>Card Type</label>
												<div class="dropdown">
													<select name="card-type" class="dropdown-select">
														<option value="1">Visa</option>
														<option value="2">Master Card</option>
													</select>
												</div><!--/ .input-dropdown-->
											</fieldset><!--/ .input-card-type-->
										</div>
										
									</div><!--/ .row-->

									<div class="row">
										
										<div class="col-md-9 col-sm-9">
											<fieldset class="input-block">
												<label for="card-number">Card Number</label>
												<input type="text" id="card-number" placeholder="Number"  />
												 
											</fieldset><!--/ .input-cardnumber-->
										</div>
										
										<div class="col-md-3 col-sm-3">
											<fieldset class="input-block">
												<label for="cvc">CVC</label>
												<input type="password" id="cvc" placeholder="Cvc"  />
												 
											</fieldset><!--/ .input-cvc-->
										</div>
										
									</div><!--/ .row-->

									<div class="row">
										
										<div class="col-md-12 col-sm-12">
											<fieldset class="input-block">
												<label for="card-holder">Card Holder Name</label>
												<input type="text" id="card-holder" class="form-icon form-icon-user" placeholder="Please enter your email ID"  />
												 
											</fieldset><!--/ .input-holder-->
										</div>
										
									</div><!--/ .row-->

									<div class="row">
										
										<div class="col-md-3 col-sm-4">
											
											<fieldset class="input-block">
												<label>Expiry Month</label>
												<div class="dropdown">
													<select name="month" class="dropdown-select">
														<option value="1">01</option>
														<option value="2">02</option>
														<option value="3">03</option>
														<option value="4">04</option>
														<option value="5">05</option>
														<option value="6">06</option>
														<option value="7">07</option>
														<option value="8">08</option>
														<option value="9">09</option>
														<option value="10">10</option>
														<option value="11">11</option>
														<option value="12">12</option>
													</select>
												</div><!--/ .dropdown-->
											</fieldset><!--/ .input-month-->
											
										</div>

										<div class="col-md-3 col-sm-4">

											<fieldset class="input-block">
												<label>Expiry Year</label>
												<div class="dropdown">
													<select name="year" class="dropdown-select">
														<option value="1">2014</option>
														<option value="2">2015</option>
														<option value="3">2016</option>
														<option value="4">2017</option>
													</select>
												</div><!--/ .dropdown-->
											</fieldset><!--/ .input-year-->

										</div>

									</div><!--/ .row-->

								</div>

							</div><!--/ .row-->
							
						</div><!--/ .form-wizard-->
 

						<div class="prev1" style="width:300px;">
							<button class="button button-control" type="button"  onclick="goNext('2')" ><span>Prev Step  </span></button>
							<div class="button-divider"></div>
						</div>
						
						<div class="next1"  style="float:right; margin-top:-61px;">
							<button class="button button-control" type="button" id="validateSubmit"><span>Register</span></button>&nbsp;&nbsp;
							<button class="button button-control" type="button" onclick="goNext('4')"><span>Skip</span></button>
							<div class="button-divider"></div>
						</div>
					
						 
					</div> 
					 </html:form>
					<div id="div4">
					<form  action="Login.do?action=login" class="login active" method="post" id="loginform" onsubmit="return validate();" >
						<div class="row">

						<div class="col-xs-12">

							<div class="form-header">
								<div class="form-title form-icon title-icon-lock">
									<b>Login</b>
								</div>
								<div   class="form-title" style="width: 725px;text-align: center;" >
									<b style="color: red;"></b>
								</div>
								<div class="steps">
									Steps 4 - 4
								</div>
							</div><!--/ .form-header-->
							<div class="form-wizard">
							<div class="row" style="color: red;text-align: center;">
							<div class="col-md-12 col-sm-12">
											<fieldset class="input-block">
							<logic:notEmpty name="status"><bean:write name="status"/> </logic:notEmpty>
							</fieldset><!--/ .input-first-name-->
										</div>
							</div>
							<div class="row">
										<div class="col-md-6 col-sm-12">
											<fieldset class="input-block">
												<label for="last-name">User Name</label>
												<input type="text" name="userName"  id="userNameLogin" placeholder="User Name"  autofocus />
												
											</fieldset><!--/ .input-first-name-->
										</div>
										 
										
									</div><!--/ .row-->
							
							
							<div class="row">
										 
										<div class="col-md-6 col-sm-12">
											<fieldset class="input-block">
												<label for="input-block">Password</label>
												<input name="password" id="passwordLogin" type="password" placeholder="Password"  />
												
											</fieldset><!--/ .input-email-->
										</div>
										
									</div><!--/ .row-->
						</div> 
							
							
							
<!-- <div class="prev1" style="width:300px;">
							<button class="button button-control" type="button"   onclick="goNext('3')" ><span>Prev Step  </span></button>
							<div class="button-divider"></div>
						</div>
	 -->					
						<div class="next1"  style="float:right; margin-top:-61px;">
							<button class="button button-control" type="submit"  id="validateLogin"><span>LogIn</span></button>
							<div class="button-divider"></div>
						</div>
						</div>

					</div><!--/ .row-->
					 </form>
					</div>
						 
					<!--/ .form-wizard-->
				</div><!--/ .container-->
				
			</div><!--/ .form-container-->

		</div><!--/ #content-->


		<!-- - - - - - - - - - - - end Content - - - - - - - - - - - - - -->
 <!-- Start Footer -->
     <jsp:include page="jsp/common/student_footer.jsp" /> 
  <!-- End Footer -->
<logic:notEmpty name="status">
<script >
$(document).ready( function() {
	for (i = 1; i < 4; i++) {
		$("#div"+i).hide()
		$("#topDiv"+i).removeClass("tmm-current")
	}
	$("#div4").show();
	$("#topDiv4").addClass("tmm-current")
	
});
</script>
</logic:notEmpty>
<logic:empty name="status">
<script type="text/javascript">
$(document).ready( function() {
	$("#firstName").focus();
	$("#countryid").val("-1");
	var num = Math.floor(Math.random() * 9000) + 1000;
	//alert(num);
	$("#captchaText").val(num);
	
	for (i = 2; i < 5; i++) {
		$("#div"+i).hide()
		$("#topDiv"+i).removeClass("tmm-current")
	}
$("#countryid").change( function() {
	var countryid=$(this).val();
	//alert(countryid);
	$.ajax({	
		type: "GET",		
		url:"State.do?action=ajaxActiveList",
		data:{countryId:countryid},
		success: function(r){	
			var json= JSON.parse(r);
			console.log(json);			
			$("#stateid").empty();
			var firstOption= $("<option>",{text:"--Select State--",value:"-1"});
			$("#stateid").append(firstOption);
			$.each(json,function(i,obj){
				var option= $("<option>",{text:obj.state, value:obj.stateId});
				$("#stateid").append(option);
			});
			 
		} 
		
	});
});

$("#stateid").change( function() {
	var stateid=$(this).val();
	//alert(stateid);
	$.ajax({	
		type: "GET",		
		url:"City.do?action=ajaxActiveList",
		data:{stateId:stateid},
		success: function(r){	
			var json= JSON.parse(r);
			console.log(json);			
			$("#cityid").empty();
			var firstOption= $("<option>",{text:"--Select City--",value:"-1"});
			$("#cityid").append(firstOption);
			$.each(json,function(i,obj){
				var option= $("<option>",{text:obj.city, value:obj.cityId});
				$("#cityid").append(option);
			});
			 
		} 
		
	});
	
});

});

function pincodOnlyNumbers() {
	//alert("arg");
	re = /\D/g; // remove any characters that are not numbers
	socnum = document.forms[0].pincode.value.replace(re, "");
	document.forms[0].pincode.value = socnum;
}

</script>
</logic:empty>
	<script src="js/commonFunction.js"  type="text/javascript"></script>	
	<script type="text/javascript">
function readURL(input) {
	if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$(document).ready(function() {
$("#imageFile").change(function(){
    readURL(this);
});
});
</script>
	</body>

<!-- Mirrored from 0.s3.envato.com/files/94682367/form-wizard-with-icon.html by HTTrack Website Copier/3.x [XR&CO'2013], Tue, 09 Jun 2015 12:23:40 GMT -->
</html>