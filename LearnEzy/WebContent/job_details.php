﻿<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"  dir="ltr" lang="en-US"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<!-- Set the viewport width to device width for mobile -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="author" content="ppandp">
<meta name="Description" content="FULLSCREEN – Photography Portfolio HTML5" />
<title>Greenpiece – Careers</title>
<link href="css/reset.css" rel="stylesheet" type="text/css" media="screen" />
<link href="css/contact.css" rel="stylesheet" type="text/css" media="screen" />
<link href="css/styles.css" rel="stylesheet" type="text/css" media="screen" />
<link href="css/flexslider.css" rel="stylesheet" type="text/css" media="screen">
<link href="css/jquery.fancybox.css" rel="stylesheet" type="text/css" media="screen" />
<!--[if gt IE 8]><!--><link href="css/retina-responsive.css" rel="stylesheet" type="text/css" media="screen" /><!--<![endif]-->
<!--[if !IE]> <link href="css/retina-responsive.css" rel="stylesheet" type="text/css" media="screen" /> <![endif]-->
<!--[if gt IE 6]> <link href="css/styles-ie.css" rel="stylesheet" type="text/css" media="screen" /> <![endif]-->
<link href="css/print.css" rel="stylesheet" type="text/css" media="print" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,700,600,800' rel='stylesheet' type='text/css' />
<style>
.table-format-car tr td {
    padding: 9px 4px;
    border-bottom: solid 1px #ded9d9;
    line-height: 20px;
}
.table-format-car a {
    font-size: 14px;
    padding-left: 5px;
    color: #c92828;
}
.table-format-car {
    font-size: 14px;
}
.careers-head {
    background: #514244;
    color: #fff;
    padding: 7px 0 7px 9px !important;
    font-size: 16px;
}

#text ul li {
    background: url(images/bullet.jpg) left 8px no-repeat;
    padding-left: 10px;
    margin-bottom: 2px;
}
.apply-btn {
    background: #514244;
    color: #dbd0ce;
    padding: 8px 12px;
    font-family: myriad;
    font-size: 18px;
    text-align: center;
    border: none;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    outline: 0;
    cursor: pointer;
	margin: 17px auto;
	width:180px !important;
}
</style>
</head>
<body class="post">
<div id="wrap">
  <!-- Preloader -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <header id="wrapper">
    <div class="container clearfix">
      <h1 id="logo"><a href="index.html">MY FOLIO</a></h1>
      <!-- start navi -->
      <div id="nav-button"> <span class="nav-bar"></span> <span class="nav-bar"></span> <span class="nav-bar"></span> </div>
      <div id="options" class="clearfix">
      <ul id="main-menu">
          <li><a href="index.html">Home</a>
          	<!--<ul class="other">
              <li><a href="index2.html">Full Opacity</a></li>
            </ul>--></li>
          
          <li><a href="about.html">About</a></li>
    <li><a href="jobopenings.php" class="current">Careers</a></li>
          <li><a href="#" class="dead-link">Portfolio</a>
            <ul class="other">
              <li><a href="corporates.html" >Corporates</a></li>
              <li><a href="hospitality.html">Hospitality</a></li>
              <li><a href="real_estate.html">Real Estate</a></li>
              <li><a href="residences.html">Residences</a></li>
              
            </ul>
          </li>
          <li><a href="locations.html">Where we are</a></li>
          <li><a href="contact.html">Contact</a></li>
        </ul>
         <ul id="homepage" class="option-set clearfix" data-option-key="filter">
          <li><a href="#filter=.home" class="selected">All</a>
          <li><a href="corporates.html">Corporates</a></li>
          <li><a href="hospitality.html">Hospitality</a></li>
          
          <li><a href="real_estate.html">Real Estate</a></li>
          <li><a href="residences.html">Residences</a></li>
        </ul>
       
      </div>
      <!-- end nav -->
    </div>
   
  </header>
  <!-- end header -->
  <div id="content">
    <div class="container clearfix">
      <div id="container">
        <div class="col2 home element">
          <div class="images"> <img src="images/careers.jpg" alt="" />
            <!--<div id="map"></div>-->
          </div>
          <div class="white-bottom  grey-area-last" id="text">
         
<div style="float:right"><a href="careers-current-openings.php" class="mail">Back</a></div>
<h1 style="font-size:22px; text-transform:uppercase; font-weight:900; margin:12px 0px;">Current Openings</h1>
<?php 
       $id = $_REQUEST['jobpostid'];
       $conn=  mysqli_connect("localhost", "root", "root","greenpie_greenpiece");
       
		if(!$conn )
		{
  			die('Could not connect: ' . mysql_error());
		}
		
       $sql = "select * from jobpostdetails where jobpostid =$id";
       $retval = mysqli_query($conn,$sql);
       while($row =  mysqli_fetch_array($retval))
       {
       	
       	?>

<table border="0" cellspacing="0" width="100%" class=" table table-responsive table-format-car">
<tr><td colspan="3" class="careers-head" style="padding-left:10px;">Job Details</td></tr>
<tr>
<td width="20%" align='left' valign='TOP'>Job Position</td>
<td width="5%" align='left' valign='TOP'><strong>&nbsp;&nbsp;:</strong></td>
<td align='left' valign='TOP' width="75%" class="noborrgt"><?php echo $row['jobposition'] ;?>
</td>
</tr>

<tr>
<td align='left' valign='TOP'>Functional Area</td>
<td align='left' valign='TOP'><strong>&nbsp;&nbsp;:</str ong></td>
<td align='left' valign='TOP'><?php echo $row['functionalarea'] ;?></td>
</tr>
<tr>
<td align='left' valign='TOP'>Number of Position</td>
<td align='left' valign='TOP'><strong>&nbsp;&nbsp;:</str ong></td>
<td align='left' valign='TOP'><?php echo $row['nuberofpositions'] ;?></td>
</tr>

<tr>
<td align='left' valign='TOP'>Desired Profile</td>
<td align='left' valign='TOP'><strong>&nbsp;&nbsp;:</str ong></td>
<td align='left' valign='TOP'><?php echo $row['desiredprofile'] ;?></td>

<tr>
<td align='left' valign='TOP'>Experience</td>
<td align='left' valign='TOP'><strong>&nbsp;&nbsp;:</str ong></td>
<td align='left' valign='TOP'><?php echo $row['experience'] ;?>
</td>
</tr>

<tr>
<td align='left' valign='TOP'>PAY &amp; Benefits</td>
<td align='left' valign='TOP'><strong>&nbsp;&nbsp;:</str ong></td>
<td align='left' valign='TOP'><?php echo $row['payandbenifits'] ;?>
</td>
</tr>



<tr>
<td align='left' valign='TOP'>Location</td>
<td align='left' valign='TOP'><strong>&nbsp;&nbsp;:</str ong></td>
<td align='left' valign='TOP'><?php echo $row['location'] ;?>
</td>
</tr>

<tr>
<td align='left' valign='TOP'>Job Description</td>
<td align='left' valign='TOP'><strong>&nbsp;&nbsp;:</str ong></td>
<td align='left' valign='TOP'><?php echo $row['jobdescription'] ;?>

</td>
</tr>
<tr>
<td align='left' valign='TOP'>Key Skills </td>
<td align='left' valign='TOP'><strong>&nbsp;&nbsp;:</str ong></td>
<td align='left' valign='TOP'><?php echo $row['skills'] ;?>

</td>
</tr>

<tr>
<td align='left' valign='TOP'>Contact</td>
<td align='left' valign='TOP'><strong>&nbsp;&nbsp;:</strong></td>
<td align='left' valign='TOP'><?php echo $row['emaildid'] ;?>

</td>
</tr>
<tr>
<td align='left' valign='TOP'>Last Date</td>
<td align='left' valign='TOP'><strong>&nbsp;&nbsp;:</strong></td>
<td align='left' valign='TOP'>27<sup>th</sup> October 2016</td>
</tr>
<div align="left"><form action="jobApply.php" method="POST" >
<input name="jobid" type="hidden" value=""/>
<tr>
<td align="center" valign="top" style="border-style: none; border-color: inherit; border-width: medium; height: 45px;" colspan="3"><input type="submit" value="Apply Online" name="B1" class="apply-btn"></td>
</tr>
</form></div></table>
<p>&nbsp;</p>
<p>&nbsp;</p>

          </div>
        </div>
      
      </div>
    </div>
  </div>
  <!-- end content -->
</div>
<!-- end wrap -->
<footer>
  <div class="container">
    <ul class="social clearfix alignleft">
     <li class="facebook"><a href="https://www.facebook.com/pages/GreenPiece-Landscapes/765725810116252?skip_nax_wizard=true" target="_blank" onClick="return true">Visit our pinterest Account</a></li>
      <li class="google"><a href="https://plus.google.com/u/0/103599611639524788636/about" target="_blank" onClick="return true">Visit our dribble Account</a></li>
      <li class="twitter"><a href="https://twitter.com/GreenpieceBlr" target="_blank" onClick="return true">Visit our twitter Account</a></li>
      <li class="linkedin"><a href="https://www.linkedin.com/pub/sunil-desouza/7a/620/a44" target="_blank" onClick="return true">Visit our instagram Account</a></li>
    </ul>
    <p class="small alignright"> © 2013, GREENPIECE, All Rights Reserved <br>
    Designed By :<a href="www.itechsolutions.in" target="_blank"> Itech Solutions</a></p>
  </div>
</footer>

<!-- BACK TO TOP BUTTON -->
<div id="backtotop">
  <ul>
    <li><a id="toTop" href="#" onClick="return false">Back to Top</a></li>
  </ul>
</div>
<!--<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>-->
<script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="js/jquery-easing-1.3.js" type="text/javascript"></script>
<script src="js/modernizr.js" type="text/javascript"></script>
<script src="js/retina.js" type="text/javascript"></script>
<!--<script src="js/jquery.gomap-1.3.2.min.js" type="text/javascript"></script>-->
<script src="js/jquery.isotope.min.js" type="text/javascript"></script>
<script src="js/jquery.ba-bbq.min.js" type="text/javascript"></script>
<script src="js/jquery.isotope.load.js" type="text/javascript"></script>
<script src="js/jquery.isotope.perfectmasonry.js"></script>
<script src="js/jquery.form.js" type="text/javascript"></script>
<script src="js/input.fields.js" type="text/javascript"></script>
<script src="js/responsive-nav.js" type="text/javascript"></script>
<script defer src="js/jquery.flexslider-min.js"></script>
<script src="js/jquery.fancybox.pack.js" type="text/javascript"></script>
<script src="js/image-hover_opacity1.js" type="text/javascript"></script>
<script src="js/scrollup.js" type="text/javascript"></script>
<script src="js/preloader.js" type="text/javascript"></script>
<script src="js/navi-slidedown.js" type="text/javascript"></script>
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</body>
</html>
