<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>

<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui">
		<title>Learnezy</title>
		<link href="favicon.png" type="image/x-icon" rel="shortcut icon">
		<link href="assets/css/master.css" rel="stylesheet">
		<script src="<%=request.getContextPath()%>/js/jquery-2.1.1.min.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/jquerypopupcss/jquery-ui.css">
<script src="<%=request.getContextPath()%>/jquerypopup/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">

$(document).ready(function()
		
		{
   var id=$("#sid").val();
	var b=$("#statusValue").val();
	
	$("#tempId").val(id);
	$("#enquiryPopup").show();
	$("#enquiryDialog").dialog();
	
		});
		
</script>
		

</head>

<body>
<div style="color:green;" id="enquiryDialog" title="Confirm purchase" width="500" height="1000">
<div id="enquiryPopup"> 
		
		
		
	<form action="pg1.jsp"  method="post" id="prodForm"> 
	<input type="hidden" name="userid" value="10">
	
	<logic:notEmpty name="course">
	<p>CourseName:<bean:write name="course"></bean:write></p>
		</logic:notEmpty>
		<logic:notEmpty name="emailid">
	
	<input type="hidden" name="email" value=<bean:write name="emailid"></bean:write>></input>
	</logic:notEmpty>
	<logic:notEmpty name="price">
	<p>Price:<bean:write name="price"></bean:write></p>
			<input type="hidden" name="totamt" value="<bean:write name="price"></bean:write>">
	
	</logic:notEmpty>
			
			<input type='submit' value='Paybal' id="prodSubmit" >
			
			
	</form>
     
</div>
</div>

<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->

		<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200">



			<div id="wrapper">

				<!-- HEADER -->
				<header class="header">
      <div class="top-header">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <div class="top-header__contacts"><i class="icon stroke icon-Phone2"></i>(123) 0800 12345</div>
              <div class="top-header__contacts"><i class="icon stroke icon-Mail"></i><a href="mailto:Info@academica.com">Info@academica.com</a></div>
             
              
            </div>
            <!-- end col  --> 
          </div>
          <!-- end row  --> 
        </div>
        <!-- end container  --> 
      </div>
      <!-- end top-header  -->
      
      <div class="container">
        <div class="row">
          <div class="col-xs-12"> <a class="header-logo" href="javascript:void(0);"><img class="header-logo__img" src="assets/img/logo.png" height="50" width="195" alt="Logo"></a>
         
          </div>
          <!-- end col  --> 
        </div>
        <!-- end row  --> 
      </div>
      <!-- end container--> 
    </header><!-- end header -->


				<div class="wrap-title-page">
					<div class="container">
						<div class="row">
							<div class="col-xs-12"><h1 class="ui-title-page"> Confirm Purchase</h1></div>
						</div>
					</div><!-- end container-->
				</div><!-- end wrap-title-page -->


				
				<div class="container">
					<div class="row">
						<div class="col-md-8">
							
<div class="col-xs-12 col-sm-6 widget-container-col" id="widget-container-col-4">
											<div class="widget-box" id="widget-box-4">
												<div class="widget-header widget-header-large">
													<h4 class="widget-title">Big Header</h4>

													<div class="widget-toolbar">
														<a href="#" data-action="settings">
															<i class="ace-icon fa fa-cog"></i>
														</a>

														<a href="#" data-action="reload">
															<i class="ace-icon fa fa-refresh"></i>
														</a>

														<a href="#" data-action="collapse">
															<i class="ace-icon fa fa-chevron-up"></i>
														</a>

														<a href="#" data-action="close">
															<i class="ace-icon fa fa-times"></i>
														</a>
													</div>
												</div>
					
</div>
						


						</div><!-- end col -->
					</div><!-- end row -->
		
<div style="clear:both;"></div>
				<footer class="footer">
					<div class="container-fluid">
						

						<div class="row">
							<div class="col-xs-12">
								<div class="footer-bottom">
									<div class="copyright">Copyright © 2016 <a href="javascript:void(0);">Learnezy</a></div>
									
								</div><!-- end footer-bottom -->
							</div><!-- end col -->
						</div><!-- end row -->
					</div><!-- end container -->
				</footer>

			</div><!-- end wrapper -->
		</div><!-- end layout-theme -->

<!-- SCRIPTS --> 
<script src="assets/js/jquery-migrate-1.2.1.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<script src="assets/js/modernizr.custom.js"></script> 
<script src="assets/js/waypoints.min.js"></script> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> 

<!--THEME--> 
<script  src="assets/plugins/sliderpro/js/jquery.sliderPro.min.js"></script> 
<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script> 
<script src="assets/plugins/isotope/jquery.isotope.min.js"></script> 
<script src="assets/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script> 
<script src="assets/plugins/datetimepicker/jquery.datetimepicker.js"></script> 
<script src="assets/plugins/jelect/jquery.jelect.js"></script> 
<script src="assets/plugins/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js"></script> 
<script src="assets/js/cssua.min.js"></script> 
<script src="assets/js/wow.min.js"></script> 
<script src="assets/js/custom.min.js"></script> 


</body>
</html>
