$(document).on("focusin", '.calendar', function(event) {
	$(this).prop('readonly', true);
});
$(document).on("focusout", '.calendar', function(event) {
	$(this).prop('readonly', false);
});
$(document).ready(function () {
	$('.calendar').calendar();
});