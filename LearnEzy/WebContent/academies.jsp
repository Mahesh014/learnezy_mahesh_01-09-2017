<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui">
		<title>ACADEMICA, Instructors</title>
		<link href="favicon.png" type="image/x-icon" rel="shortcut icon">
		<link href="assets/css/master.css" rel="stylesheet">

<script src="assets/plugins/jquery/jquery-1.11.3.min.js"></script>

</head>

<body>

<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->
		<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200">

			


			<div id="wrapper">

			<jsp:include page="Commonheader.jsp"></jsp:include>

				<div class="wrap-title-page">
					<div class="container">
						<div class="row">
							<div class="col-xs-12"><h1 class="ui-title-page">our Academies</h1></div>
						</div>
					</div><!-- end container-->
				</div><!-- end wrap-title-page -->


				<div class="section-breadcrumb">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div class="wrap-breadcrumb clearfix">
									<ol class="breadcrumb">
										<li><a href="index.html"><i class="icon stroke icon-House"></i></a></li>
										<li class="active">Academies</li>
									</ol>
								</div>
							</div>
						</div><!-- end row -->
					</div><!-- end container -->
				</div><!-- end section-breadcrumb -->


				<main class="main-content">

					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<div class="wrap-title wrap-title_mod-b">
									<div class="ui-subtitle-block">We have very skilled and professional Academies to make you learn what you want</div>
								</div><!-- end wrap-title -->


								<div class="posts-wrap">

									<div class="staff">
										<div class="staff__media"><img class="img-responsive" src="assets/media/staff/7.jpg" height="250" width="270" alt="foto"><div class="staff__hover"><a class="btn btn-primary btn-effect" href="javascript:void(0);">VIEW PROFILE</a></div></div>
										<div class="staff__inner staff__inner_mod-a">
											<h3 class="staff__title">Academy Name</h3>
											<div class="staff__categories"><a class="post-link" href="javascript:void(0);">Location</a></div>
											<div class="staff__description">Easellus luctus nib pulvinar bien quam ligula sapien condime</div>
											
										</div>
									</div>

									<div class="staff">
										<div class="staff__media"><img class="img-responsive" src="assets/media/staff/5.jpg" height="250" width="270" alt="foto"><div class="staff__hover"><a class="btn btn-primary btn-effect" href="javascript:void(0);">VIEW PROFILE</a></div></div>
										<div class="staff__inner staff__inner_mod-a">
											<h3 class="staff__title">MARIA LAURAN</h3>
											<div class="staff__categories"><a class="post-link" href="javascript:void(0);">Developer</a></div>
											<div class="staff__description">Easellus luctus nib pulvinar bien quam ligula sapien condime</div>
											
										</div>
									</div>

									<div class="staff">
										<div class="staff__media"><img class="img-responsive" src="assets/media/staff/3.jpg" height="250" width="270" alt="foto"><div class="staff__hover"><a class="btn btn-primary btn-effect" href="javascript:void(0);">VIEW PROFILE</a></div></div>
										<div class="staff__inner staff__inner_mod-a">
											<h3 class="staff__title">ANDREW FINCH</h3>
											<div class="staff__categories"><a class="post-link" href="javascript:void(0);">medical</a></div>
											<div class="staff__description">Easellus luctus nib pulvinar bien quam ligula sapien condime</div>
											
										</div>
									</div>

									<div class="staff">
										<div class="staff__media"><img class="img-responsive" src="assets/media/staff/6.jpg" height="250" width="270" alt="foto"><div class="staff__hover"><a class="btn btn-primary btn-effect" href="javascript:void(0);">VIEW PROFILE</a></div></div>
										<div class="staff__inner staff__inner_mod-a">
											<h3 class="staff__title">SANDY ANDERSON</h3>
											<div class="staff__categories"><a class="post-link" href="javascript:void(0);">Computer science</a></div>
											<div class="staff__description">Easellus luctus nib pulvinar bien quam ligula sapien condime</div>
											
										</div>
									</div>

									<div class="staff">
										<div class="staff__media"><img class="img-responsive" src="assets/media/staff/2.jpg" height="250" width="270" alt="foto"><div class="staff__hover"><a class="btn btn-primary btn-effect" href="javascript:void(0);">VIEW PROFILE</a></div></div>
										<div class="staff__inner staff__inner_mod-a">
											<h3 class="staff__title">anna Charlotte</h3>
											<div class="staff__categories"><a class="post-link" href="javascript:void(0);">Artist</a></div>
											<div class="staff__description">Easellus luctus nib pulvinar bien quam ligula sapien condime</div>
										
										</div>
									</div>

									<div class="staff">
										<div class="staff__media"><img class="img-responsive" src="assets/media/staff/1.jpg" height="250" width="270" alt="foto"><div class="staff__hover"><a class="btn btn-primary btn-effect" href="javascript:void(0);">VIEW PROFILE</a></div></div>
										<div class="staff__inner staff__inner_mod-a">
											<h3 class="staff__title">AMELIA JOHNSON</h3>
											<div class="staff__categories"><a class="post-link" href="javascript:void(0);">Developer</a></div>
											<div class="staff__description">Easellus luctus nib pulvinar bien quam ligula sapien condime</div>
											
										</div>
									</div>

									<div class="staff">
										<div class="staff__media"><img class="img-responsive" src="assets/media/staff/4.jpg" height="250" width="270" alt="foto"><div class="staff__hover"><a class="btn btn-primary btn-effect" href="javascript:void(0);">VIEW PROFILE</a></div></div>
										<div class="staff__inner staff__inner_mod-a">
											<h3 class="staff__title">Emma lily</h3>
											<div class="staff__categories"><a class="post-link" href="javascript:void(0);">medical</a></div>
											<div class="staff__description">Easellus luctus nib pulvinar bien quam ligula sapien condime</div>
											
										</div>
									</div>

									<div class="staff">
										<div class="staff__media"><img class="img-responsive" src="assets/media/staff/8.jpg" height="250" width="270" alt="foto"><div class="staff__hover"><a class="btn btn-primary btn-effect" href="javascript:void(0);">VIEW PROFILE</a></div></div>
										<div class="staff__inner staff__inner_mod-a">
											<h3 class="staff__title">Michael Ryan</h3>
											<div class="staff__categories"><a class="post-link" href="javascript:void(0);">Literature</a></div>
											<div class="staff__description">Easellus luctus nib pulvinar bien quam ligula sapien condime</div>
										
										</div>
									</div>
								</div><!-- end posts-wrap -->
							</div><!-- end col -->
						</div><!-- end row -->
					</div><!-- end container -->

				</main><!-- end main-content -->


		<jsp:include page="Commonfooter.jsp"></jsp:include>

			</div><!-- end wrapper -->
		</div><!-- end layout-theme -->


	<!-- SCRIPTS --> 
 <script src="assets/js/jquery-migrate-1.2.1.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<script src="assets/js/modernizr.custom.js"></script> 
<script src="assets/js/waypoints.min.js"></script> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> 

<!--THEME--> 
<script  src="assets/plugins/sliderpro/js/jquery.sliderPro.min.js"></script> 
<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script> 
<script src="assets/plugins/isotope/jquery.isotope.min.js"></script> 
<script src="assets/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script> 
<script src="assets/plugins/datetimepicker/jquery.datetimepicker.js"></script> 
<script src="assets/plugins/jelect/jquery.jelect.js"></script> 
<script src="assets/plugins/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js"></script> 
<script src="assets/js/cssua.min.js"></script> 
<script src="assets/js/wow.min.js"></script> 
<script src="assets/js/custom.min.js"></script> 
</body>
</html>

