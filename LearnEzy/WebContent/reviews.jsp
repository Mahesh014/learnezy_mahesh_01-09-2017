<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
        <%@ taglib uri="/WEB-INF/TLD/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/TLD/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/TLD/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Itech Training :: Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300' rel='stylesheet' type='text/css'>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/cmsstyle.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/carousel.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>
       

<script src="js/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="js/bootstrap.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/owl.carousel.js" type="text/javascript"></script>
 
</head>
<body>
<!-- Start Header -->
       <div class="header">	
   	 	    <div class="header-top">
   	 	      <div class="wrap"> 
   	 	    	
   				  <div class="header-top-right">
				        <ul>
				            <li><a href"#">Careers</a></li>
				            <li  class="login">
				              <div id="loginContainer">
				            	   <a href="#" id="loginButton"><span>Join Us</span></a>
						                <div id="loginBox" class="login-form">    
						                	<h3>Login into Your Account</h3>            
						                      <form id="loginForm">
						                                <span>
											 	    		<i><img src="images/user.png" alt="" /></i>
											 	    		 <input type="text" value="yourname@mail.com" onFocus="this.value = '';" onBlur="if (this.value == '') {this.value = 'yourname@mail.com';}">
											 	    	</span>
						                                <span>
												 	     <i><img src="images/lock.png" alt="" /></i>
											 	         <input type="password" value="........." onFocus="this.value = '';" onBlur="if (this.value == '') {this.value = '.........';}">
												 	    </span>
						                               <input type="submit" value="Login">
						                       </form>
						                  </div>
						             </div>
				               </li>
				               <li><a href="#" >Sitemap</a></li>
				         </ul>
				    </div>
			      <div class="clear"></div>
			     </div> 
		      </div>
          <div class="header-logo-nav">
<div class="navbar navbar-inverse navbar-static-top nav-bg"
	role="navigation">
<div class="container">
<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse"
	data-target=".navbar-collapse"><span class="sr-only">Toggle
navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span>
<span class="icon-bar"></span></button>
<div class="logo"><a href="http://itechtraining.in/" class="logo"><img
	src="images/Itech Solutions logo.png" alt="" /></a></div>
<div class="clear"></div>
</div>
<div class="collapse navbar-collapse">
<ul class=" menu nav navbar-nav">

	<li><a href="<%=request.getContextPath()%>/studentProfile.do?action=toHome"><img src="images/back.png"></a></li>

	<li><a href="Logout.do"><img src="images/logout.png"></a></li>
	
	      <li><a href="studentProfile.do?action=toHome"><img src="images/homeover.png"></a></li>
</ul>
</div>
<!--/.nav-collapse --></div>
</div>
<div class="clear"></div>
<div class="header-bottom">
<div class="container">
<div class="time">
<p id="clockbox" ></p>
<script src="js/dynamicClock.js" type="text/javascript"></script>
</div>

<div class="welcome">
<h3 align="right"></h3>
</div>
</div>
</div>
<div class="clear"></div>
</div>
<div class="header-banner"></div>

</div>
   <!-- End Header -->
   
   <!-- Start Main Content -->
	 <div class="main" style="min-height:450px;">
       <div class="container">
      	 <div class="row"> 
	 	<h2>REVIEWS</h2>
    
                                    <div class="col-md-6">
                                    <logic:notEmpty name="status">
					<font color="blue"><bean:write name="status"></bean:write></font>
				</logic:notEmpty>
                        <html:form action="Reviews.do?action=add" styleId="reviewid">
                                                    <div class="form-group">
                                            <label>Select Course</label>
                                            <html:select property="courseid" onchange="fetch()" style="width:150px;" styleId="courseid" >
 <html:option value="-1" >--Select Course--</html:option>

 <html:options collection="courseList" property="courseid" labelProperty="coursename" /> 
 </html:select>
                                        </div>
                                           <div class="form-group">
                                            <label>Enter Your Review</label>
                                            <textarea class="form-control" rows="3" placeholder="Enter ..." name="reviewcomment"></textarea>
                                        </div>
                                             <button type="submit" class="btn btn-primary">Submit</button>
                                        </html:form>
           </div>                                </div>
     </div>
    </div>
    </div>
   
   <!-- End Main Content -->
	   
  <!-- Start Footer -->
     
      
			 <div class="footer-bottom">
			 	<div class="wrap">
			 	<div class="copy-right">
			 		<p>Itech Training, Copyright 2014, All Rights Reserved. Designed by  <a href="http://www.itechsolutions.in/" target="_blank">Itech Solutions</a></p>
			 	</div>
			 	<div class="social-icons">
			 		<ul>
			 			<li><a href="#"><i class="fa fa-twitter"></i></a></li>
			 			<li><a href="#"><i class="fa fa-facebook"></i></a></li>
			 			<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
			 			<li><a href="#"><i class="fa fa-youtube"></i></a></li>
			 			<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
			 		</ul>
			 	</div>
			 	<div class="clear"></div>
			 </div>
	       </div>
  <!-- End Footer -->
</body>
</html>