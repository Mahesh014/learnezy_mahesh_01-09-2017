<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>
<style type="text/css">
.jPaginate{
    height:34px;
    position:relative;
    color:#a5a5a5;
    font-size:small;   
	width:100%;
}
.jPaginate a{
    line-height:15px;
    height:18px;
    cursor:pointer;
    padding:2px 5px;
    margin:2px;
    float:left;
}
.jPag-control-back{
	position:absolute;
	left:0px;
}
.jPag-control-front{
	position:absolute;
	top:0px;
}
.jPaginate span{
    cursor:pointer;
}
ul.jPag-pages{
    float:left;
    list-style-type:none;
    margin:0px 0px 0px 0px;
    padding:0px;
}
ul.jPag-pages li{
    display:inline;
    float:left;
    padding:0px;
    margin:0px;
}
ul.jPag-pages li a{
    float:left;
    padding:2px 5px;
}
span.jPag-current{
    cursor:default;
    font-weight:normal;
    line-height:15px;
    height:18px;
    padding:2px 5px;
    margin:2px;
    float:left;
}
ul.jPag-pages li span.jPag-previous,
ul.jPag-pages li span.jPag-next,
span.jPag-sprevious,
span.jPag-snext,
ul.jPag-pages li span.jPag-previous-img,
ul.jPag-pages li span.jPag-next-img,
span.jPag-sprevious-img,
span.jPag-snext-img{
    height:22px;
    margin:2px;
    float:left;
    line-height:18px;
}

ul.jPag-pages li span.jPag-previous,
ul.jPag-pages li span.jPag-previous-img{
    margin:2px 0px 2px 2px;
    font-size:12px;
    font-weight:bold;
        width:10px;

}
ul.jPag-pages li span.jPag-next,
ul.jPag-pages li span.jPag-next-img{
    margin:2px 2px 2px 0px;
    font-size:12px;
    font-weight:bold;
    width:10px;
}
span.jPag-sprevious,
span.jPag-sprevious-img{
    margin:2px 0px 2px 2px;
    font-size:18px;
    width:15px;
    text-align:right;
}
span.jPag-snext,
span.jPag-snext-img{
    margin:2px 2px 2px 0px;
    font-size:18px;
    width:15px;
     text-align:right;
}
ul.jPag-pages li span.jPag-previous-img{
    background:transparent url(../images/previous.png) no-repeat center right;
            }
ul.jPag-pages li span.jPag-next-img{
    background:transparent url(../images/next.png) no-repeat center left;
            }
span.jPag-sprevious-img{
    background:transparent url(../images/sprevious.png) no-repeat center right;
            }
span.jPag-snext-img{
    background:transparent url(../images/snext.png) no-repeat center left;
            }



</style>

<body>

<div id="paginationdemo" class="demo">
                <h1>Demo 5</h1>
                <div id="p1" class="pagedemo _current" style="">Page 1</div>
				<div id="p2" class="pagedemo" style="display:none;">Page 2</div>
				<div id="p3" class="pagedemo" style="display:none;">Page 3</div>
				<div id="p4" class="pagedemo" style="display:none;">Page 4</div>
				<div id="p5" class="pagedemo" style="display:none;">Page 5</div>
				<div id="p6" class="pagedemo" style="display:none;">Page 6</div>
				<div id="p7" class="pagedemo" style="display:none;">Page 7</div>
				<div id="p8" class="pagedemo" style="display:none;">Page 8</div>
				<div id="p9" class="pagedemo" style="display:none;">Page 9</div>
				<div id="p10" class="pagedemo" style="display:none;">Page 10</div>
				<div id="demo5">                   
                </div>
            </div>
            
            
            <script type="text/javascript" src="assets/js/jquery-1.3.2.js"></script>
		<script src="assets/js/jquery.paginate.js" type="text/javascript"></script>
		<script type="text/javascript">
		$(function() {
			
			$("#demo5").paginate({
				count 		: 10,
				start 		: 1,
				display     : 7,
				border					: true,
				border_color			: '#fff',
				text_color  			: '#fff',
				background_color    	: 'black',	
				border_hover_color		: '#ccc',
				text_hover_color  		: '#000',
				background_hover_color	: '#fff', 
				images					: false,
				mouse					: 'press',
				onChange     			: function(page){
											$('._current','#paginationdemo').removeClass('_current').hide();
											$('#p'+page).addClass('_current').show();
										  }
			});
		});
		</script>

</body>
</html>
